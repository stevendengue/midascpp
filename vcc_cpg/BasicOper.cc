/**
************************************************************************
* 
* @file                BasicOper.cc
*
* Created:             13-03-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Implementation of BasicOper class.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <iostream>
#include <iomanip>
#include <vector>
#include <map>
#include <cctype>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "util/Fractions.h"
#include "vcc_cpg/BasicOper.h"

// using declarations
using std::map;

// create oper maps for changing between OP:: enum constant and string
const std::map<OP,char> OP_MAP::MAP = 
   OP_MAP::create_map();
const std::map<char,OP> OP_MAP::INV_MAP = 
   OP_MAP::create_inv_map();

/**
 * Initialize mapping between integers and characters describing operator types.
 **/
/*void BasicOper::InitOperTypeMaps()
{
   msTypeCharMap.insert(make_pair(OP_U, '?'));
   msTypeCharMap.insert(make_pair(OP_H, 'H'));
   msTypeCharMap.insert(make_pair(OP_T, 'T'));
   msTypeCharMap.insert(make_pair(OP_R, 'R'));
   msTypeCharMap.insert(make_pair(OP_P, 'P'));
   msTypeCharMap.insert(make_pair(OP_Q, 'Q'));
   msTypeCharMap.insert(make_pair(OP_S, 'S'));
   msTypeCharMap.insert(make_pair(OP_E, 'E'));

   msCharTypeMap.insert(make_pair('?', OP_U));
   msCharTypeMap.insert(make_pair('H', OP_H));
   msCharTypeMap.insert(make_pair('T', OP_T));
   msCharTypeMap.insert(make_pair('R', OP_R));
   msCharTypeMap.insert(make_pair('P', OP_P));
   msCharTypeMap.insert(make_pair('Q', OP_Q));
   msCharTypeMap.insert(make_pair('S', OP_S));
   msCharTypeMap.insert(make_pair('E', OP_E));
}*/

char BasicOper::CharForType(OP aType, bool aLowerCase)
{
   if (aLowerCase)
      return tolower(OP_MAP::MAP.at(aType));
   else
      return OP_MAP::MAP.at(aType);
}

OP BasicOper::TypeForChar(char aC)
{
   char c = toupper(aC);
   map<char, OP>::const_iterator it = OP_MAP::INV_MAP.find(c);
   if (it == OP_MAP::INV_MAP.end())
      return OP::U;
   else
      return it->second;
}

bool operator<(const BasicOper& aLeft, const BasicOper& aRight)
{
   if (aLeft.mType == aRight.mType)
      return aLeft.mLevel < aRight.mLevel;
   else
      return aLeft.mType < aRight.mType;
}

bool operator!=(const BasicOper& aLeft, const BasicOper& aRight)
{
   return !(aLeft == aRight);
}

bool operator==(const BasicOper& aLeft, const BasicOper& aRight)
{
   return (aLeft.mType == aRight.mType) && (aLeft.mLevel == aRight.mLevel);
}

void BasicOper::Latex(ostream& aOut) const
{
   aOut << char(BasicOper::CharForType(mType)) << "_{" << mLevel << "}";
}

ostream& operator<<(ostream& aOut, const BasicOper& aOper)
{
   aOut << char(BasicOper::CharForType(aOper.mType)) << aOper.mLevel;
   return aOut;
}
