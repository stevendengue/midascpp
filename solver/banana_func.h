#ifndef BANANA_FUNC_H_INCLUDED
#define BANANA_FUNC_H_INCLUDED

#include "typedefs.h"
#include "../libmda/util/require.h"

struct banana_func
{
   // Banana Function, min: f(1, 1) = 0
   //
   //
   
   /*! Evaluation of function
    * 
    */
   template< class T
           , libmda::Require_order<T,1> = 0
           >
   double operator()(T&& arg) const
   {
      assert(arg.size() == 2);
      return ((1-arg.at(0))*(1-arg.at(0)) + 
            100*(arg.at(1) - arg.at(0)*arg.at(0))*(arg.at(1) - arg.at(0)*arg.at(0)));
   }
   
   /*! First derivative
    *
    */
   template< class T
           , libmda::Enforce_order<T,1> = 0
           > 
   void first_deriv(T&& arg, T&& der) const
   {
      assert(arg.size() == 2 && der.size() == 2);
      der(0) = 2.0*arg.at(0) - 2.0 + 400.0*arg.at(0)*arg.at(0)*arg.at(0) - 400.0*arg.at(1)*arg.at(0);
      der(1) = 200.0*arg.at(1) - 200.0*arg.at(0)*arg.at(0);
   }
};

#endif /* BANANA_FUNC_H_INCLUDED */
