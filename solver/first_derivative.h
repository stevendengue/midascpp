#ifndef FIRST_DERIVATIVE_H_INCLUDED
#define FIRST_DERIVATIVE_H_INCLUDED

#include"../libmda/meta/std_wrappers.h"
#include"result_of_aug.h"
#include"tag.h"

namespace detail
{

template<class T> struct first_derivative_;

template<> struct first_derivative_<float_tag>
{
   template<class F, class Arg>
   //static const Argum2_t<F(Arg)> apply(F&& f, Arg&& x, Resul_t<F(Arg)> dx_acc)
   static void apply(F&& f, Arg&& x, Argum2_t<F(Arg)>& deriv, Resul_t<F(Arg)> dx_acc)
   {
      //Resul_t<F(Arg)>  dx = x*dx_acc;        // is this a good idea ? :O
      Resul_t<F(Arg)>  dx = norm(x)*dx_acc;        // is this a good idea ? :O
      //Resul_t<F(Arg)>  dx = dx_acc;        // is this a good idea ? :O
      Argum2_t<F(Arg)> new_x = x - dx/2.0;
      Resul_t<F(Arg)>  x1 = f(new_x);
      new_x += dx;
      Resul_t<F(Arg)>  x2 = f(new_x);
      //Argum2_t<F(Arg)> deriv = (x2-x1)/dx;
      deriv = (x2-x1)/dx;
      //return deriv;
   }
};

template<> struct first_derivative_<mda_tag>
{
   template<class F, class Arg>
   //static const Argum2_t<F(Arg)> apply(F&& f, Arg&& x, Resul_t<F(Arg)> dx_acc)
   static void apply(F&& f, Arg&& x, Argum2_t<F(Arg)>& deriv, Resul_t<F(Arg)> dx_acc)
   {
      //Argum2_t<F(Arg)> deriv(x.size());
      auto x_norm = norm(x);
      for(typename std::decay<decltype(deriv)>::type::size_type i=0; i<deriv.size(); ++i)
      {
         //Resul_t<F(Arg)> dx = x.at(i)*dx_acc; // is this a good idea ? :O
         Resul_t<F(Arg)> dx = x_norm*dx_acc; // is this a good idea ? :O
         //Resul_t<F(Arg)> dx = dx_acc; // is this a good idea ? :O
         Argum2_t<F(Arg)> new_x = x;
         new_x.at(i) -= dx/2.0;
         Resul_t<F(Arg)> x1 = f(new_x);
         new_x.at(i) += dx;
         Resul_t<F(Arg)> x2 = f(new_x);
         deriv.at(i) = (x2 - x1)/dx;
         //first_derivative_<float_tag>().apply(std::forward<F>(f)
         //                                   , x.at(i)
         //                                   , deriv.at(i)
         //                                   , dx_acc);
      }
      //std::cout << " return deriv: " << std::endl;
      //std::cout << deriv << std::endl;
      //return deriv;
   }
};

} // namespace detail

/////
// do numerical first derivative
//
//////
template<class F, class Arg>
//Argum2_t<F(Arg)> first_derivative(F&& f, Arg&& x, Resul_t<F(Arg)> dx_acc = 1e-8)
void first_derivative(F&& f, Arg&& x, Argum2_t<F(Arg)>& d, Resul_t<F(Arg)> dx_acc = 1e-10)
{
   //std::cout << " using numeric first derivative " << std::endl;
   using tag_type = Get_tag_type<libmda::Remove_const<libmda::Remove_reference<Arg> > >;
   return ::detail::first_derivative_<tag_type>::apply(std::forward<F>  (f)
                                                     , std::forward<Arg>(x)
                                                     , d
                                                     , dx_acc);
}

#endif /* FIRST_DERIVATIVE_H_INCLUDED */
