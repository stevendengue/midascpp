#ifndef POLAK_RIBIERE_H_INCLUDED
#define POLAK_RIBIERE_H_INCLUDED

#include <iostream>
#include "typedefs.h"
#include "ncg.h"
#include "function_wrapper.h"

namespace detail
{

template<class Signature>
class polak_ribiere_impl: public Ncg<Signature,polak_ribiere_impl<Signature> >
{
   public:
      
      Resul_t<Signature> beta() const 
      {
         Resul_t<Signature> beta = dot(this->m_df_new,Argum2_t<Signature>(this->m_df_new-this->m_df))/dot(this->m_df,this->m_df);
         beta = std::max(beta,Resul_t<Signature>(0.0));
         return beta;
      }
   public:
      template<class... Ts>
      polak_ribiere_impl(Ts&&... ts): 
         Ncg<Signature,polak_ribiere_impl<Signature> >(std::forward<Ts>(ts)...) 
      { 
      }
};

} // namespace detail

template<class F, class Arg>
auto polak_ribiere(F&& f
                 , Arg&& arg
                 //, Ncg_args<F(Arg)>  ncg_args  = {}
                 //, Line_args<F(Arg)> line_args = {}
                 , Resul_t<F(Arg)> acc=1e-10
                 , bool summary=false
                 ) 
   -> decltype(wrap_the_function< ::detail::polak_ribiere_impl>
         (make_function_wrapper(std::forward<F>(f)),std::forward<Arg>(arg), {acc,summary},{}))
{
   return wrap_the_function< ::detail::polak_ribiere_impl>
      (make_function_wrapper(std::forward<F>(f)),std::forward<Arg>(arg), {acc,summary},{});
}

#endif /* POLAK_RIBIERE_H_INCLUDED */
