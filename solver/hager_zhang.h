/**
************************************************************************
* 
* @file    hager_zhang.h
*
* @date    11-01-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Implementation of the Hager-Zhang conjugate-gradient method.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef HAGER_ZHANG_H_INCLUDED
#define HAGER_ZHANG_H_INCLUDED

#include <iostream>
#include "typedefs.h"
#include "ncg.h"
#include "function_wrapper.h"

namespace detail
{

template <class Signature>
class hager_zhang_impl : public Ncg< Signature, hager_zhang_impl<Signature> >
{
   public:
      Resul_t<Signature> beta() const 
      {
         auto theta = Resul_t<Signature>(2.);  // theta=2 corresponds to CG_DESCENT. Other choices are possible.
         auto y_k = this->m_df_new-this->m_df;
         Resul_t<Signature> denom = dot(this->m_p, y_k);
         Resul_t<Signature> beta = dot( Argum2_t<Signature>(y_k - (theta*dot(y_k, y_k)/denom)*this->m_p), this->m_df_new ) / denom;
         beta = std::max(beta,Resul_t<Signature>(0.));

         return beta;
      }
   public:
      template<class... Ts>
      hager_zhang_impl(Ts&&... ts): 
         Ncg<Signature, hager_zhang_impl<Signature> >(std::forward<Ts>(ts)...) 
      { 
      }
};

} /* namespace detail */


template<class F, class Arg>
auto hager_zhang(F&& f
                , Arg&& arg
                //, Ncg_args<F(Arg)>  ncg_args  = {}
                //, Line_args<F(Arg)> line_args = {}
                , Resul_t<F(Arg)> acc=1e-10
                , bool summary=false
                ) 
   -> decltype(wrap_the_function< ::detail::hager_zhang_impl>
         (make_function_wrapper(std::forward<F>(f)),std::forward<Arg>(arg), {acc,summary},{}))
{
   return wrap_the_function< ::detail::hager_zhang_impl>
      (make_function_wrapper(std::forward<F>(f)),std::forward<Arg>(arg), {acc,summary},{});
}

#endif /* HAGER_ZHANG_H_INCLUDED */
