#ifndef POD_FUNCTIONS_H_INCLUDED
#define POD_FUNCTIONS_H_INCLUDED

// dot for doubles
double dot(double arg1, double arg2);

double norm(double arg);

#endif /* POD_FUNCTIONS_H_INCLUDED */
