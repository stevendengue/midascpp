#include"convergence.h"
#include<cmath>

bool converged(const double arg, const double acc)
{
   double abs_arg = fabs(arg);
   return libmda::numeric::float_leq(abs_arg,acc);
}

