#ifndef FUNCTION_WRAPPER_H_INCLUDED
#define FUNCTION_WRAPPER_H_INCLUDED

#include<utility>
#include"../libmda/util/warning.h"
#include"util.h" 
#include"first_derivative.h"

// comment in to get warning when using numerical derivatives
#define PRINT_NUMERICAL_WARNING

namespace detail
{
   /////////
   // use analytical first derivate given in function
   //
   /////////
   struct analytical_deriv
   {
      template<class F, class... Args>
      inline auto operator()(F&& f, Args&&... args) const
            -> decltype(std::declval<F>().first_deriv(std::declval<Args>()...))
      {
         return f.first_deriv(std::forward<Args>(args)...);
      }
   };
   
   //////
   // use numerical first derivative
   //
   //////
   struct numerical_deriv
   {
      struct WARNING_using_numerical_deriv {};

      template<class F, class... Args>
      inline auto operator()(F&& f, Args&&... args) const
            -> decltype(first_derivative(std::declval<F>(), std::declval<Args>()...))
      {
         #ifdef PRINT_NUMERICAL_WARNING
         libmda::util::print_warning(WARNING_using_numerical_deriv());
         #endif /* PRINT_NUMERICAL_WARNING */
         return first_derivative(std::forward<F>(f), std::forward<Args>(args)...);
      }
   };

   //////
   // derivative helper class, that switches between analytical and numerical 
   // first derivatives
   /////
   template<bool,class,class=void> 
   struct wrapper_deriv_help : numerical_deriv
   { 
   };

   template<class F, class... Args> 
   struct wrapper_deriv_help<true
                           , F(Args...)
                           , typename std::enable_if<type_sink<decltype(std::declval<F>().first_deriv(std::declval<Args>()...))>::value>::type
                           > : analytical_deriv 
   { 
   };

   ///////
   // indirection that calls the correct deriv_helper
   //
   ///////
   template<class F, class... Args>
   inline auto invoke_wrapper_deriv(F&& f, Args&&... args) 
         -> decltype(wrapper_deriv_help<std::is_class<typename std::remove_reference<F>::type>::value
                                      , F(Args...)
                                      >()(std::declval<F>(), std::declval<Args>()...))
   {
      return wrapper_deriv_help<std::is_class<typename std::remove_reference<F>::type>::value
                                            , F(Args...)
                                            >()(std::forward<F>(f), std::forward<Args>(args)...);
   }
} // namespace detail

///////
// function wrapper class that supplies evaluate and deriv
// if no deriv is given, function wrapper supplies a numerical one
//
///////
template<class F>
class function_wrapper
{
   public:
      template<class FF>
      function_wrapper(FF&& f): m_f(std::forward<FF>(f)) 
      {
      }
      
      template<class... Args>
      inline auto operator()(Args&&... args) const 
            -> decltype(std::declval<F>()(std::declval<Args>()...))
      {
         return m_f(std::forward<Args>(args)...);
      }
      
      template<class... Args>
      inline auto first_deriv(Args&&... args) const 
            -> decltype(::detail::invoke_wrapper_deriv(std::declval<F>(),std::declval<Args>()...))
      {
         return ::detail::invoke_wrapper_deriv(m_f, std::forward<Args>(args)...);
      }

   private:
      const F& m_f;
};


/////
// interface function for making function wrappers
//
/////
template<class F>
inline function_wrapper<F> make_function_wrapper(F&& f)
{
   return {std::forward<F>(f)};
}

#endif /* FUNCTION_WRAPPER_H_INCLUDED */
