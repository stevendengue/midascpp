#ifndef FUNC_LEXAMPLE_H_INCLUDED
#define FUNC_LEXAMPLE_H_INCLUDED

#include "../libmda/arrays/SDArray1D.h"
#include "../libmda/util/enforce.h"

///
// implements f(x,y) = x + y
//
// constraint:  x^2 + y^2 = 1
//
///

struct func_lexample
{
   using argum_t = midas::mmv::SDArray1D<double>;
   using value_t = double;
   using step_t  = double;

   double operator()(const argum_t& arg) const
   {
      return arg.at(0) + arg.at(1);
   }

   void first_deriv(const argum_t& arg, argum_t& der) const
   {
      der.at(0) = 1.0;
      der.at(1) = 1.0;
   }

   template<class D, libmda::Enforce_order<D,2> = 0>
   void second_deriv(const argum_t& arg, D& d) const
   {
      // no second derivative
      d(0,0) = 0.0; d(0,1) = 0.0;
      d(1,0) = 0.0; d(1,1) = 0.0;
   }
};

struct func_lexample_c
{
   using argum_t = midas::mmv::SDArray1D<double>;
   // x^2 + y^2 = 1
   double operator()(const argum_t& arg) const
   {
      return arg.at(0)*arg.at(0) + arg.at(1)*arg.at(1) - 1.0;
   }

   void first_deriv(const argum_t& arg, argum_t& der) const
   {
      der.at(0) = 2.0*arg.at(0);
      der.at(1) = 2.0*arg.at(1);
   }

   template<class L, class D>
   void sub_second_deriv(const argum_t& arg, L& l, D& d) const
   {
      std::cout << " HEREEEE !!!! " << std::endl;
      std::cout << " L =  " << l << std::endl;
      d(0,0) -= 2.0*l; d(1,1) -= 2.0*l;
   }
};

#endif  /* FUNC_LEXAMPLE_H_INCLUDED */
