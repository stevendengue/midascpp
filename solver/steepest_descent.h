#ifndef STEEPEST_DESCENT_H_INCLUDED
#define STEEPEST_DESCENT_H_INCLUDED

#include"typedefs.h"
#include"phi_func.h"
#include"convergence.h"
#include"function_wrapper.h"
#include"invoke_derivatives.h"

#include "libmda/numeric/optim/wolfe_line_search.h"

namespace detail
{

//////
// steepest descent algorithm
//
//////
template<class F, class Arg>
Argum2_t<F(Arg)> steepest_descent_(F&& f
                                 , Arg&& x_0
                                 , Resul_t<F(Arg)> acc=1e-10
                                 , const bool summary = false
                                 )
{
   Resul_t<F(Arg)> alpha_max = 100000;
   Argum2_t<F(Arg)> x = x_0;
   //Argum2_t<F(Arg)> p = -f.first_deriv(x);
   Argum2_t<F(Arg)> p = x; 
   invoke_first_deriv(f,x,p);
   p*=-1.0;
   //std::cout << -f.first_deriv(x) << std::endl;
   //std::cout << p << std::endl;

   unsigned int k = 0;
//   std::cout << p.FormatModeMatrices() << std::endl;
//   std::cout << converged(-p,acc) << std::endl;
   while( !converged(-p,acc) )
   {
      phi_func<F(Arg)> phi(f,x,p);
//      std::cout << "x:" << std::endl;
//      std::cout << x.FormatModeMatrices() << std::endl;
//      std::cout << "p:" << std::endl;
//      std::cout << p.FormatModeMatrices() << std::endl;
//      std::cout << "Phi(1.0)" << phi(1.0) << std::endl;
//      std::cout << "alpha max: " << alpha_max << std::endl;
      Resul_t<F(Arg)> alpha = libmda::numeric::optim::wolfe_line_search(phi, 0.0, alpha_max, 0.1, 1e-5, 0.05);
//      Resul_t<F(Arg)> alpha = wolfe_line_search(phi,10.,1e-5,0.05);
      
      x = x + alpha*p;
      
//      std::cout << "x new:" << std::endl;
//      std::cout << x.FormatModeMatrices() << std::endl;
//      exit(1);

      invoke_first_deriv(f,x,p);
      p *= -1.0;
      
      // print out
      if(summary)
      {
         std::cout << " Summary iteration " << k << "\n"
                   << " f(x) = " << f(x) << "\n"
                   << " step = " << alpha << "\n"
                   << " |x|  = " << norm(x) << "\n"
                   << " |p|  = " << norm(p) << "\n"
                   << std::endl;
      }
      
      k+=1;
//      exit(1);
   }
   
   return x;
}
} // namespace detail

template<class F, class Arg>
auto steepest_descent(F&& f
                    , Arg&& arg
                    , Resul_t<F(Arg)> acc=1e-10
                    , const bool summary = false
                    )
   -> decltype(::detail::steepest_descent_(make_function_wrapper(std::forward<F>(f)), std::forward<Arg>(arg), acc, summary))
{
   //std::cout << " her ! " << std::endl;
   //make_function_wrapper(std::forward<F>(f)).deriv(arg);
   //std::cout << " her ! after " << std::endl;
   return ::detail::steepest_descent_(make_function_wrapper(std::forward<F>(f)), std::forward<Arg>(arg), acc, summary);
}

#endif /* STEEPEST_DESCENT_H_INCLUDED */
