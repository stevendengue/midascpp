#ifndef LINEARCONVERGENCECHECKER_H_INCLUDED
#define LINEARCONVERGENCECHECKER_H_INCLUDED

#include "ConvergenceHolder.h"
#include "ResidualEpsilonHolder.h"
#include "SolutionEpsilonHolder.h"
#include "it_solver/IES.h"
#include "util/MidasStream.h"
#include "util/Io.h"

/**
 *
 **/
namespace detail
{

/**
 *
 **/
inline double SolutionDiffNorm
   ( const StandardContainer<ComplexVector<MidasVector,MidasVector> >& oldred
   , const StandardContainer<ComplexVector<MidasVector,MidasVector> >& red
   , int i_neq
   )
{
   // calculate diff norm
   double diff_norm = 0.0;
   if(i_neq < oldred.size())
   {
      for(size_t i = 0; i < oldred[i_neq].Re().Size(); ++i)
      {
         auto real_diff = (oldred[i_neq].Re()[i] - red[i_neq].Re()[i]);
         real_diff *= real_diff;

         auto imag_diff = (oldred[i_neq].Im()[i] - red[i_neq].Im()[i]);
         imag_diff *= imag_diff;

         diff_norm += real_diff + imag_diff;
      }
      
      for(size_t i = oldred[i_neq].Re().Size(); i < red[i_neq].Re().Size(); ++i)
      {
         diff_norm += red[i_neq].Re()[i]*red[i_neq].Re()[i];
      }
   }
   else
   {
      for(size_t i = 0; i < red[i_neq].Re().Size(); ++i)
      {
         auto real_norm = (red[i_neq].Re()[i]*red[i_neq].Re()[i]);
         auto imag_norm = (red[i_neq].Im()[i]*red[i_neq].Im()[i]);

         diff_norm += real_norm + imag_norm;
      }
   }
   
   // calculate new norm
   double new_norm  = 0.0;
   for(size_t i = 0; i < red[i_neq].Re().Size(); ++i)
   {
      auto real_norm = (red[i_neq].Re()[i]*red[i_neq].Re()[i]);
      auto imag_norm = (red[i_neq].Im()[i]*red[i_neq].Im()[i]);

      new_norm += real_norm + imag_norm;
   }

   return sqrt(diff_norm)/sqrt(new_norm);
}

/**
 *
 **/
class CheckIthLinearConvergence
{
   public:
      template<class V, class Y, class RED>
      bool Apply(V& residuals
               , size_t i_neq
               , Nb res_eps_abs
               , Nb res_eps_rel
               , Nb sol_eps
               , Y& y
               , RED& oldred
               , RED& red
               , MidasStream& os
               , bool relative_conv
               )
      {
         bool converged = true;
         auto residual_norm = IES_Norm(residuals[i_neq]);
         auto y_norm = IES_Norm(y[i_neq]);
         auto residual_rel_norm = residual_norm / y_norm;
         //auto solution_diff_norm = SolutionDiffNorm(oldred,red,i_neq);

         //
         // some output
         //
         midas::stream::ScopedPrecision(3, os);
         os << " Eq. " << i_neq << "   |r| = " << residual_norm
                                << "   |r|/|b| = " << residual_rel_norm;
                                  
         //bool residual_converged = relative_conv ? (residual_rel_norm <= res_eps) : (residual_norm <= res_eps);
         bool residual_converged = (residual_norm <= res_eps_abs);
         if(residual_converged) // check convergence for both eigval and residual
         {
            // if converged we show in output
            os << " C" << (relative_conv ? " (Rel)" : " (Abs)");
         }
         else 
         {
            converged = false;
         }
         os << "\n";
         
         return converged;
      }

};

/**
 *
 **/
class CheckIthComplexLinearConvergence
{
   public:
      template<class V, class Y, class RED>
      bool Apply(V& residuals
               , size_t i_neq
               , Nb res_eps_abs
               , Nb res_eps_rel
               //, Nb res_eps
               , Nb sol_eps
               , Y& y
               , RED& oldred
               , RED& red
               , MidasStream& os
               , bool relative_conv
               )
      {
         bool converged = true;
         auto residual_norm = IES_Norm(residuals[i_neq]);
         auto y_norm = IES_Norm(y[i_neq]);
         auto residual_rel_norm = residual_norm / y_norm;
         auto re_residual_norm = IES_Norm(residuals[i_neq].Re());
         auto im_residual_norm = IES_Norm(residuals[i_neq].Im());

         auto solution_diff_norm = SolutionDiffNorm(oldred,red,i_neq);
         auto solution_norm = IES_Norm(red[i_neq]);
         auto re_solution_norm = IES_Norm(red[i_neq].Re());
         auto im_solution_norm = IES_Norm(red[i_neq].Im());

         //Mout << std::endl <<  "DEBUG: The input absolute threshold should be equal to: " << res_eps_abs << std::endl;
         //Mout << "DEBUG: The input relative threshold should be equal to: " << res_eps_rel << std::endl;
         
         midas::stream::ScopedPrecision(3, os);
         os << " Eq. " << i_neq << "   |r| = " << residual_norm 
                                << "   |b| = " << y_norm
                                << "   |r|/|b| = " << residual_rel_norm
                                << "   |re^r|=" << re_residual_norm 
                                << "   |im^r|=" << im_residual_norm
                                << "   |x_new|=" << solution_norm
                                << "   re|x_new|=" << re_solution_norm
                                << "   im|x_new|=" << im_solution_norm
                                << "   |x_new-x_old|/|x_new|=" << solution_diff_norm;
         
         bool residual_converged_rel = (residual_rel_norm <= res_eps_rel);
         bool residual_converged_abs = (residual_norm <= res_eps_abs);
         //bool residual_converged = relative_conv ? (residual_rel_norm <= res_eps) : (residual_norm <= res_eps);
         bool solution_converged = (solution_diff_norm <= sol_eps);
         if((residual_converged_rel || residual_converged_abs) && solution_converged) // check convergence for both solution and residuals
         //if(residual_converged && solution_converged) // check convergence for both solution and residual
         {
            // if converged we show in output
            //os << " C" << (relative_conv ? " (Rel)" : " (Abs)");
            if (residual_converged_rel && residual_converged_abs)
            { 
               os << " C" << " (abs=C , rel=C)" ;
            }
            else
            {
               if (residual_converged_rel)
               { 
                  os << " C" << " (abs=notC , rel=C)" ;
               }
               else
               { 
                  os << " C" << " (abs=C , rel=notC)" ;
               }
            }

         }
         else 
         {
            converged = false;
         }
         os << "\n" << std::flush;
         
         return converged;
      }
};

} /* namespace detail */

template<class CONV, class A>
class LinearConvergenceCheckerBase: 
   public SolutionEpsilonHolder<
          ResidualEpsilonHolder<
          ConvergenceHolder<
            A
          > > >
 , private CONV
{
   private:
      //Nb mResidualEpsilon = 1e-10;
      //typename std::decay<decltype(std::declval<A>().self().ReducedSolution())>::type mSavedReduced;
      MAKE_VARIABLE(bool,RelativeConvergence,false);

   public:
      bool CheckConvergence()
      {
         bool converged = true;
         //for(size_t i_neq=0; i_neq<this->self().Neq(); ++i_neq)
         for(size_t i_neq=0; i_neq<this->self().Neq(); ++i_neq)
         {
            this->self().Converged()[i_neq] 
               = CONV::Apply(this->self().Residuals()
                           , i_neq
                           , this->self().ResidualEpsilon()
                           , this->self().ResidualEpsilonRel()
                           , this->self().SolutionEpsilon()
                           , this->self().Y()
                           , this->self().OldReducedSolution()
                           , this->self().ReducedSolution()
                           , this->self().Ostream()
                           , this->self().RelativeConvergence()
                           );

            if(!this->self().Converged()[i_neq])
            {
               converged = false;
            }
         }
         
         this->self().Ostream() << std::endl; // make line break for prettier output

         // save reduced
         //mSavedReduced = this->self().ReducedSolution();

         return converged;
      }

      void Output() const
      {
         A::Output();
         auto& os = A::Ostream();
         os << std::setw(30) << " Solution threshold "  << ": " << this->SolutionEpsilon() << "\n";
      }
};

template<class A>
using LinearConvergenceChecker = LinearConvergenceCheckerBase<detail::CheckIthLinearConvergence,A>;

template<class A>
using ComplexLinearConvergenceChecker = LinearConvergenceCheckerBase<detail::CheckIthComplexLinearConvergence,A>;

#endif /*  LINEARCONVERGENCECHECKER_H_INCLUDED */
