#ifndef REDUCEDTYPE_H_INCLUDED
#define REDUCEDTYPE_H_INCLUDED

#include "mmv/DataCont.h"
#include "vcc/TensorDataCont.h"
#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"

#include "it_solver/ComplexVector.h"
#include "it_solver/SolutionVectorContainer.h"

namespace detail
{
template<class T>
struct ReducedTypeImpl;

template<class T>
struct ReducedTypeImpl<GeneralDataCont<T> >
{
   using type = GeneralMidasMatrix<T>;
};

template<>
struct ReducedTypeImpl<TensorDataCont>
{
   using type = MidasMatrix;
};

template<class T, class U>
struct ReducedTypeImpl<ComplexVector<T,U> >
{
   using type = StandardContainer<ComplexVector<MidasVector,MidasVector> >;
};

} /* namespace detail */

template<class T>
using ReducedType = typename ::detail::ReducedTypeImpl<T>::type;

#endif /* REDUCEDTYPE_H_INCLUDED */
