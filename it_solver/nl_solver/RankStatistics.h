/**
************************************************************************
* 
* @file    RankStatistics.h
*
* @date    07-10-2016
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Class containing information on tensor ranks.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef RANKSTATISTICS_H_INCLUDED
#define RANKSTATISTICS_H_INCLUDED

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

template
   <  typename T
   >
class GeneralTensorDataCont;
using TensorDataCont = GeneralTensorDataCont<Nb>;

class ModeCombiOpRange;

/**
 * @brief
 *    Class containing statistics of tensor ranks.
 *
 * The struct contains rank information for each tensor order.
 * The stored info is:
 *
 **/
class RankStatistics
{
   private:
      //! Name of TensorDataCont
      std::string mName;

      //! Label for sets of data
      std::string mLabel = "Iteration";

      /** @name Data **/
      //!@{
      std::vector< std::vector<In> > mMaxRanks;
      std::vector< std::vector<In> > mMinRanks;
      std::vector< std::vector<Nb> > mAverageRanks;
      std::vector< std::vector<Nb> > mAverageRankDifferences;
      std::vector< std::vector<Nb> > mCompressionFactors;
      //!@}

   public:
      /** @name Default constructors and destructor **/
      //!@{
      RankStatistics() = delete;
      RankStatistics( const RankStatistics& ) = default;
      RankStatistics( RankStatistics&& ) = default;
      ~RankStatistics() = default;
      //!@}

      //! Custom constructor from name
      RankStatistics
         (  const std::string&
         );
      
      //! Reserve space in all data vectors
      void Reserve
         (  const size_t&
         );

      //! Emplace new rank info
      void EmplaceRankInfo
         (  const TensorDataCont&
         );

      //! Write info to file
      void WriteToDisc
         (  const std::string&
         )  const;

      //! Write all ranks to file
      void WriteAllRanksToDisc
         (  const TensorDataCont&
         ,  const std::string&
         )  const;

      //! Perform rank analysis
      void PerformRankAnalysis
         (  const TensorDataCont&
         ,  const ModeCombiOpRange&
         )  const;

      //! Set label
      void SetLabel
         (  const std::string& aLabel
         )
      {
         mLabel = aLabel;
      }

      //! Output statistics
      friend std::ostream& operator<<
         (  std::ostream&
         ,  const RankStatistics&
         );

      //! Default copy assignment
      RankStatistics& operator=( const RankStatistics& ) = default;

      //! Default move assignment
      RankStatistics& operator=( RankStatistics&& ) = default;
};


#endif /* RANKSTATISTICS_H_INCLUDED */
