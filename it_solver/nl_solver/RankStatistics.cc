/**
************************************************************************
* 
* @file    RankStatistics.cc
*
* @date    07-10-2016
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Implementing member functions of RankStatistics
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#include "it_solver/nl_solver/RankStatistics.h"
#include "util/read_write_binary.h"
#include "input/ModeCombiOpRange.h"
#include "tensor/CanonicalTensor.h"
#include "tensor/TensorSum.h"
#include "tensor/TensorDirectProduct.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/TensorDataCont.h"

#include "mpi/Impi.h"

/**
 * Construct RankStatistics from name.
 *
 * @param arName
 *    The name of the TensorDataCont to track
 **/
RankStatistics::RankStatistics
   (  const std::string& arName
   )
   :  mName(arName)
{
}


/**
 * Reserve space in all data vectors
 *
 * @param arCapacity
 *    The new capacity of all data vectors
 **/
void RankStatistics::Reserve
   (  const size_t& arCapacity
   )
{
   this->mMaxRanks.reserve( arCapacity );
   this->mMinRanks.reserve( arCapacity );
   this->mAverageRanks.reserve( arCapacity );
   this->mAverageRankDifferences.reserve( arCapacity );
   this->mCompressionFactors.reserve( arCapacity );
}


/**
 * Emplace current rank statistics to the data vectors.
 *
 * @param arTensors
 *    The TensorDataCont
 **/
void RankStatistics::EmplaceRankInfo
   (  const TensorDataCont& arTensors
   )
{
   this->mMaxRanks.emplace_back(arTensors.GetMaxRankVec());
   this->mMinRanks.emplace_back(arTensors.GetMinRankVec());
   this->mAverageRanks.emplace_back(arTensors.AverageRankVec());
   this->mAverageRankDifferences.emplace_back(arTensors.AverageRankDifferenceVec());
   this->mCompressionFactors.emplace_back(arTensors.DataCompressionFactors());
}

/**
 * Write RankInfo to file
 *
 * @param arName
 *    Name of the file
 **/
void RankStatistics::WriteToDisc
   (  const std::string& arName
   )  const
{
   midas::mpi::OFileStream output( arName, midas::mpi::OFileStream::StreamType::MPI_MASTER, std::ios::out | std::ios::binary );
//   output.exceptions( std::ifstream::failbit | std::ifstream::badbit );

   output << std::setprecision(3) << *this;

   output.close();
}

/**
 * Write all ranks to disc.
 *
 * @param arTdc
 *    TensorDataCont.
 * @param arName
 *    Name of file.
 **/
void RankStatistics::WriteAllRanksToDisc
   (  const TensorDataCont& arTdc
   ,  const std::string& arName
   )  const
{
   auto size = arTdc.Size();
   auto ranks = std::unique_ptr<In[]>(new In[size]);

   for(size_t i=0; i<size; ++i)
   {
      auto& tens = arTdc.GetModeCombiData(i);
      
      switch(tens.Type())
      {
         case BaseTensor<Nb>::typeID::CANONICAL:
         {
            ranks[i] = tens.StaticCast<CanonicalTensor<Nb>>().GetRank();
            break;
         }
         case BaseTensor<Nb>::typeID::SUM:
         {
            ranks[i] = tens.StaticCast<TensorSum<Nb>>().Rank();
            break;
         }
         case BaseTensor<Nb>::typeID::DIRPROD:
         {
            ranks[i] = tens.StaticCast<TensorDirectProduct<Nb>>().Rank();
            break;
         }
         case BaseTensor<Nb>::typeID::ZERO:
         {
            ranks[i] = I_0;
            break;
         }
         default:
         {
            MidasWarning(tens.ShowType() + " does not have a rank...");
            ranks[i] = -I_1;
            break;
         }
      }
   }

   midas::mpi::OFileStream output(arName, midas::mpi::OFileStream::StreamType::MPI_MASTER, std::ios::out | std::ios::binary);
   if (  size > 0
      )
   {
      // Use function from util/read_write_binary.h
      write_binary_array(ranks.get(), size, output);
   }
   else
   {
      MidasWarning("Cannot write ranks of TensorDataCont with size < 1.");
   }
   output.close();
}

/**
 * Perform analysis of ranks
 *
 * @param aTdc
 *    The TensorDataCont
 * @param aMcr
 *    The ModeCombiOpRange
 **/
void RankStatistics::PerformRankAnalysis
   (  const TensorDataCont& aTdc
   ,  const ModeCombiOpRange& aMcr
   )  const
{
   // Get size of TensorDataCont (number of tensors)
   auto size = aTdc.Size();

   // Output
   Mout  << " Performing full rank analysis of TensorDataCont of size: " << size << ":" << std::endl;

   // Get all ranks in one big vector (ordered like arMcr)
   auto ranks = aTdc.GetRanks();

   // Get ranks sorted by excitation level
   // ordered_ranks is a std::vector<std::vector<In>> where ranks[0] is a vector of ranks of singles excitations, ranks[1] is a vector of ranks of doubles excitations, etc.
   auto ordered_ranks = aTdc.GetRanksVec();

   size_t i = 0;
   for(const auto& mc : aMcr)
   {
      // Skip reference MC (the empty one)
      if (  mc.Size() == 0
         )
      {
         continue;
      }

      const auto& rank = ranks[i];

      // Playground for Bjørn...
      Mout <<mc<<"\n has rank:  "<<rank<< std::endl; 
      ++i;
   }

   // Print rank distribution
   auto maxrank = aTdc.GetMaxRank();
   In maxdim = aTdc.GetModeCombiData(size-I_1).NDim();

   std::vector<In> rank_dist(maxrank+1);
   std::vector<In> rank_dist_maxdim(maxrank+1);
   std::vector<In> rank_dist_nomatvec(maxrank+1);
   for(size_t j=0; j<size; ++j)
   {
      const auto& tens = aTdc.GetModeCombiData(j);
      In rank = I_0;

      switch(tens.Type())
      {
         case BaseTensor<Nb>::typeID::CANONICAL:
         {
            rank = tens.StaticCast<CanonicalTensor<Nb>>().GetRank();
            break;
         }
         case BaseTensor<Nb>::typeID::SUM:
         {
            rank = tens.StaticCast<TensorSum<Nb>>().Rank();
            break;
         }
         case BaseTensor<Nb>::typeID::DIRPROD:
         {
            rank = tens.StaticCast<TensorDirectProduct<Nb>>().Rank();
            break;
         }
         case BaseTensor<Nb>::typeID::ZERO:
         {
            rank = I_0;
            break;
         }
         default:
         {
            MidasWarning(tens.ShowType() + " does not have a rank...");
            rank = -I_1;
            break;
         }
      }

      if (  rank >= I_0
         )
      {
         ++rank_dist[rank];
         if (  tens.NDim() == maxdim
            )
         {
            ++rank_dist_maxdim[rank];
         }
         if (  tens.NDim() > 2
            )
         {
            ++rank_dist_nomatvec[rank];
         }
      }
   }

   // Print rank dist
   Mout  << " === Rank distribution of total solution vector === \n"
         << " Rank         Count\n"
         << std::flush;
   for(size_t k=0; k<rank_dist.size(); ++k)
   {
      Mout  << k << "        " << rank_dist[k] << std::endl;
   }

   Mout  << " === Rank distribution of highest-order tensors === \n"
         << " Rank         Count\n"
         << std::flush;
   for(size_t l=0; l<rank_dist_maxdim.size(); ++l)
   {
      Mout  << l << "        " << rank_dist_maxdim[l] << std::endl;
   }
   Mout  << " === Rank distribution without matrices and vectors === \n"
         << " Rank         Count\n"
         << std::flush;
   for(size_t m=0; m<rank_dist_nomatvec.size(); ++m)
   {
      Mout  << m << "        " << rank_dist_nomatvec[m] << std::endl;
   }
}

/**
 * Output operator for RankStatistics
 *
 * @param os
 *    The output stream
 * @param arRS
 *    The RankStatistics
 * @return
 *    The ostream reference
 **/
std::ostream& operator<<
   (  std::ostream& os
   ,  const RankStatistics& arRS
   )
{
   In width = 12;
   In width2 = 30;
   os << arRS.mName << ": \n"
        << std::setw(width) << arRS.mLabel << std::setw(width) << "Max rank"
        << std::setw(width) << "Min rank"  << std::setw(width) << "Ave. rank" 
        << std::setw(width) << "Ave. rank diff";
   os << std::setw(width2) << "Data compression (CP / Full)" << std::endl;

   for(In i_order = I_0; i_order < arRS.mMaxRanks.back().size(); ++i_order)
   {
      os << i_order+I_1 << ".-order tensors:" << std::endl;

      for(In i_iter = I_0; i_iter < arRS.mMaxRanks.size(); ++i_iter)
      {
         os << std::setw(width)  << i_iter+I_1
            << std::setw(width)  << arRS.mMaxRanks.at(i_iter).at(i_order) 
            << std::setw(width)  << arRS.mMinRanks.at(i_iter).at(i_order);
         os << std::setw(width)  << arRS.mAverageRanks.at(i_iter).at(i_order)  
            << std::setw(width)  << arRS.mAverageRankDifferences.at(i_iter).at(i_order);
         os << std::setw(width2) << arRS.mCompressionFactors.at(i_iter).at(i_order)
            << std::endl;
      }
   }
   os << std::endl;

   return os;
}
