#ifndef INITIALIZEITERATIVESOLVERFROMCALCDEF_H_INCLUDED 
#define INITIALIZEITERATIVESOLVERFROMCALCDEF_H_INCLUDED 

#include "util/Error.h"
#include "it_solver/IterativeEquationSolver.h"
#include "it_solver/expand/IterativeOrthogonalizer.h"
#include "it_solver/LinearEquation.h"

// Forward declarations
template
   <  typename T
   >
class GeneralTensorDataCont;

template<class ITEQSOL, class CALCDEF>
void InitializeIterativeOrthogonalizerFromRspCalcDef(ITEQSOL& iteqsol, const CALCDEF* const calcdef)
{
   iteqsol.SetOrthoMax(calcdef->GetRspOrthoMax());
   iteqsol.SetThreshOrthoDiscard(calcdef->GetRspThreshOrthoDiscard());
   iteqsol.SetThreshOrthoRetry(calcdef->GetRspThreshOrthoRetry());
}

template<class ITEQSOL, class CALCDEF>
void InitializeIterativeSolverFromRspCalcDef(ITEQSOL& iteqsol, const CALCDEF* const calcdef)
{
   // general
   iteqsol.SetResidualEpsilon(calcdef->GetRspItEqResidThr());
   iteqsol.SetResidualEpsilonRel(calcdef->GetRspItEqResidThrRel());
   iteqsol.SetMaxIter(calcdef->GetRspItEqMaxIter());
   iteqsol.BreakDim() = calcdef->GetRspRedBreakDim();
   iteqsol.SetCalcDef(calcdef);
   iteqsol.IoLevel() = calcdef->RspIoLevel();
}

template<class ITEQSOL, class CALCDEF>
void InitializeEigenvalueSolverFromRspCalcDef(ITEQSOL& iteqsol, const CALCDEF* const calcdef)
{
   //MidasAssert(dynamic_cast<const RspCalcDef* const>(calcdef),"not a rspcalcdef");
   
   InitializeIterativeSolverFromRspCalcDef(iteqsol,calcdef);
   InitializeIterativeOrthogonalizerFromRspCalcDef(iteqsol,calcdef);
   iteqsol.SetNeq(calcdef->GetRspNeig());
   iteqsol.SetEigenvalueEpsilon(calcdef->GetRspItEqEnerThr());
   iteqsol.SetSaveToDisc(calcdef->ItEqSaveToDisc());
   
   iteqsol.Initialize();
}

template<class ITEQSOL, class CALCDEF>
void InitializeTensorEigenvalueSolverFromRspCalcDef(ITEQSOL& iteqsol, const CALCDEF* const calcdef)
{
   InitializeEigenvalueSolverFromRspCalcDef(iteqsol, calcdef);
   iteqsol.SetDecompInfo(calcdef->GetVccRspDecompInfo());
   iteqsol.SetBalanceAfterPrecon(calcdef->TensorVccRspEigBalanceAfterPrecon());
   iteqsol.SetRecompAfterPrecon(calcdef->TensorVccRspEigRecompAfterPrecon());
   iteqsol.AbsPrecon() = calcdef->TensorVccRspEigAbsPrecon();
   iteqsol.FullRankAnalysis() = calcdef->FullRspRankAnalysis();
   iteqsol.Olsen() = calcdef->Olsen();
   iteqsol.ConvertEigvalUnits() = calcdef->TensorRspSolverConvertUnits();
   iteqsol.OrthoScreening() = calcdef->TensorRspOrthoScreening();
   iteqsol.HarmonicRayleighRitz() = calcdef->GetRspHarmonicRR();
   iteqsol.HarmonicRrShift() = calcdef->GetRspEnerShift();
   iteqsol.UseHarmonicRrShiftInPrecon() = calcdef->RspUseEnerShiftInPrecon();
   iteqsol.AdaptiveHarmonicRrShift() = calcdef->RspAdaptiveHarmonicRrShift();
   iteqsol.RefinedHarmonicRr() = calcdef->RspRefinedHarmonicRr();
   iteqsol.UseRayleighQuotientsAsEigvals() = calcdef->RspUseRayleighQuotientsAsEigvals();
}

template<class ITEQSOL, class CALCDEF>
void InitializeTargetingTensorEigenvalueSolverFromRspCalcDef(ITEQSOL& iteqsol, const CALCDEF* const calcdef, const std::vector<TensorDataCont>& targets)
{
   InitializeTensorEigenvalueSolverFromRspCalcDef(iteqsol, calcdef);
   iteqsol.Targets() = targets;
   iteqsol.PreDiagonalization() = calcdef->RspPreDiag();
   iteqsol.OverlapNMax() = calcdef->GetRspOverlapNMax();
   iteqsol.OverlapSumMin() = calcdef->GetRspOverlapSumMin();
   
   // Niels: We should perhaps choose this per input, but this is not done in the old solver, and the OVERLAPMIN keyword does something different...
//   iteqsol.OverlapMin() = calcdef->GetOverlapMin();
}

template<class ITEQSOL, class CALCDEF>
void InitializeLinearSolverFromRspCalcDef(ITEQSOL& iteqsol, const CALCDEF* const calcdef)
{
   //static_assert(is_linear_solver(iteqsol),"not a linear solver");
   //MidasAssert(dynamic_cast<const RspCalcDef* const>(calcdef),"not a rspcalcdef");
   
   InitializeIterativeSolverFromRspCalcDef(iteqsol,calcdef);
   InitializeIterativeOrthogonalizerFromRspCalcDef(iteqsol,calcdef);
   iteqsol.SetSolutionEpsilon(calcdef->GetRspItEqSolThr());
   
   iteqsol.Initialize();
}

template<class ITEQSOL, class CALCDEF>
void InitializeLinearSolverFromPreconRspCalcDef(ITEQSOL& iteqsol, const CALCDEF* const calcdef)
{
   //static_assert(is_linear_solver(iteqsol),"not a linear solver");
   //MidasAssert(dynamic_cast<const RspCalcDef* const>(calcdef),"not a rspcalcdef");
   
   InitializeIterativeSolverFromRspCalcDef(iteqsol,calcdef);
   InitializeIterativeOrthogonalizerFromRspCalcDef(iteqsol,calcdef);
   iteqsol.SetSolutionEpsilon(calcdef->GetRspPreconItEqSolThr());

   iteqsol.Initialize();
}


//template<class ITEQSOL1, class ITEQSOL2>
//void InitializeIterativeGeneral(ITEQSOL1& iteqsol1i, const ITEQSOL2& iteqsol2i)
//{
//   IterativeEquationSolver<ITEQSOL1>& iteqsol1 
//      = static_cast<IterativeEquationSolver<ITEQSOL1>& >(iteqsol1i);
//   
//   const IterativeEquationSolver<ITEQSOL2>& iteqsol2 
//      = static_cast<const IterativeEquationSolver<ITEQSOL2>& >(iteqsol2i);
//
//   iteqsol1.SetResidualEpsilon(iteqsol2.ResidualEpsilon());
//   iteqsol1.SetMaxIter(iteqsol2.MaxIter());
//   iteqsol1.BreakDim() = iteqsol2.BreakDim();
//}
//
//template<class ITEQSOL1, class ITEQSOL2>
//void InitializeIterativeOrthogonalizer(ITEQSOL1& iteqsol1, const ITEQSOL2& iteqsol2)
//{
//   iteqsol1.SetOrthoMax(iteqsol2.OrthoMax());
//   iteqsol1.SetThreshOrthoDiscard(iteqsol2.ThreshOrthoDiscard());
//   iteqsol1.SetThreshOrthoRetry(iteqsol2.ThreshOrthoRetry());
//}
//
//template<class ITEQSOL1, class ITEQSOL2>
//void InitializeIterativeSolver(ITEQSOL1& iteqsol1, const ITEQSOL2& iteqsol2)
//{
//   InitializeIterativeGeneral(iteqsol1,iteqsol2);
//   InitializeIterativeOrthogonalizer(iteqsol1,iteqsol2);
//}

// IT EQ CALC DEF INTERFACE
//inline void InitializeIterativeOrthogonalizer(IterativeOrthogonalizer& iteqsol, const ItEqCalcDef& calc_def)
//{
//   iteqsol.SetOrthoMax(calc_def.OrthoMax());
//   iteqsol.SetThreshOrthoDiscard(calc_def.OrthoDiscardThresh());
//   iteqsol.SetThreshOrthoRetry(calc_def.OrthoRetryThresh());
//}
//
//template<class ITEQSOL>
//void InitializeGeneralIterativeSolver(ITEQSOL& iteqsol, const ItEqCalcDef& calc_def)
//{
//   // general
//   iteqsol.SetResidualEpsilon(calc_def.ResidualThresh());
//   iteqsol.SetMaxIter(calc_def.MaxIter());
//}
//
//template<class ITEQSOL>
//void InitializeIterativeOrthogonalizer(ITEQSOL& iteqsol, const ItEqCalcDef& calc_def)
//{
//}
//
//template<class ITEQSOL>
//void InitializeLinearSolver(ITEQSOL& iteqsol, const ItEqCalcDef& calc_def)
//{
//   InitializeIterativeOrthogonalizer(iteqsol,calc_def);
//   InitializeGeneralIterativeSolver(iteqsol,calc_def);
//}

#endif /* INITIALIZEITERATIVESOLVERFROMCALCDEF_H_INCLUDED */
