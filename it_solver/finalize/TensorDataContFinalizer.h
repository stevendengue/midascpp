/**
************************************************************************
* 
* @file    TensorDataContFinalizer.h
*
* @date    11-8-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Finalizer for TensorDataContEigenvalueSolver
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef TENSORDATACONTFINALIZER_H_INCLUDED
#define TENSORDATACONTFINALIZER_H_INCLUDED

#include <fstream>
#include "it_solver/nl_solver/RankStatistics.h"
#include "it_solver/IES_Macros.h"

template<class A>
class TensorDataContFinalizer
   :  public A
{
   std::string mAnalysisName;

   MAKE_VARIABLE(bool, FullRankAnalysis, false);

   private:
      template<class TRIALS>
      void FinalizeImpl(TRIALS& trials)
      {
         const auto& decompinfo = this->self().DecompInfo();

         if (  !decompinfo.empty()
            )
         {
            const auto& mcr = this->self().Atrans().Vss().MCR();

            RankStatistics stat_before("Solution vector before recompression");
            stat_before.SetLabel("Equation");
            RankStatistics stat("Solution vector after recompression");
            stat.SetLabel("Equation");

            for(size_t i=0; i<this->self().Neq(); ++i)
            {
               auto& sol = this->self().GetSolVec()[i];

               auto ranks_before = sol.GetRanks();
               stat_before.EmplaceRankInfo(sol);

               Mout  << " == Recompressing solution vector of Eq. " << i << " == " << std::endl;
               sol.Decompose(decompinfo, static_cast<Nb>(1.e-7));

               auto ranks = sol.GetRanks();
               stat.EmplaceRankInfo(sol);

               if (  this->mFullRankAnalysis
                  )
               {
                  Mout  << " == Performing rank analysis == " << std::endl;
                  size_t j = 0;
                  for(const auto& mc : mcr)
                  {
                     if (  mc.Size() == 0
                        )
                     {
                        continue;
                     }
                     Mout  << mc << "\n" 
                           << " Rank before recompression: " << ranks_before[j] << "\n"
                           << " Rank after recompression:  " << ranks[j] << "\n"
                           << std::endl;

                     ++j;
                  }
               }
            }
            {
               Mout << std::fixed;
               midas::stream::ScopedPrecision(3, Mout);
               Mout  << stat_before << std::endl;
               Mout  << std::endl;
               Mout  << stat << std::endl;
               Mout << std::scientific;
            }
         }
      }

   public:
      void Finalize()
      {
         FinalizeImpl(this->self().Trials());
      }

      void SetAnalysisName(const std::string& aAnalysisName)
      {
         mAnalysisName = aAnalysisName;
      }
};

#endif /* TENSORDATACONTFINALIZER_H_INCLUDED */
