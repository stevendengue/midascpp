#ifndef REDUCEDLINEAREQUAITION_H_INCLUDED
#define REDUCEDLINEAREQUAITION_H_INCLUDED

#include <iostream>

#include "inc_gen/Warnings.h"
#include "util/MidasStream.h"
#include "ReducedEquationBase.h"
#include "lapack_interface/ILapack.h"
#include "lapack_interface/LapackInterface.h"
#include "CheckForComplexEigenvalues.h"
#include "DEBUG.h"
#include "AddToReducedMatrixable.h"
#include "Linearsolvable.h"
#include "MatrixTag.h"

extern MidasStream Mout;

/**
 * Reduced linear equation (RLE)
 * AX = Y
 **/
template<class A
       , class Y
       , TransformerType TT
       , class REDUCED = ReducedEquation<TT
                                       , Linearsolvable<TT,PROBLEM_TYPE::PROBLEM_TYPE_STD> 
                                       > 
       >
class RLE_Impl: public REDUCED, private AddToReducedMatrixableY
{

   private:
      A mA;
      Y mY;
  
   public:
      explicit RLE_Impl(): REDUCED(), mA()
      {
      }
      
      /**
       * interface to grow the reduced matricies
       **/
      template<class T, class U>
      void AddToReducedMatrix(T& trials, U& trans, matrix_tag::MatA_t)
      {
         this->AddToReducedMatrixImpl(trials,trans,mA);
      }
      
      template<class T, class U>
      void AddToReducedMatrix(T& trials, U& y, matrix_tag::MatY_t)
      {
         this->AddToReducedMatrixYImpl(trials,y,mY);
      }
      
      /**
       * Evaluate interface
       **/
      template<class T>
      void Evaluate(T& aSolution)
      {
         this->LinearSolution(mA,mY,aSolution);
      }
      
      /**
       * clear 
       **/
      void ClearReducedSolution()
      {
         mA.SetNewSize(I_0,I_0);
         mY.SetNewSize(I_0,I_0);
      }
};

template<class A, class Y>
class RFSLE_Base: public AddToReducedMatrixableY
{
   public:
      template<class FRQ>
      A MakeTempA(const A& aA, size_t idx, const FRQ& frq) const
      {
         A temp_a(aA);
         for(size_t i = 0; i<temp_a.Ncols(); ++i)
         {
            temp_a[i][i]-=frq[idx];
         }
         return temp_a;
      }

      Y MakeTempY(const Y& aY, size_t idx) const
      {
         Y temp_y(aY.Nrows(),1);
         for(size_t i=0; i<temp_y.Nrows(); ++i)
         {
            temp_y[i][0] = aY[i][idx];
         }
         return temp_y;
      }

      template<class T>
      void LoadSolution(size_t idx, const T& temp_sol, T& sol)
      {
         for(size_t i=0; i<temp_sol.Nrows(); ++i)
         {
            sol[i][idx] = temp_sol[i][0];
         }
      }
}; 

/**
 * Reduced "frequency shifted" linear equation (RFSLE)
 * AX = B
 **/
template<class A
       , class Y
       , TransformerType TT
       , class REDUCED = ReducedEquation<TT
                                       , Linearsolvable<TT,PROBLEM_TYPE::PROBLEM_TYPE_STD> 
                                       > 
       >
class RFSLE_Impl: public REDUCED, private RFSLE_Base<A,Y>
{
   using Base = RFSLE_Base<A,Y>;
   private:
      A mA;
      Y mY;
  
   public:
      explicit RFSLE_Impl(): REDUCED(), mA()
      {
      }
      
      /**
       * interface to grow the reduced matricies
       **/
      template<class T, class U>
      void AddToReducedMatrix(T& trials, U& trans, matrix_tag::MatA_t)
      {
         this->AddToReducedMatrixImpl(trials,trans,mA);
      }
      
      template<class T, class U>
      void AddToReducedMatrix(T& trials, U& y, matrix_tag::MatY_t)
      {
         this->AddToReducedMatrixYImpl(trials,y,mY);
      }
      
      /**
       * Evaluate interface
       **/
      template<class T, class TRANS>
      void Evaluate(T& aSolution, const TRANS& trans)
      {
         auto frq = trans.Frequencies();

         assert(frq.Size() == mY.Ncols()); // assert we have correct dimensions
         
         if(CheckForComplexEigenvalues(mA))
         {
            Mout << " ** Warning  ** Reduced matrix has complex eigenvalues! " << std::endl;
            MidasWarning("** Warning  ** Reduced matrix has complex eigenvalues!");
         }

         aSolution.SetNewSize(mY.Nrows(),mY.Ncols());
         for(size_t frq_idx = 0; frq_idx<frq.Size(); ++frq_idx)
         {
            auto temp_a = Base::MakeTempA(mA,frq_idx,frq);
            auto temp_y = Base::MakeTempY(mY,frq_idx);
            T temp_solution(mY.Nrows(),1);
            this->LinearSolution(temp_a,temp_y,temp_solution);
            Base::LoadSolution(frq_idx,temp_solution,aSolution);
         }
      }
      
      /**
       * clear 
       **/
      void ClearReducedSolution()
      {
         mA.SetNewSize(0,0);
         mY.SetNewSize(0,0);
      }
};

/**
 * reduced individually freq shifted linear equation
 **/
template<class A
       , class Y
       , TransformerType TT
       , class REDUCED = ReducedEquation<TT
                                       , Linearsolvable<TT,PROBLEM_TYPE::PROBLEM_TYPE_STD> 
                                       > 
       >
class RIFSLE_Impl: public REDUCED, private RFSLE_Base<A,Y>
{
   using Base = RFSLE_Base<A,Y>;
   
   private:
      A mA;
      Y mY;
  
   public:
      explicit RIFSLE_Impl(): REDUCED(), mA()
      {
      }
      
      /**
       * interface to grow the reduced matricies
       **/
      template<class T, class U>
      void AddToReducedMatrix(T& trials, U& trans, matrix_tag::MatA_t)
      {
         this->AddToReducedMatrixImpl(trials,trans,mA);
      }
      
      template<class T, class U>
      void AddToReducedMatrix(T& trials, U& y, matrix_tag::MatY_t)
      {
         this->AddToReducedMatrixYImpl(trials,y,mY);
      }
      
      /**
       * Evaluate interface
       **/
      template<class T, class TRANS>
      void Evaluate(T& aSolution, const TRANS& trans)
      {
         auto frq = trans.Frequencies();

         if(CheckForComplexEigenvalues(mA))
         {
            Mout << " ** Warning  ** Reduced matrix has complex eigenvalues! " << std::endl;
            MidasWarning("** Warning  ** Reduced matrix has complex eigenvalues!");
         }

         aSolution.SetNewSize(mY.Nrows(),mY.Ncols()*frq.Size());
         for(size_t y_idx = 0; y_idx<mY.Ncols(); ++y_idx)
         {
            for(size_t frq_idx = 0; frq_idx<frq.Size(); ++frq_idx)
            {
               auto temp_a = Base::MakeTempA(mA,frq_idx,frq);
               auto temp_y = Base::MakeTempY(mY,y_idx);
               T temp_solution(mY.Nrows(),1);
               this->LinearSolution(temp_a,temp_y,temp_solution);
               Base::LoadSolution( (y_idx*frq.Size() + frq_idx), temp_solution, aSolution);
            }
         }
      }
      
      /**
       * clear 
       **/
      void ClearReducedSolution()
      {
         mA.SetNewSize(0,0);
         mY.SetNewSize(0,0);
      }
};


template<class A
       , class Y
       , TransformerType TT
       , class REDUCED = ReducedEquation<TT
                                       , Linearsolvable<TT,PROBLEM_TYPE::PROBLEM_TYPE_STD> 
                                       > 
       >
class RCFSLE_Impl: public REDUCED, private AddToReducedMatrixableComplexY
{
   //using Base = RFSLE_Base<A,Y>;

   private:
      A mA;
      Y mY;
  
   public:
      explicit RCFSLE_Impl(): REDUCED(), mA()
      {
      }
      
      /**
       * interface to grow the reduced matricies
       **/
      template<class T, class U>
      void AddToReducedMatrix(T& trials, U& trans, matrix_tag::MatA_t)
      {
         this->AddToReducedMatrixImpl(trials,trans,mA);
      }
      
      template<class T, class U>
      void AddToReducedMatrix(T& trials, U& y, matrix_tag::MatY_t)
      {
         this->AddToReducedMatrixComplexYImpl(trials,y,mY);
      }
   
      template<class TRANS>
      A MakeTempA(const A& a, size_t frq_idx, const TRANS& trans) const
      {
         A temp_a(2*a.Nrows(), 2*a.Ncols(), C_0);
         // make "diagonal"
         for(size_t i=0; i<a.Nrows(); ++i)
         {
            for(size_t j=0; j<a.Ncols(); ++j)
            {
               temp_a[i][j] = (i==j ? mA[i][j] - trans.ReFrequency(frq_idx) : mA[i][j]);
               temp_a[i+a.Nrows()][j+a.Ncols()] = -temp_a[i][j];
            }
         }
         
         // make "off diagonal"
         for(size_t i=a.Nrows(); i<2*a.Nrows(); ++i)
         {
            for(size_t j=0; j<a.Ncols(); ++j)
            {
               if((i-a.Nrows())==j)
               {
                  temp_a[i][j] = trans.ImFrequency(frq_idx);
                  temp_a[j][i] = temp_a[i][j];
               }
            }
         }


         return temp_a;
      }
      
      Y MakeTempY(const Y& aY, size_t frq_idx) const
      {
         Y temp_y(aY.Nrows(),I_1);
         for(size_t i=0; i<temp_y.Nrows(); ++i)
         {
            temp_y[i][0] = aY[i][frq_idx];
         }
         return temp_y;
      }

      template<class T>
      void LoadSolution(size_t idx, const Y& temp_sol, T& sol) const
      {
         size_t size = temp_sol.Nrows()/2;
         sol.emplace_back();
         sol.back().Re().SetNewSize(size);
         sol.back().Im().SetNewSize(size);
         for(size_t i=0; i<size; ++i)
         {
            sol[idx].Re()[i] = temp_sol[i][0];
            sol[idx].Im()[i] = temp_sol[i+size][0];
         }
      }

      /**
       * Evaluate interface
       **/
      template<class T, class TRANS>
      void Evaluate(T& aSolution, const TRANS& aTrans)
      {
         //assert(frq.Size() == mY.Ncols()); // assert we have correct dimensions
         //
         aSolution.clear(); // clear old solution

         if(CheckForComplexEigenvalues(mA))
         {
            Mout << " ** Warning  ** Reduced matrix has complex eigenvalues! " << std::endl;
            MidasWarning("** Warning  ** Reduced matrix has complex eigenvalues!");
         }

         for(size_t frq_idx = 0; frq_idx < aTrans.NFrequencies(); ++frq_idx)
         {
            auto temp_a = MakeTempA(mA,frq_idx,aTrans);
            auto temp_y = MakeTempY(mY,frq_idx);
            //Mout << " temp a: " << temp_a << std::endl;
            //Mout << " temp y: " << temp_y << std::endl;
            decltype(temp_y) temp_solution(mY.Nrows(),I_1);
            this->LinearSolution(temp_a,temp_y,temp_solution);
            LoadSolution(frq_idx,temp_solution,aSolution);
            //Mout << " TEMP SOL: " << temp_solution << std::endl;
            //Mout << " SOLUTION: " << std::endl;
            //Mout << aSolution << std::endl;
         }
      }
      
      /**
       * clear 
       **/
      void ClearReducedSolution()
      {
         //Mout << " ClearReducedSolution! " << std::endl;
         mA.SetNewSize(I_0,I_0);
         mY.SetNewSize(I_0,I_0);
      }
};



// interface
template<class A, class Y>
using ReducedNonHermitianLinearEquation = RLE_Impl<A,Y,TransformerType::TRANS_NHERM>;
template<class A, class Y>
using ReducedHermitianLinearEquation = RLE_Impl<A,Y,TransformerType::TRANS_HERM>;

//
template<class A, class Y>
using ReducedNonHermitianFreqShiftedLinearEquation = RFSLE_Impl<A,Y,TransformerType::TRANS_NHERM>;
template<class A, class Y>
using ReducedHermitianFreqShiftedLinearEquation = RFSLE_Impl<A,Y,TransformerType::TRANS_HERM>;

//
template<class A, class Y>
using ReducedNonHermitianIndividuallyFreqShiftedLinearEquation = RIFSLE_Impl<A,Y,TransformerType::TRANS_NHERM>;
template<class A, class Y>
using ReducedHermitianIndividuallyFreqShiftedLinearEquation = RIFSLE_Impl<A,Y,TransformerType::TRANS_HERM>;

//
template<class A, class Y>
using ReducedNonHermitianComplexFreqShiftedLinearEquation = RCFSLE_Impl<A,Y,TransformerType::TRANS_NHERM>;
template<class A, class Y>
using ReducedHermititanComplexFreqShiftedLinearEquation = RCFSLE_Impl<A,Y,TransformerType::TRANS_HERM>;

#endif /* REDUCEDLINEAREQUAITION_H_INCLUDED */
