#ifndef REDUCED_DEBUG_H_INCLUDED
#define REDUCED_DEBUG_H_INCLUDED

#include "libmda/numeric/float_eq.h"
#include "mmv/MidasVector.h"
#include "it_solver/EigenvectorContainer.h"
#include "it_solver/EigenvalueContainer.h"

template<class AA, class T, class U>
void DEBUG_CheckEigenSolution(const AA& a
                            , const EigenvectorContainer<GeneralMidasMatrix<T>,GeneralMidasMatrix<T> >& eigvecs
                            , const EigenvalueContainer<GeneralMidasVector<U>,GeneralMidasVector<U> >& eigvals)
{
   std::cout << __FILE__ << " " << __LINE__ << " not doing anything " << std::endl;
}

template<class AA, class T, class U>
void DEBUG_CheckEigenSolution(const AA& a, const GeneralMidasMatrix<T>& eigvecs
                            , const GeneralMidasVector<U>& eigvals)
{
   for(size_t i=0; i<eigvecs.Ncols(); ++i)
   {
      MidasVector trans_eigvec(eigvecs.Nrows());
      MidasVector diff_eigvec(eigvecs.Nrows());
      MidasVector eigvec(eigvecs.Nrows());
      eigvecs.GetCol(eigvec,i);

      trans_eigvec = a*eigvec;
      
      for(size_t j=0; j<eigvec.Size(); ++j)
         diff_eigvec[j] = trans_eigvec[j] - eigvals[i]*eigvec[j];
      auto diff_norm = diff_eigvec.Norm();
      
      if((diff_norm/eigvec.Norm()) > 1e-12)
      {
         Mout << " Diff norm : " << diff_norm << std::endl;
         MIDASERROR("Eigensolutions are bad!");
      }
   }
}

template<class T>
void DEBUG_CheckNoImagEigenvalues(Eigenvalue_ext_struct<T>& eig_sol)
{
   for(size_t i=0; i<eig_sol.num_eigval_; ++i)
      if(eig_sol.im_eigenvalues_[i] != C_0)
      {
         Mout << " imag part = " << eig_sol.im_eigenvalues_[i] << std::endl;
         MIDASERROR("IMAG EIGVALS... I DO NOT HANDLE THIS YET");
      }
}

template<class T>
void DEBUG_CheckEigenvectorOrthonormality(const GeneralMidasMatrix<T>& eigvecs)
{
   GeneralMidasVector<T> eigvec_i(eigvecs.Nrows());
   GeneralMidasVector<T> eigvec_j(eigvecs.Nrows());

   for(In i=0; i<eigvecs.Ncols(); ++i)
   {
      eigvecs.GetCol(eigvec_i,i);
      if(libmda::numeric::float_neq(eigvec_i.Norm(),1.0,8))
      {
         Mout << " Norm: " << eigvec_i.Norm() << std::endl;
         MIDASERROR("Norm not 1");
      }
      for(In j=i+1; j<eigvecs.Ncols(); ++j)
      {
         eigvecs.GetCol(eigvec_j,j);
         if(libmda::numeric::float_neq(Dot(eigvec_i,eigvec_j),0.0,8))
         {
            Mout << "Dot: " << Dot(eigvec_i,eigvec_j) << std::endl;
            MIDASERROR("Dot not 0");
         }
      }
   }  
}

template<class T>
void DEBUG_CheckEigenvectorOrthonormality(const EigenvectorContainer<GeneralMidasMatrix<T>, GeneralMidasMatrix<T> >& eigvecs)
{
   GeneralMidasVector<T> eigvec_i(eigvecs.Lhs().Nrows());
   GeneralMidasVector<T> eigvec_j(eigvecs.Rhs().Nrows());

   for(In i=0; i<eigvecs.Lhs().Ncols(); ++i)
   {
      eigvecs.Lhs().GetCol(eigvec_i,i);
      eigvecs.Rhs().GetCol(eigvec_j,i);
      if(libmda::numeric::float_neq(Dot(eigvec_i,eigvec_j),1.0,8))
      {
         Mout << " BiNorm: " << Dot(eigvec_i,eigvec_j) << std::endl;
         MIDASERROR("BiNorm not 1");
      }
      for(In j=i+1; j<eigvecs.Rhs().Ncols(); ++j)
      {
         eigvecs.Rhs().GetCol(eigvec_j,j);
         if(libmda::numeric::float_neq(Dot(eigvec_i,eigvec_j),0.0,8))
         {
            Mout << "Dot: " << Dot(eigvec_i,eigvec_j) << std::endl;
            MIDASERROR("Dot not 0");
         }
      }
   }  
}

#endif /* REDUCED_DEBUG_H_INCLUDED */
