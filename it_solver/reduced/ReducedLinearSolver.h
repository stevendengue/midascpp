#ifndef REDUCEDLINEARSOLVER_H_INCLUDED
#define REDUCEDLINEARSOLVER_H_INCLUDED

#include "TransformImplementation.h"
#include "MatrixTag.h"

//template<class T>
//void swap(T& a, T& b)
//{
//   T temp {a};
//   a = b;
//   b = temp;
//}

template<class REDUCED, class A>
class ReducedLinearSolver:
   public A
 , private REDUCED
 , protected detail::TransformImplementation
{
   protected:
      using REDUCEDEQ = REDUCED;
   
   public:
      void DoReducedSolution()
      {
         //Mout << " Doing reduced linear solutions " << std::endl;
         //std::cout << " Doing reduced linear solutions " << std::endl;
         this->self().DoTransforms();
         REDUCED::AddToReducedMatrix(this->self().Trials(),this->self().Sigma(),matrix_tag::MatA);
         REDUCED::AddToReducedMatrix(this->self().Trials(),this->self().Y(),matrix_tag::MatY);
         std::swap(this->self().OldReducedSolution(),this->self().ReducedSolution()); // save reduced solution
//         Mout << "Sigma.size() before evaluate = " << this->self().Sigma().size() << std::endl;
//         Mout << "Sigma.back().Size() before evaluate = " << this->self().Sigma().back().Size() << std::endl;
//         Mout << "Sigma.back() after solution swap \n" << this->self().Sigma().back() << std::endl;
         REDUCED::Evaluate(this->self().ReducedSolution());
//         Mout << "Sigma.size() after evaluate = " << this->self().Sigma().size() << std::endl;
//         Mout << "Sigma.back().Size() after evaluate = " << this->self().Sigma().back().Size() << std::endl;
//         Mout << "Sigma.back() after evaluate\n" << this->self().Sigma().back() << std::endl;
         //SortSolution(this->self().Eigenvalues(),this->self().ReducedEigenvectors());
      }

      void ClearSolver()
      {
//         Mout << " clear Reduced solver " << std::endl;
         REDUCED::ClearReducedSolution();
//         Mout << " after clearance " << std::endl;
         A::ClearSolver();
      }
};

template<class REDUCED, class A>
class ReducedFreqShiftedLinearSolver:
   public A
 , private REDUCED
 , protected detail::TransformImplementation
{
   protected:
      using REDUCEDEQ = REDUCED;
   
   public:
      void DoReducedSolution()
      {
         //Mout << " ~~~ Reduced linear solutions ~~~ " << std::endl;
         //std::cout << " Doing reduced linear solutions " << std::endl;
         this->self().DoTransforms();
         REDUCED::AddToReducedMatrix(this->self().Trials(),this->self().Sigma(),matrix_tag::MatA);
         REDUCED::AddToReducedMatrix(this->self().Trials(),this->self().Y(),matrix_tag::MatY);
         //this->self().OldReducedSolution() = this->self().ReducedSolution(); // save reduced solution
         std::swap(this->self().OldReducedSolution(),this->self().ReducedSolution()); // save reduced solution
         REDUCED::Evaluate(this->self().ReducedSolution(),this->self().Atrans());
         //SortSolution(this->self().Eigenvalues(),this->self().ReducedEigenvectors());
      }
      
      void ClearSolver()
      {
         //Mout << " clear Reduced solver " << std::endl;
         REDUCED::ClearReducedSolution();
         //Mout << " after clearance " << std::endl;
         A::ClearSolver();
      }
};

#endif /* REDUCEDLINEARSOLVER_H_INCLUDED */
