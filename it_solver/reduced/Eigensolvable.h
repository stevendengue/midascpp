#ifndef EIGENSOLVABLE_H_INCLUDED
#define EIGENSOLVABLE_H_INCLUDED

#include "it_solver/reduced/ProblemType.h"
#include "lapack_interface/ILapack.h"
#include "lapack_interface/SetMidasMatrixFromColMajPtr.h"
#include "lapack_interface/LapackInterface.h"
#include "it_solver/EigenvectorContainer.h"
#include "it_solver/EigenvalueContainer.h"


class Eigenloadable
{
   private:
      /**
       * helper function to load eigenvectors
       **/
      template<class T, class U>
      void SetReducedEigVecsFromEigSol(GeneralMidasMatrix<T>& aReducedEigVecs, Eigenvalue_struct<U>& aEigSol, ilapack::row_major_t)
      {
         SetMidasMatrixFromColumnMajorPtr(aReducedEigVecs,aEigSol._eigenvectors,aEigSol._n,aEigSol._num_eigval);
      }
      
      template<class T, class U>
      void SetReducedEigVecsFromEigSol(GeneralMidasMatrix<T>& aReducedEigVecs, Eigenvalue_ext_struct<U>& aEigSol, ilapack::row_major_t)
      {
         SetMidasMatrixFromColumnMajorPtr(aReducedEigVecs,aEigSol.rhs_eigenvectors_,aEigSol.n_,aEigSol.num_eigval_);
      }
      
      template<class T, class U>
      void SetReducedEigVecsFromEigSol(EigenvectorContainer<GeneralMidasMatrix<T>,GeneralMidasMatrix<T> >& aReducedEigVecs
                                     , Eigenvalue_ext_struct<U>& aEigSol, ilapack::row_major_t)
      {
         SetMidasMatrixFromColumnMajorPtr(aReducedEigVecs.Rhs(),aEigSol.rhs_eigenvectors_,aEigSol.n_,aEigSol.num_eigval_);
         SetMidasMatrixFromColumnMajorPtr(aReducedEigVecs.Lhs(),aEigSol.lhs_eigenvectors_,aEigSol.n_,aEigSol.num_eigval_);
      }
      
      template<class T, class U>
      void SetReducedEigValsFromEigSol(GeneralMidasVector<T>& aReducedEigVals, Eigenvalue_struct<U>& eig_sol)
      {
         aReducedEigVals.SetData(eig_sol._eigenvalues,eig_sol._num_eigval,eig_sol._num_eigval);
      }
      
      template<class T, class U>
      void SetReducedEigValsFromEigSol(GeneralMidasVector<T>& aReducedEigVals, Eigenvalue_ext_struct<U>& eig_sol)
      {
         aReducedEigVals.SetData(eig_sol.re_eigenvalues_,eig_sol.num_eigval_,eig_sol.num_eigval_);
      }
      
      template<class T, class U>
      void SetReducedEigValsFromEigSol(EigenvalueContainer<GeneralMidasVector<T>,GeneralMidasVector<T> >& aReducedEigVals
                                     , Eigenvalue_ext_struct<U>& eig_sol)
      {
         aReducedEigVals.Re().SetData(eig_sol.re_eigenvalues_,eig_sol.num_eigval_,eig_sol.num_eigval_);
         aReducedEigVals.Im().SetData(eig_sol.im_eigenvalues_,eig_sol.num_eigval_,eig_sol.num_eigval_);
      }
   protected:
      
      /**
       * load eigenvalues and eigenvectors from Eigenvalue_struct
       **/
      template<class T, class U, class E, class TAG>
      void LoadEigenvalueData(T& aReducedEigVecs, U& aReducedEigVals, E& eig_sol, TAG t)
      {
         SetReducedEigValsFromEigSol(aReducedEigVals,eig_sol);
         SetReducedEigVecsFromEigSol(aReducedEigVecs,eig_sol,t);
      }
};

template<TransformerType, PROBLEM_TYPE>
class Eigensolvable;

template<>
class Eigensolvable<TransformerType::TRANS_NHERM, PROBLEM_TYPE::PROBLEM_TYPE_STD>: 
   protected Eigenloadable
{
   protected:
      template<class MAT, class T, class U>
      void EigenSolution(const MAT& aA, T& aReducedEigVecs, U& aReducedEigVals, ilapack::iDGEEV_t)
      {
         auto eig_sol = GEEV(aA);
         Eigenloadable::LoadEigenvalueData(aReducedEigVecs,aReducedEigVals,eig_sol,ilapack::row_major);
      }
};

template<>
class Eigensolvable<TransformerType::TRANS_HERM, PROBLEM_TYPE::PROBLEM_TYPE_STD>
   :  public Eigensolvable<TransformerType::TRANS_NHERM, PROBLEM_TYPE::PROBLEM_TYPE_STD> // symmetric can also be solved generally
{
   protected:
      template<class MAT, class T, class U>
      void EigenSolution(const MAT& aA, T& aReducedEigVecs, U& aReducedEigVals, ilapack::iDSYEVD_t)
      {
         auto eig_sol = SYEVD(aA);
         Eigenloadable::LoadEigenvalueData(aReducedEigVecs,aReducedEigVals,eig_sol,ilapack::row_major);
      }

      using Eigensolvable<TransformerType::TRANS_NHERM, PROBLEM_TYPE::PROBLEM_TYPE_STD>::EigenSolution;
};

template<>
class Eigensolvable<TransformerType::TRANS_NHERM, PROBLEM_TYPE::PROBLEM_TYPE_GEN>: 
   protected Eigenloadable
{
   protected:
      template<class MATA, class MATB, class T, class U>
      //void EigenSolution(const MATA& aA, const MATB& aB, T& aReducedEigVecs, U& aReducedEigVals, ilapack::iDGGEV_t)
      void EigenSolution(const MATA& aA, const MATB& aB, T& aReducedEigVecs, U& aReducedEigVals)
      {
         auto eig_sol = GGEV(aA,aB);
         NormalizeEigenvectors(eig_sol,ilapack::normalize_rhs); // DGGEV returns non-normalized eigenvectors
         Eigenloadable::LoadEigenvalueData(aReducedEigVecs,aReducedEigVals,eig_sol,ilapack::row_major);
      }
};
 
template<>
class Eigensolvable<TransformerType::TRANS_HERM, PROBLEM_TYPE::PROBLEM_TYPE_GEN>
   :  protected Eigenloadable
//   :  public Eigensolvable<TransformerType::TRANS_HERM, PROBLEM_TYPE::PROBLEM_TYPE_GEN>
{
   protected:
      template<class MATA, class MATB, class T, class U>
      //void EigenSolution(const MATA& aA, const MATB& aB, T& aReducedEigVecs, U& aReducedEigVals, ilapack::iDSYGVD_t)
      void EigenSolution(const MATA& aA, const MATB& aB, T& aReducedEigVecs, U& aReducedEigVals)
      {
         auto eig_sol = SYGVD(aA,aB);
         Eigenloadable::LoadEigenvalueData(aReducedEigVecs,aReducedEigVals,eig_sol,ilapack::row_major);
      }
};

#endif /* EIGENSOLVABLE_H_INCLUDED */
