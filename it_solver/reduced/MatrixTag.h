#ifndef MATRIXTAG_H_INCLUDED
#define MATRIXTAG_H_INCLUDED

#include "libmda/util/static_value.h"

namespace matrix_tag
{

// put static_value into namespace
using libmda::util::static_value;

/**
 * matrix switches
 **/
enum matrix_t { MATRIX_A, MATRIX_B, MATRIX_Y };

typedef static_value<matrix_t,MATRIX_A> (*MatA_t) ();
typedef static_value<matrix_t,MATRIX_B> (*MatB_t) ();
typedef static_value<matrix_t,MATRIX_Y> (*MatY_t) ();

inline static_value<matrix_t,MATRIX_A> MatA() { return 0; }
inline static_value<matrix_t,MATRIX_B> MatB() { return 0; }
inline static_value<matrix_t,MATRIX_Y> MatY() { return 0; }

} /* namespace matrix_tag */


#endif /* MATRIXTAG_H_INCLUDED */
