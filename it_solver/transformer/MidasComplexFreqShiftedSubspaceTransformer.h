#ifndef MIDASCOMPLEXFREQSCALEDSUBSPACETRANSFORMER_H_INCLUDED
#define MIDASCOMPLEXFREQSCALEDSUBSPACETRANSFORMER_H_INCLUDED

#include <complex>

#include "it_solver/IES.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "mmv/Transformer.h"
#include "test/check/MatricesAreEqual.h"

template<class TRANS
       , class REFRQ = const MidasVector&
       , class IMFRQ = const MidasVector&
       >
class MidasComplexFreqShiftedSubspaceTransformerImpl
{
   private:
      TRANS mTrans;
      In mDim;
      REFRQ mReFrq;
      IMFRQ mImFrq;

      std::vector<DataCont>& mS; // S matrix
      std::vector<DataCont>& mSigma; // \Sigma^S = A S
      std::vector<DataCont>& mLSigma; // L^\Sigma^S = S^\dagger A
      const MidasMatrix& mDelta;  // S^\dagger A S
      DataCont& mAdiag; // A_diag

      bool mDebug = true;

   public:
      explicit MidasComplexFreqShiftedSubspaceTransformerImpl(TRANS&& aTrans
                                                            , REFRQ&& re_frq 
                                                            , IMFRQ&& im_frq
                                                            , std::vector<DataCont>& aS
                                                            , std::vector<DataCont>& aSigma
                                                            , std::vector<DataCont>& aLSigma
                                                            , const MidasMatrix& aDelta
                                                            , DataCont& aAdiag
                                                            ): 
         mTrans(std::forward<TRANS>(aTrans))
       , mDim(mTrans.VectorDimension())
       , mReFrq(std::forward<REFRQ>(re_frq))
       , mImFrq(std::forward<IMFRQ>(im_frq))
       , mS(aS)
       , mSigma(aSigma)
       , mLSigma(aLSigma)
       , mDelta(aDelta)
       , mAdiag(aAdiag)
      {
         assert(mReFrq.size() == mImFrq.size()); // 
         //// Transform S ( \Sigma = A S )
         //for(In i = 0; i < mS.size(); ++i)
         //{
         //   mSigma.emplace_back(mS[i].Size());
         //   Nb dummy = 0.0;
         //   aTrans.Transform(mS[i], mSigma[i], 1, 0, dummy);
         //}
         //// Left Transform s ( L^\Sigma = S^\dagger A )
         //for(In i = 0; i < mS.size(); ++i)
         //{
         //   mLSigma.emplace_back(mS[i].Size());
         //   Nb dummy = 0.0;
         //   aLeftTrans.Transform(mS[i], mLSigma[i], 1, 0, dummy);
         //}
         //// calc \Delta  = S^\dagger A S = S^\dagger \Sigma
         //mDelta.SetNewSize(mS.size(), mS.size());
         //for(In i = 0; i < mS.size(); ++i)
         //{
         //   for(In j = 0; j < mS.size(); ++j)
         //   {
         //      mDelta[i][j] = IES_Dot(mS[i], mSigma[j]);
         //   }
         //}
         //// Get A_diag
         //DataCont vec_of_ones(mDim, C_1, "InMem");
         //Nb e_dummy = 0.0; // dummy input for Transform
         //aAtrans.Transform(vec_of_ones, mAdiag, I_0, I_1, e_dummy);
         //mAdiag.Scale(-C_1); // hmm gets -diag above... (so we need to change sign...)
      }

      void Transform(DataCont& in, DataCont& out, In i=I_1, In j=I_0, Nb e=0.0) const
      {
         out.Zero();
         /////
         // precalculate some stuff
         /////
         MidasVector s_dot(mS.size());
         for(In i = 0; i < mS.size(); ++i)
         {
            s_dot[i] = IES_Dot(mS[i], in);
         }
         MidasVector s_delta_dot(mS.size());
         s_delta_dot = mDelta * s_dot; // s_delta_dot <- \delta_S * S^\dagger b_j

         /////
         // calculate transformation
         /////
         // term 1: S L^\Sigma^S b_j = \sum_i s_i * dot(sigma_i, b_j)
         for(In i = 0; i < mS.size(); ++i)
         {
            IES_Axpy(out, mS[i], IES_Dot(mLSigma[i],in));
         } 
         // term 2: \Sigma^S S^\dagger b_j = \sum_i \sigma_i * dot(s_i,b_j)
         for(In i = 0; i < mS.size(); ++i)
         {
            IES_Axpy(out, mSigma[i], s_dot[i]);
         }
         // term 3: - S \Delta^S S^\dagger b_j (using same philosophy as above, but with s_delta_dot as 'dot' coefficient)
         for(In i = 0; i < mS.size(); ++i)
         {
            IES_Axpy(out, mS[i], -s_delta_dot[i]);
         }
         // term 4: Q_S A_diag Q_S b_j = (1 - P_S) A_diag (1 - P_S) b_j
         //                            = A_diag b_j - P_S A_diag b_j - A_diag P_S b_j + P_S A_diag P_S b_j
         //                              ----4a----   ------4b------   ------4c------   --------4d--------
         DataCont a_diag_bj(in); // a_diag_bj <- b_j
         a_diag_bj.HadamardProduct(mAdiag,1.0,DataCont::Product::Normal); // a_diag_bj <- A_diag * b_j

         DataCont ps_bj(mDim); ps_bj.Zero(); // ps_bj <- 0
         for(In i = 0; i < mS.size(); ++i)
         {
            IES_Axpy(ps_bj, mS[i], s_dot[i]);
         } // ps_bj <- P_S b_j

         // term 4a
         IES_Axpy(out,a_diag_bj,1.0);
         // term 4b
         for(In i = 0; i < mS.size(); ++i)
         {
            IES_Axpy(out, mS[i], -IES_Dot(mS[i], a_diag_bj));
         }
         // term 4c
         ps_bj.HadamardProduct(mAdiag, 1.0 ,DataCont::Product::Normal); // ps_bj <- A_diag P_S b_j
         IES_Axpy(out, ps_bj, - 1.0);
         // term 4d
         for(In i = 0; i < mS.size(); ++i)
         {
            IES_Axpy(out, mS[i], IES_Dot(mS[i], ps_bj)); //
         }

         ///////////////////////////////////////////////////////////////////////////////////////////////////
         // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
         ///////////////////////////////////////////////////////////////////////////////////////////////////
         if(mDebug)
         {
            bool muted = Mout.Muted();
            if(muted) Mout.Unmute();
            if(mS.size() == 0) // then out = a_diag * in 
            {
               Mout << " Testing Empty subspace transformation: ";
               DataCont test_out(in); // test_out <- in
               test_out.HadamardProduct(mAdiag,1.0,DataCont::Product::Normal); // test_out <- a_diag*in
               MidasAssert(midas::test::check::DataContAreEqual(test_out, out, 10), "NOT EQUAL FOR ZERO SPACE");
               Mout << "SUCCESS!" << std::endl;
            }
            if(mS.size() == mTrans.VectorDimension())
            {
               Mout << " Testing FULL space transformation: ";
               DataCont test_out(in.Size());
               Nb dummy;
               mTrans.Transform(in, test_out, 1, 0, dummy);
               if(!midas::test::check::DataContAreEqual(test_out, out, 10000)) 
                  MidasWarning("NOT EQUAL FOR FULL SPACE");
               else
                  Mout << "SUCCESS!" << std::endl;
            }
            if(muted) Mout.Mute();
         }
      }

      In Dim() const
      {
         return mDim;
      }

      DataCont TemplateZeroVector() const
      {
         return DataCont(Dim(), C_0);
      }

      DataCont TemplateUnitVector(Uin aIndex) const
      {
         // Default construct (size 0), resize and set to unit.
         DataCont unit_dc;
         unit_dc.SetNewSize(Dim());
         unit_dc.SetToUnitVec(aIndex);
         return unit_dc;
      }

      auto NFrequencies() const -> decltype(mReFrq.size())
      {
         return mReFrq.size();
      }

      std::complex<Nb> Frequency(size_t i) const
      {
         return {mReFrq[i],mImFrq[i]};
      }

      auto ReFrequency(size_t i) const
         -> decltype(mReFrq[i])
      {
         return mReFrq[i];
      }
      
      auto ImFrequency(size_t i) const
         -> decltype(mImFrq[i])
      {
         return mImFrq[i];
      }

      REFRQ& GetReFrq() const
      {
         return mReFrq;
      }

      IMFRQ& GetImFrq() const
      {
         return mImFrq;
      }

      auto CloneImprovedPrecon(In aLevel) const 
         -> decltype(mTrans.CloneImprovedPrecon(aLevel))
      {
         auto ptr = mTrans.CloneImprovedPrecon(aLevel);
         return ptr;
      }

      void ConstructExplicit(MidasMatrix& mat) const
      {
         mTrans.ConstructExplicit(mat);
      }
};

using MidasComplexFreqShiftedSubspaceTransformer = MidasComplexFreqShiftedSubspaceTransformerImpl<Transformer&>;

#endif /* MIDASCOMPLEXFREQSCALEDTRANSFORMER_H_INCLUDED */
