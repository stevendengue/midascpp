#ifndef MATRIXTRANSFORMER_H_INCLUDED
#define MATRIXTRANSFORMER_H_INCLUDED

#include <utility>

template<class M, class V>
class MatrixTransformer
{
   private:
      const M& mMat;
      using value_type = decltype(std::declval<V>().operator()(std::declval<int>()));
      using size_type  = decltype(std::declval<M>().template extent<0>());
   
   public:
      explicit MatrixTransformer(const M& mat): 
         mMat(mat)
      {
      }

      void Transform(V& v, V& t) const
      {
         for(int i=0; i<mMat.template extent<0>(); ++i)
         {
            t(i) = 0;
            for(int j=0; j<mMat.template extent<1>(); ++j)
                  t(i) += mMat(i,j)*v(j);
         }
      }

      size_type Dim() const
      {
         return mMat.template extent<0>();
      }

      V TemplateZeroVector()
      {
         return V(Dim(), static_cast<value_type>(0.));
      }

      V TemplateUnitVector(int index)
      {
         V unit(Dim(), static_cast<value_type>(0.));
         unit(index) = static_cast<value_type>(1.);
         return unit;
      }
};

#endif /* MATRIXTRANSFORMER_H_INCLUDED */
