#ifndef SPACEEXPANDER_H_INCLUDED
#define SPACEEXPANDER_H_INCLUDED

#include "it_solver/IES.h"
#include "it_solver/DEBUG.h"

template<template<class> class ORTHO, template<class> class ADDTRIAL, class A>
class SpaceExpander: public ORTHO<ADDTRIAL<A> >
{
   private:
      Nb mResidualThrow = 1e-19; // norm threshold for adding residual
                                 // if residual has norm below this upon entry it is discarded

      template<class T, class R>
      void TryToAddTrial(T& trials, R& residual, bool& added_vector, In& n_added_vec)
      {
         // equation not converged, we try to add new vector
         auto norm = IES_Norm(residual);
         const Nb eps = mResidualThrow; 
         if(norm > eps) 
         {
            // norm ok: try to orthogonalze
            if(this->self().Orthogonalize(trials,residual))
            {
               // ortho ok: add vector
               if(this->self().AddTrialVector(trials,residual))
               {
                  added_vector = true;
                  ++n_added_vec;
               }
               else
               { // add trial failed

               }
            }
            else 
            { // ortho failed
               //Mout << " could not orthogonalize " << std::endl;
            }
         }
         else
         { // norm to small 
            //Mout << " Norm to small " << std::endl;
         }
      }
   protected:
      /**
       *
       **/
      template<class T, class R>
      bool ExpandSpaceImpl(T& trials, R& residuals)
      {
         bool added_vector = false;
         In n_added_vec = 0;
         
         for(size_t i=0; i<residuals.size(); ++i)
         {
//            Mout << "DEBUG: Converged().size() = " << this->self().Converged().size() << std::endl;
            if(!this->self().Converged()[i])
            {
               TryToAddTrial(trials,residuals[i],added_vector,n_added_vec);
            }
            else
            { // equation already converged
               // Mout << " Equation converged, not adding new trail " << std::endl;
            }  
         }
         
         // some output
         Mout << " Added " << n_added_vec << " new vectors.\n";

         //DEBUG_CheckOrthogonality(trials);
         //DEBUG_CheckLinearDependence(trials);
         //DEBUG_CheckLinearDependenceSVD(trials);
         residuals.clear();
         return added_vector;
      }

   public:
      bool ExpandSpace()
      {
         return ExpandSpaceImpl(this->self().Trials(),this->self().Residuals());
      }

      void SetResidualThrow(const Nb aResidualThrow) { mResidualThrow = aResidualThrow; }
};

#endif /* SPACEEXPANDER_H_INCLUDED */
