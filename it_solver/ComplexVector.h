#ifndef COMPLEXVECTOR_H_INCLUDED
#define COMPLEXVECTOR_H_INCLUDED

template<class RE, class IM>
class ComplexVector
{
   private:
      RE mRe;
      IM mIm;

   public:
      ComplexVector(): mRe(), mIm()
      {
      }

      ComplexVector(const RE& re, const IM& im)
         : mRe(re), mIm(im)
      {
      }

      ComplexVector(RE&& re, IM&& im)
         : mRe(std::move(re)), mIm(std::move(im))
      {
      }

      ComplexVector(int i, int j)
         : mRe(i), mIm(j)
      {
      }

      RE& Re() 
      {
         return mRe;
      }
      
      const RE& Re() const
      {
         return mRe;
      }
      
      IM& Im() 
      {
         return mIm;
      }
      
      const IM& Im() const
      {
         return mIm;
      }

      void Zero()
      {
         mRe.Zero();
         mIm.Zero();
      }
      
      //auto Norm() const -> decltype(sqrt(Dot(mRe,mRe)+Dot(mIm,mIm)))
      //{
      //   return sqrt(Dot(mRe,mRe)+Dot(mIm,Im));
      //}
      
      // non-const norm function... here because of weak DataCont interface :(
      auto Norm() -> decltype(sqrt(Dot(mRe,mRe)+Dot(mIm,mIm)))
      {
         return sqrt(Dot(mRe,mRe)+Dot(mIm,mIm));
      }

      //friend std::ostream& operator<< <>(std::ostream&, const ComplexVector<RE,IM>&);
};

template<class RE, class IM>
std::ostream& operator<<(std::ostream& os, const ComplexVector<RE,IM>& cv)
{
   os << "RE: " << cv.Re() << "\n" << "IM: " << cv.Im();
   return os;
}

template<class RE, class IM>
ComplexVector<RE,IM> MakeComplexVector(RE&& re, IM&& im)
{
   return ComplexVector<RE,IM>(std::forward<RE>(re), std::forward<IM>(im));
}

/**
 * NB: hardcoded to Nb... fix at some point
 **/
template<class RE, class IM>
std::complex<Nb> Dot(const ComplexVector<RE,IM>& v1, const ComplexVector<RE,IM>& v2)
{
   auto re_dot = Dot(v1.Re(),v2.Re()) + Dot(v1.Im(),v2.Im());
   auto im_dot = Dot(v1.Re(),v2.Im()) - Dot(v1.Im(),v2.Re());
   return {re_dot,im_dot};
}

/**
 *
 **/
template<class RE, class IM>
void Scale(ComplexVector<RE,IM>& cv, const std::complex<Nb> a)
{
   ComplexVector<RE,IM> cv_copy(cv);
   Scale(cv.Re(), a.real());
   Scale(cv.Im(), a.real());
   Axpy(cv.Re(), cv_copy.Im(), -a.imag());
   Axpy(cv.Im(), cv_copy.Re(),  a.imag());
}

/**
 *
 **/
template<class RE, class IM>
void Zero(ComplexVector<RE,IM>& cv)
{
   cv.Zero();
}

template<class RE, class IM>
auto Norm(ComplexVector<RE,IM>& cv) -> decltype(cv.Norm())
{
   return cv.Norm();
}

#endif /* COMPLEXVECTOR_H_INCLUDED */
