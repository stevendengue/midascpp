#ifndef LINEARRESIDUALMAKER_H_INCLUDED
#define LINEARRESIDUALMAKER_H_INCLUDED

#include "util/CallStatisticsHandler.h"
#include "it_solver/IES.h"

#include <type_traits>
#include<typeinfo>

template<class A>
class LinearResidualMaker: public A
{
   private:
      /**
       * r = A*x - y
       **/
      template
         <  class SIG
         ,  class Y
         ,  class RES
         ,  std::enable_if_t
            <  (  !std::is_same_v<StandardContainer<TensorDataCont>, SIG>
               || !std::is_same_v<StandardContainer<TensorDataCont>, Y>
               || !std::is_same_v<StandardContainer<TensorDataCont>, RES>
               )
            >* = nullptr
         >
      void MakeIthResidualNoImag
         (  SIG& sig
         ,  Y& y
         ,  MidasMatrix& sol
         ,  RES& res
         ,  size_t i
         )
      {
         res.emplace_back(sig.back()); // Same shape as sig
         IES_Zero(res.back());
         
         for(size_t j=0; j<sig.size(); ++j)
         {
            IES_Axpy(res.back(),sig[j],sol[j][i]);
         }
         
         IES_Axpy(res.back(),y[i],-C_1);
      }

      /**
       * r = A*x - y
       **/
      template
         <  class SIG
         ,  class Y
         ,  class RES
         ,  std::enable_if_t
            <  (  std::is_same_v<StandardContainer<TensorDataCont>, SIG>
               && std::is_same_v<StandardContainer<TensorDataCont>, Y>
               && std::is_same_v<StandardContainer<TensorDataCont>, RES>
               )
            >* = nullptr
         >
      void MakeIthResidualNoImag
         (  SIG& sig
         ,  Y& y
         ,  MidasMatrix& sol
         ,  RES& res
         ,  size_t i
         )
      {
         // Add zero vector (for shape)
         res.emplace_back(this->self().Atrans().TemplateZeroVector());
      
         // Get decompinfo to check if we are using CP tensors
         const auto& decompinfo = this->self().Atrans().GetDecompInfo();
         if (  decompinfo.empty()
            )
         {
            IES_Zero(res.back());

            for(size_t j=0; j<sig.size(); ++j)
            {
               IES_Axpy(res.back(),sig[j],sol[j][i]);
            }
            
            IES_Axpy(res.back(),y[i],-C_1);
         }
         else
         {
            // Construct decomposer
            #pragma omp parallel
            {
               midas::tensor::TensorDecomposer decomposer(decompinfo);
      
               auto trfrank = this->self().Atrans().GetAllowedRank();
               auto n_mcs = res.back().Size();
      
               #pragma omp for schedule(dynamic)
               for(size_t i_mc=0; i_mc<n_mcs; ++i_mc)
               {
                  auto& tens = res.back().GetModeCombiData(i_mc);
                  const auto& dims = tens.GetDims();
                  midas::vcc::TensorSumAccumulator<Nb> sum_acc(dims, &decomposer, trfrank);

                  // Subtract RHS
                  sum_acc -= y[i];

                  // Add transformed vectors
                  for(size_t j=0; j<sol.size(); ++j)
                  {
                     const auto& coef = sol[j][i];
                     const auto& trans_trial_j_imc = sig[j].GetModeCombiData(i_mc);

                     sum_acc.Axpy(trans_trial_j_imc, coef);
                  }

                  const auto& sum_tens = sum_acc.Tensor();

                  // Evaluate sum
                  if (  sum_tens.NDim() == 0
                     || sum_tens.Type() == BaseTensor<Nb>::typeID::SCALAR
                     )
                  {
                     Nb scalar;
                     sum_acc.EvaluateSum().GetTensor()->DumpInto(&scalar);
                     tens.ElementwiseScalarAddition(scalar);
                  }
                  else
                  {
                     tens = sum_acc.EvaluateSum();
                  }
               }
            }
         }
      } 
      

      /**
       *
       **/
      template
         <  class SIG
         ,  class Y
         ,  class RES
         >
      void MakeResidualImpl
         (  SIG& sig
         ,  Y& y
         ,  MidasMatrix& sol
         ,  RES& res
         )
      {
         assert(res.size() == 0);
         
         for(size_t i=0; i<this->self().Neq(); ++i)
         {
            this->MakeIthResidualNoImag(sig, y, sol, res, i);
         }
      }

   public:
      
      /**
       *
       **/
      void MakeResidual()
      {
         LOGCALL("residual");
         MakeResidualImpl(this->self().Sigma()
                        , this->self().Y()
                        , this->self().ReducedSolution()
                        , this->self().Residuals()
                        );
      }
};

#endif /* LINEARRESIDUALMAKER_H_INCLUDED */
