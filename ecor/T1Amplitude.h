
/**
************************************************************************
* 
* @file                
*
* Created:            20-03-2017         
*
* Author:             Gunnar Schmitz (gunnar.schmitz@chem.au.dk)  
*
* Short Description:  Class for T1 amplitudes in the ABC format
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef T1Amplitude_H_INCLUDED
#define T1Amplitude_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <string>

#include "ecor/DFIntegrals.h"
#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"
#include "tensor/NiceTensor.h"
#include "tensor/TensorLibrarian.h"
#include "ecor/TurboInfo.h"
//#include "ecor/ri_svd.h"
#include "tensor/TensorDecompInfo.h"
#include "tensor/TensorDecomposer.h"
#include "input/TensorDecompInput.h"
#include "input/MolStruct.h"
#include "util/CallStatisticsHandler.h"

class T1Amplitude : RI4Integral
{
   private:
      std::map<std::string, std::vector<Nb>> mOrben;
      LaplaceQuadrature<Nb> mLapQuad;

      void CalcIterm(NiceTensor<Nb>&);

   public:
      /******** Constructors and destructor ********/
      ///> Constructor
      T1Amplitude(RI3Integral&, RI3Integral&, std::map<std::string, std::vector<Nb>>, LaplaceQuadrature<Nb>&&, const bool);

      ///> Delete copy- and move constructor
      T1Amplitude(const T1Amplitude&)  = delete;
      T1Amplitude(T1Amplitude&&)       = delete;

      ///> Destructor
      ~T1Amplitude()
      {
      }

      /******** Methods ********/
 
};

#endif /* T1Amplitude_H_INCLUDED */
