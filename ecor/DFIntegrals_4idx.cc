/**
************************************************************************
* 
* @file                
*
* Created:            22-06-2016         
*
* Author:             Gunnar Schmitz (gunnar.schmitz@chem.au.dk)  
*
* Short Description:  Class for using atom dependend 4 Index RI/DF
*                     integrals
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <string>
#include <stdio.h>

#include "ecor/DFIntegrals.h"
#include "tensor/EnergyDenominatorTensor.h"
#include "ecor/ri_svd.h" 
#include "ecor/reducerank.h"
#include "lapack_interface/packed_matrix.h" 

namespace X=contraction_indices;



/**
* Constructor 
* */
RI4Integral::RI4Integral
   (  RI3Integral& bqij
   ,  RI3Integral& bqkl
   ,  const bool haveAOints
   ,  const bool doBatching
   )
{

   // constants
   const bool locdbg = false;
   const bool prtrank = true;
   const bool prtsing = false;

   // do we use the overlapmetric?
   mIsOvlpMetric = bqij.IsOvlpMetric();

   // set some shortcuts
   In naux   = bqij.NAux();
   In natoms = bqij.NAtoms();

   mNAtoms = natoms;
   mPairRanks = bqij.GetRanks();

   mHaveAOInts = haveAOints;
   mTurboInfo.ReadInfo("MIDAS_3ATX.aux");

   mIsAtomicBatched = doBatching; 

   // calculate offsets
   unsigned int iadr = 0;
   mPairRankOffset.push_back(iadr);
   for (unsigned i = 1; i < mPairRanks.size(); i++) 
   {
      iadr += mPairRanks[i-1];
      mPairRankOffset.push_back(iadr);
   }

   mPairList = bqij.GetPairList();  // Simple copy pair list of the 3 index integrals

   if (locdbg) 
   {
      Mout << "bqij" << std::endl;
      Mout << bqij.Get3Idx() << std::endl;   

      Mout << "bqkl" << std::endl;
      Mout << bqkl.Get3Idx() << std::endl;   
   }

   NiceTensor<Nb> Iqij = bqij.Get3Idx();
   NiceTensor<Nb> Iqkl = bqkl.Get3Idx();

   CanonicalTensor<Nb>* bqij_canon = dynamic_cast<CanonicalTensor<Nb>*>(Iqij.GetTensor());
   CanonicalTensor<Nb>* bqkl_canon = dynamic_cast<CanonicalTensor<Nb>*>(Iqkl.GetTensor());

   std::vector<unsigned int> dimij = bqij.GetDims();
   std::vector<unsigned int> dimkl = bqkl.GetDims();

   // Get Mode matrices and ranks
   Nb** mode_matij = bqij_canon->GetModeMatrices();
   unsigned int nrank1  = bqij_canon->GetRank();

   Nb** mode_matkl = bqkl_canon->GetModeMatrices();
   unsigned int nrank2  = bqkl_canon->GetRank();

   if (locdbg || prtrank)
   {
      Mout << " nrank1: " << nrank1 << std::endl;
      Mout << " nrank2: " << nrank2 << std::endl;
   }

   // set info 
   mNdim = 4;  // This will always be four, but lets see if the code becomes more applicable at some point

   mDims = {static_cast<unsigned int>(dimij[0]),static_cast<unsigned int>(dimij[1]),
            static_cast<unsigned int>(dimkl[0]),static_cast<unsigned int>(dimkl[1])};

   mRank = {static_cast<unsigned int>(nrank1),static_cast<unsigned int>(nrank1),
            static_cast<unsigned int>(nrank2),static_cast<unsigned int>(nrank2)};

   // set mode or "W" matrices
   if (locdbg) Mout << "Set mode matrices" << std::endl;
   for (unsigned int imode = 0; imode < mNdim; ++imode)
   {
      mWmat[imode].reset(new Nb[mRank[imode]*mDims[imode]]);
   }

   memcpy(reinterpret_cast<void*>(mWmat[0].get()),
          reinterpret_cast<void*>(mode_matij[1]),mRank[0]*mDims[0]*sizeof(Nb));
   memcpy(reinterpret_cast<void*>(mWmat[1].get()),
          reinterpret_cast<void*>(mode_matij[2]),mRank[1]*mDims[1]*sizeof(Nb));

   memcpy(reinterpret_cast<void*>(mWmat[2].get()),
          reinterpret_cast<void*>(mode_matkl[1]),mRank[2]*mDims[2]*sizeof(Nb));
   memcpy(reinterpret_cast<void*>(mWmat[3].get()),
          reinterpret_cast<void*>(mode_matkl[2]),mRank[3]*mDims[3]*sizeof(Nb));


   // compute a SVD of the coulomb metric
   if (doBatching)
   {
      /*************************************************************************
            The integrals are atomic batched that means we don't need
            the full combination matrix, but a decompositon like 

                              V^{-1} = U U^T
           
       ************************************************************************/  

      // save mode matrices for auxiliary index for later use
      mAuxModeMat.reset(new Nb[naux*mRank[0]]);
      mNAux = naux;
      memcpy(reinterpret_cast<void*>(mAuxModeMat.get()),
             reinterpret_cast<void*>(mode_matij[0]),mRank[0]*naux*sizeof(Nb));

      // Allocate memory and decompose V^{-1}
      mUmatJ.reset(new Nb[naux*naux]);
      if (mIsOvlpMetric) 
      {
         // We use the Overlap metric
         
         // 1) Get J_PQ^(1/2) matrix
         std::string algo = "Eigen";
         std::unique_ptr<Nb> jmat(new Nb[naux*naux]);
         DecomposeRIMatrix(jmat.get(), algo, true, naux);

         // 2) Get Overlap matrix S_PQ
         NiceTensor<Nb> spqmat = mTurboInfo.ReadVPQ("MIDAS_SPQ");
         Nb* spqmat_ptr = dynamic_cast<SimpleTensor<Nb>*>(spqmat.GetTensor())->GetData();

         // 3) Invert S_PQ matrix by eigen decomposition
         std::unique_ptr<Nb> sinv(new Nb[naux*naux]);
         InvertSymMatrix(sinv.get(), spqmat_ptr, algo, naux);

         // 4) Form ~J_PQ^(-1/2) = S_PQ^(-1) J_PQ^(1/2)
         {
            char nc = 'N';
            char nt = 'N';
            int m = naux;  
            int n = naux;
            int k = naux;  
      
            Nb one  = static_cast<Nb>(1.0); 
            Nb zero = static_cast<Nb>(0.0);
            
            // GS: I'm not really sure we the order of J and S is correct in this way,
            //     but it works...    
            midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one 
                                         , jmat.get(), &m, sinv.get(), &m 
                                         , &zero, mUmatJ.get(), &m );    
         }

      }
      else
      {
         // We use the Coulomb metric
         DecomposeRIMatrix(mUmatJ.get(), mAlgoVPQ, false, naux);
      }
   }
   else
   {
      /*************************************************************************
            The integrals are considered as the full set therefore we
            need the combination matrix to combine both expansion lengths
       ************************************************************************/  

      // generate M matrix, which combines the expansion lengthes
      unsigned int nmatx;
      if (mIsMpacked)
      {
         nmatx = mRank[0]*(mRank[0]+1)/2;
         mScr12.reset(new Nb[mRank[0]*mScrBlock]);
      }
      else
      {
         nmatx = mRank[0]*mRank[2];
      }
      mMat12.reset(new Nb[nmatx]);

      Nb one  = static_cast<Nb>(1.0); 
      Nb zero = static_cast<Nb>(0.0);

      if (mIsMpacked)
      {
         /*
          * We build the combination matrix in a packed format  
          */ 

         if (mIsOvlpMetric)
         {
            MIDASERROR("Usage of overlap metric not yet possible!");
         }
         else
         {
            // Decompose V^{-1} to something like U U^T
            mUmatJ.reset(new Nb[naux*naux]);
            std::string salgo = "SVD";
            DecomposeRIMatrix(mUmatJ.get(), salgo, false, naux);

            std::unique_ptr<Nb> bpr(new Nb[naux*mRank[0]]);
            // 1.) B_P,r1  = sum_Q V^{-1/2}_PQ W_r^{(Q)}  (dgemm)
            {
               char nc = 'N'; char nt = 'N';
               int m = naux;  
               int n = mRank[0];
               int k = naux;  
                   
               midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                            , mUmatJ.get(), &m, mode_matij[0], &m
                                            , &zero, bpr.get(), &m );    

               // free memory
               mUmatJ.reset(nullptr);
            }

            // 2.) M_r1,r2 = sum_P B_P,r1 B_P,r 2  (dsfrk) 
            {
               std::unique_ptr<Nb> tmp(new Nb[mRank[0]*(mRank[0]+1)/2]);

               char tr = 'N';
               char up = 'U';
               char trans = 'T';
               int n = mRank[0];
               int k = naux;

               midas::lapack_interface::sfrk( &tr, &up, &trans, &n, &k, &one
                                            , bpr.get(), &k, &zero, tmp.get() );


               // convert from rectangular full format (RFP) to normal packed format 
               // (in the future use RFP the whole way down...)
               int info = 0;
               midas::lapack_interface::tfttp( &tr, &up, &n, tmp.get(), mMat12.get(), &info);

            }
         }
      }
      else
      {

         /*
          * We build the full combination matrix and do not exploit that it can
          * be saved and computed in a packed format  
          */

         std::unique_ptr<Nb> rimatrix( new Nb[naux*naux]);
 
         if (mIsOvlpMetric)
         {
            
            NiceTensor<Nb> spqmat = mTurboInfo.ReadVPQ("MIDAS_SPQ");
            NiceTensor<Nb> jpqmat = mTurboInfo.ReadVPQ("MIDAS_VPQ");

            Nb* jpqmat_ptr = dynamic_cast<SimpleTensor<Nb>*>(jpqmat.GetTensor())->GetData();
            Nb* spqmat_ptr = dynamic_cast<SimpleTensor<Nb>*>(spqmat.GetTensor())->GetData();

            // Invert S_PQ matrix by eigen decomposition
            std::unique_ptr<Nb> sinv(new Nb[naux*naux]);
            std::string algo = "Eigen";
            InvertSymMatrix(sinv.get(), spqmat_ptr, algo, naux);            

            // C_PQ = S_PQ^(-1) J_PQ
            std::unique_ptr<Nb> cpq(new Nb[naux*naux]);
            {
               char nc = 'N'; 
               char nt = 'N';
               int m = naux;
               int n = naux;
               int k = naux;

               midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                            , sinv.get(), &k, jpqmat_ptr, &k
                                            , &zero, cpq.get(), &m );
            }

            // ~J_PQ = C_PQ S_PQ^(-1)
            {
               char nc = 'N'; 
               char nt = 'N';
               int m = naux;
               int n = naux;
               int k = naux;

               midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                            , cpq.get(), &k, sinv.get(), &k
                                            , &zero, rimatrix.get(), &m );
            }
         }
         else
         {
            NiceTensor<Nb> vpqinv = mTurboInfo.ReadVPQ("MIDAS_VPQINV");
            Nb* vpqinv_ptr = dynamic_cast<SimpleTensor<Nb>*>(vpqinv.GetTensor())->GetData();
         
            memcpy(reinterpret_cast<void*>(rimatrix.get()),
                   reinterpret_cast<void*>(vpqinv_ptr), naux * naux * sizeof(Nb));
         }

         //////////////////////////////////////////////////////////////////////
         // Finally construct combination matrix
         //////////////////////////////////////////////////////////////////////

         std::unique_ptr<Nb> cpr(new Nb[naux*mRank[0]]);
         {
            // C_P,r1 = sum_Q V^{-1}_PQ W_r^{(Q)} 
            char nc = 'N'; 
            char nt = 'N';
            int m = naux;  
            int n = mRank[0];
            int k = naux;  
                
            midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                         , rimatrix.get(), &m, mode_matij[0], &m
                                         , &zero, cpr.get(), &m );    
         }

         {
            // M_r1,r2 = sum_P W_r2^{(P)} C_P,r1
            char nc = 'T'; 
            char nt = 'N';
            int m = mRank[2];  
            int n = mRank[0];
            int k = naux; 

            { 
               midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                            , mode_matkl[0], &k, cpr.get(), &k
                                            , &zero, mMat12.get(), &m );  
            }
         }
         
      }

      if (locdbg || prtsing) 
      {
         Nb* tmp(new Nb[nrank1*nrank2]);
         memcpy(reinterpret_cast<void*>(tmp),reinterpret_cast<void*>(mMat12.get()),nrank1*nrank2*sizeof(Nb));         
         
         SVD_struct<Nb> m12_svd = GESVD(tmp, static_cast<int>(nrank1), static_cast<int>(nrank2), 'A');
         
         auto rank = RankAbs(m12_svd, 1e-12);
         Mout << " Rank of M(r1,r2): " << rank << std::endl;

         delete[] tmp;
      }
   }

   //if (dumpInt) 
   if (false) 
   {
      SaveIntegral("eri.abc");
   } 

}

/**
* Constructor 
* */
RI4Integral::RI4Integral
   (  std::string file
   )
{

   // constants
   const bool locdbg = false;
   const bool prtrank = true;
   const bool prtsing = false;

   // Read Info from Turbomole
   mTurboInfo.ReadInfo("MIDAS_3ATX.aux");

   // Read integral 
   ReadIntegral(file);

   bool doBatching = mIsAtomicBatched;

   // compute a SVD of the coulomb metric
   if (doBatching)
   {
      /*************************************************************************
            The integrals are atomic batched that means we don't need
            the full combination matrix, but a decompositon like 

                              V^{-1} = U U^T
           
       ************************************************************************/  

      // Allocate memory and decompose V^{-1}
      mUmatJ.reset(new Nb[mNAux*mNAux]);
      DecomposeRIMatrix(mUmatJ.get(), mAlgoVPQ, false, mNAux);
   }
   else
   {
      /*************************************************************************
            The integrals are considered as the full set therefore we
            need the combination matrix to combine both expansion lengths
       ************************************************************************/  

      // generate M matrix, which combines the expansion lengthes
      // TODO
   }


   return;
}

void RI4Integral::InvertSymMatrix
   (  Nb* pOutputMat
   ,  Nb* pInputMat
   ,  std::string algo
   ,  In ndim
   )
{

   // constants
   const bool locdbg = true;

   if (algo == "Eigen") 
   {
      // 1) Do eigen decomposition M = V D V^T
      // 2) Invert D  
      // 3) Sandwich D^{-1} between the eigen vectors

      std::unique_ptr<Nb> tmp1(new Nb[ndim*ndim]);
      std::unique_ptr<Nb> tmp2(new Nb[ndim*ndim]);
      std::unique_ptr<Nb> eig( new Nb[ndim]);

      memcpy(reinterpret_cast<void*>(tmp2.get()),
             reinterpret_cast<void*>(pInputMat), ndim * ndim * sizeof(Nb));

      char jobz = 'V';
      char uplo = 'U';
      int n = ndim;
      int lwork = -1;
      int info = 0; 
      Nb dlen;

      // first run to get dimension for work
      midas::lapack_interface::syev( &jobz, &uplo, &n, tmp2.get(), &n, eig.get(), &dlen, &lwork, &info);

      // allocate memory for work
      lwork = int(dlen);
      std::unique_ptr<Nb> work(new Nb[lwork]);

      // do eigen decomposition
      midas::lapack_interface::syev( &jobz, &uplo, &n, tmp2.get(), &n, eig.get(), work.get(), &lwork, &info);

      if (info != 0)
      {
         Mout << " info " << info << std::endl;
         MIDASERROR("Diagonalization failed in InvertSymMatrix!");
      }

      Nb* eig_ptr = eig.get();
      // Invert the diagonal matrix 
      for (unsigned int iaux = 0; iaux < ndim; ++iaux)
      {
         eig_ptr[iaux] = 1.0 / eig_ptr[iaux];
      }

      // multiply the eigenvector with the diagonal matrix of eigenvalues
      size_t counter = 0;
      Nb* tmp1_ptr = tmp1.get();
      Nb* tmp2_ptr = tmp2.get();
      for (int j = 0; j < ndim; ++j) 
      {
         for (int i = 0; i < ndim; ++i)
         {
            tmp1_ptr[j*ndim + i] = tmp2_ptr[counter++] * eig_ptr[j];
         }
      }
     
      // Build now the output matrix 
      {
         char nc = 'N';
         char nt = 'T';
         int m = ndim;
         int n = ndim;
         int k = ndim;

         Nb one  = static_cast<Nb>(1.0); 
         Nb zero = static_cast<Nb>(0.0);

         midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one 
                                      , tmp1.get(), &m, tmp2.get(), &m 
                                      , &zero, pOutputMat, &m );   
      }
   }
   else
   {
      MIDASERROR("Unknown algorithm!");
   }

   return;
}

void RI4Integral::DecomposeRIMatrix
   (  Nb* pDecompMat
   ,  std::string algo
   ,  const bool skipinversion
   ,  In naux
   )
{

   // constants
   const bool locdbg = true;

   if (algo == "SVD")
   {
      // save left singular vector (which are idenitcal to the right ones in our case)
      // an absorb (sig)^(1/2) in singular vector so that it is a symmetric decomposition

      if (skipinversion)
      {
         MIDASERROR("Using option SVD it is not possible to skip the inversion (DecomposeRIMatrix)!");
      }

      NiceTensor<Nb> vpqinv = mTurboInfo.ReadVPQ("MIDAS_VPQINV");
      Nb* vpqinv_ptr = dynamic_cast<SimpleTensor<Nb>*>(vpqinv.GetTensor())->GetData();

      Nb* tmp(new Nb[naux*naux]);
      memcpy(reinterpret_cast<void*>(tmp),
             reinterpret_cast<void*>(vpqinv_ptr),naux*naux*sizeof(Nb));

      SVD_struct<Nb> svd = GESVD(tmp, static_cast<int>(naux), static_cast<int>(naux), 'A');

      size_t counter = 0;
      Nb val = 0.0;
      Nb sig;
      Nb* u_ptr = pDecompMat;
      for (int i = 0; i < naux; ++i) 
      {
         sig = svd.s[i];
         if (sig > 0.0)
         {
            val = sqrt(svd.s[i]);
         }
         else
         {
            val = 0.0;
         }

         for (int j = 0; j < naux; ++j)
         {
            u_ptr[j*naux + i] = svd.u[counter++] * val;
         }
      }
      u_ptr = nullptr;

      if (locdbg) 
      {
         Mout << "singular values: " << std::endl;
         for (int i = 0; i < naux; ++i)
         {
            Mout << " " << i << " " << svd.s[i] << std::endl;
         }
      }

      delete[] tmp;

   }
   else if (algo == "Eigen") 
   {
      // Build square root of (P|Q) matrix and invert it
      // 1) Do eigen decomposition (P|Q) = V D V^T
      // 2) Build the square root of D and invert it 
      // 3) Sandwich D^{-1/2} between the eigen vectors

      // 0) Read (P|Q) matrix from file
      std::string file = "MIDAS_VPQ";
      NiceTensor<Nb> vpqmat = mTurboInfo.ReadVPQ(file);
      Nb* vpqmat_ptr = dynamic_cast<SimpleTensor<Nb>*>(vpqmat.GetTensor())->GetData();

      std::unique_ptr<Nb> tmp1(new Nb[naux*naux]);
      std::unique_ptr<Nb> tmp2(new Nb[naux*naux]);
      std::unique_ptr<Nb> eig( new Nb[naux]);

      memcpy(reinterpret_cast<void*>(tmp2.get()),
             reinterpret_cast<void*>(vpqmat_ptr), naux*naux*sizeof(Nb));

      char jobz = 'V';
      char uplo = 'U';
      int n = naux;
      int lwork = -1;
      int info = 0; 
      Nb dlen;

      // first run to get dimension for work
      midas::lapack_interface::syev( &jobz, &uplo, &n, tmp2.get(), &n, eig.get(), &dlen, &lwork, &info);

      // allocate memory for work
      lwork = int(dlen);
      std::unique_ptr<Nb> work(new Nb[lwork]);

      // do eigen decomposition
      midas::lapack_interface::syev( &jobz, &uplo, &n, tmp2.get(), &n, eig.get(), work.get(), &lwork, &info);

      Nb* eig_ptr = eig.get();
      // take the square root of the eigenvalues
      if (skipinversion)
      {
         // now inversion, only square root
         for (unsigned int iaux = 0; iaux < naux; ++iaux)
         {
            eig_ptr[iaux] = sqrt(eig_ptr[iaux]);
         }
      }
      else
      {
         // also invert matrix
         for (unsigned int iaux = 0; iaux < naux; ++iaux)
         {
            eig_ptr[iaux] = 1.0 / sqrt(eig_ptr[iaux]);
         }
      }

      // multiply the eigenvector with the diagonal matrix of eigenvalues
      size_t counter = 0;
      Nb* tmp1_ptr = tmp1.get();
      Nb* tmp2_ptr = tmp2.get();
      for (int j = 0; j < naux; ++j) 
      {
         for (int i = 0; i < naux; ++i)
         {
            tmp1_ptr[j*naux + i] = tmp2_ptr[counter++] * eig_ptr[j];
         }
      }
     
      // Build now square root of (P|Q) or (P|Q)^(-1) 
      {
         char nc = 'N';
         char nt = 'T';
         int m = naux;
         int n = naux;
         int k = naux;

         Nb one  = static_cast<Nb>(1.0); 
         Nb zero = static_cast<Nb>(0.0);

         midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one 
                                      , tmp1.get(), &m, tmp2.get(), &m 
                                      , &zero, pDecompMat, &m );   
      }


   }
   else if (algo == "Cholesky")
   {
      if (skipinversion)
      {
         MIDASERROR("Using option Cholesky it is not possible to skip inversion (DecomposeRIMatrix)!");
      }

      // 0) Read (P|Q)^-1
      NiceTensor<Nb> vpqinv = mTurboInfo.ReadVPQ("MIDAS_VPQINV");
      Nb* vpqinv_ptr = dynamic_cast<SimpleTensor<Nb>*>(vpqinv.GetTensor())->GetData();

      // 1) Calculate the Cholesky decomposition of the (P|Q)^-1 Matrix
      char uplo = 'U';
      int n = naux;
      int info = 0;
      
      memcpy(reinterpret_cast<void*>(pDecompMat),
             reinterpret_cast<void*>(vpqinv_ptr), naux*naux*sizeof(Nb));

      midas::lapack_interface::potrf( &uplo, &n, pDecompMat, &n, &info);

      if (info != 0) 
      {
         MIDASERROR("Error in call to potrf!");
      }

      // 2) Zero out not referenced values...
      Nb* u_ptr = pDecompMat;
      for (int iaux = 0; iaux < naux; ++iaux)
      {
         for (int jaux = 0; jaux < iaux; ++jaux)
         {
            u_ptr[jaux*naux + iaux] = 0.0;      
         }
      }

   }
   else if (algo == "CholeskyInversion")
   {
      // Calculate Cholesky decomposition of (P|Q) = L L^T and than invert L 
      // ... then follows (L^T)^{-1} L^{-1} = (P|Q)^{-1}          

      // 0) Read (P|Q) matrix from file 
      std::string file = "MIDAS_VPQ";
      NiceTensor<Nb> vpqmat = mTurboInfo.ReadVPQ(file);
      Nb* vpqmat_ptr = dynamic_cast<SimpleTensor<Nb>*>(vpqmat.GetTensor())->GetData();

      memcpy(reinterpret_cast<void*>(pDecompMat),
             reinterpret_cast<void*>(vpqmat_ptr), naux*naux*sizeof(Nb));

      // 1) Do Cholesky decomposition
      char uplo = 'L';
      char diag = 'N'; // diagonal different from one
      int n = naux;
      int info = 0;
      
      midas::lapack_interface::potrf( &uplo, &n, pDecompMat, &n, &info);

      if (info != 0) 
      {
         MIDASERROR("Error in call to potrf!");
      }

      // 2) invert cholesky decomposition
      if (!skipinversion)
      {
         midas::lapack_interface::trtri( &uplo, &diag, &n, pDecompMat, &n, &info);
      }

      if (info != 0) 
      {
         MIDASERROR("Error in call to trtri!");
      }

      // 3) Zero out not referenced values...
      Nb* u_ptr = pDecompMat;
      for (int iaux = 0; iaux < naux; ++iaux)
      {
         for (int jaux = 0; jaux < iaux; ++jaux)
         {
            u_ptr[iaux*naux + jaux] = 0.0;      
         }
      }

   }
   else
   {
      MIDASERROR("Unknown algorithm to decompose (PQ) matrix!");
   }

   return;
}

void RI4Integral::GetUMatrix
   (  Nb* umatab
   ,  int rankab
   ,  unsigned int abpair
   )
{
   
   Nb one  = static_cast<Nb>(1.0); 
   Nb zero = static_cast<Nb>(0.0);

   // = sum_P U^{J}_Pk W_{r^{AB}P}^{(3)} 
   char nc = 'N'; char nt = 'N';
   int m = mNAux;  
   int n = rankab;
   int k = mNAux;  
   
   unsigned int irankstart = GetStartRank(abpair);
   unsigned int iadr = irankstart * mNAux;
   
   Nb* auxmodemat = mAuxModeMat.get();
   
   midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one,
                                  mUmatJ.get(), &m, &auxmodemat[iadr], &m,
                                  &zero, umatab, &m ); 
      
   return;
}


/**
 *  @brief Converts the THC format to the CP format
 *
 *  Combines the expansion lengthes r1 and r2 via the combination matrix M_r1,r2
 * 
 *
 * @return Returns a NiceTensor<Nb> with the 4 index integral as CP tensor.
 * */
NiceTensor<Nb> RI4Integral::ToCanonicalTensor()
{
  
   // constants
   const bool locdbg = false;
 
   // new total rank
   In nranktot = mRank[0]*mRank[2];

   // allocate memory for mode matrices
   Nb** modemat = new Nb*[mNdim];
   for (unsigned int idim = 0; idim < mNdim; ++idim)
   {
      modemat[idim] = new Nb[nranktot*mDims[idim]];
   }

   // copy mode matrices
   In irank = 0;
   In irankloc = 0;
   Nb m12;
   Nb* m_ptr = mMat12.get();
   for (unsigned int irank1 = 0; irank1 < mRank[0]; ++irank1)
   {
      for (unsigned int irank2 = 0; irank2 < mRank[2]; ++irank2)
      {
         // first seperately since we absorb here the weigthing factors
         if (mIsMpacked)
         {
            MIDASERROR("Not yet tested!");
            m12 = m_ptr[(irank2+1)*irank2/2  + irank1];
         }
         else
         {
            m12 =  m_ptr[irank2*mRank[0] + irank1];
         }

         Nb* pWmat = mWmat[0].get();
         for (unsigned int ibas = 0; ibas < mDims[0]; ++ibas)
         {
            modemat[0][irank*mDims[0] + ibas] = pWmat[irank1*mDims[0] + ibas] * m12;
         }

         // now the remaining 3...
         for (unsigned int idim = 1; idim<mNdim; ++idim)
         {
            if (idim == 1) 
            {
               irankloc = irank1;
            }
            else
            {
               irankloc = irank2;
            }
            Nb* pWmat = mWmat[idim].get();
            for(unsigned int ibas = 0; ibas < mDims[idim]; ++ibas)
            {
               modemat[idim][irank*mDims[idim] + ibas] = pWmat[irankloc*mDims[idim] + ibas];
            }
         }         

         irank += 1;
      }
   }
   m_ptr = nullptr;

   // patch everything together to form exchange integrals
   NiceTensor<Nb> result = (new CanonicalTensor<Nb>(mDims, nranktot, modemat));

   return result;
}


/**
 *  @brief Carries out a transformation  
 *
 *  Combines the expansion lengthes r1 and r2 via the combination matrix M_r1,r2
 * 
 * @param idx:    Index which should be transformed
 * @param cmo:    Transformation coefficients 
 * @param nbas:   Source dimension 
 * @param nmodim: Target dimension
 * */
void RI4Integral::Transform
   (  const unsigned int idx
   ,  Nb* cmo
   ,  In nbas
   ,  In nmodim
   )   
{
   // sanity checks
   bool dotraf = true;

   dotraf = idx < mNdim; 

   if (idx < mNdim) dotraf = dotraf && nbas == mDims[idx];

   if (dotraf) 
   {
      char nc = 'N'; char nt = 'N';
      Nb alpha = static_cast<Nb>(1.0); Nb beta  = static_cast<Nb>(0.0);
      int m = nmodim;  
      int n = mRank[idx];
      int k = nbas;  
      
      // allocate array for output 
      Nb* mat = new Nb[nmodim*mRank[idx]];

      // do transformation
      midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &alpha,
                                     cmo, &m, mWmat[idx].get(), &k,
                                     &beta, mat, &m );

      // copy result to new mode matrix for idx and update dimensions
      if (nmodim != mDims[idx])  
      {       
         // if size changed reallocate memory 
         mDims[idx] = nmodim;
         mWmat[idx].reset(new Nb[mDims[idx]*mRank[idx]]);
      }
        
      memcpy(reinterpret_cast<void*>(mWmat[idx].get()),reinterpret_cast<void*>(mat),mDims[idx]*mRank[idx]*sizeof(Nb));

      // clean up
      delete[] mat;
   }
   else
   {
      MIDASERROR("Bad index chosen for transformation!");
   }
}

/**
 * @brief Gets dimensions of the 4 index integral tensor 
 * */
const std::vector<unsigned int>& RI4Integral::GetDims() const
{  
   return mDims;
}

/**
 * @brief Gets ranks of the 4 index integral tensor 
 * */
const std::vector<unsigned int>& RI4Integral::GetRanks() const
{  
   return mRank;
}


Nb RI4Integral::THCEMP2J
   (  std::map<std::string, std::vector<Nb>>& orben
   ,  LaplaceQuadrature<Nb> &quad
   )
{
   // constants
   const bool locdbg = false;

   // sanity check (Have we MO integrals?)
   if (!mHaveAOInts) 
   {
      MIDASERROR("RI4Integral::THCEMP2J :>> I need AO integrals but only have MO integrals!");
   }

   Nb emp2j = C_0;

   unsigned int irankstart,irankend,jrankstart,jrankend;

   // create energy denominator e_{ab}^{ij} = (e_a + e_b - e_i - e_j)^{-1}
   NiceTensor<Nb> eabij = new EnergyDenominatorTensor<Nb>(2, orben.at("v"), orben.at("oct"), quad);
   unsigned int nlap = quad.NumPoints();


   // unsigned int natompairs = mNAtoms * mNAtoms; // replace later with prescreend list 
   unsigned int natompairs = mPairList.size();

   LOGCALL("THC-COULOMB-AB");

   unsigned int lapinc = 1;

   //for (unsigned int ilap = 0; ilap < nlap; ++ilap)
   for (unsigned int ilap = 0; ilap < nlap; ilap += lapinc)
   {
      Nb dlapwei = quad.Weights()[ilap];

      if (locdbg) Mout << "Start with generation of E and F intermediates for ilap " << ilap << std::endl;

      /*************************************************************************
                        Construct E and F intermediates O(N^5) step
      *************************************************************************/ 
      std::string efile = "eintermed.bin";

      long filsize = 0;

      std::vector<long> elist(natompairs*(natompairs+1)/2);
      
      {
         LOGCALL("E-intermed");

         ofstream outeint;
         outeint.open(efile, ios::out | ios::binary);

         unsigned int mxrank = GetMaxRank();

         // We keep some of the intermediates in main memory to reduce I/O
         unsigned int nmaxmem = 1000 * 1024*128; // buffer of max 1000 MiB
         unsigned int maxbuff = std::min<In>(nmaxmem / (mxrank*mxrank), natompairs); 
         unsigned int inmem = 0;
         std::vector<NiceTensor<Nb>> eintermediates(maxbuff);
         std::vector<std::tuple<unsigned int,unsigned int>> quadruples(maxbuff); 

         if (ilap == 0) Mout << " I have a buffer of size " << maxbuff << std::endl << std::endl;

         for (unsigned int abpair = 0; abpair < natompairs; ++abpair)
         {
            // get start and end rank for pair AB
            irankstart = GetStartRank(abpair);
            irankend = GetEndRank(abpair);  
            unsigned int rankab = mPairRanks[abpair]; 

            if (rankab == 0) continue;

            std::tuple<unsigned int,unsigned int> indab = mPairList[abpair];
            unsigned int aat = std::get<0>(indab);
            unsigned int bat = std::get<1>(indab);

            if (locdbg) Mout << "(aat,bat) " << aat << " , " << bat << std::endl;

            // construct MO mode matrices for this atom pair
            NiceTensor<Nb> moint_ab = GetHalfMP2MOints(aat,bat,irankstart,irankend);

            for (unsigned int cdpair = 0; cdpair <= abpair; ++cdpair)
            {

               // get start and end rank for pair CD 
               jrankstart = GetStartRank(cdpair);
               jrankend = GetEndRank(cdpair);
               unsigned int rankcd = mPairRanks[cdpair]; 

               if (rankcd == 0) continue;

               std::tuple<unsigned int, unsigned int> indcd = mPairList[cdpair];
               unsigned int cat = std::get<0>(indcd);
               unsigned int dat = std::get<1>(indcd);

               if (locdbg) Mout << "(cat,dat) " << cat << " , " << dat << std::endl;
            
               // construct MO mode matrices for this atom pair
               NiceTensor<Nb> moint_cd = GetHalfMP2MOints(cat,dat,jrankstart,jrankend);
  
               // compute A^{z,ABCD}_{r^{AB}r^{CD}} and I^{z,ABCD}_{r^{AB}r^{CD}} and combine
               {
                  LOGCALL("AI-intermed");
                  NiceTensor<Nb> ar1r3 = ContractMOIndices(eabij,moint_ab,moint_cd,0,0,ilap,ilap+lapinc);  // all virtual indices "a"
                  NiceTensor<Nb> ir1r3 = ContractMOIndices(eabij,moint_ab,moint_cd,1,1,ilap,ilap+lapinc);  // all occupied indices "i"
                  eintermediates[inmem] = ar1r3 * ir1r3;
               }
               quadruples[inmem] = std::make_tuple(abpair, cdpair);

               // dump E^{z,ABCD}_{r^{AB}r^{CD}} and F^{z,ABCD}_{r^{AB}r^{CD}} to file
               if (inmem < maxbuff - 1)
               {
                  inmem += 1;
               }
               else
               {
                  LOGCALL("IO to write EF");

                  // write everything to file
                  for (unsigned int i = 0; i < inmem + 1; ++i)
                  {
                     std::tuple<unsigned int,unsigned int> indabcd = quadruples[i];
                     unsigned int abpair = std::get<0>(indabcd); 
                     unsigned int cdpair = std::get<1>(indabcd);

                     // write to file and save current position in output stream
                     long iadre = SaveIntermediate(eintermediates[i],outeint);

                     elist[abpair*(abpair+1)/2 + cdpair] = iadre;

                     long ndata_e(eintermediates[i].TotalSize());
                     filsize += ndata_e ; 
                  }
                  // clear counter
                  inmem = 0; 
               }
            }
         }

         // check if we still hold intermediates in memory
         if (inmem > 0) 
         {
            for (unsigned int i = 0; i < inmem; ++i)
            {
               LOGCALL("IO to write EF");

               std::tuple<unsigned int,unsigned int> indabcd = quadruples[i];
               unsigned int abpair = std::get<0>(indabcd);
               unsigned int cdpair = std::get<1>(indabcd);

               // write to file and save current position in output stream
               long iadre = SaveIntermediate(eintermediates[i],outeint);

               elist[abpair*(abpair+1)/2 + cdpair] = iadre;

               long ndata_e(eintermediates[i].TotalSize());
               filsize += ndata_e; 
            } 
            inmem = 0;
         }
         // close files
         outeint.close();
      }

      if (ilap == 1) 
      {
         Mout << " Storage requirement for E and F intermediates:  " 
              << ( filsize * 8.0 / (1024*1024) ) << " MiB  " 
              << ( filsize * 8.0 / (1024*1024*1024) ) << " GiB" << std::endl;
      }

      if (locdbg) Mout << "Start with generation of G and H intermediates for ilap " << ilap << std::endl;

      /*************************************************************************
                        Construct G and H intermediates 
      *************************************************************************/ 

      {
         ifstream ineint;
         ineint.open(efile, ios::in | ios::binary);

         LOGCALL("GH-intermed");

         Nb one  = static_cast<Nb>(1.0); 
         Nb zero = static_cast<Nb>(0.0);

         ////////////////////////////////////////////////////////////
         // allocate Memory
         ////////////////////////////////////////////////////////////

         // for full output arrays
         Nb* fullf = new Nb[mNAux*mNAux];  // data that later is packed to a NiceTensor

         // for inner loop output arrays
         unsigned int mxrank = GetMaxRank();
         std::unique_ptr<Nb> fdata(new Nb[mNAux*mxrank]);

         // init
         for (unsigned int i = 0; i<mNAux*mNAux; ++i)
         {
            fullf[i] = zero;
         }

         ////////////////////////////////////////////////////////////
         // Loop over CD,AB to build F and H intemediate
         ////////////////////////////////////////////////////////////
         const unsigned int mxbufab = std::min<In>(static_cast<unsigned int>(1000),natompairs);

         std::vector<NiceTensor<Nb>> eintbuf(mxbufab);

         // define Blocks of AB for that we can hold intermediates in memory
         unsigned int nblkab = (natompairs + mxbufab - 1) / mxbufab; 

         if (ilap == 1) Mout << " My buffer is large enough to batch everything in " << nblkab << " passes" << std::endl << std::endl;

         //for (unsigned int iblkcd = 0; iblkcd < nblkcd; ++iblkcd)
         {
            unsigned int cdstart = 0;
            unsigned int cdend   = natompairs;

            // actually construct G and H
            for (unsigned int cdpair = cdstart; cdpair < cdend; ++cdpair)
            {
               unsigned int rankcd = mPairRanks[cdpair];

               if (rankcd == 0) continue;

               // init local G and H intermediate
               Nb* fptr = fdata.get();
               for (unsigned int i = 0; i<mNAux*mxrank; ++i)
               {
                  fptr[i] = zero;
               }
               fptr = nullptr;

               for (unsigned int iblkab = 0; iblkab < nblkab; ++iblkab)
               {
                  const unsigned int abstart = iblkab * mxbufab;
                  const unsigned int abend   = std::min<In>((iblkab+1)*mxbufab, natompairs);
                  unsigned int abpair0 = 0;

                  // read in intermediates for this block
                  for (unsigned int abpair = abstart; abpair < abend; ++abpair)
                  {
                     
                     unsigned int rankab = mPairRanks[abpair]; 

                     if (rankab == 0) continue;

                     LOGCALL("IO to read EF");
                     // read E^{z,ABCD}_{r^{AB}r^{CD}} and F^{z,ABCD}_{r^{AB}r^{CD}}
                     if (abpair >= cdpair)
                     {
                        long iadre = elist[abpair*(abpair+1)/2+cdpair]; 

                        eintbuf[abpair0] = ReadIntermediate(ineint,iadre);
                     }
                     else
                     {
                        // we have only the transposed intermediates stored...
                        long iadre = elist[cdpair*(cdpair+1)/2+abpair]; 

                        NiceTensor<Nb> eintermed = ReadIntermediate(ineint,iadre);
                        eintbuf[abpair0] = eintermed[X::o,X::q,X::p];
                     }
                     abpair0 += 1;
                  }

                  // do computations for this block
                  abpair0 = 0;
                  for (unsigned int abpair = abstart; abpair < abend; ++abpair)
                  {
                     unsigned int rankab = mPairRanks[abpair]; 

                     if (rankab == 0) continue;

                     // get handle on data so that we can directly perform dgemm on it
                     SimpleTensor<Nb>* simplehandle1 = dynamic_cast<SimpleTensor<Nb>*>(eintbuf[abpair0].GetTensor());
                     Nb* edata = simplehandle1->GetData();

                     // built \bar{U}^{M,AB}_{r^{AB}k}
                     std::unique_ptr<Nb> umatmab(new Nb[mNAux*rankab]); 

                     {
                        // = sum_P U^{J}_Pk W_{r^{AB}P}^{(3)} 
                        char nc = 'N'; char nt = 'N';
                        int m = mNAux;  
                        int n = rankab;
                        int k = mNAux;  
                     
                        unsigned int irankstart = GetStartRank(abpair);
                        unsigned int iadr = irankstart * mNAux;
                        
                        Nb* auxmodemat = mAuxModeMat.get();
    
                        midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one,
                                                       mUmatJ.get(), &m, &auxmodemat[iadr], &m,
                                                       &zero, umatmab.get(), &m ); 
                     }


                     // F^{CD,z}_{r^{CD}k} = sum_{r^{AB}}  E^{z,ABCD}_{r^{AB}r^{CD}} \bar{U}^{M,AB}_{r^{AB}k}
                     {
                        char nc = 'N'; char nt = 'T';
                        int m = mNAux;  
                        int n = rankcd;
                        int k = rankab; 
 
                        midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one,
                                                       umatmab.get(), &m, edata, &n,
                                                       &one, fdata.get(), &m );    

                     }

                     // increment local counter for pairs ab in the current block AB
                     abpair0 += 1;
                  }
               }

               // built \bar{U}^{M,CD}_{r^{CD}k}
               std::unique_ptr<Nb> umatmcd(new Nb[mNAux*rankcd]); 

               {
                  // = sum_P U^{J}_Pk W_{r^{CD}P}^{(3)} 
                  char nc = 'N'; 
                  char nt = 'N';
                  int m = mNAux;  
                  int n = rankcd;
                  int k = mNAux;  
                  
                  unsigned int irankstart = GetStartRank(cdpair);
                  unsigned int iadr = irankstart * mNAux;
    
                  Nb* auxmodemat = mAuxModeMat.get();
    
                  midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one,
                                                 mUmatJ.get(), &m, &auxmodemat[iadr], &m,
                                                 &zero, umatmcd.get(), &m );
               }

   
               // F^{z}_{k_2 k_1} += sum_{r^{CD}} G^{CD,z}_{r^{CD}k_1} \bar{U}^{M,CD}_{r^{CD}k_2}
               {
                  char nc = 'N'; 
                  char nt = 'T';
                  int m = mNAux;  
                  int n = mNAux;
                  int k = rankcd; 
 
                  midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one,
                                                 fdata.get(), &m, umatmcd.get(), &m,
                                                 &one, fullf, &m );    

               }

            }
         }
         ineint.close();

         // pack everything together and contract
         std::vector<unsigned int> dims = {static_cast<unsigned int>(mNAux), 
                                           static_cast<unsigned int>(mNAux)};

         NiceTensor<Nb> fint(new SimpleTensor<Nb>(dims, fullf));

         NiceTensor<Nb> enerj = (contract(fint[X::p,X::q],fint[X::p,X::q]));
         emp2j += -2.0 * dlapwei * enerj.GetScalar();
      }
   }

   Mout << "emp2j " << emp2j << std::endl;
   return emp2j;
}

Nb RI4Integral::THCEMP2K
   (  std::map<std::string
   ,  std::vector<Nb>>& orben
   ,  LaplaceQuadrature<Nb>& quad 
   )
{
   // constants
   const bool locdbg = false;
   const bool doread = true;

   // sanity check (Have we MO integrals?)
   if (!mHaveAOInts) 
   {
      MIDASERROR("RI4Integral::THCEMP2K :>> I need AO integrals but only have MO integrals!");
   }

   Nb emp2k = C_0;

   unsigned int irankstart,irankend,rankab;
   unsigned int jrankstart,jrankend;
   unsigned int rankcd;

   // create energy denominator e_{ab}^{ij} = (e_a + e_b - e_i - e_j)^{-1}
   NiceTensor<Nb> eabij = new EnergyDenominatorTensor<Nb>(2, orben.at("v"), orben.at("oct"), quad);
   std::vector<unsigned int> dims_eabij = eabij.GetDims();
   unsigned int nlap = quad.NumPoints();


   // Get pointer to mode matrices
   CanonicalTensor<Nb>* eabij_canon = dynamic_cast<CanonicalTensor<Nb>*>(eabij.GetTensor());
   Nb** eabij_modemat = eabij_canon->GetModeMatrices();

   // unsigned int natompairs = mNAtoms * mNAtoms; // replace later with prescreend list 
   unsigned int natompairs = mPairList.size();

   unsigned int nvir = mTurboInfo.NVirt();
   unsigned int noct = mTurboInfo.NOct();

   unsigned int mxrank = GetMaxRank();

   LOGCALL("THC-EXCHANGE-AB");

   Nb one  = static_cast<Nb>(1.0); 
   Nb zero = static_cast<Nb>(0.0);

   unsigned int noctlen = 1; 

   for (unsigned int joct = 0; joct < noct; ++joct)
   {
   
      // allocate Memory
      Nb* ejak = new Nb[mNAux*nvir*noctlen];

      for (unsigned int i = 0; i < mNAux*nvir*noctlen; ++i)
      {
         ejak[i] = zero;
      }

      {
         LOGCALL("E intemediate");

         for (unsigned int abpair = 0; abpair < natompairs; ++abpair)
         {
            // get start and end rank for pair AB
            irankstart = GetStartRank(abpair);
            irankend = GetEndRank(abpair);  
            rankab = mPairRanks[abpair]; 

            if (rankab == 0) continue;

            std::tuple<unsigned int, unsigned int> indab = mPairList[abpair];
            unsigned int aat = std::get<0>(indab);
            unsigned int bat = std::get<1>(indab);

            // Get Mode matrices in MO basis
            NiceTensor<Nb> moint_ab = GetHalfMP2MOints(aat, bat, irankstart, irankend);

            // make A_jb^{r}
            NiceTensor<Nb> ajbr = CombineFactorMatrices(moint_ab, joct, joct+noctlen);

            // built \bar{U}^{M,AB}_{r^{AB}k}
            std::unique_ptr<Nb> umatmab(new Nb[mNAux*rankab]); 
            {
               // = sum_P U^{J}_Pk W_{r^{AB}P}^{(3)} 
               char nc = 'N'; 
               char nt = 'N';
               int m = mNAux;  
               int n = rankab;
               int k = mNAux;  
           
               unsigned int irankstart = GetStartRank(abpair);
               unsigned int iadr = irankstart * mNAux;

               Nb* auxmodemat = mAuxModeMat.get();
                  
               midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                            , mUmatJ.get(), &m, &auxmodemat[iadr], &m
                                            , &zero, umatmab.get(), &m );  
            }

            /*******************************************************************************
               transform with decomposed combination matrix and add to result matrix
             ******************************************************************************/

            // get handle on data in A
            SimpleTensor<Nb>* simplehandle1 = dynamic_cast<SimpleTensor<Nb>*>(ajbr.GetTensor());
            Nb* adata = simplehandle1->GetData();

            // E^{AB}_{jbk} = sum_{r^{AB}}  A^{AB}_{jbr^{AB}} \bar{U}^{M,AB}_{r^{AB}k}
            {
               char nc = 'N'; 
               char nt = 'N';
               int m = mNAux;  
               int n = nvir*noctlen; // (R,V,O)  
               int k = rankab; 
 
               midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                            , umatmab.get(), &m, adata, &k
                                            , &one, ejak, &m );    
            }
             
         }
      }

      // pack everything together to a tensor
      std::vector<unsigned int> dims = {static_cast<unsigned int>(noctlen), 
                                        static_cast<unsigned int>(nvir), 
                                        static_cast<unsigned int>(mNAux)};
      NiceTensor<Nb> eint(new SimpleTensor<Nb>(dims, ejak));


      {
         LOGCALL("F intemediate");

         for (unsigned int ilap = 0; ilap < nlap; ++ilap)
         {
            Nb dlapwei = quad.Weights()[ilap];

            unsigned int nmaxmem = 8000 * 1024*128; // buffer of max 8000 MiB
            unsigned int maxbuff = std::min<In>(nmaxmem / (4*mxrank*mNAux), natompairs); 
            unsigned int inmem = 0;  

            // define Blocks of AB for that we can hold intermediates in memory
            unsigned int nblk = (natompairs + maxbuff - 1) / maxbuff;

            if (joct == 0 && ilap == 0) Mout << "  My buffer is large enough to have " << nblk << " block of size " << maxbuff << std::endl;

            /************************************************************************************************
                         Calculate the F intermediates and thereby the energy 
             ***********************************************************************************************/
            fstream fsxint;
            fsxint.open ("xintermed.bin", std::fstream::in | std::fstream::out | std::fstream::app | ios::binary);

            fstream fseint;
            fseint.open ("ezab2.bin", std::fstream::in | std::fstream::out | std::fstream::app | ios::binary);

            // allocate memory
            std::vector<std::shared_ptr<Nb>> umatmab(maxbuff);
            std::vector<std::shared_ptr<Nb>> umatmcd(maxbuff);
            std::vector<std::shared_ptr<Nb>> ezab2(maxbuff);
            std::vector<std::shared_ptr<Nb>> ezcd2(maxbuff);
            for (unsigned i=0; i < umatmab.size(); i++) 
            {
               umatmab[i].reset(new Nb[mNAux*mxrank]); // Buffer for writing \bar{U}^{M,AB}_{r^{AB}k}
               umatmcd[i].reset(new Nb[mNAux*mxrank]); // Buffer for reading \bar{U}^{M,CD}_{r^{CD}k}

               ezab2[i].reset(new Nb[mNAux*mxrank]); // Buffer for writing E^{M,AB}_{r^{AB}k}
               ezcd2[i].reset(new Nb[mNAux*mxrank]); // Buffer for reading E^{M,CD}_{r^{CD}k}
            }
            std::vector<xinfo> lstOfXABintermed(natompairs);  // Book keeping for intermediate
            std::vector<xinfo> lstOfEABintermed(natompairs);  // Book keeping for intermediate
            std::vector<unsigned int> lstOfPairInMemory(maxbuff);
            inmem = 0;
            
            long iadrnew = 0;
            long iadrin = 0;

            unsigned int abpair0 = 0;

            std::unique_ptr<Nb> frabcd(new Nb[mxrank*mxrank]);
            std::unique_ptr<Nb> frcdab(new Nb[mxrank*mxrank]);
            std::unique_ptr<Nb> Hzjr(new Nb[mxrank*mxrank]);

            std::vector<NiceTensor<Nb>> moint_cd(natompairs);

            // Calculate F intemediates which depend on 4 atom labels
            for (unsigned int abpair = 0; abpair < natompairs; ++abpair)
            {
               // get start and end rank for pair AB
               irankstart = GetStartRank(abpair);
               irankend = GetEndRank(abpair);  
               rankab = mPairRanks[abpair]; 

               if (rankab == 0) continue;

               std::tuple<unsigned int,unsigned int> indab = mPairList[abpair];
               unsigned int aat = std::get<0>(indab);
               unsigned int bat = std::get<1>(indab);

               // Get Mode matrices in MO basis
               NiceTensor<Nb> moint_ab = GetHalfMP2MOints(aat,bat,irankstart,irankend);

               //GS
               moint_cd[abpair] = moint_ab;
               //GS

               // E^{AB,(a)}_{zjr^{AB}k} = sum_a E_{jak} W_{a r^{AB}}^{AB} a_{az}
               NiceTensor<Nb> ezjr1 = ContractMOIndicesOf(eint, moint_ab, eabij, 0, 0, 0, ilap);

               // E^{AB,(b)}_{zjr^AB}k} = sum_b E_{jbk} W_{b r^{AB}}^{AB} a_{bz}
               //NiceTensor<Nb> ezjr2 = ContractMOIndicesOf(eint, moint_ab, eabij, 0, 0, 2, ilap);
               
               //Nb* edata = dynamic_cast<SimpleTensor<Nb>*>(ezjr2.GetTensor())->GetData();
               Nb* edata = dynamic_cast<SimpleTensor<Nb>*>(ezjr1.GetTensor())->GetData();
               
               memcpy(reinterpret_cast<void*>(ezab2[inmem].get()),
                      reinterpret_cast<void*>(edata), mNAux*rankab * sizeof(Nb));

               if (locdbg) Mout << " built {U}^{M,AB}_{r^{AB}k}" << std::endl;

               // built \bar{U}^{M,AB}_{r^{AB}k}
               {
                  LOGCALL("Calc UmatAB");
                  // = sum_P U^{J}_Pk W_{r^{AB}P}^{(3)} 
                  char nc = 'N'; char nt = 'N';
                  int m = mNAux;  
                  int n = rankab;
                  int k = mNAux;  
                  
                  unsigned int irankstart = GetStartRank(abpair);
                  unsigned int iadr = irankstart * mNAux;
             
                  Nb* auxmodemat = mAuxModeMat.get();
             
                  midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                               , mUmatJ.get(), &m, &auxmodemat[iadr], &m
                                               , &zero, umatmab[inmem].get(), &m );
               }

               // set buffer info
               lstOfXABintermed[abpair].isincore = true;
               lstOfXABintermed[abpair].iadrmem  = static_cast<long>(inmem);

               lstOfEABintermed[abpair].isincore = true;
               lstOfEABintermed[abpair].iadrmem  = static_cast<long>(inmem);

               lstOfPairInMemory[inmem] = abpair;
               abpair0 = inmem;

               // increment buffer coutner or dump everything to file
               if (inmem < maxbuff - 1 )
               {
                  inmem += 1;
               }
               else if (abpair != natompairs - 1)
               {
                  // if the buffer will be full in the next cycle dump
                  // intermediates to file
                  
                  LOGCALL("IO to dump U and E");

                  unsigned int abpair;

                  for (unsigned int i = 0; i < inmem + 1; ++i)
                  {
                     iadrin = iadrnew;
                     iadrnew = SaveRawData(umatmab[i].get(), iadrin, mNAux*mxrank, fsxint);
                     iadrnew = SaveRawData(ezab2[i].get(),   iadrin, mNAux*mxrank, fseint);

                     abpair = lstOfPairInMemory[i];

                     lstOfXABintermed[abpair].isondisk = true;
                     lstOfXABintermed[abpair].iadrfil  = iadrin;
                     lstOfXABintermed[abpair].isincore = false;
                     lstOfXABintermed[abpair].iadrmem  = static_cast<long>(-1);

                     lstOfEABintermed[abpair].isondisk = true;
                     lstOfEABintermed[abpair].iadrfil  = iadrin;
                     lstOfEABintermed[abpair].isincore = false;
                     lstOfEABintermed[abpair].iadrmem  = static_cast<long>(-1);
                  }
                  inmem = 0;
               }

               for (unsigned int iblkcd = 0; iblkcd < nblk; ++iblkcd)
               {
                  const unsigned int cdstart = iblkcd * maxbuff;
                  const unsigned int cdend   = std::min<In>((iblkcd+1)*maxbuff, abpair+1);

                  unsigned int cdpair0 = 0;

                  // read in all intermediates which we can hold in memory
                  for (unsigned int cdpair = cdstart; cdpair < cdend; ++cdpair)
                  {
                     LOGCALL("IO to read E");

                     rankcd = mPairRanks[cdpair];
                     if (rankcd == 0) continue;

                     ///////////////////////////////////////////////////////////
                     // read E^{CD,(a)}_{zjr^{CD}k} }
                     ///////////////////////////////////////////////////////////
                     
                     if (lstOfEABintermed[cdpair].isincore)
                     {
                        LOGCALL("Copy E");

                        // We still have the intermediate in the write buffer, so we can 
                        // do a fast memcopy
                        long idx = lstOfEABintermed[cdpair].iadrmem;
                        ezcd2[cdpair0] = ezab2[idx];
                     }
                     else if (lstOfEABintermed[cdpair].isondisk)
                     {
                        LOGCALL("Read E");
                        // We write the intermediate from disk... slow process but better
                        // than the recomputation each time...
                        unsigned int ndata;
                        long iadr = lstOfEABintermed[cdpair].iadrfil;
                        ezcd2[cdpair0].reset(new Nb[mxrank*mNAux]);
                        ReadRawData(fseint, iadr, ezcd2[cdpair0].get(), ndata);
                        if (ndata != mNAux*mxrank)
                        {
                           MIDASERROR("Dimensions for reading do not agree!");
                        }
                     }
                     else
                     {
                        MIDASERROR("Try to read intermediate from disk, which does not exsit!");
                     }
                     cdpair0 += 1;
                  }

                  cdpair0 = 0;
                  for (unsigned int cdpair = cdstart; cdpair < cdend; ++cdpair)
                  {
                     LOGCALL("IO to read U");

                     rankcd = mPairRanks[cdpair];
                     if (rankcd == 0) continue;

                     ///////////////////////////////////////////////////////////
                     // read \bar{U}^{M,CD}_{r^{CD}k} (if necessary)
                     ///////////////////////////////////////////////////////////
                     if (lstOfXABintermed[cdpair].isincore)
                     {
                        LOGCALL("Copy U");

                        // We still have the intermediate in the write buffer, so we can 
                        // do a fast memcopy
                        long idx = lstOfXABintermed[cdpair].iadrmem;
                        umatmcd[cdpair0] = umatmab[idx];
                     }
                     else if (lstOfXABintermed[cdpair].isondisk)
                     {
                        LOGCALL("Read U");
                        // We write the intermediate from disk... slow process but better
                        // than the recomputation each time...
                        if (doread)  
                        {
                           unsigned int ndata;
                           long iadr = lstOfXABintermed[cdpair].iadrfil;
                           umatmcd[cdpair0].reset(new Nb[mxrank*mNAux]);
                           ReadRawData(fsxint,iadr,umatmcd[cdpair0].get(),ndata);
                           if (ndata != mNAux*mxrank)
                           {
                              MIDASERROR("Dimensions for reading do not agree!");
                           }
                        }
                        else
                        { 
                        
                           LOGCALL("Calc UmatCD");
                           // = sum_P U^{J}_Pk W_{r^{AB}P}^{(3)} 
                           char nc = 'N'; char nt = 'N';
                           int m = mNAux;  
                           int n = rankcd;
                           int k = mNAux;  
                           
                           unsigned int irankstart = GetStartRank(cdpair);
                           unsigned int iadr = irankstart * mNAux;
             
                           Nb* auxmodemat = mAuxModeMat.get();
             
                           midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                                        , mUmatJ.get(), &m, &auxmodemat[iadr], &m
                                                        , &zero, umatmcd[cdpair0].get(), &m );
                        
                        }
                     }
                     else
                     {
                        MIDASERROR("Try to read intermediate from disk, which does not exsit!");
                     }
                     cdpair0 += 1;
                  }

                  // do the computation steps
                  cdpair0 = 0;
                  for (unsigned int cdpair = cdstart; cdpair < cdend; ++cdpair)
                  {
                     rankcd = mPairRanks[cdpair];

                     if (rankcd == 0) continue;

                     std::tuple<unsigned int,unsigned int> indcd = mPairList[cdpair];
                     unsigned int cat = std::get<0>(indcd);
                     unsigned int dat = std::get<1>(indcd);

                     // F^{ABCD,(b)}_{zjr^{AB}r^{CD}} = sum_k E^{AB,(b)}_{zjr^{AB}k} \bar{U}^{M,CD}_{r^{CD}k}
                     Nb* fdata1 = frabcd.get();
                     {
                        LOGCALL("Calc F1");
                        // get handle on data in E^{ABCD,(b)}_{zjr^{AB}r^{CD}}
                        Nb* edata = dynamic_cast<SimpleTensor<Nb>*>(ezjr1.GetTensor())->GetData();

                        char nc = 'T';
                        char nt = 'N';
                        int m = rankcd;
                        int n = rankab;
                        int k = mNAux; 

                        midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one 
                                                     , umatmcd[cdpair0].get(), &k, edata, &k 
                                                     , &zero, fdata1, &m );
                     }

                     //     ... and ...
                     // F^{CDAB,(a)}_{zjr^{CD}r^{AB}} = sum_k E^{CD,(a)}_{zjr^{CD}k} \bar{U}^{M,AB}_{r^{AB}k}
                     Nb* fdata2 = frcdab.get(); 
                     {
                        LOGCALL("Calc F2");
                        // get handle on data in E^{ABCD,(b)}_{zjr^{AB}r^{CD}}
                        Nb* edata = ezcd2[cdpair0].get(); 

                        char nc = 'T'; 
                        char nt = 'N';
                        int m = rankab;
                        int n = rankcd;
                        int k = mNAux; 

                        midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one 
                                                     , umatmab[abpair0].get(), &k, edata, &k 
                                                     , &zero, fdata2, &m );
                     }
                     cdpair0 += 1;

                     // Form H^{ABCD}_{zr^{AB}r^{CD}} = F^{ABCD,(b)}_{zjr^{AB}r^{CD}} F^{CDAB,(a)}_{zjr^{CD}r^{AB}}
                     Nb dlap = eabij_modemat[3][ilap*noct + joct];  
                     NiceTensor<Nb> Hzjr = CreateHzjrIntermediate(rankab, rankcd, fdata1, fdata2, dlap, false);

                     // Form G^{ABCD}_{zr^{AB}r^{CD}} = \sum_i W^{AB}_ir W^{cd}_ir a_iz
                     NiceTensor<Nb> Gzjr = ContractAlongOcc(moint_ab, moint_cd[cdpair], eabij, ilap); 

                     // Calculate energy contribution
                     {
                        LOGCALL("Calc ener I");

                        // get handle on data in Hzjr and Gzjr
                        Nb* hdata = dynamic_cast<SimpleTensor<Nb>*>(Hzjr.GetTensor())->GetData();
                        Nb* gdata = dynamic_cast<SimpleTensor<Nb>*>(Gzjr.GetTensor())->GetData();

                        int n = rankab*rankcd;
                        int incx = 1;
                        int incy = 1;

                        emp2k += dlapwei * midas::lapack_interface::dot(&n, hdata, &incx, gdata, &incy);

                     }

                     // for non diagonal case calculate additional energy contribution
                     if (cdpair != abpair)
                     {
                        // Form H^{CDAB}_{zr^{CD}r^{AB}} = F^{ABCD,(b)}_{zjr^{AB}r^{CD}} F^{CDAB,(a)}_{zjr^{CD}r^{AB}}
                        NiceTensor<Nb> Hzjr = CreateHzjrIntermediate(rankcd, rankab, fdata2, fdata1, dlap, true);

                        // Calculate energy contribution
                        {
                           LOGCALL("Calc ener II");
                           
                           // get handle on data in Hzjr and Gzjr
                           Nb* hdata = dynamic_cast<SimpleTensor<Nb>*>(Hzjr.GetTensor())->GetData();
                           Nb* gdata = dynamic_cast<SimpleTensor<Nb>*>(Gzjr.GetTensor())->GetData();

                           int n = rankab*rankcd;
                           int incx = 1;
                           int incy = 1;

                           emp2k += dlapwei * midas::lapack_interface::dot(&n, hdata, &incx, gdata, &incy);

                        }
                     }

                     fdata1 = nullptr;
                     fdata2 = nullptr;

                  } // cdpair
               } // iblkcd
            } // abpair

            // Delete Files
            fsxint.close();
            fseint.close();

            std::set<std::string> files2delete; 

            files2delete.insert("ezab2.bin");
            files2delete.insert("xintermed.bin");

            set<std::string>::iterator iter;
            for (iter = files2delete.begin(); iter != files2delete.end(); ++iter) 
            {
               std::string file = *iter;
               const char* cfile = file.c_str();
               remove(cfile);
            }         

         } // ilap
      }


   } // joct
   return emp2k;
}

/**
 *  @brief Calculates the H intermediate in ABC-THC exchange contribution 
 *
 *  Calculates the H intermediate in the ABC-THC exchange like contribution
 *  for a given quadruple of atoms:
 *
 *  \f[
 *      H_r^{AB}r^{CD} = F_j,r^{AB}r^{CD} F_j,r^{CD}r^{AB} a_jz  
 *  \f]
 *
 * 
 * @param rankab: ranks for atoms A and B
 * @param rankcd: ranks for atoms C and D
 * @param fdata1: pointer to the array of F_j,r^{AB}r^{CD}
 * @param fdata2: pointer to the array of F_j,r^{CD}r^{AB}
 * @param dlap:   value for the Laplace weight for a given occ. orb.
 * @param btransposed: Is the intermediate transposed?
 * */
NiceTensor<Nb> RI4Integral::CreateHzjrIntermediate
   (  const unsigned int rankab
   ,  const unsigned int rankcd
   ,  Nb* fdata1
   ,  Nb* fdata2
   ,  Nb dlap
   ,  const bool btransposed 
   )
{

   LOGCALL("Create Hzjr");

   Nb* hdata = new Nb[rankab*rankcd];
   unsigned int iadrh,iadr1,iadr2;

   unsigned int blocksize = 16;  // Tiling to get better memory access
   std::vector<unsigned int> dimsh;

   if (btransposed) 
   {
      std::copy(fdata2,fdata2 + rankab*rankcd, hdata);

      for (unsigned int irankab = 0; irankab < rankab; irankab += blocksize)
      {
         for (unsigned int irankcd = 0; irankcd < rankcd; irankcd += blocksize)
         {
            for (unsigned int k = irankab; k < irankab + blocksize && k < rankab; ++k)
            {
               for (unsigned int l = irankcd; l < irankcd + blocksize && l < rankcd; ++l)
               {
                  iadrh = l*rankab + k;
                  iadr1 = k*rankcd + l;
                  //iadr2 = l*rankab + k;

                  //hdata[iadrh] = fdata1[iadr1] * fdata2[iadr2] * dlap;
                  hdata[iadrh] *= fdata1[iadr1] * dlap;
                  //hdata[iadrh] *= fdata1[iadrh] * dlap;
               }
            }
         }
      }
      dimsh = {static_cast<unsigned int>(rankcd), static_cast<unsigned int>(rankab)};
   }
   else
   {
      std::copy(fdata1,fdata1 + rankab*rankcd, hdata);
      
      for (unsigned int irankab = 0; irankab < rankab; irankab += blocksize)
      {
         for (unsigned int irankcd = 0; irankcd < rankcd; irankcd += blocksize)
         {
            for (unsigned int k = irankab; k < irankab + blocksize && k < rankab; ++k)
            {
               for (unsigned int l = irankcd; l < irankcd + blocksize && l < rankcd; ++l)
               {
                  iadrh = k*rankcd + l;
                  //iadr1 = k*rankcd + l;
                  iadr2 = l*rankab + k;

                  //hdata[iadrh] = fdata1[iadr1] * fdata2[iadr2] * dlap;
                  hdata[iadrh] *= fdata2[iadr2] * dlap;
                  //hdata[iadrh] *= fdata2[iadrh] * dlap;
               }
            }
         }
      }
      dimsh = {static_cast<unsigned int>(rankab), static_cast<unsigned int>(rankcd)};
   }

   NiceTensor<Nb> Hzjr(new SimpleTensor<Nb>(dimsh, hdata));


   return Hzjr;
}



/**
 *  @brief Calculates the MP2 energy  
 *
 *  Calculates the MP2 energy exploting the THC format
 * 
 * @param orben:    Map for the orbtial energies
 * @param nlap:     Number of Laplace quadrature points 
 * @param onlySOS:  Only the SOS-MP2 contribution is calculated
 * */
std::tuple<Nb,Nb> RI4Integral::THCEMP2
   (  std::map<std::string, std::vector<Nb>>& orben
   ,  const unsigned int nlap
   ,  const bool onlySOS
   )
{

   //constants 
   const bool locdbg = false;
   const bool prtener = true;
   const bool UseSym = true;

   // sanity check (Have we MO integrals?)
   if (mHaveAOInts) 
   {
      MIDASERROR("RI4Integral::THCEMP2 :>> I need MO integrals but only have AO integrals!");
   }

   Nb emp2j = C_0;
   Nb emp2k = C_0;

   // create energy denominator e_{ab}^{ij} = (e_a + e_b - e_i - e_j)^{-1}
   midas::tensor::LaplaceInfo lapinfo;
   lapinfo["TYPE"] = "FIXLAP";
   lapinfo["NUMPOINTS"] = nlap;
   detail::ValidateLaplaceInput(lapinfo);

//   LaplaceQuadrature<Nb> quad(nlap, 2, orben.at("v"), orben.at("oct"));
   LaplaceQuadrature<Nb> quad(lapinfo, 2, orben.at("v"), orben.at("oct"));

   NiceTensor<Nb> eabij = new EnergyDenominatorTensor<Nb>(2, orben.at("v"), orben.at("oct"), quad);

   /*************************************************************************
                       Coulomb like contribution
    *************************************************************************/ 
 
   {
      LOGCALL("THC-COULOMB");

      for (unsigned int ilap = 0; ilap < nlap; ++ilap)
      {
         Nb dlapwei = quad.Weights()[ilap];

         const unsigned int rankincr = mRank[0]; // ToDo: Is not working yet due to reuqired full contracion with combi matrix
         unsigned int jrankend;
         for (unsigned int jrank = 0; jrank < mRank[0]; jrank += rankincr)
         {
            jrankend = std::min<In>(jrank+rankincr,mRank[0]);

            if (locdbg) Mout << "Contract MO indices I for ilap " << ilap << std::endl; 

            NiceTensor<Nb> er1r3;
            {
               NiceTensor<Nb> ar1r3 = ContractMOIndices(eabij,0,ilap,ilap+1,jrank,jrankend);  // all virtual indices "a"
               NiceTensor<Nb> ir1r3 = ContractMOIndices(eabij,1,ilap,ilap+1,jrank,jrankend);  // all occupied indices "i"
               er1r3 = ar1r3 * ir1r3;  // form shortcut
            }

            NiceTensor<Nb> fr2r4;
            if (UseSym)
            {
               fr2r4 = er1r3;
            }
            else
            {
               NiceTensor<Nb> br2r4 = ContractMOIndices(eabij,2,ilap,ilap+1,jrank,jrankend);  // all virutal indices "b"
               NiceTensor<Nb> jr2r4 = ContractMOIndices(eabij,3,ilap,ilap+1,jrank,jrankend);  // all occupied indices "j"
               fr2r4 = br2r4 * jr2r4;  // form shortcut
            }

            if (locdbg) Mout << "Contract with combination matrix" << std::endl;

            // Carry out trasformation with Combination matrix M
            NiceTensor<Nb> er1r4 = ContractWithCombiMat(er1r3,2,static_cast<char>('N'));
            NiceTensor<Nb> fr1r4 = ContractWithCombiMat(fr2r4,1,static_cast<char>('N'));   

            if (locdbg) Mout << "Calculate coulomb like energy contribution" << std::endl;

            // Calculate Coulomb like energy contribution by contracting away all indices
            NiceTensor<Nb> enerj = (contract(er1r4[X::p,X::q,X::r],fr1r4[X::p,X::q,X::r]));
            emp2j += -2.0 * dlapwei * enerj.GetScalar();

         } 
      }

   }

   /*************************************************************************
                       Exchange like contribution
    *************************************************************************/ 
   if (!onlySOS) // if we want only SOS-MP2 skip this part 
   {
      LOGCALL("THC-EXCHANGE");

      // define some shortcuts
      const unsigned int noct = mDims[3];
      const unsigned int nbtnlap = 1;

      for (unsigned int ilap = 0; ilap < nlap; ++ilap)
      {
         Nb dlapwei = quad.Weights()[ilap];

         if (locdbg) Mout << "Init H matrix for ilap " << ilap << std::endl; 

         // init H intermediate
         unsigned int nsize_h = nbtnlap*mRank[0]*mRank[0];
         Nb* data(new Nb[nsize_h]);
         for (unsigned int idx = 0; idx < nsize_h; ++idx)
         {
            data[idx] = 0.0;
         }

         NiceTensor<Nb> hr1r3z( new SimpleTensor<Nb>( std::vector<unsigned int>{static_cast<unsigned int>(nbtnlap),
                                                                                static_cast<unsigned int>(mRank[0]),
                                                                                static_cast<unsigned int>(mRank[0])}, data));
 
         for (unsigned int joct = 0; joct < noct; ++joct)
         {
            unsigned int jstart = joct;
            unsigned int jend = joct+1;
            std::vector<unsigned int> jmap = {joct};

            if (locdbg) Mout << "make A and C for ilap " << ilap << " and joct " << joct << std::endl; 

            // make A_jb^{r2} and C_ja^{r4} for this j
            NiceTensor<Nb> ajbr2 = CombineFactorMatrices(3,2,jstart,jend);
            //NiceTensor<Nb> cjar4 = CombineFactorMatrices(1,0,jstart,jend);

            if (locdbg) Mout << "make B and D for ilap " << ilap << " and joct " << joct << std::endl; 

            // construct B_jb^{r1} and D_ja^{r3} by transformation with combination matrix
            NiceTensor<Nb> bjbr1 = ContractWithCombiMat(ajbr2,2,static_cast<char>('N'));
            NiceTensor<Nb> djar3 = ContractWithCombiMat(ajbr2,2,static_cast<char>('T'));    

            if (locdbg) Mout << "make E and F for ilap " << ilap << " and joct " << joct << std::endl; 

            // make E_j^{r1,r3,z} and F_j^{r1,r3,z}
            const unsigned int rankincr = 500; // replace later with something user defined
            unsigned int jrankend;
            for (unsigned int jrank = 0; jrank < mRank[0]; jrank += rankincr)
            {
               jrankend = std::min<In>(jrank+rankincr,mRank[0]);

               NiceTensor<Nb> er1r3zj = ContractMOIndicesWith(bjbr1,eabij,2,ilap,ilap+1,jrank,jrankend,true);
               NiceTensor<Nb> fr1r3zj = ContractMOIndicesWith(djar3,eabij,0,ilap,ilap+1,jrank,jrankend,false);

               if (locdbg) Mout << "Add to H for ilap " << ilap << " and joct " << joct << std::endl; 

               // make H_{r1,r3,z}
               AddToH(hr1r3z,eabij,er1r3zj,fr1r3zj,jmap,ilap,ilap+1,jrank,jrankend);
               //AddToH(hr1r3z,eabij,er1r3zj,er1r3zj,jmap,ilap,ilap+1,jrank,jrankend);
            }
         }

         if (locdbg) Mout << "Contract MO indices for ilap " << ilap  << std::endl; 

         // make G_{r1,r3,z}
         NiceTensor<Nb> gr1r3z = ContractMOIndices(eabij,1,ilap,ilap+1);
  
         if (locdbg) Mout << "Calculate exchange like energy contribution" << std::endl;

         // calculate exchange like energy contribution
         NiceTensor<Nb> enerk = (contract(gr1r3z[X::p,X::q,X::r],hr1r3z[X::p,X::q,X::r]));
         emp2k += dlapwei * enerk.GetScalar();
      }

   }

   /*************************************************************************
                       sum up and return
    *************************************************************************/ 

   if (locdbg || prtener) 
   {
      Mout << "EMP2(J) " << emp2j << std::endl; 
      Mout << "EMP2(K) " << emp2k << std::endl; 
   }

   return std::make_tuple(emp2j, emp2k);
} 

/**
 *  @brief Contracts an input tensor with the combination matrix 
 *
 *  Contracts a input tensor along dimension idx with the combination matrix 
 * 
 * @param input       tensor which should be contracted with the combination matrix
 * @param idx         index which whould be contracted with the combination matrix
 * @param nc          Character of 'T' or 'N' to tell if combination matrix should be transposed
 *
 * @result tensor which was contracted with combination matrix 
 * */
NiceTensor<Nb> RI4Integral::ContractWithCombiMat
   (  NiceTensor<Nb>& input
   ,  const unsigned int idx
   ,  char nc
   )
{
   const unsigned int irankstart = 0;
   const unsigned int irankend = mRank[2];

   return ContractWithCombiMat(input,idx,nc,irankstart,irankend);
}

/**
 *  @brief Contracts an input tensor with the combination matrix 
 *
 *  Contracts a input tensor along dimension idx with the combination matrix 
 * 
 * @param input       tensor which should be contracted with the combination matrix
 * @param idx         index which whould be contracted with the combination matrix
 * @param nc          Character of 'T' or 'N' to tell if combination matrix should be transposed
 * @param irankstart  startindex for rank
 * @param irankend    end infex for rank
 *
 * @result tensor which was contracted with combination matrix 
 * */
NiceTensor<Nb> RI4Integral::ContractWithCombiMat
   (  NiceTensor<Nb>& input
   ,  const unsigned int idx
   ,  char nc
   ,  const unsigned int irankstart
   ,  const unsigned int irankend
   )
{

   std::vector<unsigned int> dims = input.GetDims();
   std::vector<unsigned int> newdims = dims;
   const unsigned int nlap = dims[0];

   // get dimensions for the ranks
   const unsigned int nrank1 = mRank[0];
   const unsigned int nrank2 = irankend - irankstart;

   // get pointer to input data
   Nb* input_data = dynamic_cast<SimpleTensor<Nb>*>(input.GetTensor())->GetData();

   // allocate data for output
   Nb* data = new Nb[nrank1*nrank2*nlap];

   // sanity checks
   if (nc != 'N' && nc != 'T')
   {
      MIDASERROR("Unknown nc for ContractWithCombiMat!");
   }

   int m,n,k;
   char nt = 'N';
   Nb one = static_cast<Nb>(1.0); Nb zero  = static_cast<Nb>(0.0);


   if (idx == 2)     // contraction along second index 
   {
      n = dims[1]*nlap;     
      if (nc == 'N') 
      {
         m = mRank[0];
         k = mRank[2];
      }
      else
      {
         m = mRank[2];
         k = mRank[0];
      }

      if (mIsMpacked)
      {
         // sanity check
         if (nc != 'N')
         {
            MIDASERROR("We assume a symmetric matrix. So transposition has no effect! Are you sure you know what you are doing?");
         }
         char side = 'L';
         matmulpak(side,m,n,data,mMat12.get(),input_data,&one,&zero,mScr12.get(),m,mScrBlock);
      }
      else
      {
         midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one,
                                        mMat12.get(), &k,input_data, &k,&zero, data, &m );
      }

      newdims[2] = mRank[0];
   }
   else if (idx == 1) // contraction along first index
   {
      int nsize = dims[1]*dims[2];

      m = dims[2];
      if (nc == 'N')
      {
         n = mRank[0];     
         k = mRank[2];
      }
      else
      {
         n = mRank[2];     
         k = mRank[0];
      }

      if (mIsMpacked)
      {     
         char side = 'R';      

         for (unsigned int ilap = 0; ilap < nlap; ++ilap)
         {
            matmulpak( side,m,n,&data[ilap*nsize]
                     , mMat12.get(),&input_data[ilap*nsize]
                     , &one,&zero,mScr12.get(),m,mScrBlock);
         }
      }
      else
      {
         for (unsigned int ilap = 0; ilap < nlap; ++ilap)
         {
            midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                         , &input_data[ilap*nsize], &n
                                         , mMat12.get(), &k, &zero 
                                         , &data[ilap*nsize], &m );
         }
      }

      newdims[1] = mRank[0];
   }
   else
   {
      MIDASERROR("Unknown option for ContractWithCombiMat!");
   }

   NiceTensor<Nb> result(
         new SimpleTensor<Nb>(
            std::vector<unsigned int>{static_cast<unsigned int>(nlap),
                                      static_cast<unsigned int>(newdims[1]),
                                      static_cast<unsigned int>(newdims[2])}
     , data)
   );

   return result;    
}

void RI4Integral::AddToH
   (  NiceTensor<Nb>& target
   ,  NiceTensor<Nb>& eabij 
   ,  NiceTensor<Nb>& eprvj
   ,  NiceTensor<Nb>& fprvj
   ,  std::vector<unsigned int>& jmap
   )
{
   CanonicalTensor<Nb>* eabij_canon = dynamic_cast<CanonicalTensor<Nb>*>(eabij.GetTensor());
   const unsigned int nlap = eabij_canon->GetRank(); 
   
   return AddToH(target, eabij, eprvj, fprvj, jmap, 0, nlap);
}

void RI4Integral::AddToH
   (  NiceTensor<Nb>& target
   ,  NiceTensor<Nb>& eabij 
   ,  NiceTensor<Nb>& eprvj
   ,  NiceTensor<Nb>& fprvj
   ,  std::vector<unsigned int>& jmap
   ,  const unsigned int ilapstart
   ,  const unsigned int ilapend
   )
{
   CanonicalTensor<Nb>* eabij_canon = dynamic_cast<CanonicalTensor<Nb>*>(eabij.GetTensor());
   const unsigned int nlap = eabij_canon->GetRank(); 
   
   return AddToH(target, eabij, eprvj, fprvj, jmap, 0, nlap, 0, mRank[0]);

}

void RI4Integral::AddToH
   (  NiceTensor<Nb>& target
   ,  NiceTensor<Nb>& eabij 
   ,  NiceTensor<Nb>& eprvj
   ,  NiceTensor<Nb>& fprvj
   ,  std::vector<unsigned int>& jmap
   ,  const unsigned int ilapstart
   ,  const unsigned int ilapend
   ,  const unsigned int irankstart
   ,  const unsigned int irankend
   )
{
  
   // constants
   const bool locdbg = false;
 
   // get info on energy denominator
   CanonicalTensor<Nb>* eabij_canon = dynamic_cast<CanonicalTensor<Nb>*>(eabij.GetTensor());
   Nb** eabij_modemat = eabij_canon->GetModeMatrices();
   const unsigned int nlap = ilapend - ilapstart;

   // get data
   if (locdbg) Mout << "Get pointer to data of H" << std::endl; 
   Nb* data = dynamic_cast<SimpleTensor<Nb>*>(target.GetTensor())->GetData();

   // e_data and f_data
   if (locdbg) Mout << "Get pointer to data of E" << std::endl; 
   Nb* e_data = dynamic_cast<SimpleTensor<Nb>*>(eprvj.GetTensor())->GetData();

   if (locdbg) Mout << "Get pointer to data of F" << std::endl; 
   Nb* f_data = dynamic_cast<SimpleTensor<Nb>*>(fprvj.GetTensor())->GetData();

   std::vector<unsigned int> edims = eprvj.GetDims();
   std::vector<unsigned int> fdims = fprvj.GetDims();

   // sanity check
   if (edims[0] != fdims[0])
   {
      MIDASERROR("Dimensions do not agree in AddToH!");
   }

   // define some shortcuts
   const unsigned int noct = edims[0];
   const unsigned int nrank1 = mRank[0];
   const unsigned int nrank2 = irankend - irankstart;
   const unsigned int nmatr  = nrank1*nrank2; 

   const unsigned int nlda1 = nlap*mRank[0]*mRank[0];
   const unsigned int nlda2 = mRank[0]*mRank[0];
   const unsigned int nlda3 = mRank[0];

   unsigned int iadrdest,iadrsrc1,iadrsrc2;
   for (unsigned int joct = 0; joct < noct; ++joct)
   {
      if (locdbg) Mout << "loop for joct " << joct << std::endl; 
   
      unsigned int ilap_counter = 0;
      for (unsigned int ilap = ilapstart; ilap < ilapend; ++ilap)
      {

         Nb dfac = eabij_modemat[3][ilap*mDims[3] + jmap[joct]];

         for (unsigned int irank1 = 0; irank1 < nrank1; ++irank1)
         {
            unsigned int irank_counter = 0;
            for (unsigned int irank2 = irankstart; irank2 < irankend; ++irank2)
            {
               iadrdest = ilap_counter*nlda2 + irank1*nlda3 + irank2;

               //iadrsrc1 = joct*nlda1 + ilap_counter*nlda2 + irank1*nlda3 + irank2;    
               //iadrsrc2 = joct*nlda1 + ilap_counter*nlda2 + irank2*nlda3 + irank1;    

               iadrsrc1 = joct*nlap*nmatr + ilap_counter*nmatr + irank1*nrank2 + irank_counter; 
               iadrsrc2 = joct*nlap*nmatr + ilap_counter*nmatr + irank_counter*nrank1 + irank1;

               data[iadrdest] += dfac * e_data[iadrsrc2] * f_data[iadrsrc1]  ;  

               irank_counter += 1;
            }
         }
         ilap_counter += 1;
      }
   }

}



/**
 *  @brief Combines two factor matrices two 3 index intermediate 
 *
 *  Combines two factor matrices W_r^{(a)} and W_r^{(i)} along r to 
 *  a new 3 index intermediate A_r^{(ai)} as
 *
 *  \f[
 *    A_r^{(ai)} = W_r^{(a)} W_r^{(i)} 
 *  \f]
 *
 *  the index for (a) is than the leading index followed by (i) and r_1  
 * 
 * @param idm_dim     index of a
 * @param jdm_dim     index of i
 * @param istart      start index for a
 * @param iend        end index for a   
 * @param irankstart  startindex for rank of first dimension
 * @param irankend    end index for rank of first dimension
 * */
NiceTensor<Nb> RI4Integral::CombineFactorMatrices
   (  const unsigned int idm_dim
   ,  const unsigned int jdm_dim
   ,  const unsigned int istart
   ,  const unsigned int iend
   ,  const unsigned int irankstart
   ,  const unsigned int irankend
   )
{

   // get rank dimension
   unsigned int nrank = I_0;
   if (mRank[idm_dim] == mRank[jdm_dim])
   {
      nrank = mRank[idm_dim];
   }
   else
   {
      MIDASERROR("Ranks are not the same!");
   }
   
   // sanity check
   if (iend > mDims[idm_dim])
   {
      MIDASERROR("Too large end index in CombineFactorMatrices"); 
   }

   const unsigned int ndim = iend - istart;
   const unsigned int nranklen = irankend - irankstart;

   Nb* data = new Nb[ndim*mDims[jdm_dim]*nranklen];

   unsigned int iadr;

   Nb* pWmati = mWmat[idm_dim].get();
   Nb* pWmatj = mWmat[jdm_dim].get();
   for (unsigned int idx = istart; idx < iend; ++idx)
   {
      for (unsigned int jdx = 0; jdx < mDims[jdm_dim]; ++jdx)
      {
         for (unsigned int irank = irankstart; irank < irankend; ++irank)
         {
            iadr = (idx-istart)*mDims[jdm_dim]*nrank + jdx*nrank + irank;

            data[iadr] = pWmati[irank*mDims[idm_dim] + idx] * pWmatj[irank*mDims[jdm_dim] + jdx];
         }
      }
   }

   NiceTensor<Nb> result( new SimpleTensor<Nb>( std::vector<unsigned int>{static_cast<unsigned int>(ndim),
                                                                          static_cast<unsigned int>(mDims[jdm_dim]),
                                                                          static_cast<unsigned int>(nranklen)}, data)); 
   return result;
}

/**
 *  @brief Combines two factor matrices two 3 index intermediate 
 *
 *  Combines two factor matrices W_r^{(a)} and W_r^{(i)} along r to 
 *  a new 3 index intermediate A_r^{(ai)} as
 *
 *  \f[
 *    A_r^{(ai)} = W_r^{(a)} W_r^{(i)} 
 *  \f]
 *  
 *  the index for (a) is than the leading index followed by (i) and r_1  
 * 
 * @param idm_dim index of a
 * @param jdm_dim index of i  
 * @param istart  start index for a
 * @param iend    end index for a     
 * */
NiceTensor<Nb> RI4Integral::CombineFactorMatrices
   (  const unsigned int idm_dim
   ,  const unsigned int jdm_dim
   ,  const unsigned int istart
   ,  const unsigned int iend
   )
{
   const unsigned int irankstart = 0;
   const unsigned int irankend = mRank[idm_dim];

   return CombineFactorMatrices(idm_dim, jdm_dim, istart, iend, irankstart, irankend);
}

/**
 *  @brief Combines two factor matrices to 3 index intermediate 
 *
 *  Combines two factor matrices W_r^{(a)} and W_r^{(i)} along r to 
 *  a new 3 index intermediate A_r^{(ai)} as
 *
 *  \f[
 *    A_r^{(ai)} = W_r^{(a)} W_r^{(i)} 
 *  \f]
 *  
 *  the index for (a) is than the leading index followed by (i) and r_1  
 * 
 * @param idm_dim index of a
 * @param jdm_dim index of i  
 * */
NiceTensor<Nb> RI4Integral::CombineFactorMatrices
   (  const unsigned int idm_dim
   ,  const unsigned int jdm_dim
   )
{
   const unsigned int istart = 0;
   const unsigned int iend = mDims[idm_dim];
   const unsigned int irankstart = 0;
   const unsigned int irankend = mRank[idm_dim];

   return CombineFactorMatrices(idm_dim, jdm_dim, istart, iend, irankstart, irankend);
}

/**
 *  @brief Combines two factor matrices to 3 index intermediate 
 *
 *  Combines two factor matrices W_r^{(a)} and W_r^{(i)} along r to 
 *  a new 3 index intermediate A_r^{(ai)} as
 *
 *  \f[
 *    A_r^{(ai)} = W_r^{(a)} W_r^{(i)} 
 *  \f]
 *  
 *  the index for (a) is than the leading index followed by (i) and r_1  
 *
 * @param moint      Tensor containing the mode matrices
 * @param ioctstart  Start index for the occupied space
 * @param ioctend    End index for the occupied space
 * */
NiceTensor<Nb> RI4Integral::CombineFactorMatrices
   (  NiceTensor<Nb>& moint
   ,  unsigned int ioctstart
   ,  unsigned int ioctend 
   )
{

   // Get mode matrix
   CanonicalTensor<Nb>* moint_canon = dynamic_cast<CanonicalTensor<Nb>*>(moint.GetTensor());
   Nb** modemat = moint_canon->GetModeMatrices();
   const unsigned int nrank = moint_canon->GetRank();
   std::vector<unsigned int> dims = moint.GetDims(); 

   const unsigned int nvir = dims[0]; 
   const unsigned int noct = dims[1]; 

   const unsigned int noctloc = ioctend - ioctstart;

   Nb* data = new Nb[noctloc*nvir*nrank];

   unsigned int iadr;

   for (unsigned int ivir = 0; ivir < nvir; ++ivir)
   {
      unsigned int ioctcounter = 0;
      for (unsigned int ioct = ioctstart; ioct < ioctend; ++ioct)
      {
         for (unsigned int irank = 0; irank < nrank; ++irank)
         {
            iadr = ioctcounter*nvir*nrank + ivir*nrank + irank;

            data[iadr] = modemat[0][irank*nvir + ivir] * modemat[1][irank*noct + ioct];
         }
         ioctcounter += 1;
      }
   }

   NiceTensor<Nb> result( new SimpleTensor<Nb>( std::vector<unsigned int>{static_cast<unsigned int>(noctloc),
                                                                          static_cast<unsigned int>(nvir),
                                                                          static_cast<unsigned int>(nrank)}, data)); 
   return result;

}



NiceTensor<Nb> RI4Integral::GetHalfMP2MOints
   (  const unsigned int aat
   ,  const unsigned int bat
   ,  const unsigned int irankstart
   ,  const unsigned int irankend
   )
{

   LOGCALL("Transform AO ints");

   // get virtual CMO coefficients for aat
   NiceTensor<Nb> cmvir = mTurboInfo.ReadCmvir("MIDAS_CMO", aat);  

   // get occupied CMO coeficients for bat
   NiceTensor<Nb> cmoct = mTurboInfo.ReadCmoct("MIDAS_CMO", bat); 


   // shortcuts
   int nvir = mTurboInfo.NVirt();
   int noct = mTurboInfo.NOct();

   int nbasa = mTurboInfo.NBasAt(aat);
   int nbasb = mTurboInfo.NBasAt(bat);
   int nbas  = mTurboInfo.NBas();

   int ista = mTurboInfo.NBasOff(aat);
   int istb = mTurboInfo.NBasOff(bat);

   int rank = irankend - irankstart;

   // get handle on MO coefficients
   Nb* cmvir_ptr = dynamic_cast<SimpleTensor<Nb>*>(cmvir.GetTensor())->GetData(); 
   Nb* cmoct_ptr = dynamic_cast<SimpleTensor<Nb>*>(cmoct.GetTensor())->GetData(); 

   char nc = 'N'; char nt = 'N';
   Nb one = static_cast<Nb>(1.0); Nb zero  = static_cast<Nb>(0.0);
   
   // allocate array for output
   Nb** mat = new Nb*[2];  
   mat[0] = new Nb[nvir*rank];
   mat[1] = new Nb[noct*rank];

   unsigned int iadra = irankstart * nbas + ista;

   // do transformation to virtual space
   Nb* pWmat = mWmat[0].get();
   midas::lapack_interface::gemm( &nc, &nt, &nvir, &rank, &nbasa, &one,
                                  cmvir_ptr, &nvir, &pWmat[iadra], &nbas,
                                  &zero, mat[0], &nvir );

   unsigned int iadrb = irankstart * nbas + istb;

   // do transformation to occupied space
   pWmat = mWmat[1].get();
   midas::lapack_interface::gemm( &nc, &nt, &noct, &rank, &nbasb, &one,
                                  cmoct_ptr, &noct, &pWmat[iadrb], &nbas,
                                  &zero, mat[1], &noct );

   std::vector<unsigned int> dims = {static_cast<unsigned int>(nvir), static_cast<unsigned int>(noct)};
   NiceTensor<Nb> result (new CanonicalTensor<Nb>(dims, static_cast<unsigned int>(rank), mat));
   return result;  
}

/**
 *  @brief Contract two factor matrices along same index
 *
 *  Contracts two factor matrices along a common MO/AO index a with the
 *  energy denominator yielding a 3 index intermediate
 *
 *  \f[
 *    A_z^{r1,r3} = \sum_a a_z^{(a)} W^{(a)}_r1 W^{(a)}_r3
 *  \f]
 *  
 *  the leading index z refers to the number of laplace points.  
 * 
 * @param eabij representation of the energy denominator
 * @param idx   index of the common MO/AO index
 * */
NiceTensor<Nb> RI4Integral::ContractMOIndices
   (  NiceTensor<Nb>& eabij
   ,  const unsigned int idx
   )
{
   CanonicalTensor<Nb>* eabij_canon = dynamic_cast<CanonicalTensor<Nb>*>(eabij.GetTensor());
   const unsigned int nlap = eabij_canon->GetRank();

   return ContractMOIndices(eabij,idx,0,nlap);
}

/**
 *  @brief Contract two factor matrices along same index
 *
 *  Contracts two factor matrices along a common MO/AO index a with the
 *  energy denominator yielding a 3 index intermediate
 *
 *  \f[
 *    A_z^{r1,r3} = \sum_a a_z^{(a)} W^{(a)}_r1 W^{(a)}_r3 
 *  \f]
 *  
 *  the leading index z refers to the number of laplace points.  
 * 
 * @param eabij representation of the energy denominator
 * @param idx:  index of the common MO/AO index
 * @param ilapstart: Start index for laplace points
 * @param ilapend:   End index for laplace points
 * */
NiceTensor<Nb> RI4Integral::ContractMOIndices
   (  NiceTensor<Nb>& eabij
   ,  const unsigned int idx
   ,  const unsigned int ilapstart
   ,  const unsigned int ilapend
   ) 
{
   return ContractMOIndices(eabij,idx,ilapstart,ilapend,0,mRank[idx]);
}

/**
 *  @brief Contract two factor matrices along same index
 *
 *  Contracts two factor matrices along a common MO/AO index a with the
 *  energy denominator yielding a 3 index intermediate
 *
 *  \f[
 *    A_z^{r1,r3} = \sum_a a_z^{(a)} W^{(a)}_r1 W^{(a)}_r3 
 *  \f]
 *  
 *  the leading index z refers to the number of laplace points.  
 * 
 * @param eabij representation of the energy denominator
 * @param idx:  index of the common MO/AO index
 * @param ilapstart: Start index for laplace points
 * @param ilapend:   End index for laplace points
 * @param irankstart start index for one rank dimension
 * @param irankend   end index for one rank dimension
 * */
NiceTensor<Nb> RI4Integral::ContractMOIndices
   (  NiceTensor<Nb>& eabij
   ,  const unsigned int idx
   ,  const unsigned int ilapstart
   ,  const unsigned int ilapend
   ,  const unsigned int irankstart
   ,  const unsigned int irankend
   )
{
   return ContractMOIndices(eabij,idx,ilapstart,ilapend,irankstart,irankend,0,mRank[idx]);
}

/**
 *  @brief Contract two factor matrices along same index
 *
 *  Contracts two factor matrices along a common MO/AO index a with the
 *  energy denominator yielding a 3 index intermediate
 *
 *  \f[
 *    A_z^{r1,r3} = \sum_a a_z^{(a)} W^{(a)}_r1 W^{(a)}_r3 
 *  \f]
 *  
 *  the leading index z refers to the number of laplace points.  
 * 
 * @param eabij representation of the energy denominator
 * @param idx:  index of the common MO/AO index
 * @param ilapstart: start index for laplace points
 * @param ilapend:   end index for laplace points
 * @param irankstart start index for first rank dimension
 * @param irankend   end index for first rank dimension
 * @param jrankstart start index for second rank dimension
 * @param jrankend   end index for second rank dimension
 * */
NiceTensor<Nb> RI4Integral::ContractMOIndices
   (  NiceTensor<Nb>& eabij
   ,  const unsigned int idx
   ,  const unsigned int ilapstart
   ,  const unsigned int ilapend
   ,  const unsigned int irankstart
   ,  const unsigned int irankend
   ,  const unsigned int jrankstart
   ,  const unsigned int jrankend
   )
{

   // constants
   const bool locdbg = false;

   // Get mode matrices of energy denominator
   CanonicalTensor<Nb>* eabij_canon = dynamic_cast<CanonicalTensor<Nb>*>(eabij.GetTensor());
   Nb** eabij_modemat = eabij_canon->GetModeMatrices();
   const unsigned int nlap = ilapend - ilapstart;

   unsigned int nrank1 = irankend - irankstart;
   unsigned int nrank2 = jrankend - jrankstart;

   if (locdbg)
   {
      Mout << "nrank1 " << nrank1 << std::endl;
      Mout << "nrank2 " << nrank2 << std::endl;
   }

   // allocate memory for output and intermediate
   Nb* data(new Nb[nlap*nrank1*nrank2]);

   std::unique_ptr<Nb> intermed(new Nb[nrank1*nrank2]);
   std::unique_ptr<Nb> scalmat(new Nb[nrank1*mDims[idx]]);
   Nb* pdata = nullptr;  
 
   const unsigned int nsize = nrank1*nrank2;
   const unsigned int nlda = mDims[idx];
   
   unsigned int ilap_counter = 0;
   unsigned int irank_counter = 0;

   Nb* pWmat = mWmat[idx].get();
   for (unsigned int ilap = ilapstart; ilap < ilapend; ++ilap)
   {

      // scale one factor matrix with the values in eabij
      pdata = scalmat.get();
      irank_counter = 0;
      for (unsigned int irank = irankstart; irank < irankend; ++irank)
      {
         for (unsigned int idm = 0; idm < nlda; ++idm)
         {
            Nb dfac = eabij_modemat[idx][ilap*nlda + idm];
            pdata[irank_counter*nlda + idm] = pWmat[irank*nlda + idm] * dfac;
         }
         irank_counter += 1;
      }
      pdata = nullptr;

      // calculate block of output for ilap
      char nc = 'T'; char nt = 'N';
      Nb alpha = static_cast<Nb>(1.0); Nb beta  = static_cast<Nb>(0.0);
      int m = nrank2;
      int n = nrank1;     
      int k = mDims[idx];

      if (locdbg) Mout << " call to dgemm " << std::endl;

      unsigned int iadr = mDims[idx] * jrankstart;

      midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &alpha
                                   , &pWmat[iadr], &k
                                   , scalmat.get(), &k
                                   , &beta, intermed.get(), &m );
      
      // copy block
      memcpy(reinterpret_cast<void*>(&data[ilap_counter*nsize]),reinterpret_cast<void*>(intermed.get()),nsize*sizeof(Nb));
      ilap_counter += 1;
   }

   NiceTensor<Nb> result(
         new SimpleTensor<Nb>(
            std::vector<unsigned int>{static_cast<unsigned int>(nlap),
                                      static_cast<unsigned int>(nrank1),
                                      static_cast<unsigned int>(nrank2)}
     , data)
   );

   return result;
}

/**
 *  @brief Contract three tensors along same MO index
 *
 *  Contracts two tensors along a common MO/AO index a with the
 *  energy denominator yielding a 3 index intermediate
 *
 *  \f[
 *    A_z^{r1,r3} = \sum_a a_z^{(a)} W^{(a)}_r1 W^{(a)}_r3 
 *  \f]
 *  
 *  the leading index z refers to the number of laplace points.  
 * 
 * */
NiceTensor<Nb> RI4Integral::ContractMOIndices
   (  NiceTensor<Nb>& eabij
   ,  NiceTensor<Nb>& tensor1 
   ,  NiceTensor<Nb>& tensor2 
   ,  const unsigned int idx
   ,  const unsigned int idxdenom
   ,  const unsigned int ilapstart
   ,  const unsigned int ilapend
   )
{

   // constants
   const bool locdbg = false;

   // Get mode matrices of energy denominator
   CanonicalTensor<Nb>* eabij_canon = dynamic_cast<CanonicalTensor<Nb>*>(eabij.GetTensor());
   Nb** eabij_modemat = eabij_canon->GetModeMatrices();
   const unsigned int nlap = ilapend - ilapstart;
   std::vector<unsigned int> dims_eabij = eabij.GetDims();

   // Get mode matrices of both tensors
   CanonicalTensor<Nb>* tensor1_canon = dynamic_cast<CanonicalTensor<Nb>*>(tensor1.GetTensor());
   Nb** modemat1 = tensor1_canon->GetModeMatrices();
   const unsigned int nrank1 = tensor1_canon->GetRank();
   std::vector<unsigned int> dims1 = tensor1.GetDims(); 

   CanonicalTensor<Nb>* tensor2_canon = dynamic_cast<CanonicalTensor<Nb>*>(tensor2.GetTensor());
   Nb** modemat2 = tensor2_canon->GetModeMatrices();
   const unsigned int nrank2 = tensor2_canon->GetRank();
   std::vector<unsigned int> dims2 = tensor2.GetDims(); 

   // sanity checks 
   if (dims1 != dims2)
   {
      MIDASERROR("Dimensions of tensors must agree");
   }

   if (locdbg)
   {
      Mout << "nrank1 " << nrank1 << std::endl;
      Mout << "nrank2 " << nrank2 << std::endl;
   }

   // allocate memory for output and intermediate
   Nb* data(new Nb[nlap*nrank1*nrank2]);

   std::unique_ptr<Nb> intermed(new Nb[nrank1*nrank2]);
   std::unique_ptr<Nb> scalmat(new Nb[nrank1*dims1[idx]]);
   Nb* pdata = nullptr;  
 
   const unsigned int nsize = nrank1*nrank2;
   const unsigned int nlda = dims1[idx];
   
   unsigned int ilap_counter = 0;

   // sanity check 
   if (nlda != dims_eabij[idxdenom])
   {
      MIDASERROR("Dimensions do not agree for denominator and tensor!");
   }

   for (unsigned int ilap = ilapstart; ilap < ilapend; ++ilap)
   {

      // scale one factor matrix with the values in eabij
      pdata = scalmat.get();
      for (unsigned int irank = 0; irank < nrank1; ++irank)
      {
         for (unsigned int idm = 0; idm < nlda; ++idm)
         {
            Nb dfac = eabij_modemat[idxdenom][ilap*nlda + idm];
            pdata[irank*nlda + idm] = modemat1[idx][irank*nlda + idm] * dfac;
         }
      }
      pdata = nullptr;

      // calculate block of output for ilap
      char nc = 'T'; char nt = 'N';
      Nb alpha = static_cast<Nb>(1.0); Nb beta  = static_cast<Nb>(0.0);
      int m = nrank2;
      int n = nrank1;     
      int k = dims1[idx];

      midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &alpha
                                   , modemat2[idx], &k
                                   , scalmat.get(), &k
                                   , &beta, intermed.get(), &m );
      
      // copy block
      memcpy(reinterpret_cast<void*>(&data[ilap_counter*nsize]),reinterpret_cast<void*>(intermed.get()),nsize*sizeof(Nb));
      ilap_counter += 1;
   }

   NiceTensor<Nb> result(
         new SimpleTensor<Nb>(
            std::vector<unsigned int>{static_cast<unsigned int>(nlap),
                                      static_cast<unsigned int>(nrank1),
                                      static_cast<unsigned int>(nrank2)}
     , data)
   );

   return result;
}

/**
 *  @brief Contract a factor matrix with two tensor along same index
 *
 *  Contracts a factor matrix with two tensor along a common 
 *  MO/AO index a with energy denominator yielding a 3 index intermediate
 *
 *  \f[
 *    A_j,z^{r1,r3} = \sum_a a_z^{(a)} W^{(a)}_r1 B^{(a),j}_r3
 *  \f]
 *  
 *  the leading index z refers to the number of laplace points.  
 *
 * @param tensor      tensor which should be contrtacted with factor matrix idx
 * @param eabij       representation of the energy denominator
 * @param idx         index of the factor matrix
 *
 * @return Tensor containing the 4 index quanitity
 * */
NiceTensor<Nb> RI4Integral::ContractMOIndicesWith
   (  NiceTensor<Nb>& tensor
   ,  NiceTensor<Nb>& eabij
   ,  const unsigned int idx
   )
{

   CanonicalTensor<Nb>* eabij_canon = dynamic_cast<CanonicalTensor<Nb>*>(eabij.GetTensor());
   const unsigned int nlap = eabij_canon->GetRank();

   return ContractMOIndicesWith(tensor,eabij,idx,0,nlap,0,mRank[0],true);
}

/**
 *  @brief Contract a factor matrix with two tensor along same index
 *
 *  Contracts a factor matrix with two tensor along a common 
 *  MO/AO index a with energy denominator yielding a 3 index intermediate
 *
 *  \f[
 *    A_j,z^{r1,r3} = \sum_a a_z^{(a)} W^{(a)}_r1 B^{(a),j}_r3
 *  \f]
 *  
 *  the leading index z refers to the number of laplace points.  
 *
 * @param tensor      tensor which should be contrtacted with factor matrix idx
 * @param eabij       representation of the energy denominator
 * @param idx         index of the factor matrix
 * @param ilapstart   start index for Laplace points
 * @param ilapend     end index for Laplace points
 *
 * @return Tensor containing the 4 index quanitity
 * */
NiceTensor<Nb> RI4Integral::ContractMOIndicesWith
   (  NiceTensor<Nb>& tensor
   ,  NiceTensor<Nb>& eabij
   ,  const unsigned int idx
   ,  const unsigned int ilapstart
   ,  const unsigned int ilapend
   )
{
   CanonicalTensor<Nb>* eabij_canon = dynamic_cast<CanonicalTensor<Nb>*>(eabij.GetTensor());
   const unsigned int nlap = eabij_canon->GetRank();

   return ContractMOIndicesWith(tensor,eabij,idx,ilapstart,ilapend,0,mRank[0],true);

}

/**
 *  @brief Contract a factor matrix with two tensor along same index
 *
 *  Contracts a factor matrix with two tensor along a common 
 *  MO/AO index a with energy denominator yielding a 3 index intermediate
 *
 *  \f[
 *    A_z^{r1,r3} = \sum_a a_z^{(a)} W^{(a)}_r1 B^{(a),j}_r3
 *  \f]
 *  
 *  the leading index z refers to the number of laplace points.  
 *
 * @param tensor       tensor which should be contrtacted with factor matrix idx
 * @param eabij        representation of the energy denominator
 * @param idx          index of the factor matrix
 * @param ilapstart    start index for Laplace points
 * @param ilapend      end index for Laplace points
 * @param irankstart   start index for one rank
 * @param irankend     end index for one rank
 * @param isrank1full  if true first rank dimension is truncated otherwise second rank dimension
 *
 * @return Tensor containing the 4 index quanitity
 * */
NiceTensor<Nb> RI4Integral::ContractMOIndicesWith
   (  NiceTensor<Nb>& tensor
   ,  NiceTensor<Nb>& eabij
   ,  const unsigned int idx
   ,  const unsigned int ilapstart
   ,  const unsigned int ilapend
   ,  const unsigned int irankstart
   ,  const unsigned int irankend
   ,  const bool isrank1full
   )
{

   // constants
   const bool locdbg = false;

   // get dimensions on tensor
   std::vector<unsigned int> dims = tensor.GetDims();
   const unsigned int noct = dims[0]; // number of occupied orbitals in this batch

   // Get mode matrices of energy denominator
   CanonicalTensor<Nb>* eabij_canon = dynamic_cast<CanonicalTensor<Nb>*>(eabij.GetTensor());
   Nb** eabij_modemat = eabij_canon->GetModeMatrices();
   const unsigned int nlap = ilapend-ilapstart;

   // Get data on tensor
   Nb* mat = dynamic_cast<SimpleTensor<Nb>*>(tensor.GetTensor())->GetData();

   unsigned int nrank1;
   unsigned int nrank2;

   if (isrank1full)
   {
      nrank1 = mRank[idx];
      nrank2 = irankend - irankstart;
   }
   else
   {
      nrank1 = irankend - irankstart;
      nrank2 = mRank[idx];
   }

   // allocate memory for final tensor
   unsigned int ncopy = nlap*nrank1*nrank2;
   Nb* data = new Nb[noct*ncopy];

   for (unsigned int joct = 0; joct < noct; ++joct)
   {
      // allocate memory for output and intermediate
      Nb* block    = new Nb[nlap*nrank1*nrank2];
      Nb* intermed = new Nb[nrank1*nrank2];
      Nb* scalmat  = new Nb[nrank2*mDims[idx]];
      
      const unsigned int nsize = nrank1*nrank2;
      const unsigned int nlda = mDims[idx];
      
      unsigned int ilap_counter = 0;
      for (unsigned int ilap = ilapstart; ilap < ilapend; ++ilap)
      {
      
         // scale one factor matrix with the values in eabij
         unsigned int irank_counter = 0;
         unsigned int istart,iend;
         if (isrank1full)
         {
            istart = irankstart;
            iend = irankend;
         } 
         else
         {
            istart = 0;
            iend = nrank2;
         }

         for (unsigned int irank = istart; irank < iend; ++irank)
         {
            for (unsigned int idm = 0; idm < nlda; ++idm)
            {
               Nb dfac = eabij_modemat[idx][ilap*nlda + idm];
               scalmat[irank_counter*nlda + idm] = mat[joct*mRank[idx]*mDims[idx] + idm*mRank[idx] + irank] * dfac;
            }
            irank_counter += 1;
         }
      
         // calculate block of output for ilap
         char nc = 'T'; char nt = 'N';
         Nb alpha = static_cast<Nb>(1.0); Nb beta = static_cast<Nb>(0.0);
         int m = nrank1;
         int n = nrank2;     
         int k = mDims[idx];
         unsigned int iadr;
      
         if (isrank1full)
         {
            iadr = 0;
         }
         else
         {
            iadr = mDims[idx]*irankstart;
         }

         if (locdbg) Mout << "Do dgemm" << std::endl;

         Nb* pWmat = mWmat[idx].get();
         midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &alpha,
                                        &pWmat[iadr], &k,
                                        scalmat, &k,
                                        &beta, intermed, &m );
         
         if (locdbg) Mout << "copy data" << std::endl;

         // copy block
         memcpy(reinterpret_cast<void*>(&block[ilap_counter*nsize]),reinterpret_cast<void*>(intermed),nsize*sizeof(Nb));
         ilap_counter += 1;
      }

      if (locdbg) Mout << "copy data II" << std::endl;
      // add to full tensor
      memcpy(reinterpret_cast<void*>(&data[joct*ncopy]),reinterpret_cast<void*>(block),ncopy*sizeof(Nb));

      delete[] block;
      delete[] intermed;
      delete[] scalmat;
      
   }

   if (locdbg) Mout << "Form tensor" << std::endl;

   NiceTensor<Nb> result( new SimpleTensor<Nb>( std::vector<unsigned int>{static_cast<unsigned int>(noct),
                                                                          static_cast<unsigned int>(nlap),
                                                                          static_cast<unsigned int>(nrank2),
                                                                          static_cast<unsigned int>(nrank1)}, data)); 
   return result;
   
}

/**
 *  @brief Contract the given tensors along a common MO index
 *
 *  Contracts the given tensors along a common MO index 
 *
 *  \f[
 *    A_z^{r1,r3} = \sum_a a_z^{(a)} W^{(a)}_r1 B^{(a),j}_r3
 *  \f]
 *  
 *  the leading index z refers to the number of laplace points.  
 *
 * @param tensor       tensor which should be contrtacted with factor matrix idx
 * @param moint        MO integrals given as mode matrix
 * @param eabij        representation of the energy denominator
 * @param joct         current occupied index 
 * @param idxmo        MO index along with should be contracted 
 * @param ilapmo       MO index for the denominator 
 * @param ilap         Index of the Laplace point
 *
 * @return Tensor containing the 4 index quanitity
 * */
NiceTensor<Nb> RI4Integral::ContractMOIndicesOf
   (  NiceTensor<Nb>& tensor
   ,  NiceTensor<Nb>& moint
   ,  NiceTensor<Nb>& eabij
   ,  const unsigned int joct
   ,  const unsigned int idxmo
   ,  const unsigned int ilapmo
   ,  const unsigned int ilap
   )
{

   LOGCALL("ContractMOIndicesOf");

   // TODO adapt to use other idxmo rather than 0
   if (idxmo != 0) MIDASERROR("ContractMOIndicesOf: At the moment restricted to contraction along the virutal index!");

   // get dimensions on tensor
   std::vector<unsigned int> dims = tensor.GetDims();
   const unsigned int noct = dims[0]; // number of occupied orbitals in this batch
   const unsigned int nvir = dims[1]; // number of virtual orbitals in this batch
   const unsigned int naux = dims[2]; // number of auxiliary functions

   // Get mode matrices of energy denominator
   CanonicalTensor<Nb>* eabij_canon = dynamic_cast<CanonicalTensor<Nb>*>(eabij.GetTensor());
   Nb** eabij_modemat = eabij_canon->GetModeMatrices();

   // Get data on tensor
   Nb* mat = dynamic_cast<SimpleTensor<Nb>*>(tensor.GetTensor())->GetData();

   // Get data on tensor
   CanonicalTensor<Nb>* moint_canon = dynamic_cast<CanonicalTensor<Nb>*>(moint.GetTensor());
   Nb** modemat = moint_canon->GetModeMatrices(); 
   unsigned int nrank = moint_canon->GetRank();

   // allocate memory for final tensor
   //unsigned int ndata = noct*nrank*naux;
   unsigned int ndata = nrank*naux;
   Nb* data = new Nb[ndata];

   // allocate memory for output and intermediate
   std::unique_ptr<Nb> scalmat(new Nb[nrank*nvir]);
      
   // scale one factor matrix with the values in eabij
   Nb* scalmat_ptr = scalmat.get();
   for (unsigned int irank = 0; irank < nrank; ++irank)
   {
      for (unsigned int ivir = 0; ivir < nvir; ++ivir)
      {
         Nb dfac = eabij_modemat[ilapmo][ilap*nvir + ivir];
         scalmat_ptr[irank*nvir + ivir] = modemat[idxmo][irank*nvir + ivir] * dfac;
      }
   }

   {
      char nc = 'N'; char nt = 'N';
      Nb one = static_cast<Nb>(1.0); Nb zero = static_cast<Nb>(0.0);
      int m = naux;
      int n = nrank;     
      int k = nvir;
      unsigned int iadrin,iadrout;
      
      iadrin  = joct*nvir*naux;
      iadrout = 0; 

      midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                   , &mat[iadrin], &m
                                   , scalmat.get(), &k
                                   , &zero, &data[iadrout], &m );
   }

   NiceTensor<Nb> result( new SimpleTensor<Nb>( std::vector<unsigned int>{static_cast<unsigned int>(nrank),
                                                                          static_cast<unsigned int>(naux)}, data)); 
   return result;
   
}

NiceTensor<Nb> RI4Integral::ContractAlongOcc
   (  NiceTensor<Nb>& moint_ab
   ,  NiceTensor<Nb>& moint_cd
   ,  NiceTensor<Nb>& eabij
   ,  const unsigned int ilap
   )
{

   LOGCALL("Build I intermediate");

   // constants
   const bool locdbg = false;

   // get dimensions on tensor
   unsigned int noct = static_cast<unsigned int>(mTurboInfo.NOct()); 

   // Get mode matrices of energy denominator
   CanonicalTensor<Nb>* eabij_canon = dynamic_cast<CanonicalTensor<Nb>*>(eabij.GetTensor());
   Nb** eabij_modemat = eabij_canon->GetModeMatrices();

   // Get data on MO integrals 
   CanonicalTensor<Nb>* mointab_canon = dynamic_cast<CanonicalTensor<Nb>*>(moint_ab.GetTensor());
   Nb** modemat_ab = mointab_canon->GetModeMatrices(); 
   unsigned int nrankab = mointab_canon->GetRank();

   CanonicalTensor<Nb>* mointcd_canon = dynamic_cast<CanonicalTensor<Nb>*>(moint_cd.GetTensor());
   Nb** modemat_cd = mointcd_canon->GetModeMatrices(); 
   unsigned int nrankcd = mointcd_canon->GetRank();

   // allocate memory for final tensor
   unsigned int ndata = nrankab*nrankcd;
   Nb* data = new Nb[ndata];

   // allocate memory for output and intermediate
   std::unique_ptr<Nb> scalmat(new Nb[nrankab*noct]);
   Nb* scalmat_ptr  = scalmat.get();
      
   // scale one factor matrix with the values in eabij
   for (unsigned int irank = 0; irank < nrankab; ++irank)
   {
      for (unsigned int ioct = 0; ioct < noct; ++ioct)
      {
         Nb dfac = eabij_modemat[1][ilap*noct + ioct];
         scalmat_ptr[irank*noct + ioct] = modemat_ab[1][irank*noct + ioct] * dfac;
      }
   }

   {
      char nc = 'T'; char nt = 'N';
      Nb one = static_cast<Nb>(1.0); Nb zero = static_cast<Nb>(0.0);
      int m = nrankcd;
      int n = nrankab;     
      int k = noct;

      midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                   , modemat_cd[1], &k
                                   , scalmat.get(), &k
                                   , &zero, data, &m );
   }

   NiceTensor<Nb> result( new SimpleTensor<Nb>( std::vector<unsigned int>{static_cast<unsigned int>(nrankab),
                                                                          static_cast<unsigned int>(nrankcd)}, data)); 
   return result;
   
}


/**
 * @brief Returns pointer to the combination matrix
 * */
Nb* RI4Integral::GetCombinationMatrix
   (  
   ) const
{     
   return mMat12.get();
}  

/**
 * @brief Returns the start index of the rank for a given atompair 
 * */
unsigned int RI4Integral::GetStartRank
(
   const unsigned int pairindex
)
{
   return mPairRankOffset[pairindex];
}

/**
 * @brief Returns the maximal rank for all atom pairs 
 * */
unsigned int RI4Integral::GetMaxRank()
{
   unsigned int mxrank = *(std::max_element(mPairRanks.begin(), mPairRanks.end()));

   return mxrank;
}

/**
 * @brief Returns the end index of the rank for a given atompair 
 * */
unsigned int RI4Integral::GetEndRank
(
   const unsigned int pairindex
)
{
   unsigned int offset = mPairRankOffset[pairindex];

   return offset + mPairRanks[pairindex];
}

long RI4Integral::SaveRawData
   (  Nb* data
   ,  unsigned int iadrstart
   ,  unsigned int ndata
   ,  fstream& outBinFile
   )
{
   // constants
   const bool locdbg = false;

   // set to start position for this intermediate
   outBinFile.seekg(iadrstart);

   // write header
   outBinFile.write((const char*)&ndata, sizeof(unsigned int));

   // write data
   outBinFile.write((const char*)&data[0], ndata * sizeof(Nb));

   long iadr = outBinFile.tellp(); 

   return iadr;
}

void RI4Integral::ReadRawData
   (  fstream& inBinFile
   ,  long iadr
   ,  Nb* data
   ,  unsigned int &ndata
   )
{

   // set to start position for this intermediate
   inBinFile.seekg(iadr);

   // read header
   inBinFile.read( reinterpret_cast<char*>(&ndata), sizeof(unsigned int) );

   inBinFile.read( reinterpret_cast<char*>(&data[0]), ndata * sizeof(Nb) ); 

}

long RI4Integral::SaveIntermediate
   (  NiceTensor<Nb>& tensor
   ,  ofstream& outBinFile
   )
{
   // constants
   const bool locdbg = false;

   long iadr = outBinFile.tellp();

   // get handle on data
   SimpleTensor<Nb>* simplehandle=dynamic_cast<SimpleTensor<Nb>*>(tensor.GetTensor());
   Nb* data = simplehandle->GetData();
   const size_t ndata(tensor.TotalSize());
   
   // get dimension
   std::vector<unsigned int> dims = tensor.GetDims();

   int size = dims.size();
   // write header
   outBinFile.write((const char*)&size, sizeof(int));
   outBinFile.write((const char*)&dims[0], size * sizeof(unsigned int));

   // write data
   outBinFile.write((const char*)&data[0], ndata * sizeof(Nb));

   return iadr;
}

void RI4Integral::SaveIntermediate
   (  NiceTensor<Nb>& tensor
   ,  std::string label
   ,  const unsigned int abpair
   ,  const unsigned int cdpair
   ,  const unsigned int ilap 
   )
{
   // constants
   const bool locdbg = false;

   std::string file = label +      "-" + std::to_string(abpair) 
                            +      "-" + std::to_string(cdpair)  
                            + "-ilap-" + std::to_string(ilap) + ".bin";

   mFilesToDelte.insert(file);

   ofstream outBinFile;
   outBinFile.open(file, ios::out | ios::binary);

   // get handle on data
   SimpleTensor<Nb>* simplehandle=dynamic_cast<SimpleTensor<Nb>*>(tensor.GetTensor());
   Nb* data = simplehandle->GetData();
   const size_t ndata(tensor.TotalSize());

   // get dimension
   std::vector<unsigned int> dims = tensor.GetDims();

   if(outBinFile.is_open())
   {
      if (locdbg) std::cout << "File is open" << std::endl;

      // write header
      int size = dims.size();
      outBinFile.write((const char*)&size, sizeof(int));
      outBinFile.write((const char*)&dims[0], size * sizeof(unsigned int));

      if (locdbg) Mout << "ndata (out) " << ndata << std::endl;      

      // write data
      outBinFile.write((const char*)&data[0], ndata * sizeof(Nb));

      // close file
      outBinFile.close();
   
   }
   else 
   {
      MIDASERROR("Error while writing mode matrices to file!");
   }

   return;
}

NiceTensor<Nb> RI4Integral::ReadIntermediate
   (  ifstream& inBinFile
   ,  long iadr
   )
{
   // object for dimensions
   std::vector<unsigned int> dims;

   Nb* data;
   unsigned int ndata;


   // set to start position for this intermediate
   inBinFile.seekg(iadr);

   // read header
   int size;
   inBinFile.read( reinterpret_cast<char*>(&size), sizeof(int) );
   dims.resize(size);
   inBinFile.read( reinterpret_cast<char*>(&dims[0]), size*sizeof(unsigned int) );      

   // read data
   ndata = std::accumulate (dims.begin(), dims.end(), 1, std::multiplies<unsigned int>());
   
   data = new Nb[ndata];

   inBinFile.read( reinterpret_cast<char*>(&data[0]), ndata*sizeof(Nb) ); 


   return NiceTensor<Nb> (new SimpleTensor<Nb>(dims, data));
}

NiceTensor<Nb> RI4Integral::ReadIntermediate
   (  std::string label
   ,  const unsigned int abpair
   ,  const unsigned int cdpair
   ,  const unsigned ilap
   )
{
   // constants
   const bool locdbg = false;

   std::string file = label +      "-" + std::to_string(abpair) 
                            +      "-" + std::to_string(cdpair)  
                            + "-ilap-" + std::to_string(ilap) + ".bin";


   ifstream inBinFile;
   inBinFile.open(file, ios::in | ios::binary);

   // object for dimensions
   std::vector<unsigned int> dims;

   Nb* data = nullptr;
   unsigned int ndata = I_0;

   if(inBinFile.is_open())
   {
      if (locdbg) std::cout << "File is open" << std::endl;

      // read header
      int size;
      inBinFile.read( reinterpret_cast<char*>(&size), sizeof(int) );
      dims.resize(size);
      inBinFile.read( reinterpret_cast<char*>(&dims[0]), size*sizeof(unsigned int) );      

      if (locdbg)
      {
         Mout << "size " << size << std::endl;
         Mout << "dims " << dims << std::endl;
      }

      // read data
      ndata = std::accumulate (dims.begin(), dims.end(), 1, std::multiplies<unsigned int>());
      
      if (locdbg) Mout << "ndata " << ndata << std::endl;      

      data = new Nb[ndata];

      inBinFile.read( reinterpret_cast<char*>(&data[0]), ndata*sizeof(Nb) ); 

      // close file
      inBinFile.close();
   
   }
   else 
   {
      MIDASERROR("Error while reading mode matrices from file!");
   }

   return NiceTensor<Nb> (new SimpleTensor<Nb>(dims, data));
}

void RI4Integral::SaveIntegral
   (  std::string file
   )
{

   // constants
   const bool locdbg = false;

   ofstream outBinFile;
   outBinFile.open(file, ios::out | ios::binary);

   if(outBinFile.is_open())
   {
      if (locdbg) std::cout << "File is open" << std::endl;

      ////////////////////////////////////////////////////////////////////
      // write header
      ////////////////////////////////////////////////////////////////////

      // mDims
      {
         int size = mDims.size();
         outBinFile.write((const char*)&size, sizeof(int));
         outBinFile.write((const char*)&mDims[0], size * sizeof(unsigned int));
      }
      // mRank
      {
         int size = mRank.size();
         outBinFile.write((const char*)&size, sizeof(int));
         outBinFile.write((const char*)&mRank[0], size * sizeof(unsigned int));
      }
      // mPairRanks
      {
         int size = mPairRanks.size();
         outBinFile.write((const char*)&size, sizeof(int));
         outBinFile.write((const char*)&mPairRanks[0], size * sizeof(unsigned int));
      }
      // mPairRankOffset
      {
         int size = mPairRankOffset.size();
         outBinFile.write((const char*)&size, sizeof(int));
         outBinFile.write((const char*)&mPairRankOffset[0], size * sizeof(unsigned int));
      }
      // mPairList
      {
         int size = mPairList.size();
         outBinFile.write((const char*)&size, sizeof(int));
         outBinFile.write((const char*)&mPairList[0], size * sizeof(unsigned int));
      }
      // mNAtoms, mNdim, mNAux, mIsAtomicBatched, mHaveAOInts
      {
         outBinFile.write((const char*)&mNAtoms, sizeof(In));
         outBinFile.write((const char*)&mNdim,   sizeof(In));
         outBinFile.write((const char*)&mNAux,   sizeof(In));

         outBinFile.write((const char*)&mIsAtomicBatched, sizeof(bool));
         outBinFile.write((const char*)&mHaveAOInts,      sizeof(bool));
         outBinFile.write((const char*)&mIsMpacked,       sizeof(bool));
      }

      ////////////////////////////////////////////////////////////////////
      // write data
      ////////////////////////////////////////////////////////////////////

      // Factor Matrices
      {
         outBinFile.write((const char*)mWmat[0].get(), mRank[0]*mDims[0] * sizeof(Nb));
         outBinFile.write((const char*)mWmat[1].get(), mRank[1]*mDims[1] * sizeof(Nb));
         outBinFile.write((const char*)mWmat[2].get(), mRank[2]*mDims[2] * sizeof(Nb));
         outBinFile.write((const char*)mWmat[3].get(), mRank[3]*mDims[3] * sizeof(Nb));
      }
      // Mode Matrix for the auxiliary index
      if (mIsAtomicBatched) 
      {
         outBinFile.write((const char*)mAuxModeMat.get(), mNAux*mRank[0] * sizeof(Nb));
      }  
      else
      {
         MIDASERROR("At the moment only possible to store ABC integrals");
      }    

      // close file
      outBinFile.close();
   
   }
   else 
   {
      MIDASERROR("Error while writing mode matrices to file!");
   }


   return;
}

void RI4Integral::ReadIntegral
   (  std::string file
   )
{

   // constants
   const bool locdbg = false;

   ifstream inBinFile;
   inBinFile.open(file, ios::in | ios::binary);

   if(inBinFile.is_open())
   {
      if (locdbg) std::cout << "File is open" << std::endl;

      ///////////////////////////////////////////////////////////////
      // read header
      ///////////////////////////////////////////////////////////////

      // mDims
      if (locdbg) Mout << "Read mDims" << std::endl;
      {
         int size;
         inBinFile.read( reinterpret_cast<char*>(&size), sizeof(int) );
         mDims.resize(size);
         inBinFile.read( reinterpret_cast<char*>(&mDims[0]), size*sizeof(unsigned int) );
      }
      // mRank
      if (locdbg) Mout << "Read mRank" << std::endl;
      {
         int size;
         inBinFile.read( reinterpret_cast<char*>(&size), sizeof(int) );
         mRank.resize(size);
         inBinFile.read( reinterpret_cast<char*>(&mRank[0]), size*sizeof(unsigned int) );
      }
      // mPairRanks
      if (locdbg) Mout << "Read mPairRanks" << std::endl;
      {
         int size;
         inBinFile.read( reinterpret_cast<char*>(&size), sizeof(int) );
         mPairRanks.resize(size);
         inBinFile.read( reinterpret_cast<char*>(&mPairRanks[0]), size*sizeof(unsigned int) );
      }
      // mPairRankOffset
      if (locdbg) Mout << "Read mPairRankOffset" << std::endl;
      {
         int size;
         inBinFile.read( reinterpret_cast<char*>(&size), sizeof(int) );
         mPairRankOffset.resize(size);
         inBinFile.read( reinterpret_cast<char*>(&mPairRankOffset[0]), size*sizeof(unsigned int) );
      }
      // mPairList
      if (locdbg) Mout << "Read mPairList" << std::endl;
      {
         int size;
         inBinFile.read( reinterpret_cast<char*>(&size), sizeof(int) );
         mPairList.resize(size);
         inBinFile.read( reinterpret_cast<char*>(&mPairList[0]), size*sizeof(unsigned int) );
      }
      // mNAtoms, mNdim, mNAux, mIsAtomicBatched
      if (locdbg) Mout << "Read NAtoms..." << std::endl;
      {
         inBinFile.read( reinterpret_cast<char*>(&mNAtoms), sizeof(In) );
         inBinFile.read( reinterpret_cast<char*>(&mNdim),   sizeof(In) );
         inBinFile.read( reinterpret_cast<char*>(&mNAux),   sizeof(In) );

         inBinFile.read( reinterpret_cast<char*>(&mIsAtomicBatched), sizeof(bool));
         inBinFile.read( reinterpret_cast<char*>(&mHaveAOInts),      sizeof(bool));
         inBinFile.read( reinterpret_cast<char*>(&mIsMpacked),       sizeof(bool));
      }


      ///////////////////////////////////////////////////////////////
      // read data
      ///////////////////////////////////////////////////////////////

      // Factor Matrices
      {
         // Allocate memory
         if (locdbg) Mout << "Allocate memory " << mNdim << std::endl;
         for (unsigned int imode = 0; imode < mNdim; ++imode)
         {
            mWmat[imode].reset(new Nb[mRank[imode]*mDims[imode]]);
         }
         // read data
         inBinFile.read( reinterpret_cast<char*>(mWmat[0].get()), mRank[0]*mDims[0] * sizeof(Nb));
         inBinFile.read( reinterpret_cast<char*>(mWmat[1].get()), mRank[1]*mDims[1] * sizeof(Nb));
         inBinFile.read( reinterpret_cast<char*>(mWmat[2].get()), mRank[2]*mDims[2] * sizeof(Nb));
         inBinFile.read( reinterpret_cast<char*>(mWmat[3].get()), mRank[3]*mDims[3] * sizeof(Nb));
      }
      // Mode Matrix for the auxiliary index
      if (mIsAtomicBatched) 
      {
         if (locdbg) Mout << "AuxModeMat mNaux " << mNAux << " mRank[0] " << mRank[0] << std::endl;
         mAuxModeMat.reset( new Nb[mNAux*mRank[0]]);

         if (locdbg) Mout << "Read AuxModeMat" << std::endl;
         inBinFile.read( reinterpret_cast<char*>(mAuxModeMat.get()), mNAux*mRank[0] * sizeof(Nb));
      }  
      else
      {
         MIDASERROR("At the moment only possible to store ABC integrals");
      }    

      if (locdbg) Mout << "Finished reading integral file" << std::endl;

      // close file
      inBinFile.close();
   
   }
   else 
   {
      MIDASERROR("Error while reading mode matrices from file!");
   }

   return;
}



