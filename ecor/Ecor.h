/**
************************************************************************
* 
* @file                Ecor.h 
*
* Created:             18-03-2015
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Ecor class and function declarations.
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ECOR_H
#define ECOR_H

void EcorDrv();

#include <iostream> 
using std::ostream;
#include <string> 
using std::string;
#include <vector> 
using std::vector;
#include <string> 
using std::string;

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "input/EcorCalcDef.h"
//#include "input/ElecBasDef.h"
#include "ecor/ReferenceState.h"

class Ecor
{
  private:
//CalcDefs
     MolStruct*  mpMolStruct;

     EcorCalcDef*   mpEcorCalcDef;   ///< Pointer to input/setup info 
     ReferenceState mReferenceState;      ///< Reference state information: MO coeffs, AO Integrals, Eigenvalues

//Correlated wave function informatiion


  public:
    Ecor(EcorCalcDef* apCalcDef,  MolStruct* apMolStruct);      ///< Constructor 

    void Solve();                               // Solve cc equations
    void CalcRIMP2(const bool, const bool);     // Calculate RI-MP2 energy
    void BatchRIMP2();                          // Calculate RI-MP2 energy

    void CalcTHCMP2(const Nb, const Nb, const bool, const bool, const bool, const Nb);     // Calculate THC-MP2 energy

    Nb CorrelationEnergy(const std::vector<NiceTensor<Nb>>& t_amplitudes, const T1Integrals& integrals) const;  // Calculate correlation energy
    Nb CCSDt_correction(const std::vector<NiceTensor<Nb>>& ccsd_amplitudes) const;
    Nb NuclearEnergy() const;      // Calculate nuclear energy

//Getters
    const EcorCalcDef* GetEcorCalcDef() {return mpEcorCalcDef;}
};


//Non-member functions implemented in Ecor.cc
std::vector<NiceTensor<Nb>> starting_amplitudes(const unsigned int order,
      const T1Integrals& integrals,
      const std::vector<Nb>& e_vir,
      const std::vector<Nb>& e_occ,
      const bool& to_cp_tensors);

std::vector<NiceTensor<Nb>> quasi_newton_update(
      const std::vector<NiceTensor<Nb>>& error_vector,
      const std::vector<NiceTensor<Nb>>& denominators);

#endif
