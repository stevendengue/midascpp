/**
************************************************************************
* 
* @file                
*
* Created:            25-01-2017         
*
* Author:             Gunnar Schmitz (gunnar.schmitz@chem.au.dk)  
*
* Short Description:  Class for T2 ampltiudes
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <string>
#include <stdio.h>

#include "util/InterfaceOpenMP.h"

#include "ecor/T2Amplitude.h"
#include "tensor/EnergyDenominatorTensor.h"
#include "tensor/LaplaceQuadrature.h"
#include "ecor/IntermedCont.h"

namespace X=contraction_indices;

/**
 * * Constructor 
 * * */
T2Amplitude::T2Amplitude
   (  RI3Integral& bqij
   ,  RI3Integral& bqkl
   ,  std::map<std::string, std::vector<Nb>> orben
   ,  LaplaceQuadrature<Nb>&& lapquad 
   ,  const bool doBatching
   )
   :  RI4Integral(   bqij
                  ,  bqkl
                  ,  doBatching 
                  ,  doBatching
                  )
   ,  mOrben(orben)
   ,  mLapQuad(std::move(lapquad))
   
   
{
}



/**
 *  @brief Calculates the MP2 energy  
 *
 *  Calculates the MP2 energy exploting the RC/ABC format
 * 
 * @param onlySOS:  Only the SOS-MP2 energy is computed 
 * */
std::tuple<Nb,Nb> T2Amplitude::EMP2(const bool onlySOS)
{

   //constants 
   const bool locdbg = false;
   const bool prtener = true;


   Nb emp2j = 0.0;
   Nb emp2k = 0.0;

   if (mIsAtomicBatched) 
   {
      emp2j = ABCEMP2J();
      if (!onlySOS) emp2k = ABCEMP2K();
   }
   else
   {
      emp2j = RCEMP2J(); 
      if (!onlySOS) emp2k = RCEMP2K();
   }

   /*************************************************************************
                       sum up and return
    *************************************************************************/ 

   if (locdbg || prtener) 
   {
      Mout << "EMP2(J) " << emp2j << std::endl; 
      Mout << "EMP2(K) " << emp2k << std::endl; 
   }

   return std::make_tuple(emp2j, emp2k);
}

Nb T2Amplitude::RCEMP2J()
{
   //constants 
   const bool locdbg = false;
   const bool prtener = true;
   const bool UseSym = true;

   // sanity check (Have we MO integrals?)
   if (mHaveAOInts) 
   {
      MIDASERROR("T2Amplitude::RCEMP2 :>> I need MO integrals but only have AO integrals!");
   }

   Nb emp2j = C_0;

   // create energy denominator e_{ab}^{ij} = (e_a + e_b - e_i - e_j)^{-1}
   NiceTensor<Nb> eabij = new EnergyDenominatorTensor<Nb>(2, mOrben.at("v"), mOrben.at("oct"), mLapQuad);
   unsigned int nlap = mLapQuad.NumPoints();

   /*************************************************************************
                       Coulomb like contribution
    *************************************************************************/ 
 
   
   LOGCALL("RC-COULOMB");

   for (unsigned int ilap = 0; ilap < nlap; ++ilap)
   {
      Nb dlapwei = mLapQuad.Weights()[ilap];

      const unsigned int rankincr = mRank[0]; // ToDo: Is not working yet due to reuqired full contracion with combi matrix
      unsigned int jrankend;
      for (unsigned int jrank = 0; jrank < mRank[0]; jrank += rankincr)
      {
         jrankend = std::min<In>(jrank+rankincr,mRank[0]);

         if (locdbg) Mout << "Contract MO indices I for ilap " << ilap << std::endl; 

         NiceTensor<Nb> er1r3;
         {
            NiceTensor<Nb> ar1r3 = ContractMOIndices(eabij,0,ilap,ilap+1,jrank,jrankend);  // all virtual indices "a"
            NiceTensor<Nb> ir1r3 = ContractMOIndices(eabij,1,ilap,ilap+1,jrank,jrankend);  // all occupied indices "i"
            er1r3 = ar1r3 * ir1r3;  // form shortcut
         }

         NiceTensor<Nb> fr2r4;
         if (UseSym)
         {
            fr2r4 = er1r3;
         }
         else
         {
            NiceTensor<Nb> br2r4 = ContractMOIndices(eabij,2,ilap,ilap+1,jrank,jrankend);  // all virutal indices "b"
            NiceTensor<Nb> jr2r4 = ContractMOIndices(eabij,3,ilap,ilap+1,jrank,jrankend);  // all occupied indices "j"
            fr2r4 = br2r4 * jr2r4;  // form shortcut
         }

         if (locdbg) Mout << "Contract with combination matrix" << std::endl;

         // Carry out trasformation with Combination matrix M
         NiceTensor<Nb> er1r4 = ContractWithCombiMat(er1r3,2,static_cast<char>('N'));
         NiceTensor<Nb> fr1r4 = ContractWithCombiMat(fr2r4,1,static_cast<char>('N'));   

         if (locdbg) Mout << "Calculate coulomb like energy contribution" << std::endl;

         // Calculate Coulomb like energy contribution by contracting away all indices
         NiceTensor<Nb> enerj = (contract(er1r4[X::p,X::q,X::r],fr1r4[X::p,X::q,X::r]));
         emp2j += -2.0 * dlapwei * enerj.GetScalar();

      } 
   }

   return emp2j;
}

Nb T2Amplitude::RCEMP2K()
{
   //constants 
   const bool locdbg = false;
   const bool prtener = true;
   const bool UseSym = true;

   // create energy denominator e_{ab}^{ij} = (e_a + e_b - e_i - e_j)^{-1}
   NiceTensor<Nb> eabij = new EnergyDenominatorTensor<Nb>(2, mOrben.at("v"), mOrben.at("oct"), mLapQuad);
   unsigned int nlap = mLapQuad.NumPoints();

   Nb emp2k = C_0;

   /*************************************************************************
                       Exchange like contribution
    *************************************************************************/ 
   LOGCALL("RC-EXCHANGE");

   // define some shortcuts
   const unsigned int noct = mDims[3];
   const unsigned int nbtnlap = 1;

   for (unsigned int ilap = 0; ilap < nlap; ++ilap)
   {
      Nb dlapwei = mLapQuad.Weights()[ilap];

      if (locdbg) Mout << "Init H matrix for ilap " << ilap << std::endl; 

      // init H intermediate
      unsigned int nsize_h = nbtnlap*mRank[0]*mRank[0];
      Nb* data(new Nb[nsize_h]);
      for (unsigned int idx = 0; idx < nsize_h; ++idx)
      {
         data[idx] = 0.0;
      }

      NiceTensor<Nb> hr1r3z( new SimpleTensor<Nb>( std::vector<unsigned int>{static_cast<unsigned int>(nbtnlap),
                                                                             static_cast<unsigned int>(mRank[0]),
                                                                             static_cast<unsigned int>(mRank[0])}, data));
 
      for (unsigned int joct = 0; joct < noct; ++joct)
      {
         unsigned int jstart = joct;
         unsigned int jend = joct+1;
         std::vector<unsigned int> jmap = {joct};

         if (locdbg) Mout << "make A and C for ilap " << ilap << " and joct " << joct << std::endl; 

         // make A_jb^{r2} and C_ja^{r4} for this j
         NiceTensor<Nb> ajbr2 = CombineFactorMatrices(3,2,jstart,jend);
         //NiceTensor<Nb> cjar4 = CombineFactorMatrices(1,0,jstart,jend);

         if (locdbg) Mout << "make B and D for ilap " << ilap << " and joct " << joct << std::endl; 

         // construct B_jb^{r1} and D_ja^{r3} by transformation with combination matrix
         NiceTensor<Nb> bjbr1 = ContractWithCombiMat(ajbr2,2,static_cast<char>('N'));
         NiceTensor<Nb> djar3 = ContractWithCombiMat(ajbr2,2,static_cast<char>('T'));    

         if (locdbg) Mout << "make E and F for ilap " << ilap << " and joct " << joct << std::endl; 

         // make E_j^{r1,r3,z} and F_j^{r1,r3,z}
         const unsigned int rankincr = 500; // replace later with something user defined
         unsigned int jrankend;
         for (unsigned int jrank = 0; jrank < mRank[0]; jrank += rankincr)
         {
            jrankend = std::min<In>(jrank+rankincr,mRank[0]);

            NiceTensor<Nb> er1r3zj = ContractMOIndicesWith(bjbr1,eabij,2,ilap,ilap+1,jrank,jrankend,true);
            NiceTensor<Nb> fr1r3zj = ContractMOIndicesWith(djar3,eabij,0,ilap,ilap+1,jrank,jrankend,false);

            if (locdbg) Mout << "Add to H for ilap " << ilap << " and joct " << joct << std::endl; 

            // make H_{r1,r3,z}
            AddToH(hr1r3z,eabij,er1r3zj,fr1r3zj,jmap,ilap,ilap+1,jrank,jrankend);
            //AddToH(hr1r3z,eabij,er1r3zj,er1r3zj,jmap,ilap,ilap+1,jrank,jrankend);
         }
      }

      if (locdbg) Mout << "Contract MO indices for ilap " << ilap  << std::endl; 

      // make G_{r1,r3,z}
      NiceTensor<Nb> gr1r3z = ContractMOIndices(eabij,1,ilap,ilap+1);
  
      if (locdbg) Mout << "Calculate exchange like energy contribution" << std::endl;

      // calculate exchange like energy contribution
      NiceTensor<Nb> enerk = (contract(gr1r3z[X::p,X::q,X::r],hr1r3z[X::p,X::q,X::r]));
      emp2k += dlapwei * enerk.GetScalar();
   }

   return emp2k;
} 

Nb T2Amplitude::ABCEMP2J()
{
   // constants
   const bool locdbg = false;             // activate debug output
   const bool enforce_batching = false;   // enforce that the we test the batching (only for debuging)  

   // sanity check (Have we MO integrals?)
   if (!mHaveAOInts) 
   {
      MIDASERROR("T2Amplitude::ABCEMP2J :>> I need AO integrals but only have MO integrals!");
   }

   Nb emp2j = C_0;

   // info on OMP threads
   int nthreads = omp_get_max_threads();

   unsigned int irankstart, irankend, jrankstart, jrankend;

   // create energy denominator e_{ab}^{ij} = (e_a + e_b - e_i - e_j)^{-1} and do not absorb weights
   NiceTensor<Nb> eabij = new EnergyDenominatorTensor<Nb>(2, mOrben.at("v"), mOrben.at("oct"), mLapQuad);
   unsigned int nlap = mLapQuad.NumPoints();

   // unsigned int natompairs = mNAtoms * mNAtoms; // replace later with prescreend list 
   unsigned int natompairs = mPairList.size();

   LOGCALL("ABC-COULOMB");

   unsigned int lapinc = 1;

   //for (unsigned int ilap = 0; ilap < nlap; ++ilap)
   for (unsigned int ilap = 0; ilap < nlap; ilap += lapinc)
   {
      Nb dlapwei = mLapQuad.Weights()[ilap];

      if (locdbg) Mout << "Start with generation of E and F intermediates for ilap " << ilap << std::endl;

      /*************************************************************************
                        Construct E and F intermediates O(N^5) step
      *************************************************************************/ 
      std::string efile = "eintermed.bin";

      long filsize = 0;

      std::vector<long> elist(natompairs*(natompairs+1)/2);
      
      {
         LOGCALL("E-intermed");

         ofstream outeint;
         outeint.open(efile, ios::out | ios::binary);

         unsigned int mxrank = GetMaxRank();

         // We keep some of the intermediates in main memory to reduce I/O
         unsigned int nmaxmem = 1000 * 1024*128; // buffer of max 1000 MiB
         unsigned int maxbuff = std::min<In>(nmaxmem / (mxrank*mxrank), natompairs); 
         unsigned int inmem = 0;

         if (enforce_batching)
         {
            maxbuff = 1;
         }

         // define intermediates
         std::vector<NiceTensor<Nb>> eintermediates(maxbuff);
         std::vector<NiceTensor<Nb>> eintermed(nthreads);
         std::vector<std::tuple<unsigned int,unsigned int>> HaveInMem(maxbuff); 

         if (ilap == 0) Mout << " I have a buffer of size " << maxbuff << std::endl << std::endl;

         for (unsigned int abpair = 0; abpair < natompairs; ++abpair)
         {
            // get start and end rank for pair AB
            irankstart = GetStartRank(abpair);
            irankend   = GetEndRank(abpair);  
            unsigned int rankab = mPairRanks[abpair]; 

            if (rankab == 0) continue;

            std::tuple<unsigned int,unsigned int> indab = mPairList[abpair];
            unsigned int aat = std::get<0>(indab);
            unsigned int bat = std::get<1>(indab);

            if (locdbg) Mout << "(aat,bat) " << aat << " , " << bat << std::endl;

            // construct MO mode matrices for this atom pair
            NiceTensor<Nb> moint_ab = GetHalfMP2MOints(aat, bat, irankstart, irankend);

            #pragma omp parallel for  \
             private(jrankstart, jrankend) \
             shared(Mout, ilap, lapinc, eabij, elist, natompairs, \
                    HaveInMem, outeint, filsize, inmem, maxbuff, \
                    eintermediates, irankstart, irankend, moint_ab, abpair) 
            for (unsigned int cdpair = 0; cdpair <= abpair; ++cdpair)
            {
               // get start and end rank for pair CD 
               jrankstart = GetStartRank(cdpair);
               jrankend   = GetEndRank(cdpair);
               unsigned int rankcd = mPairRanks[cdpair]; 

               if (rankcd == 0) continue;

               int ithrd = omp_get_thread_num();
                  
               std::tuple<unsigned int, unsigned int> indcd = mPairList[cdpair];
               unsigned int cat = std::get<0>(indcd);
               unsigned int dat = std::get<1>(indcd);

               if (locdbg) Mout << "(cat,dat) " << cat << " , " << dat << std::endl;
            
               // construct MO mode matrices for this atom pair
               NiceTensor<Nb> moint_cd = GetHalfMP2MOints(cat, dat, jrankstart, jrankend);

               // compute A^{z,ABCD}_{r^{AB}r^{CD}} and I^{z,ABCD}_{r^{AB}r^{CD}} and combine
               { 
                  LOGCALL("AI-intermed");
                  NiceTensor<Nb> ar1r3 = ContractMOIndices(eabij, moint_ab, moint_cd, 0, 0, ilap ,ilap+lapinc);  // all virtual indices "a"
                  NiceTensor<Nb> ir1r3 = ContractMOIndices(eabij, moint_ab, moint_cd, 1, 1, ilap, ilap+lapinc);  // all occupied indices "i"
                  eintermed[ithrd] = ar1r3 * ir1r3;
               }

               #pragma omp critical
               {
                  eintermediates[inmem] = eintermed[ithrd];
                  HaveInMem[inmem] = std::make_tuple(abpair, cdpair);

                  // dump E^{z,ABCD}_{r^{AB}r^{CD}} and F^{z,ABCD}_{r^{AB}r^{CD}} to file
                  CheckMemBuffer(inmem, filsize, maxbuff, outeint, eintermediates, HaveInMem, elist);
               }
            }
         }

         // check if we still hold intermediates in memory
         #pragma omp critical
         {
            ClearMemBuffer(inmem, filsize, maxbuff, outeint, eintermediates, HaveInMem, elist);
         }

         // close files
         outeint.close();
      }

      if (ilap == 0) 
      {
         Mout << " Storage requirement for E and F intermediates:  " 
              << ( filsize * 8.0 / (1024*1024) ) << " MiB  " 
              << ( filsize * 8.0 / (1024*1024*1024) ) << " GiB" << std::endl;
      }

      if (locdbg) Mout << "Start with generation of G and H intermediates for ilap " << ilap << std::endl;
      Mout << "Start with generation of G and H intermediates for ilap " << ilap << std::endl;

      /*************************************************************************
                        Construct G and H intermediates 
      *************************************************************************/ 

      {
         ifstream ineint;
         ineint.open(efile, ios::in | ios::binary);

         LOGCALL("GH-intermed");

         Nb one  = static_cast<Nb>(1.0); 
         Nb zero = static_cast<Nb>(0.0);

         ////////////////////////////////////////////////////////////
         // allocate Memory
         ////////////////////////////////////////////////////////////

         // for full output arrays
         Nb* fullf = new Nb[mNAux*mNAux];  // data that later is packed to a NiceTensor

         // for inner loop output arrays
         unsigned int mxrank = GetMaxRank();
         std::unique_ptr<Nb> fdata(new Nb[mNAux*mxrank]);

         // init
         for (unsigned int i = 0; i<mNAux*mNAux; ++i)
         {
            fullf[i] = zero;
         }

         ////////////////////////////////////////////////////////////
         // Loop over CD,AB to build F and H intemediate
         ////////////////////////////////////////////////////////////
         const unsigned int mxbufab = std::min<In>(static_cast<unsigned int>(1000),natompairs);

         std::vector<NiceTensor<Nb>> eintbuf(mxbufab);

         // define Blocks of AB for that we can hold intermediates in memory
         unsigned int nblkab = (natompairs + mxbufab - 1) / mxbufab; 

         if (ilap == 0) Mout << " My buffer is large enough to batch everything in " << nblkab << " passes" << std::endl << std::endl;

         //for (unsigned int iblkcd = 0; iblkcd < nblkcd; ++iblkcd)
         {
            unsigned int cdstart = 0;
            unsigned int cdend   = natompairs;

            // actually construct G and H
            for (unsigned int cdpair = cdstart; cdpair < cdend; ++cdpair)
            {
               unsigned int rankcd = mPairRanks[cdpair];

               if (rankcd == 0) continue;

               // init local G and H intermediate
               Nb* fptr = fdata.get();
               for (unsigned int i = 0; i<mNAux*mxrank; ++i)
               {
                  fptr[i] = zero;
               }
               fptr = nullptr;

               for (unsigned int iblkab = 0; iblkab < nblkab; ++iblkab)
               {
                  unsigned int abstart = iblkab * mxbufab;
                  unsigned int abend   = std::min<In>((iblkab+1)*mxbufab, natompairs);

                  // read in intermediates for this block
                  for (unsigned int abpair = abstart; abpair < abend; ++abpair)
                  {
                     
                     unsigned int rankab = mPairRanks[abpair]; 

                     if (rankab == 0) continue;

                     unsigned int idx = abpair - abstart;

                     LOGCALL("IO to read EF");
                     // read E^{z,ABCD}_{r^{AB}r^{CD}} and F^{z,ABCD}_{r^{AB}r^{CD}}
                     if (abpair >= cdpair)
                     {
                        long iadre = elist[abpair*(abpair+1)/2+cdpair]; 

                        eintbuf[idx] = ReadIntermediate(ineint,iadre);
                     }
                     else
                     {
                        // we have only the transposed intermediates stored...
                        long iadre = elist[cdpair*(cdpair+1)/2+abpair]; 

                        NiceTensor<Nb> eintermed = ReadIntermediate(ineint,iadre);
                        eintbuf[idx] = eintermed[X::o,X::q,X::p];
                     }
                  }

                  // do computations for this block

// Note: Unfortunately no working OpenMP parallelization yet for gcc >= 9
#if defined(__GNUC__) && (__GNUC__ < 9)
                  #pragma omp parallel for default(none) \
                   shared(rankcd, zero, one, eintbuf, fdata, abstart, abend)
#endif
                  for (unsigned int abpair = abstart; abpair < abend; ++abpair)
                  {
                     unsigned int rankab = mPairRanks[abpair]; 

                     if (rankab == 0) continue;

                     // get handle on data so that we can directly perform dgemm on it
                     unsigned int idx = abpair - abstart;
                     SimpleTensor<Nb>* simplehandle1 = dynamic_cast<SimpleTensor<Nb>*>(eintbuf[idx].GetTensor());
                     Nb* edata = simplehandle1->GetData();

                     // built \bar{U}^{M,AB}_{r^{AB}k}
                     std::unique_ptr<Nb> umatmab(new Nb[mNAux*rankab]); 

                     {
                        // = sum_P U^{J}_Pk W_{r^{AB}P}^{(3)} 
                        char nc = 'N'; char nt = 'N';
                        int m = mNAux;  
                        int n = rankab;
                        int k = mNAux;  
                     
                        unsigned int irankstart = GetStartRank(abpair);
                        unsigned int iadr = irankstart * mNAux;
                        
                        Nb* auxmodemat = mAuxModeMat.get();
    
                        midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one,
                                                       mUmatJ.get(), &m, &auxmodemat[iadr], &m,
                                                       &zero, umatmab.get(), &m ); 
                     }


                     // F^{CD,z}_{r^{CD}k} = sum_{r^{AB}}  E^{z,ABCD}_{r^{AB}r^{CD}} \bar{U}^{M,AB}_{r^{AB}k}
#if defined(__GNUC__) && (__GNUC__ < 9)
                     #pragma omp critical
#endif
                     {
                        char nc = 'N'; char nt = 'T';
                        int m = mNAux;  
                        int n = rankcd;
                        int k = rankab; 
 
                        midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one,
                                                       umatmab.get(), &m, edata, &n,
                                                       &one, fdata.get(), &m );    

                     }

                  }
               }

               // built \bar{U}^{M,CD}_{r^{CD}k}
               std::unique_ptr<Nb> umatmcd(new Nb[mNAux*rankcd]); 

               {
                  // = sum_P U^{J}_Pk W_{r^{CD}P}^{(3)} 
                  char nc = 'N'; 
                  char nt = 'N';
                  int m = mNAux;  
                  int n = rankcd;
                  int k = mNAux;  
                  
                  unsigned int irankstart = GetStartRank(cdpair);
                  unsigned int iadr = irankstart * mNAux;
    
                  Nb* auxmodemat = mAuxModeMat.get();
    
                  midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one,
                                                 mUmatJ.get(), &m, &auxmodemat[iadr], &m,
                                                 &zero, umatmcd.get(), &m );
               }

   
               // F^{z}_{k_2 k_1} += sum_{r^{CD}} G^{CD,z}_{r^{CD}k_1} \bar{U}^{M,CD}_{r^{CD}k_2}
               {
                  char nc = 'N'; 
                  char nt = 'T';
                  int m = mNAux;  
                  int n = mNAux;
                  int k = rankcd; 
 
                  midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one,
                                                 fdata.get(), &m, umatmcd.get(), &m,
                                                 &one, fullf, &m );    

               }

            }
         }
         ineint.close();

         // pack everything together and contract
         std::vector<unsigned int> dims = {static_cast<unsigned int>(mNAux), 
                                           static_cast<unsigned int>(mNAux)};

         NiceTensor<Nb> fint(new SimpleTensor<Nb>(dims, fullf));

         NiceTensor<Nb> enerj = (contract(fint[X::p,X::q],fint[X::p,X::q]));
         emp2j += -2.0 * dlapwei * enerj.GetScalar();
      }
   }

   Mout << "emp2j " << emp2j << std::endl;
   return emp2j;
}

Nb T2Amplitude::ABCEMP2K()
{
   // constants
   const bool locdbg = false;

   // sanity check (Have we MO integrals?)
   if (!mHaveAOInts) 
   {
      MIDASERROR("T2Amplitude::ABCEMP2K :>> I need AO integrals but only have MO integrals!");
   }

   Nb emp2k = C_0;

   unsigned int irankstart, irankend, rankab;
   unsigned int jrankstart, jrankend;
   unsigned int rankcd;

   // create energy denominator e_{ab}^{ij} = (e_a + e_b - e_i - e_j)^{-1}
   NiceTensor<Nb> eabij = new EnergyDenominatorTensor<Nb>(2, mOrben.at("v"), mOrben.at("oct"), mLapQuad);
   std::vector<unsigned int> dims_eabij = eabij.GetDims();
   unsigned int nlap = mLapQuad.NumPoints();

   // Get pointer to mode matrices
   CanonicalTensor<Nb>* eabij_canon = dynamic_cast<CanonicalTensor<Nb>*>(eabij.GetTensor());
   Nb** eabij_modemat = eabij_canon->GetModeMatrices();

   // unsigned int natompairs = mNAtoms * mNAtoms; // replace later with prescreend list 
   unsigned int natompairs = mPairList.size();

   unsigned int nvir = mTurboInfo.NVirt();
   unsigned int noct = mTurboInfo.NOct();

   unsigned int mxrank = GetMaxRank();

   LOGCALL("ABC-EXCHANGE");

   Nb one  = static_cast<Nb>(1.0); 
   Nb zero = static_cast<Nb>(0.0);

   unsigned int noctlen = 1; 

   for (unsigned int joct = 0; joct < noct; ++joct)
   {
   
      // allocate Memory
      Nb* ejak = new Nb[mNAux*nvir*noctlen];

      for (unsigned int i = 0; i < mNAux*nvir*noctlen; ++i)
      {
         ejak[i] = zero;
      }

      {
         LOGCALL("E intemediate");

         for (unsigned int abpair = 0; abpair < natompairs; ++abpair)
         {
            // get start and end rank for pair AB
            irankstart = GetStartRank(abpair);
            irankend = GetEndRank(abpair);  
            rankab = mPairRanks[abpair]; 

            if (rankab == 0) continue;

            std::tuple<unsigned int, unsigned int> indab = mPairList[abpair];
            unsigned int aat = std::get<0>(indab);
            unsigned int bat = std::get<1>(indab);

            // Get Mode matrices in MO basis
            NiceTensor<Nb> moint_ab = GetHalfMP2MOints(aat, bat, irankstart, irankend);

            // make A_jb^{r}
            NiceTensor<Nb> ajbr = CombineFactorMatrices(moint_ab, joct, joct+noctlen);

            // built \bar{U}^{M,AB}_{r^{AB}k}
            std::unique_ptr<Nb> umatmab(new Nb[mNAux*rankab]); 
            {
               // = sum_P U^{J}_Pk W_{r^{AB}P}^{(3)} 
               char nc = 'N'; 
               char nt = 'N';
               int m = mNAux;  
               int n = rankab;
               int k = mNAux;  
           
               unsigned int irankstart = GetStartRank(abpair);
               unsigned int iadr = irankstart * mNAux;

               Nb* auxmodemat = mAuxModeMat.get();
                  
               midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                            , mUmatJ.get(), &m, &auxmodemat[iadr], &m
                                            , &zero, umatmab.get(), &m );  
            }

            /*******************************************************************************
               transform with decomposed combination matrix and add to result matrix
             ******************************************************************************/

            // get handle on data in A
            SimpleTensor<Nb>* simplehandle1 = dynamic_cast<SimpleTensor<Nb>*>(ajbr.GetTensor());
            Nb* adata = simplehandle1->GetData();

            // E^{AB}_{jbk} = sum_{r^{AB}}  A^{AB}_{jbr^{AB}} \bar{U}^{M,AB}_{r^{AB}k}
            {
               char nc = 'N'; 
               char nt = 'N';
               int m = mNAux;  
               int n = nvir*noctlen; // (R,V,O)  
               int k = rankab; 
 
               midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                            , umatmab.get(), &m, adata, &k
                                            , &one, ejak, &m );    
            }
             
         }
      }

      // pack everything together to a tensor
      std::vector<unsigned int> dims = {static_cast<unsigned int>(noctlen), 
                                        static_cast<unsigned int>(nvir), 
                                        static_cast<unsigned int>(mNAux)};
      NiceTensor<Nb> eint(new SimpleTensor<Nb>(dims, ejak));


      {
         LOGCALL("F intemediate");

         for (unsigned int ilap = 0; ilap < nlap; ++ilap)
         {
            Nb dlapwei = mLapQuad.Weights()[ilap];

            unsigned int nmaxmem = 8000 * 1024*128; // buffer of max 8000 MiB
            unsigned int maxbuff = std::min<In>(nmaxmem / (4*mxrank*mNAux), natompairs); 

            // define Blocks of AB for that we can hold intermediates in memory
            unsigned int nblk = (natompairs + maxbuff - 1) / maxbuff;

            if (joct == 0 && ilap == 0) Mout << "  My buffer is large enough to have " << nblk << " block of size " << maxbuff << std::endl;

            /************************************************************************************************
                         Calculate the F intermediates and thereby the energy 
             ***********************************************************************************************/

            int nthreads = omp_get_max_threads();

            // For buffered I/O
            IntermedCont xcont(maxbuff, natompairs, mNAux*mxrank, "xintermed.bin");
            IntermedCont econt(maxbuff, natompairs, mNAux*mxrank, "ezab2.bin");

            std::vector<std::shared_ptr<Nb>> umatmcd(maxbuff);
            std::vector<std::shared_ptr<Nb>> ezcd2(maxbuff);
            for (unsigned i=0; i < maxbuff; i++) 
            {
               umatmcd[i].reset(new Nb[mNAux*mxrank]);   // Buffer for reading \bar{U}^{M,CD}_{r^{CD}k}
               ezcd2[i].reset(new Nb[mNAux*mxrank]);     // Buffer for reading E^{M,CD}_{r^{CD}k}
            }

            // allocate memory
            std::unique_ptr<Nb[]> umatmab0(new Nb[mNAux*mxrank]);
            std::vector<std::unique_ptr<Nb[]>> frabcd(nthreads);
            std::vector<std::unique_ptr<Nb[]>> frcdab(nthreads);
            for (unsigned ithrd = 0; ithrd < nthreads; ithrd++)
            {
               frabcd[ithrd].reset(new Nb[mxrank*mxrank]);
               frcdab[ithrd].reset(new Nb[mxrank*mxrank]);
            }

            std::vector<NiceTensor<Nb>> moint_cd(natompairs);

            // Calculate F intemediates which depend on 4 atom labels
            for (unsigned int abpair = 0; abpair < natompairs; ++abpair)
            {
               // get start and end rank for pair AB
               irankstart = GetStartRank(abpair);
               irankend = GetEndRank(abpair);  
               rankab = mPairRanks[abpair]; 

               if (rankab == 0) continue;

               std::tuple<unsigned int,unsigned int> indab = mPairList[abpair];
               unsigned int aat = std::get<0>(indab);
               unsigned int bat = std::get<1>(indab);

               // Get Mode matrices in MO basis
               NiceTensor<Nb> moint_ab = GetHalfMP2MOints(aat,bat,irankstart,irankend);

               moint_cd[abpair] = moint_ab; // Save it for later use...

               // E^{AB,(a)}_{zjr^{AB}k} = sum_a E_{jak} W_{a r^{AB}}^{AB} a_{az}
               NiceTensor<Nb> ezjr1 = ContractMOIndicesOf(eint, moint_ab, eabij, 0, 0, 0, ilap);

               // Store E intermediate if possible in memory and if not at least on disk
               Nb* edata = dynamic_cast<SimpleTensor<Nb>*>(ezjr1.GetTensor())->GetData();
               econt.AddIntermediate(abpair, edata, mNAux*rankab); 
 
               if (locdbg) Mout << " built {U}^{M,AB}_{r^{AB}k}" << std::endl;

               // built \bar{U}^{M,AB}_{r^{AB}k}
               {
                  LOGCALL("Calc UmatAB");
                  // = sum_P U^{J}_Pk W_{r^{AB}P}^{(3)} 
                  char nc = 'N'; char nt = 'N';
                  int m = mNAux;  
                  int n = rankab;
                  int k = mNAux;  
                  
                  unsigned int irankstart = GetStartRank(abpair);
                  unsigned int iadr = irankstart * mNAux;
             
                  Nb* auxmodemat = mAuxModeMat.get();
             
                  midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                               , mUmatJ.get(), &m, &auxmodemat[iadr], &m
                                               , &zero, umatmab0.get(), &m );
               }

               // Store U intermediate if possible in memory and if not at least on disk
               xcont.AddIntermediate(abpair, umatmab0.get(), mNAux*rankab);

               for (unsigned int iblkcd = 0; iblkcd < nblk; ++iblkcd)
               {
                  const unsigned int cdstart = iblkcd * maxbuff;
                  const unsigned int cdend   = std::min<In>((iblkcd+1)*maxbuff, abpair+1);

                  // read in all intermediates which we can hold in memory
                  for (unsigned int cdpair = cdstart; cdpair < cdend; ++cdpair)
                  {
                     LOGCALL("IO to read E");

                     rankcd = mPairRanks[cdpair];
                     if (rankcd == 0) continue;

                     unsigned int idxcd = cdpair - cdstart;

                     // read E^{CD,(a)}_{zjr^{CD}k} } from Memory or disk
                     econt.GetIntermediate(ezcd2[idxcd], cdpair);
                  }

                  for (unsigned int cdpair = cdstart; cdpair < cdend; ++cdpair)
                  {
                     LOGCALL("IO to read U");

                     rankcd = mPairRanks[cdpair];
                     if (rankcd == 0) continue;
 
                     unsigned int idxcd = cdpair - cdstart;       // This is not that great since it allows holes in the array umatmcd
                                                                  // where we have unused data

                     // read \bar{U}^{M,CD}_{r^{CD}k} (if necessary)
                     xcont.GetIntermediate(umatmcd[idxcd], cdpair);
                  }

                  // do the computation steps
                  #pragma omp parallel for \
                   private(rankcd) \
                   shared(ilap, noct, joct, eabij_modemat, dlapwei, emp2k, zero, one, rankab,  \
                          ezjr1, frcdab, moint_ab, moint_cd, umatmcd, ezcd2, frabcd, umatmab0, \
                          abpair, eabij) 
                  for (unsigned int cdpair = cdstart; cdpair < cdend; ++cdpair)
                  {
                     rankcd = mPairRanks[cdpair];

                     if (rankcd == 0) continue;

                     int ithrd = omp_get_thread_num();         // Which thread is runing in this cycle?
                     unsigned int idxcd = cdpair - cdstart;    // What internal index I have in this cycle?

                     std::tuple<unsigned int,unsigned int> indcd = mPairList[cdpair];
                     unsigned int cat = std::get<0>(indcd);
                     unsigned int dat = std::get<1>(indcd);

                     // F^{ABCD,(b)}_{zjr^{AB}r^{CD}} = sum_k E^{AB,(b)}_{zjr^{AB}k} \bar{U}^{M,CD}_{r^{CD}k}
                     Nb* fdata1 = frabcd[ithrd].get();
                     {
                        LOGCALL("Calc F1");
                        // get handle on data in E^{ABCD,(b)}_{zjr^{AB}r^{CD}}
                        Nb* edata = dynamic_cast<SimpleTensor<Nb>*>(ezjr1.GetTensor())->GetData();

                        char nc = 'T';
                        char nt = 'N';
                        int m = rankcd;
                        int n = rankab;
                        int k = mNAux; 

                        midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one 
                                                     , umatmcd[idxcd].get(), &k, edata, &k 
                                                     , &zero, fdata1, &m );
                     }

                     //     ... and ...
                     // F^{CDAB,(a)}_{zjr^{CD}r^{AB}} = sum_k E^{CD,(a)}_{zjr^{CD}k} \bar{U}^{M,AB}_{r^{AB}k}
                     Nb* fdata2 = frcdab[ithrd].get(); 
                     {
                        LOGCALL("Calc F2");
                        // get handle on data in E^{ABCD,(b)}_{zjr^{AB}r^{CD}}
                        Nb* edata = ezcd2[idxcd].get(); 

                        char nc = 'T'; 
                        char nt = 'N';
                        int m = rankab;
                        int n = rankcd;
                        int k = mNAux; 

                        midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one 
                                                     , umatmab0.get(), &k, edata, &k 
                                                     , &zero, fdata2, &m );
                     }

                     // Form H^{ABCD}_{zr^{AB}r^{CD}} = F^{ABCD,(b)}_{zjr^{AB}r^{CD}} F^{CDAB,(a)}_{zjr^{CD}r^{AB}}
                     Nb dlap = eabij_modemat[3][ilap*noct + joct];  
                     NiceTensor<Nb> Hzjr = CreateHzjrIntermediate(rankab, rankcd, fdata1, fdata2, dlap, false);

                     // Form G^{ABCD}_{zr^{AB}r^{CD}} = \sum_i W^{AB}_ir W^{cd}_ir a_iz
                     NiceTensor<Nb> Gzjr = ContractAlongOcc(moint_ab, moint_cd[cdpair], eabij, ilap); 

                     //-------------------------------------------------------------------------------------+
                     // Calculate energy contribution
                     //-------------------------------------------------------------------------------------+
                     {
                        LOGCALL("Calc ener I");

                        // get handle on data in Hzjr and Gzjr
                        Nb* hdata = dynamic_cast<SimpleTensor<Nb>*>(Hzjr.GetTensor())->GetData();
                        Nb* gdata = dynamic_cast<SimpleTensor<Nb>*>(Gzjr.GetTensor())->GetData();

                        int n = rankab*rankcd;
                        int incx = 1;
                        int incy = 1;

                        Nb dadd = dlapwei * midas::lapack_interface::dot(&n, hdata, &incx, gdata, &incy);

                        #pragma omp atomic
                        emp2k += dadd;

                     }

                     // for non diagonal case calculate additional energy contribution
                     if (cdpair != abpair)
                     {
                        // Form H^{CDAB}_{zr^{CD}r^{AB}} = F^{ABCD,(b)}_{zjr^{AB}r^{CD}} F^{CDAB,(a)}_{zjr^{CD}r^{AB}}
                        NiceTensor<Nb> Hzjr = CreateHzjrIntermediate(rankcd, rankab, fdata2, fdata1, dlap, true);

                        // Calculate energy contribution
                        {
                           LOGCALL("Calc ener II");
                           
                           // get handle on data in Hzjr and Gzjr
                           Nb* hdata = dynamic_cast<SimpleTensor<Nb>*>(Hzjr.GetTensor())->GetData();
                           Nb* gdata = dynamic_cast<SimpleTensor<Nb>*>(Gzjr.GetTensor())->GetData();

                           int n = rankab*rankcd;
                           int incx = 1;
                           int incy = 1;

                           Nb dadd = dlapwei * midas::lapack_interface::dot(&n, hdata, &incx, gdata, &incy);

                           #pragma omp atomic
                           emp2k += dadd;
                        }
                     }

                     fdata1 = nullptr;
                     fdata2 = nullptr;

                  } // cdpair
               } // iblkcd
            } // abpair
         } // ilap
      }


   } // joct
   return emp2k;
}

void T2Amplitude::Update
(  std::map<std::string, std::vector<Nb>>& orben
   ,  const unsigned int nlap
   )
{
   // constants
   const bool locdbg = false;
   Nb one  = static_cast<Nb>(1.0); 
   Nb zero = static_cast<Nb>(0.0);

   // create energy denominator e_{ab}^{ij} = (e_a + e_b - e_i - e_j)^{-1}
   midas::tensor::LaplaceInfo lapinfo;
   lapinfo["TYPE"] = "FIXLAP";
   lapinfo["NUMPOINTS"] = nlap;
   detail::ValidateLaplaceInput(lapinfo);

//   LaplaceQuadrature<Nb> quad(nlap, 2, orben.at("v"), orben.at("oct"));
   LaplaceQuadrature<Nb> quad(lapinfo, 2, orben.at("v"), orben.at("oct"));
   NiceTensor<Nb> eabij = new EnergyDenominatorTensor<Nb>(2, orben.at("v"), orben.at("oct"), quad);

   Mout << " I will update the T2 amplitudes " << std::endl;

   unsigned int irankstart,irankend,jrankstart,jrankend;
   unsigned int natompairs = mPairList.size();

   const unsigned int lapinc = 1;

   for (unsigned int ilap = 0; ilap < nlap; ilap += lapinc)
   {
      Nb dlapwei = quad.Weights()[ilap];

      /*************************************************************************
                        Construct F intermediates
      *************************************************************************/ 
      {
         unsigned int mxrank = GetMaxRank();
         NiceTensor<Nb> eintermediate;

         for (unsigned int abpair = 0; abpair < natompairs; ++abpair)
         {
            unsigned int rankab = mPairRanks[abpair];
 
            // get start and end rank for pair AB
            irankstart = GetStartRank(abpair);
            irankend = GetEndRank(abpair);  

            if (rankab == 0) continue;

            std::tuple<unsigned int,unsigned int> indab = mPairList[abpair];
            unsigned int aat = std::get<0>(indab);
            unsigned int bat = std::get<1>(indab);

            if (locdbg) Mout << "(aat,bat) " << aat << " , " << bat << std::endl;

            // construct MO mode matrices for this atom pair
            NiceTensor<Nb> moint_ab = GetHalfMP2MOints(aat, bat, irankstart, irankend);

            // built \bar{U}^{M,AB}_{r^{AB}k}
            std::unique_ptr<Nb[]> umatmab(new Nb[mNAux*rankab]); 
            //GetUMatrix(umatmab.get(), rankab, abpair);

            // allocate memory for F_{r^{AB}k} 
            std::unique_ptr<Nb[]> fdata(new Nb[mNAux*rankab]);
            for (unsigned int i = 0; i < mNAux*rankab; ++i)
            {
               fdata[i] = zero;
            }

            for (unsigned int cdpair = 0; cdpair <= abpair; ++cdpair)
            {
               unsigned int rankcd = mPairRanks[cdpair]; 

               // get start and end rank for pair CD 
               jrankstart = GetStartRank(cdpair);
               jrankend = GetEndRank(cdpair);

               if (rankcd == 0) continue;

               std::tuple<unsigned int, unsigned int> indcd = mPairList[cdpair];
               unsigned int cat = std::get<0>(indcd);
               unsigned int dat = std::get<1>(indcd);

               if (locdbg) Mout << "(cat,dat) " << cat << " , " << dat << std::endl;
            
               // construct MO mode matrices for this atom pair
               NiceTensor<Nb> moint_cd = GetHalfMP2MOints(cat,dat,jrankstart,jrankend);
  
               // compute A^{z,ABCD}_{r^{AB}r^{CD}} and I^{z,ABCD}_{r^{AB}r^{CD}} and combine
               {
                  LOGCALL("AI-intermed");
                  NiceTensor<Nb> ar1r3 = ContractMOIndices(eabij, moint_ab, moint_cd, 0, 0, ilap, ilap + lapinc);  // all virtual indices "a"
                  NiceTensor<Nb> ir1r3 = ContractMOIndices(eabij, moint_ab, moint_cd, 1, 1, ilap, ilap + lapinc);  // all occupied indices "i"
                  eintermediate = ar1r3 * ir1r3;
               }

               // Contract with umat and accumulate for atom pair

               // built \bar{U}^{M,CD}_{r^{CD}k}
               std::unique_ptr<Nb> umatmcd(new Nb[mNAux*rankab]);
               //GetUMatrix(umatmcd.get(), rankcd, cdpair);
               
               {
                  // get handle on data so that we can directly perform dgemm on it
                  //SimpleTensor<Nb>* simplehandle1 = dynamic_cast<SimpleTensor<Nb>*>(eintermediate.GetTensor());
                  //Nb* edata = simplehandle1->GetData();
                  Nb* edata = eintermediate.StaticCast<SimpleTensor<Nb> >().GetData();
               
                  char nc = 'N'; 
                  char nt = 'T';
                  int m = mNAux;  
                  int n = rankab;
                  int k = rankcd; 
 
                  //midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one,
                  //                               umatmcd.get(), &m, edata, &n,
                  //                               &one, fdata.get(), &m );    

               }

            }

            // dump F_{r^{AB}k} for this AB
            // ToDO

         }

      }



   }
}


void T2Amplitude::CheckMemBuffer
   (  unsigned int &inmem
   ,  long &filsize  
   ,  unsigned int &maxbuff
   ,  ofstream &output
   ,  std::vector<NiceTensor<Nb>> &intermediates 
   ,  std::vector<std::tuple<unsigned int,unsigned int>> &HaveInMem
   ,  std::vector<long> &list
   )
{
   if (inmem < maxbuff - 1)
   {
      inmem += 1;
   }
   else
   {
      // write everything to file
      for (unsigned int i = 0; i < inmem + 1; ++i)
      {
         std::tuple<unsigned int,unsigned int> indabcd = HaveInMem[i];
         unsigned int abpair = std::get<0>(indabcd); 
         unsigned int cdpair = std::get<1>(indabcd);
   
         // write to file and save current position in output stream
         long iadre = SaveIntermediate(intermediates[i], output);
   
         list[abpair*(abpair+1)/2 + cdpair] = iadre;
   
         long ndata(intermediates[i].TotalSize());
         filsize += ndata ; 
      }
      // clear counter
      inmem = 0; 
   }

}


void T2Amplitude::ClearMemBuffer
   (  unsigned int &inmem
   ,  long &filsize  
   ,  unsigned int &maxbuff
   ,  ofstream &output
   ,  std::vector<NiceTensor<Nb>> &intermediates 
   ,  std::vector<std::tuple<unsigned int,unsigned int>> &HaveInMem
   ,  std::vector<long> &list
   )
{
   if (inmem > 0) 
   {
      for (unsigned int i = 0; i < inmem; ++i)
      {
         std::tuple<unsigned int,unsigned int> indabcd = HaveInMem[i];
         unsigned int abpair = std::get<0>(indabcd);
         unsigned int cdpair = std::get<1>(indabcd);
   
         // write to file and save current position in output stream
         long iadre = SaveIntermediate(intermediates[i], output);
   
         list[abpair*(abpair+1)/2 + cdpair] = iadre;
   
         long ndata(intermediates[i].TotalSize());
         filsize += ndata; 
      } 
      inmem = 0;
   }

}

