/**
************************************************************************
* 
* @file                EcorDrv.cc
*
* Created:             18-03-2015
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Driver for MidasCPP Ecor calculation 
 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// My headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "input/Input.h"
//#include "input/ElecBasDef.h"
//#include "bsplinebas/IntegralMachine.h"
#include "input/EcorCalcDef.h"
#include "ecor/Ecor.h"

/**
* Run scf calc 
* */
void EcorDrv()
{

  Mout << "\n\n";
  Out72Char(Mout,'$','$','$');
  Out72Char(Mout,'$',' ','$');
  OneArgOut72(Mout," Begin Electron correlation calculations ",'$');
  Out72Char(Mout,'$',' ','$');
  Out72Char(Mout,'$','$','$');
  Mout << "\n\n";


  for (In i_calc =0;i_calc<gEcorCalcDef.size();i_calc++) 
  {  

/*
***********   Create Ecor object  **************************
*/
    Mout << "\n\n\n Do Ecor:  " << gEcorCalcDef[i_calc].GetName() << " calculation \n" << endl;

    //Ecor ecor(gEcorCalcDef[i_calc]); ;// ,&gElecBas[i_basis],&gMolStruct);
    Ecor ecor(&gEcorCalcDef[i_calc],&gMolStruct);

/*
***********   Start Ecor calc  **************************
*/

    Timer time_it;

    if (gEcorCalcDef[i_calc].GetDoRIMP2())
    {
      // for better organisation seperate RIMP2 part from CC solver
      string ptalgo = gEcorCalcDef[i_calc].GetPTAlgo();     

      transform(ptalgo.begin(),ptalgo.end(),ptalgo.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(ptalgo.find(" ")!= ptalgo.npos) ptalgo.erase(ptalgo.find(" "),I_1);          // Delete ALL blanks

      bool useOvlp = gEcorCalcDef[i_calc].GetUseOvlpMetric();

      // GS: Earase the option Normal... especially it is pointing to a more specialized algorithm
      if 
         (  ptalgo == "NORMAL" 
         || ptalgo == "THC-MP2" 
         || ptalgo == "THC-SOS-MP2" 
         || ptalgo == "AB-THC-SOS-MP2"
         || ptalgo == "AB-THC-MP2"
         )
      {
         Nb thrsvd = gEcorCalcDef[i_calc].GetThrSVD();
         Nb threrr = gEcorCalcDef[i_calc].GetThrErr();

         Nb lapconv = gEcorCalcDef[i_calc].GetLapConv();

         //bool useOvlp = false;

         bool batched = false;
         bool onlySOS = false;

         if (ptalgo == "AB-THC-SOS-MP2" || ptalgo == "THC-SOS-MP2") 
         {
            onlySOS = true;
            if (ptalgo == "AB-THC-SOS-MP2") batched = true;
         }

         if (ptalgo == "AB-THC-MP2") batched = true;

         ecor.CalcTHCMP2(thrsvd,threrr,batched,onlySOS,useOvlp,lapconv);
      }
      else if (ptalgo == "RIMP2" || ptalgo == "RI-MP2")
      {
         ecor.CalcRIMP2(useOvlp, true);
      }
      else if (ptalgo == "BATCHED")
      {
         ecor.BatchRIMP2(); 
      }
      else
      {
         Mout << " Algorithm: " << gEcorCalcDef[i_calc].GetPTAlgo() << std::endl;
         MIDASERROR("Unkown algorithm for RI-MP2 calculation!");
      }
    }
    else
    {
      ecor.Solve();
    }

    string s_time = " CPU time used in solving electron correlation problem :";
    time_it.CpuOut(cout,s_time);

  }

  Mout << "\n\n";
  Out72Char(Mout,'$','$','$');
  Out72Char(Mout,'$',' ','$');
  OneArgOut72(Mout," Electron Correlation calculations done ",'$');
  Out72Char(Mout,'$',' ','$');
  Out72Char(Mout,'$','$','$');
}

