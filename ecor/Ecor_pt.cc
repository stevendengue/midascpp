/**
************************************************************************
* 
* @file                Ecor_pt.cc
*
* Created:             05-07-2016
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk)
*
* Short Description:   Implementing Ecor class methods.
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include<iostream>
#include<string>
#include<list>

// Link to standard headers:
#include "inc_gen/math_link.h" 

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/EcorCalcDef.h"
#include "ecor/Ecor.h"
#include "util/Timer.h"
#include "util/Io.h"
#include "tensor/EnergyDenominatorTensor.h"
#include "ecor/TurboInfo.h"
#include "ecor/mkexchange.h"
//#include "ecor/reducerank.h"
#include "ecor/DFIntegrals.h"
#include "ecor/T2Amplitude.h"


namespace X=contraction_indices;

void InvertSymMatrix
   (  Nb* pOutputMat
   ,  Nb* pInputMat
   ,  std::string algo
   ,  In ndim
   )
{

   // constants
   const bool locdbg = true;

   if (algo == "Eigen") 
   {
      // 1) Do eigen decomposition M = V D V^T
      // 2) Invert D  
      // 3) Sandwich D^{-1} between the eigen vectors

      std::unique_ptr<Nb> tmp1(new Nb[ndim*ndim]);
      std::unique_ptr<Nb> tmp2(new Nb[ndim*ndim]);
      std::unique_ptr<Nb> eig( new Nb[ndim]);

      memcpy(reinterpret_cast<void*>(tmp2.get()),
             reinterpret_cast<void*>(pInputMat), ndim * ndim * sizeof(Nb));

      char jobz = 'V';
      char uplo = 'U';
      int n = ndim;
      int lwork = -1;
      int info = 0; 
      Nb dlen;

      // first run to get dimension for work
      midas::lapack_interface::syev( &jobz, &uplo, &n, tmp2.get(), &n, eig.get(), &dlen, &lwork, &info);

      // allocate memory for work
      lwork = int(dlen);
      std::unique_ptr<Nb> work(new Nb[lwork]);

      // do eigen decomposition
      midas::lapack_interface::syev( &jobz, &uplo, &n, tmp2.get(), &n, eig.get(), work.get(), &lwork, &info);

      if (info != 0)
      {
         Mout << " info " << info << std::endl;
         MIDASERROR("Diagonalization failed in InvertSymMatrix!");
      }

      Nb* eig_ptr = eig.get();
      // Invert the diagonal matrix 
      for (unsigned int iaux = 0; iaux < ndim; ++iaux)
      {
         eig_ptr[iaux] = 1.0 / eig_ptr[iaux];
      }

      // multiply the eigenvector with the diagonal matrix of eigenvalues
      size_t counter = 0;
      Nb* tmp1_ptr = tmp1.get();
      Nb* tmp2_ptr = tmp2.get();
      for (int j = 0; j < ndim; ++j) 
      {
         for (int i = 0; i < ndim; ++i)
         {
            tmp1_ptr[j*ndim + i] = tmp2_ptr[counter++] * eig_ptr[j];
         }
      }
     
      // Build now the output matrix 
      {
         char nc = 'N';
         char nt = 'T';
         int m = ndim;
         int n = ndim;
         int k = ndim;

         Nb one  = static_cast<Nb>(1.0); 
         Nb zero = static_cast<Nb>(0.0);

         midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one 
                                      , tmp1.get(), &m, tmp2.get(), &m 
                                      , &zero, pOutputMat, &m );   
      }
   }
   else
   {
      MIDASERROR("Unknown algorithm!");
   }

   return;
}



/**
* Calculate THC-MP2 energy 
* */
void Ecor::CalcTHCMP2
   (  const Nb thrsvd
   ,  const Nb threrr
   ,  const bool dobatching
   ,  const bool onlySOS
   ,  const bool useOvlp 
   ,  const Nb lapconv
   )
{
   // constants 
   const bool locdbg = true;
   const bool doread = false;

   // Spin-Scaling Factor
   const Nb scscos = 1.2; 
   const Nb scscss = 1.0/3.0;
   const Nb soscos = 1.3;
   const Nb soscss = 0.0;

   // locals
   Nb emp2 = C_0;
   Nb emp2_scs = C_0;
   Nb emp2_sos = C_0;
   const midas::tensor::TensorDecompInfo::Set decompinfoset  = mpEcorCalcDef->GetDecompInfoSet();
   const midas::tensor::LaplaceInfo::Set      laplaceinfoset = mpEcorCalcDef->GetLaplaceInfoSet();
   std::map<std::string, std::vector<Nb>> orben;

   midas::tensor::LaplaceInfo laplaceinfo;

   if (!laplaceinfoset.empty())
   {
      // get the first one later we will introduce to use a specific one
      laplaceinfo = *(laplaceinfoset.begin());
   }
   else // use defaults
   {
      laplaceinfo["TYPE"] = "BESTLAP";
      laplaceinfo["LAPCONV"] = 1.e-4;
      laplaceinfo["MINPOINTS"] = 2;
      laplaceinfo["MAXPOINTS"] = 8;
      laplaceinfo["HISTOGRAMDELTA"] = 0.1;
      detail::ValidateLaplaceInput(laplaceinfo);
   }

   /************************************************************
   // Init Turbomole info
   ************************************************************/
   TurboInfo turboinfo;
   turboinfo.ReadInfo("MIDAS_3ATX.aux");

   // set some shortcuts
   In nocc  = turboinfo.NOcc();
   In noct  = turboinfo.NOct();
   In nvir  = turboinfo.NVirt();
   In nfroz = nocc - noct;

   /************************************************************
   // Get CMO coefficients
   ************************************************************/
   const NiceTensor<Nb> cmocc = turboinfo.ReadCmoct("MIDAS_CMO");  // active occupied orbitals
   const NiceTensor<Nb> cmvir = turboinfo.ReadCmvir("MIDAS_CMO");  // virtual orbtials (yet no truncation in vir. space)

   /************************************************************
   // Get orbital enegies
   ************************************************************/
   ifstream orben_input("MIDAS_ORBEN", ios::in | ios::binary);
   orben_input.exceptions( std::ios::badbit | std::ios::failbit );

   Nb tmp_energy_occ [nocc];
   Nb tmp_energy_oct [noct];
   Nb tmp_energy_vir [nvir];
   
   orben_input.read(reinterpret_cast<char*>(tmp_energy_occ), nocc *sizeof(Nb) );
   orben_input.read(reinterpret_cast<char*>(tmp_energy_vir), nvir *sizeof(Nb) );

   orben["o"].assign(tmp_energy_occ, tmp_energy_occ + nocc);
   orben["v"].assign(tmp_energy_vir, tmp_energy_vir + nvir);

   // active occupied and virutals
   for (int iocc = 0; iocc<noct; ++iocc)
   {
      tmp_energy_oct[iocc] = tmp_energy_occ[iocc+nfroz];
   }
   orben["oct"].assign(tmp_energy_oct, tmp_energy_oct + noct);

   /************************************************************
   // Main algorithm 
   ************************************************************/

   Nb enerj = C_0;
   Nb enerk = C_0;

//   if (false)
//   {
//
//      Nb shift = 3.1;
//
//      // build energy denomitar and test Laplace 
//      Nb* dabij(new Nb[noct*noct*nvir*nvir]);
//      for (int ioct = 0; ioct < noct; ioct++)
//      {
//         for (int joct = 0; ioct < noct; ioct++)
//         {
//            for (int moa = 0; moa < nvir; moa++)
//            {
//               for (int mob = 0; mob < nvir; mob++)
//               {
//                  int idx = moa*noct*nvir*noct + ioct*noct*nvir + mob*noct + joct;
//                  dabij[idx] = 1.0 / ( orben.at("v")[moa] + orben.at("v")[mob] 
//                                     - orben.at("oct")[ioct] - orben.at("oct")[joct] 
//                                     - shift   );
//               }
//            } 
//         }
//      }
//
//      LaplaceQuadrature<Nb> quadlap(laplaceinfo, 2, orben.at("v"), orben.at("oct"), shift);
//      nlap = quadlap.NumPoints();
//      
//      NiceTensor<Nb> eabij_lap = new EnergyDenominatorTensor<Nb>(2, orben.at("v"), orben.at("oct"), quadlap, true);
//
//      std::vector<unsigned int> dims = {static_cast<unsigned int>(nvir), 
//                                        static_cast<unsigned int>(noct),
//                                        static_cast<unsigned int>(nvir),
//                                        static_cast<unsigned int>(noct)};
//
//      NiceTensor<Nb> eabij(new SimpleTensor<Nb>(dims, dabij));
//
//      NiceTensor<Nb> diff = eabij - eabij_lap;
//
//      Mout << "||Delta|| " << diff.Norm() << std::endl;  
//   }

   const bool do_cp = true;      
   const bool use_risvd = true;  // use the RI-SVD for initial CP like decomp.
   
   const Nb thrcp = 1.e-4;       // not used if decompinfo provided

   // read in all 3 index integrals
   Timer tim_ri;

   if (useOvlp) 
   {
      Mout << " Start with the calculation of 3 index integrals using overlap metric!" << std::endl;
   }
   else
   {
      Mout << " Start with the calculation of 3 index integrals using Coulomb metric!" << std::endl;
   }

   RI3Integral bqij(true, useOvlp, thrcp, thrsvd, threrr, turboinfo, decompinfoset, mpMolStruct); 

   tim_ri.CpuOut(Mout, " Time for decomposition to THC format (CPU)  ");
   tim_ri.WallOut(Mout," Time for decomposition to THC format (WALL) ");

   // Transform to MO basis
   Timer tim_motraf;

   if (!dobatching)
   {
      bqij.Transform(0,cmvir[X::q,X::p]);
      bqij.Transform(1,cmocc[X::q,X::p]);
   }

   tim_motraf.CpuOut(Mout, " Time for transformation to MO basis (CPU)  ");
   tim_motraf.WallOut(Mout," Time for transformation to MO basis (WALL) ");
   
   // Carry out additional compression
   Timer tim_reduce;
   //bqij.ReduceRank();

   tim_reduce.CpuOut(Mout, " Time for second reduction (CPU)  ");
   tim_reduce.WallOut(Mout," Time for second reduction (WALL) ");
   

   // construct exchange integrals 
   Timer tim_tiajb;

//   unsigned int nlap = 6;        // TODO set this by input 
   unsigned int maxlap = 20;     //

   // Determine Laplace quadrature 
   LaplaceQuadrature<Nb> quadlap(laplaceinfo, 2, orben.at("v"), orben.at("oct"));
//   nlap = quadlap.NumPoints();

   T2Amplitude tiajb(bqij, bqij, orben, std::move(quadlap), dobatching);   

   tim_tiajb.CpuOut(Mout, " Time for construction of MP2 amplitudes (CPU)  ");
   tim_tiajb.WallOut(Mout," Time for construction of MP2 amplitudes (WALL) ");

   // calculate THC-MP2 energy
   Timer tim_ener;

   std::tuple<Nb,Nb> ener = tiajb.EMP2(onlySOS);
   enerj = std::get<0>(ener);
   enerk = std::get<1>(ener);

   tim_ener.CpuOut(Mout, " Time for energy calculation (CPU)  ");
   tim_ener.WallOut(Mout," Time for energy calculation (WALL) ");


   /************************************************************
   // Output results 
   ************************************************************/

   if (!onlySOS) 
   {
      // calculate "normal" (THC)-MP2 energy
      emp2 = enerj + enerk;

      // calculate SCS-MP2 and SOS-MP2 energy
      Nb ener_os = enerj / 2.0;
      Nb ener_ss = (enerj / 2.0) + enerk;

      emp2_scs = scscos * ener_os + scscss * ener_ss ;
      emp2_sos = soscos * ener_os;

      Out72Char(Mout,'|','*','|');
      Mout << std::endl; 
      Mout << "      THC-MP2 Energy: " << emp2 << std::endl;
      Mout << std::endl;
      Mout << "      SCS-MP2 Energy: " << emp2_scs << std::endl;
      Mout << std::endl;
      Mout << "      SOS-MP2 Energy: " << emp2_sos << std::endl;
      Mout << std::endl; 
      Out72Char(Mout,'|','*','|');
      Mout << std::endl;
   }
   else
   {
      // calculate SOS-MP2 energy
      Nb ener_os = enerj / 2.0;

      emp2_sos = soscos * ener_os;

      Out72Char(Mout,'|','*','|');
      Mout << std::endl;
      Mout << "      SOS-MP2 Energy: " << emp2_sos << std::endl;
      Mout << std::endl; 
      Out72Char(Mout,'|','*','|');
      Mout << std::endl;
      
   }

}

/**
* Calculate RI-MP2 energy 
* */
void Ecor::CalcRIMP2
   (  const bool useOvlp
   ,  const bool useRobust
   )
{
   // constants 
   const bool locdbg = true;
   const bool doprtmat = false;
   const bool useTHC = false;

   // locals
   Nb emp2 = C_0;
   const std::map<std::string, std::vector<Nb>>& orbital_energies = mReferenceState.GetEnergies();
   
   const bool& do_cp = mpEcorCalcDef->GetDoCP();
   const midas::tensor::TensorDecompInfo::Set decompinfoset = mpEcorCalcDef->GetDecompInfoSet();

   NiceTensor<Nb> bqint;
   NiceTensor<Nb> sqint;
   NiceTensor<Nb> vpqinv;

   std::map<std::string, std::vector<Nb>> orben;

   /************************************************************
   // Calculate Reference state energy and nuclear energy 
   ************************************************************/
   //Nb ref_energy = mReferenceState.Energy(); 
   //Nb nuc_energy = NuclearEnergy(); 

   //Nb hf_energy = ref_energy + nuc_energy;
   Nb hf_energy = 0.0;

   /************************************************************
   // Init Turbomole info
   ************************************************************/
   TurboInfo turboinfo;
   turboinfo.ReadInfo("MIDAS_3ATX.aux");

   // set some shortcuts
   In nocc  = turboinfo.NOcc();
   In noct  = turboinfo.NOct();
   In nvir  = turboinfo.NVirt();
   In nfroz = nocc - noct;
   In naux  = turboinfo.NAux();

   /************************************************************
   // Get CMO coefficients
   ************************************************************/
   const NiceTensor<Nb> cmocc = turboinfo.ReadCmoct("MIDAS_CMO");  // active occupied orbitals
   const NiceTensor<Nb> cmvir = turboinfo.ReadCmvir("MIDAS_CMO");  // virtual orbtials (yet no truncation in vir. space)

   /************************************************************
   // Get orbital enegies
   ************************************************************/
   ifstream orben_input("MIDAS_ORBEN", ios::in | ios::binary);
   orben_input.exceptions( std::ios::badbit | std::ios::failbit );

   Nb tmp_energy_occ [nocc];
   Nb tmp_energy_oct [noct];
   Nb tmp_energy_vir [nvir];
   
   orben_input.read(reinterpret_cast<char*>(tmp_energy_occ), nocc *sizeof(Nb) );
   orben_input.read(reinterpret_cast<char*>(tmp_energy_vir), nvir *sizeof(Nb) );

   orben["o"].assign(tmp_energy_occ, tmp_energy_occ + nocc);
   orben["v"].assign(tmp_energy_vir, tmp_energy_vir + nvir);

   // active occupied and virutals
   for (int iocc = 0; iocc<noct; ++iocc)
   {
      tmp_energy_oct[iocc] = tmp_energy_occ[iocc+nfroz];
   }
   orben["oct"].assign(tmp_energy_oct, tmp_energy_oct + noct);


   /************************************************************
   // Build integrals and calculate RI-MP2 energy
   ************************************************************/
   
   // do not decompose (P|Q)^(-1) since it is in our case usally a small matrix
   // and contractions in CP format only will substanially increase the rank 
   if (useOvlp || useRobust)
   {

      if (useRobust)
      {
         Mout << "I will use robust density fitting!" << std::endl << std::endl;
      }
      else
      {
         Mout << "I will use the overlap metric for density fitting!" << std::endl << std::endl;
      }

      Nb one  = static_cast<Nb>(1.0); 
      Nb zero = static_cast<Nb>(0.0);

      NiceTensor<Nb> jpqmat = turboinfo.ReadVPQ("MIDAS_VPQ");
      NiceTensor<Nb> spqmat = turboinfo.ReadVPQ("MIDAS_SPQ");

      Nb* jpqmat_ptr = dynamic_cast<SimpleTensor<Nb>*>(jpqmat.GetTensor())->GetData();
      Nb* spqmat_ptr = dynamic_cast<SimpleTensor<Nb>*>(spqmat.GetTensor())->GetData();

      // Invert S_PQ matrix by eigen decomposition
      std::unique_ptr<Nb> sinv(new Nb[naux*naux]);
      std::string algo = "Eigen";
      InvertSymMatrix(sinv.get(), spqmat_ptr, algo, naux);            

      // C_PQ = S_PQ^(-1) J_PQ
      std::unique_ptr<Nb> cpq(new Nb[naux*naux]);
      {
         char nc = 'N'; 
         char nt = 'N';
         int m = naux;
         int n = naux;
         int k = naux;

         midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                      , sinv.get(), &k, jpqmat_ptr, &k
                                      , &zero, cpq.get(), &m );
      }

      // ~J_PQ = C_PQ S_PQ^(-1)
      Nb* rimatrix = new Nb[naux*naux];
      {
         char nc = 'N'; 
         char nt = 'N';
         int m = naux;
         int n = naux;
         int k = naux;

         midas::lapack_interface::gemm( &nc, &nt, &m, &n, &k, &one
                                      , cpq.get(), &k, sinv.get(), &k
                                      , &zero, rimatrix, &m );
      }

      std::vector<unsigned int> dims = {static_cast<unsigned int>(naux),
                                        static_cast<unsigned int>(naux)};
                                        
      NiceTensor<Nb> newmat(new SimpleTensor<Nb>(dims, rimatrix));
      vpqinv = newmat;

   }
   else
   {
      Mout << "I will use the coulomb metric for density fitting!" << std::endl << std::endl;

      vpqinv = turboinfo.ReadVPQ("MIDAS_VPQINV");
   }

   // Test and cross fingers
   if (do_cp) 
   {
      if (!decompinfoset.empty())
      {
         int ncount = 1;
         for(auto& decompinfo : decompinfoset)
         {
            if (ncount > 1)
            {
               Mout << "At the moment Ecor can only handle one decomposition info at a time" << std::endl;
               MIDASERROR(" Check your input please ");
            }
            else
            {
               midas::tensor::TensorDecomposer decomposer(decompinfo);
  
               std::string file = "MIDAS_3ATX";
               if (useOvlp) file = "MIDAS_SQIJ"; 
               NiceTensor<Nb> full_tensor = turboinfo.Read3RI(file); 

               //bqint =  decomposer.Decompose(full_tensor);  

               NiceTensor<Nb> intermediate = ri_svd(6, full_tensor, false, 10e-10, true, turboinfo.NAux(), turboinfo.NBas(), turboinfo.NBas());
               bqint = decomposer.Decompose(intermediate);
               //bqint = reducerank(intermediate,10e-10,10e-8,30);

               if (doprtmat) 
               {
                  Mout << "bqint" << std::endl;
                  Mout << bqint << std::endl;
               }
            }
            ncount += 1;
         }
      }
      else
      {
         Mout << "CP decomposition requested, but no information how to do it" << std::endl;
         MIDASERROR(" Check your input please ");
      }
   }
   else
   {
      std::string file = "MIDAS_3ATX";
      if (useOvlp) file = "MIDAS_SQIJ"; 
      bqint = turboinfo.Read3RI(file);

      if (useRobust)    
      {
         sqint = turboinfo.Read3RI("MIDAS_SQIJ");
      }  
   }

   NiceTensor<Nb> k_mo;
   if (useRobust) 
   {
      // make integrals (Q,ai) 
      NiceTensor<Nb> qai_c = bqint.ContractForward(1,cmvir[X::q,X::p])
                                  .ContractForward(2,cmocc[X::q,X::p]);
      NiceTensor<Nb> qai_s = sqint.ContractForward(1,cmvir[X::q,X::p])
                                  .ContractForward(2,cmocc[X::q,X::p]);
      NiceTensor<Nb> pjb_s = qai_s[X::q,X::p,X::a];
      NiceTensor<Nb> pbj_s = pjb_s[X::q,X::p,X::r];
      NiceTensor<Nb> pjb_c = qai_c[X::q,X::p,X::a];
      NiceTensor<Nb> pbj_c = pjb_c[X::q,X::p,X::r];

      NiceTensor<Nb> spqmat = turboinfo.ReadVPQ("MIDAS_SPQ");

      Nb* spqmat_ptr = dynamic_cast<SimpleTensor<Nb>*>(spqmat.GetTensor())->GetData();

      // Invert S_PQ matrix by eigen decomposition
      //std::unique_ptr<Nb> sinv(new Nb[naux*naux]);
      std::string algo = "Eigen";
      InvertSymMatrix(spqmat_ptr, spqmat_ptr, algo, naux);
      

      // (ai P) [S]_{PQ}^{-1} (Q| bj)
      NiceTensor<Nb> cpai_c = contract(spqmat[X::b,X::a], qai_c[X::b,X::c,X::d]);
      NiceTensor<Nb> term1 = contract(pbj_s[X::a,X::b,X::p], cpai_c[X::p,X::c,X::d]); 
     
      // (ai|P) [S]_{PQ}^{-1} (Qbj) 
      NiceTensor<Nb> cpai_s = contract(spqmat[X::b,X::a], qai_s[X::b,X::c,X::d]);
      NiceTensor<Nb> term2 = contract(pbj_c[X::a,X::b,X::p], cpai_s[X::p,X::c,X::d]);

      // (ai P) [S]_{PQ}^{-1} [J]_QR [S]_{RS}^{-1} (S bj) 
      NiceTensor<Nb> cpai_sc = contract(vpqinv[X::b,X::a], qai_s[X::b,X::c,X::d]);
      NiceTensor<Nb> term3 = contract(pbj_s[X::a,X::b,X::p], cpai_sc[X::p,X::c,X::d]); 

      // make (bj|ai)
      k_mo = term1 + term2 - term3;

   }
   else
   {

      // make integrals (Q,ai) 
      NiceTensor<Nb> qai = bqint.ContractForward(1,cmvir[X::q,X::p])
                                .ContractForward(2,cmocc[X::q,X::p]);

      // make C_(P,ai) = sum_{Q} (P|Q)^-1 (Q,ai)
      NiceTensor<Nb> cpai = contract(vpqinv[X::b,X::a], qai[X::b,X::c,X::d]);

      if (doprtmat) 
      {
         Mout << "qai" << std::endl;   Mout << qai << std::endl;
         Mout << "cpai" << std::endl;  Mout << cpai << std::endl;
      }

      // make (bj,P)
      NiceTensor<Nb> pjb = qai[X::q,X::p,X::a];
      NiceTensor<Nb> pbj = pjb[X::q,X::p,X::r];  

      if (locdbg && do_cp) 
      {
         CanonicalTensor<Nb>* canon = dynamic_cast<CanonicalTensor<Nb>*>(pbj.GetTensor());
         unsigned int rank = canon->GetRank();
         Mout << " Rank of (Q,bj): " << rank << std::endl;
      }

      // make (bj|ai) 
      k_mo = contract(pbj[X::a,X::b,X::p], cpai[X::p,X::c,X::d]);

      // reduce rank again
      if (do_cp) 
      {
         for(auto& decompinfo : decompinfoset)
         {
             midas::tensor::TensorDecomposer decomposer(decompinfo);
             k_mo = decomposer.Decompose(k_mo);  
         }
      }
   }

   if (locdbg && do_cp) 
   {
      CanonicalTensor<Nb>* canon = dynamic_cast<CanonicalTensor<Nb>*>(k_mo.GetTensor());
      unsigned int rank = canon->GetRank();
      Mout << " Rank of (ai|bj): " << rank << std::endl;
   }

   // create energy denominator e_{ab}^{ij} = (e_a + e_b - e_i - e_j)^{-1}
   NiceTensor<Nb> eabij;

   if (do_cp)
   {
      // use Laplace decomposition to find a good CP like decomposition with low rank
      eabij = new EnergyDenominatorTensor<Nb>(2,
                                    orben.at("v"),
                                    orben.at("oct"),
                                    4);
   }
   else
   {
      // use exact denominator
      eabij = new ExactEnergyDenominatorTensor<Nb>(2
                  ,orben.at("v")
                  ,orben.at("oct"));
   }


   if (locdbg && do_cp) 
   {
      CanonicalTensor<Nb>* canon = dynamic_cast<CanonicalTensor<Nb>*>(eabij.GetTensor());
      unsigned int rank = canon->GetRank();
      Mout << " Rank of e_ab^ij: " << rank << std::endl;
   }

   // make t_{ab}^{ij} = (ai|bj) * e_{ab}^{ij}
   NiceTensor<Nb> t2amp = k_mo * eabij;

   if (do_cp) 
   {
      // if rank is above a certain limit refit tensor
      CanonicalTensor<Nb>* canon = dynamic_cast<CanonicalTensor<Nb>*>(t2amp.GetTensor());
      unsigned int rank = canon->GetRank();
      if (rank > 1000) // replace this with a relative number, although 1000 is bad anyway...
      {
         Mout << " RIMP2-INFO: I will refit tensor t_{ab}^{ij} since it's rank is " << rank << std::endl;
         for(auto& decompinfo : decompinfoset)
         {
             midas::tensor::TensorDecomposer decomposer(decompinfo);
             t2amp = decomposer.Decompose(t2amp);
         }
      }
   }

   // make u = 2(ai|bj) - (aj|bi)
   NiceTensor<Nb> tcme = 2.0*t2amp - t2amp[X::p,X::s,X::r,X::q];   

   if (locdbg && do_cp) 
   {
      CanonicalTensor<Nb>* canon = dynamic_cast<CanonicalTensor<Nb>*>(tcme.GetTensor());
      unsigned int rank = canon->GetRank();
      Mout << " Rank of tcme: " << rank << std::endl;
   }

   // Calculate energy E = (2-delta_ij) sum_{i<=j} sum_{ab} u_{ab}^{ij} * k_{ab}^{ij}
   NiceTensor<Nb> rimp2_energy = (contract(tcme[X::p,X::q,X::r,X::s], k_mo[X::p,X::q,X::r,X::s]));
 
   emp2 = - rimp2_energy.GetScalar();

   Out72Char(Mout,'|','*','|');
   Mout << "      HF Energy:     " << hf_energy << std::endl;
   Mout << "      RI-MP2 Energy: " << emp2 << std::endl;
   Mout << std::endl; 
   Mout << "      Total Energy:  " << hf_energy + emp2 << std::endl;
   Out72Char(Mout,'|','*','|');
   Mout << std::endl;

}


void Ecor::BatchRIMP2()
{

   // constants
   const bool locdbg = false;
   const bool use_mobasis = true;    // Build exchange integrals in AO or MO basis?

   // locals
   unsigned int nbasi,nbasj,nbask,nbasl;
   unsigned int iboff,jboff,kboff,lboff;
   unsigned int nrank = 0;
   const midas::tensor::TensorDecompInfo::Set decompinfoset = mpEcorCalcDef->GetDecompInfoSet();

   std::map<std::string, std::vector<Nb>> orben;

   /************************************************************
   // Init Turbomole info
   ************************************************************/
   TurboInfo turboinfo;
   turboinfo.ReadInfo("MIDAS_3ATX.aux");

   // set some shortcuts
   In nocc = turboinfo.NOcc();
   In noct = turboinfo.NOct();
   In nvir = turboinfo.NVirt();
   In nfroz = nocc - noct;
   In natoms = turboinfo.NAtoms();
   In nbas = turboinfo.NBas();

   /************************************************************
   // Get CMO coefficients
   ************************************************************/
   const NiceTensor<Nb> cmocc = turboinfo.ReadCmoct("MIDAS_CMO");  // active occupied orbitals
   const NiceTensor<Nb> cmvir = turboinfo.ReadCmvir("MIDAS_CMO");  // virtual orbtials (yet no truncation in vir. space)

   /************************************************************
   // Get orbital enegies
   ************************************************************/
   ifstream orben_input("MIDAS_ORBEN", ios::in | ios::binary);
   orben_input.exceptions( std::ios::badbit | std::ios::failbit );

   Nb tmp_energy_occ [nocc];
   Nb tmp_energy_oct [noct];
   Nb tmp_energy_vir [nvir];
   
   orben_input.read(reinterpret_cast<char*>(tmp_energy_occ), nocc *sizeof(Nb) );
   orben_input.read(reinterpret_cast<char*>(tmp_energy_vir), nvir *sizeof(Nb) );

   orben["o"].assign(tmp_energy_occ, tmp_energy_occ + nocc);
   orben["v"].assign(tmp_energy_vir, tmp_energy_vir + nvir);

   // active occupied and virutals
   for (int iocc = 0; iocc<noct; ++iocc)
   {
      tmp_energy_oct[iocc] = tmp_energy_occ[iocc+nfroz];
   }
   orben["oct"].assign(tmp_energy_oct, tmp_energy_oct + noct);


   /************************************************************
   // sanity checks 
   ************************************************************/
   if (decompinfoset.empty())
   {
      MIDASERROR("Empty Decomposition Info Set!");
   }

   /************************************************************
   // RI-MP2 alogrithm
   ************************************************************/

   // get exchange integrals
   Mout << "make exchange integrals" << std::endl;
   NiceTensor<Nb> full_k = mkexchange(use_mobasis, true, 10e-14, 10000, turboinfo, decompinfoset); 
   //NiceTensor<Nb> full_k = mkexchange(use_mobasis, false, 10e-14, 10000, turboinfo, decompinfoset); 

   // transform to MO Basis
   NiceTensor<Nb> k_mo;
   if (use_mobasis)   
   {
      // we have already integrals in the MO basis
      k_mo = full_k;
   }
   else
   {
      Mout << "transform to MO basis" << std::endl;
      // we need to transform to the MO basis
      k_mo = full_k.ContractForward(0, cmvir[X::q,X::p])
                   .ContractForward(1, cmocc[X::q,X::p])
                   .ContractForward(2, cmvir[X::q,X::p])
                   .ContractForward(3, cmocc[X::q,X::p]);

      // reduce rank after transformation since it is very likely that now a smaller rank is sufficient   
      for(auto& decompinfo : decompinfoset)
      {
         midas::tensor::TensorDecomposer decomposer(decompinfo);
         k_mo = decomposer.Decompose(k_mo);

         CanonicalTensor<Nb>* k_canon=dynamic_cast<CanonicalTensor<Nb>*>(k_mo.GetTensor());
         unsigned int rank = k_canon->GetRank();

         Mout << " rank after reduction: " << rank << std::endl;
      }
   }


   // use Laplace decomposition to find a good CP like decomposition with low rank
   Mout << "make eabij" << std::endl;
   NiceTensor<Nb> eabij = new EnergyDenominatorTensor<Nb>(2,orben.at("v"),orben.at("oct"),4);
   
   // make t_{ab}^{ij} = (ai|bj) * e_{ab}^{ij}
   Mout << "make ampltiude" << std::endl;
   NiceTensor<Nb> t2amp = k_mo * eabij;

   // make u = 2t_{ab}^{ij} -  t_{ab}^{ji} 
   Mout << "make u" << std::endl;
   NiceTensor<Nb> tcme = 2.0*t2amp - t2amp[X::p,X::s,X::r,X::q];   

   if (locdbg) 
   {
      CanonicalTensor<Nb>* canon = dynamic_cast<CanonicalTensor<Nb>*>(tcme.GetTensor());
      unsigned int rank = canon->GetRank();
      Mout << " Rank of tcme: " << rank << std::endl;
   }

   // Calculate energy E = (2-delta_ij) sum_{i<=j} sum_{ab} u_{ab}^{ij} * k_{ab}^{ij}
   Mout << "calc energy" << std::endl;
   NiceTensor<Nb> rimp2_energy = (contract(tcme[X::p,X::q,X::r,X::s], k_mo[X::p,X::q,X::r,X::s]));
 
   Nb emp2 = - rimp2_energy.GetScalar();

   Out72Char(Mout,'|','*','|');
   Mout << "      RI-MP2 Energy: " << emp2 << std::endl;
   Out72Char(Mout,'|','*','|');
   Mout << std::endl;

}


