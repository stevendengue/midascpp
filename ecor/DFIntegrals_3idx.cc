/**
************************************************************************
* 
* @file                
*
* Created:            22-06-2016         
*
* Author:             Gunnar Schmitz (gunnar.schmitz@chem.au.dk)  
*
* Short Description:  Class for using atom dependend 3 Index RI/DF
*                     integrals
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <string>
#include <stdio.h>

#include "util/InterfaceOpenMP.h"

#include "ecor/DFIntegrals.h"
#include "tensor/EnergyDenominatorTensor.h"
#include "ecor/ri_svd.h" 
#include "ecor/reducerank.h"
#include "lapack_interface/packed_matrix.h" 

namespace X=contraction_indices;

/**
* Constructor (using an incremental build for the integrals)
* */
RI3Integral::RI3Integral
   (  const bool doIncr
   ,  const bool useovlp
   ,  const Nb& thr
   ,  const Nb& thrsvd
   ,  const Nb& threrr
   ,  TurboInfo& turboinfo
   ,  TensorDecompInfo::Set decompinfoset
   ,  MolStruct* pmolstruct
   ) 
{

   // constants
   const bool locdbg     = false; 
   const bool docheckerr = false;
   const Nb maxerr = 0.0;

   const unsigned int nmaxrank = 30000;

   const bool fullreduction        = false;
   const bool sliced_recompression = false;

   // OpenMP info
   int nthreads = omp_get_max_threads();
   Mout << "I will use " << nthreads << " threads to parallize the generation of the CP decomposed 3 index integrals" << std::endl;

   // code
   mDoCP = doIncr;
   mVPQINV = false;
   mNAux = turboinfo.NAux(); 
   mNdim = 3;
   //mIsOvlpMetric = useovlp;
   mIsOvlpMetric = false;

   mDecompInfoSet = decompinfoset;
   // Get first (and only) element of DecompInfoSet
   mDecompInfo = *(mDecompInfoSet.begin());
   // get decomposer
   midas::tensor::TensorDecomposer decomposer(mDecompInfo);

   // some general dimensions
   In natoms = turboinfo.NAtoms();
   In nbas = turboinfo.NBas();
   In naux = turboinfo.NAux();

   mNAtoms = natoms; // save it for later use

   // later needed to patch the mode matrices together
   std::vector<unsigned int> idim1 = {0,1,2};
   std::vector<unsigned int> idim2 = {0,2,1};

   // some values to reset decompinfo
   Nb thrcp = mDecompInfo["FINDBESTCPRESTHRESHOLD"].get<Nb>();

   mDims = {static_cast<unsigned int>(nbas),static_cast<unsigned int>(nbas)};


   std::vector<unsigned int> dims = {static_cast<unsigned int>(naux)
                                    ,static_cast<unsigned int>(nbas)
                                    ,static_cast<unsigned int>(nbas)} ;


   // print info on thresholds
   Mout << "Info on CP build of 3 index integrals:" << endl;
   char c='|';

   Out72Char(Mout,'+','=','+');
   TwoArgOut(Mout, "Threshold for RI-SVD:     ", 46, thrsvd, 10,c);
   TwoArgOut(Mout, "Threshold for C2T/T2C:    ", 46, threrr, 10,c);
   TwoArgOut(Mout, "Threshold for CP-decomp:  ", 46, thrcp,  10,c);
   Out72Char(Mout,'+','=','+');

   Mout << std::endl;


   // assign all atoms to this integral
   for (unsigned int iat = 0; iat < natoms; ++iat)
   {
      // get info on atom   
      std::string elem = pmolstruct->GetAtomLabel(iat);
      Vector3D origin = pmolstruct->GetNucleusCoordinates3D(iat); 
      Nb charge = pmolstruct->GetCharge(iat);

      // save info locally in integral class
      mIatoms.push_back(iat);
      mElems.push_back(elem); 
      mXyz.insert( std::pair<unsigned int, Vector3D>(iat,origin) );
      mCharges.insert( std::pair<unsigned int, Nb>(iat,charge));   
   }

   Nb** mode_matrices = new Nb*[mNdim];
   for (unsigned int imode = 0; imode < mNdim; ++imode)
   {
      mode_matrices[imode] = new Nb[dims[imode]*nmaxrank];
   } 

   unsigned int nbasi,nbasj,iboff,jboff,iadr,iadrloc;

   // variables to manage mode matrix in core and on disk
   unsigned int nranktot = 0;                            // rank of our mode matrices
   unsigned int nrankmem = 0;                            // rank of mode matrices we are holding in memory
   std::vector<unsigned int> fileblocks;                 // counter for the blocks we dumped to file


   for (unsigned int iat = 0; iat < natoms; ++iat)
   {
      nbasi = turboinfo.NBasAt(iat);
      iboff = turboinfo.NBasOff(iat);

// Currently not OpenMP parallized for gcc 9 and above due to the pointer 
// to canoncial tensor declared inside the loop
#if defined(__GNUC__) && (__GNUC__ < 9)
      #pragma omp parallel for default(none) \
       private(nbasi, iboff, nbasj, jboff, iadr, iadrloc) \
       shared(Mout, turboinfo, naux, fileblocks, nrankmem, dims, \
              thrcp, mode_matrices, iat, idim1, idim2, nranktot,thrsvd,threrr)
#endif
      for (unsigned int jat = 0; jat<=iat; ++jat)
      {

         int iiat = iat;
         int jjat = jat;

         nbasi = turboinfo.NBasAt(iat);
         iboff = turboinfo.NBasOff(iat);

         nbasj = turboinfo.NBasAt(jat);
         jboff = turboinfo.NBasOff(jat);

         if (nbasi < nbasj) 
         {
            std::swap(nbasi,nbasj);
            std::swap(iboff,jboff);
            iiat = jat;
            jjat = iat;
         }

         // calculate distance of i and j
         Vector3D origini = mXyz.at(iat);
         Vector3D originj = mXyz.at(jat);
   
         Nb distij = origini.Dist(originj); 
         Nb chargei = mCharges.at(iat);
         Nb chargej = mCharges.at(jat); 

         // read in (Q, mu nu) for atom pair iat,jat
         if (locdbg) Mout << "Read integrals for atoms " << iat << " and " << jat << std::endl;
         std::string file = "MIDAS_3ATX";
         if (mIsOvlpMetric) file = "MIDAS_SQIJ";
         NiceTensor<Nb> bqij = turboinfo.Read3RI(file,iiat,jjat);

         // use two step procedure where we create an inital high rank tensor with large accuracy
         // followed by a rank reduction step
         bqij =  ri_svd(0, bqij, true, thrsvd, true, naux, nbasi, nbasj); 
         
         // get handle on tensor
         CanonicalTensor<Nb>* bqij_canon = dynamic_cast<CanonicalTensor<Nb>*>(bqij.GetTensor());
         unsigned int rankij = bqij_canon->GetRank();
         unsigned int rankold = rankij; 
 
         // if necessary reduce rank
         int nrankguess = rankij / 10;
         if (rankij > 10)
         { 
            // predict a final rank to save a few iterations
            if (iat == jat) 
            {
               nrankguess = 0.047 * (chargei) * rankij + 31.447;  // strange factor were optimized on a small test set
                                                                  // improvements are possible by refitting... 
            }
            else
            {
               nrankguess = 0.152 * (chargei + chargej) / distij * rankij + 43.275;
            }
            nrankguess /= 5;  // damp the estimate
            nrankguess = std::max<In>(nrankguess,1);

            //Nb thrcpij = std::min(10 * thrcp, thrcp * exp(0.15 * distij));
            //Nb thrcpij = std::min(10 * thrcp, thrcp * exp(0.10 * distij));
            //Nb thrcpij = std::min(10 * thrcp, thrcp * exp(0.10 * distij*distij));
            //Nb thrcpij = std::min(10 * thrcp, thrcp * exp(0.05 * distij*distij));
            //Nb thrcpij = std::min(10 * thrcp, 0.1 * thrcp * exp(0.10 * distij*distij));
            //Nb thrcpij = std::min(10 * thrcp, thrcp * exp(0.025 * distij*distij));
            //Nb thrcpij = std::min(10 * thrcp, thrcp * exp(0.01 * distij*distij));
            //Nb thrcpij = std::min(10 * thrcp, thrcp +  thrcp * distij);
            //Nb thrcpij = std::min(10 * thrcp, thrcp +  0.5* thrcp * distij);
            //Nb thrcpij = std::min(10 * thrcp, thrcp +  0.25* thrcp * distij);
            Nb thrcpij = std::min(10 * thrcp, thrcp +  0.10* thrcp * distij);
            
            //Nb thrcpij = thrcp;

            mDecompInfo["STARTINGRANK"] = std::max<In>(nrankguess,1);
            mDecompInfo["FINDBESTCPRESTHRESHOLD"] = thrcpij;           
            Mout << "thrcpij " << thrcpij << std::endl;
 
            midas::tensor::TensorDecomposer decomposer(mDecompInfo);

            //Nb threrr = std::min(1.e-5, 1.e-7 * exp(0.5 * distij) ); // = 1.e-6
            //Nb threrr = std::min(1.e-3, 1.e-5 * exp(0.5 * distij) ); // loose
            
            bqij = reducerank(bqij, thrsvd, 1.e-7, 20, true, threrr, &decomposer);
         }


         // GS: This ciritical is much too large.... have to think about something more clever
#if defined(__GNUC__) && (__GNUC__ < 9)
         #pragma omp critical 
#endif
         {
            bqij_canon = dynamic_cast<CanonicalTensor<Nb>*>(bqij.GetTensor());
            rankij = bqij_canon->GetRank();
            Nb** matrices = bqij_canon->GetModeMatrices();

            unsigned int nrankadd = rankij;
            if (iat != jat) nrankadd *= 2;

            Mout << "nrankmem " << nrankmem << " nrankadd " << nrankadd << std::endl;

            // check if we exceed our buffer and have to dump the mode matrices before
            if (nrankmem + nrankadd > nmaxrank)
            {
               SaveModeMatrices(0,nrankmem,mode_matrices,dims);
               fileblocks.push_back(nrankmem);
               nrankmem = 0;
            } 

            /*
               Exploit permuational symmetry of atomic indices (Q|mu nu) = (Q|nu mu)    
            */
            unsigned int idimloc;
            std::vector<std::vector<unsigned int> > ivecdim = {idim1, idim2};
 
            unsigned int nperm = 2;
            if (iat == jat) nperm = 1;
       
            unsigned int nrank = 0; 
            for (unsigned int iperm = 0; iperm < nperm; ++iperm)
            {
               std::vector<unsigned int> iioff;
               std::vector<unsigned int> dimsloc;
               if (iperm == 0) 
               {
                  iioff =   {static_cast<unsigned int>(0),    static_cast<unsigned int>(iboff), static_cast<unsigned int>(jboff)}; 
                  dimsloc = {static_cast<unsigned int>(naux), static_cast<unsigned int>(nbasi), static_cast<unsigned int>(nbasj)};
               }
               else
               {
                  iioff =   {static_cast<unsigned int>(0),    static_cast<unsigned int>(jboff), static_cast<unsigned int>(iboff)}; 
                  dimsloc = {static_cast<unsigned int>(naux), static_cast<unsigned int>(nbasj), static_cast<unsigned int>(nbasi)};
               }
   
               // append to full tensor
               for (unsigned int irank = 0; irank<rankij; ++irank)
               {
                  for (unsigned int idim = 0; idim<mNdim; ++idim) 
                  {
                     if (idim > 0) 
                     {
                        // first zero out all elements (not necessary for aux. functions idim = 0) 
                        for (unsigned int isbf = 0; isbf < dims[idim]; ++isbf)
                        {
                           iadr = (irank + nrankmem)*dims[idim] + isbf;
                           mode_matrices[idim][iadr] = 0.0;
                        }
                     }

                     // now enter non zero elements 
                     for (unsigned int ibas = 0; ibas < dimsloc[idim]; ++ibas)
                     {
                        iadr = (irank + nrankmem)*dims[idim] + ibas + iioff[idim];
                        iadrloc = irank*dimsloc[idim] + ibas;
   
                        idimloc = ivecdim[iperm][idim];
                        
                        mode_matrices[idim][iadr] = matrices[idimloc][iadrloc];
                     }
                     
                  }
               }
               nranktot += rankij;
               nrankmem += rankij;
               mRanks.push_back(rankij);
            }
            if (locdbg) 
            {
               Mout << "dist " << distij << " rank  " << rankij  << " size " << nbasi << " " << nbasj << " charge " << chargei << " " << chargej  << " rank_svd " << rankold <<  std::endl;
               Mout << "nrankguess " << nrankguess << " rank " << rankij << std::endl;
            }

            // add atom pair to pair list
            mPairList.push_back(std::make_tuple(iiat, jjat));
            if (iat != jat) mPairList.push_back(std::make_tuple(jjat, iiat)); 
         }
      }
   }

   Mout << " rank after incr. built: " << nranktot << std::endl;

   // trim of unused memory
   if (nranktot < nmaxrank) 
   {
      for(unsigned int imode = 0; imode < 3; ++imode)
      {
         mode_matrices[imode] = reinterpret_cast<Nb*>(realloc(mode_matrices[imode], nranktot*dims[imode]*sizeof(Nb)));
      }
   }
   else if (nranktot > nmaxrank)
   {
      // This is not optimal. We don't want to hold such large things in main memory
      // -> extend other parts to read data from file
      
      // put remaining buffer to file
      SaveModeMatrices(0,nrankmem,mode_matrices,dims);

      // reallocate new memory
      for(unsigned int imode = 0; imode < 3; ++imode)
      {
         mode_matrices[imode] = reinterpret_cast<Nb*>(realloc(mode_matrices[imode], nranktot*dims[imode]*sizeof(Nb)));
      }

      // read all mode matrices to memory and delete file holding the information
      ReadModeMatrices(0,nranktot,mode_matrices,dims);
      DeleteModeMatrices();
   }

   // patch everything together
   NiceTensor<Nb> fulltensor(new CanonicalTensor<Nb>(dims, nranktot, mode_matrices));

   // and reduce again a last time
   if (fullreduction)
   {
      // recompress again on the full tensor
      mIqij = reducerank(fulltensor, 1.e-15, 1.e-7, 20, docheckerr, maxerr, &decomposer); 
   }
   else if (sliced_recompression)
   {
      // recompress on blocks of the tensor and merge 
      const unsigned int incr = 10;
      const unsigned int nsizerij = mRanks.size();

      std::vector<unsigned int> istart;
      std::vector<unsigned int> blokln;

      unsigned int ist = 0;
      for (unsigned int ij = 0; ij < nsizerij; ij += incr)
      {
         unsigned int ntmp = 0;
         unsigned int nend = ij + incr;
         if (nend > nsizerij) nend = nsizerij;
         for (unsigned int ihlp = ij; ihlp < nend; ++ihlp)
         {
            ntmp += mRanks[ihlp];
         } 
         blokln.push_back(ntmp);
         istart.push_back(ist);   
         ist += ntmp;
      }
      mIqij = reducerank(fulltensor, 1.e-15, 1.e-7, 20, docheckerr, maxerr, istart, blokln, &decomposer); 
   }
   else
   {
      // no recompression we live with the result
      mIqij = fulltensor; 
   }

   if (locdbg)
   {
      CanonicalTensor<Nb>* canon = dynamic_cast<CanonicalTensor<Nb>*>(mIqij.GetTensor());

      // Get rank
      unsigned int nrank  = canon->GetRank();
      
      Mout << "nrank I(Q,mu,nu) " << nrank << std::endl;
      Mout << mIqij << std::endl;

      Mout << " ranks for each pair " << std::endl;
      Mout << mRanks << std::endl;
   }

}

/**
 * @brief save factor matrices on file
 **/
void RI3Integral::SaveModeMatrices
   (  const unsigned int irankstart
   ,  const unsigned int irankend
   ,  Nb** mode_matrices
   ,  std::vector<unsigned int> dims
   )
{

   // constants
   const bool locdbg = false;

   if (locdbg)
   {
      Mout << "irankstart " << irankstart << std::endl;
      Mout << "irankend   " << irankend << std::endl;
   }

   for (unsigned int imode = 0; imode < mNdim; ++imode)
   {
      std::string file = "modemat-" + std::to_string(imode) + ".bin";
      ofstream outBinFile;
      outBinFile.open(file, ios::out | ios::app | ios::binary);

      if(outBinFile.is_open())
      {
         unsigned int iadr;

         if (locdbg) std::cout << "File is open" << std::endl;

         for (unsigned int irank = irankstart; irank < irankend ; irank++)
         {
            if (locdbg) 
            {
               Mout << "irank " << irank << std::endl;
               Mout << "dims  " << dims[imode] << std::endl;
            }

            for ( unsigned int idim = 0; idim < dims[imode]; ++idim)
            {
               iadr = irank*dims[imode] + idim;

               outBinFile.write(reinterpret_cast<char*>(&mode_matrices[imode][iadr]), sizeof(Nb));
            }
         }

         outBinFile.close();    
      }
      else 
      {
         MIDASERROR("Error while writing mode matrices to file!");
      }
   }

}

/**
 * @brief deletes file with mode matrices 
 **/
void RI3Integral::DeleteModeMatrices()
{
   for (unsigned int imode = 0; imode < mNdim; ++imode)
   {
      std::string file = "modemat-" + std::to_string(imode) + ".bin";
      const char* cfile = file.c_str();
 
      if( remove( cfile ) != 0 )
      {
         MIDASERROR("Error while deleting file with mode matrices!");
      }
   }
}

/**
 * @brief save factor matrices on file
 **/
void RI3Integral::ReadModeMatrices
   (  const unsigned int irankstart
   ,  const unsigned int irankend
   ,  Nb** mode_matrices
   ,  std::vector<unsigned int> dims
   )
{

   // constants
   const bool locdbg = false;

   if (locdbg)
   {
      Mout << "irankstart " << irankstart << std::endl;
      Mout << "irankend   " << irankend << std::endl;
   }

   for (unsigned int imode = 0; imode < mNdim; ++imode)
   {
      std::string file = "modemat-" + std::to_string(imode) + ".bin";
      ifstream inBinFile;
      inBinFile.open(file, ios::in | ios::binary);

      if(inBinFile.is_open())
      {
         unsigned int iadr;

         for (unsigned int irank = irankstart; irank < irankend ; irank++)
         {
            for ( unsigned int idim = 0; idim < dims[imode]; ++idim)
            {
               iadr = irank*dims[imode] + idim;

               inBinFile.read( reinterpret_cast<char*>(&mode_matrices[imode][iadr]), sizeof(Nb) );
            }
         }
         inBinFile.close();    
      }
      else 
      {
         MIDASERROR("Error while writing mode matrices to file!");
      }
   } 

}


/**
* Constructor 
* */
RI3Integral::RI3Integral
   (  const In iat
   ,  const In jat
   ,  const bool doCP
   ,  const bool useRISVD
   ,  const Nb& thr 
   ,  TurboInfo& turboinfo
   ,  TensorDecompInfo::Set decompinfoset
   )
{

   // constants
   const bool locdbg = false;
   const bool checknrm = true;
   const bool docheckerr = false;
   const Nb maxerr = 0.0;

   mDoCP = doCP;
   mVPQINV = false;

   if (iat < 0 && jat < 0)
   {
      std::string file = "MIDAS_3ATX";
      if (mIsOvlpMetric) file = "MIDAS_SQIJ";
      mIqij = turboinfo.Read3RI(file);
   }
   else
   {
      std::string file = "MIDAS_3ATX";
      if (mIsOvlpMetric) file = "MIDAS_SQIJ";
      mIqij = turboinfo.Read3RI(file, iat, jat);
   }

   if (locdbg)
   {
      Mout << "I(Q,mu,nu)" << std::endl;
      Mout << mIqij << std::endl;
   }

   In nbasi = turboinfo.NBasAt(iat);
   In nbasj = turboinfo.NBasAt(jat);
   mNAux = turboinfo.NAux(); 

   mNdim = 3;
   mDims = {static_cast<unsigned int>(nbasi),static_cast<unsigned int>(nbasj)};

   mDecompInfoSet = decompinfoset;
   // Get first (and only) element of DecompInfoSet
   mDecompInfo = *(mDecompInfoSet.begin());

   if (doCP) 
   {
      if (useRISVD)
      {
         // use RI guided decomposition with followed rank reduction step
         mIqij = ri_svd(0,mIqij,true,thr,true,mNAux,nbasi,nbasj);

         NiceTensor<Nb> intermediate = mIqij;

         // reduce initial rank
         midas::tensor::TensorDecomposer decomposer(mDecompInfo);
         mIqij = reducerank(mIqij, 1.e-12, 1.e-7, 20, docheckerr, maxerr, &decomposer);

         if (checknrm) 
         {
            NiceTensor<Nb> diff = intermediate - mIqij;
  
            std::vector<unsigned int> dims = diff.GetDims();
            CanonicalTensor<Nb> diff_cp = *dynamic_cast<CanonicalTensor<Nb>*>(diff.GetTensor());

            SimpleTensor<Nb> intermed(diff_cp);
            Mout << "error-norm: " << intermed.Norm() <<  std::endl;
         }
      }
      else
      {
         // use Find-Best-CPALS algorithm
         midas::tensor::TensorDecomposer decomposer(mDecompInfo);
         mIqij = decomposer.Decompose(mIqij);

         mIqij = reducerank(mIqij, 1.e-15, 1.e-7, 20, docheckerr, maxerr, &decomposer);
      }
   }

   if (locdbg && doCP)
   {
      CanonicalTensor<Nb>* canon = dynamic_cast<CanonicalTensor<Nb>*>(mIqij.GetTensor());

      // Get rank
      unsigned int nrank  = canon->GetRank();
      
      Mout << "nrank I(Q,mu,nu) " << nrank << std::endl;
      Mout << mIqij << std::endl;
   }

}

/**
* @brief Returns the distance of two atoms present in this integral 
* */
Nb RI3Integral::GetDistance
   (  unsigned int iat
   ,  unsigned int jat
   ) const
{
   // Get vectors
   Vector3D origini = mXyz.at(iat);
   Vector3D originj = mXyz.at(jat);

   return origini.Dist(originj); 
}

/**
* @brief Reduces the Rank of the integral tensor 
* */
void RI3Integral::ReduceRank()
{
   midas::tensor::TensorDecomposer decomposer(mDecompInfo);
   mIqij = decomposer.Decompose(mIqij);
}

/**
* @brief Reduces the Rank of the integral tensor using the C2T/T2C algorithm
* */
void RI3Integral::ReduceRank
   (  const Nb thrsvd
   ,  const unsigned int maxiter
   )
{
   const bool docheckerr = false;
   const Nb maxerr = 0.0;

   midas::tensor::TensorDecomposer decomposer(mDecompInfo);
   mIqij = reducerank(mIqij, thrsvd, 1.e-5, maxiter, docheckerr, maxerr, &decomposer);
}

/**
* @brief Contracts integral tensor with (P|Q)^{-1} matrix 
* */
void RI3Integral::ContractWithVPQInv(NiceTensor<Nb>& vpqinv)
{
   mVPQINV = true;

   mIqij = contract(vpqinv[X::b,X::a], mIqij[X::b,X::c,X::d]);
}

/**
* @brief Contracts integral tensor with other integral tensor yielding an 4 index integral
* */
NiceTensor<Nb> RI3Integral::ContractWith3idx(RI3Integral& bqkl)
{
   if (mVPQINV == bqkl.HasVPQInv() )
   {
      MIDASERROR("Missing or double used overlap metrik!");
      
   }

   // assemble
   NiceTensor<Nb> k_ao = contract(bqkl.Get3Idx()[X::p,X::a,X::b], mIqij[X::p,X::c,X::d]);
   
   for(auto& decompinfo : mDecompInfoSet)
   {
      midas::tensor::TensorDecomposer decomposer(decompinfo);
      k_ao = decomposer.Decompose(k_ao);
   }
   
   return k_ao;
}

/**
* @brief Performs transformation to a different orbital space 
* */
void RI3Integral::Transform( const unsigned int idx, NiceTensor<Nb> cmo) 
{
   // sanity checks
   bool dotraf = true;

   dotraf = idx < mNdim;

   if (dotraf)
   {
      // do the transformation
      mIqij = mIqij.ContractForward(idx+1, cmo);

      // update dimensions
      mDims[idx] = cmo.GetDims()[0];
   }
   else
   {
      MIDASERROR("Error in RI3Integral::TransformToMO");
   }
}

/**
* @brief Returns dimensions of the 3 index integral tensor 
* */
const std::vector<unsigned int>& RI3Integral::GetDims() const
{  
   return mDims;
}  

/**
* @brief Returns atom pair ranks of the 3 index integral tensor 
* */
const std::vector<unsigned int>& RI3Integral::GetRanks() const
{  
   return mRanks;
}  

/**
* @brief Returns pair list of included atoms 
* */
const std::vector<std::tuple<unsigned int, unsigned int>>& RI3Integral::GetPairList() const
{  
   return mPairList;
}  


