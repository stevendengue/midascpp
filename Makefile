# ==============================================================================
#  NON-DEVELOPER'S SECTION
# ==============================================================================
# We need the SECONDEXPANSION feature of 'make' ($$ --> $, see manual) for some
# of the templated functions.
.SECONDEXPANSION :

# ------------------------------------------------------------------------------
#  CONFIGURATIONS
# ------------------------------------------------------------------------------
# All configurations pertaining to regular usage are set in Makefile.config,
# which is then included here.

include Makefile.config
include Makefile.extlibs

# ==============================================================================
#  DEVELOPER'S SECTION
# ==============================================================================
# ------------------------------------------------------------------------------
#  GIT VERSION FILE
# ------------------------------------------------------------------------------
# Every time make is executed, use git to get information on the last commit.
# Print this information to a temporary file, and if it differs with the
# current one, update the latter. inc_gen/GitVersion.h is included by
# mains/midascpp.cc, meaning that the program is rebuilt if the commit
# information has changed. The commit information is displayed when running the
# program.
# Furthermore, check whether the repository is clean (no modifications or
# untracked files, except for those listed in .gitignore) or dirty
# (modifications or untracked files).
#
# If 'git' is not available provide some other values for the GIT_* macros,
# since otherwise compilation will fail.
# 
# From the 'git help log' man page:
#  -n 1
#     Limit the number of commits output (to one).
#  --format=format:<string>
#     The format: format allows you to specify which information you want to
#     show. It works a little bit like printf format, with the notable
#     exception that you get a newline with %n instead of \n.
#     The placeholders are (among others):
#     %H:  commit hash
#     %n:  newline
#     %ai: author date, ISO 8601 format
#
# There are various ways of retrieving the branch. The 'git symbolic-ref' way
# should work as of git version 1.8.1.

# Provide git command if not already defined.
GIT ?= git

file_GIT_VERSION:=GitVersion.h#EOL
file_GIT_VERSION_TMP:=.$(file_GIT_VERSION)_tmp#EOL

# Clear the tmp file.
$(shell $(PRINTF) "" > $$(pwd)/inc_gen/$(file_GIT_VERSION_TMP))

# Run 'git --version' and check exit status to find out whether 'git' works.
$(shell \
	if $(GIT) --version > /dev/null 2>&1; \
	then \
		if $(GIT) rev-parse > /dev/null 2>&1; \
		then \
			echo "#define GIT_INFO 0 // '$(GIT)' works, and in git repo." \
			>> $$(pwd)/inc_gen/$(file_GIT_VERSION_TMP); \
			echo "#define GIT_INFO_MSG \"Built from a git repository.\"" \
			>> $$(pwd)/inc_gen/$(file_GIT_VERSION_TMP); \
		else \
			echo "#define GIT_INFO 1 // '$(GIT)' works, but not in git repo." \
			>> $$(pwd)/inc_gen/$(file_GIT_VERSION_TMP); \
			echo "#define GIT_INFO_MSG \"N/A (Not built from a git repository.)\"" \
			>> $$(pwd)/inc_gen/$(file_GIT_VERSION_TMP); \
		fi; \
	else \
		echo "#define GIT_INFO 127 // '$(GIT)' not working." \
		>> $$(pwd)/inc_gen/$(file_GIT_VERSION_TMP); \
		echo "#define GIT_INFO_MSG \"N/A ('$(GIT)' command not working.)\"" \
		>> $$(pwd)/inc_gen/$(file_GIT_VERSION_TMP); \
	fi \
)

# Write a line like
#    #define GIT_COMMIT "<commit_hash>"
# to inc_gen/$(file_GIT_VERSION_TMP).
$(shell \
	if \
		! $(GIT) log -n 1 --format=format:"#define GIT_COMMIT \"%H\"%n" HEAD \
		>> $$(pwd)/inc_gen/$(file_GIT_VERSION_TMP) 2> /dev/null; \
	then \
		echo "#define GIT_COMMIT \"N/A ('$(GIT) log' failed)\"" \
		>> $$(pwd)/inc_gen/$(file_GIT_VERSION_TMP); \
	fi \
)

# Append a line like
#    #define GIT_BRANCH "<branch_name>"
# to inc_gen/$(file_GIT_VERSION_TMP).
$(shell \
	if \
		$(GIT) symbolic-ref --short HEAD > /dev/null 2>&1 ; \
	then \
		echo "#define GIT_BRANCH \"$$($(GIT) symbolic-ref --short HEAD)\"" \
		>> $$(pwd)/inc_gen/$(file_GIT_VERSION_TMP); \
	else \
		echo "#define GIT_BRANCH \"N/A ('$(GIT) symbolic-ref' failed)\"" \
		>> $$(pwd)/inc_gen/$(file_GIT_VERSION_TMP); \
	fi \
)

# Append a line like
#    #define GIT_DATE "YYYY-MM-DD hh:mm:ss +hhmm"
# to inc_gen/$(file_GIT_VERSION_TMP).
$(shell \
	if \
		! $(GIT) log -n 1 --format=format:"#define GIT_DATE \"%ai\"%n" HEAD \
		>> $$(pwd)/inc_gen/$(file_GIT_VERSION_TMP) 2> /dev/null; \
	then \
		echo "#define GIT_DATE \"N/A ('$(GIT) log' failed)\"" \
		>> $$(pwd)/inc_gen/$(file_GIT_VERSION_TMP); \
	fi \
)

# Append lines like
#    #define GIT_REPO_DIRTY <0,1>
#    #define GIT_STATUS_PORCELAIN "<XY> <file>\n ... \n<XY> <file>\n"
# to inc_gen/$(file_GIT_VERSION_TMP).
# GIT_REPO_DIRTY values:
#    0:   clean repository
#    1:   dirty repository (modified, added, removed, untracked files, etc.)
#    127: git status --porcelain failed
$(shell \
	outfile="$$(pwd)/inc_gen/$(file_GIT_VERSION_TMP)"; \
	if git_stat=$$($(GIT) status --porcelain 2> /dev/null); \
	then \
		if test -z "$${git_stat}"; \
		then \
			echo "#define GIT_REPO_DIRTY 0 // Repo is clean." >> $${outfile}; \
		else \
			echo "#define GIT_REPO_DIRTY 1 // Repo is dirty." >> $${outfile}; \
		fi; \
		$(PRINTF) "#define GIT_STATUS_PORCELAIN \"" >> $${outfile}; \
		echo "$${git_stat}" | while IFS= read -r line; \
		do \
			$(PRINTF) "%s\\\\n" "$${line}" >> $${outfile}; \
		done; \
		$(PRINTF) "\"\n" >> $${outfile}; \
	else \
		echo "#define GIT_REPO_DIRTY 127 // Clean/dirty repo is undetermined." >> $${outfile}; \
		echo "#define GIT_STATUS_PORCELAIN \"N/A ('$(GIT) status --porcelain' failed)\"" >> $${outfile}; \
	fi \
)

# Compare $(file_GIT_VERSION_TMP) with existing $(file_GIT_VERSION). If they
# differ or the latter doesn't exist, move $(file_GIT_VERSION_TMP) to
# $(file_GIT_VERSION).
$(shell \
	if \
		! [ -f $$(pwd)/inc_gen/$(file_GIT_VERSION) ] \
		|| ! cmp -s $$(pwd)/inc_gen/$(file_GIT_VERSION_TMP) $$(pwd)/inc_gen/$(file_GIT_VERSION); \
	then \
		mv $$(pwd)/inc_gen/$(file_GIT_VERSION_TMP) $$(pwd)/inc_gen/$(file_GIT_VERSION); \
	else \
		rm $$(pwd)/inc_gen/$(file_GIT_VERSION_TMP); \
	fi \
)

# ------------------------------------------------------------------------------
#  VARIABLES, FLAGS, DEFINITIONS, ETC.
# ------------------------------------------------------------------------------
# EXECUTABLES
# Files containing a 'main' function, that thus cannot be linked with each
# other. They MUST be located in the directories dir_MAINS or dir_TOOLS
# respectively, as specified below in DIRECTORIES section.
# MidasPot source files (also containing 'main') should be put in dir_POT;
# it is automatically assumed that these are standalone executables, so the
# must not be put here.
# For each executable specify its dependencies and link libraries.
# Also list the files to install, i.e. those that are copied to the
# installation directory (full names, i.e. including extension (if applicable)
# but excluding directory part).
# MBH-NB: main() functions seem to be located in either 'mains' or 'tools'.
# Except for some that are located somewhere deep down in the 'libmda'. 
# MBH-NB: In the first run, let's just get midascpp going.
# MBH-NB: I THINK lst_DATA contains what it should, according to old Makefile.
MAIN_exe     = midascpp
lst_MAINS   := $(MAIN_exe)
lst_TOOLS   := comparevibs\
					convert_old_pes_format_to_new\
					tensordatacont_diff_norm\
					laplace_quadrature\
					tensordatacont_to_datacont\
					datacont_diff_norm\
					datacont_to_ascii\
					der_binary_ascii\
					mctdh_wf_analysis\
					autocorr_spectrum\
					MassDataPrepare\
					midasdynlib
lst_INST_bin := $(lst_MAINS) midasutil midastools midasdynlib
lst_INST_exec = $(addsuffix .$(ext_EXE), $(lst_MAINS)) $(addsuffix .$(ext_EXE), $(lst_TOOLS))
lst_INST_sh  := midas\
					 dalton_for_midas.sh\
					 aces2_for_midas.sh\
					 cfour_for_midas.sh\
					 cfour-parallel_for_midas.sh\
					 setup_midas_ruby_interface.sh\
					 make_lcs_table.sh\
					 midasutil.x\
					 midastools.x
lst_DATA     := $(patsubst $(ORIGINDATADIR)/%, %, $(shell $(FIND) $(ORIGINDATADIR) -maxdepth 1 -type f))
#MBH-NB: lst_DATA MAYBE works...
lst_VIM      := $(patsubst $(ORIGINVIMDIR)/%, %, $(shell $(FIND) $(ORIGINVIMDIR) -type f))
lst_AUTOCOMP := $(patsubst $(ORIGINAUTOCOMPDIR)/%, %, $(shell $(FIND) $(ORIGINAUTOCOMPDIR) -type f))
lst_INCLUDE  := $(patsubst $(ORIGININCLUDEDIR)/%, %, $(shell $(FIND) $(ORIGININCLUDEDIR) -type f))
lst_TEMPLATE := $(patsubst $(ORIGINTEMPLATEDIR)/%, %, $(shell $(FIND) $(ORIGINTEMPLATEDIR) -type f))

# As default, all (lst_MAINS and lst_TOOLS) executables depend on its own
# object file, every other non-main() object file and all libraries.
$(foreach m, $(lst_MAINS),\
   $(eval lst_DEP_$(m)  = $$(dir_MAINS)/$(m).$$(ext_OBJ) $$(lst_OBJ))\
   $(eval lst_SHIPLIBS_$(m) = $$(lst_SHIPLIBS))\
   $(foreach b, $(lst_BUILDS),\
      $(eval lst_LIB_LIST_EXTLIBS_$(b)_$(m) = $(LIB_LIST_$(b)))\
   )\
)
$(foreach m, $(lst_TOOLS),\
   $(eval lst_DEP_$(m)  = $$(dir_TOOLS)/$(m).$$(ext_OBJ) $$(lst_OBJ))\
   $(eval lst_SHIPLIBS_$(m) = $$(lst_SHIPLIBS))\
   $(foreach b, $(lst_BUILDS),\
      $(eval lst_LIB_LIST_EXTLIBS_$(b)_$(m) = $(LIB_LIST_$(b)))\
   )\
)

# For files with other dependencies/libraries overwrite the above depend-on-all
# policy by specifying explicitly the dependencies/libraries here.
lst_DEP_datacont_diff_norm = $(dir_TOOLS)/datacont_diff_norm.$(ext_OBJ)
lst_DEP_datacont_to_ascii  = $(dir_TOOLS)/datacont_to_ascii.$(ext_OBJ)
lst_DEP_MassDataPrepare    = $(dir_TOOLS)/MassDataPrepare.$(ext_OBJ)
lst_DEP_midasdynlib        = $(dir_TOOLS)/midasdynlib.$(ext_OBJ) potentials/Dll.$(ext_OBJ)

lst_SHIPLIBS_datacont_diff_norm = 
lst_SHIPLIBS_datacont_to_ascii  = 
lst_SHIPLIBS_MassDataPrepare    = 

$(foreach b, $(lst_BUILDS),\
   $(eval lst_LIB_LIST_EXTLIBS_$(b)_datacont_diff_norm = ) \
   $(eval lst_LIB_LIST_EXTLIBS_$(b)_datacont_to_ascii  = ) \
   $(eval lst_LIB_LIST_EXTLIBS_$(b)_MassDataPrepare    = ) \
)

# DIRECTORIES
# There can be NO spaces after the directory name, hence the '#EOL.' is
# appended to visualize the end-of-line, i.e. that no space follows the dir.
# name. Also, for the purpose of properly excluding hidden files/directories
# from search results, don't write any directories in any "stupid" way
# containing the sequence "/.", since this is the signature for hidden files.
# E.g. don't write "dir_WORK := ./mains/..", which would be a proper path but
# would make dir_WORK look like a hidden directory.
# We always exclude the build directories (specified by lst_BUILDS), as well as
# the install directory, from file searches. Other directories to be excluded
# can be appended to the dir_EXCL variable. You would probably put libraries
# and such here.
# Also exclude the documentation and manual directories.
dir_WORK     = .#EOL.
dir_MAINS    = mains#EOL.
dir_TOOLS    = tools#EOL.
dir_TESTSUITE= test_suite#EOL.
dir_BIN      = bin#EOL.
dir_OBJ      = obj#EOL.
dir_DEP      = dep#EOL.
dir_DOCU     = docu#EOL.
dir_MAN      = manual#EOL.
# Excluded dirs. with abs. path should be put here.
dir_EXCL_abs := $(INSTALLPREFIX) $(dir_SCRATCH)
# Then remove the root dir part of the path ($(CURDIR) is GNU Make var.). If
# dir. is not subdir. of CURDIR nothing happens, which is okay.
dir_EXCL     := $(patsubst $(CURDIR)/%, %, $(dir_EXCL_abs))
# Dirs. with paths relative to CURDIR can just be appended below.
dir_EXCL     += $(lst_BUILDS)\
					 $(dir_TESTSUITE)\
					 config\
					 data\
					 $(dir_DOCU)\
					 example_suite\
					 $(dir_EXTLIBS)\
					 generic_pes_examples\
					 libmda\
					 makefile_config_examples\
					 $(dir_MAN)\
					 mathlib\
					 tools/pesfuncs\
					 $(wildcard debug_*)

# The file_EXCL variable should hold any files that you don't wan't to compile
# but for which you can't exclude the entire directory. Format is entire file
# name including extension (but excluding directory part). See
# TinkerSinglePoint.cc below for an example.
#
# Executables that are out-dated and do not work (as of 2016, January). They
# are put on the file_EXCL list, see below.
#    Manalysis
#    Mpes
#    Mtest
#    Mvcc
file_EXCL    := 
					 
# If 'Tinker' is not enabled, it is excluded from directory searches and
# compilation of the TinkerSinglePoint is disabled.  The variables are set in
# 'Makefile.config' determined by the 'configure' script.
ifneq ($(MAKE_TINKER), 1)
	dir_EXCL     += tinker_interface
	file_EXCL    += TinkerSinglePoint.cc
endif

# These variables are for the completion percentage bar.
file_COUNT = $(dir_MAKE)/file_var_FILE_COUNT.makefile
dir_COUNT = $(dir_MAKE)/dir_file_count

# This directory is used for the file search variables files.
dir_FILE_SEARCH_VARS = $(dir_MAKE)/dir_file_search_vars


# FILE EXTENSIONS
# Set file extensions for source files, so that they correspond to actual
# extensions of files in program. Set object, dependency and executable file
# extensions to what you prefer (but you should probably stick to 'o' for
# object files).
# MBH-NB: Not including the dot in the extension name means that all of these
# file types are forced to have a non-empty extension, so no executables with
# no extensions. Can be changed, though.
lst_SRC_types = CXX C F
ext_CXX       = cc cpp C
ext_C         = c
ext_F         = f f90 f03
ext_OBJ       = o
ext_DEP       = dep
ext_EXE       = x
ext_DYNLIB    = so.1.0

# MIDASPOT
# MidasPot source files can be used as model potentials in the test_suite. Put
# them in the midaspot subdirectory of the tools directory. The executables are
# assumed to only depend on their own source file, and the executables are
# installed in a subdirectory of the test_suite.
dir_POT     = $(dir_TOOLS)/midaspot
dir_POT_inst= $(dir_TESTSUITE)/midaspot

# Add all c++ files in the midaspot dir. to the list of source files.
# Without directory part, without extension.
# The corresponding executables just need an extension.
lst_POT     = $(basename $(notdir $(foreach s, $(ext_CXX), $(wildcard $(dir_POT)/*.$(s)))))
lst_POT_bin = $(addsuffix .$(ext_EXE), $(lst_POT))

# MIDASDYNLIB
# MidasDynlib source files can be used as model potentials in the test_suite. Put
# them in the midasdynlib subdirectory of the tools directory. The dynamic libraries are
# assumed to only depend on their own source file, and the dynamic libraries are
# installed in a subdirectory of the test_suite.
dir_DYNLIB     = $(dir_TOOLS)/midasdynlib
dir_DYNLIB_inst= $(dir_TESTSUITE)/midasdynlib

# Add all c++ files in the midaspot dir. to the list of source files.
# Without directory part, without extension.
# The corresponding executables just need an extension.
lst_DYNLIB     = $(basename $(notdir $(foreach s, $(ext_CXX), $(wildcard $(dir_DYNLIB)/*.$(s)))))
lst_DYNLIB_bin = $(addsuffix .$(ext_DYNLIB), $(lst_DYNLIB))

# Make them depend only on their own sources and no libraries.
$(foreach m, $(lst_POT),\
   $(eval lst_DEP_$(m)  = $(dir_POT)/$(m).$$(ext_OBJ))\
   $(eval lst_SHIPLIBS_$(m) = )\
   $(foreach b, $(lst_BUILDS),\
      $(eval lst_LIB_LIST_EXTLIBS_$(b)_$(m) = )\
   )\
)
$(foreach m, $(lst_DYNLIB),\
   $(eval lst_DEP_$(m)  = $(dir_DYNLIB)/$(m).$$(ext_OBJ))\
   $(eval lst_SHIPLIBS_$(m) = )\
   $(foreach b, $(lst_BUILDS),\
      $(eval lst_LIB_LIST_EXTLIBS_$(b)_$(m) = )\
   )\
)


# EXTERNAL LIBRARIES
# Mostly set up in Makefile.extlibs, but here we put some details necessary for
# prerequisites and execution of compiling and linking.

# Sets up a variable containing all the actual targets (files or directories),
# to be used as prerequisites of MidasCpp object/executable files.
# E.g., it could end up being something like:
# lst_EXTLIBS_prereq =
#    extlibs/gsl-1.16/lib/libgsl.dylib
#    extlibs/fftw-3.3.4/lib/libfftw3.3.dylib
lst_EXTLIBS_prereq  := $(foreach l, $(lst_EXTLIBS), $(file_LIB_TARGET_$(call get_lib_name,$(l))))

# LIBRARIES AND THEIR INCLUDE DIRECTORIES
# Libraries on which the executables depend should be put on the list here.
# They are then listed as prerequisites for the executables (MBH-NB: all of
# them at the moment). They are only listed as prerequisites if the program is
# set to build using the shipped versions, which is determined by the
# 'configure' script.
# They are currently built using their own separate makefile. See the 'SPECIAL
# TARGETS' section.
lst_SHIPLIBS     := 

# libmda is part of Midas and thus always required.
lst_SHIPLIBS     += libmda/lib/libmda.a

# ------------------------------------------------------------------------------
#  MAIN TARGETS
# ------------------------------------------------------------------------------
# BUILDING
# All targets are ALWAYS built for ALL build types in lst_BUILDS. Therefore,
# edit lst_BUILDS to control which builds are made.
# Or 'make all_<build>' (with <build> in lst_BUILDS) to build 'all' for the
# <build> build.

# ......................................
# Build all executables.
.PHONY : all
all: $(foreach b, $(lst_BUILDS), all_$(b))

# Template: targets 'all_$(1)'.
#    $(1): build type
define tmpl_TARGET_all
.PHONY : all_$(1)
all_$(1): \
	$(foreach m, $(lst_MAINS) $(lst_TOOLS) $(lst_POT), $(BUILDDIR)/$(1)/$(dir_BIN)/$(m).$(ext_EXE)) \
	$(foreach m, $(lst_DYNLIB), $(BUILDDIR)/$(1)/$(dir_BIN)/$(m).$(ext_DYNLIB))
endef

$(foreach b, $(lst_BUILDS), $(eval $(call tmpl_TARGET_all,$(b))))

# ......................................
# Build the main executable(s) only (as specified in MAIN_exe).
.PHONY : main
main: $(foreach b, $(lst_BUILDS), main_$(b))

# Template: targets 'main_$(1)'.
#    $(1): build type
define tmpl_TARGET_main
.PHONY : main_$(1)
main_$(1): $(foreach m, $(MAIN_exe), $(BUILDDIR)/$(1)/$(dir_BIN)/$(m).$(ext_EXE))
endef

$(foreach b, $(lst_BUILDS), $(eval $(call tmpl_TARGET_main,$(b))))

# ......................................
# Build all libraries, shipped and external.
# (The build_extlibs rule defined in Makefile.extlibs.)
.PHONY : libs
libs: build_shiplibs build_extlibs

.PHONY: build_shiplibs
build_shiplibs: $(lst_SHIPLIBS)

# ......................................
# Install all installation files (as given in lst_INST_exec and lst_INST_sh).

.PHONY : install
install: install_$(MAIN_build)

# The actual data and shell files are prerequisites so it will be properly
# checked that these aren't outdated, but the install_libexec_<build_type> are
# phony targets (that only check for existence of the files to be installed),
# so that rule will be executed every time, however we avoid checking all
# dependencies first. It's sort of an expert option for quickly switching
# between builds.
# The data and script files are (at the moment) build type independent, so we
# can use the actual files here.

# Template: targets 'install_$(1)'.
#    $(1): build type
define tmpl_TARGET_install
.PHONY : install_$(1)
install_$(1):  install_bin\
					install_libexec_$(1)\
					install_midaspot_$(1)\
					install_midasdynlib_$(1)\
					install_extlibs\
					$(addprefix $(INSTALLEXECDIR)/, $(lst_INST_sh))\
					$(addprefix $(INSTALLDATADIR)/, $(lst_DATA))\
					$(addprefix $(INSTALLVIMDIR)/, $(lst_VIM)) \
					$(addprefix $(INSTALLAUTOCOMPDIR)/, $(lst_AUTOCOMP)) \
					$(addprefix $(INSTALLINCLUDEDIR)/, $(lst_INCLUDE)) \
					$(addprefix $(INSTALLTEMPLATEDIR)/, $(lst_TEMPLATE))
endef

# Template: targets 'install_libexec_$(1)'.
#    $(1): build type
define tmpl_TARGET_install_libexec
.PHONY : install_libexec_$(1)
install_libexec_$(1): | $(INSTALLEXECDIR)
	@for i in $(lst_INST_exec); do\
		exe_orig=$(BUILDDIR)/$(1)/$(dir_BIN)/$$$$i;\
		exe_inst=$(INSTALLEXECDIR)/$$$$i;\
		if [ ! -f $$$${exe_orig} ]; then\
			$(PRINTF) "\n%s\n%s\n%s\n%s\n\n%s\n\n%s\n%s\n%s\n\n"\
				"ERROR:"\
				"   You tried to install for the build type \"$(1)\"."\
				"   The binary files must exist before installation can proceed properly."\
				"   The following file is missing:"\
				"      $$$${exe_orig}"\
				"   Run"\
				"      'make all_$(1)'      to compile it or"\
				"      'make complete_$(1)' to compile and install it automatically."\
				;\
			exit 1;\
		else\
			chmod +x $$$${exe_orig};\
			if $(install) $$$${exe_orig} $$$${exe_inst}; then\
				$(PRINTF) "%-10s%s\n%10s%s\n" "Installed" "$$$${exe_orig}" "--> " "$$$${exe_inst}";\
			else\
				$(PRINTF) "%s\n" "Installation failed for $$$${exe_orig}.";\
			fi;\
		fi;\
	done
endef

# Template: targets 'install_midaspot_$(1)'.
#    $(1): build type
# (NB! Print a small README file to the directory every time.)
define tmpl_TARGET_install_midaspot
.PHONY : install_midaspot_$(1)
install_midaspot_$(1): | $(dir_POT_inst)
	@$(PRINTF) "Please consult the README in $(dir_POT).\n" > $(dir_POT_inst)/README
	@for i in $(lst_POT_bin); do\
		exe_orig=$(BUILDDIR)/$(1)/$(dir_BIN)/$$$$i;\
		exe_inst=$(dir_POT_inst)/$$$$i;\
		if [ ! -f $$$${exe_orig} ]; then\
			$(PRINTF) "\n%s\n%s\n%s\n%s\n\n%s\n\n%s\n%s\n%s\n\n"\
				"ERROR:"\
				"   You tried to install MidasPot executables for the build type \"$(1)\"."\
				"   The binary files must exist before installation can proceed properly."\
				"   The following file is missing:"\
				"      $$$${exe_orig}"\
				"   Run"\
				"      'make all_$(1)'      to compile it or"\
				"      'make complete_$(1)' to compile and install it automatically."\
				;\
			exit 1;\
		else\
			chmod +x $$$${exe_orig};\
			if $(install) $$$${exe_orig} $$$${exe_inst}; then\
				$(PRINTF) "%-10s%s\n%10s%s\n" "Installed" "$$$${exe_orig}" "--> " "$$$${exe_inst}";\
			else\
				$(PRINTF) "%s\n" "Installation failed for $$$${exe_orig}.";\
			fi;\
		fi;\
	done
endef

# Template: targets 'install_midasdynlib_$(1)'.
#    $(1): build type
# (NB! Print a small README file to the directory every time.)
define tmpl_TARGET_install_midasdynlib
.PHONY : install_midasdynlib_$(1)
install_midasdynlib_$(1): | $(dir_DYNLIB_inst)
	@$(PRINTF) "Please consult the README in $(dir_DYNLIB).\n" > $(dir_DYNLIB_inst)/README
	@for i in $(lst_DYNLIB_bin); do\
		exe_orig=$(BUILDDIR)/$(1)/$(dir_BIN)/$$$$i;\
		exe_inst=$(dir_DYNLIB_inst)/$$$$i;\
		if [ ! -f $$$${exe_orig} ]; then\
			$(PRINTF) "\n%s\n%s\n%s\n%s\n\n%s\n\n%s\n%s\n%s\n\n"\
				"ERROR:"\
				"   You tried to install MidasPot executables for the build type \"$(1)\"."\
				"   The binary files must exist before installation can proceed properly."\
				"   The following file is missing:"\
				"      $$$${exe_orig}"\
				"   Run"\
				"      'make all_$(1)'      to compile it or"\
				"      'make complete_$(1)' to compile and install it automatically."\
				;\
			exit 1;\
		else\
			chmod +x $$$${exe_orig};\
			if $(install) $$$${exe_orig} $$$${exe_inst}; then\
				$(PRINTF) "%-10s%s\n%10s%s\n" "Installed" "$$$${exe_orig}" "--> " "$$$${exe_inst}";\
			else\
				$(PRINTF) "%s\n" "Installation failed for $$$${exe_orig}.";\
			fi;\
		fi;\
	done
endef

$(foreach b, $(lst_BUILDS),\
	$(eval $(call tmpl_TARGET_install,$(b)))\
	$(eval $(call tmpl_TARGET_install_libexec,$(b)))\
	$(eval $(call tmpl_TARGET_install_midaspot,$(b)))\
	$(eval $(call tmpl_TARGET_install_midasdynlib,$(b)))\
)

$(addprefix $(INSTALLEXECDIR)/, $(lst_INST_sh)): $(BUILDDIR)/$(MAIN_build)/$(dir_BIN)/$$(@F) | $(INSTALLEXECDIR)
	@chmod +x $<
	@$(install) $< $@
	@$(PRINTF) "%-10s%s\n%10s%s\n" "Installed" "$<" "--> " "$@"

$(addprefix $(INSTALLDATADIR)/, $(lst_DATA)): $(ORIGINDATADIR)/$$(@F) | $(INSTALLDATADIR)
	$(eval MODE := $(shell $(STAT_MODE) $<))
	$(eval MODE := $(shell echo "$$(( $(MODE) & 07777 ))" ))
	@$(install) -m $(MODE) $< $@
	@$(PRINTF) "%-10s%s\n%10s%s\n" "Installed" "$<" "--> " "$@"

$(addprefix $(INSTALLVIMDIR)/, $(lst_VIM)): $(ORIGINVIMDIR)/$(subst $(INSTALLVIMDIR),,$(@)) | $(INSTALLVIMDIR)
	$(eval MODE := $(shell $(STAT_MODE) $</$(subst $(INSTALLVIMDIR),,$(@))))
	$(eval MODE := $(shell echo "$$(( $(MODE) & 07777 ))" ))
	@$(install) -m $(MODE) $</$(subst $(INSTALLVIMDIR),,$(@)) $@
	@$(PRINTF) "%-10s%s\n%10s%s\n" "Installed" "$<" "--> " "$@"

$(addprefix $(INSTALLAUTOCOMPDIR)/, $(lst_AUTOCOMP)): $(ORIGINAUTOCOMPDIR)/$(subst $(INSTALLAUTOCOMPDIR),,$(@)) | $(INSTALLAUTOCOMPDIR)
	$(eval MODE := $(shell $(STAT_MODE) $</$(subst $(INSTALLAUTOCOMPDIR),,$(@))))
	$(eval MODE := $(shell echo "$$(( $(MODE) & 07777 ))" ))
	@$(install) -m $(MODE) $</$(subst $(INSTALLAUTOCOMPDIR),,$(@)) $@
	@$(PRINTF) "%-10s%s\n%10s%s\n" "Installed" "$<" "--> " "$@"

$(addprefix $(INSTALLINCLUDEDIR)/, $(lst_INCLUDE)): $(ORIGININCLUDEDIR)/$(subst $(INSTALLINCLUDEDIR),,$(@)) | $(INSTALLINCLUDEDIR)
	$(mkinstalldirs) $(INSTALLINCLUDEDIR)/$(subst $(INSTALLINCLUDEDIR)/,,$(@D))
	$(eval MODE := $(shell $(STAT_MODE) $</$(subst $(INSTALLINCLUDEDIR),,$(@))))
	$(eval MODE := $(shell echo "$$(( $(MODE) & 07777 ))" ))
	@$(install) -m $(MODE) $</$(subst $(INSTALLINCLUDEDIR),,$(@)) $@
	@$(PRINTF) "%-10s%s\n%10s%s\n" "Installed" "$<" "--> " "$@"

$(addprefix $(INSTALLTEMPLATEDIR)/, $(lst_TEMPLATE)): $(ORIGINTEMPLATEDIR)/$(subst $(INSTALLTEMPLATEDIR),,$(@)) | $(INSTALLTEMPLATEDIR)
	$(eval MODE := $(shell $(STAT_MODE) $</$(subst $(INSTALLTEMPLATEDIR),,$(@))))
	$(eval MODE := $(shell echo "$$(( $(MODE) & 07777 ))" ))
	@$(install) -m $(MODE) $</$(subst $(INSTALLTEMPLATEDIR),,$(@)) $@
	@$(PRINTF) "%-10s%s\n%10s%s\n" "Installed" "$<" "--> " "$@"

$(addprefix $(BUILDDIR)/$(MAIN_build)/$(dir_BIN)/, $(lst_INST_sh)): $(dir_WORK)/scripts/$$(@F) | $(BUILDDIR)/$(MAIN_build)/$(dir_BIN)
	@chmod +x $<
	@cp $< $@
	@$(PRINTF) "%-10s%s\n%10s%s\n" "Copied" "$<" "--> " "$@"

# Rules for stuff that goes in <install-prefix>/bin.
.PHONY: install_bin
install_bin: $(addprefix $(INSTALLBINDIR)/, midascpp.config $(lst_INST_bin))

$(INSTALLBINDIR)/midascpp.config: $(dir_WORK)/bin/midascpp.config | $(INSTALLBINDIR)
	cp $< $@

$(addprefix $(INSTALLBINDIR)/, $(lst_INST_bin)): $(dir_WORK)/bin/midascpp.template | $(INSTALLBINDIR)
	$(install) $< $@

.PHONY: install_vim
install_vim: 

# Rules for making install directories.
$(INSTALLBINDIR):
	$(mkinstalldirs) $@

$(INSTALLEXECDIR):
	$(mkinstalldirs) $@

$(INSTALLDATADIR):
	$(mkinstalldirs) $@

$(INSTALLLIBDIR):
	$(mkinstalldirs) $@

$(INSTALLVIMDIR): 
	$(mkinstalldirs) $@
	$(mkinstalldirs) $@/syntax
	$(mkinstalldirs) $@/ftdetect

$(INSTALLAUTOCOMPDIR): 
	$(mkinstalldirs) $@

$(INSTALLTEMPLATEDIR): 
	$(mkinstalldirs) $@

$(INSTALLINCLUDEDIR): 
	$(mkinstalldirs) $@

# ......................................
# The complete target builds all build types and installs the one chosen by the
# _<build_type> suffix (the $(MAIN_build) by default).
.PHONY : complete
complete: complete_$(MAIN_build)

# Template: targets 'complete_$(1)'.
#    $(1): build type
define tmpl_TARGET_complete
.PHONY : complete_$(1)
complete_$(1): all
	$$(MAKE) install_$(1)
endef

$(foreach b, $(lst_BUILDS), $(eval $(call tmpl_TARGET_complete,$(b))))

# --------------------------------------
# CLEANING
# As for building, cleaning also applies to ALL build types in lst_BUILDS.

# ......................................
# Clean object and dependency files, but leave executables as is.
.PHONY : clean
clean: $(foreach b, $(lst_BUILDS), clean_$(b)) cleanlibmda cleanlibs cleanmidaspot cleanmidasdynlib cleanfilesearch

# Template: targets 'clean_$(1)'.
#    $(1): build type
define tmpl_TARGET_clean
.PHONY : clean_$(1)
clean_$(1):
	$(RM) -r $(BUILDDIR)/$(1)/$(dir_OBJ)
	$(RM) -r $(BUILDDIR)/$(1)/$(dir_DEP)
endef

$(foreach b, $(lst_BUILDS), $(eval $(call tmpl_TARGET_clean,$(b))))

# ......................................
# Library cleaning/wiping, both shipped and external.
# (The [clean/wipe]_extlibs rules defined in Makefile.extlibs.)
.PHONY: cleanlibs wipelibs
cleanlibs: clean_shiplibs clean_extlibs
wipelibs: wipe_shiplibs wipe_extlibs

.PHONY: clean_shiplibs
clean_shiplibs: $(cleanlibmda)

.PHONY: wipe_shiplibs
wipe_shiplibs: $(cleanlibmda)

# ......................................
# The 'cleanlibmda' is more of a "wipe" since it also removes the static
# library. This is done, since it's easily rebuilt, and if not removed it
# sometimes causes linking problems when cleaning and rebuilding the rest of
# Midas but not libmda.)
.PHONY: cleanlibmda
cleanlibmda:
	cd libmda/lib && $(MAKE) clean
	cd libmda/lib && $(MAKE) cleanlib

# ......................................
# Clean the test suite.
.PHONY: cleantest
cleantest:
	cd test_suite && $(MAKE) clean

# ......................................
# Clean the MidasPot directory in the test suite.
.PHONY: cleanmidaspot
cleanmidaspot:
	$(RM) -r $(dir_POT_inst)

# ......................................
# Clean the MidasPot directory in the test suite.
.PHONY: cleanmidasdynlib
cleanmidasdynlib:
	$(RM) -r $(dir_DYNLIB_inst)

# ......................................
# Clean the (hidden) directory used for storing file search results.
.PHONY: cleanfilesearch
cleanfilesearch:
	$(RM) -r $(dir_FILE_SEARCH_VARS)

# ......................................
## Clean entire build, including executables.
#.PHONY : purge
#purge: $(foreach b, $(lst_BUILDS), purge_$(b))

# --------------------------------------
# ADDITIONALS
# Documentation, manual and such.
.PHONY : docu
docu:
	cd $(dir_DOCU) && $(MAKE)

.PHONY : cleandocu
cleandocu:
	cd $(dir_DOCU) && $(MAKE) clean

.PHONY : manual
manual:
	cd $(dir_MAN) && $(MAKE)

.PHONY : cleanmanual
cleanmanual:
	cd $(dir_MAN) && $(MAKE) clean

# --------------------------------------
# HELP AND TESTS
# There could be something like 'make help' which would output which targets
# are available and which flags are set.

# 'make help' prints
#    - regular targets
#    - enabled build types
#    - build type specific targets
# Formatting:
# (fht:  format, help, tab)
# (fhl:  format, help, left justified)
# (fhr:  format, help, right justified)
# (fhll: format, help, left justified line)
fht = ""   ""
fhl = %-21s
fhl2 = %-56s
fhr = %56s
fhll = $(fht)$(fhl)$(fhl2)

.PHONY : help
help:
	@$(PRINTF) "\n"
	@$(PRINTF) "%s\n" "+------------------------------------------------------------------------------+"
	@$(PRINTF) "%s\n" "|                            MidasCpp Makefile help.                           |"
	@$(PRINTF) "%s\n" "+------------------------------------------------------------------------------+"
	@$(PRINTF) "\n"
	@$(PRINTF) "%s\n"      "You can use one of the following general make commands:"
	@$(PRINTF) "$(fhll)\n" "make"          "Default target as specified in Makefile.config."
	@$(PRINTF) "$(fhll)\n" "make all"      "Build all executables, put in build dir. (all builds)."
	@$(PRINTF) "$(fhll)\n" "make main"     "Build main executable, put in build dir. (all builds)."
	@$(PRINTF) "$(fhll)\n" ""              "(Main executable: $(MAIN_exe).$(ext_EXE))"
	@$(PRINTF) "$(fhll)\n" "make libs"     "Build shipped libraries (that are enabled by configure)."
	@$(PRINTF) "$(fhll)\n" "make install"  "Install files of main build type in install dir."
	@$(PRINTF) "$(fhll)\n" "make complete" "Build all (all build types) and install main build."
	@$(PRINTF) "\n"
	@$(PRINTF) "$(fhll)\n" "make clean"     "Clean objects/dependencies for all builds and libraries."
	@$(PRINTF) "$(fhll)\n" "make cleanlibs" "Clean objects in libraries."
	@$(PRINTF) "$(fhll)\n" "make wipelibs"  "Completely remove the libraries."
	@$(PRINTF) "$(fhll)\n" "make cleantest" "Clean the test suite."
	@$(PRINTF) "\n"
	@$(PRINTF) "$(fhll)\n" "make docu" "Build documentation, in subdirectory '$(dir_DOCU)'."
	@$(PRINTF) "$(fhll)\n" "make manual" "Build manuals, in subdirectory '$(dir_MAN)'."
	@$(PRINTF) "$(fhll)\n" "make cleandocu" "Clean documentation, in subdirectory '$(dir_DOCU)'."
	@$(PRINTF) "$(fhll)\n" "make cleanmanual" "Clean manuals, in subdirectory '$(dir_MAN)'."
	@$(PRINTF) "\n"
	@$(PRINTF) "$(fhll)\n" "make help"           "Display this help and exit."
	@$(PRINTF) "$(fhll)\n" "make help_settings"  "Display settings/flags for various build types."
	@$(PRINTF) "$(fhll)\n" "make empty"          "An empty target; no prerequisites, no products."
	@$(PRINTF) "\n"
	@$(PRINTF) "%s\n"      "You can specify various build types through lst_BUILDS in Makefile.config."
	@$(PRINTF) "$(fhll)\n" "Build types:" "$(strip $(foreach t, $(lst_BUILDS), $(t)))"
	@$(PRINTF) "$(fhll)\n" "Main build:"  "$(MAIN_build)"
	@$(PRINTF) "\n"
	@$(PRINTF) "%s\n"      "A number of build type specific commands exist, e.g. 'make all_$(MAIN_build)':"
	@$(PRINTF) "$(fhll)\n" "make all_<type>"      "Like 'make all' but only for the <type> build."
	@$(PRINTF) "$(fhll)\n" "make main_<type>"     "Like 'make main' but only for the <type> build."
	@$(PRINTF) "$(fhll)\n" "make install_<type>"  "Like 'make install' but for the <type> build."
	@$(PRINTF) "$(fhll)\n" "make complete_<type>" "Like 'make complete' but installs the <type> build."
	@$(PRINTF) "$(fhll)\n" "make clean_<type>"    "Like 'make clean' but only for the <type> build."
	@$(PRINTF) "\n"
	@$(PRINTF) "%s\n"      "Some of the variables referred to:"
	@$(PRINTF) "$(fhll)\n" "Main executable(s):" "$(addsuffix .$(ext_EXE), $(MAIN_exe))"
	@$(PRINTF) "$(fhll)\n" "Build dir.:"         "$(BUILDDIR)"
	@$(PRINTF) "$(fhll)\n" "Install dir.:"       "$(INSTALLDIR)"
	@$(PRINTF) "\n"


# 'make help_settings' prints general settings/variables and those pertaining
# to specific build types. Most of it modified in Makefile.config.

# Template for help_settings for each build type.
#    $(1): build type
define tmpl_HELP_settings
$(PRINTF) "%s\n"      "Settings for \"$(1)\":";\
$(foreach flag, ARCH FC CC CXX FFLAGS CFLAGS CXXFLAGS INCLUDE_DIR,\
	$(PRINTF) "$(fhll)\n" "$(flag)_$(1)" "= $($(flag)_$(1))"; \
)\
$(foreach flag, CPPFLAGS LIB_LIST,\
	$(PRINTF) "$(fht)$(fhl)=\n" "$(flag)_$(1)";\
	$(foreach val, $($(flag)_$(1)),\
		$(PRINTF) "$(fht)$(fht)$(fhl2)\n" "$(val)";\
	)\
)\
$(PRINTF) "\n";
endef

.PHONY : help_settings
help_settings:
	@$(PRINTF) "\n"
	@$(PRINTF) "%s\n" "+------------------------------------------------------------------------------+"
	@$(PRINTF) "%s\n" "|                          MidasCpp Makefile settings.                         |"
	@$(PRINTF) "%s\n" "+------------------------------------------------------------------------------+"
	@$(PRINTF) "\n"
	@$(PRINTF) "%s\n" "(These are modified in Makefile.config.)"
	@$(PRINTF) "\n"
	@$(PRINTF) "%s\n\n"   "Build types enabled: $(strip $(foreach t, $(lst_BUILDS), $(t)))"
	@$(foreach t, $(lst_BUILDS), $(call tmpl_HELP_settings,$(t)))


# MBH-NB: Make a help that shows files and dirs.

.PHONY : test
test:
	@$(PRINTF) "Testing.\n"
	@$(PRINTF) "find              =$(cmd_FIND) $(flg_FIND_excl) $(flg_FIND_cxx)\n"
	@$(PRINTF) "lst_SRC_CXX_all   =\n\t$(lst_SRC_CXX_all)\n"
	@$(PRINTF) "lst_SRC_C_all     =\n\t$(lst_SRC_C_all)\n"
	@$(PRINTF) "lst_SRC_F_all     =\n\t$(lst_SRC_F_all)\n"
	@$(PRINTF) "lst_OBJ_CXX_all   =\n\t$(lst_OBJ_CXX_all)\n"
	@$(PRINTF) "lst_OBJ_C_all     =\n\t$(lst_OBJ_C_all)\n"
	@$(PRINTF) "lst_OBJ_F_all     =\n\t$(lst_OBJ_F_all)\n"
	@$(PRINTF) "$(foreach t, $(lst_SRC_types), $(ext_$(t)))\n"
	@$(PRINTF) "lst_OBJ           =\n\t$(lst_OBJ)\n"
	@$(PRINTF) "lst_SRC_PATHS     =\n\t$(lst_SRC_PATHS)\n"
	@$(PRINTF) "dir_EXCL          =\n\t$(dir_EXCL)\n"
	@$(PRINTF) "\$$(cmd_FIND) \$$(flg_FIND_path) \$$(flg_FIND_excl) \$$(flg_FIND_$(t)) =\n\t$(cmd_FIND) $(flg_FIND_path) $(flg_FIND_excl) $(flg_FIND_$(t))\n"
	@$(PRINTF) "lst_DATA          =\\n\\t$(lst_DATA)\n"

.PHONY : empty
empty:


# ------------------------------------------------------------------------------
#  SPECIAL TARGETS
# ------------------------------------------------------------------------------
# This section contains targets that are not handled by the general framework
# in the 'MAKEFILE DEVELOPER DETAILS' section (which handles most source,
# object, dependency, executable files). E.g. libraries, Tinker, etc.


# libmda and LAPACK libraries and their include directories.
# These are listed as having no prerequisites (though in fact they do), which
# means that if the file doesn't exist it will be built, but otherwise it is
# considered up-to-date. The rule for making them is to run the appropriate
# 'make' in the mathlib subdirectory. Ideally everything should be handled by
# one single Makefile (this one), but since the libraries rarely need building
# more than once, the current solution is acceptable.
# NB! Only the rules of the LIB_SHIPPED_<lib> will actually make the libraries,
# while the other LIB_SHIPPED_ files are made to depend on the former. So it
# will all cause the libraries to be made, but in this way it can't erroneously
# be executed in parallel, which will cause the configure script to be run
# simultaneously for the same library resulting in errors.
# This is not the most elegant solution but it works for practical purposes.

# libmda (LIBrary for MultiDimensional Arrays)
libmda/lib/libmda.a:
	cd libmda/lib && export LIBMDA_COMPILER_SETTING="$(COMPILE_CXX) $(CXXFLAGS)" && $(MAKE)

# Tinker interface.
# We list the dependency upon 'tinker_interface.i' and how to create the
# latter, but the compilation of the .f90 should be handled by the general
# framework.

# Arguments:
#    $(1): build type
define tmpl_DEP_tinker_interface
$(BUILDDIR)/$(1)/$(dir_OBJ)/tinker_interface.$(ext_OBJ): tinker_interface/tinker_interface.i
endef

$(foreach t, $(lst_BUILDS), $(eval $(call tmpl_DEP_tinker_interface,$(t))))

# MBH-NB: Not sure this works as intended (haven't been able to compile Tinker
# as of yet).
tinker_interface/tinker_interface.i: \
	$(addprefix $(PATH_TO_TINKER)/source/,\
	sizes.i\
	atmtyp.i\
	atoms.i\
	couple.i\
	keys.i\
	files.i\
	hescut.i\
	resdue.i\
	params.i\
	fields.i\
	katoms.i\
	potent.i\
	defiv.i\
	)
	@$(RM) $@
	@sed -e 's/^c\|^C\(.*\)/!\1/g' -e ':a; $!N;s/,\n/,\&\n/;ta;P;D ;' \
	   $(PATH_TO_TINKER)/source/sizes.i >$@
	@sed -e 's/^c\|^C\(.*\)/!\1/g' -e ':a; $!N;s/,\n/,\&\n/;ta;P;D ;' \
	   $(PATH_TO_TINKER)/source/atmtyp.i >>$@
	@sed -e 's/^c\|^C\(.*\)/!\1/g' -e ':a; $!N;s/,\n/,\&\n/;ta;P;D ;' \
	   $(PATH_TO_TINKER)/source/atoms.i >>$@
	@sed -e 's/^c\|^C\(.*\)/!\1/g' -e ':a; $!N;s/,\n/,\&\n/;ta;P;D ;' \
	   $(PATH_TO_TINKER)/source/couple.i >>$@
	@sed -e 's/^c\|^C\(.*\)/!\1/g' -e ':a; $!N;s/,\n/,\&\n/;ta;P;D ;' \
	   $(PATH_TO_TINKER)/source/keys.i >>$@
	@sed -e 's/^c\|^C\(.*\)/!\1/g' -e ':a; $!N;s/,\n/,\&\n/;ta;P;D ;' \
	   $(PATH_TO_TINKER)/source/files.i >>$@
	@sed -e 's/^c\|^C\(.*\)/!\1/g' -e ':a; $!N;s/,\n/,\&\n/;ta;P;D ;' \
	   $(PATH_TO_TINKER)/source/hescut.i >>$@
	@sed -e 's/^c\|^C\(.*\)/!\1/g' -e ':a; $!N;s/,\n/,\&\n/;ta;P;D ;' \
	   $(PATH_TO_TINKER)/source/resdue.i >>$@
	@sed -e 's/^c\|^C\(.*\)/!\1/g' -e ':a; $!N;s/,\n/,\&\n/;ta;P;D ;' \
	   $(PATH_TO_TINKER)/source/params.i >>$@
	@sed -e 's/^c\|^C\(.*\)/!\1/g' -e ':a; $!N;s/,\n/,\&\n/;ta;P;D ;' \
	   $(PATH_TO_TINKER)/source/fields.i >>$@
	@sed -e 's/^c\|^C\(.*\)/!\1/g' -e ':a; $!N;s/,\n/,\&\n/;ta;P;D ;' \
	   $(PATH_TO_TINKER)/source/katoms.i >>$@
	@sed -e 's/^c\|^C\(.*\)/!\1/g' -e ':a; $!N;s/,\n/,\&\n/;ta;P;D ;' \
	   $(PATH_TO_TINKER)/source/potent.i >>$@
	@sed -e 's/^c\|^C\(.*\)/!\1/g' -e ':a; $!N;s/,\n/,\&\n/;ta;P;D ;' \
	   $(PATH_TO_TINKER)/source/deriv.i >>$@


# ==============================================================================
#  MAKEFILE DEVELOPER DETAILS (CHANGE AT OWN RISK!)
# ==============================================================================
# This section contains the infrastructure of the Makefile and you should under
# ANY normal circumstances NOT need to change this. It mainly concerns how to
# compile and link, how to locate source files, and how to handle dependency
# generation. It is intended to be possible to control all regular usage needs
# through the Makefile.config. Only if this is not possible should you make
# changes to the section below, but in that case consider consulting the author
# of the Makefile first.

# ------------------------------------------------------------------------------
#  MISC
# ------------------------------------------------------------------------------
# Set the SHELL variable to avoid trouble on systems where the SHELL variable
# might be inherited from the environment.
SHELL = /bin/sh

# We don't want 'make' to use any built-in rules because it will then
# unnecessarily check for some obsolete pattern matches. (Gives a slight
# speed-up, roughly 15-20 %.)
MAKEFLAGS += -r

# Delete existing suffix rules and redefine the suffixes we need.
# MBH-NB: Don't think this is necessary, actually. But doesn't harm either.
.SUFFIXES :


# ------------------------------------------------------------------------------
#  DIRECTORIES
# ------------------------------------------------------------------------------
# We need to make directories for object, dependency, executable files, etc.
# For each build type, we need a directory <build_type>.  In each of those we
# make directories for objects, dependency files, executables. The <build_type>
# directories are "order-only prerequisites" (signified by the | in the prereq.
# list) for the obj./dep./bin. directories, meaning that <build_type> will be
# built before these if it doesn't exist, but won't be updated just because
# files are changed inside the <build_type> directory (exactly the behaviour we
# want for directories). Likewise the obj./dep./bin. directories will later on
# be declared as order-only prerequisites for the corresponding files.
# Also make the directory for MidasPot executables (in the test_suite).

# Command for making directories (will probably always be mkdir).
# It works without giving the -p flag for mkdir ("Create intermediate
# directories as required... no error will be reported if a directory given as
# an operand already exists." from 'man mkdir'), so we'll actually get an error
# if something goes wrong, which is probably preferable.
# Only use the -p version if really needed (and prefer it for features that can
# be disabled), it's less compatible.
# [Look in Makefile.config to see how MKDIR and MKDIR_P are defined.]

# MidasPot installation direcotry.
$(dir_POT_inst):
	$(MKDIR_P) $@

# MidasDynlib installation direcotry.
$(dir_DYNLIB_inst):
	$(MKDIR_P) $@

# Build directory and subdirectories.
$(BUILDDIR):
	$(MKDIR_P) $@

# Template: targets '$(1)'.
#    $(1): build type
define tmpl_TARGET_build_type_dir
$(BUILDDIR)/$(1): | $(BUILDDIR)
	$(MKDIR_P) $$@
endef

# Template: targets '$(1)/$(2)'.
#    $(1): build type
#    $(2): file type
define tmpl_TARGET_build_type_file_type_dir
$(BUILDDIR)/$(1)/$(2): | $(BUILDDIR)/$(1)
	$(MKDIR_P) $$@
endef

$(foreach b, $(lst_BUILDS),\
   $(foreach f, $(dir_OBJ) $(dir_DEP) $(dir_BIN),\
      $(eval $(call tmpl_TARGET_build_type_file_type_dir,$(b),$(f)))\
   )\
   $(eval $(call tmpl_TARGET_build_type_dir,$(b)))\
)

# The dir_MAKE directory may need to be constructed... Do this always, not as
# part of a rule. All parent directories of dir_MAKE _must_ exist prior to
# executing 'make'.
$(shell if ! [ -d $(dir_MAKE) ]; then $(MKDIR) $(dir_MAKE); fi)

# ------------------------------------------------------------------------------
#  SOURCE FILE DETECTION
# ------------------------------------------------------------------------------
# Default behaviour is to do a full file/directory search. Exceptions:
#  -  If lst_OBJ and lst_SRC_PATHS are already assigned some value, stick to that
#     value; it means this make execution is a sub-make, and we might then just
#     use the inherited values.
#  -  If reuse_file_search=true (in command line or Makefile.config) we read in
#     the variables from appropriate files. This reuses the results from last
#     execution of make.
# Otherwise search it all.
#
# Let's assume that if either of the variables to be defined in the file search
# have not been defined it's safer to just run it all. The check is a little
# ackward due to make's lack of logical and/or.

# The variables that will be set in a file search:
variables_from_file_search := lst_SRC_PATHS lst_OBJ\
   $(foreach t, $(lst_SRC_types), lst_SRC_$(t)_all)\
   $(foreach t, $(lst_SRC_types), lst_OBJ_$(t)_all)\
   $(foreach t, $(lst_SRC_types), lst_OBJ_$(t))

bool_run_file_search := false
# Define a template, that sets the bool to true if variable is undefined.
# Arguments:
#  $1: the variable to check for.
define tmpl_VARIABLE_DEF_CHECK
   ifndef $(1)
      bool_run_file_search := true
   endif
endef
# Then evaluate template to check each variable.
$(foreach v, $(variables_from_file_search), $(eval $(call tmpl_VARIABLE_DEF_CHECK,$(v))))

# Then start running the file search if variables are not yet defined.
ifeq "$(strip $(bool_run_file_search))" "true"
   # If user requested taking files from the stored file, do so. Otherwise do
   # full search.
   ifeq "$(strip $(reuse_file_search))" "true"
      # Include makefiles with variables. Error if 'include' fails.
      $(foreach v, $(variables_from_file_search), $(eval include $(dir_FILE_SEARCH_VARS)/$(v).makefile))
   else
      # FIND COMMANDS AND FLAGS
      # The 'find' command should work in regular Unix-like environments.
      cmd_FIND       = $(FIND)
      
      # Flags for finding different types of files; source files, directories.
      #    $(foreach t, )  Loop over source file type in $(lst_SRC_types), e.g.
      #                    CXX, C, F (C++, C, Fortran).
      #    $(eval ...)     'make' will interpret the following as an actual
      #                    command in the Makefile (in this case a variable
      #                    assignment).
      #    $(dir_WORK)     The directory within which we search.
      #    -type f         Look for regular files.
      #    $(foreach...)   Expands to " -name '*.cpp' -o" etc. for each C++
      #                    extension given in $(ext_CXX).
      #    -name '*.cpp'   Look for files with names ending in .cpp.
      #    -o              Logical "or", so that we can match either of the
      #                    extensions in $(ext_CXX).
      #    -false          Logical false, just to "kill" the last "or" from the
      #                    'foreach' expansion.
      #    -type d         Look for directories.
      flg_FIND_path  := $(dir_WORK)
      flg_FIND_dir   := -type d
      
      $(foreach t, $(lst_SRC_types),\
         $(eval flg_FIND_$(t) := -type f \( $(foreach e, $(ext_$(t)), -name '*.$(e)' -o) -false \) )\
      )
      
      # Flags for excluding certain files/directories from the search.
      #    $(foreach...)   Expands to " ! \( -path $(dir_WORK)/mathlib -prune \)"
      #                    etc. (for "mathlib" or whatever might be in the
      #                    $(dir_EXCL) variable).
      #    !               Negation, i.e. don't match files/directories
      #                    fulfilling the \(...\). Some (all?) version of find
      #                    can also use '-not' as negation operator.
      #    -path [pattern] True if pattern matches the pathname being examined.
      #    -prune          Makes 'find' NOT descend into the matched directory,
      #                    resulting in efficient exclusion of directories.
      #                    Returns true (which then makes the negation operator
      #                    return false).
      #    -path '*/\.*'   Prunes any file/directory beginning with a '.', i.e.
      #                    excludes hidden files.
      flg_FIND_excl  := $(foreach d, $(dir_EXCL), ! \( -path $(dir_WORK)/$(d) -prune \))
      flg_FIND_excl  += ! \( -path '*/\.*' -prune \)
      flg_FIND_excl  += $(foreach f, $(file_EXCL), \( ! -name $(f) \))
      
      # MBH-NB: Use -prune when together with 'find' for more efficient file
      # finding. Put -print at the end.
      # See http://stackoverflow.com/questions/4210042/exclude-directory-from-find-command:
      # f10bit solution:
      # find . -path ./misc -prune -o -name '*.txt' -print
      # find . -type d \( -path dir1 -o -path dir2 -o -path dir3 \) -prune -o -print
      # Daniel C. Sobral solution (slightly faster apparently):
      # find build -not \( -path build/external -prune \) -name \*.js
      # find build -not \( -path build/external -prune \) -not \( -path build/blog -prune \) -name \*.js
      # find . ! \( -path './vcc' -prune \) -type d
      
      # FINDING SOURCE FILES AND DIRECTORIES
      # Use the commands and flags above to locate source files/directories and
		# assign to corresponding variables. We keep the relative directory path
		# (but trim away the dir_WORK prefix) so that we can replicate the
		# directory tree in the build directory.
      
      $(foreach t, $(lst_SRC_types),\
         $(eval\
            lst_SRC_$(t)_all := \
               $(patsubst $(dir_WORK)/%, %, \
                  $(shell $(cmd_FIND) $(flg_FIND_path) $(flg_FIND_excl) $(flg_FIND_$(t)))\
               )\
         )\
      )
      
      # We then change the file extension from source to object extension to
      # generate lists of object files (all with same object file extension).
      # It is done by stripping the extension through use of 'basename', then
      # appending the appropriate suffix. This could also have been done (maybe
      # more elegantly) using 'make's' 'patsubst' but then we would have
      # problems handling multiple
      # valid file extensions for the same kind of source files (e.g. .cc,
      # .cpp, .C for C++ files).
      $(foreach t, $(lst_SRC_types),\
         $(eval\
            lst_OBJ_$(t)_all := \
               $(addsuffix .$(ext_OBJ), $(basename $(lst_SRC_$(t)_all)))\
         )\
      )
      
      # From the list of all source files we filter out the files containing
      # 'main' functions, specified in $(lst_MAINS), $(lst_TOOLS) $(lst_DYNLIB) and
      # $(lst_POT). (Remembering to prepend the corresponding directory name.)
      $(foreach t, $(lst_SRC_types),\
         $(eval lst_OBJ_$(t) := \
            $(filter-out \
               $(patsubst %, $(dir_MAINS)/%.$(ext_OBJ), $(lst_MAINS)) \
               $(patsubst %, $(dir_TOOLS)/%.$(ext_OBJ), $(lst_TOOLS)) \
               $(patsubst %, $(dir_POT)/%.$(ext_OBJ), $(lst_POT)) \
               $(patsubst %, $(dir_DYNLIB)/%.$(ext_OBJ), $(lst_DYNLIB)) \
               , $(lst_OBJ_$(t)_all) \
            )\
         )\
      )
      
      # We need a variable containing all object files, excluding main files.
      lst_OBJ := $(foreach t, $(lst_SRC_types), $(lst_OBJ_$(t)))
      
      
      # The directories are assigned to lst_SRC_PATHS, which 'make' will use
      # for replicating the source file directory tree in the build directory.
      # We trim away the dir_WORK prefix, though.
      lst_SRC_PATHS := $(filter-out .,\
         $(patsubst $(dir_WORK)/%, %, \
            $(shell $(cmd_FIND) $(flg_FIND_path) $(flg_FIND_excl) $(flg_FIND_dir))\
         )\
      )
      
      # Create the directory in which to save files with variables.
      $(shell if ! [ -d $(dir_FILE_SEARCH_VARS) ]; then if ! $(MKDIR) $(dir_FILE_SEARCH_VARS); then $(MKDIR_P) $(dir_FILE_SEARCH_VARS); fi; fi)
      # At last, write all these variables from the file search to files.
      # Arguments:
      #  $(1): variable name
      define tmpl_PRINT_VARIABLES_TO_FILES
         $(shell $(PRINTF) '$(1) =' > $(dir_FILE_SEARCH_VARS)/$(1).makefile; for f in $(value $(1)); do $(PRINTF) '\\\n%s' "$${f}" >> $(dir_FILE_SEARCH_VARS)/$(1).makefile; done)
      endef
      $(foreach v, $(variables_from_file_search), $(eval $(call tmpl_PRINT_VARIABLES_TO_FILES,$(v))))
   endif
endif

# In either case, we now (re)export the file search variables, so that any
# sub-calls to make ($(MAKE)) can reuse these values, thus avoiding an
# unnecessary file search with the same result.
$(foreach v, $(variables_from_file_search), $(eval export $(v)))

# ------------------------------------------------------------------------------
#  COLORS
# ------------------------------------------------------------------------------
# If color support some outputs will be colored by the commands, which should
# have been set in Makefile.config, by default it's the 'tput' command.
# Set colors:
ifeq "$(strip $(color_support))" "true"
	prog_color_beg=$$$$($(color_format_beg))#EOL
	prog_color_end=$$$$($(color_format_end))#EOL
else
	prog_color_beg=#EOL
	prog_color_end=#EOL
endif

# ------------------------------------------------------------------------------
#  DISPLAY PROGRESS
# ------------------------------------------------------------------------------
# A "fun" little gimmick that outputs a percentage indicating how many of the
# source files have been compiled out of the total required in this invocation
# of 'make'.
# Works like this:
# 1. At this point of 'make' reading the Makefile, make a dry-run with the same
#    command-line arguments. Grep and count the lines corresponding to source
#    file compilation. (For each build type.)
# 2. Make a directory in which we'll touch some dummy files when each source
#    file has been compiled.
# 3. Make some pre-compilation output, where the number of dummy files are
#    counted, then divided by the total number.
# 4. Some post-compilation command that touches the dummy file.
#  
# Implemented this way it _should_ work with parallel excecution as well - the
# percentage doesn't have to be spot on.

# Only do this stuff if we are not making some of the "non-building" targets.
# Check this by filtering out non_compilation_goals from the MAKECMDGOALS; if
# this does not return an empty string the MAKECMDGOALS was a compilation
# target, and we proceed to do the progress display stuff.
non_compilation_goals = libs cleanlibmda cleanlibs wipelibs cleantest
non_compilation_goals += build_shiplibs clean_shiplibs wipe_shiplibs
non_compilation_goals += cleanmidaspot cleanmidasdynlib cleanfilesearch
non_compilation_goals += install $(foreach b, $(lst_BUILDS), install_$(b))
non_compilation_goals += clean $(foreach b, $(lst_BUILDS), clean_$(b))
non_compilation_goals += help help_settings test empty
non_compilation_goals += install_extlibs $(foreach l, $(lst_EXTLIBS_names), install_ext_$(l))
non_compilation_goals += install_vim
non_compilation_goals += $(foreach l, $(lst_EXTLIBS_names), download_ext_$(l))
non_compilation_goals += $(foreach l, $(lst_EXTLIBS_names), unpack_ext_$(l))
non_compilation_goals += $(foreach l, $(lst_EXTLIBS_names), build_ext_$(l))
non_compilation_goals += $(foreach l, $(lst_EXTLIBS_names), clean_ext_$(l))
non_compilation_goals += $(foreach l, $(lst_EXTLIBS_names), wipe_ext_$(l))
non_compilation_goals += download_extlibs unpack_extlibs build_extlibs clean_extlibs wipe_extlibs

# A trick to also be able to handling the "no target" case (which usually
# defaults to default, all, main, complete or something) as a "compilation
# goal".
# If MAKECMDGOALS is the empty string, just add something to it (not on the
# list above) so that it will pass the if statement.
tmp_MAKECMDGOALS = $(MAKECMDGOALS)
ifeq "" "$(strip $(MAKECMDGOALS))"
   tmp_MAKECMDGOALS += dummy_variable_used_for_passing_if_statement
endif

ifneq "" "$(strip $(filter-out $(non_compilation_goals), $(tmp_MAKECMDGOALS)))"
   # ... and then only do it if activated.
   ifeq "$(strip $(progress_display))" "true"
      # Total number of files to compile in this invocation.
      ifneq "$(strip $(bool_call_recursively))" "false"
         # The 'make invocation:
         #  - --dry-run: don't actually make anything, just pretend. (So we can
         #    grep for the object files.)
         #  - bool_call_recursively=false: to avoid infinite recursion.
         #  - --assume-old=<lib_files>: otherwise they will be unpacked, and we
         #    don't want that done in this dry-run call.
         #  - $(MAKECMDGOALS): to call make with same cmd-line goal as user.
         cmd_MAKE_COUNT := $(MAKE) --dry-run bool_call_recursively=false
         cmd_MAKE_COUNT += $(foreach l, $(lst_SHIPLIBS), --assume-old=$(dir_WORK)/$(l))
         cmd_MAKE_COUNT += $(foreach l,$(lst_EXTLIBS_prereq), --assume-old=$(l))
         cmd_MAKE_COUNT += $(foreach l,$(lst_EXTLIBS_names), --assume-old=$(dir_LIB_$(l)))
         cmd_MAKE_COUNT += $(foreach l,$(lst_EXTLIBS_names), --assume-old=$(dir_LIB_$(l)).$(ext_LIB_TAR_$(l)))
         cmd_MAKE_COUNT += $(MAKECMDGOALS)
         # The grep command to use; greps 
         # "-o <build_dir>/<build_type>/obj/<file_name>/.o" at end of line.
         cmd_GREP_COUNT = grep -c "\-o $(BUILDDIR)/.*/$(dir_OBJ)/.*\.$(ext_OBJ)$$"
         $(shell $(PRINTF) "export var_FILE_COUNT = " > $(file_COUNT); $(cmd_MAKE_COUNT) 2>/dev/null | $(cmd_GREP_COUNT) >> $(file_COUNT))
         # And include it to get the value.
         include $(file_COUNT)
      endif
      # Don't repeat more than once!
      export bool_call_recursively = false

      # Directory for dummy files. Delete any contents if already there.
      $(shell if [ -d $(dir_COUNT) ]; then $(RM) -r $(dir_COUNT); fi; $(MKDIR) $(dir_COUNT))

      # Pre-compilation command. ls -1U: list files in dir., unsorted, 1 per line.
      # Then do a line count. If there's color support, color the output.
      # Result is something like [ 43% (of 417 files)]
      # If file count was zero and we still end up compiling (it's a flaw then,
      # but anyway), avoid divide-by-zero error by avoiding percentage stuff:
      # NB! The cmd_PRECOMP_percent is a little "hacky-hacky" in the sense that
      # the PRINTF statement contains 3 tokens (in the var_FILE_COUNT != 0
      # case):
      #   %3d the percentage
      #   %d  the number of files
      #   %s  the information string ("Building ...")
      # The latter shall contain the name of the object file under
      # construction, and so is omitted here and must be supplied later, see
      # the tmpl_COMP_* commands.
      # The reason for printing all in one statement is so that print
      # statements from parallel make jobs don't clutter up in the stdout.
      cmd_PRECOMP_percent = curr_files=$$$$(ls -1U $(dir_COUNT)|wc -l);
      ifneq "$(strip $(var_FILE_COUNT))" "0"
         cmd_PRECOMP_percent += curr_perc=$$$$(($$$${curr_files}*100/$(var_FILE_COUNT)));
         cmd_PRECOMP_percent += $(PRINTF) "$(prog_color_beg)[%3d%% (of %d files)]$(prog_color_end) %s\n"\
                                $$$${curr_perc} $(var_FILE_COUNT)
      else
         cmd_PRECOMP_percent += $(PRINTF) "$(prog_color_beg)[%4d files compiled]$(prog_color_end) %s\n"\
                                $$$${curr_files}
      endif

      # Post-compilation is just to touch (thus creating) a dummy file in the
      # directory. The 'subst' replaces directory /'s with _'s to generate a
      # proper filename.
      cmd_POSTCOMP_percent = touch $(dir_COUNT)/$(1)_$$(subst /,_,$$*).compiled
   else
      # Need a PRINTF here for consistency with the cmd_PRECOMP_percent above.
      # It must take one argument, the string "Building ...".
      cmd_PRECOMP_percent = $(PRINTF) "%s\n"
   endif
endif

# ------------------------------------------------------------------------------
#  AUTOMATICALLY GENERATING DEPENDENCY FILES
# ------------------------------------------------------------------------------
# Flags for c/c++ preprocessor that, as a side-effect of compilation,  will
# generate a dependency file based on (potentially nested) "#include"
# statements in the source file(s). The dependency files are saved to a
# temporary file that is then moved/renamed to the actual file after
# compilation (to avoid corrupted files in case of unsuccessful compilation).
# The target object file is touched in the end to ensure that it's newer than
# the dependency file; otherwise it will always be rebuilt in some cases.
#    -MT $$@   Set the name of the target in the generated dependency file.
#    -MMD      Generate dependency information as a side-effect of compilation,
#              not instead of compilation. This version omits system headers from
#              the generated dependencies: if you prefer to preserve system
#              headers as prerequisites, use -MD.
#    -MP       Adds a target for each prerequisite in the list, to avoid errors
#              when deleting files.
#    -MF $(1)/$(dir_DEP)/$$*.tmp_$(ext_DEP)  
#              Write the generated dependency file to a temporary location
#              $(1)/$(dir_DEP)/$*.tmp_$(ext_DEP) to avoided corrupted
#              dependency files if compilation fails. Will then be moved to the
#              actual file after compilation.
#    $$*       We use double $$ in stead of a single $ to utilize the second
#              expansion feature of 'make', because the variables are used in a
#              template, that is called and evaluated. When using 'eval' the
#              the substitution $$ --> $ is made, meaning that the final text
#              interpreted by 'make' will correctly be "$@" (the target when
#              the recipe is CALLED, i.e. when the actual target is to be
#              built). If only "$@" was used it would be interpreted as the
#              current target when the template is EVALUATED, which would most
#              likely be the empty string since make is at that point not in
#              the process of building anything yet.
#    $$$$@     Like $$ but twice, because we do two 'eval's on the flg_CPP_dep,
#              one when constructing the cmd_COMP_cxx_<built_type> command, and
#              one when calling the templated rule, see below.
#    $@        File name of the target of the rule.
#    $*        The stem with which an implicit rule matches, i.e. the % part.
#              (See "automatic variables" in the manual.)
#    $(1)      The build type directory, see the template later.
flg_CPP_dep        = -MT $$$$@ -MMD -MP -MF $(BUILDDIR)/$$(1)/$(dir_DEP)/$$$$*.tmp_$(ext_DEP)
cmd_POSTCOMP_dep   = mv -f $(BUILDDIR)/$(1)/$(dir_DEP)/$$*.tmp_$(ext_DEP) $(BUILDDIR)/$(1)/$(dir_DEP)/$$*.$(ext_DEP) && touch $$@


# ------------------------------------------------------------------------------
#  COMPILATION RULES
# ------------------------------------------------------------------------------
# Compilation commands (for writing the rules more compactly). There must be
# one for each build type, handled by 'foreach' loops, and one for each
# programming language, handled manually to allow for different compilation
# rules for different languages.
$(foreach b, $(lst_BUILDS),\
   $(eval\
      cmd_COMP_CXX_$(b) = $(CXX_$(b))\
                          $(INCLUDE_DIR_$(b))\
                          $(CPPFLAGS_$(b))\
                          $(flg_CPP_dep)\
                          $(CXXFLAGS_$(b))\
                          -c\
   )\
)

$(foreach b, $(lst_BUILDS),\
   $(eval\
      cmd_COMP_C_$(b)   = $(CC_$(b))\
                          $(INCLUDE_DIR_$(b))\
                          $(CPPFLAGS_$(b))\
                          $(flg_CPP_dep)\
                          $(CFLAGS_$(b))\
                          -c\
   )\
)

$(foreach b, $(lst_BUILDS),\
   $(eval\
      cmd_COMP_F_$(b)   = $(FC_$(b))\
                          $(FFLAGS_$(b))\
                          -c\
   )\
)

# Handling verbose or non-verbose output.
# If verbose_make is enabled, (in Makefile.config) commands from the
# compilation/linking/directory making process will be displayed on the
# command-line. If not enabled, detail_verbose_make is set to the @ character
# which will suppress display of said commands.
ifeq "$(strip $(verbose_make))" "true"
   detail_verbose_make =#EOL.
else
   detail_verbose_make =@#EOL.
endif

# Templates for compilation.
#  ... $(1)/$(dir_DEP)/%.$(ext_DEP)
#                  This declares the generated dependency file as a
#                  prerequisite of the target, so that if it's missing the
#                  target will be rebuilt (and thus the dependency file will be
#                  regenerated).
#  %.$(ext_CXX)    The % pattern here includes the relative path, so 'make'
#                  will know where to find the file.
#  $$<, etc.       Second expansion, $$ --> $ when template is evaluated, see
#                  explanation above section.
#  ... | ...$(1)/$(dir_OBJ)/... ...$(1)/$(dir_DEP)/...
#                  Declare the directories as order-only prerequisites of the
#                  object files, meaning that they will be built if they are
#                  not existing already, but that the target file won't be
#                  rebuilt just because the directory has been updated (which
#                  happens when a new file is created in it).
#                  Further explanations:
#                  -  $$$$(dir $$$$(*)): This will take the directory part of
#                  the matching pattern stem (i.e the % in the rule). In other
#                  words it'll tell 'make' that the object file directory is a
#                  prerequisite for building the object file (and likewise the
#                  dependency file directory). This dependency triggers the
#                  replication of the source file directory tree in
#                  $(BUILDDIR)/<build_type>/$(dir_OBJ) and .../$(dir_DEP)
#                  respectively.
#                  -  $$$$(abspath .../): Putting a trailing slash to the dir
#                  name, so there will be at least one independent of the
#                  result of $$$$(dir ...) (which would generally put a
#                  trailing slash). The 'abspath' will reduce any multiple /'s
#                  to a single /. There were problems with this question about
#                  the number of trailing slashes, and this way of doing it
#                  (using abspath) resolved it.
#                  -  $$$$: A double 'second expansion'. When evaluating the
#                  template, it will produce something like:
#                     <obj_dir>/%.o: %.cc <dep_dir>/%.dep 
#                     | $$(abspath <obj_dir>/$$(dir $$*)
#                       $$(abspath <dep_dir>/$$(dir $$*)
#                       ...
#                  and this is apparently the way to make a target depend on
#                  its own target directory.
#  $(detail_verbose_make)
#                  Should be either the empty string or the @ character, thus
#                  giving verbose/non-verbose output, respectively.
# 
# Arguments:
#    $(1): build type (which defines both the directory and the type of
#          compilation command to use.
#    $(2): extension (e.g. cc, cpp, C; c; f, f90, f03)
#
$(foreach f, $(lst_DYNLIB),\
	$(eval\
		flags_DYNLIB_$(f) = -fPIC \
	)\
)

# C++
define tmpl_COMP_CXX
$(BUILDDIR)/$(1)/$(dir_OBJ)/%.$(ext_OBJ) \
            :  %.$(2) \
               $(BUILDDIR)/$(1)/$(dir_DEP)/%.$(ext_DEP) \
            |  $$$$(abspath $(BUILDDIR)/$(1)/$(dir_OBJ)/$$$$(dir $$$$*)/) \
               $$$$(abspath $(BUILDDIR)/$(1)/$(dir_DEP)/$$$$(dir $$$$*)/) \
               $(lst_SHIPLIBS) \
               $(lst_EXTLIBS_prereq)
	@$(cmd_PRECOMP_percent) "Building <$(1)>/$(dir_OBJ)/$$*.$(ext_OBJ)"
	$(detail_verbose_make)$(cmd_COMP_CXX_$(1)) $$(flags_DYNLIB_$$(notdir $$*)) $$< -o $$@
	$(detail_verbose_make)$(cmd_POSTCOMP_dep)
	$(detail_verbose_make)$(cmd_POSTCOMP_percent)
endef

# C
define tmpl_COMP_C
$(BUILDDIR)/$(1)/$(dir_OBJ)/%.$(ext_OBJ) \
            :  %.$(2) \
               $(BUILDDIR)/$(1)/$(dir_DEP)/%.$(ext_DEP) \
            |  $$$$(abspath $(BUILDDIR)/$(1)/$(dir_OBJ)/$$$$(dir $$$$*)/) \
               $$$$(abspath $(BUILDDIR)/$(1)/$(dir_DEP)/$$$$(dir $$$$*)/) \
               $(lst_SHIPLIBS) \
               $(lst_EXTLIBS_prereq)
	@$(cmd_PRECOMP_percent) "Building <$(1)>/$(dir_OBJ)/$$*.$(ext_OBJ)"
	$(detail_verbose_make)$(cmd_COMP_C_$(1)) $$< -o $$@
	$(detail_verbose_make)$(cmd_POSTCOMP_dep)
	$(detail_verbose_make)$(cmd_POSTCOMP_percent)
endef

# Fortran
define tmpl_COMP_F
$(BUILDDIR)/$(1)/$(dir_OBJ)/%.$(ext_OBJ) \
            :  %.$(2) \
            |  $$$$(abspath $(BUILDDIR)/$(1)/$(dir_OBJ)/$$$$(dir $$$$*)/) \
               $(lst_SHIPLIBS) \
               $(lst_EXTLIBS_prereq)
	@$(cmd_PRECOMP_percent) "Building <$(1)>/$(dir_OBJ)/$$*.$(ext_OBJ)"
	$(detail_verbose_make)$(cmd_COMP_F_$(1)) $$< -o $$@
	$(detail_verbose_make)$(cmd_POSTCOMP_percent)
endef

# Template for some detailed rules for the dependency files.
#  .../%.$(ext_DEP): ;
#                  Create a pattern rule with an empty recipe, so that 'make'
#                  won't fail if the dependency file doesn't exist.
#  .PRECIOUS: $(dir_DEP/%.$(ext_DEP)
#                  Mark the dependency files precious to 'make', so they won't
#                  be automatically deleted as intermediate files.
#  -include ...    Include the dependency files that exist; translate each file
#                  listed in lst_SRC_CXX_all into its dependency file. Use
#                  -include (instead of include) to avoid failing on
#                  non-existent files.
# Arguments:
#    $(1): build type (directory)

define tmpl_DETAILS_dep
$(BUILDDIR)/$(1)/$(dir_DEP)/%.$(ext_DEP): ;

.PRECIOUS : $(BUILDDIR)/$(1)/$(dir_DEP)/%.$(ext_DEP)

-include $(patsubst %, $(BUILDDIR)/$(1)/$(dir_DEP)/%.$(ext_DEP), $(basename $(lst_SRC_CXX_all) $(lst_SRC_C_all)))
endef

# Evaluation of the templates.
# %.<obj. ext.> : %.<src. ext.> 
#                  This empty rule is to delete any rule predefined by 'make'
#                  so that we can force it to use our own.
# $(foreach t,... $(foreach s,... $(foreach e,...)))
#                  Evaluate the rule for each combination of build type and
#                  source file extension.

$(foreach e, $(ext_CXX) $(ext_C) $(ext_F), $(eval %.$(ext_OBJ): %.$(e)))

$(foreach t, $(lst_BUILDS),\
   $(foreach s, $(lst_SRC_types),\
      $(foreach e, $(ext_$(s)),\
         $(eval $(call tmpl_COMP_$(s),$(t),$(e)))\
      )\
   )\
   $(eval $(call tmpl_DETAILS_dep,$(t)))\
)

# ------------------------------------------------------------------------------
#  REPLICATING DIRECTORY TREE
# ------------------------------------------------------------------------------
# Rules for replicating the source directory tree in the build directory.
# It uses the $(lst_SRC_PATHS) as found by 'find' earlier to make rules for the
# directories that we know contain source files, so as not to provide rules for
# just making _any_ directory in some potentially exotic place.
#
# Also the use of $(abspath ...) and the trailing (extra) / (in .../%/) is to
# mirror the format in tmpl_COMP_CXX etc., and was -- as mentioned there --
# required to make the directory prerequisites work smoothly.
#
# The recipe uses the $(MKDIR_P) (probably it's = 'mkdir -p') so that any
# parent directories will be made automatically. This would also build
# $(BUILDDIR)/$(1)/$(2) but it's listed as a separate (order-only) prerequisite
# in order to call its own rule (this is mostly for historic reasons).
#
# $1: The build type.
# $2: The file type; obj, dep
define tmpl_MKDIR_SRC_TREE
$$(abspath $(patsubst %, $(BUILDDIR)/$(1)/$(2)/%/, $(lst_SRC_PATHS))): | $(BUILDDIR)/$(1)/$(2)
	$(detail_verbose_make)$(MKDIR_P) $$@
endef

# Then evaluate the rule for all build types and the two required target
# directories $(dir_OBJ) and $(dir_DEP).
$(foreach b, $(lst_BUILDS),\
   $(foreach f, $(dir_OBJ) $(dir_DEP),\
      $(eval $(call tmpl_MKDIR_SRC_TREE,$(b),$(f)))\
   )\
)


# ------------------------------------------------------------------------------
#  LINKING RULES
# ------------------------------------------------------------------------------
# This defines some pretty-print statement for non-verbose output.
# It will use prog_color_beg and prog_color_end as defined along with
# cmd_PRECOMP_percent.
# The tokens, which both must be supplied in the linking recipe, are:
#   %3d number of files to link
#   %s  the information string ("Building ...")
ifeq "$(strip $(progress_display))" "true"
   cmd_PRELINK_print = $(PRINTF) "$(prog_color_beg)[Linking %3d files]$(prog_color_end) %s\n"
else
   cmd_PRELINK_print = $(PRINTF) "%s\n"
endif

# Template for linking object files to executables.
#  $(addprefix...  Preprends <build_type>/$(dir_BIN)/ to all the main files.
#  $(addsuffix...  Appends .$(ext_EXE) to all main files.
#  : ...           Each main file depends on its corresponding object files as
#                  well as every other object file generated from a source
#                  file, EXCEPT other object files containing the main()
#                  function.
#  | ...           Order-only dependency on the $(dir_BIN) directory.
#  $$              Second expansion, $$ --> $ when using 'eval'.
#  $$$$            Same, but twice.
#  $^              All prerequisites (are linked).
#  $@              Target name.
#  $(@F)           Target name, excluding directory part.
#  $(patsubst...)  Gives name of object file corresponding to the target
#                  executable.
#  $(detail_verbose_make)
#                  Should be either the empty string or the @ character, thus
#                  giving verbose/non-verbose output, respectively.
#  $$(words $$^)   This simply counts the number of words (i.e. object files)
#                  in the $$^ variable.
#
# Arguments:
#    $(1): build type
#    $(2): executable (no extension)
define tmpl_LINK
$(BUILDDIR)/$(1)/$(dir_BIN)/$(2).$(ext_EXE) \
            :  $$(addprefix $(BUILDDIR)/$(1)/$(dir_OBJ)/, $$(lst_DEP_$(2))) \
               $$(lst_SHIPLIBS_$(2)) \
               $(lst_EXTLIBS_prereq) \
            |  $(BUILDDIR)/$(1)/$(dir_BIN)
	@$(cmd_PRELINK_print) $$(words $$(filter-out $(dir_EXTLIBS)/%,$$^)) "Building <$(1)>/$(dir_BIN)/$(2).$(ext_EXE)"
	$(detail_verbose_make)$(CXX_$(1)) $(CXXFLAGS_$(1)) $$(filter-out $(dir_EXTLIBS)/%,$$^) $(LDFLAGS_$(1)) $(lst_LIB_LIST_EXTLIBS_$(1)_$(2)) -o $$@
endef

define tmpl_LINK_DYNLIB
$(BUILDDIR)/$(1)/$(dir_BIN)/$(2).$(ext_DYNLIB) \
            :  $$(addprefix $(BUILDDIR)/$(1)/$(dir_OBJ)/, $$(lst_DEP_$(2))) \
               $$(lst_SHIPLIBS_$(2)) \
               $(lst_EXTLIBS_prereq) \
            |  $(BUILDDIR)/$(1)/$(dir_BIN)
	@$(cmd_PRELINK_print) $$(words $$(filter-out $(dir_EXTLIBS)/%,$$^)) "Building <$(1)>/$(dir_BIN)/$(2).$(ext_DYNLIB)"
	$(detail_verbose_make)$(CXX_$(1)) $(CXXFLAGS_$(1)) $$(filter-out $(dir_EXTLIBS)/%,$$^) $(LDFLAGS_$(1)) $(lst_LIB_LIST_EXTLIBS_$(1)_$(2)) -shared -Wl,-$(DYNLIB_NAME),lib$(2).$(ext_DYNLIB) -o $$@
endef

# Evaluate the templates.
$(foreach b, $(lst_BUILDS),\
   $(foreach m, $(lst_MAINS) $(lst_TOOLS) $(lst_POT),\
      $(eval $(call tmpl_LINK,$(b),$(m)))\
   )\
   $(foreach m, $(lst_DYNLIB),\
      $(eval $(call tmpl_LINK_DYNLIB,$(b),$(m)))\
   )\
)
