/*
************************************************************************
*
* @file                 mctdh_tensor_utils.h
*
* Created:              22-01-2019
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Functions for performing MCTDH-relevant math 
*                       operators on tensors.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDH_TENSOR_UTILS_H_INCLUDED
#define MCTDH_TENSOR_UTILS_H_INCLUDED

#include "util/CallStatisticsHandler.h"
#include "tensor/NiceTensor.h"
#include "vcc/TensorDataCont.h"
#include "vcc/VccStateSpace.h"
#include "input/Input.h"

namespace midas::mctdh::detail
{

/**
 * Do forward contraction with MidasMatrix
 *
 * @param arTensor      Tensor to modify by performin forward (one-index) contraction
 * @param arMatrix      The matrix as row-major pointer
 * @param aMode         Mode to transform
 * @param aNRow         Number of rows in arMatrix (-1 means we assume a square matrix)
 **/
template
   <  typename T
   >
void DoForwardContraction
   (  NiceTensor<T>& arTensor
   ,  std::unique_ptr<T[]>& arMatrix
   ,  In aMode
   ,  In aNRow = -I_1
   )
{
   LOGCALL("forward contraction");

   unsigned ncol = arTensor.Extent(aMode);

   unsigned nrow = (aNRow == -I_1) ? ncol : aNRow;

   // Make SimpleTensor
   auto integrals_tensor = NiceSimpleTensor<T>(std::vector<unsigned>{nrow, ncol}, arMatrix.release());
   arMatrix = nullptr;

   // Do contraction
   arTensor = arTensor.ContractForward(aMode, integrals_tensor);

   // Move data ownership back
   arMatrix = std::unique_ptr<T[]>(static_cast<SimpleTensor<T>*>(integrals_tensor.GetTensor())->ReleaseData());
}

/**
 * Do forward contraction with MidasMatrix
 *
 * @param arTensor      Tensor to modify by performin forward (one-index) contraction
 * @param aMatrix       The matrix
 * @param aIdx          Index of the tensor to transform
 * @param aSkipFirst    Skip first row/column in aMatrix
 **/
template
   <  typename T
   >
void DoForwardContraction
   (  NiceTensor<T>& arTensor
   ,  const GeneralMidasMatrix<T>& aMatrix
   ,  In aIdx
   ,  bool aSkipFirst = false
   )
{
   unsigned nrows = aMatrix.Nrows();
   unsigned ncols = aMatrix.Ncols();
   const In start = aSkipFirst ? I_1 : I_0;

   assert(nrows == ncols);
   assert(ncols == (aSkipFirst ? arTensor.Extent(aIdx) + I_1 : arTensor.Extent(aIdx)));

   // Put data into row-major ptr
   size_t size = aSkipFirst ? (nrows-1)*(ncols-1) : nrows*ncols;
   auto integrals_ptr = std::make_unique<T[]>(size);
   {
      auto* ptr = integrals_ptr.get();
      for(In i=start; i<nrows; ++i)
      {
         for(In j=start; j<ncols; ++j)
         {
            *(ptr++) = aMatrix[i][j];
         }
      }
   }

   auto n_row_trans = aSkipFirst ? nrows - I_1 : nrows;

   DoForwardContraction(arTensor, integrals_ptr, aIdx, n_row_trans);
}

/**
 * Contract all indices of two tensors but the one corresponding to a given mode.
 *
 * @param aLeft         The first tensor (the one that is conjugated!)
 * @param aRight        The second tensor
 * @param aMode         The mode to keep
 * @return
 *    Result as 2D SimpleTensor (row-major matrix)
 **/
template
   <  typename T
   >
NiceTensor<T> ContractAllButOne
   (  const NiceTensor<T>& aLeft
   ,  const NiceTensor<T>& aRight
   ,  In aMode
   )
{
   LOGCALL("contract all but one - tensor");

   // Sanity check
   assert_same_shape(aLeft, aRight);
   auto nmodes = aLeft.NDim();
   assert(aMode < nmodes);

   if (  aLeft.Type() != BaseTensor<T>::typeID::SIMPLE
      || aRight.Type() != BaseTensor<T>::typeID::SIMPLE
      )
   {
      MIDASERROR("ContractAllButOne only works for SimpleTensor!");
   }
   const auto& newdim = aLeft.GetDims()[aMode];

   // Init result
   auto result = NiceSimpleTensor<T>(std::vector<unsigned>{newdim, newdim});

   // Pointers to data
   auto* result_ptr_ref = static_cast<SimpleTensor<T>*>(result.GetTensor())->GetData();
   auto* left_ptr_ref = static_cast<SimpleTensor<T>*>(aLeft.GetTensor())->GetData();
   auto* right_ptr_ref = static_cast<SimpleTensor<T>*>(aRight.GetTensor())->GetData();

   // Aux sizes.
   size_t nbefore = 1;
   for(In i=I_0; i<aMode; ++i)
   {
      nbefore *= aLeft.GetDims()[i];
   }
   size_t nafter = 1;
   for(In i=aMode+I_1; i<nmodes; ++i)
   {
      nafter *= aLeft.GetDims()[i];
   } 
   size_t nafter_n = nafter * newdim;

   // Do loops
   for(In ibefore=I_0; ibefore<nbefore; ++ibefore)       // Indices before aMode
   {
      // Start from beginning of result
      auto* result_ptr = result_ptr_ref;

      auto* left_ptr_tmp = left_ptr_ref;
      for(In irow=I_0; irow<newdim; ++irow)              // Result rows
      {
         auto* right_ptr = right_ptr_ref;
         for(In icol=I_0; icol<newdim; ++icol)           // Result cols
         {
            auto* left_ptr = left_ptr_tmp;
            for(In iafter=I_0; iafter<nafter; ++iafter)  // Indices after aMode
            {
               *result_ptr += midas::math::Conj(*(left_ptr++)) * *(right_ptr++);
            }
            // Increment result ptr
            ++result_ptr;
         }
         left_ptr_tmp += nafter;
      }

      // Increment indices before aMode
      left_ptr_ref += nafter_n;
      right_ptr_ref += nafter_n;
   }

   return result;
}

/**
 * Contract all indices of two tensors but the one corresponding to a given mode.
 * The modes are different in the two tensors.
 *
 * @param aLeft         The first tensor (the one that is conjugated!)
 * @param aRight        The second tensor
 * @param aModeLeft     The mode to keep in aLeft
 * @param aModeRight    The mode to keep in aRight
 * @param aForceGeneral Force the use of the general contraction framework
 *
 * @return
 *    Result as 2D SimpleTensor (row-major matrix)
 **/
template
   <  typename T
   >
NiceTensor<T> ContractAllButOne
   (  const NiceTensor<T>& aLeft
   ,  const NiceTensor<T>& aRight
   ,  In aModeLeft
   ,  In aModeRight
   ,  bool aForceGeneral = false
   )
{
   LOGCALL("contract all but one - two diff indeces");

   // Sanity check
   auto ndim = aLeft.NDim();
   assert(ndim == aRight.NDim());

   // Call fast impl if mode indices are the same and tensors have same shape
   if (  !aForceGeneral
      && ndim != I_2
      && aModeLeft == aModeRight
      && have_same_shape(*aLeft.GetTensor(), *aRight.GetTensor())
      )
   {
      return ContractAllButOne(aLeft, aRight, aModeLeft);
   }

   // Special case of dim=2
   if (  !aForceGeneral
      && ndim == I_2
      )
   {
      const auto& ldims = aLeft.GetDims();
      const auto& rdims = aRight.GetDims();

      auto* ldata = static_cast<const SimpleTensor<T>* const>(aLeft.GetTensor())->GetData();
      auto* rdata = static_cast<const SimpleTensor<T>* const>(aRight.GetTensor())->GetData();

      std::unique_ptr<T[]> conj_ldata = nullptr;
      if (  aModeLeft == I_0
         )
      {
         auto lsize = ldims[0]*ldims[1];
         conj_ldata = std::make_unique<T[]>(lsize);
         for(In i=I_0; i<lsize; ++i)
         {
            conj_ldata[i] = midas::math::Conj(ldata[i]);
         }
      }

      if (  aModeLeft == I_0
         && aModeRight == I_1
         )
      {
         // We have row-major matrices, i.e. transposed. Calculate as [c^T]_ji = c_ij = l^*_ik r_kj = [r^T]_jk [l^T]^*_ki
         // We need to conjugate the left elements before calling gemm
         auto result = std::make_unique<T[]>(ldims[0]*rdims[1]);
         char transa = 'N';
         char transb = 'N';
         int m = rdims[1];
         int n = ldims[0];
         int k = ldims[1];
         assert(k == rdims[0]);
         T alpha(1.0);
         int lda = std::max(1,m);
         int ldb = std::max(1,k);
         T beta(0.0);
         int ldc = std::max(1,m);

         midas::lapack_interface::gemm
            (  &transa, &transb
            ,  &m, &n, &k, &alpha
            ,  rdata, &lda, conj_ldata.get(), &ldb
            ,  &beta, result.get(), &ldc
            );

         return NiceSimpleTensor<T>(std::vector<unsigned>{ldims[0], rdims[1]}, result.release());
      }
      else if  (  aModeLeft == I_1
               && aModeRight == I_0
            )
      {
         auto result = std::make_unique<T[]>(ldims[1]*rdims[0]);

         // We have row-major matrices, i.e. transposed. Calculate as [c^T]_ji = c_ij = l^*_ki r_jk = [r^T]^T_jk [l^T]^H_ki
         // We need to conjugate the left elements before calling gemm
         char transa = 'T';
         char transb = 'C';
         int m = rdims[0];
         int n = ldims[1];
         int k = ldims[0];
         assert(k == rdims[1]);
         T alpha(1.0);
         int lda = std::max(1,k);
         int ldb = std::max(1,n);
         T beta(0.0);
         int ldc = std::max(1,m);

         midas::lapack_interface::gemm
            (  &transa, &transb
            ,  &m, &n, &k, &alpha
            ,  rdata, &lda, ldata, &ldb
            ,  &beta, result.get(), &ldc
            );

         return NiceSimpleTensor<T>(std::vector<unsigned>{ldims[1], rdims[0]}, result.release());
      }
      else if  (  aModeLeft == I_0
               && aModeRight == I_0
            )
      {
         auto result = std::make_unique<T[]>(ldims[0]*rdims[0]);

         // We have row-major matrices, i.e. transposed. Calculate as [c^T]_ji = c_ij = l^*_ik r_jk = [r^T]^T_jk [l^T]^*_ki
         char transa = 'T';
         char transb = 'N';
         int m = rdims[0];
         int n = ldims[0];
         int k = ldims[1];
         assert(k == rdims[1]);
         T alpha(1.0);
         int lda = std::max(1,k);
         int ldb = std::max(1,k);
         T beta(0.0);
         int ldc = std::max(1,m);

         midas::lapack_interface::gemm
            (  &transa, &transb
            ,  &m, &n, &k, &alpha
            ,  rdata, &lda, conj_ldata.get(), &ldb
            ,  &beta, result.get(), &ldc
            );

         return NiceSimpleTensor<T>(std::vector<unsigned>{ldims[0], rdims[0]}, result.release());
      }
      else if  (  aModeLeft == I_1
               && aModeRight == I_1
            )
      {
         auto result = std::make_unique<T[]>(ldims[1]*rdims[1]);

         // We have row-major matrices, i.e. transposed. Calculate as [c^T]_ji = c_ij = l^*_ki r_kj = [r^T]_jk [l^T]^H_ki
         // We need to conjugate the left elements before calling gemm
         char transa = 'N';
         char transb = 'C';
         int m = rdims[1];
         int n = ldims[1];
         int k = ldims[0];
         assert(k == rdims[0]);
         T alpha(1.0);
         int lda = std::max(1,m);
         int ldb = std::max(1,n);
         T beta(0.0);
         int ldc = std::max(1,m);

         midas::lapack_interface::gemm
            (  &transa, &transb
            ,  &m, &n, &k, &alpha
            ,  rdata, &lda, ldata, &ldb
            ,  &beta, result.get(), &ldc
            );

         return NiceSimpleTensor<T>(std::vector<unsigned>{ldims[1], rdims[1]}, result.release());
      }
      else
      {
         MIDASERROR("Mode indices are wrong for ContractAllButOne!");
         return NiceTensor<T>();
      }
   }
   // Dummy implementation:
   // Note for future impl: Similar to ContractAllInRight two times
   // i.e. first contract all before first kept index and then contract the rest
   else
   {
      MidasWarning("No efficient implementation of ContractAllButOne for two different indices (except for matrices)!");

      std::vector<unsigned> indices_l(aLeft.NDim());
      auto indices_r = indices_l;
      unsigned ileft = 0;
      unsigned iright = 0;
      for(In i=I_0; i<aLeft.NDim(); ++i)
      {
         if (  i == aModeLeft
            )
         {
            indices_l[i] = aLeft.NDim();
         }
         else
         {
            indices_l[i] = ileft++;
         }

         if (  i == aModeRight
            )
         {
            indices_r[i] = aLeft.NDim() + I_1;
         }
         else
         {
            indices_r[i] = iright++;
         }
      }

      if (  gMcTdHIoLevel >= I_10
         || gDebug
         )
      {
         Mout  << " ContractAllButOne with two different indices.\n"
               << "    aModeLeft  = " << aModeLeft << "\n"
               << "    aModeRight = " << aModeRight << "\n"
               << "    indices_l  = " << indices_l << "\n"
               << "    indices_r  = " << indices_r << "\n"
               << std::flush;
      }

      return contract(aLeft, indices_l, aRight, indices_r, true, false);
   }
}

/**
 * Contract all indices of two tensors but the one corresponding to a given mode.
 * Only left tensor contains the mode, so all indices of right tensor are contracted away.
 *
 * @param aLeft         The first tensor (the one that is conjugated!)
 * @param aRight        The second tensor
 * @param aMode         The mode to keep
 * @return
 *    Result as 1D SimpleTensor (vector)
 **/
template
   <  typename T
   >
NiceTensor<T> ContractAllInRight
   (  const NiceTensor<T>& aLeft
   ,  const NiceTensor<T>& aRight
   ,  In aMode
   )
{
   LOGCALL("contract all in right");

   // Sanity check
   auto nmodes_left = aLeft.NDim();
   auto nmodes_right = aRight.NDim();
   if (  nmodes_left != nmodes_right + 1
      )
   {
      MIDASERROR("ContractAllInRight: Left tensor must be of 1 higher dimensionality than right");
   }
   assert(aMode < nmodes_left);

   // If Vector times scalar
   if (  aRight.Type() == BaseTensor<T>::typeID::SCALAR
      )
   {
      assert(aLeft.Type() == BaseTensor<T>::typeID::SIMPLE);

      auto result = aLeft;
      result.Conjugate();
      result.Scale(aRight.GetScalar());

      return result;
   }

   {
      In j=I_0;
      for(In i=I_0; i<nmodes_left; ++i)
      {
         if (  i != aMode
            )
         {
            assert(aLeft.GetDims()[i] == aRight.GetDims()[j]);
            ++j;
         }
      }
   }

   if (  aLeft.Type() != BaseTensor<T>::typeID::SIMPLE
      || aRight.Type() != BaseTensor<T>::typeID::SIMPLE
      )
   {
      Mout  << " aLeft type = " << aLeft.ShowType() << ", aRight type = " << aRight.ShowType() << std::endl;
      MIDASERROR("ContractAllInRight only works for SimpleTensor (and Scalar)!");
   }
   const auto& newdim = aLeft.GetDims()[aMode];

   // Init result
   auto result = NiceSimpleTensor<T>(std::vector<unsigned>{newdim});

   // Pointers to data
   auto* result_ptr_ref = static_cast<SimpleTensor<T>*>(result.GetTensor())->GetData();
   auto* left_ptr_ref = static_cast<SimpleTensor<T>*>(aLeft.GetTensor())->GetData();
   auto* right_ptr_ref = static_cast<SimpleTensor<T>*>(aRight.GetTensor())->GetData();

   // Aux sizes.
   size_t nbefore = 1;
   for(In i=I_0; i<aMode; ++i)
   {
      nbefore *= aLeft.GetDims()[i];
   }
   size_t nafter = 1;
   for(In i=aMode+I_1; i<nmodes_left; ++i)
   {
      nafter *= aLeft.GetDims()[i];
   } 
   size_t nafter_n = nafter * newdim;

   // Do loops
   for(In ibefore=I_0; ibefore<nbefore; ++ibefore)       // Indices before aMode
   {
      // Start from beginning of result
      auto* result_ptr = result_ptr_ref;

      auto* left_ptr_tmp = left_ptr_ref;
      for(In irow=I_0; irow<newdim; ++irow)              // Result rows
      {
         auto* right_ptr = right_ptr_ref;
         auto* left_ptr = left_ptr_tmp;
         for(In iafter=I_0; iafter<nafter; ++iafter)     // Indices after aMode
         {
            *result_ptr += midas::math::Conj(*(left_ptr++)) * *(right_ptr++);
         }

         // Increment result ptr
         ++result_ptr;

         left_ptr_tmp += nafter;
      }

      // Increment indices before aMode.
      // right_ptr_ref is incremented less than left_ptr_ref because of the missing index.
      left_ptr_ref += nafter_n;
      right_ptr_ref += nafter;
   }

   return result;
}

/**
 * Contract all indices of two tensors but the one corresponding to a given mode.
 *
 * @param aLeft         The first TensorDataCont (the one that is conjugated!)
 * @param aRight        The second TensorDataCont
 * @param aMode         The mode to keep
 * @param aVssI         The state space of the TensorDataCont%s
 * @return
 *    Result as 2D SimpleTensor (row-major matrix)
 **/
template
   <  typename T
   >
NiceTensor<T> ContractAllButOne
   (  const GeneralTensorDataCont<T>& aLeft
   ,  const GeneralTensorDataCont<T>& aRight
   ,  const VccStateSpace& aVssI
   ,  In aMode
   )
{
   LOGCALL("contract all but one - tdc");

   // Get number of active + occupied modals
   auto nact = aVssI.Dims()[aMode];
   auto ntd = nact + I_1;

   auto result = NiceSimpleTensor<T>(std::vector<unsigned>{ntd, ntd});
   auto* const result_ptr = static_cast<SimpleTensor<T>*>(result.GetTensor())->GetData();

   const auto& mcr = aVssI.MCR();

//   Mout  << " ContractAllButOne (TensorDataCont):\n"
//         << "    nact = " << nact << "\n"
//         << "    ntd  = " << ntd << "\n"
//         << "    result (before calc):\n" << result << "\n"
//         << std::flush;

   for(In i_mc=I_0; i_mc<mcr.Size(); ++i_mc)
   {
      const auto& mc = mcr.GetModeCombi(i_mc);

      const auto& c_left = aLeft.GetModeCombiData(i_mc);
      
      // Reference. We add coef squared to occ-occ element and continue
      if (  mc.Empty()
         )
      {
         const auto& c_right = aRight.GetModeCombiData(i_mc);
         result_ptr[0] += (midas::math::Conj(c_left.GetScalar()) * c_right.GetScalar());
         continue;
      }

      // If the MC includes aMode, we add density matrices to act-act and off-diagonal blocks
      if (  mc.IncludeMode(aMode)
         )
      {
         const auto& c_right = aRight.GetModeCombiData(i_mc);

         // Get index of aMode in current tensor
         auto mode_idx = mc.IdxNrForMode(aMode);

         // Calculate act-act contribution
         {
            auto result_vw = detail::ContractAllButOne(c_left, c_right, mode_idx);

//            Mout  << "    act-act contrib:\n" << result_vw << "\n"
//                  << std::flush;

            // Load row-major pointer into matrix
            const auto* ptr = result_vw.template StaticCast<SimpleTensor<T> >().GetData();
            for(In v=I_1; v<ntd; ++v)
            {
               for(In w=I_1; w<ntd; ++w)
               {
                  result_ptr[v*ntd + w] += *(ptr++);
               }
            }
         }

         // Calculate act-occ and occ-act contributions
         {
            // Construct MC with aMode removed
            auto mc_new = mc;
            mc_new.RemoveMode(aMode);

            // Find the corresponding tensor
            ModeCombiOpRange::const_iterator it_mc;
            In idx;
            if (  !mcr.Find(mc_new, it_mc, idx)
               )
            {
               Mout  << " Could not find MC:\n" << mc_new << std::endl;
               MIDASERROR("ContractAllButOne: Could not find MC in MCR!");
            }
            const auto& c_right = aRight.GetModeCombiData(idx);

            // Contract all indices of right tensor. Keep mode_idx in left.
            auto result_vi = detail::ContractAllInRight(c_left, c_right, mode_idx);

//            Mout  << "    act-occ contrib:\n" << result_vi << std::endl;

            // Load data
            const auto* ptr_vi = result_vi.template StaticCast<SimpleTensor<T> >().GetData();
            for(In v=I_1; v<ntd; ++v)
            {
               result_ptr[v*ntd] += *(ptr_vi++);
            }

            auto result_iv_conj = detail::ContractAllInRight(aRight.GetModeCombiData(i_mc), aLeft.GetModeCombiData(idx), mode_idx);

//            Mout  << "    occ-act contrib (conj):\n" << result_iv_conj << std::endl;

            const auto* ptr_iv_conj = result_iv_conj.template StaticCast<SimpleTensor<T> >().GetData();
            for(In v=I_1; v<ntd; ++v)
            {
               result_ptr[v] += midas::math::Conj(*(ptr_iv_conj++));
            }
         }
      }
      // Else, the occ-occ element gets a contribution
      else
      {
         const auto& c_right = aRight.GetModeCombiData(i_mc);
         result_ptr[0] += dot_product(c_left, c_right);
      }
   }

//   // DEBUG
//   Mout  << " Result of TDC ContractAllButOne:\n" << result << std::endl;

   // Return result
   return result;
}


/**
 * Non-conjugated "dot" product of tensors
 *
 * @param aLeft
 * @param aRight
 * @return
 *    Sum of product of elements
 **/
template
   <  typename T
   >
T NonConjugateDot
   (  const NiceTensor<T>& aLeft
   ,  const NiceTensor<T>& aRight
   )
{
   if (  aLeft.Type() == BaseTensor<T>::typeID::SCALAR
      && aRight.Type() == BaseTensor<T>::typeID::SCALAR
      )
   {
      return aLeft.GetScalar() * aRight.GetScalar();
   }
   else
   {
      // Sanity checks...
      assert_same_shape(aLeft, aRight);

      bool left_not_simple    = (aLeft.Type()   != BaseTensor<T>::typeID::SIMPLE);
      bool right_not_simple   = (aRight.Type()  != BaseTensor<T>::typeID::SIMPLE);
      if (  left_not_simple
         || right_not_simple
         )
      {
         std::string err = left_not_simple ? "Left" : "Right";
         if (  left_not_simple
            && right_not_simple
            )
         {
            err += " and right";
         }
         MIDASERROR(err + " tensor are not SimpleTensor instances in NonConjugateDot!");
      }

      T result(0.);
      auto size = aLeft.TotalSize();

      auto* left_ptr = aLeft.template StaticCast<SimpleTensor<T> >().GetData();
      auto* right_ptr = aRight.template StaticCast<SimpleTensor<T> >().GetData();

      for(In i=I_0; i<size; ++i)
      {
         result += *(left_ptr++) * *(right_ptr++);
      }

      return result;
   }
}

/**
 * Non-conjugated "dot" product of tensors
 *
 * @param aLeft
 * @param aRight
 * @return
 *    Sum of product of elements
 **/
template
   <  typename T
   >
T NonConjugateDot
   (  const GeneralTensorDataCont<T>& aLeft
   ,  const GeneralTensorDataCont<T>& aRight
   )
{
   T result(0.);

   MidasAssert(aLeft.Size() == aRight.Size(), "TensorDataConts have different sizes!");
   auto size = aLeft.Size();

   for(In i=I_0; i<size; ++i)
   {
      result += NonConjugateDot(aLeft.GetModeCombiData(i), aRight.GetModeCombiData(i));
   }

   return result;
}

} /* namespace midas::mctdh::detail */

#endif /* MCTDH_TENSOR_UTILS_H_INCLUDED */
