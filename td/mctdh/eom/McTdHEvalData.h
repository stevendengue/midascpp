/*
************************************************************************
*
* @file                 McTdHEvalData.h
*
* Created:              01-04-2019
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Class that holds data for using the V3Contrib 
*                       framework for MCTDH
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHEVALDATA_H_INCLUDED
#define MCTDHEVALDATA_H_INCLUDED

#include <type_traits>

#include "vcc/VccStateSpace.h"
#include "vcc_cpg/BasicOper.h"
#include "td/mctdh/container/McTdHVectorContainer.h"
#include "vcc/v3/IntermediateMachine.h"

namespace midas::mctdh
{

namespace detail
{

/**
 * Class that contains the data necessary to perform a VCI-like transformation using the V3Contrib framework.
 *
 * Note that the INTEGRALS template class must implement the necessary contractions for using the V3 framework.
 **/
template
   <  typename T
   ,  template<typename> typename VEC_T
   ,  typename INTEGRALS
   ,  typename INTERMEDS
   >
class McTdHEvalData
{
   public:
      //! Alias
      using param_t = T;
      using step_t = midas::type_traits::RealTypeT<param_t>;
      using vec_t = VEC_T<param_t>;
      using coefs_t = typename vec_t::coefs_t;
      using modal_t = typename vec_t::modal_t;
      using integrals_t = INTEGRALS;
      using intermeds_t = INTERMEDS;

      using allow_midasvector_t = std::false_type;
      using allow_nicetensor_t = std::true_type;

   private:
      //! Pointer to vector
      const vec_t* const mpVector = nullptr;

      //! Pointer to integrals
      const integrals_t* const mpIntegrals = nullptr;

      //! Reference to VccStateSpace
      const VccStateSpace& mrStateSpace;

      //! Reference to intermediates
      intermeds_t& mrIntermeds;

      //! Time (for using time-dependent operators)
      step_t mTime;

   public:
      //! c-tor
      McTdHEvalData
         (  const vec_t* const apVector
         ,  const integrals_t* const apIntegrals
         ,  const VccStateSpace& arStateSpace
         ,  intermeds_t& arIntermeds
         ,  step_t aTime
         )
         :  mpVector
               (  apVector
               )
         ,  mpIntegrals
               (  apIntegrals
               )
         ,  mrStateSpace
               (  arStateSpace
               )
         ,  mrIntermeds
               (  arIntermeds
               )
         ,  mTime
               (  aTime
               )
      {
      }

      /** @name Methods needed by the V3Contrib framework **/
      //!@{

      /**
       * @return
       *    Integrals (ready for performing contractions)
       **/
      const integrals_t* const GetIntegrals
         (
         )  const
      {
         return this->mpIntegrals;
      }

      /**
       * Get intermediates (must be template for using with V3)
       *
       * @return
       *    Reference to intermediates
       **/
      template
         <  template<typename> typename VECTOR
         ,  std::enable_if_t<std::is_same_v<VECTOR<param_t>, NiceTensor<param_t> > >* = nullptr
         >
      intermeds_t& GetIntermediates
         (
         )  const
      {
         return mrIntermeds;
      }

      /**
       * @return
       *    Reference to MCR included in the wave function
       **/
      const ModeCombiOpRange& GetModeCombiOpRange
         (
         )  const
      {
         return mrStateSpace.MCR();
      }

      /**
       * Get dimensions of tensor for mode combi
       *
       * @param aMcIdx
       * @return
       *    Dimensions of amplitude tensor
       **/
      const std::vector<unsigned>& GetTensorDims
         (  In aMcIdx
         )  const
      {
         return this->mpVector->Coefs().GetModeCombiData(aMcIdx).GetDims();
      }

      /**
       * Get total number of excitations in the vector
       *
       * @return
       *    Size of Xvec
       **/
      In NExci
         (
         )  const
      {
         return this->mpVector->Coefs().TotalSize();
      }

      /**
       * Get number of excitation amplitudes for a given MC from mpXvec
       *
       * @param aMcIdx    Index of ModeCombi
       * @return
       *    Number of WF params for ModeCombi
       **/
      In NExciForModeCombi
         (  In aMcIdx
         )  const
      {
         return this->mpVector->Coefs().GetModeCombiData(aMcIdx).TotalSize();
      }

      /**
       * Get number of time-dependent modals for mode (active + occupied)
       *
       * @param aMode
       * @return
       *    Number of modals
       **/
      In NModals
         (  LocalModeNr aMode
         )  const
      {
         return this->mrStateSpace.Dims()[aMode] + I_1;
      }

      /**
       * Get number of modals as vector
       *
       * @return
       *    N vector
       **/
      auto GetNModals
         (
         )  const
      {
         auto result = this->mrStateSpace.Dims();
         for(auto& d : result)
         {
            ++d;
         }
         return result;
      }

      /**
       * Get number of modes
       *
       * @return
       *    M
       **/
      In NModes
         (
         )  const
      {
         return this->mrStateSpace.Dims().size();
      }

      /**
       * Get MC level of the operator.
       * @return
       *    MC level
       **/
      In McLevel
         (
         )  const
      {
         const auto& opdef = this->mpIntegrals->Oper().Oper();
         return opdef.McLevel();
      }

      /**
       * Operator coefficient
       *
       * @return
       *    c_t
       **/
      param_t OperatorCoef
         (  In aTerm
         )  const
      {
         const auto& oper = this->mpIntegrals->Oper();
         return oper.Coef(this->mTime) * oper.Oper().Coef(aTerm);
      }

      /**
       * Terms for MC
       *
       * @param[in]  aMc            MC to find in MCR for operator
       * @param[out] arFirstTerm    First term for aMc
       * @param[out] arNterms       Number of terms for aMc
       * @return
       *    aMc found?
       **/
      bool TermsForMc
         (  const ModeCombi& aMc
         ,  In& arFirstTerm
         ,  In& arNterms
         )  const
      {
         const auto& opdef = this->mpIntegrals->Oper().Oper();
         bool result = opdef.TermsForMc(aMc, arFirstTerm, arNterms);

         return result;
      }

      /**
       * Get operators for term
       *
       * @param aTerm
       * @return
       *    Vector of operator numbers in term
       **/
      const std::vector<LocalOperNr>& GetOpers
         (  In aTerm
         )  const
      {
         const auto& opdef = this->mpIntegrals->Oper().Oper();
         return opdef.GetOpers(aTerm);
      }


      /**
       * Get excitation vector. Interface.
       *
       * @param aType      Type of vector
       * @return
       *    Pointer to correct vector
       **/
      template
         <  typename V
         >
      const V* const GetExciVec
         (  const OP aType
         )  const
      {
         // Check that we only ask for amplitudes/wf coefs
         if (  aType != OP::T
            )
         {
            MIDASERROR("McTdHEvalData only contains OP::T (amplitudes/wf coefs).");
         }

         // Check that we ask for TensorDataCont
         if constexpr   (  std::is_same_v<V, coefs_t>
                        )
         {
            return &this->mpVector->Coefs();
         }
         else
         {
            MIDASERROR("McTdHEvalData only contains ComplexTensorDataCont coefficients.");
            return nullptr;
         }
      }

      /**
       * Is TransformerV3 (and thereby the IntermediateMachine) using NiceTensor?
       *
       * @return
       *    Always true for this data class
       **/
      constexpr bool TensorTransform
         (
         )  const
      {
         return true;
      }

      //!@}
};

} /* namespace detail */

}  /* namespace midas::mctdh */



namespace midas::mctdh
{

//! Forward decl.
class LinearRasTdHIntegrals;
class LinearRasTdHIntermediates;

//! Alias
template
   <  typename T
   >
using LinearRasTdHEvalData = detail::McTdHEvalData<T, RasTdHVectorContainer, LinearRasTdHIntegrals, LinearRasTdHIntermediates>;

}  /* namespace midas::mctdh */

#endif /* MCTDHEVALDATA_H_INCLUDED */
