/*
************************************************************************
*
* @file                 McTdHIntegrals.h
*
* Created:              16-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Time-dependent integrals used for MCTDH
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHINTEGRALS_H_INCLUDED
#define MCTDHINTEGRALS_H_INCLUDED

#include "td/PrimitiveIntegrals.h"
#include "td/oper/TdOperTerm.h"
#include "vcc/IntegralContractor.h"

namespace midas::mctdh
{

namespace detail
{

/**
 * Base class for time-dependent integrals
 *
 * Not abstract because the Update functions of derived classes could take different arguments
 **/
class McTdHIntegralsBase
   :  public midas::td::PrimitiveIntegrals<std::complex<Nb> >
{
   public:
      //! Alias
      using Prim = midas::td::PrimitiveIntegrals<std::complex<Nb> >;
      using param_t = typename Prim::param_t;
      using step_t = typename Prim::step_t;
      template<typename U> using modal_templ_t = typename Prim::template vec_templ_t<U>;
      using modal_t = typename Prim::vec_t;
      template<typename U> using mat_templ_t = typename Prim::template mat_templ_t<U>;
      using mat_t = typename Prim::mat_t;
      using offset_t = std::vector<std::vector<In> >;
      using oper_t = midas::td::TdOperTerm<step_t, param_t, OpDef>;

   protected:
      /** @name Time-dependent integrals **/
      //!@{
      //! Active-active block of the integrals in time-dependent basis. Accessed with [LocalModeNr][LocalOperNr].
      std::vector<std::vector<mat_t>> mActiveIntegrals;
      //!@}
      
      //! Operator
      oper_t mOper;

      //! Pointer to basdef
      const BasDef* const mpBasDef = nullptr;

      //! Assume fixed electronic DOF modals
      bool mAssumeFixedElectronicDofModals = true;

   public:
      //! Delete default c-tor (empty integrals)
      McTdHIntegralsBase
         (
         )  = delete;

      //! Virtual d-tor
      virtual ~McTdHIntegralsBase
         (
         )  = default;

      //! Allow move c-tor
      McTdHIntegralsBase
         (  McTdHIntegralsBase&&
         )  = default;

      //! Dis-allow copy c-tor
      McTdHIntegralsBase
         (  const McTdHIntegralsBase&
         )  = delete;

      //! Allow move assignment
      McTdHIntegralsBase& operator=
         (  McTdHIntegralsBase&&
         )  = default;

      //! Dis-allow copy assignment
      McTdHIntegralsBase& operator=
         (  const McTdHIntegralsBase&
         )  = delete;

      //! Constructor from calcdefs
      McTdHIntegralsBase
         (  const McTdHCalcDef* const apCalcDef
         ,  const oper_t& aOper
         ,  const BasDef* const apBasDef
         );

      //! Get integral
      const mat_t& GetActiveIntegrals
         (  LocalModeNr
         ,  LocalOperNr
         )  const;

      //! Number of active integrals
      In NActive
         (  LocalModeNr aMode
         )  const;

      //! Active-active overlap matrix
      const mat_t& GetActiveOverlaps
         (  LocalModeNr
         )  const;

      //! Get operator
      const oper_t& Oper
         (
         )  const;

      //! Get BasDef
      const BasDef* const GetBasDef
         (
         )  const;
};

} /* namespace detail */


/**
 * Implementation for linear parametrization of modal space
 **/
class LinearMcTdHIntegrals
   :  public detail::McTdHIntegralsBase
{
   public:
      //! Alias
      using Base = detail::McTdHIntegralsBase;
      using param_t = typename Base::param_t;
      using step_t = typename Base::step_t;
      template<typename U> using modal_templ_t = typename Base::template vec_templ_t<U>;
      using modal_t = typename Base::vec_t;
      template<typename U> using mat_templ_t = typename Base::template mat_templ_t<U>;
      using mat_t = typename Base::mat_t;
      using oper_t = typename Base::oper_t;

   private:
      //! Half-transformed integrals
      std::vector<std::vector<mat_t>> mHalfTransformedIntegrals;

   public:
      //! Delete default c-tor
      LinearMcTdHIntegrals
         (
         )  = delete;

      //! Virtual d-tor
      virtual ~LinearMcTdHIntegrals
         (
         )  = default;

      //! Allow move c-tor
      LinearMcTdHIntegrals
         (  LinearMcTdHIntegrals&&
         )  = default;

      //! Dis-allow copy c-tor
      LinearMcTdHIntegrals
         (  const LinearMcTdHIntegrals&
         )  = delete;

      //! Allow move assignment
      LinearMcTdHIntegrals& operator=
         (  LinearMcTdHIntegrals&&
         )  = default;

      //! Dis-allow copy assignment
      LinearMcTdHIntegrals& operator=
         (  const LinearMcTdHIntegrals&
         )  = delete;


      //! Constructor from calcdefs
      LinearMcTdHIntegrals
         (  const McTdHCalcDef* const apCalcDef
         ,  const oper_t& aOper
         ,  const BasDef* const apBasDef
         );

      //! Update integrals
      void Update
         (  const modal_t& aY
         ,  const offset_t& aOffsets
         );

      //! Get half-transformed integrals
      const mat_t& GetHalfTransformedIntegrals
         (  LocalModeNr
         ,  LocalOperNr
         )  const;
};

/**
 * Specialization for MCTDH[n] for using the V3 framework
 **/
class LinearRasTdHIntegrals
   :  public LinearMcTdHIntegrals
{
   public:
      //! Alias
      using Base = LinearMcTdHIntegrals;
      using param_t = typename Base::param_t;
      using step_t = typename Base::step_t;
      template<typename U> using modal_templ_t = typename Base::template modal_templ_t<U>;
      using modal_t = modal_templ_t<param_t>;
      template<typename U> using mat_templ_t = typename Base::template mat_templ_t<U>;
      using mat_t = mat_templ_t<param_t>;
      using offset_t = typename Base::offset_t;
      using oper_t = typename Base::oper_t;
      using contractor_t = midas::vcc::detail::IntegralContractor<param_t, modal_templ_t, mat_templ_t>;

      using ContT = typename contractor_t::ContT;

   private:
      //! Vector of number of active integrals
      std::vector<Uin> mVecNAct;

      //! Integral contractor
      contractor_t mContractor;

   public:
      //! Delete default c-tor
      LinearRasTdHIntegrals
         (
         )  = delete;

      //! Virtual d-tor
      virtual ~LinearRasTdHIntegrals
         (
         )  = default;

      //! Allow move c-tor
      LinearRasTdHIntegrals
         (  LinearRasTdHIntegrals&&
         )  = default;

      //! Dis-allow copy c-tor
      LinearRasTdHIntegrals
         (  const LinearMcTdHIntegrals&
         )  = delete;

      //! Allow move assignment
      LinearRasTdHIntegrals& operator=
         (  LinearRasTdHIntegrals&&
         )  = default;

      //! Dis-allow copy assignment
      LinearRasTdHIntegrals& operator=
         (  const LinearRasTdHIntegrals&
         )  = delete;


      //! c-tor from calcdefs
      LinearRasTdHIntegrals
         (  const McTdHCalcDef* const apCalcDef
         ,  const oper_t& aOper
         ,  const BasDef* const apBasDef
         );

      //! Get occ element (required for IntermedI)
      param_t GetOccElement
         (  LocalModeNr
         ,  LocalOperNr
         )  const;

      //! Number of operators
      In NOper
         (  LocalModeNr aMode
         )  const override;

      /** @name contractions **/
      //!@{

      //! Contract a set of coefficients using the integrals of a specific one-mode operator.
      void Contract
         (  const ModeCombi& arM1
         ,  const ModeCombi& arM0
         ,  const LocalModeNr aOperMode
         ,  const LocalOperNr aOper
         ,  GeneralMidasVector<param_t>& arY1
         ,  const GeneralMidasVector<param_t>& arY0
         ,  In aWhich
         ,  bool aTransp = false
         )  const;

      //! Perform one-mode operator contraction with NiceTensor framework.
      NiceTensor<param_t> ContractDown
         (  const In
         ,  const In
         ,  const In
         ,  const bool
         ,  const NiceTensor<param_t>&
         )  const;

      NiceTensor<param_t> ContractForward
         (  const In
         ,  const In
         ,  const In
         ,  const bool
         ,  const NiceTensor<param_t>&
         )  const;

      NiceTensor<param_t> ContractUp
         (  const In
         ,  const In
         ,  const In
         ,  const bool
         ,  const NiceTensor<param_t>&
         )  const;

      //!@}
};

///**
// * Implementation for exponential parametrization of modal space
// **/
//class ExponentialMcTdHIntegrals
//   :  public detail::McTdHIntegralsBase
//{
//   private:
//      // Here, we perhaps need the active-virtual blocks.
//      // ...
//   
//   public:
//      //! Constructor from calcdefs
//      ExponentialMcTdHIntegrals
//         (  const OpDef* const apOpDef
//         ,  const BasDef* const apBasDef
//         );
//
//      //! The Update method may take more arguments than in the linear case...
//      // ...
//};

} /* namespace midas::mctdh */

#endif /* MCTDHINTEGRALS_H_INCLUDED */
