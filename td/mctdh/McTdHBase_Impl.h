/*
************************************************************************
*
* @file                 McTdHBase_Impl.h
*
* Created:              02-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    McTdHBase implementation.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHBASE_IMPL_H_INCLUDED
#define MCTDHBASE_IMPL_H_INCLUDED

#include "McTdHBase_Decl.h"
#include "input/McTdHCalcDef.h"
#include "util/SanityCheck.h"
#include "ode/ode_exceptions.h"
#include "ode/OdeIntegrator.h"
#include "util/Timer.h"

#include "mpi/Impi.h"

namespace midas::mctdh
{

/**
 * Constructor from CalcDef, oper_t, and BasDef
 *
 * @param apCalcDef
 * @param aOper
 * @param apBasDef
 **/
template
   <  typename EOM
   >
McTdHBase<EOM>::McTdHBase
   (  const McTdHCalcDef* const apCalcDef
   ,  const oper_t& aOper
   ,  const BasDef* const apBasDef
   )
   :  detail::McTdHProperties<EOM>
         (  apCalcDef
         )
   ,  mName
         (  apCalcDef->Name()
         )
   ,  mEom
         (  apCalcDef
         ,  aOper
         ,  apBasDef
         )
{
   // Set up integration scheme from apCalcDef
   static const std::map<std::string, integratorType> integrator_map =
   {  {"VMF", integratorType::VMF}
   ,  {"CMF", integratorType::CMF}
   ,  {"LUBICH", integratorType::LUBICH}
   };

   const auto& scheme = apCalcDef->IntegrationInfo().template At<std::string>("SCHEME");
   mIntegrationScheme = integrator_map.at(scheme);
}

/**
 * Derivative
 *
 * @param aT
 * @param aY
 *
 * @return
 *    Derivative vector
 **/
template
   <  typename EOM
   >
typename McTdHBase<EOM>::vec_t McTdHBase<EOM>::Derivative
   (  step_t aT
   ,  const vec_t& aY
   )
{
   // Call implementation in EOM
   return this->mEom.Derivative(aT, aY);
}

/**
 * Process new vector.
 *
 * @param arY
 * @param aYOld
 * @param arT
 * @param aTOld
 * @param arDyDt
 * @param aDyDtOld
 * @param arStep
 * @param arNDerivFail
 *
 * @return
 *    Step accepted?
 **/
template
   <  typename EOM
   >
bool McTdHBase<EOM>::ProcessNewVector
   (  vec_t& arY
   ,  const vec_t& aYOld
   ,  step_t& arT
   ,  step_t aTOld
   ,  vec_t& arDyDt
   ,  const vec_t& aDyDtOld
   ,  step_t& arStep
   ,  size_t& arNDerivFail
   )  const
{
   return this->mEom.ProcessNewVector(arY, aYOld, arT, aTOld, arDyDt, aDyDtOld, arStep, arNDerivFail);
}

/**
 * Save data for accepted step
 *
 * @param aTime
 * @param aY
 * @return
 *    True if the ODE integrator should stop here.
 **/
template
   <  typename EOM
   >
bool McTdHBase<EOM>::AcceptedStep
   (  step_t aTime
   ,  const vec_t& aY
   )
{
   return this->AcceptedStepImpl(aTime, aY, this->mEom);
}

/**
 * Save data for interpolated point
 *
 * @param aTime
 * @param aY
 **/
template
   <  typename EOM
   >
void McTdHBase<EOM>::InterpolatedPoint
   (  step_t aTime
   ,  const vec_t& aY
   )
{
   this->InterpolatedStepImpl(aTime, aY, this->mEom);
}

/**
 * Return zero vector of correct shape.
 *
 * @return
 *    Zero vector
 **/
template
   <  typename EOM
   >
typename McTdHBase<EOM>::vec_t McTdHBase<EOM>::ShapedZeroVector
   (
   )  const
{
   return this->mEom.ShapedZeroVector();
}

/**
 * Number of modes (from EOM)
 *
 * @return
 *    NModes
 **/
template
   <  typename EOM
   >
Uin McTdHBase<EOM>::NModes
   (
   )  const
{
   return this->mEom.NModes();
}

/**
 * Finalize calculation
 **/
template
   <  typename EOM
   >
void McTdHBase<EOM>::Finalize
   (  const std::vector<step_t>& aTs
   ,  const std::vector<vec_t>& aYs
   )  const
{
#ifdef VAR_MPI
   // Only master node does output
   if (  !midas::mpi::IsMaster()
      )
   {
      return;
   }
#endif

   // Output summary of properties
   Properties::Summary();

   // Calculate spectra
   Properties::CalculateSpectrum();

   // Output wave functions
   this->WaveFunctionOutput(aTs, aYs);
}

/**
 * Wave-function related output
 *
 * @param aTs     Saved time points in OdeIntegrator
 * @param aYs     Saved wave functions in OdeIntegrator
 **/
template
   <  typename EOM
   >
void McTdHBase<EOM>::WaveFunctionOutput
   (  const std::vector<step_t>& aTs
   ,  const std::vector<vec_t>& aYs
   )  const
{
   const auto& wf_out_modes = this->mEom.CalcDef()->WfOutModes();
   const auto& two_mode_dens = this->mEom.CalcDef()->TwoModeDensityPairs();

   if (  !( wf_out_modes.empty()
         && two_mode_dens.empty()
         )
      )
   {
      MidasWarning("MCTDH wave-function output not implemented!");
   }
}


/**
 * Propagate EOMs using variable mean field, i.e. using a standard ODE integrator 
 * on the full set of parameters.
 *
 * @param arY     Initial wave function
 **/
template
   <  typename EOM
   >
void McTdHBase<EOM>::PropagateVmf
   (  vec_t& arY
   )
{
   // Init timer
   Timer timer;

   // Make integrator and evolve
   const auto& odeinfo = this->mEom.IntegrationInfo().template At<midas::ode::OdeInfo>("ODEINFO");
   MidasAssert(!odeinfo.empty(), "Empty OdeInfo in McTdHBase::PropagateVmf.");
   OdeIntegrator<McTdHBase<EOM>> integrator(this, odeinfo);
   integrator.Integrate(arY);

   // Write summary of integration
   integrator.Summary();

   // Output timing
   timer.CpuOut(Mout, "CPU time spent on MCTDH VMF integration:  ");
   timer.WallOut(Mout, "Wall time spent on MCTDH VMF integration: ");

   // Get vectors of time and y
   const auto& t = integrator.GetDatabaseT();
   const auto& y = integrator.GetDatabaseY();

   // Output properties
   this->Finalize(t, y);
}


/**
 * Propagate EOMs using 2nd-order constant mean field (CMF) scheme described in:
 *
 * M.H. Beck and H.-D. Meyer
 *
 * An efficient and robust integration scheme for the equations of motion of the 
 * multiconfiguration time-dependent Hartree (MCTDH) method
 *
 * Z. Phys. D 42, 113-129 (1997)
 *
 *
 * @param arY     Initial wave function
 **/
template
   <  typename EOM
   >
void McTdHBase<EOM>::PropagateCmf
   (  vec_t& arY
   )
{
   MIDASERROR("CMF not implemented!");
}

/**
 * Propagate EOMs using Lubich integrator described in:
 *
 * C. Lubich
 *
 * Time Integration in the Multiconfiguration Time-Dependent Hartree Method 
 * of Molecular Quantum Dynamics
 *
 * Applied Mathematics Research eXpress, Vol. 2015, No. 2, pp. 311-328
 *
 *
 * @param arY     Initial wave function
 **/
template
   <  typename EOM
   >
void McTdHBase<EOM>::PropagateLubich
   (  vec_t& arY
   )
{
   MIDASERROR("Lubich integrator not implemented!");
}


/**
 * Wrapper for propagating EOMs.
 **/
template
   <  typename EOM
   >
void McTdHBase<EOM>::Propagate
   (
   )
{
   // Prepare integration
   auto wf = this->ShapedZeroVector();
   this->mEom.PrepareIntegration(wf); // gets modals for transforming basis

   // Set energy shift for spectrum if changed
   this->SetSpectrumEnergyShift(this->mEom.SpectrumEnergyShift());

   // Transform property integrals to same basis as H integrals
   this->TransformPropertyIntegrals(this->mEom, wf);

   // Call implementation which does integration and call Finalize with suitable arguments
   switch   (  this->mIntegrationScheme
            )
   {
      case integratorType::VMF:
      {
         this->PropagateVmf(wf);
         break;
      }
      case integratorType::CMF:
      {
         this->PropagateCmf(wf);
         break;
      }
      case integratorType::LUBICH:
      {
         this->PropagateLubich(wf);
         break;
      }
      default:
      {
         MIDASERROR("McTdHBase: Unknown integration scheme!");
      }
   }

   // Write final wave function to disc
   wf.WriteToDisc(this->Name());
}

/**
 * @return
 *    Name of calculation
 **/
template
   <  typename EOM
   >
const std::string& McTdHBase<EOM>::Name
   (
   )  const
{
   return this->mName;
}

} /* namespace midas::mctdh */

#endif /* MCTDHBASE_IMPL_H_INCLUDED */
