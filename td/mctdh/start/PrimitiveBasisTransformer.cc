/*
************************************************************************
*
* @file                 PrimitiveBasisTransformer.cc
*
* Created:              03-12-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Class for transforming integrals to an orthonormal basis
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "PrimitiveBasisTransformer.h"
#include "input/BasDef.h"

#include "td/SpectralBasisGeneration.h"

namespace midas::mctdh
{
/**
 * Constructor from BasDef
 *
 * @param apCalcDef
 * @param apBasDef
 * @param apOpDef
 *
 **/
PrimitiveBasisTransformer::PrimitiveBasisTransformer
   (  const McTdHCalcDef* const apCalcDef
   ,  const BasDef* const apBasDef
   ,  const OpDef* const apOpDef
   )
   :  mpBasDef
         (  apBasDef
         )
   ,  mNSpectralModalCoef
         (  I_0
         )
   ,  mNonAdiabatic
         (  gOperatorDefs.ElectronicDofNr() != -I_1
         && apOpDef->ActiveForMode(gOperatorDefs.ElectronicDofNr())
         )
{
   auto ndof = mpBasDef->GetmNoModesInBas();
   auto nmodes = ndof;

   // Modify active space if using single-set formalism for non-adiabatic dynamics
   if (  this->NonAdiabatic()
      )
   {
      --nmodes;

      // Get GlobalModeNr of electronic DOF
      this->mElectronicDofNr = gOperatorDefs.ElectronicDofNr();

      // Set number of electronic states
      this->mNElectronicStates = apOpDef->StateTransferIJMax(apOpDef->GetLocalModeNr(this->mElectronicDofNr)) + I_1;

      // Construct nmodals with all DOF
      this->mNModalsVec.resize(ndof);
      this->mNModalsVec[this->mElectronicDofNr] = this->NElectronicStates();
      In count = 0;
      for(In idof=I_0; idof<ndof; ++idof)
      {
         if (  idof != this->mElectronicDofNr
            )
         {
            this->mNModalsVec[idof] = apCalcDef->ActiveSpace()[count++];
         }
      }
   }
   else
   {
      this->mNModalsVec = apCalcDef->ActiveSpace();
   }

   // Check limits
   const auto& calcdef_limits = apCalcDef->ModalBasisLimits();
   bool has_limits = !calcdef_limits.empty();
   MidasAssert(calcdef_limits.empty() || calcdef_limits.size() == nmodes, "ModalBasisLimits in McTdHCalcDef has wrong size!");

   // Reserve space for offsets
   this->mSpectralModalOffsets.reserve(ndof);

   // Calculate offsets
   In off = I_0;
   In count = I_0;
   for(In i_op_mode=I_0; i_op_mode<ndof; ++i_op_mode)
   {
      this->mSpectralModalOffsets.emplace_back(off);

      GlobalModeNr g_mode = apOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr i_bas_mode = this->mpBasDef->GetLocalModeNr(g_mode);
      auto nbas = this->mpBasDef->Nbas(i_bas_mode);

      // Modify nbas if we have limits
      if (  has_limits
         )
      {
         if (  apOpDef->GetGlobalModeNr(i_op_mode) == this->mElectronicDofNr
            )
         {
            this->mModalBasisLimits.emplace_back(nbas);
         }
         else
         {
            this->mModalBasisLimits.emplace_back(std::min(nbas, calcdef_limits[count++]));
            nbas = this->mModalBasisLimits[i_op_mode];
         }
      }

      // Add to number of modal coefs
      auto nact = this->mNModalsVec[i_op_mode];
      this->mNSpectralModalCoef += nact*nbas;

      off += nbas*nbas;
   }
}

/**
 * Read spectral-basis modals
 *
 * @param aNCoefs    Number of coefs in modals DataCont
 * @param aFileName  Name of DataCont to read from
 **/
void PrimitiveBasisTransformer::ReadSpectralBasisModals
   (  In aNCoefs
   ,  const std::string& aFileName
   )
{
   mSpectralBasisModals.GetFromExistingOnDisc(aNCoefs, aFileName);
   mSpectralBasisModals.SaveUponDecon(true);
}

/**
 * Calcaulte spectral-basis modals for given operator using VSCF.
 * If using exponential parameterization of the modal transform, we need to include the initial active modals 
 * in the basis. In that case, only the virtual space is modified by this function compared to simply reading the 
 * spectral basis.
 *
 * @param aOperName           Name of operator to construct VSCF spectral basis for
 * @param aKeepNRefModals     Keep the first N spectral modals of the reference Hamiltonian as they are (used for exponential parameterization)
 * @param aNRefModalCoefs     Number of coefs in the reference modals DataCont
 * @param aRefModalsFileName  Name of reference-modal DataCont
 **/
void PrimitiveBasisTransformer::CalculateSpectralBasisModals
   (  const std::string& aOperName
   ,  const std::vector<unsigned>& aKeepNRefModals
   ,  In aNRefModalCoefs
   ,  const std::string& aRefModalsFileName
   )
{
   this->mGeneralModalBasis = true;

   if (  !aKeepNRefModals.empty()
      )
   {
      MIDASERROR("Spectral basis for general operators and exponential modal transform not implemented!");
   }

   auto [ncoef, label] = ::midas::td::GenerateSpectralBasisModals(aOperName, this->mpBasDef, I_0);
   this->mSpectralBasisModals.GetFromExistingOnDisc(ncoef, label);
   this->mSpectralBasisModals.SaveUponDecon(true);
}

/**
 * Get modals for spectral-basis transform
 *
 * @return
 *    Const ref to spectral basis
 **/
const DataCont& PrimitiveBasisTransformer::SpectralBasisModals
   (
   )  const
{
   return this->mSpectralBasisModals;
}

/**
 * Get offsets in SpectralBasisModals
 *
 * @return
 *    Offsets in spectral-basis modals
 **/
const std::vector<In>& PrimitiveBasisTransformer::SpectralModalOffsets
   (
   )  const
{
   return this->mSpectralModalOffsets;
}

/**
 * Get BasDef
 *
 * @return
 *    const pointer to basdef
 **/
const BasDef* const PrimitiveBasisTransformer::GetBasDef
   (
   )  const
{
   return this->mpBasDef;
}

/**
 * @return
 *    Const ref to mModalBasisLimits
 **/
const std::vector<In>& PrimitiveBasisTransformer::ModalBasisLimits
   (
   )  const
{
   return this->mModalBasisLimits;
}

/**
 * @return
 *    Number of electronic states
 **/
In PrimitiveBasisTransformer::NElectronicStates
   (
   )  const
{
   return mNElectronicStates;
}

/**
 * @return
 *    ElectronicDofNr
 **/
GlobalModeNr PrimitiveBasisTransformer::ElectronicDofNr
   (
   )  const
{
   return this->mElectronicDofNr;
}

/**
 * @return
 *    True if we are doing non-adiabatic dynamics.
 **/
bool PrimitiveBasisTransformer::NonAdiabatic
   (
   )  const
{
   return this->mNonAdiabatic;
}

/**
 * @return
 *    Vector of number of modals in MCTDH calculation
 **/
const std::vector<unsigned>& PrimitiveBasisTransformer::NModalsVec
   (
   )  const
{
   return this->mNModalsVec;
}

/**
 * @return
 *    Number of modal coefs
 **/
In PrimitiveBasisTransformer::NSpectralModalCoef
   (
   )  const
{
   return this->mNSpectralModalCoef;
}

} /* namespace midas::mctdh */
