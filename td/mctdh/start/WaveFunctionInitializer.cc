/*
************************************************************************
*
* @file                 WaveFunctionInitializer.cc
*
* Created:              03-12-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Class for initializing MCTDH wave functions
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "WaveFunctionInitializer.h"

#include "input/BasDef.h"
#include "tensor/NiceTensor.h"
#include "tensor/ArrayHopper.h"
#include "vcc/TensorDataCont.h"
#include "vcc/VccStateSpace.h"
#include "mmv/MidasVector.h"
#include "input/FindCalculation.h"
#include "util/Io.h"

namespace midas::mctdh
{

/**
 * c-tor from calcdef
 *
 * @param apCalcDef
 * @param apBasDef
 **/
WaveFunctionInitializer::WaveFunctionInitializer
   (  const McTdHCalcDef* const apCalcDef
   ,  const OpDef* const apOpDef
   ,  const BasDef* const apBasDef
   )
   :  PrimitiveBasisTransformer
         (  apCalcDef
         ,  apBasDef
         ,  apOpDef
         )
   ,  mpCalcDef
         (  apCalcDef
         )
   ,  mType
         (  apCalcDef->InitialWavePacket()
         )
{
   const auto& init_name = apCalcDef->InitCalcName();
   const auto& mctdh_basname = apCalcDef->Basis();
   const auto& basdef = *apBasDef;
   MidasAssert(basdef.GetmBasName() == mctdh_basname, "Input BasDef should correspond to the MCTDH basis...");
   std::string ref_oper_name;
   std::string ref_bas_name;

   auto nmodes_op = apOpDef->NmodesInOp();
   auto nmodes_bas = apBasDef->GetmNoModesInBas();

   MidasAssert(nmodes_op == nmodes_bas, "MCTDH number of modes in basis and operator must be the same!");
   MidasAssert(this->NModalsVec().size() == nmodes_op, "MCTDH active space must have same number of modes as operator!");

   std::string spectral_modals_filename = "";

   switch   (  mType
            )
   {
      case McTdHCalcDef::initType::VSCF:
      {
         In i_vscf = midas::input::FindCalculationIndex(gVscfCalcDef, init_name, "VSCF");

         // get calcdef
         const auto& calcdef = gVscfCalcDef[i_vscf];
         ref_bas_name = calcdef.Basis();
         ref_oper_name = calcdef.Oper();

         // Print info on reference state
         const auto& occvec = calcdef.OccupVec();
         const auto& opdef = gOperatorDefs.GetOperator(ref_oper_name);
         Mout  << " Info on reference VSCF calculation:\n"
               << "    Operator:       " << ref_oper_name << "\n"
               << "    Basis:          " << ref_bas_name << "\n"
               << "    Occupation vector:" << std::endl;
         for(In i_op_mode=I_0; i_op_mode<occvec.size(); ++i_op_mode)
         {
            auto g_mode = opdef.GetGlobalModeNr(i_op_mode);
            auto label = gOperatorDefs.GetModeLabel(g_mode);
            Mout  << label << "^" << occvec[i_op_mode] << "  ";
         }
         Mout  << std::endl;

         // Set file names, etc.
         mModalsFilename = calcdef.Name() + "_Modals";
         spectral_modals_filename = mModalsFilename;
         mCoefsFilename = "";   // no coefs
         mNModesRef = calcdef.GetNmodesInOcc();
         mCalcNr = i_vscf;

         // Set number of modals and modal coefs
         this->mNModalsRef.reserve(this->mNModesRef);
         this->mNReferenceModalCoefs = I_0;
         for(In imode=I_0; imode<mNModesRef; ++imode)
         {
            auto g_mode = apOpDef->GetGlobalModeNr(imode);
            auto bas_mode = basdef.GetLocalModeNr(g_mode);
            auto nbas = basdef.Nbas(bas_mode);
            this->mNModalsRef.emplace_back(nbas);
            this->mNReferenceModalCoefs += nbas*nbas;
         }

         break;
      }
      case McTdHCalcDef::initType::VCI:
      {
         // Find Vcc with name 'init_name'
         In i_vcc = midas::input::FindCalculationIndex(gVccCalcDef, init_name, "VCI");

         MidasAssert(gVccCalcDef[i_vcc].Vci(), "MCTDH WaveFunctionInitializer: '" + init_name + "' is not a VCI calculation!");

         // get calcdef
         const auto& calcdef = gVccCalcDef[i_vcc];
         ref_bas_name = calcdef.Basis();
         ref_oper_name = calcdef.Oper();

         // Print info on reference state
         const auto& occvec = calcdef.OccupVec();
         const auto& opdef = gOperatorDefs.GetOperator(ref_oper_name);
         Mout  << " Info on reference VCI calculation:\n"
               << "    Operator:                " << ref_oper_name << "\n"
               << "    Basis:                   " << ref_bas_name << "\n"
               << "    Max exci level:          " << calcdef.MaxExciLevel() << "\n"
               << "    Ext.-range modes:        " << calcdef.GetExtRangeModes() << "\n"
               << "    Ext.-range max eff exci: " << calcdef.GetExtRangeMaxEffExciLevel() << "\n"
               << "    Occupation vector:       " << std::endl;
         for(In i_op_mode=I_0; i_op_mode<occvec.size(); ++i_op_mode)
         {
            auto g_mode = opdef.GetGlobalModeNr(i_op_mode);
            auto label = gOperatorDefs.GetModeLabel(g_mode);
            Mout  << label << "^" << occvec[i_op_mode] << "  ";
         }
         Mout  << std::endl;

         // Assert extended range for electronic DOF
         if (  this->NonAdiabatic()
            && calcdef.GetExtRangeModes().find(opdef.GetLocalModeNr(this->ElectronicDofNr())) == calcdef.GetExtRangeModes().end()
            )
         {
            MIDASERROR("Starting non-adiabatic calculation from VCI requires extended MCR in VCI as well!");
         }

         // Set file names
         mModalsFilename = calcdef.Name() + "_Modals";
         spectral_modals_filename = mModalsFilename;
         mCoefsFilename = calcdef.Name() + "_Vci_vec_0"; // Always take ground state. This can be changed!
         mNModesRef = calcdef.GetNmodesInOcc();
         mCalcNr = i_vcc;

         // Number of VSCF modals in VCI
         const auto& nmodals = calcdef.GetNModals();
         this->CheckNModals(nmodals);

         // Set number of modals and modal coefs
         this->mNModalsRef.reserve(this->mNModesRef);
         this->mNReferenceModalCoefs = I_0;
         for(In imode=I_0; imode<mNModesRef; ++imode)
         {
            auto g_mode = apOpDef->GetGlobalModeNr(imode);
            auto bas_mode = basdef.GetLocalModeNr(g_mode);
            auto nbas = basdef.Nbas(bas_mode);
            this->mNModalsRef.emplace_back(nbas);
            this->mNReferenceModalCoefs += nbas*nbas;
         }

         break;
      }
      // Niels: If initializing from TDH, we also need to read in the reference VSCF modals of the TDH calculation and orthonormalize those to the final TDH wf.
//      case McTdHCalcDef::initType::TDH:
//      {
//         In i_tdh = midas::input::FindCalculationIndex(gTdHCalcDef, init_name, "TDH");
//
//         // get calcdef
//         const auto& calcdef = gTdHCalcDef[i_tdh];
//         ref_bas_name = calcdef.Basis();
//         ref_oper_name = calcdef.Oper();
//
//         // Print info
//         Mout  << " Info on reference TDH calculation:\n"
//               << "    Operator: " << ref_oper_name << "\n"
//               << "    Basis:    " << ref_bas_name << "\n"
//               << std::flush;
//
//         // Set file names, etc.
//         mModalsFilename = calcdef.Name() + "_final_modals";
//         mCoefsFilename = "";   // no coefs
//         mNModesRef = basdef.GetmNoModesInBas();      // Niels: We check that the same basis is used anyway...
//         mCalcNr = i_tdh;
//
//         // Set number of modals and modal coefs
//         this->mNModalsRef.reserve(this->mNModesRef);
//         this->mNReferenceModalCoefs = I_0;
//         for(In imode=I_0; imode<mNModesRef; ++imode)
//         {
//            auto g_mode = apOpDef->GetGlobalModeNr(imode);
//            auto bas_mode = basdef.GetLocalModeNr(g_mode);
//            auto nbas = basdef.Nbas(bas_mode);
//            this->mNModalsRef.emplace_back(nbas);
//            this->mNReferenceModalCoefs += nbas*nbas;
//         }
//
//         break;
//      }
      case McTdHCalcDef::initType::MCTDH:
      {
         In i_mctdh = midas::input::FindCalculationIndex(gMcTdHCalcDef, init_name, "MCTDH");

         // get calcdef
         const auto& calcdef = gMcTdHCalcDef[i_mctdh];
         ref_bas_name = calcdef.Basis();
         ref_oper_name = calcdef.Oper();

         // Print info
         Mout  << " Info on reference MCTDH calculation:\n"
               << "    Operator: " << ref_oper_name << "\n"
               << "    Basis:    " << ref_bas_name << "\n"
               << std::flush;

         // Assert same MCTDH parameterization
         MidasAssert
            (  calcdef.MethodInfo().template At<McTdHCalcDef::mctdhID>("PARAMETERIZATION") == this->mpCalcDef->MethodInfo().template At<McTdHCalcDef::mctdhID>("PARAMETERIZATION")
            ,  "Cannot start MCTDH from MCTDH with different parameterization."
            );

         // Set file names (for MCTDH we only need one file name)
         mModalsFilename = calcdef.Name();   // NB: These are the time-dependent modals!!!
         spectral_modals_filename = (calcdef.SpectralBasisOper().empty() || calcdef.SpectralBasisOper() == ref_oper_name) ? calcdef.InitCalcName() + "_Modals" : "vscf_" + calcdef.SpectralBasisOper() + "_Modals";
         mCoefsFilename = calcdef.Name();
         mNModesRef = calcdef.ActiveSpace().size();
         mCalcNr = i_mctdh;

         // Number of active modals
         const auto& nmodals = calcdef.ActiveSpace();

         // Assert same active spaces
         MidasAssert(nmodals.size() == this->mpCalcDef->ActiveSpace().size(), "Active spaces must have same size for both MCTDH calculations. REF: " + std::to_string(nmodals.size()) + ", CUR: " + std::to_string(this->mpCalcDef->ActiveSpace().size()));
         for(In imode=I_0; imode<nmodals.size(); ++imode)
         {
            if (  nmodals[imode] != this->mpCalcDef->ActiveSpace()[imode]
               )
            {
               MIDASERROR("Reference MCTDH calculation must use same number of active modals as this MCTDH!");
            }
         }

         // Set number of modals and modal coefs
         this->mNModalsRef.reserve(this->mNModesRef);
         this->mNReferenceModalCoefs = I_0;
         for(In imode=I_0; imode<mNModesRef; ++imode)
         {
            auto g_mode = apOpDef->GetGlobalModeNr(imode);
            auto bas_mode = basdef.GetLocalModeNr(g_mode);
            auto nbas = basdef.Nbas(bas_mode);
            this->mNModalsRef.emplace_back(nbas);
            this->mNReferenceModalCoefs += nbas*nbas;
         }
         
         break;
      }
      default:
      {
         MIDASERROR("Unrecognized init type in midas::mctdh::detail::WaveFunctionInitializer");
      }
   }

   // Sanity check
   MidasAssert(ref_bas_name == mctdh_basname, "MCTDH and reference calculation must use the same basis!");
   MidasAssert(mNModesRef == basdef.GetmNoModesInBas(), "Reference calculation must have the same number of modes as the MCTDH basis!");


   // Read or construct spectral-basis modals
   if (  this->mpCalcDef->SpectralBasisOper() == ref_oper_name
      || this->mpCalcDef->SpectralBasisOper() == ""
      || this->mType == McTdHCalcDef::initType::MCTDH      // This must be the way to go for MCTDH start
      )
   {
      this->ReadSpectralBasisModals(this->mNReferenceModalCoefs, spectral_modals_filename);
   }
   else
   {
      std::vector<unsigned> keep_n_ref_modals;
      if (  this->mpCalcDef->ExponentialModalTransform()
         )
      {
         keep_n_ref_modals = this->NModalsVec();
      }

      this->CalculateSpectralBasisModals(this->mpCalcDef->SpectralBasisOper(), keep_n_ref_modals, this->mNReferenceModalCoefs, spectral_modals_filename);
   }
}

//! Get CalcDef
const McTdHCalcDef* const WaveFunctionInitializer::CalcDef
   (
   )  const
{
   return this->mpCalcDef;
}


/**
 * Read modals
 *
 * @param arModals            Reference to modal vector
 *
 * @return
 *    Read in okay?
 **/
bool WaveFunctionInitializer::ReadModalsImpl
   (  ComplexMidasVector& arModals
   )  const
{
   // Read in
   switch   (  this->mType
            )
   {
      // Real modals for VSCF and VCI
      case McTdHCalcDef::initType::VSCF:
      case McTdHCalcDef::initType::VCI:
      {
         Mout  << " Reading modals for VSCF/VCI reference. Filename: '" << this->mModalsFilename << "'." << std::endl;
         
         arModals = this->GetModalCoefsFromDisc<real_t>(false);

         break;
      }
      // Complex modals for TDH
//      case McTdHCalcDef::initType::TDH:
//      {
//         Mout  << " Reading modals for TDH reference. Filename: '" << this->mModalsFilename << "'." << std::endl;
//
//         arModals = this->GetModalCoefsFromDisc<param_t>(true);
//         break;
//      }
      default:
      {
         MIDASERROR("Unrecognized initType!");
      }
   }

   return true;
}

/**
 * Read coefs into complex NiceTensor
 *
 * @param arCoefs             Configuration-space coefs in NiceTensor format
 *
 * @return
 *    Read in okay?
 **/
bool WaveFunctionInitializer::ReadCoefsImpl
   (  NiceTensor<param_t>& arCoefs
   )  const
{
   // X-MCTDH has not been implemented yet...
   if (  this->mpCalcDef->ExponentialCoefsTransform()
      )
   {
      MIDASERROR("Not implemented for exponential coefs transform!");
   }
   else
   {
      // Initialize tensor to correct shape
      arCoefs = NiceSimpleTensor<param_t>(this->NModalsVec());
   }

   switch   (  this->mType
            )
   {
      // For VSCF, put a 1 at the reference coefficient
      case McTdHCalcDef::initType::VSCF:
      {
         Mout  << " VSCF reference. Set reference coefficient to 1." << std::endl;
         // Set the first element to one
         static_cast<SimpleTensor<param_t>*>(arCoefs.GetTensor())->GetData()[0] = param_t(1.);

         // break case
         break;
      }
      // For VCI, read vector as DataCont and convert to tensor. Also works for approximate VCI.
      case McTdHCalcDef::initType::VCI:
      {
         Mout  << " VCI reference. Read coefficients from file: " << this->mCoefsFilename << std::endl;

         // Get calcdef
         const auto& calcdef = gVccCalcDef[this->mCalcNr];
         const auto& nmodals = calcdef.GetNModals();

         In maxexci = this->NonAdiabatic() ? calcdef.GetExtRangeMaxEffExciLevel() : calcdef.MaxExciLevel();
         const auto& ext_range_modes_ref = calcdef.GetExtRangeModes();

         // Initialize MCR to get structure of input VCI vector
         ModeCombiOpRange mcr;
         if (  ext_range_modes_ref.empty()
            )
         {
            mcr = ModeCombiOpRange(maxexci, static_cast<Uin>(this->mNModesRef));
         }
         else
         {
            mcr = ConstructModeCombiOpRangeFromExtendedRangeModes(maxexci, this->mNModesRef, ext_range_modes_ref);
         }

         // Deduce total number of CI coefs
         In n_ci_coefs = I_0;
         for(const auto& mc : mcr)
         {
            In n_coefs = I_1; // We get 1 coef from the reference MC

            const auto& mcvec = mc.MCVec();
            for(const auto& m : mcvec)
            {
               n_coefs *= (nmodals[m]-I_1);
            }

            n_ci_coefs += n_coefs;
         }

         // Output
         if (  gDebug
            || this->mpCalcDef->IoLevel() >= I_8
            )
         {
            Mout  << " Number of VCI coefs to read:   " << n_ci_coefs << std::endl;
         }

         // Get data
         DataCont init_coefs;
         init_coefs.GetFromExistingOnDisc(n_ci_coefs, this->mCoefsFilename);
         init_coefs.SaveUponDecon(true);

         // Put data into tensor
         In data_offset = I_0;
         for(const auto& mc : mcr)
         {
            In n_coefs = I_1; // We get 1 coef from the reference MC
            const auto& mcvec = mc.MCVec();
            for(const auto& m : mcvec)
            {
               n_coefs *= (nmodals[m]-I_1);
            }
            
            // Load real VCI coefs into tmp vector
            MidasVector tmp_vec(n_coefs);
            init_coefs.DataIo(IO_GET, tmp_vec, n_coefs, data_offset);

            // Output
            Mout  << " Read " << n_coefs << " coefficients for MC: " << mc << std::endl;

            // Debug
            if (  gDebug
               || this->mpCalcDef->IoLevel() >= I_10
               )
            {
               Mout  << " tmp_vec:\n" << tmp_vec << std::endl;
            }

            // Use ArrayHopper to iterate through arCoefs
            std::vector<unsigned> hopper_dims(mcvec.size());   // Dims to hop over
            std::vector<unsigned> subdims(arCoefs.NDim(), 1);       // Dimensions of sub-tensor to set data for. Set to one for all indices but the ones iterated over...
            std::vector<unsigned> tensor_start_elem(arCoefs.NDim(), 0); // Start element of tensor (set 1 for modes in MC).
            for(In idim=I_0; idim<mcvec.size(); ++idim)
            {
               hopper_dims[mcvec.size()-idim-1] = mcvec[idim]; // We iterate over the last modes first (generalized row-major...)
               subdims[mcvec[idim]] = nmodals[mcvec[idim]] - 1;
               tensor_start_elem[mcvec[idim]] = 1;
            }
            unsigned start_idx = tensor_start_elem[0];      // Calculate offset in arCoefs (SimpleTensor)
            for(unsigned i=1; i<arCoefs.NDim(); ++i)
            {
               start_idx *= arCoefs.GetDims()[i];
               start_idx += tensor_start_elem[i];
            }

            if (  gDebug
               || this->mpCalcDef->IoLevel() >= I_10
               )
            {
               Mout  << " VCI init: Hopper start elem: " << start_idx << std::endl;
            }

            // Initialize ArrayHopper
            ArrayHopper<param_t> hopper
               (  static_cast<SimpleTensor<param_t>*>(arCoefs.GetTensor())->GetData()+start_idx // pointer to first element of sub-tensor
               ,  arCoefs.GetDims() // dimensions of total tensor
               ,  subdims  // dimensions of the sub-block to hop over
               ,  hopper_dims // dimensions that we iterate through
               );

            // Set elements of arCoefs
            In count = I_0;
            for(auto& elem : hopper)
            {
               elem = param_t(tmp_vec[count++]);
            }

            // Sanity check
            MidasAssert(count == n_coefs, "Number of set coefs is not correct. n_coefs: " + std::to_string(n_coefs) + ", count: " + std::to_string(count));

            // Debug
            if (  gDebug
               || this->mpCalcDef->IoLevel() >= I_10
               )
            {
               Mout  << " arCoefs:\n" << arCoefs << std::endl;
            }

            // incr offset
            data_offset += n_coefs;
         }

         // Check normalization of read-in tensor
         auto norm = arCoefs.Norm();

         if (  libmda::numeric::float_neq(norm, 1.)
            )
         {
            Mout  << " Norm of VCI tensor: " << norm << std::endl;
            MidasWarning("WaveFunctionInitializer: Unnormalized VCI coefs read in for MCTDH. Will re-normalize!");
            arCoefs.Scale(1./norm);
         }

         // break case
         break;
      }
      case McTdHCalcDef::initType::TDH:
      {
         MIDASERROR("Not impl!");
      }
      default:
      {
         MIDASERROR("Unrecognized initType!");
      }
   }

   return true;
}

/**
 * Read coefs into complex TensorDataCont
 *
 * @param arCoefs             Configuration-space coefs in ComplexTensorDataCont format
 *
 * @return
 *    Read in okay?
 **/
bool WaveFunctionInitializer::ReadCoefsImpl
   (  ComplexTensorDataCont& arCoefs
   )  const
{
   // X-MCTDH not ready
   if (  this->mpCalcDef->ExponentialCoefsTransform()
      )
   {
      MIDASERROR("Not implemented for exponential coefs transform!");
   }

   auto ext_range_modes = this->CalcDef()->MethodInfo().template At<std::set<In>>("EXTRANGEMODES");
   auto max_exci = static_cast<Uin>(this->CalcDef()->MethodInfo().template At<In>("MAXEXCILEVEL"));

   ModeCombiOpRange mcr;
   if (  ext_range_modes.empty()
      )
   {
      mcr = ModeCombiOpRange(max_exci, static_cast<Uin>(this->mNModesRef));
   }
   else
   {
      mcr = ConstructModeCombiOpRangeFromExtendedRangeModes(max_exci, this->mNModesRef, ext_range_modes);
   }

   switch   (  this->mType
            )
   {
      // For VSCF, put a 1 at the reference coefficient
      case McTdHCalcDef::initType::VSCF:
      {
         Mout  << " VSCF reference. Set reference coefficient to 1." << std::endl;

         // Init state space
         VccStateSpace vss
            (  mcr
            ,  this->NModalsVec()
            ,  false
            );

         // Init to zero with one at the reference position
         arCoefs = ComplexTensorDataCont(vss, 0, BaseTensor<param_t>::typeID::SIMPLE);

         // break case
         break;
      }
      // For VCI, read vector as DataCont and convert to tensor. Also works for approximate VCI.
      case McTdHCalcDef::initType::VCI:
      {
         Mout  << " VCI reference. Read coefficients from file: " << this->mCoefsFilename << std::endl;

         // Get calcdef
         const auto& calcdef = gVccCalcDef[this->mCalcNr];
         const auto& nmodals = calcdef.GetNModals();

         In max_exci_ref = this->NonAdiabatic() ? calcdef.GetExtRangeMaxEffExciLevel() : calcdef.MaxExciLevel();
         const auto& ext_range_modes_ref = calcdef.GetExtRangeModes();

         MidasAssert(max_exci == max_exci_ref, "MCTDH[n] must start from VCI with same excitation level.");
         MidasAssert(ext_range_modes_ref == ext_range_modes, "MCTDH[n] must start from VCI with same extended-range modes.");

         // Deduce total number of CI coefs
         In n_ci_coefs = I_0;
         for(const auto& mc : mcr)
         {
            In n_coefs = I_1; // We get 1 coef from the reference MC

            const auto& mcvec = mc.MCVec();
            for(const auto& m : mcvec)
            {
               n_coefs *= (nmodals[m]-I_1);
            }

            n_ci_coefs += n_coefs;
         }

         // Output
         if (  gDebug
            || this->mpCalcDef->IoLevel() >= I_8
            )
         {
            Mout  << " Number of VCI coefs to read:   " << n_ci_coefs << std::endl;
         }

         // Get data
         DataCont init_coefs;
         init_coefs.GetFromExistingOnDisc(n_ci_coefs, this->mCoefsFilename);
         init_coefs.SaveUponDecon(true);

         // Set data
         SetAddresses(mcr, nmodals);
         arCoefs = ComplexTensorDataCont(init_coefs, mcr, nmodals, true);

         // Check normalization of read-in coefs
         auto norm = arCoefs.Norm();

         if (  libmda::numeric::float_neq(norm, 1.)
            )
         {
            Mout  << " Norm of VCI TensorDataCont: " << norm << std::endl;
            MidasWarning("WaveFunctionInitializer: Unnormalized VCI coefs read in for MCTDH. Will re-normalize!");
            arCoefs.Scale(1./norm);
         }

         // break case
         break;
      }
      case McTdHCalcDef::initType::TDH:
      {
         MIDASERROR("Not impl!");
      }
      default:
      {
         MIDASERROR("Unrecognized initType!");
      }
   }

   return true;
}

} /* namespace midas::mctdh */
