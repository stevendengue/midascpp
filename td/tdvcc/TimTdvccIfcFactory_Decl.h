/**
 *******************************************************************************
 * 
 * @file    TimTdvccIfcFactory.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TIMTDVCCIFCFACTORY_DECL_H_INCLUDED
#define TIMTDVCCIFCFACTORY_DECL_H_INCLUDED

// Standard headers.
#include <memory>

// Midas headers.
#include "td/tdvcc/TimTdvccIfc.h"
#include "td/tdvcc/TimTdvccEnums.h"

// Forward declares.

namespace midas::tdvcc
{

   //!
   template<class... Args>
   std::unique_ptr<TimTdvccIfc> TimTdvccIfcFactory
      (  CorrType
      ,  TrfType
      ,  Args&&...
      );

} /* namespace midas::tdvcc */


#endif /* TIMTDVCCIFCFACTORY_DECL_H_INCLUDED */
