/**
 *******************************************************************************
 * 
 * @file    TrfTimTdvccMatRep_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTIMTDVCCMATREP_DECL_H_INCLUDED
#define TRFTIMTDVCCMATREP_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "td/tdvcc/TimTdvccEnums.h"
#include "td/tdvcc/trf/TrfTimTdvccMatRepBase.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   class TrfTimTdvccMatRep
      :  public TrfTimTdvccMatRepBase<PARAM_T>
   {
      public:
         using typename TrfTimTdvccMatRepBase<PARAM_T>::param_t;
         using typename TrfTimTdvccMatRepBase<PARAM_T>::step_t;
         using typename TrfTimTdvccMatRepBase<PARAM_T>::opdef_t;
         using typename TrfTimTdvccMatRepBase<PARAM_T>::modalintegrals_t;
         using typename TrfTimTdvccMatRepBase<PARAM_T>::n_modals_t;
         using typename TrfTimTdvccMatRepBase<PARAM_T>::cont_t;

         //! Forward all constructor calls to base class.
         template<class... Args>
         TrfTimTdvccMatRep(Args&&... args)
            :  TrfTimTdvccMatRepBase<PARAM_T>(std::forward<Args>(args)...)
         {
         }

         cont_t ErrVec(step_t aTime, const cont_t& arAmpls) const;
         cont_t EtaVec(step_t aTime, const cont_t& arAmpls) const;
         cont_t LJac(step_t aTime, const cont_t& arAmpls, const cont_t& arCoefs) const;

         inline cont_t ExpValTrf(step_t aTime, const cont_t& arAmpls) const {return ErrVec(aTime, arAmpls);}

   };

} /* namespace midas::tdvcc */


#endif/*TRFTIMTDVCCMATREP_DECL_H_INCLUDED*/
