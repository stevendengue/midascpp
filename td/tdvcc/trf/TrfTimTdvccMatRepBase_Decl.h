/**
 *******************************************************************************
 * 
 * @file    TrfTimTdvccMatRepBase_Decl.h
 * @date    06-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTIMTDVCCMATREPBASE_DECL_H_INCLUDED
#define TRFTIMTDVCCMATREPBASE_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "util/matrep/SparseClusterOper.h"
#include "util/matrep/ShiftOperBraketLooper.h"
#include "util/matrep/OperMat.h"
#include "td/tdvcc/TimTdvccEnums.h"
#include "td/tdvcc/trf/TrfTimTdvccBase.h"

// Forward declarations.
class OpDef;
class ModeCombiOpRange;
template<typename> class ModalIntegrals;
template<typename> class GeneralDataCont;


namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   class TrfTimTdvccMatRepBase
      :  public TrfTimTdvccBase<PARAM_T>
   {
      public:
         using typename TrfTimTdvccBase<PARAM_T>::param_t;
         using typename TrfTimTdvccBase<PARAM_T>::step_t;
         using typename TrfTimTdvccBase<PARAM_T>::opdef_t;
         using typename TrfTimTdvccBase<PARAM_T>::modalintegrals_t;
         using typename TrfTimTdvccBase<PARAM_T>::n_modals_t;
         template<typename U> using cont_tmpl = GeneralMidasVector<U>;
         using cont_t = cont_tmpl<param_t>;
         using mat_t = midas::util::matrep::OperMat<param_t>;
         using cl_oper_t = midas::util::matrep::SparseClusterOper;
         using looper_t = midas::util::matrep::ShiftOperBraketLooper;
         static inline constexpr TrfType trf_type = TrfType::MATREP;

         //! Forward all constructor calls to base class.
         template<class... Args>
         TrfTimTdvccMatRepBase(Args&&... args)
            :  TrfTimTdvccBase<PARAM_T>(std::forward<Args>(args)...)
            ,  mOperMat(ConstructOperMat(this->NModals(), this->Oper(), this->ModInts()))
            ,  mShiftOperBraketLooper(this->NModals(), this->Mcr())
            ,  mSparseClusterOper(ConstructSparseClusterOper(this->NModals(), this->Mcr()))
         {
         }


      protected:
         const mat_t& OperMat() const {return mOperMat;}
         const looper_t& ShiftOperBraketLooper() const {return mShiftOperBraketLooper;}
         const cl_oper_t& SparseClusterOper() const {return mSparseClusterOper;}
         void PrintAnalysisAndTimings() const;
         void PrintScoOptAnalysis() const;
         void PrintOperMatCtorTiming() const;

      private:
         std::map<std::string,Nb> opermat_ctor_timing = {{"CPU", 0.}, {"Wall", 0.}};
         std::map<std::string,Nb> sco_ctor_timing = {{"CPU", 0.}, {"Wall", 0.}};
         mat_t mOperMat;
         looper_t mShiftOperBraketLooper;
         cl_oper_t mSparseClusterOper;
         mutable bool has_printed_sco_opt_anal = false;
         mutable bool has_printed_opermat_timing = false;

         //! Utility function for the MatRep transformers.
         mat_t ConstructOperMat
            (  const n_modals_t& arNModals
            ,  const opdef_t& arOpDef
            ,  const modalintegrals_t& arModInts
            );
         cl_oper_t ConstructSparseClusterOper
            (  const n_modals_t& arNModals
            ,  const ModeCombiOpRange& arMcr
            );

   };

} /* namespace midas::tdvcc */


#endif/*TRFTIMTDVCCMATREPBASE_DECL_H_INCLUDED*/
