/**
 *******************************************************************************
 * 
 * @file    TrfTimTdextvccMatRep_Decl.h
 * @date    06-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTIMTDEXTVCCMATREP_DECL_H_INCLUDED
#define TRFTIMTDEXTVCCMATREP_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "td/tdvcc/TimTdvccEnums.h"
#include "td/tdvcc/trf/TrfTimTdvccMatRepBase.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   class TrfTimTdextvccMatRep
      :  public TrfTimTdvccMatRepBase<PARAM_T>
   {
      public:
         using typename TrfTimTdvccMatRepBase<PARAM_T>::param_t;
         using typename TrfTimTdvccMatRepBase<PARAM_T>::step_t;
         using typename TrfTimTdvccMatRepBase<PARAM_T>::opdef_t;
         using typename TrfTimTdvccMatRepBase<PARAM_T>::modalintegrals_t;
         using typename TrfTimTdvccMatRepBase<PARAM_T>::n_modals_t;
         using typename TrfTimTdvccMatRepBase<PARAM_T>::cont_t;

         //! Forward all constructor calls to base class.
         template<class... Args>
         TrfTimTdextvccMatRep(Args&&... args)
            :  TrfTimTdvccMatRepBase<PARAM_T>(std::forward<Args>(args)...)
         {
         }

         cont_t HamDerExtAmp(step_t aTime, const cont_t& arClustAmps, const cont_t& arExtAmps) const;
         cont_t HamDerClustAmp(step_t aTime, const cont_t& arClustAmps, const cont_t& arExtAmps) const;
         cont_t LeftExpmS(const cont_t& arVec, const cont_t& arExtAmps) const;
         cont_t RightExpmS(const cont_t& arVec, const cont_t& arExtAmps) const;

   };

} /* namespace midas::tdvcc */


#endif/*TRFTIMTDEXTVCCMATREP_DECL_H_INCLUDED*/
