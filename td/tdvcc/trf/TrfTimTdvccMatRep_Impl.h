/**
 *******************************************************************************
 * 
 * @file    TrfTimTdvccMatRep_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTIMTDVCCMATREP_IMPL_H_INCLUDED
#define TRFTIMTDVCCMATREP_IMPL_H_INCLUDED

// Midas headers.
#include "util/matrep/MatRepTransformers.h"
#include "util/matrep/MatRepUtils.h"
#include "input/OpDef.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "mmv/MidasVector.h"
#include "util/Timer.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTimTdvccMatRep<PARAM_T>::cont_t TrfTimTdvccMatRep<PARAM_T>::ErrVec
      (  step_t aTime
      ,  const cont_t& arAmpls
      )  const
   {
      using namespace midas::util::matrep;
      this->PrintAnalysisAndTimings();

      Timer timer;
      timer.Reset();
      auto result = TrfVccErrVec(this->OperMat(), this->SparseClusterOper(), arAmpls);
      this->PrintTimings("TrfTimTdvccMatRep", "ErrVec", timer);

      return result;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTimTdvccMatRep<PARAM_T>::cont_t TrfTimTdvccMatRep<PARAM_T>::EtaVec
      (  step_t aTime
      ,  const cont_t& arAmpls
      )  const
   {
      using namespace midas::util::matrep;
      this->PrintAnalysisAndTimings();

      Timer timer;
      timer.Reset();
      auto result = TrfVccEtaVec(this->OperMat(), this->ShiftOperBraketLooper(), this->SparseClusterOper(), arAmpls);
      this->PrintTimings("TrfTimTdvccMatRep", "EtaVec", timer);

      return result;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTimTdvccMatRep<PARAM_T>::cont_t TrfTimTdvccMatRep<PARAM_T>::LJac
      (  step_t aTime
      ,  const cont_t& arAmpls
      ,  const cont_t& arCoefs
      )  const
   {
      using namespace midas::util::matrep;
      this->PrintAnalysisAndTimings();

      Timer timer;
      timer.Reset();
      auto result = TrfVccLJac(this->OperMat(), this->ShiftOperBraketLooper(), this->SparseClusterOper(), arAmpls, arCoefs);
      this->PrintTimings("TrfTimTdvccMatRep", "LJac", timer);

      return result;
   }

} /* namespace midas::tdvcc */




#endif/*TRFTIMTDVCCMATREP_IMPL_H_INCLUDED*/
