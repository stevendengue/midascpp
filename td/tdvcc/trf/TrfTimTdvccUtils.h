/**
 *******************************************************************************
 * 
 * @file    TrfTimTdvccUtils.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTIMTDVCCUTILS_H_INCLUDED
#define TRFTIMTDVCCUTILS_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "td/tdvcc/TimTdvccEnums.h"

// Forward declarations.
class OpDef;
class ModeCombiOpRange;
template<typename> class ModalIntegrals;
template<typename> class GeneralDataCont;


namespace midas::tdvcc
{
   /************************************************************************//**
    * @brief
    *    Passes IoLevel() and TimeIt() options on to transformer.
    ***************************************************************************/
   template
      <  class TRF_T
      ,  class OTHER_T
      >
   void PassSettingsToTrf
      (  TRF_T& arTarget
      ,  const OTHER_T& arSource
      )
   {
      arTarget.IoLevel() = arSource.IoLevel();
      arTarget.TimeIt() = arSource.TimeIt();
   }

} /* namespace midas::tdvcc */


#endif/*TRFTIMTDVCCUTILS_H_INCLUDED*/
