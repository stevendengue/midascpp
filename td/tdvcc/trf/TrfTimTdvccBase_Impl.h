/**
 *******************************************************************************
 * 
 * @file    TrfTimTdvccBase_Impl.h
 * @date    06-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTIMTDVCCBASE_IMPL_H_INCLUDED
#define TRFTIMTDVCCBASE_IMPL_H_INCLUDED

// Midas headers.
#include "util/matrep/MatRepVibOper.h"
#include "input/OpDef.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "mmv/DataCont.h"
#include "vcc/vcc2/Vcc2TransReg.h"
#include "vcc/vcc2/Vcc2TransEta.h"
#include "vcc/vcc2/Vcc2TransLJac.h"
#include "td/tdvcc/trf/TrfTimTdvccUtils.h"
#include "util/Timer.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   TrfTimTdvccBase<PARAM_T>::TrfTimTdvccBase
      (  const n_modals_t& arNModals
      ,  const opdef_t& arOpDef
      ,  const modalintegrals_t& arModInts
      ,  const ModeCombiOpRange& arMcr
      )
      :  mrNModals(arNModals)
      ,  mrOpDef(arOpDef)
      ,  mrModInts(arModInts)
      ,  mrMcr(arMcr)
   {
      this->SanityCheck();
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void TrfTimTdvccBase<PARAM_T>::PrintTimings
      (  const std::string& arClassName
      ,  const std::string& arFuncName
      ,  Timer& arTimer
      )  const
   {
      if (this->TimeIt())
      {
         std::stringstream ss;
         ss << arClassName << "<" << midas::type_traits::TypeName<PARAM_T>() << ">::"
            << arFuncName << "(...)"
            << ", oper = '" << this->Oper().Name() << "'; "
            ;
         arTimer.CpuOut (Mout, ss.str()+"CPU  time = ");
         arTimer.WallOut(Mout, ss.str()+"Wall time = ");
      }
   }

   /************************************************************************//**
    * Checks:
    *    -  Mcr() must contain the empty ModeCombi (for reference element),
    *    otherwise the transformer will e.g. not be able to compute expectation
    *    values.
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void TrfTimTdvccBase<PARAM_T>::SanityCheck
      (
      )  const
   {
      std::stringstream ss;
      if (this->Mcr().NumEmptyMCs() == 0)
      {
         ss << "ModeCombiOpRange must contain the empty ModeCombi but doesn't.\n"
            << "this->Mcr() = \n" << this->Mcr()
            ;
         MIDASERROR(ss.str());
      }
   }

} /* namespace midas::tdvcc */




#endif/*TRFTIMTDVCCBASE_IMPL_H_INCLUDED*/
