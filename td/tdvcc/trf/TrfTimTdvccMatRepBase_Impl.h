/**
 *******************************************************************************
 * 
 * @file    TrfTimTdvccMatRepBase_Impl.h
 * @date    06-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRFTIMTDVCCMATREPBASE_IMPL_H_INCLUDED
#define TRFTIMTDVCCMATREPBASE_IMPL_H_INCLUDED

// Midas headers.
#include "util/matrep/MatRepVibOper.h"
#include "util/Timer.h"
#include "input/OpDef.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTimTdvccMatRepBase<PARAM_T>::mat_t TrfTimTdvccMatRepBase<PARAM_T>::ConstructOperMat
      (  const n_modals_t& arNModals
      ,  const opdef_t& arOpDef
      ,  const modalintegrals_t& arModInts
      )
   {
      Timer timer;
      timer.Reset();
      mat_t m(arNModals, arOpDef, arModInts, true);
      this->opermat_ctor_timing.at("CPU") = timer.CpuTime();
      this->opermat_ctor_timing.at("Wall") = timer.WallTime();
      return m;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   typename TrfTimTdvccMatRepBase<PARAM_T>::cl_oper_t TrfTimTdvccMatRepBase<PARAM_T>::ConstructSparseClusterOper
      (  const n_modals_t& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      Timer timer;
      timer.Reset();
      cl_oper_t sco(arNModals, arMcr);
      this->sco_ctor_timing.at("CPU") = timer.CpuTime();
      this->sco_ctor_timing.at("Wall") = timer.WallTime();
      return sco;
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void TrfTimTdvccMatRepBase<PARAM_T>::PrintAnalysisAndTimings
      (
      )  const
   {
      PrintOperMatCtorTiming();
      PrintScoOptAnalysis();
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void TrfTimTdvccMatRepBase<PARAM_T>::PrintScoOptAnalysis
      (
      )  const
   {
      if (  (  this->TimeIt()
            || this->IoLevel() > 5
            )
         && !has_printed_sco_opt_anal
         )
      {
         std::stringstream ss;
         ss << "TrfTimTdvccMatRepBase<" << midas::type_traits::TypeName<PARAM_T>()
            << ">::ConstructSparseClusterOper(...)"
            << ", oper = '" << this->Oper().Name() << "'; "
            ;
         for(const auto& [type,time]: this->sco_ctor_timing)
         {
            std::stringstream ss2;
            ss2 << std::left << std::scientific << std::setw(4) << type << " time = " << time << "s\n";
            Mout << ss.str() << ss2.str();
         }
         Mout << std::flush;
         Mout << "SparseClusterOper optimization analysis:\n";
         midas::util::matrep::OptimizationAnalysis(this->SparseClusterOper(), Mout);
         Mout << std::flush;
         this->has_printed_sco_opt_anal = true;
      }
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   void TrfTimTdvccMatRepBase<PARAM_T>::PrintOperMatCtorTiming
      (
      )  const
   {
      if (  (  this->TimeIt()
            || this->IoLevel() > 5
            )
         && !has_printed_opermat_timing
         )
      {
         std::stringstream ss;
         ss << "TrfTimTdvccMatRepBase<" << midas::type_traits::TypeName<PARAM_T>()
            << ">::ConstructOperMat(...)"
            << ", oper = '" << this->Oper().Name() << "'; "
            ;
         for(const auto& [type,time]: this->opermat_ctor_timing)
         {
            std::stringstream ss2;
            ss2 << std::left << std::scientific << std::setw(4) << type << " time = " << time << "s\n";
            Mout << ss.str() << ss2.str();
         }
         Mout << std::flush;
         this->has_printed_opermat_timing = true;
      }
   }

} /* namespace midas::tdvcc */




#endif/*TRFTIMTDVCCMATREPBASE_IMPL_H_INCLUDED*/
