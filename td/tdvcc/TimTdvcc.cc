/**
 *******************************************************************************
 * 
 * @file    TimTdvcc.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "td/tdvcc/TimTdvcc.h"
#include "td/tdvcc/TimTdvcc_Impl.h"

#include <complex>
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "td/tdvcc/params/ParamsTimTdvcc.h"
#include "td/tdvcc/params/ParamsTimTdvci.h"
#include "td/tdvcc/params/ParamsTimTdextvcc.h"
#include "td/tdvcc/deriv/DerivTimTdvcc.h"
#include "td/tdvcc/deriv/DerivTimTdvci.h"
#include "td/tdvcc/deriv/DerivTimTdextvcc.h"
#include "td/tdvcc/trf/TrfTimTdvccMatRep.h"
#include "td/tdvcc/trf/TrfTimTdvcc2.h"
#include "td/tdvcc/trf/TrfTimTdvciMatRep.h"
#include "td/tdvcc/trf/TrfTimTdvci2.h"
#include "td/tdvcc/trf/TrfTimTdextvccMatRep.h"

// Define instatiation macro.
#define UNPACK(...) __VA_ARGS__
#define INSTANTIATE_TIMTDVCC_IMPL(VEC_T,TRF_T,DERIV_T) \
   template class TimTdvcc<VEC_T,TRF_T,DERIV_T>; \

#define INSTANTIATE_TIMTDVCC(PARAM_T,IMAG_TIME) \
   namespace midas::tdvcc \
   { \
      INSTANTIATE_TIMTDVCC_IMPL(UNPACK(ParamsTimTdvcc<PARAM_T,GeneralMidasVector>),TrfTimTdvccMatRep<PARAM_T>,UNPACK(DerivTimTdvcc<PARAM_T,GeneralMidasVector,TrfTimTdvccMatRep,IMAG_TIME>)); \
      INSTANTIATE_TIMTDVCC_IMPL(UNPACK(ParamsTimTdvcc<PARAM_T,GeneralDataCont>),TrfTimTdvcc2<PARAM_T>,UNPACK(DerivTimTdvcc<PARAM_T,GeneralDataCont,TrfTimTdvcc2,IMAG_TIME>)); \
      INSTANTIATE_TIMTDVCC_IMPL(UNPACK(ParamsTimTdvci<PARAM_T,GeneralMidasVector>),TrfTimTdvciMatRep<PARAM_T>,UNPACK(DerivTimTdvci<PARAM_T,GeneralMidasVector,TrfTimTdvciMatRep,IMAG_TIME>)); \
      INSTANTIATE_TIMTDVCC_IMPL(UNPACK(ParamsTimTdvci<PARAM_T,GeneralDataCont>),TrfTimTdvci2<PARAM_T>,UNPACK(DerivTimTdvci<PARAM_T,GeneralDataCont,TrfTimTdvci2,IMAG_TIME>)); \
      INSTANTIATE_TIMTDVCC_IMPL(UNPACK(ParamsTimTdextvcc<PARAM_T,GeneralMidasVector>),TrfTimTdextvccMatRep<PARAM_T>,UNPACK(DerivTimTdextvcc<PARAM_T,GeneralMidasVector,TrfTimTdextvccMatRep,IMAG_TIME>)); \
   }; /* namespace midas::tdvcc */ \

// Instantiations.
INSTANTIATE_TIMTDVCC(std::complex<Nb>,false);
INSTANTIATE_TIMTDVCC(std::complex<Nb>,true);
INSTANTIATE_TIMTDVCC(Nb,true);

#undef INSTANTIATE_TIMTDVCC
#undef INSTANTIATE_TIMTDVCC_IMPL
#undef UNPACK
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
