/**
 *******************************************************************************
 * 
 * @file    TimTdvccIfc.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TIMTDVCCIFC_H_INCLUDED
#define TIMTDVCCIFC_H_INCLUDED

// Standard headers.
#include <complex>
#include <map>
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "td/tdvcc/TimTdvccEnums.h"
#include "ode/OdeInfo.h"
#include "input/OdeInput.h"

// Forward declares.
class OpDef;
class ModeCombiOpRange;
class TdPropertyDef;
template<typename> class ModalIntegrals;
template<typename> class GeneralMidasVector;

namespace midas::tdvcc
{

   /************************************************************************//**
    * @brief
    *    Interface base class for running and storing results from a TIM-TDVCC
    *    calculation.
    ***************************************************************************/
   class TimTdvccIfc
   {
      public:
         using ifc_step_t = Nb;
         using ifc_absval_t = Nb;
         using ifc_param_t = std::complex<Nb>;
         using ifc_prop_t = std::complex<Nb>;

         TimTdvccIfc(const TimTdvccIfc&) = default;
         TimTdvccIfc(TimTdvccIfc&&) = default;
         TimTdvccIfc& operator=(const TimTdvccIfc&) = default;
         TimTdvccIfc& operator=(TimTdvccIfc&&) = default;
         virtual ~TimTdvccIfc() = default;

         TimTdvccIfc();

         /******************************************************************//**
          * @name Get/set general settings
          *********************************************************************/
         //!@{
         const std::string& Name() const {return mName;}
         std::string& Name() {return mName;}

         Uin IoLevel() const {return mIoLevel;}
         Uin& IoLevel() {return mIoLevel;}

         bool TimeIt() const {return mTimeIt;}
         bool& TimeIt() {return mTimeIt;}

         bool SaveFvciVecs() const {return mSaveFvciVecs;}
         bool& SaveFvciVecs() {return mSaveFvciVecs;}

         ifc_absval_t ImagTimeHaultThr() const {return mImagTimeHaultThr;}
         ifc_absval_t& ImagTimeHaultThr() {return mImagTimeHaultThr;}

         ifc_step_t PrintoutInterval() const {return mPrintoutInterval;}
         ifc_step_t& PrintoutInterval() {return mPrintoutInterval;}
         //!@}

         /******************************************************************//**
          * @name Queries
          *********************************************************************/
         //!@{
         virtual Uin SizeParams() const = 0;
         virtual Uin SizeTotal() const = 0;
         virtual bool HasPhase() const = 0;
         virtual bool ImagTime() const = 0;
         virtual Uin NumHamiltonianContribs() const = 0;
         virtual const std::vector<Uin>& NModals() const = 0;
         virtual Uin MaxExci() const = 0;
         virtual CorrType GetCorrType() const = 0;
         virtual TrfType GetTrfType() const = 0;
         const midas::ode::OdeInfo& GetOdeInfo() const {return mOdeInfo;}

         void PrintSettings(std::ostream&, const Uin aIndent = 0) const;
         void Summary(std::ostream&, const Uin aIndent = 0) const;
         void PrintToFiles(std::ostream&, const std::string& arAbsPathDir, const Uin aIndent) const;
         void CalcAndWriteSpectra(std::ostream&, const std::string& arAbsPathDir, const Uin aIndent, const TdPropertyDef&) const;
         std::string GenerateFileName(PropID) const;
         std::string GenerateFileName(StatID) const;
         //!@}

         /******************************************************************//**
          * @name Set propagation details
          *********************************************************************/
         //!@{
         void SetInitState(std::map<ParamID,GeneralMidasVector<Nb>> = {});
         void SetVccGroundState(std::map<ParamID,GeneralMidasVector<Nb>> = {});
         void SetOdeInfo(midas::ode::OdeInfo);
         virtual void EnableImagTime() = 0;
         void EnableTracking(PropID);
         void EnableTracking(StatID);
         bool Tracks(PropID) const;
         bool Tracks(StatID) const;

         //! _Must_ reserve space for operators to avoid re-allocations!
         void ExptValReserve(const Uin);

         //! Enable expt.val. operator; hard error if enabling more than ExpValReserve()'d.
         void EnableExptVal(const OpDef*, ModalIntegrals<Nb>&&);
         //!@}

         /******************************************************************//**
          * @name Driver calls
          *********************************************************************/
         //!@{
         void Evolve();
         //!@}

         /******************************************************************//**
          * @name Post-calc. extractions
          *********************************************************************/
         //!@{
         std::vector<ifc_step_t> ExtractInterpolTs() &&;
         std::vector<GeneralMidasVector<ifc_param_t>> ExtractFvciVecsKet() &&;
         std::vector<GeneralMidasVector<ifc_param_t>> ExtractFvciVecsBra() &&;
         //!@}

         /******************************************************************//**
          * @name Utilities
          *********************************************************************/
         //!@{
         template<typename T>
         static void PrintTableToFile
            (  const std::string& arAbsPathFile
            ,  const std::vector<std::string>& arHeaders
            ,  const std::vector<ifc_step_t>& arTs
            ,  const std::vector<const std::vector<T>*>& arVecVals
            );
         //!@}


      protected:
         const std::set<PropID>& TrackedProps() const;
         const std::set<PropID>& TrackedAutoCorr() const;
         const std::set<StatID>& TrackedStats() const;
         const std::set<std::string>& TrackedExptVals() const;

         void SaveAcceptedTime(ifc_step_t);
         void SaveInterpolatedTime(ifc_step_t);

         void SaveEnergyAndContribs(const std::vector<std::tuple<ifc_prop_t,ifc_param_t,ifc_param_t>>&);
         void SavePhase(ifc_param_t);
         void SaveStat(StatID, ifc_absval_t, const std::map<ParamID,ifc_absval_t>&);
         void SaveExpVals(const std::map<std::string,ifc_prop_t>&);
         void SaveAutoCorr(PropID, const std::pair<ifc_prop_t,ifc_prop_t>&);
         void SaveFvciVec(GeneralMidasVector<ifc_param_t>&&,GeneralMidasVector<ifc_param_t>&&);

         ifc_absval_t LastDerivNorm() const {return mLastDerivNorm;}
         ifc_absval_t& LastDerivNorm() {return mLastDerivNorm;}
         bool CheckLastDerivNorm() const {return ImagTime() && ImagTimeHaultThr() > 0;}

         void PrintEvolveStatus() const;
         virtual void PassSettingsToTrfs() = 0;

      private:
         std::string mName = "default_timtdvcc_name";
         Uin mIoLevel = 0;
         bool mTimeIt = false;
         bool mSaveFvciVecs = false;
         ifc_absval_t mImagTimeHaultThr = 0.0;
         ifc_absval_t mLastDerivNorm = std::numeric_limits<ifc_absval_t>::max();
         mutable ifc_step_t mNextPrintoutAtTime = 0.0;
         ifc_step_t mPrintoutInterval = 0.0;

         midas::ode::OdeInfo mOdeInfo;

         //@{
         //! Times for saving; actual steps and interpolated times.
         std::vector<ifc_step_t> mStepTs;
         std::vector<ifc_step_t> mInterpolTs;
         //@}

         //@{
         //! Properties for tracking.
         std::set<PropID> mTrackedProps;
         std::map<PropID,std::vector<ifc_prop_t>> mProps;
         //@}

         //@{
         //! Hamiltonian components. (coef, deriv, exp.val. contrib.) contribs<times<vals>>
         std::vector<std::vector<ifc_prop_t>> mHamContribExpVal;
         std::vector<std::vector<ifc_param_t>> mHamContribCoef;
         std::vector<std::vector<ifc_param_t>> mHamContribDeriv;
         //@}

         //@{
         //! Statistics (norms^2) for tracking. Totals and ParamID contributions.
         std::set<StatID> mTrackedStats;
         std::map<StatID,std::vector<ifc_absval_t>> mStats;
         std::map<StatID,std::map<ParamID,std::vector<ifc_absval_t>>> mStatsContribs;
         //@}

         //@{
         std::set<std::string> mTrackedExptVals;
         std::map<std::string,std::vector<ifc_prop_t>> mExptVals;
         //@}

         //@{
         //! FVCI vectors at interpolated times, if so requested.
         std::vector<GeneralMidasVector<ifc_param_t>> mFvciVecsKet;
         std::vector<GeneralMidasVector<ifc_param_t>> mFvciVecsBra;
         //@}

         /******************************************************************//**
          * @name Auto-correlation functions
          *********************************************************************/
         //!@{
         std::set<PropID> mTrackedAutoCorr;

         //! The \f$ \langle \Lambda(0) \vert \Psi(t) \rangle \f$ (`<L(0)|VCC(t)>`).
         std::vector<ifc_prop_t> mAutoCorrA;

         //! The \f$ \langle \Lambda(t) \vert \Psi(0) \rangle^* \f$ (`<L(t)|VCC(0)>^*`).
         std::vector<ifc_prop_t> mAutoCorrB;

         //! The average of the two above.
         std::vector<ifc_prop_t> mAutoCorr;
         //!@}

         /******************************************************************//**
          * @name "Extended" auto-correlation functions
          *
          * Same as mAutoCorrA, etc., _but_ with the bra changed as follows:
          *    -  `<Phi|(1+L)exp(-T)      --> <Phi|exp(L)exp(-T)`    for VCC
          *    -  `<Phi|exp(Sigma)exp(-T) --> <Phi|(1+Sigma)exp(-T)` for ExtVCC
          *    -  no changes for VCI
          *********************************************************************/
         //!@{
         std::vector<ifc_prop_t> mAutoCorrExtA;
         std::vector<ifc_prop_t> mAutoCorrExtB;
         std::vector<ifc_prop_t> mAutoCorrExt;
         //!@}

         void SanityCheckState(const std::map<ParamID,GeneralMidasVector<Nb>>&) const;
         virtual void SetInitStateImpl(std::map<ParamID,GeneralMidasVector<Nb>>&&) = 0;
         virtual void SetVccGroundStateImpl(std::map<ParamID,GeneralMidasVector<Nb>>&&) = 0;
         virtual void ExptValReserveImpl(const Uin) = 0;
         virtual void EnableExptValImpl(const OpDef*, ModalIntegrals<Nb>&&) = 0;
         static midas::ode::OdeInfo DefaultOdeInfo();

         virtual void EvolveImpl() = 0;
         virtual const std::set<ParamID>& GetParamIDs() const = 0;

         /******************************************************************//**
          * @name Utilities
          *********************************************************************/
         //!@{
         static bool AllValuesReal(const std::vector<ifc_prop_t>&);

         static std::array<ifc_absval_t,5> DistAnal(const std::vector<ifc_absval_t>&, const std::vector<TimTdvccIfc::ifc_step_t>& arT);
         static std::array<ifc_absval_t,5> DistAnalReal(const std::vector<ifc_prop_t>&, const std::vector<TimTdvccIfc::ifc_step_t>& arT);
         static std::array<ifc_absval_t,5> DistAnalImag(const std::vector<ifc_prop_t>&, const std::vector<TimTdvccIfc::ifc_step_t>& arT);
         static std::array<ifc_absval_t,5> DistAnalAbs(const std::vector<ifc_prop_t>&, const std::vector<TimTdvccIfc::ifc_step_t>& arT);

         template<typename T>
         static void PrintEqualityLine
            (  std::ostream& arOs
            ,  const std::string& arLeft
            ,  const T& arRight
            ,  const Uin aIndent
            ,  const Uin aWLeft
            ,  const Uin aWRight
            );

         template<typename T>
         static void SummaryOutputUtil
            (  std::ostream& arOs
            ,  const std::vector<T>& arVec
            ,  const std::vector<ifc_step_t>& arTs
            ,  const std::string& arPrefix
            ,  const Uin aIndent
            ,  const Uin aWLeft
            ,  const Uin aWRight
            );

         std::pair<std::vector<std::string>, std::vector<const std::vector<ifc_prop_t>*>>
         PrepEnergyForWriting() const;

         std::pair<std::vector<std::string>, std::vector<const std::vector<ifc_prop_t>*>>
         PrepPropForWriting(PropID) const;

         std::pair<std::vector<std::string>, std::vector<const std::vector<ifc_absval_t>*>>
         PrepStatForWriting(StatID) const;

         std::pair<std::vector<std::string>, std::vector<const std::vector<ifc_prop_t>*>>
         PrepAutoCorrForWriting(PropID) const;

         std::pair<std::vector<std::string>, std::vector<const std::vector<ifc_prop_t>*>>
         PrepExptValForWriting(const std::string&) const;

         bool PrintPropToFile
            (  const std::string& arAbsPathFile
            ,  PropID aID
            )  const;

         bool PrintStatToFile
            (  const std::string& arAbsPathFile
            ,  StatID aID
            )  const;

         bool PrintAutoCorrToFile
            (  const std::string& arAbsPathFile
            ,  PropID aID
            )  const;

         bool PrintExptValToFile
            (  const std::string& arAbsPathFile
            ,  const std::string& arName
            )  const;

         //!@}
   };

} /* namespace midas::tdvcc */

#include "td/tdvcc/TimTdvccIfc_Impl.h"

#endif /* TIMTDVCCIFC_H_INCLUDED */
