/**
 *******************************************************************************
 * 
 * @file    TimTdvcc_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TIMTDVCC_IMPL_H_INCLUDED
#define TIMTDVCC_IMPL_H_INCLUDED

// Midas headers.
#include "input/OpDef.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "vcc/ModalIntegralsFuncs.h"
#include "ode/OdeIntegrator.h"
#include "td/tdvcc/anal/ExpValTimTdvcc.h"
#include "td/tdvcc/anal/FvciNorm2.h"
#include "td/tdvcc/anal/AutoCorr.h"
#include "td/tdvcc/trf/TrfTimTdvccUtils.h"

namespace midas::tdvcc
{

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   TimTdvcc<VEC_T,TRF_T,DERIV_T>::TimTdvcc
      (  const std::vector<Uin>& arNModals
      ,  const OpDef& arHamiltonian
      ,  ModalIntegrals<param_t> aModInts
      ,  const ModeCombiOpRange& arMcr
      ,  param_t aHamiltonianCoef
      )
      :  mrNModals(arNModals)
      ,  mHamiltonian(TimeIndepOper(arHamiltonian, aHamiltonianCoef))
      ,  mVecModInts({std::move(aModInts)})
      ,  mrMcr(arMcr)
      ,  mSizeParams(CalcSizeParams(mrNModals,mrMcr))
      ,  mVecTrf(VecTrfs(mHamiltonian))
   {
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   TimTdvcc<VEC_T,TRF_T,DERIV_T>::TimTdvcc
      (  const std::vector<Uin>& arNModals
      ,  oper_t aHamiltonian
      ,  std::vector<ModalIntegrals<param_t>> aVecModInts
      ,  const ModeCombiOpRange& arMcr
      )
      :  mrNModals(arNModals)
      ,  mHamiltonian(std::move(aHamiltonian))
      ,  mVecModInts(std::move(aVecModInts))
      ,  mrMcr(arMcr)
      ,  mSizeParams(CalcSizeParams(mrNModals,mrMcr))
      ,  mVecTrf(VecTrfs(mHamiltonian))
   {
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   Uin TimTdvcc<VEC_T,TRF_T,DERIV_T>::SizeTotal
      (
      )  const
   {
      return VEC_T::SizeTotal(SizeParams());
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   bool TimTdvcc<VEC_T,TRF_T,DERIV_T>::HasPhase
      (
      )  const
   {
      return VEC_T::num_phases > 0;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   bool TimTdvcc<VEC_T,TRF_T,DERIV_T>::ImagTime
      (
      )  const
   {
      return DERIV_T::imag_time || mImagTime;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   Uin TimTdvcc<VEC_T,TRF_T,DERIV_T>::MaxExci
      (
      )  const
   {
      return mrMcr.GetMaxExciLevel();
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   CorrType TimTdvcc<VEC_T,TRF_T,DERIV_T>::GetCorrType
      (
      )  const
   {
      return VEC_T::corr_type;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   TrfType TimTdvcc<VEC_T,TRF_T,DERIV_T>::GetTrfType
      (
      )  const
   {
      return TRF_T::trf_type;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::EnableImagTime
      (
      )
   {
      mImagTime = true;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   typename TimTdvcc<VEC_T,TRF_T,DERIV_T>::vec_t
   TimTdvcc<VEC_T,TRF_T,DERIV_T>::ShapedZeroVector
      (
      )  const
   {
      return vec_t(SizeParams());
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   typename TimTdvcc<VEC_T,TRF_T,DERIV_T>::vec_t
   TimTdvcc<VEC_T,TRF_T,DERIV_T>::Derivative
      (  step_t aTime
      ,  const vec_t& arVec
      )  const
   {
      DERIV_T deriv;
      if (ImagTime())
      {
         deriv.EnableImagTime();
      }
      clock_t clock_tot = clock();
      vec_t result = this->ShapedZeroVector();
      for(Uin i = 0; i < mHamiltonian.size(); ++i)
      {
         Axpy(result, deriv(aTime, arVec, GetHamTrf(i)), mHamiltonian.at(i).Coef(aTime));
      }
      if (this->TimeIt())
      {
         Mout
            << "TimTdvcc<...>::Derivative(...); "
            << "total CPU time: " << Nb(clock()-clock_tot)/CLOCKS_PER_SEC << " s"
            << std::endl;
      }
      return result;
   }

   /************************************************************************//**
    * Always returns true (meaning accept the step), but if propagating in
    * imaginary time _and_ the haulting threshold is larger than zero, then
    * calculate and store the norm of the last derivative, for checking it
    * agains the threshold in AcceptedStep().
    *
    * @param[in] arY
    * @param[in] aYOld
    * @param[in] arT
    * @param[in] aTOld
    * @param[in] arDyDt
    *    Calculate the norm of this if imag. time and positive haulting
    *    threshold.
    * @param[in] aDyDtOld
    * @param[in] arStep
    * @param[in] arNDerivFail
    * @return
    *    Whether to accepted the step. Always true for now.
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   bool TimTdvcc<VEC_T,TRF_T,DERIV_T>::ProcessNewVector
      (  vec_t& arY
      ,  const vec_t& aYOld
      ,  step_t& arT
      ,  step_t aTOld
      ,  vec_t& arDyDt
      ,  const vec_t& aDyDtOld
      ,  step_t& arStep
      ,  size_t& arNDerivFail
      )
   {
      if (this->CheckLastDerivNorm())
      {
         this->LastDerivNorm() = Norm(arDyDt);
      }
      return true;
   }

   /************************************************************************//**
    * @note
    *    For now it always return false (meaning continue) if propagating in
    *    real time.
    *    For imag. time it can check whether to hault based on last derivative
    *    norm and a haulting threshold.
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   bool TimTdvcc<VEC_T,TRF_T,DERIV_T>::AcceptedStep
      (  step_t aTime
      ,  const vec_t& arVec
      )
   {
      // Store the time.
      this->SaveAcceptedTime(aTime);

      SaveProperties(aTime, arVec);
      SaveStats(aTime, arVec);
      SaveExpValsImpl(aTime, arVec);

      this->PrintEvolveStatus();

      if (this->CheckLastDerivNorm())
      {
         return this->LastDerivNorm() < this->ImagTimeHaultThr();
      }
      else
      {
         return false;
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::InterpolatedPoint
      (  step_t aTime
      ,  const vec_t& arVec
      )
   {
      // Store the time.
      this->SaveInterpolatedTime(aTime);

      SaveAutoCorrelations(aTime, arVec);
      
      if (this->SaveFvciVecs())
      {
         SaveFvciVecImpl(arVec);
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   const typename TimTdvcc<VEC_T,TRF_T,DERIV_T>::vec_t& TimTdvcc<VEC_T,TRF_T,DERIV_T>::InitState
      (
      )  const
   {
      if (mpInitState)
      {
         return *mpInitState;
      }
      else
      {
         MIDASERROR("Called InitState(), but it's nullptr.");
         static vec_t dummy;
         return dummy;
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   const typename TimTdvcc<VEC_T,TRF_T,DERIV_T>::vec_t& TimTdvcc<VEC_T,TRF_T,DERIV_T>::FinalState
      (
      )  const
   {
      if (mpFinalState)
      {
         return *mpFinalState;
      }
      else
      {
         MIDASERROR("Called FinalState(), but it's nullptr.");
         static vec_t dummy;
         return dummy;
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   const typename TimTdvcc<VEC_T,TRF_T,DERIV_T>::vec_t& TimTdvcc<VEC_T,TRF_T,DERIV_T>::VccGroundState
      (
      )  const
   {
      if (mpVccGroundState)
      {
         return *mpVccGroundState;
      }
      else
      {
         MIDASERROR("Called VccGroundState(), but it's nullptr.");
         static vec_t dummy;
         return dummy;
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   Uin TimTdvcc<VEC_T,TRF_T,DERIV_T>::CalcSizeParams
      (  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      Uin size = 0;
      for(const auto& mc: arMcr)
      {
         size += NumParams(mc, arNModals);
      }
      return size;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   typename TimTdvcc<VEC_T,TRF_T,DERIV_T>::oper_t TimTdvcc<VEC_T,TRF_T,DERIV_T>::TimeIndepOper
      (  const OpDef& arOpDef
      ,  param_t aCoef
      )
   {
      oper_t op;
      op.emplace_back(arOpDef, aCoef);
      return op;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   std::vector<TRF_T> TimTdvcc<VEC_T,TRF_T,DERIV_T>::VecTrfs
      (  const oper_t& arOpers
      )  const
   {
      if (arOpers.size() != mVecModInts.size())
      {
         std::stringstream ss;
         ss << "arOpers.size() (which is " << arOpers.size()
            << ") != mVecModInts.size() (which is " << mVecModInts.size()
            << ")."
            ;
         MIDASERROR(ss.str());
      }

      std::vector<TRF_T> v;
      v.reserve(arOpers.size());

      auto it_modints = this->mVecModInts.begin(), end_modints = this->mVecModInts.end();
      auto it_op = arOpers.begin(), end = arOpers.end();
      for(; it_op != end && it_modints != end_modints;  ++it_op, ++it_modints)
      {
         TRF_T trf(mrNModals, it_op->Oper(), *it_modints, mrMcr);
         PassSettingsToTrf(trf, *this);
         v.emplace_back(std::move(trf));
      }
      if (it_op != end || it_modints != end_modints)
      {
         MIDASERROR("Something wrong, not both iterators are at end.");
      }
      return v;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::PassSettingsToTrfs
      (
      )
   {
      for(auto& trf: mVecTrf)
      {
         PassSettingsToTrf(trf, *this);
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::SetInitStateImpl
      (  std::map<ParamID,GeneralMidasVector<Nb>>&& arParams
      )
   {
      mpInitState = std::make_unique<vec_t>(std::move(arParams));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::SetVccGroundStateImpl
      (  std::map<ParamID,GeneralMidasVector<Nb>>&& arParams
      )
   {
      mpVccGroundState = std::make_unique<vec_t>(std::move(arParams));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::ExptValReserveImpl
      (  const Uin aSize
      )
   {
      if (mVecExpValOpers.size() > 0)
      {
         MIDASERROR("mVecExpValOpers.size() ("+std::to_string(mVecExpValOpers.size())+") > 0; can only reserve once!");
      }
      mVecExpValOpers.reserve(aSize);
      mVecExpValTrfs.reserve(aSize);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::EnableExptValImpl
      (  const OpDef* apOpDef
      ,  ModalIntegrals<Nb>&& arModInts
      )
   {
      if (mVecExpValOpers.size() < mVecExpValOpers.capacity())
      {
         mVecExpValOpers.emplace_back
            (  std::make_tuple
                  (  apOpDef
                  ,  midas::vcc::modalintegrals::detail::ConvertFromReal<param_t>::Convert
                        (  std::move(arModInts)
                        )
                  )
            );
         const auto& oper = *std::get<0>(mVecExpValOpers.back());
         const auto& modints = std::get<1>(mVecExpValOpers.back());
         mVecExpValTrfs.emplace_back
            (  TRF_T(mrNModals, oper, modints, mrMcr)
            );
         PassSettingsToTrf(mVecExpValTrfs.back(), *this);
      }
      else
      {
         MIDASERROR("Trying to enable more expt.val. opers. than reserved space for (size = "+std::to_string(mVecExpValOpers.size())+", capacity = "+std::to_string(mVecExpValOpers.capacity())+").");
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::SaveProperties
      (  step_t aTime
      ,  const vec_t& arVec
      )
   {
      for(const auto& id: this->TrackedProps())
      {
         switch(id)
         {
            case PropID::ENERGY:
               SaveEnergyAndContribsImpl(aTime, arVec);
               break;
            case PropID::PHASE:
               SavePhaseImpl(arVec);
               break;
            default:
               MIDASERROR("Oops, don't know what to do for PropID '"+StringFromEnum(id)+"'.");
         }
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::SaveStats
      (  step_t aTime
      ,  const vec_t& arVec
      )
   {
      for(const auto& id: this->TrackedStats())
      {
         switch(id)
         {
            case StatID::FVCINORM2:
               SaveFvciNorm2Impl(arVec);
               break;
            case StatID::NORM2:
               SaveParamNorm2Impl(arVec);
               break;
            case StatID::DNORM2INIT:
               SaveParamNorm2InitImpl(arVec);
               break;
            case StatID::DNORM2VCCGS:
               SaveParamNorm2InitVccGs(arVec);
               break;
            default:
               MIDASERROR("Oops, don't know what to do for StatID '"+StringFromEnum(id)+"'.");
         }
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::SaveExpValsImpl
      (  step_t aTime
      ,  const vec_t& arVec
      )
   {
      std::map<std::string,ifc_prop_t> m;
      for(Uin i = 0; i < this->mVecExpValTrfs.size(); ++i)
      {
         try
         {
            const auto& oper = *std::get<0>(mVecExpValOpers.at(i));
            const auto& name = oper.Name();
            const auto& trf = mVecExpValTrfs.at(i);
            const ifc_prop_t val = ExpVal(aTime, arVec, trf);
            m[name] = val;
         }
         catch(const std::out_of_range& oor)
         {
            MIDASERROR("Caught out-of-range, i = "+std::to_string(i)+"; "+std::string(oor.what()));
         }
      }
      this->SaveExpVals(m);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::SaveAutoCorrelations
      (  step_t
      ,  const vec_t& arVec
      )
   {
      for(const auto& id: this->TrackedAutoCorr())
      {
         switch(id)
         {
            case PropID::AUTOCORR:
               SaveAutoCorrImpl(arVec);
               break;
            case PropID::AUTOCORR_EXT:
               SaveAutoCorrExtImpl(arVec);
               break;
            default:
               MIDASERROR("Oops, don't know what to do for PropID '"+StringFromEnum(id)+"'.");
               break;
         }
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::SaveEnergyAndContribsImpl
      (  step_t aTime
      ,  const vec_t& arVec
      )
   {
      // term<{contrib,coef,deriv}> for passing on to base class.
      std::vector<std::tuple<ifc_prop_t, ifc_param_t, ifc_param_t>> v_tpl;
      for(Uin i = 0; i < mHamiltonian.size(); ++i)
      {
         v_tpl.emplace_back
            (  ExpVal(aTime, arVec, GetHamTrf(i))
            ,  mHamiltonian.at(i).Coef(aTime)
            ,  mHamiltonian.at(i).Deriv(aTime)
            );
      }
      this->SaveEnergyAndContribs(v_tpl);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::SavePhaseImpl
      (  const vec_t& arVec
      )
   {
      this->SavePhase(arVec.Phase());
   }

   /************************************************************************//**
    * Convention:
    *    -  PHASE: stores exp(2Im(phase))
    *    -  KET:   stores FVCI norm^2 without phase factor
    *    -  BRA:   zero for now (in principle same as KET for VCI, but for VCC
    *    it could be either same as KET or the norm of the Lambda state)
    *    -  total: product of PHASE and KET values
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::SaveFvciNorm2Impl
      (  const vec_t& arVec
      )
   {
      const ifc_absval_t val = FvciNorm2Ket(arVec, this->mrNModals, this->mrMcr);
      const ifc_absval_t phase = exp(2*std::imag(arVec.Phase()));
      std::map<ParamID,ifc_absval_t> m;
      for(const auto& id: this->GetParamIDs())
      {
         switch(id)
         {
            case ParamID::PHASE:
               m[id] = phase;
               break;
            case ParamID::KET:
               m[id] = val;
               break;
            case ParamID::BRA:
               m[id] = FvciNorm2Bra(arVec, this->mrNModals, this->mrMcr);
            default:
               break;
         }
      }
      this->SaveStat(StatID::FVCINORM2, phase*val, m);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::SaveParamNorm2Impl
      (  const vec_t& arVec
      )
   {
      ifc_absval_t val = Norm2(arVec);
      std::map<ParamID,ifc_absval_t> m;
      for(const auto& id: this->GetParamIDs())
      {
         m[id] = Norm2(id, arVec);
      }
      this->SaveStat(StatID::NORM2, val, m);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::SaveParamNorm2InitImpl
      (  const vec_t& arVec
      )
   {
      ifc_absval_t val = DiffNorm2(arVec, this->InitState());
      std::map<ParamID,ifc_absval_t> m;
      for(const auto& id: this->GetParamIDs())
      {
         m[id] = DiffNorm2(id, arVec, this->InitState());
      }
      this->SaveStat(StatID::DNORM2INIT, val, m);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::SaveParamNorm2InitVccGs
      (  const vec_t& arVec
      )
   {
      ifc_absval_t val = DiffNorm2(arVec, this->VccGroundState());
      std::map<ParamID,ifc_absval_t> m;
      for(const auto& id: this->GetParamIDs())
      {
         m[id] = DiffNorm2(id, arVec, this->VccGroundState());
      }
      this->SaveStat(StatID::DNORM2VCCGS, val, m);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::SaveAutoCorrImpl
      (  const vec_t& arVec
      )
   {
      auto [corrA, corrB] = AutoCorr(arVec, this->InitState(), this->mrNModals, this->mrMcr);
      this->SaveAutoCorr(PropID::AUTOCORR, std::make_pair(ifc_prop_t(corrA), ifc_prop_t(corrB)));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::SaveAutoCorrExtImpl
      (  const vec_t& arVec
      )
   {
      auto [corrA, corrB] = AutoCorrExt(arVec, this->InitState(), this->mrNModals, this->mrMcr);
      this->SaveAutoCorr(PropID::AUTOCORR_EXT, std::make_pair(ifc_prop_t(corrA), ifc_prop_t(corrB)));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::SaveFvciVecImpl
      (  const vec_t& arVec
      )
   {
      GeneralDataCont<param_t> dc_ket(ConvertKetToFvci(arVec, this->mrNModals, this->mrMcr));
      GeneralMidasVector<ifc_param_t> mv_ket(std::move(*dc_ket.GetVector()));
      Scale(mv_ket, exp(ifc_param_t(0,-1)*arVec.Phase()));
      Scale(mv_ket, 1.0/Norm(mv_ket));

      GeneralDataCont<param_t> dc_bra(ConvertBraToFvci(arVec, this->mrNModals, this->mrMcr));
      GeneralMidasVector<ifc_param_t> mv_bra(std::move(*dc_bra.GetVector()));
      Scale(mv_bra, exp(ifc_param_t(0,+1)*arVec.Phase()));
      Scale(mv_bra, 1.0/Norm(mv_bra));

      this->SaveFvciVec(std::move(mv_ket), std::move(mv_bra));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   void TimTdvcc<VEC_T,TRF_T,DERIV_T>::EvolveImpl
      (
      )
   {
      OdeIntegrator<TimTdvcc<VEC_T,TRF_T,DERIV_T>> ode(this, this->GetOdeInfo());
      mpFinalState = std::make_unique<vec_t>(InitState());
      ode.Integrate(*mpFinalState);
   }

} /* namespace midas::tdvcc */

#endif/*TIMTDVCC_IMPL_H_INCLUDED*/
