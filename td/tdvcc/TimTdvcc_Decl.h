/**
 *******************************************************************************
 * 
 * @file    TimTdvcc_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TIMTDVCC_DECL_H_INCLUDED
#define TIMTDVCC_DECL_H_INCLUDED

// Standard headers.
#include <type_traits>
#include <vector>

// Midas headers.
#include "util/type_traits/Complex.h"
#include "inc_gen/TypeDefs.h"
#include "td/oper/LinCombOper.h"
#include "td/tdvcc/TimTdvccIfc.h"

// Forward declarations.
class OpDef;
class ModeCombiOpRange;
template<typename> class ModalIntegrals;

namespace midas::tdvcc
{
   /************************************************************************//**
    * Template parameters:
    *    -  VEC_T: class holding data; must have param_t
    *    -  INITIALIZER_T: sets up initial state parameters
    *    -  DERIV_T: given a vec_t (+extra stuff), must compute and return
    *    a vec_t which is the corresponding derivative
    ***************************************************************************/
   template
      <  class VEC_T
      ,  class TRF_T
      ,  class DERIV_T
      >
   class TimTdvcc
      :  public TimTdvccIfc
   {
      public:
         using vec_t = VEC_T;
         using param_t = typename vec_t::param_t;
         using step_t = midas::type_traits::RealTypeT<param_t>;
         using oper_t = td::LinCombOper<step_t, param_t, OpDef>;

         /******************************************************************//**
          * @name Constructors and friends
          *********************************************************************/
         //!@{
         TimTdvcc
            (  const std::vector<Uin>& arNModals
            ,  const OpDef& arHamiltonian
            ,  ModalIntegrals<param_t> aModInts
            ,  const ModeCombiOpRange& arMcr
            ,  param_t aOperCoef = 1
            );

         TimTdvcc
            (  const std::vector<Uin>& arNModals
            ,  oper_t aHamiltonian
            ,  std::vector<ModalIntegrals<param_t>> aVecModInts
            ,  const ModeCombiOpRange& arMcr
            );
         //!@}

         /******************************************************************//**
          * @name Queries
          *********************************************************************/
         //!@{
         //!
         Uin SizeParams() const override {return mSizeParams;}
         Uin SizeTotal() const override;
         bool HasPhase() const override;
         bool ImagTime() const override;
         Uin NumHamiltonianContribs() const override {return mHamiltonian.size();}
         const std::vector<Uin>& NModals() const override {return mrNModals;}
         Uin MaxExci() const override;
         CorrType GetCorrType() const override;
         TrfType GetTrfType() const override;
         //!@}

         /******************************************************************//**
          * @name Set propagation details
          *********************************************************************/
         //!@{
         //! Enable imaginary time at compile time, even if IMAG_TIME is false.
         void EnableImagTime() override;
         //!@}

         /******************************************************************//**
          * @name ODE interface
          *********************************************************************/
         //!@{
         //! 
         vec_t ShapedZeroVector() const;

         //! Calculate time-derivative.
         vec_t Derivative(step_t, const vec_t&) const;

         //! Process new vector, deciding whether to accept step.
         bool ProcessNewVector
            (  vec_t& arY
            ,  const vec_t& aYOld
            ,  step_t& arT
            ,  step_t aTOld
            ,  vec_t& arDyDt
            ,  const vec_t& aDyDtOld
            ,  step_t& arStep
            ,  size_t& arNDerivFail
            );

         //! What to do at accepted step (saving props., etc.). Returns "always continue" a.t.m.
         bool AcceptedStep(step_t, const vec_t&);

         //! Like AcceptedStep but at an interpolated point.
         void InterpolatedPoint(step_t, const vec_t&);

         //!@}

      private:
         using TimTdvccIfc::ifc_step_t;
         using TimTdvccIfc::ifc_absval_t;
         using TimTdvccIfc::ifc_param_t;
         using TimTdvccIfc::ifc_prop_t;

         const std::vector<Uin>& mrNModals;
         const oper_t mHamiltonian;
         const std::vector<ModalIntegrals<param_t>> mVecModInts;
         const ModeCombiOpRange& mrMcr;
         const Uin mSizeParams;
         std::vector<TRF_T> mVecTrf;
         std::unique_ptr<vec_t> mpInitState = nullptr;
         std::unique_ptr<vec_t> mpFinalState = nullptr;
         std::unique_ptr<vec_t> mpVccGroundState = nullptr;
         bool mImagTime = DERIV_T::imag_time;
         std::vector<std::tuple<const OpDef*, ModalIntegrals<param_t>>> mVecExpValOpers;
         std::vector<TRF_T> mVecExpValTrfs;

         const vec_t& InitState() const;
         const vec_t& FinalState() const;
         const vec_t& VccGroundState() const;
         const TRF_T& GetHamTrf(Uin i) const {return mVecTrf.at(i);}

         static Uin CalcSizeParams(const std::vector<Uin>&, const ModeCombiOpRange&);
         static oper_t TimeIndepOper(const OpDef&, param_t);
         std::vector<TRF_T> VecTrfs(const oper_t&) const;
         void PassSettingsToTrfs() override;

         void SetInitStateImpl(std::map<ParamID,GeneralMidasVector<Nb>>&&) override;
         void SetVccGroundStateImpl(std::map<ParamID,GeneralMidasVector<Nb>>&&) override;
         void ExptValReserveImpl(const Uin) override;
         void EnableExptValImpl(const OpDef*, ModalIntegrals<Nb>&&) override;

         //@{
         void SaveProperties(step_t, const vec_t&);
         void SaveStats(step_t, const vec_t&);
         void SaveExpValsImpl(step_t, const vec_t&);
         void SaveAutoCorrelations(step_t, const vec_t&);

         void SaveEnergyAndContribsImpl(step_t, const vec_t&);
         void SavePhaseImpl(const vec_t&);
         void SaveFvciNorm2Impl(const vec_t&);
         void SaveParamNorm2Impl(const vec_t&);
         void SaveParamNorm2InitImpl(const vec_t&);
         void SaveParamNorm2InitVccGs(const vec_t&);
         void SaveAutoCorrImpl(const vec_t&);
         void SaveAutoCorrExtImpl(const vec_t&);
         void SaveFvciVecImpl(const vec_t&);
         //@}

         void EvolveImpl() override;
         const std::set<ParamID>& GetParamIDs() const override {return vec_t::GetParamIDs();}
   };

} /* namespace midas::tdvcc */

#endif/*TIMTDVCC_DECL_H_INCLUDED*/
