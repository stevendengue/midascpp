/**
 *******************************************************************************
 * 
 * @file    DerivTimTdextvcc.cc
 * @date    06-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/deriv/DerivTimTdextvcc.h"
#include "td/tdvcc/deriv/DerivTimTdextvcc_Impl.h"
#include "td/tdvcc/trf/TrfTimTdextvccMatRep.h"

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"

// Define instatiation macro.
#define INSTANTIATE_DERIVTIMTDEXTVCC(PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME) \
   namespace midas::tdvcc \
   { \
      template class DerivTimTdextvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_DERIVTIMTDEXTVCC(std::complex<Nb>, GeneralMidasVector, TrfTimTdextvccMatRep, false);
INSTANTIATE_DERIVTIMTDEXTVCC(std::complex<Nb>, GeneralMidasVector, TrfTimTdextvccMatRep, true);
INSTANTIATE_DERIVTIMTDEXTVCC(Nb, GeneralMidasVector, TrfTimTdextvccMatRep, true);

#undef INSTANTIATE_DERIVTIMTDEXTVCC


#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
