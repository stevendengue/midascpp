/**
 *******************************************************************************
 * 
 * @file    DerivTimTdvcc.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/deriv/DerivTimTdvcc.h"
#include "td/tdvcc/deriv/DerivTimTdvcc_Impl.h"
#include "td/tdvcc/trf/TrfTimTdvccMatRep.h"
#include "td/tdvcc/trf/TrfTimTdvcc2.h"

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"

// Define instatiation macro.
#define INSTANTIATE_DERIVTIMTDVCC(PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME) \
   namespace midas::tdvcc \
   { \
      template class DerivTimTdvcc<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_DERIVTIMTDVCC(std::complex<Nb>, GeneralMidasVector, TrfTimTdvccMatRep, false);
INSTANTIATE_DERIVTIMTDVCC(std::complex<Nb>, GeneralMidasVector, TrfTimTdvccMatRep, true);
INSTANTIATE_DERIVTIMTDVCC(Nb, GeneralMidasVector, TrfTimTdvccMatRep, true);
INSTANTIATE_DERIVTIMTDVCC(std::complex<Nb>, GeneralDataCont, TrfTimTdvcc2, false);
INSTANTIATE_DERIVTIMTDVCC(std::complex<Nb>, GeneralDataCont, TrfTimTdvcc2, true);
INSTANTIATE_DERIVTIMTDVCC(Nb, GeneralDataCont, TrfTimTdvcc2, true);

#undef INSTANTIATE_DERIVTIMTDVCC


#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
