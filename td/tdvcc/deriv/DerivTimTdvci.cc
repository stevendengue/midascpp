/**
 *******************************************************************************
 * 
 * @file    DerivTimTdvci.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/deriv/DerivTimTdvci.h"
#include "td/tdvcc/deriv/DerivTimTdvci_Impl.h"
#include "td/tdvcc/trf/TrfTimTdvciMatRep.h"
#include "td/tdvcc/trf/TrfTimTdvci2.h"

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"

// Define instatiation macro.
#define INSTANTIATE_DERIVTIMTDVCI(PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME) \
   namespace midas::tdvcc \
   { \
      template class DerivTimTdvci<PARAM_T, CONT_TMPL, TRF_TMPL, IMAG_TIME>; \
   } /* namespace midas::tdvcc */ \
   

// Instantiations.
INSTANTIATE_DERIVTIMTDVCI(std::complex<Nb>, GeneralMidasVector, TrfTimTdvciMatRep, false);
INSTANTIATE_DERIVTIMTDVCI(std::complex<Nb>, GeneralMidasVector, TrfTimTdvciMatRep, true);
INSTANTIATE_DERIVTIMTDVCI(Nb, GeneralMidasVector, TrfTimTdvciMatRep, true);
INSTANTIATE_DERIVTIMTDVCI(std::complex<Nb>, GeneralDataCont, TrfTimTdvci2, false);
INSTANTIATE_DERIVTIMTDVCI(std::complex<Nb>, GeneralDataCont, TrfTimTdvci2, true);
INSTANTIATE_DERIVTIMTDVCI(Nb, GeneralDataCont, TrfTimTdvci2, true);

#undef INSTANTIATE_DERIVTIMTDVCI


#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
