/**
 *******************************************************************************
 * 
 * @file    DriverUtils.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.

 * 
 *******************************************************************************
 **/

// Standard headers.

// Midas headers.
#include "td/tdvcc/DriverUtils.h"
#include "input/VscfCalcDef.h"
#include "input/VccCalcDef.h"
#include "input/BasDef.h"
#include "input/OpDef.h"
#include "input/GlobalOperatorDefinitions.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "mmv/VectorAngle.h"
#include "vcc/ModalIntegralsFuncs.h"
#include "td/PrimitiveIntegrals.h"
#include "td/tdvcc/TimTdvccIfc.h"
#include "input/TimTdvccCalcDef.h"
#include "util/matrep/MatRepUtils.h"
#include "util/matrep/MatRepTransformers.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/VccStateSpace.h"
#include "vcc/VccStateModeCombi.h"
#include "vcc/subspacesolver/ExtVccSolver.h"

// Forward declares.


namespace midas::tdvcc
{
   /************************************************************************//**
    * @note
    *    If no unique match can be found, this function causes a MIDASERROR.
    *
    * @note
    *    The VCC input handles Name()/Prefix() differently than what is done
    *    here at the moment; in VCC input, a Prefix() match enables running
    *    separate VCC calcs. for all matches. (MBH, May 2019)
    *
    * @param[in] arV
    *    Vector of calc.defs. which will be searched.
    * @param[in] arLabel
    *    Search for a calc.def. whose Name() (`e.g. vscf_0.0`) matches arLabel.
    *    If none such are found, try for at match with Prefix() (e.g. `vscf`,
    *    i.e. the `#3Name` you give on input).
    *    (If there are no Name() matches, but several Prefix() matches, it
    *    causes a MIDASERROR.)
    * @return
    *    Position of the match within arV.
    *    If arV has exactly one element, this function always returns {0,true}.
    ***************************************************************************/
   Uin FindVscf
      (  const std::vector<VscfCalcDef>& arV
      ,  const std::string& arLabel
      )
   {
      if (arV.size() == 1)
      {
         return 0;
      }
      else
      {
         Uin name_matches = 0;
         Uin name_first = 0;
         Uin prefix_matches = 0;
         Uin prefix_first = 0;

         for(Uin i = 0; i < arV.size(); ++i)
         {
            if (arLabel == arV.at(i).Name())
            {
               if (name_matches == 0)
               {
                  name_first = i;
               }
               ++name_matches;
            }
            if (arLabel == arV.at(i).Prefix())
            {
               if (prefix_matches == 0)
               {
                  prefix_first = i;
               }
               ++prefix_matches;
            }
         }

         if (name_matches == 1)
         {
            return name_first;
         }
         else if (prefix_matches == 1)
         {
            return prefix_first;
         }
         else
         {
            bool not_unique = name_matches > 1 || prefix_matches > 1;
            std::stringstream ss;
            ss << "Searched for VSCF with name/prefix '" << arLabel << "', but\n"
               << "couldn't find a " << (not_unique? "unique ": "") << "match.\n"
               << "VSCF prefixes and names are:\n"
               ;
            for(const auto& calcdef: arV)
            {
               ss << "   " << std::left << std::setw(30) << calcdef.Prefix() << std::setw(30) << calcdef.Name() << '\n';
            }
            MIDASERROR(ss.str());
            return 0;
         }
      }
   }

   /************************************************************************//**
    * @note
    *    If no unique match can be found, this function causes a MIDASERROR.
    *
    * @param[in] arV
    *    Vector of calc.defs. which will be searched.
    * @param[in] arLabel
    *    Search for a calc.def. whose Prefix() matches arLabel.
    *    (Prefix() is what you give under `#3Name` in input.
    *    Name() has the extra _0.0_EXC_2_ISO_... stuff, which I don't dare
    *    fiddle with at the moment. -MBH, May 2019.)
    * @return
    *    Position of the match within arV.
    *    If arV has exactly one element, this function always returns {0,true}.
    ***************************************************************************/
   Uin FindVcc
      (  const std::vector<VccCalcDef>& arV
      ,  const std::string& arLabel
      )
   {
      if (arV.size() == 1)
      {
         return 0;
      }
      else
      {
         Uin prefix_matches = 0;
         Uin prefix_first = 0;

         for(Uin i = 0; i < arV.size(); ++i)
         {
            if (arLabel == arV.at(i).Prefix())
            {
               if (prefix_matches == 0)
               {
                  prefix_first = i;
               }
               ++prefix_matches;
            }
         }

         if (prefix_matches == 1)
         {
            return prefix_first;
         }
         else
         {
            bool not_unique = prefix_matches > 1;
            std::stringstream ss;
            ss << "Searched for VCC with prefix '" << arLabel << "', but\n"
               << "couldn't find a " << (not_unique? "unique ": "") << "match.\n"
               << "VCC prefixes and names are:\n"
               ;
            for(const auto& calcdef: arV)
            {
               ss << "   " << std::left << std::setw(30) << calcdef.Prefix() << std::setw(30) << calcdef.Name() << '\n';
            }
            MIDASERROR(ss.str());
            return 0;
         }
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   Uin FindOper(const GlobalOperatorDefinitions& arGlobOps, const std::string& arName)
   {
      Uin index = 0;
      bool found = false;
      for(Uin i = 0; i < arGlobOps.GetNrOfOpers(); ++i)
      {
         if (arGlobOps[i].Name() == arName)
         {
            found = true;
            index = i;
         }
      }
      if (!found)
      {
         std::stringstream ss;
         ss << "Tried to find operator '" << arName << "', but failed.\n"
            << "I looked through " << arGlobOps.GetNrOfOpers() << " operators, with names:\n"
            ;
         for(Uin i = 0; i < arGlobOps.GetNrOfOpers(); ++i)
         {
            ss << "   " << arGlobOps[i].Name() << '\n';
         }
         MIDASERROR(ss.str());
      }
      return index;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   Uin FindBasis(const std::vector<BasDef>& arV, const std::string& arName)
   {
      Uin index = 0;
      bool found = false;
      for(Uin i = 0; i < arV.size(); ++i)
      {
         if (arV[i].GetmBasName() == arName)
         {
            found = true;
            index = i;
         }
      }
      if (!found)
      {
         std::stringstream ss;
         ss << "Tried to find basis '" << arName << "', but failed.\n"
            << "I looked through " << arV.size() << " bases, with names:\n"
            ;
         for(Uin i = 0; i < arV.size(); ++i)
         {
            ss << "   " << arV[i].GetmBasName() << '\n';
         }
         MIDASERROR(ss.str());
      }
      return index;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   std::pair<std::vector<Uin>,Uin> GetModalOffsets
      (  const OpDef& arOpDef
      ,  const BasDef& arBasDef
      )
   {
      const Uin nmodes = arBasDef.GetmNoModesInBas();
      std::vector<Uin> v_offsets;
      v_offsets.reserve(nmodes);
      Uin offset = 0;
      for(LocalModeNr m = 0; m < nmodes; ++m)
      {
         v_offsets.emplace_back(offset);
         GlobalModeNr m_glob = arOpDef.GetGlobalModeNr(m);
         LocalModeNr m_bas = arBasDef.GetLocalModeNr(m_glob);
         Uin nbas = arBasDef.Nbas(m_bas);
         offset += nbas*nbas;
      }
      return std::make_pair(v_offsets, offset);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   GeneralDataCont<Nb> GetFromDisk
      (  Uin aSize
      ,  const std::string& arFileName
      )
   {
      if (!midas::filesystem::IsFile(arFileName+"_0"))
      {
         MIDASERROR("Did not find file '"+arFileName+"_0"+"'.");
      }
      GeneralDataCont<Nb> dc_modals;
      dc_modals.GetFromExistingOnDisc(aSize, arFileName);
      dc_modals.SaveUponDecon(true);
      return dc_modals;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   ModalIntegrals<Nb> GetModalIntegralsReal
      (  const OpDef& arOpDef
      ,  const BasDef& arBasDef
      ,  const std::vector<Uin>& arNModals
      ,  const std::string& arFileName
      )
   {
      const auto [offsets, size] = GetModalOffsets(arOpDef, arBasDef);
      const auto modals = GetFromDisk(size, arFileName);
      OneModeInt omi(&arOpDef, &arBasDef, "InMem", false);
      omi.CalcInt();
      midas::td::PrimitiveIntegrals<Nb> pi(omi, modals, std::vector<In>(offsets.begin(),offsets.end()), std::vector<In>(arNModals.begin(),arNModals.end()));
      return midas::vcc::modalintegrals::ConstructModalIntegrals(std::move(pi));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   ModalIntegrals<std::complex<Nb>> GetModalIntegrals
      (  const OpDef& arOpDef
      ,  const BasDef& arBasDef
      ,  const std::vector<Uin>& arNModals
      ,  const std::string& arFileName
      )
   {
      return midas::vcc::modalintegrals::detail::ConvertFromReal<std::complex<Nb>>::Convert(GetModalIntegralsReal(arOpDef, arBasDef, arNModals, arFileName));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   std::map<ParamID,GeneralMidasVector<Nb>> LoadVccGs
      (  InitType aInit
      ,  CorrType aCorr
      ,  Uin aSizeParams
      ,  const std::string& arVccBaseName
      )
   {
      Uin file_size = 0;
      std::map<ParamID,GeneralMidasVector<Nb>> m;
      if (aInit == InitType::VCCGS)
      {
         std::map<ParamID,std::string> loader_map;
         if (aCorr == CorrType::VCC)
         {
            MidasAssert(aSizeParams > 0, "I always expect at least one parameter {}.");
            file_size = aSizeParams - 1;
            loader_map[ParamID::KET] = arVccBaseName+"_Vcc_vec_0";
            loader_map[ParamID::BRA] = arVccBaseName+"_mul0_vec_0";
         }
         else if(aCorr == CorrType::EXTVCC)
         {
            file_size = aSizeParams;
            std::vector<std::string> descr = midas::vcc::subspacesolver::ExtVccSolver<Nb>::GetFileDescriptors();
            loader_map[ParamID::KET] = arVccBaseName + "_" + descr.at(0);
            loader_map[ParamID::BRA] = arVccBaseName + "_" + descr.at(1);
         }
         else
         {
            MIDASERROR("Implement me, CorrType::"+StringFromEnum(aCorr)+".");
            file_size = aSizeParams;
            loader_map[ParamID::KET] = arVccBaseName+"_Vci_vec_0";
         }
         for(const auto& kv: loader_map)
         {
            // Read in data from disk to DataCont.
            // Depending no method (VCC/VCI), set first elements of MidasVector
            // to 0, then load in the DataCont to rest.
            const auto dc = GetFromDisk(file_size, kv.second);
            auto& mv = m[kv.first];
            mv.SetNewSize(aSizeParams);
            Uin addr = mv.Size() - dc.Size();
            for(Uin i = 0; i < addr; ++i)
            {
               mv[i] = 0;
            }
            dc.DataIo(IO_GET, mv, dc.Size(), 0, 1, addr);
         }
      }
      else
      {
         MIDASERROR("Called with InitType '"+StringFromEnum(aInit)+"'; don't know what to do.");
      }
      return m;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   std::map<ParamID,GeneralMidasVector<Nb>> PrepInitState
      (  InitType aInit
      ,  CorrType aCorr
      ,  Uin aSizeParams
      ,  const std::string& arVccBaseName
      )
   {
      std::map<ParamID,GeneralMidasVector<Nb>> m;
      switch(aInit)
      {
         case InitType::VSCFREF:
         {
            m[ParamID::KET] = GeneralMidasVector<Nb>(aSizeParams, Nb(0));
            if (  aCorr == CorrType::VCC
               || aCorr == CorrType::EXTVCC
               )
            {
               m[ParamID::BRA] = GeneralMidasVector<Nb>(aSizeParams, Nb(0));
            }
            else if (aCorr == CorrType::VCI)
            {
               m[ParamID::KET][0] = 1;
            }
            break;
         }
         case InitType::VCCGS:
         {
            m = LoadVccGs(aInit, aCorr, aSizeParams, arVccBaseName);
            break;
         }
         default:
         {
            MIDASERROR("Called with InitType '"+StringFromEnum(aInit)+"'; don't know what to do.");
            break;
         }
      }
      return m;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   void PostFactorySettings
      (  TimTdvccIfc& arObj
      ,  const TimTdvccCalcDef& arCalcDef
      ,  const VccCalcDef* const apVccCalcDef
      )
   {
      // General settings.
      arObj.Name() = arCalcDef.Name();
      arObj.IoLevel() = arCalcDef.IoLevel();
      arObj.TimeIt() = arCalcDef.TimeIt();
      arObj.SaveFvciVecs() = arCalcDef.CompareFvciVecs();
      arObj.ImagTimeHaultThr() = arCalcDef.ImagTimeHaultThr();
      arObj.PrintoutInterval() = arCalcDef.PrintoutInterval();

      // Initial state.
      arObj.SetInitState
         (  PrepInitState
               (  arCalcDef.InitState()
               ,  arCalcDef.CorrMethod()
               ,  arObj.SizeParams()
               ,  (apVccCalcDef? apVccCalcDef->Name(): "")
               )
         );

      // Load VCC ground state if needed.
      if (apVccCalcDef && arCalcDef.LoadVccGs())
      {
         arObj.SetVccGroundState
            (  LoadVccGs
                  (  InitType::VCCGS
                  ,  arCalcDef.CorrMethod()
                  ,  arObj.SizeParams()
                  ,  (apVccCalcDef? apVccCalcDef->Name(): "")
                  )
            );
      }

      // Propagation details.
      if (arCalcDef.ImagTime())
      {
         arObj.EnableImagTime();
      }
      arObj.SetOdeInfo(arCalcDef.GetOdeInfo());

      // Properties and statistics.
      for(const auto& val: arCalcDef.Properties())
      {
         arObj.EnableTracking(EnumFromString<PropID>(val));
      }
      for(const auto& val: arCalcDef.Stats())
      {
         arObj.EnableTracking(EnumFromString<StatID>(val));
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   void EnableExpVals
      (  TimTdvccIfc& arObj
      ,  const GlobalOperatorDefinitions& arGlobOpDefs
      ,  const std::set<std::string>& arOperNames
      ,  const BasDef& arBasDef
      ,  const std::vector<Uin>& arNModals
      ,  const std::string& arVscfModalsName
      )
   {
      arObj.ExptValReserve(arOperNames.size());
      for(const auto& oper: arOperNames)
      {
         auto i_oper = FindOper(arGlobOpDefs, oper);
         const auto& op = arGlobOpDefs[i_oper];
         auto modints = GetModalIntegralsReal(op, arBasDef, arNModals, arVscfModalsName);
         arObj.EnableExptVal(&op, std::move(modints));
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   std::tuple
      <  bool
      ,  std::vector<Nb>
      ,  std::vector<std::vector<Nb>>
      ,  std::vector<std::string>
      >
   CrossCompareFvci
      (  const std::vector<std::vector<Nb>>& arVecTimes
      ,  const std::vector<std::vector<GeneralMidasVector<std::complex<Nb>>>> arVecFvcis
      )
   {
      std::stringstream err;

      // Assertions.
      if (arVecTimes.size() != arVecFvcis.size())
      {
         err<< "arVecTimes.size() (which is " << arVecTimes.size()
            << ") != arVecFvcis.size() (which is " << arVecFvcis.size()
            << ")."
            ;
         MIDASERROR(err.str());
      }

      const Uin n_calcs = arVecTimes.size();

      try
      {
         if (n_calcs < 2)
         {
            MidasWarning("CrossCompareFvci; Only "+std::to_string(n_calcs)+" calcs.; nothing to do.");
            throw int(-1);
         }

         const Uin n_ts = arVecTimes.front().size();
         auto v_ts = arVecTimes.front();
         // Assertions.
         for(Uin i = 0; i < n_calcs; ++i)
         {
            if (arVecFvcis.at(i).size() != n_ts)
            {
               err<< "arVecFvcis.at(i).size() (which is " << arVecFvcis.at(i).size()
                  << ") != n_ts (which is " << n_ts
                  << "), i = " << i << "."
                  ;
               MidasWarning("CrossCompareFvci; "+err.str());
               throw int(-1);
            }
            if (arVecTimes.at(i).size() != n_ts)
            {
               err<< "arVecTimes.at(i).size() (which is << " << arVecTimes.at(i).size()
                  << ") != n_ts (which is " << n_ts
                  << "), i = " << i << "."
                  ;
               MidasWarning("CrossCompareFvci; "+err.str());
               throw int(-1);
            }
            if (arVecTimes.at(i) != v_ts)
            {
               err<< "time vectors unequal, i = " << i << '\n'
                  << "arVecTimes.at(i) = " << arVecTimes.at(i) << '\n'
                  << "v_ts             = " << v_ts << '\n'
                  ;
               MidasWarning("CrossCompareFvci;\n"+err.str());
               throw int(-1);
            }
         }

         // Now comes the calculations.
         const auto& v_ref = arVecFvcis.front();
         std::vector<std::vector<Nb>> v_data(4*(n_calcs - 1));
         std::vector<std::string> v_headers = {"t"};

         for(Uin calc = 1; calc < n_calcs; ++calc)
         {
            const std::string calc_str = std::to_string(calc);
            v_headers.emplace_back("angle(r,v)_"+calc_str);
            v_headers.emplace_back("Re[<r|v>/|r||v|]_"+calc_str);
            v_headers.emplace_back("Im[<r|v>/|r||v|]_"+calc_str);
            v_headers.emplace_back("|v-r|_"+calc_str);
            auto& v_angle              = v_data.at((calc-1)*4 + 0);
            auto& v_cauchy_schwarz_re  = v_data.at((calc-1)*4 + 1);
            auto& v_cauchy_schwarz_im  = v_data.at((calc-1)*4 + 2);
            auto& v_diffnorm           = v_data.at((calc-1)*4 + 3);

            const auto& v_fvcis = arVecFvcis.at(calc);
            for(Uin i = 0; i < n_ts; ++i)
            {
               const auto& ref = v_ref.at(i);
               const auto& v = v_fvcis.at(i);
               const std::complex<Nb> cs = Dot(ref,v)/(Norm(ref)*Norm(v));
               const Nb angle = VectorAngle(ref,v);
               const Nb dn = sqrt(DiffNorm2(ref,v));
               v_angle.emplace_back(angle);
               v_cauchy_schwarz_re.emplace_back(std::real(cs));
               v_cauchy_schwarz_im.emplace_back(std::imag(cs));
               v_diffnorm.emplace_back(dn);
            }
         }

         for(Uin i = 0; i < v_data.size(); ++i)
         {
            MidasAssert(v_data.at(i).size() == v_ts.size(), "Wrong v_data.at(i).size().");
         }
         MidasAssert(v_headers.size() == v_data.size() + 1, "Wrong v_headers.size().");

         return std::make_tuple(true, std::move(v_ts), std::move(v_data), std::move(v_headers));
      }
      catch(const int&)
      {
         MidasWarning("Aborting CrossCompareFvci(...) (see reason above).");
         return std::make_tuple(false, std::vector<Nb>{}, std::vector<std::vector<Nb>>{}, std::vector<std::string>{""});
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   std::vector<std::pair<Nb,GeneralMidasVector<Nb>>> DiagHermPartVccGs
      (  const std::vector<Uin>& arNModals
      ,  const OpDef& arOpDef
      ,  const ModalIntegrals<Nb>& arModInts
      ,  const ModeCombiOpRange& arMcr
      ,  const GeneralMidasVector<Nb>& arTAmpls
      ,  const Uin aNumAnalyze
      )
   {
      using namespace midas::util::matrep;
      constexpr Uin max_dirprod_size = 4096;
      const Uin dirprod_size = Product(arNModals);
      std::vector<std::pair<Nb,GeneralMidasVector<Nb>>> v_result;
      if (dirprod_size > max_dirprod_size)
      {
         std::stringstream ss;
         ss << "arNModals = " << arNModals << ", giving a total matrix size = " 
            << dirprod_size << " > " << max_dirprod_size 
            << ", which is the allowed maximum due to efficiency reasons. "
            << "Returning."
            ;
         MidasWarning(ss.str());
         return v_result;
      }

      GeneralMidasMatrix<Nb> m_jac = ExplVccJac<Nb>
         (  arNModals
         ,  arOpDef
         ,  arModInts
         ,  arMcr
         ,  OrganizeMcrSpaceVec(arNModals, arMcr, arTAmpls)
         ,  true
         );
      m_jac.Symmetrize();
      auto sol_struct = SYEVD(m_jac, 'V'); // 'V': also eigenvectors.
      const Uin jac_size = m_jac.Nrows();
      GeneralMidasVector<Nb> v_eigvals(jac_size);
      LoadEigenvalues(sol_struct, v_eigvals);
      LoadEigenvectors(sol_struct, m_jac);

      for(Uin i = 0; i < jac_size && i < aNumAnalyze; ++i)
      {
         GeneralMidasVector<Nb> v_tmp(jac_size);
         m_jac.GetCol(v_tmp, i);
         v_result.emplace_back(v_eigvals[i], std::move(v_tmp));
      }

      return v_result;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   void PrintEigenpairAnalysis
      (  std::ostream& arOs
      ,  const std::vector<std::pair<Nb,GeneralMidasVector<Nb>>>& arVecEigPairs
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      const auto old_flags = arOs.flags();
      arOs << std::left;
      arOs << std::setprecision(16);
      arOs << std::scientific;
      const Uin w = 24;

      VccStateSpace vss(arMcr, std::vector<In>(arNModals.begin(), arNModals.end()), true);

      auto find_max_abs = [](const GeneralMidasVector<Nb>& v) -> std::pair<Uin,Nb>
      {
         std::pair<Uin,Nb> p = {0, 0.0};
         for(Uin i = 0; i < v.Size(); ++i)
         {
            if (midas::util::AbsVal(v[i]) > midas::util::AbsVal(p.second))
            {
               p = {i, v[i]};
            }
         }
         return p;
      };


      arOs
         << std::setw(w) << "eigenvalue" << ' '
         << std::setw(w) << "max|parameter|" << ' '
         << std::setw(w) << "mode combination" << ' '
         << std::setw(w) << "mode combi. indices" << ' '
         << std::endl;
      for(const auto& eigpair: arVecEigPairs)
      {
         const auto p = find_max_abs(eigpair.second);
         const Uin mc_refnum = vss.LocateAddress(p.first);
         auto it = vss.begin();
         std::advance(it, mc_refnum);

         std::stringstream ss_mc;
         std::stringstream ss_index;
         ss_mc << it->MC().MCVec();
         ss_index << midas::util::matrep::ShiftVals(it->GetIndexVecFromAddress(p.first),+1);

         arOs
            << std::setw(w) << std::right << eigpair.first << ' '
            << std::setw(w) << std::right << p.second << ' '
            << std::setw(w) << std::right << ss_mc.str() << ' '
            << std::setw(w) << std::right << ss_index.str() << ' '
            << std::endl;
      }

      arOs.flags(old_flags);
   }

} /* namespace midas::tdvcc */
