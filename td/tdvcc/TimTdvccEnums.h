/**
 *******************************************************************************
 * 
 * @file    TimTdvccEnums.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TIMTDVCCENUMS_H_INCLUDED
#define TIMTDVCCENUMS_H_INCLUDED

#include <map>
#include <string>

#include "util/Error.h"
#include "util/UnderlyingType.h"
#include "util/type_traits/TypeName.h"

namespace midas::tdvcc
{
   template<class ENUM>
   const std::map<std::string,ENUM>& GetMapStringToEnum()
   {
      static_assert(!std::is_same_v<ENUM,ENUM>, "Can only call specializations of this.");
      static std::map<std::string,ENUM> m;
      return m;
   }

   template<class ENUM>
   ENUM EnumFromString(const std::string& s)
   {
      try
      {
         return GetMapStringToEnum<ENUM>().at(s);
      }
      catch(const std::out_of_range& oor)
      {
         MIDASERROR("Key string '"+s+"' not found for "+midas::type_traits::TypeName<ENUM>()+" enum.");
         return ENUM();
      }
   }

   template<class ENUM>
   std::string StringFromEnum(ENUM e)
   {
      for(const auto& kv: GetMapStringToEnum<ENUM>())
      {
         if (kv.second == e)
         {
            return kv.first;
         }
      }
      MIDASERROR(midas::type_traits::TypeName<ENUM>()+" enum value '"+std::to_string(midas::util::ToUType(e))+"' not found.");
      return "";
   }

   //@{
   //! Initial state types.
   enum class InitType
   {  VSCFREF
   ,  VCCGS
   };
   template<> inline const std::map<std::string,InitType>& GetMapStringToEnum()
   {
      static std::map<std::string,InitType> m;
      m["VSCFREF"] = InitType::VSCFREF;
      m["VCCGS"]   = InitType::VCCGS;
      return m;
   }
   //@}

   //@{
   //! Transformer types.
   enum class TrfType
   {  MATREP
   ,  VCC2H2
   };
   template<> inline const std::map<std::string,TrfType>& GetMapStringToEnum()
   {
      static std::map<std::string,TrfType> m;
      m["MATREP"] = TrfType::MATREP;
      m["VCC2H2"] = TrfType::VCC2H2;
      return m;
   }
   //@}

   //@{
   //! Correlation type.
   enum class CorrType
   {  VCC
   ,  VCI
   ,  EXTVCC
   };
   template<> inline const std::map<std::string,CorrType>& GetMapStringToEnum()
   {
      static std::map<std::string,CorrType> m;
      m["VCC"] = CorrType::VCC;
      m["VCI"] = CorrType::VCI;
      m["EXTVCC"] = CorrType::EXTVCC;
      return m;
   }
   //@}

   //@{
   //! Identifiers for ket, bra, phase parameters.
   enum class ParamID
   {  PHASE
   ,  KET
   ,  BRA
   };
   template<> inline const std::map<std::string,ParamID>& GetMapStringToEnum()
   {
      static std::map<std::string,ParamID> m;
      m["PHASE"] = ParamID::PHASE;
      m["KET"] = ParamID::KET;
      m["BRA"] = ParamID::BRA;
      return m;
   }
   //@}

   //@{
   /************************************************************************//**
    * @brief
    *    Properties for monitoring.
    *
    * -  ENERGY: energy exp. val. of the Hamiltonian driving the dynamics.
    * -  PHASE: phase/norm-factor (if applicable to ansatz)
    * -  AUTOCORR: autocorrelation function w.r.t. t0; three "types" are
    *    calculated,
    *    -  `<Psi'(t0)|Psi(t)>`
    *    -  `<Psi'(t)|Psi(t0)>^*`
    *    -  and their average
    *    Note that the three are identical for variational (VCI-like)
    *    parameterizations.
    * -  AUTOCORR_EXT: (expert option!)
    *    -  For VCC: calculates autocorr. funcs. (as for AUTOCORR),
    *    by converting L and T amplitudes to EXTVCC parameterization, i.e.
    *    `<Psi'(t)| = <Phi|exp(L)exp(-T)`.
    *    -  For EXTVCC: calculates autocorr. funcs. (as for AUTOCORR),
    *    by converting Sigma and T amplitudes to VCC parameterization, i.e.
    *    `<Psi'(t)| = <Phi|(1 + Sigma)exp(-T)`.
    *    -  For others, just calculates the same quantities as AUTOCORR.
    *
    ***************************************************************************/
   //! Properties for monitoring.
   enum class PropID
   {  ENERGY
   ,  PHASE
   ,  AUTOCORR
   ,  AUTOCORR_EXT
   };
   template<> inline const std::map<std::string,PropID>& GetMapStringToEnum()
   {
      static std::map<std::string,PropID> m;
      m["ENERGY"] = PropID::ENERGY;
      m["PHASE"] = PropID::PHASE;
      m["AUTOCORR"] = PropID::AUTOCORR;
      m["AUTOCORR_EXT"] = PropID::AUTOCORR_EXT;
      return m;
   }
   //@}

   //@{
   //! Properties for monitoring.
   enum class StatID
   {  FVCINORM2
   ,  NORM2
   ,  DNORM2INIT
   ,  DNORM2VCCGS
   };
   template<> inline const std::map<std::string,StatID>& GetMapStringToEnum()
   {
      static std::map<std::string,StatID> m;
      m["FVCINORM2"] = StatID::FVCINORM2;
      m["NORM2"] = StatID::NORM2;
      m["DNORM2INIT"] = StatID::DNORM2INIT;
      m["DNORM2VCCGS"] = StatID::DNORM2VCCGS;
      return m;
   }
   //@}

} /* namespace midas::tdvcc */

template<> struct midas::type_traits::detail::TypeName<midas::tdvcc::InitType> {std::string operator()() const {return "midas::tdvcc::InitType";}};
template<> struct midas::type_traits::detail::TypeName<midas::tdvcc::TrfType> {std::string operator()() const {return "midas::tdvcc::TrfType";}};
template<> struct midas::type_traits::detail::TypeName<midas::tdvcc::CorrType> {std::string operator()() const {return "midas::tdvcc::CorrType";}};
template<> struct midas::type_traits::detail::TypeName<midas::tdvcc::ParamID> {std::string operator()() const {return "midas::tdvcc::ParamID";}};
template<> struct midas::type_traits::detail::TypeName<midas::tdvcc::PropID> {std::string operator()() const {return "midas::tdvcc::PropID";}};
template<> struct midas::type_traits::detail::TypeName<midas::tdvcc::StatID> {std::string operator()() const {return "midas::tdvcc::StatID";}};

#endif /* TIMTDVCCENUMS_H_INCLUDED */
