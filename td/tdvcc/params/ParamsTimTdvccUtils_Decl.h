/**
 *******************************************************************************
 * 
 * @file    ParamsTimTdvccUtils_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTIMTDVCCUTILS_DECL_H_INCLUDED
#define PARAMSTIMTDVCCUTILS_DECL_H_INCLUDED

// Standard headers.
#include<map>

// Midas headers.
#include "td/tdvcc/TimTdvccEnums.h"
#include "mmv/MidasVector.h"

// Forward declarations.

namespace midas::tdvcc
{
   //!
   template<typename PARAM_T, template<typename> class CONT_TMPL, bool FATAL = true>
   CONT_TMPL<PARAM_T> ExtractFromMap(std::map<ParamID,GeneralMidasVector<Nb>>&&, ParamID);

   //! Zero element of the container, and return previous value.
   template<typename PARAM_T, template<typename> class CONT_TMPL>
   PARAM_T ZeroIthElemAndReturnIt(CONT_TMPL<PARAM_T>&, Uin);

} /* namespace midas::tdvcc */

#endif/*PARAMSTIMTDVCCUTILS_DECL_H_INCLUDED*/
