/**
 *******************************************************************************
 * 
 * @file    ParamsTimTdvccUtils.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTIMTDVCCUTILS_H_INCLUDED
#define PARAMSTIMTDVCCUTILS_H_INCLUDED

#include "td/tdvcc/params/ParamsTimTdvccUtils_Decl.h"
#include "td/tdvcc/params/ParamsTimTdvccUtils_Impl.h"

#endif/*PARAMSTIMTDVCCUTILS_H_INCLUDED*/
