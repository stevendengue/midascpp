/**
 *******************************************************************************
 * 
 * @file    ParamsTimTdvci.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/params/ParamsTimTdvci.h"
#include "td/tdvcc/params/ParamsTimTdvci_Impl.h"

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"

// Define instatiation macro.
#define INSTANTIATE_PARAMSTIMTDVCI(PARAM_T, CONT_TMPL, ABSVAL_T) \
   namespace midas::tdvcc \
   { \
      template class ParamsTimTdvci<PARAM_T, CONT_TMPL>; \
   } /* namespace midas::tdvcc */ \
   template std::ostream& operator<<(std::ostream&, const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>&); \
   template void Scale(midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>&, PARAM_T); \
   template void Zero(midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>&); \
   template void Axpy(midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>&, PARAM_T); \
   template void SetShape(midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>&); \
   template typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t Norm2(midas::tdvcc::ParamID, const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>&); \
   template typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t Norm2(const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>&); \
   template typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t Norm(midas::tdvcc::ParamID, const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>&); \
   template typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t Norm(const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>&); \
   template typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t DiffNorm2(midas::tdvcc::ParamID, const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>&); \
   template typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t DiffNorm2(const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>&); \
   template Uin Size(const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>&); \
   template typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t OdeMeanNorm2 \
      (  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& \
      ,  typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t \
      ,  typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t \
      ,  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& \
      ,  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& \
      ); \
   template typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t OdeMaxNorm2 \
      (  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& \
      ,  typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t \
      ,  typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t \
      ,  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& \
      ,  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& \
      ); \
   template void DataToPointer \
      (  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& \
      ,  typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t* const \
      ); \
   template void DataFromPointer \
      (  midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& \
      ,  const typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t* const \
      ); \

   

// Instantiations.
INSTANTIATE_PARAMSTIMTDVCI(Nb, GeneralMidasVector, Nb);
INSTANTIATE_PARAMSTIMTDVCI(Nb, GeneralDataCont, Nb);
INSTANTIATE_PARAMSTIMTDVCI(std::complex<Nb>, GeneralMidasVector, Nb);
INSTANTIATE_PARAMSTIMTDVCI(std::complex<Nb>, GeneralDataCont, Nb);

#undef INSTANTIATE_PARAMSTIMTDVCI
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
