/**
 *******************************************************************************
 * 
 * @file    ParamsTimTdvccBase_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTIMTDVCCBASE_DECL_H_INCLUDED
#define PARAMSTIMTDVCCBASE_DECL_H_INCLUDED

// Standard headers.
#include <type_traits>

// Midas headers.
#include "util/type_traits/Complex.h"
#include "mmv/DataCont.h"
#include "td/tdvcc/TimTdvccEnums.h"

// Forward declarations.

namespace midas::tdvcc
{
   /************************************************************************//**
    * @brief
    *    Holds the parameters \f$ \epsilon \f$ (phase/energy), \f$ \{s_\mu\},
    *    \{l_\mu\} \f$, relevant for a TIM-TDVCC and related parametrizations.
    *
    * @note
    *    If CONT_TMPL is GeneralDataCont, this class will always
    *    SetSameLabelWhenCopying(true).
    *
    * @note
    *    Tried to use primal/dual params nomenclature in this general base
    *    class.
    *    In the context of (traditional) TDVCC,
    *    - primal: cluster amplitudes
    *    - dual: lambda coefficients
    *
    * Template parameters:
    *    -  PARAM_T: the parameter type (usually complex)
    *    -  CONT_TMPL: vectorized container, e.g. GeneralMidasVector
    *
    * CONT_TMPL must support:
    *    -  Size(CONT_TMPL<PARAM_T>)
    *    -  Scale(CONT_TMPL<PARAM_T>, PARAM_T)
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   class ParamsTimTdvccBase
   {
      public:
         using param_t = PARAM_T;
         using absval_t = midas::type_traits::RealTypeT<param_t>;
         template<typename U> using cont_tmpl = CONT_TMPL<U>;
         using cont_t = cont_tmpl<param_t>;
         static inline constexpr Uin num_param_conts = 2;
         static inline constexpr Uin num_phases = 1;

         static_assert(std::is_floating_point_v<absval_t>, "absval_t must be floating-point.");

         ParamsTimTdvccBase(const ParamsTimTdvccBase&) = default;
         ParamsTimTdvccBase(ParamsTimTdvccBase&&) = default;
         ParamsTimTdvccBase& operator=(const ParamsTimTdvccBase&) = default;
         ParamsTimTdvccBase& operator=(ParamsTimTdvccBase&&) = default;
         ~ParamsTimTdvccBase() = default;

         //! Phase = 0 and no primal/dual params.
         ParamsTimTdvccBase() = default;

         //! Phase = 0 and given number of primal/dual params, all zeroed.
         ParamsTimTdvccBase(Uin aSize);

         //! Construct with given amps./coefs. (of same size) and phase.
         ParamsTimTdvccBase(cont_t aAmps, cont_t aCoefs, param_t aPhase = 0);

         //! Construct from map with keys "KET", "BRA", and possibly "PHASE".
         ParamsTimTdvccBase(std::map<ParamID,GeneralMidasVector<Nb>>&&);

         /******************************************************************//**
          * @name Queries
          *********************************************************************/
         //!@{
         //! Combined size of T-amps., L-coefs. and phase.
         Uin Size() const;

         //! Size of each of the T-amps. and L-coefs. (which have same size).
         Uin SizeParams() const;

         //! How to compute total size given size of one parameter container.
         static Uin SizeTotal(Uin aSizeParams);

         //! The ParamIDs for containers within such an object.
         static const std::set<ParamID>& GetParamIDs();

         //!@}

         /******************************************************************//**
          * @name Data extraction
          *********************************************************************/
         //!@{
         //! The stored phase.
         param_t Phase() const;

         //@{
         //! The stored parameters.
         const cont_t& PrimalParams() const;
         const cont_t& DualParams() const;
         //@}

         //!@}

         /******************************************************************//**
          * @name Modifications
          *********************************************************************/
         //!@{
         //! Scale all parameters (phase, T-amps., L-coefs.) with same factor.
         void Scale(param_t aFactor);

         //! Zero all parameters.
         void Zero();

         //! Axpy for all parameters, i.e. Y = a*X + Y, i.e. *this += a*X.
         void Axpy(const ParamsTimTdvccBase&, param_t);

         //!@}

      private:
         param_t mPhase = 0;

         //@{
         //! Primal/dual params.
         cont_t mPrimalParams;
         cont_t mDualParams;
         //@}

         //!
         void SanityCheck();
   };

} /* namespace midas::tdvcc */

/***************************************************************************//**
 * @name Interface wrappers for ParamsTimTdvccBase
 ******************************************************************************/
//!@{
template<typename PARAM_T, template<typename> class CONT_TMPL>
std::ostream& operator<<(std::ostream&, const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL, typename U>
inline void Scale
   (  midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>& arParams
   ,  U arAlpha
   )
{
   Scale(arParams, PARAM_T(arAlpha));
}

template<typename PARAM_T, template<typename> class CONT_TMPL>
void Scale(midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>&, PARAM_T);

template<typename PARAM_T, template<typename> class CONT_TMPL>
void Zero(midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL, typename U>
inline void Axpy
   (  midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>& arY
   ,  const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>& arX
   ,  U arAlpha
   )
{
   Axpy(arY, arX, PARAM_T(arAlpha));
}

template<typename PARAM_T, template<typename> class CONT_TMPL>
void Axpy(midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>&, PARAM_T);

template<typename PARAM_T, template<typename> class CONT_TMPL>
void SetShape(midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>::absval_t Norm2(midas::tdvcc::ParamID, const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>::absval_t Norm2(const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>::absval_t Norm(midas::tdvcc::ParamID, const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>::absval_t Norm(const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>::absval_t DiffNorm2(midas::tdvcc::ParamID, const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
typename midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>::absval_t DiffNorm2(const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>&, const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>&);

template<typename PARAM_T, template<typename> class CONT_TMPL>
Uin Size(const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>&);

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>::absval_t OdeMeanNorm2
   (  const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>& aDeltaY
   ,  typename midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>::absval_t aAbsTol
   ,  typename midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>::absval_t aRelTol
   ,  const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>& aYOld
   ,  const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>& aYNew
   );

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>::absval_t OdeMaxNorm2
   (  const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>& aDeltaY
   ,  typename midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>::absval_t aAbsTol
   ,  typename midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>::absval_t aRelTol
   ,  const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>& aYOld
   ,  const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>& aYNew
   );

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void DataToPointer
   (  const midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>& arA
   ,  typename midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>::absval_t* const apPtr
   );

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void DataFromPointer
   (  midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>& arA
   ,  const typename midas::tdvcc::ParamsTimTdvccBase<PARAM_T,CONT_TMPL>::absval_t* const apPtr
   );

//!@}

#endif/*PARAMSTIMTDVCCBASE_DECL_H_INCLUDED*/
