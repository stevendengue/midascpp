/**
 *******************************************************************************
 * 
 * @file    ParamsTimTdvci_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTIMTDVCI_IMPL_H_INCLUDED
#define PARAMSTIMTDVCI_IMPL_H_INCLUDED

// Midas headers.
#include "td/tdvcc/params/ParamsTimTdvccUtils.h"

namespace midas::tdvcc
{

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
ParamsTimTdvci<PARAM_T,CONT_TMPL>::ParamsTimTdvci
   (  Uin aSize
   )
   :  mCoefs(aSize, param_t(0))
{
   SanityCheck();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
ParamsTimTdvci<PARAM_T,CONT_TMPL>::ParamsTimTdvci
   (  cont_t aCoefs
   )
   :  mCoefs(std::move(aCoefs))
{
   SanityCheck();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
ParamsTimTdvci<PARAM_T,CONT_TMPL>::ParamsTimTdvci
   (  std::map<ParamID,GeneralMidasVector<Nb>>&& arMap
   )
   :  mCoefs(ExtractFromMap<PARAM_T,CONT_TMPL>(std::move(arMap), ParamID::KET))
{
   SanityCheck();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
Uin ParamsTimTdvci<PARAM_T,CONT_TMPL>::Size
   (
   )  const
{
   return ::Size(mCoefs);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
Uin ParamsTimTdvci<PARAM_T,CONT_TMPL>::SizeTotal
   (  Uin aSizeParams
   )
{
   return num_phases + num_param_conts*aSizeParams;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
const std::set<ParamID>& ParamsTimTdvci<PARAM_T,CONT_TMPL>::GetParamIDs
   (
   )
{
   static std::set<ParamID> s_param_ids =
      {  ParamID::KET
      };
   return s_param_ids;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
const typename ParamsTimTdvci<PARAM_T,CONT_TMPL>::cont_t& ParamsTimTdvci<PARAM_T,CONT_TMPL>::Coefs
   (
   )  const
{
   return mCoefs;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void ParamsTimTdvci<PARAM_T,CONT_TMPL>::Scale
   (  param_t aFactor
   )
{
   ::Scale(mCoefs, aFactor);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void ParamsTimTdvci<PARAM_T,CONT_TMPL>::Zero
   (
   )
{
   ::Zero(mCoefs);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void ParamsTimTdvci<PARAM_T,CONT_TMPL>::Axpy
   (  const ParamsTimTdvci<PARAM_T,CONT_TMPL>& arX
   ,  param_t aA
   )
{
   ::Axpy(mCoefs, arX.Coefs(), aA);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void ParamsTimTdvci<PARAM_T,CONT_TMPL>::SanityCheck
   (
   )
{
   // If GeneralDataCont, SetSameLabelWhenCopying(true).
   if constexpr(std::is_same_v<CONT_TMPL<PARAM_T>,GeneralDataCont<PARAM_T>>)
   {
      mCoefs.SetSameLabelWhenCopying(true);
   }
}

} /* namespace midas::tdvcc */


/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
std::ostream& operator<<
   (  std::ostream& arOs
   ,  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arParams
   )
{
   arOs << arParams.Coefs() << std::endl;
   return arOs;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void Scale
   (  midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arA
   ,  PARAM_T aFactor
   )
{
   arA.Scale(aFactor);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void Zero
   (  midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arA
   )
{
   arA.Zero();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void Axpy
   (  midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arY
   ,  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arX
   ,  PARAM_T aA
   )
{
   arY.Axpy(arX, aA);
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void SetShape
   (  midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arY
   ,  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arX
   )
{
   arY = midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>(Size(arX));
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t Norm2
   (  midas::tdvcc::ParamID aID
   ,  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arA
   )
{
   using namespace midas::tdvcc;
   switch(aID)
   {
      case ParamID::KET:
         return Norm2(arA.Coefs());
      default:
         return 0;
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t Norm2
   (  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arA
   )
{
   using namespace midas::tdvcc;
   typename ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t val = 0;
   for(const auto& id: arA.GetParamIDs())
   {
      val += Norm2(id, arA);
   }
   return val;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t Norm
   (  midas::tdvcc::ParamID aID
   ,  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arA
   )
{
   return sqrt(Norm2(aID, arA));
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t Norm
   (  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arA
   )
{
   return sqrt(Norm2(arA));
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t DiffNorm2
   (  midas::tdvcc::ParamID aID
   ,  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arA
   ,  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arB
   )
{
   using namespace midas::tdvcc;
   switch(aID)
   {
      case ParamID::KET:
         return DiffNorm2(arA.Coefs(), arB.Coefs());
      default:
         return 0;
   }
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t DiffNorm2
   (  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arA
   ,  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arB
   )
{
   using namespace midas::tdvcc;
   typename ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t val = 0;
   for(const auto& id: arA.GetParamIDs())
   {
      val += DiffNorm2(id, arA, arB);
   }
   return val;
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
Uin Size
   (  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arA
   )
{
   return arA.Size();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t OdeMeanNorm2
   (  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& aDeltaY
   ,  typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t aAbsTol
   ,  typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t aRelTol
   ,  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& aYOld
   ,  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& aYNew
   )
{
   return OdeMeanNorm2(aDeltaY.Coefs(), aAbsTol, aRelTol, aYOld.Coefs(), aYNew.Coefs());
}

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t OdeMaxNorm2
   (  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& aDeltaY
   ,  typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t aAbsTol
   ,  typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t aRelTol
   ,  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& aYOld
   ,  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& aYNew
   )
{
   return OdeMaxNorm2(aDeltaY.Coefs(), aAbsTol, aRelTol, aYOld.Coefs(), aYNew.Coefs());
}

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void DataToPointer
   (  const midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arA
   ,  typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t* const apPtr
   )
{
   DataToPointer(arA.Coefs(), apPtr);
}

template
   <  typename PARAM_T
   ,  template<typename> class CONT_TMPL
   >
void DataFromPointer
   (  midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>& arA
   ,  const typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::absval_t* const apPtr
   )
{
   using cont_t = typename midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>::cont_t;
   cont_t coefs(arA.Size());
   DataFromPointer(coefs, apPtr);
   arA = midas::tdvcc::ParamsTimTdvci<PARAM_T,CONT_TMPL>(std::move(coefs));
}


#endif/*PARAMSTIMTDVCI_IMPL_H_INCLUDED*/
