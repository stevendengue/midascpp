/**
 *******************************************************************************
 * 
 * @file    ParamsTimTdvcc.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/tdvcc/params/ParamsTimTdvcc.h"
#include "td/tdvcc/params/ParamsTimTdvcc_Impl.h"

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"

// Define instatiation macro.
#define INSTANTIATE_PARAMSTIMTDVCC(PARAM_T, CONT_TMPL, ABSVAL_T) \
   namespace midas::tdvcc \
   { \
      template class ParamsTimTdvcc<PARAM_T, CONT_TMPL>; \
   } /* namespace midas::tdvcc */ \


// Instantiations.
INSTANTIATE_PARAMSTIMTDVCC(Nb, GeneralMidasVector, Nb);
INSTANTIATE_PARAMSTIMTDVCC(Nb, GeneralDataCont, Nb);
INSTANTIATE_PARAMSTIMTDVCC(std::complex<Nb>, GeneralMidasVector, Nb);
INSTANTIATE_PARAMSTIMTDVCC(std::complex<Nb>, GeneralDataCont, Nb);

#undef INSTANTIATE_PARAMSTIMTDVCC
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
