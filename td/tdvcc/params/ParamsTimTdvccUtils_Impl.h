/**
 *******************************************************************************
 * 
 * @file    ParamsTimTdvccUtils_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PARAMSTIMTDVCCUTILS_IMPL_H_INCLUDED
#define PARAMSTIMTDVCCUTILS_IMPL_H_INCLUDED

// Midas headers.
#include "util/Error.h"

namespace midas::tdvcc
{

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  bool FATAL
      >
   CONT_TMPL<PARAM_T> ExtractFromMap
      (  std::map<ParamID,GeneralMidasVector<Nb>>&& arMap
      ,  ParamID aKey
      )
   {
      try
      {
         return CONT_TMPL<PARAM_T>(GeneralMidasVector<PARAM_T>(std::move(arMap.at(aKey))));
      }
      catch(const std::out_of_range& oor)
      {
         if constexpr(FATAL)
         {
            MIDASERROR("Couldn't find key '"+StringFromEnum(aKey)+"' in map of parameters.");
            return CONT_TMPL<PARAM_T>();
         }
         else
         {
            throw oor;
         }
      }
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   PARAM_T ZeroIthElemAndReturnIt
      (  CONT_TMPL<PARAM_T>& arCont
      ,  Uin arI
      )
   {
      PARAM_T val = 0;
      if (arCont.Size() < 1)
      {
         MIDASERROR("arCont.Size() = "+std::to_string(arCont.Size())+", which is < 1.");
      }
      if constexpr(std::is_same_v<CONT_TMPL<PARAM_T>,GeneralDataCont<PARAM_T>>)
      {
         arCont.DataIo(IO_GET, I_0, val);
         arCont.DataIo(IO_PUT, I_0, PARAM_T(0));
      }
      else
      {
         val = arCont[I_0];
         arCont[I_0] = 0;
      }
      return val;
   }


} /* namespace midas::tdvcc */


#endif/*PARAMSTIMTDVCCUTILS_IMPL_H_INCLUDED*/
