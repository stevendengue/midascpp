/**
 *******************************************************************************
 * 
 * @file    TimTdvccIfcFactory.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TIMTDVCCIFCFACTORY_IMPL_H_INCLUDED
#define TIMTDVCCIFCFACTORY_IMPL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "td/tdvcc/TimTdvcc.h"
#include "td/tdvcc/params/ParamsTimTdvcc.h"
#include "td/tdvcc/params/ParamsTimTdvci.h"
#include "td/tdvcc/params/ParamsTimTdextvcc.h"
#include "td/tdvcc/trf/TrfTimTdvccMatRep.h"
#include "td/tdvcc/trf/TrfTimTdvciMatRep.h"
#include "td/tdvcc/trf/TrfTimTdextvccMatRep.h"
#include "td/tdvcc/trf/TrfTimTdvcc2.h"
#include "td/tdvcc/trf/TrfTimTdvci2.h"
#include "td/tdvcc/deriv/DerivTimTdvcc.h"
#include "td/tdvcc/deriv/DerivTimTdvci.h"
#include "td/tdvcc/deriv/DerivTimTdextvcc.h"

// Forward declares.
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
//template<typename> class GeneralMidasVector;
//template<typename> class GeneralDataCont;

namespace midas::tdvcc
{

   /************************************************************************//**
    *
    ***************************************************************************/
   template<class... Args>
   std::unique_ptr<TimTdvccIfc> TimTdvccIfcFactory
      (  CorrType aCorr
      ,  TrfType aTrf
      ,  Args&&... aArgs
      )
   {
      using param_t = TimTdvccIfc::ifc_param_t;
      //template<typename T> using mv_tmpl = GeneralMidasVector<T>;
      //template<typename T> using dc_tmpl = GeneralDataCont<T>;

      // Small util. lambdas for error info.
      auto err_info_trf = [](CorrType c, TrfType t) -> std::string
         {
            std::stringstream ss;
            ss << "TrfType '" << StringFromEnum(t) << "' "
               << "unknown or not applicable for chosen CorrType '" << StringFromEnum(c) << "'."
               ;
            return ss.str();
         };

      std::unique_ptr<TimTdvccIfc> p(nullptr);
      if (aCorr == CorrType::VCC)
      {
         if (aTrf == TrfType::MATREP)
         {
            p = std::make_unique
               <  TimTdvcc
                     <  ParamsTimTdvcc<param_t, GeneralMidasVector>
                     ,  TrfTimTdvccMatRep<param_t>
                     ,  DerivTimTdvcc<param_t, GeneralMidasVector, TrfTimTdvccMatRep>
                     >
               >
               (  std::forward<Args>(aArgs)...
               );
         }
         else if (aTrf == TrfType::VCC2H2)
         {
            p = std::make_unique
               <  TimTdvcc
                     <  ParamsTimTdvcc<param_t, GeneralDataCont>
                     ,  TrfTimTdvcc2<param_t>
                     ,  DerivTimTdvcc<param_t, GeneralDataCont, TrfTimTdvcc2>
                     >
               >
               (  std::forward<Args>(aArgs)...
               );
         }
         else
         {
            MIDASERROR(err_info_trf(aCorr, aTrf));
         }
      }
      else if (aCorr == CorrType::VCI)
      {
         if (aTrf == TrfType::MATREP)
         {
            p = std::make_unique
               <  TimTdvcc
                     <  ParamsTimTdvci<param_t, GeneralMidasVector>
                     ,  TrfTimTdvciMatRep<param_t>
                     ,  DerivTimTdvci<param_t, GeneralMidasVector, TrfTimTdvciMatRep>
                     >
               >
               (  std::forward<Args>(aArgs)...
               );
         }
         else if (aTrf == TrfType::VCC2H2)
         {
            p = std::make_unique
               <  TimTdvcc
                     <  ParamsTimTdvci<param_t, GeneralDataCont>
                     ,  TrfTimTdvci2<param_t>
                     ,  DerivTimTdvci<param_t, GeneralDataCont, TrfTimTdvci2>
                     >
               >
               (  std::forward<Args>(aArgs)...
               );
         }
         else
         {
            MIDASERROR(err_info_trf(aCorr, aTrf));
         }
      }
      else if (aCorr == CorrType::EXTVCC)
      {
         if (aTrf == TrfType::MATREP)
         {
            p = std::make_unique
               <  TimTdvcc
                     <  ParamsTimTdextvcc<param_t, GeneralMidasVector>
                     ,  TrfTimTdextvccMatRep<param_t>
                     ,  DerivTimTdextvcc<param_t, GeneralMidasVector, TrfTimTdextvccMatRep>
                     >
               >
               (  std::forward<Args>(aArgs)...
               );
         }
         else
         {
            MIDASERROR(err_info_trf(aCorr, aTrf));
         }
      }
      else
      {
         MIDASERROR("Unknown CorrType: '"+StringFromEnum(aCorr)+"'.");
      }
      return p;
   }

} /* namespace midas::tdvcc */


#endif /* TIMTDVCCIFCFACTORY_IMPL_H_INCLUDED */
