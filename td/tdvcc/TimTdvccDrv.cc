/**
 *******************************************************************************
 * 
 * @file    TimTdvccDrv.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

// Standard headers.
#include <vector>
#include <string>
#include <map>
#include <algorithm>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "input/VscfCalcDef.h"
#include "input/VccCalcDef.h"
//#include "input/Input.h"
#include "input/OpDef.h"
#include "input/GlobalOperatorDefinitions.h"
#include "input/BasDef.h"
#include "input/TimTdvccCalcDef.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "td/tdvcc/TimTdvccIfc.h"
#include "td/tdvcc/TimTdvccIfcFactory.h"
#include "td/tdvcc/DriverUtils.h"
#include "td/tdvcc/TimTdvccEnums.h"
#include "input/Trim.h"


// Extern/forward declares.
extern std::vector<BasDef> gBasis;
extern GlobalOperatorDefinitions gOperatorDefs;
extern In gVibIoLevel;
extern std::vector<TimTdvccCalcDef> gTimTdvccCalcDef;
extern std::vector<VscfCalcDef> gVscfCalcDef;
extern std::vector<VccCalcDef> gVccCalcDef;
extern std::string gAnalysisDir;


namespace midas::tdvcc
{

/**
* Run TIM-TDVCC calculations
* */
void TimTdvccDrv()
{
   using step_t = TimTdvccIfc::ifc_step_t;
   using param_t = TimTdvccIfc::ifc_param_t;
   using vec_t = GeneralMidasVector<param_t>;

   Mout << "\n\n";
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Begin TIM-TDVCC calculations ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << "\n\n";

   if (  gDebug
      || gVibIoLevel > I_5
      )
   {
      Mout << " Number of TIM-TDVCC calculations to perform: " << gTimTdvccCalcDef.size() << std::endl;
   }

   // Make a map over oper/basis combinations used in the whole set of TIM-TDVCC calculations.
   std::map<std::string,In> oper_bas_map;
   for (In i_calc=gTimTdvccCalcDef.size()-1; i_calc>=0; --i_calc)
   {
      std::string oper_name  = gTimTdvccCalcDef[i_calc].Oper();
      std::string basis_name = gTimTdvccCalcDef[i_calc].Basis();
      std::string oper_bas  = oper_name + "_" + basis_name;
      oper_bas_map[oper_bas]=i_calc;
   }

   if (  gDebug
      || gVibIoLevel > I_7
      )
   {
      Mout << " Calc. nr. | oper_basis: " << std::endl;
      for(const auto& po : oper_bas_map)
      {
          Mout << "  " << po.second << " | " << po.first << std::endl;
      }
   }

   // Stuff for cross-calc. comparison.
   std::vector<std::vector<step_t>> vec_interpol_ts;
   std::vector<std::vector<vec_t>> vec_fvci_vecs_ket;
   std::vector<std::vector<vec_t>> vec_fvci_vecs_bra;

   for(In i_calc=0; i_calc<gTimTdvccCalcDef.size(); ++i_calc)
   {
      auto& calcdef = gTimTdvccCalcDef[i_calc];
      // Set IoLevel to be gt gVibIoLevel
      calcdef.SetIoLevel(std::max(calcdef.IoLevel(), Uin(gVibIoLevel)));

      // Find VscfCalcDef to initialize from.
      const Uin i_vscf = FindVscf(gVscfCalcDef, calcdef.ModalBasisFromVscf());
      auto& vscf_calcdef = gVscfCalcDef[i_vscf];

      // Find operator number
      const auto& oper_name = calcdef.Oper();
      const Uin i_oper = FindOper(gOperatorDefs, oper_name);
      const Uin i_vscf_oper = FindOper(gOperatorDefs, vscf_calcdef.Oper());

      // Find basis number
      MidasAssert(calcdef.Basis() == vscf_calcdef.Basis(), "TIM-TDVCC must use the same primitive basis as VSCF");
      Uin i_basis = FindBasis(gBasis, calcdef.Basis());

      // Generate basis set
      gBasis[i_basis].InitBasis(gOperatorDefs[i_vscf_oper]);

      Mout << "\n\n\n Do TIM-TDVCC: \"" << calcdef.Name() << "\" calculation" << std::endl;

      if (  gDebug
         || gVibIoLevel > I_2
         )
      {
         Mout  << "\n\n";
         Mout  << " Operator:                            "
               << oper_name << std::endl;
         Mout  << " Operator-Type:                       "
               << gOperatorDefs[i_oper].Type() << std::endl;
         Mout  << " Number of terms in operator:         "
               << gOperatorDefs[i_oper].Nterms() << std::endl;
         Mout  << " Basis:                               "
               << gBasis[i_basis].GetmBasName() << std::endl;
         Mout  << " Basis-Type:                          "
               << gBasis[i_basis].GetmBasSetType() << std::endl;
         Mout  << " Number of modes:                     "
               << gOperatorDefs[i_vscf_oper].NmodesInOp() << std::endl;
         Mout  << std::endl;
      }

      // Find and load VSCF.
      std::string vscf_modals_name = vscf_calcdef.Name() + "_Modals";
      auto modints = GetModalIntegrals(gOperatorDefs[i_oper], gBasis[i_basis], calcdef.LimitModalBasis(), vscf_modals_name);

      // Find VCC.
      const VccCalcDef* p_vcc = nullptr;
      if (  !gVccCalcDef.empty()
         )
      {
         const Uin i_vcc = FindVcc(gVccCalcDef, calcdef.VccGs());
         p_vcc = &gVccCalcDef[i_vcc];
      }
      // p_vcc might still be nullptr, so remember to check when using it!
      if (  calcdef.InitState() == tdvcc::InitType::VCCGS
         && !p_vcc
         )
      {
         MIDASERROR("No VCC ground-state calculation found for initializing TIM-TDVCC[2] wave packet.");
      }

      // Only support for standard mcr(max_exci, n_modes) a.t.m.
      // Cross fingers and hope it fits what was in the VCC calc.
      ModeCombiOpRange mcr(calcdef.MaxExci(), gOperatorDefs[i_oper].NmodesInOp());
      const Uin size_params = SetAddresses(mcr, calcdef.LimitModalBasis());

      // Analysis of Hermitian part of Jacobian, if requested.
      if (p_vcc != nullptr && calcdef.AnalyzeHermPartJacVccGs().first)
      {
         const auto& n_modals = calcdef.LimitModalBasis();
         const auto& opdef = gOperatorDefs[i_oper];
         const auto& basdef = gBasis[i_basis];
         const auto vcc_gs = LoadVccGs(InitType::VCCGS, CorrType::VCC, size_params, p_vcc->Name());
         const auto eigpairs = DiagHermPartVccGs
            (  n_modals
            ,  opdef
            ,  GetModalIntegralsReal(opdef, basdef, n_modals, vscf_modals_name)
            ,  mcr
            ,  vcc_gs.at(ParamID::KET)
            ,  calcdef.AnalyzeHermPartJacVccGs().second
            );
         Mout << "Eigen-pair analysis for Herm(VCC-GS Jacobian):" << std::endl;
         PrintEigenpairAnalysis(Mout, eigpairs, n_modals, mcr);
      }

      // The time-dep. pulse, if we're using one.
      using pulse_t = midas::td::GaussPulse<step_t,param_t>;
      std::unique_ptr<pulse_t> p_pulse = nullptr;
      Uin i_pulse = 0;

      // Construct TIM-TDVCC object
      std::unique_ptr<TimTdvccIfc> p_tdvcc = nullptr;
      if (calcdef.UsePulse())
      {
         using step_t = TimTdvccIfc::ifc_step_t;
         using param_t = TimTdvccIfc::ifc_param_t;
         using oper_t = midas::td::LinCombOper<step_t, param_t, OpDef>;
         i_pulse = FindOper(gOperatorDefs, calcdef.OperPulse());
         p_pulse = std::make_unique<pulse_t>(calcdef.PulseSpecs());
         const pulse_t& pulse = *p_pulse;
         oper_t lc_oper;
         lc_oper.emplace_back
            (  gOperatorDefs[i_oper]
            ,  1
            );
         lc_oper.emplace_back
            (  gOperatorDefs[i_pulse]
            ,  [p=pulse](step_t t)->param_t {return p(t);}
            ,  [p=pulse](step_t t)->param_t {return p.Deriv(t);}
            );
         std::vector<ModalIntegrals<param_t>> v_modints = 
            {  std::move(modints)
            ,  GetModalIntegrals(gOperatorDefs[i_pulse], gBasis[i_basis], calcdef.LimitModalBasis(), vscf_modals_name)
            };

         p_tdvcc = TimTdvccIfcFactory
            (  calcdef.CorrMethod()
            ,  calcdef.Transformer()
            ,  calcdef.LimitModalBasis()
            ,  std::move(lc_oper)
            ,  std::move(v_modints)
            ,  mcr
            );
      }
      else
      {
         p_tdvcc = TimTdvccIfcFactory
            (  calcdef.CorrMethod()
            ,  calcdef.Transformer()
            ,  calcdef.LimitModalBasis()
            ,  gOperatorDefs[i_oper]
            ,  std::move(modints)
            ,  mcr
            );
      }

      PostFactorySettings(*p_tdvcc, calcdef, p_vcc);
      EnableExpVals(*p_tdvcc, gOperatorDefs, calcdef.ExptValOpers(), gBasis[i_basis], calcdef.LimitModalBasis(), vscf_modals_name);

      Mout << "TIM-TDVCC settings for '" << p_tdvcc->Name() << "':" << std::endl;
      p_tdvcc->PrintSettings(Mout, 3);

      if (p_pulse)
      {
         Mout << "Pulse used for operator '" << gOperatorDefs[i_pulse].Name() << "':" << std::endl;
         p_pulse->PrintSettings(Mout, 3, 33);
      }

      // Initalize timer.
      Timer timer;
      timer.Reset();

      // Evolve the TIM-TDVCC EOMs
      p_tdvcc->Evolve();

      // Output timings.
      Mout << std::endl;
      timer.CpuOut (Mout, "CPU  time spent on TIM-TDVCC integration, '"+p_tdvcc->Name()+"': ");
      timer.WallOut(Mout, "Wall time spent on TIM-TDVCC integration, '"+p_tdvcc->Name()+"': ");
      Mout << std::endl;

      // Finalize.
      Mout << "TIM-TDVCC propagation done for '" << p_tdvcc->Name() << "':" << std::endl;
      p_tdvcc->Summary(Mout, 3);
      Mout << "Saving results on file..." << std::endl;
      p_tdvcc->PrintToFiles(Mout, gAnalysisDir, 3);
      Mout << "Calculating and writing spectra..." << std::endl;
      p_tdvcc->CalcAndWriteSpectra(Mout, gAnalysisDir, 3, calcdef);

      // Here we start extracting stuff from the object, so this _must_ be at
      // the end, just before it goes out of scope!
      if (p_tdvcc->SaveFvciVecs())
      {
         Mout << "Storing FVCI vectors and interpolated times for later use..." << std::endl;
         vec_interpol_ts.emplace_back(std::move(*p_tdvcc).ExtractInterpolTs());
         vec_fvci_vecs_ket.emplace_back(std::move(*p_tdvcc).ExtractFvciVecsKet());
         vec_fvci_vecs_bra.emplace_back(std::move(*p_tdvcc).ExtractFvciVecsBra());
      }
   }

   if (vec_interpol_ts.size() > 1)
   {
      Mout << "\n\n";
      Out72Char(Mout,'+','-','+');
      Out72Char(Mout,'|',' ','|');
      OneArgOut72(Mout," Cross-calculation comparisons",'|');
      Out72Char(Mout,'|',' ','|');
      Out72Char(Mout,'+','-','+');

      const std::string abs_path = midas::input::RemoveTrailingSlash(gAnalysisDir);
      if (!midas::filesystem::IsDir(abs_path))
      {        
         MIDASERROR("Directory '"+abs_path+"' not found.");
      }        

      Mout << "Cross-comparing FVCI vectors..." << std::endl;
      {
         const auto [success, tbl_ts, tbl_data, tbl_headers] = CrossCompareFvci(vec_interpol_ts, vec_fvci_vecs_ket);
         if (success)
         {
            std::vector<const std::vector<Nb>*> p_tbl_data;
            p_tbl_data.reserve(tbl_data.size());
            for(const auto& t: tbl_data)
            {
               p_tbl_data.emplace_back(&t);
            }
            const std::string file_name = abs_path+"/fvci_comparisons_ket.dat";
            TimTdvccIfc::PrintTableToFile(file_name, tbl_headers, tbl_ts, p_tbl_data);
            Mout  << std::setw(3) << "" << " - " << std::setw(20) << "FVCI-kets"
                  << "(file: " << file_name << ")\n"
                  ;
         }
      }
      {
         const auto [success, tbl_ts, tbl_data, tbl_headers] = CrossCompareFvci(vec_interpol_ts, vec_fvci_vecs_bra);
         if (success)
         {
            std::vector<const std::vector<Nb>*> p_tbl_data;
            p_tbl_data.reserve(tbl_data.size());
            for(const auto& t: tbl_data)
            {
               p_tbl_data.emplace_back(&t);
            }
            const std::string file_name = abs_path+"/fvci_comparisons_bra.dat";
            TimTdvccIfc::PrintTableToFile(file_name, tbl_headers, tbl_ts, p_tbl_data);
            Mout  << std::setw(3) << "" << " - " << std::setw(20) << "FVCI-bras"
                  << "(file: " << file_name << ")\n"
                  ;
         }
      }
   }

   Mout << "\n\n";
   Out72Char(Mout,'#','#','#');
   Out72Char(Mout,'#',' ','#');
   OneArgOut72(Mout," TIM-TDVCC Done",'#');
   Out72Char(Mout,'#',' ','#');
   Out72Char(Mout,'#','#','#');

}

} /* namespace midas::tdvcc */
