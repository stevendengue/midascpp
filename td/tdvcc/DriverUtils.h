/**
 *******************************************************************************
 * 
 * @file    DriverUtils.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TDVCCDRIVERUTILS_H_INCLUDED
#define TDVCCDRIVERUTILS_H_INCLUDED

// Standard headers.
#include <complex>
#include <map>
#include <string>
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "td/tdvcc/TimTdvccEnums.h"

// Forward declares.
class VscfCalcDef;
class VccCalcDef;
class OpDef;
class BasDef;
class GlobalOperatorDefinitions;
template<typename> class GeneralDataCont;
template<typename> class GeneralMidasVector;
class ModeCombiOpRange;
template<typename> class ModalIntegrals;
class TimTdvccIfc;
class TimTdvccCalcDef;

namespace midas::tdvcc
{
   //!
   Uin FindVscf(const std::vector<VscfCalcDef>&, const std::string&);

   //!
   Uin FindVcc(const std::vector<VccCalcDef>&, const std::string&);

   //!
   Uin FindOper(const GlobalOperatorDefinitions&, const std::string&);

   //!
   Uin FindBasis(const std::vector<BasDef>&, const std::string&);

   //! 
   std::pair<std::vector<Uin>,Uin> GetModalOffsets(const OpDef&, const BasDef&);

   //! Read in modal coefficients from file.
   GeneralDataCont<Nb> GetFromDisk(Uin, const std::string&);

   //!
   ModalIntegrals<Nb> GetModalIntegralsReal
      (  const OpDef& arOpDef
      ,  const BasDef& arBasDef
      ,  const std::vector<Uin>& arNModals
      ,  const std::string& arFileName
      );
   ModalIntegrals<std::complex<Nb>> GetModalIntegrals
      (  const OpDef& arOpDef
      ,  const BasDef& arBasDef
      ,  const std::vector<Uin>& arNModals
      ,  const std::string& arFileName
      );

   //!
   std::map<ParamID,GeneralMidasVector<Nb>> LoadVccGs(InitType, CorrType, Uin, const std::string&);

   //!
   std::map<ParamID,GeneralMidasVector<Nb>> PrepInitState(InitType, CorrType, Uin, const std::string& = "");

   //!
   void PostFactorySettings(TimTdvccIfc&, const TimTdvccCalcDef&, const VccCalcDef* const = nullptr);

   //!
   void EnableExpVals
      (  TimTdvccIfc& arObj
      ,  const GlobalOperatorDefinitions& arGlobOpDefs
      ,  const std::set<std::string>& arOperNames
      ,  const BasDef& arBasDef
      ,  const std::vector<Uin>& arNModals
      ,  const std::string& arVscfModalsName
      );

   //! Returns {success?, times, vec<data>, headers}
   std::tuple
      <  bool
      ,  std::vector<Nb>
      ,  std::vector<std::vector<Nb>>
      ,  std::vector<std::string>
      >
   CrossCompareFvci
      (  const std::vector<std::vector<Nb>>& arVecTimes
      ,  const std::vector<std::vector<GeneralMidasVector<std::complex<Nb>>>> arVecFvcis
      );

   //! Construct and diagonalize Hermitian part of VCC g.s. Jacobian. Return N lowest eigenpairs.
   std::vector<std::pair<Nb,GeneralMidasVector<Nb>>> DiagHermPartVccGs
      (  const std::vector<Uin>& arNModals
      ,  const OpDef& arOpDef
      ,  const ModalIntegrals<Nb>& arModInts
      ,  const ModeCombiOpRange& arMcr
      ,  const GeneralMidasVector<Nb>& arTAmpls
      ,  const Uin aNumAnalyze
      );

   //! Printout eigenpair analysis.
   void PrintEigenpairAnalysis
      (  std::ostream& arOs
      ,  const std::vector<std::pair<Nb,GeneralMidasVector<Nb>>>& arVecEigPairs
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      );

} /* namespace midas::tdvcc */


#endif /* TDVCCDRIVERUTILS_H_INCLUDED */
