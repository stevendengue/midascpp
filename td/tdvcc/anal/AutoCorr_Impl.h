/**
 *******************************************************************************
 * 
 * @file    AutoCorr_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDVCC_AUTOCORR_IMPL_H_INCLUDED
#define TDVCC_AUTOCORR_IMPL_H_INCLUDED

// Midas headers.
#include "util/type_traits/Complex.h"
#include "util/matrep/MatRepTransformers.h"
#include "util/matrep/MatRepVibOper.h"
#include "util/matrep/SparseClusterOper.h"
#include "input/ModeCombiOpRange.h"
#include "td/tdvcc/params/ParamsTimTdvcc.h"
#include "td/tdvcc/params/ParamsTimTdvci.h"
#include "td/tdvcc/params/ParamsTimTdextvcc.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateInit
      )
   {
      const PARAM_T corr_A = Dot(arStateInit.Coefs(), arStateT.Coefs());
      const PARAM_T corr_B = Dot(arStateT.Coefs(), arStateInit.Coefs());
      return std::make_pair(corr_A, midas::math::Conj(corr_B));
   }

   /************************************************************************//**
    * @return
    *    Pair of autocorrelation function and "conjugated conjugate" (actually the
    *    same for VCI).
    *    - first:  \f$ \langle \Psi(0) \vert \Psi(t) \rangle \f$ (`<VCI(0)|VCI(t)>`)
    *    - second: \f$ \langle \Psi(t) \vert \Psi(0) \rangle^* \f$ (`<VCI(t)|VCI(0)>^*`)
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      )
   {
      return AutoCorr(arStateT, arStateInit);
   }

   /************************************************************************//**
    * @param[in] arStateT
    *    The state at time t.
    * @param[in] arStateInit
    *    The initial state.
    * @param[in] arNModals
    *    The modals per mode (for VCC->VCI conversion).
    * @param[in] arMcr
    *    The mode combination range (for VCC->VCI conversion).
    * @return
    *    Pair of autocorrelation function and "conjugated conjugate".
    *    - first:  \f$ \langle \Lambda(0) \vert \Psi(t) \rangle \f$ (`<L(0)|VCC(t)>`)
    *    - second: \f$ \langle \Lambda(t) \vert \Psi(0) \rangle^* \f$ (`<L(t)|VCC(0)>^*`)
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      if (arStateT.SizeParams() != arStateInit.SizeParams())
      {
         std::stringstream ss;
         ss << "arStateT.SizeParams() (which is " << arStateT.SizeParams()
            << ") != arStateInit.SizeParams() (which is " << arStateInit.SizeParams()
            << ")."
            ;
         MIDASERROR(ss.str());
      }

      using midas::util::matrep::ExpTRef;

      auto sumprodelem = []
         (  const CONT_TMPL<PARAM_T>& L
         ,  const CONT_TMPL<PARAM_T>& expDT
         )  ->PARAM_T
      {
         if (Size(L) != Size(expDT))
         {
            // If this fails, you probably need to enhance the ExpTRef impl. to
            // account for non-canonical ModeCombiOpRange of input/result.
            std::stringstream ss;
            ss << "Size(L) (which is "<<Size(L)<<") != Size(expDT) (which is "<<Size(expDT)<<").";
            MIDASERROR(ss.str());
         }
         PARAM_T val = SumProdElems(L, expDT, 1);
         if constexpr(std::is_same_v<CONT_TMPL<PARAM_T>,GeneralDataCont<PARAM_T>>)
         {
            PARAM_T tmp(0);
            expDT.DataIo(IO_GET, 0, tmp);
            val += tmp;
         }
         else
         {
            val += expDT[0];
         }
         return val;
      };

      // <L(0)|VCC(t)> = <ref|(1+L(0))exp(T(t)-T(0))|ref> exp(-i(e(t)-e(0)))
      auto t_diff = arStateT.ClusterAmps();
      Axpy(t_diff, arStateInit.ClusterAmps(), PARAM_T(-1));
      const CONT_TMPL<PARAM_T> exp_t_diff_A(ExpTRef(arNModals, arMcr, t_diff, arMcr.GetMaxExciLevel()));
      PARAM_T corr_A = sumprodelem(arStateInit.LambdaCoefs(), exp_t_diff_A);

      // <L(t)|VCC(0)> = <ref|(1+L(t))exp(T(0)-T(t))|ref> exp(+i(e(t)-e(0)))
      Scale(t_diff, PARAM_T(-1));
      const CONT_TMPL<PARAM_T> exp_t_diff_B(ExpTRef(arNModals, arMcr, t_diff, arMcr.GetMaxExciLevel()));
      PARAM_T corr_B = sumprodelem(arStateT.LambdaCoefs(), exp_t_diff_B);

      if constexpr(midas::type_traits::IsComplexV<PARAM_T>)
      {
         const PARAM_T phase_diff = arStateT.Phase() - arStateInit.Phase();
         corr_A *= exp(PARAM_T(0,-1)*phase_diff);
         corr_B *= exp(PARAM_T(0,+1)*phase_diff);
      }

      return std::make_pair(corr_A, midas::math::Conj(corr_B));
   }

   /************************************************************************//**
    * @param[in] arStateT
    *    The state at time t.
    * @param[in] arStateInit
    *    The initial state.
    * @param[in] arNModals
    *    The modals per mode (for VCC->VCI conversion).
    * @param[in] arMcr
    *    The mode combination range (for VCC->VCI conversion).
    * @return
    *    Pair of autocorrelation function and "conjugated conjugate".
    *    - first:  \f$ \langle \Psi'(0) \vert \Psi(t) \rangle \f$ (`<Sigma(0)|VCC(t)>`)
    *    - second: \f$ \langle \Psi'(t) \vert \Psi(0) \rangle^* \f$ (`<Sigma(t)|VCC(0)>^*`)
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      if (arStateT.SizeParams() != arStateInit.SizeParams())
      {
         std::stringstream ss;
         ss << "arStateT.SizeParams() (which is " << arStateT.SizeParams()
            << ") != arStateInit.SizeParams() (which is " << arStateInit.SizeParams()
            << ")."
            ;
         MIDASERROR(ss.str());
      }

      using midas::util::matrep::ExpTRef;

      // NB! Operator exponentials always extend to full space.
      // <S(0)|VCC(t)> = <ref|exp(Sigma(0))exp(+(T(t)-T(0)))|ref> exp(-i(e(t)-e(0)))
      auto t_diff = arStateT.ClusterAmps();
      Axpy(t_diff, arStateInit.ClusterAmps(), PARAM_T(-1));
      CONT_TMPL<PARAM_T> exp_t_diff = ExpTRef(arNModals, arMcr, t_diff, arMcr.NumberOfModes());
      CONT_TMPL<PARAM_T> exp_s = ExpTRef(arNModals, arMcr, arStateInit.ExtAmps(), arMcr.NumberOfModes());
      PARAM_T corr_A = SumProdElems(exp_s, exp_t_diff);

      // <S(t)|VCC(0)> = <ref|exp(Sigma(t))exp(-(T(t)-T(0)))|ref> exp(+i(e(t)-e(0)))
      Scale(t_diff, PARAM_T(-1));
      exp_t_diff = ExpTRef(arNModals, arMcr, t_diff, arMcr.NumberOfModes());
      exp_s = ExpTRef(arNModals, arMcr, arStateT.ExtAmps(), arMcr.NumberOfModes());
      PARAM_T corr_B = SumProdElems(exp_s, exp_t_diff);

      if constexpr(midas::type_traits::IsComplexV<PARAM_T>)
      {
         const PARAM_T phase_diff = arStateT.Phase() - arStateInit.Phase();
         corr_A *= exp(PARAM_T(0,-1)*phase_diff);
         corr_B *= exp(PARAM_T(0,+1)*phase_diff);
      }

      return std::make_pair(corr_A, midas::math::Conj(corr_B));
   }

   /************************************************************************//**
    * @return
    *    Same as for AutoCorr, just here fore the interface.
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorrExt
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      )
   {
      return AutoCorr(arStateT, arStateInit);
   }

   /************************************************************************//**
    * @param[in] arStateT
    *    The state at time t.
    * @param[in] arStateInit
    *    The initial state.
    * @param[in] arNModals
    *    The modals per mode (for VCC->VCI conversion).
    * @param[in] arMcr
    *    The mode combination range (for VCC->VCI conversion).
    * @return
    *    Pair of autocorrelation function and "conjugated conjugate", but with
    *    the bra treated as if it was an ExtVCC parameterization, i.e.
    *    `<Phi|exp(L~)exp(-T)` instead of `<Lambda| = <Phi|(1+L)exp(-T)`, where
    *    `L~` is such that `exp(L~) = 1 + L` _within_ the arMcr-space.
    *    - first:  \f$ \langle \Psi'(0) \vert \Psi(t) \rangle \f$ (`<Psi'(0)|VCC(t)>`)
    *    - second: \f$ \langle \Psi'(t) \vert \Psi(0) \rangle^* \f$ (`<Psi'(t)|VCC(0)>^*`)
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorrExt
      (  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      using midas::util::matrep::InvExpTRef;
      // Add 1 to the stored L-coefs. to account for the fact that InvExpTRef
      // expects (1+L) as argument.
      auto one_p_Lprime_coefs_t = arStateT.DualParams();
      auto one_p_Lprime_coefs_0 = arStateInit.DualParams();
      ModeCombiOpRange::const_iterator it;
      if (arMcr.Find(std::vector<In>{}, it))
      {
         const auto addr = it->Address();
         if (0 <= addr && addr < Size(one_p_Lprime_coefs_t))
         {
            if constexpr(std::is_same_v<CONT_TMPL<PARAM_T>,GeneralDataCont<PARAM_T>>)
            {
               PARAM_T val = 0;

               one_p_Lprime_coefs_t.DataIo(IO_GET, addr, val);
               val += PARAM_T(1);
               one_p_Lprime_coefs_t.DataIo(IO_PUT, addr, val);

               one_p_Lprime_coefs_0.DataIo(IO_GET, addr, val);
               val += PARAM_T(1);
               one_p_Lprime_coefs_0.DataIo(IO_PUT, addr, val);
            }
            else
            {
               one_p_Lprime_coefs_t[addr] += PARAM_T(1);
               one_p_Lprime_coefs_0[addr] += PARAM_T(1);
            }
         }
      }

      return AutoCorr
         (  ParamsTimTdextvcc<PARAM_T,CONT_TMPL>
               (  arStateT.PrimalParams()
               ,  InvExpTRef(arNModals, arMcr, one_p_Lprime_coefs_t)
               ,  arStateT.Phase()
               )
         ,  ParamsTimTdextvcc<PARAM_T,CONT_TMPL>
               (  arStateInit.PrimalParams()
               ,  InvExpTRef(arNModals, arMcr, one_p_Lprime_coefs_0)
               ,  arStateInit.Phase()
               )
         ,  arNModals
         ,  arMcr
         );
   }

   /************************************************************************//**
    * @param[in] arStateT
    *    The state at time t.
    * @param[in] arStateInit
    *    The initial state.
    * @param[in] arNModals
    *    The modals per mode (for VCC->VCI conversion).
    * @param[in] arMcr
    *    The mode combination range (for VCC->VCI conversion).
    * @return
    *    Pair of autocorrelation function and "conjugated conjugate", but with
    *    the bra treated as if it was a (traditional) VCC parameterization, i.e.
    *    `<Psi'| = <Phi|(1+Sigma~)exp(-T)` instead of `<Psi'| = <Phi|exp(Sigma)exp(-T)`,
    *    where `Sigma~` is such that `exp(Sigma) = 1 + Sigma~` _within_ the arMcr-space.
    *    Pair of autocorrelation function and "conjugated conjugate".
    *    - first:  \f$ \langle \Psi'(0) \vert \Psi(t) \rangle \f$ (`<Psi'(0)|VCC(t)>`)
    *    - second: \f$ \langle \Psi'(t) \vert \Psi(0) \rangle^* \f$ (`<Psi'(t)|VCC(0)>^*`)
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorrExt
      (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      using midas::util::matrep::SparseClusterOper;
      using midas::util::matrep::TrfExpClusterOper;
      using midas::util::matrep::MatRepVibOper;
      using mv_t = GeneralMidasVector<PARAM_T>;

      SparseClusterOper sco(arNModals, arMcr);

      mv_t S_prime_t_full(sco.FullDim());
      mv_t S_prime_0_full(sco.FullDim());
      S_prime_t_full[0] = PARAM_T(1);
      S_prime_0_full[0] = PARAM_T(1);
      if constexpr(std::is_same_v<CONT_TMPL<PARAM_T>,GeneralDataCont<PARAM_T>>)
      {
         mv_t sigma_t(arStateT.DualParams().Size());
         mv_t sigma_0(arStateInit.DualParams().Size());
         arStateT.DualParams().DataIo(IO_GET, sigma_t, sigma_t.Size());
         arStateInit.DualParams().DataIo(IO_GET, sigma_0, sigma_0.Size());
         S_prime_t_full = TrfExpClusterOper<false,false>(sco, sigma_t, std::move(S_prime_t_full), PARAM_T(1));
         S_prime_0_full = TrfExpClusterOper<false,false>(sco, sigma_0, std::move(S_prime_0_full), PARAM_T(1));
      }
      else
      {
         S_prime_t_full = TrfExpClusterOper<false,false>(sco, arStateT.DualParams(), std::move(S_prime_t_full), PARAM_T(1));
         S_prime_0_full = TrfExpClusterOper<false,false>(sco, arStateInit.DualParams(), std::move(S_prime_0_full), PARAM_T(1));
      }
      S_prime_t_full[0] -= PARAM_T(1);
      S_prime_0_full[0] -= PARAM_T(1);

      return AutoCorr
         (  ParamsTimTdvcc<PARAM_T,CONT_TMPL>
               (  arStateT.PrimalParams()
               ,  CONT_TMPL<PARAM_T>(MatRepVibOper<PARAM_T>::ExtractToMcrSpace(S_prime_t_full, arMcr, arNModals))
               ,  arStateT.Phase()
               )
         ,  ParamsTimTdvcc<PARAM_T,CONT_TMPL>
               (  arStateInit.PrimalParams()
               ,  CONT_TMPL<PARAM_T>(MatRepVibOper<PARAM_T>::ExtractToMcrSpace(S_prime_0_full, arMcr, arNModals))
               ,  arStateInit.Phase()
               )
         ,  arNModals
         ,  arMcr
         );
   }

} /* namespace midas::tdvcc */

#endif/*TDVCC_AUTOCORR_IMPL_H_INCLUDED*/
