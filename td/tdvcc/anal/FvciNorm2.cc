/**
 *******************************************************************************
 * 
 * @file    FvciNorm2.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "td/tdvcc/anal/FvciNorm2.h"
#include "td/tdvcc/anal/FvciNorm2_Impl.h"

#include <complex>

// Define instatiation macro.
#define UNPACK(...) __VA_ARGS__
#define INSTANTIATE_FVCINORM2(PARAM_T, CONT_TMPL) \
   namespace midas::tdvcc \
   { \
      template midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket \
         (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState \
         ); \
      template midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket \
         (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState \
         ,  const std::vector<Uin>& \
         ,  const ModeCombiOpRange& \
         ); \
      template midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket \
         (  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arState \
         ,  const std::vector<Uin>& arNModals \
         ,  const ModeCombiOpRange& arMcr \
         ); \
      template midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket \
         (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arState \
         ,  const std::vector<Uin>& arNModals \
         ,  const ModeCombiOpRange& arMcr \
         ); \
      template midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra \
         (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState \
         ); \
      template midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra \
         (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState \
         ,  const std::vector<Uin>& \
         ,  const ModeCombiOpRange& \
         ); \
      template midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra \
         (  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arState \
         ,  const std::vector<Uin>& arNModals \
         ,  const ModeCombiOpRange& arMcr \
         ); \
      template midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra \
         (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arState \
         ,  const std::vector<Uin>& arNModals \
         ,  const ModeCombiOpRange& arMcr \
         ); \
      template CONT_TMPL<PARAM_T> ConvertKetToFvci \
         (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState \
         ,  const std::vector<Uin>& \
         ,  const ModeCombiOpRange& \
         ); \
      template CONT_TMPL<PARAM_T> ConvertKetToFvci \
         (  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arState \
         ,  const std::vector<Uin>& \
         ,  const ModeCombiOpRange& \
         ); \
      template CONT_TMPL<PARAM_T> ConvertKetToFvci \
         (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arState \
         ,  const std::vector<Uin>& \
         ,  const ModeCombiOpRange& \
         ); \
      template CONT_TMPL<PARAM_T> ConvertBraToFvci \
         (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState \
         ,  const std::vector<Uin>& \
         ,  const ModeCombiOpRange& \
         ); \
      template CONT_TMPL<PARAM_T> ConvertBraToFvci \
         (  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arState \
         ,  const std::vector<Uin>& \
         ,  const ModeCombiOpRange& \
         ); \
      template CONT_TMPL<PARAM_T> ConvertBraToFvci \
         (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arState \
         ,  const std::vector<Uin>& \
         ,  const ModeCombiOpRange& \
         ); \
   }; /* namespace midas::tdvcc */ \

// Instantiations.
INSTANTIATE_FVCINORM2(Nb,GeneralMidasVector);
INSTANTIATE_FVCINORM2(std::complex<Nb>,GeneralMidasVector);
INSTANTIATE_FVCINORM2(Nb,GeneralDataCont);
INSTANTIATE_FVCINORM2(std::complex<Nb>,GeneralDataCont);

#undef INSTANTIATE_FVCINORM2
#undef UNPACK
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
