/**
 *******************************************************************************
 * 
 * @file    AutoCorr.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "td/tdvcc/anal/AutoCorr.h"
#include "td/tdvcc/anal/AutoCorr_Impl.h"

#include <complex>

// Define instatiation macro.
#define UNPACK(...) __VA_ARGS__
#define INSTANTIATE_AUTOCORR(PARAM_T, CONT_TMPL) \
   namespace midas::tdvcc \
   { \
      template std::pair<PARAM_T,PARAM_T> AutoCorr \
         (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateT \
         ,  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateInit \
         ); \
      template std::pair<PARAM_T,PARAM_T> AutoCorr \
         (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateT \
         ,  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateInit \
         ,  const std::vector<Uin>& \
         ,  const ModeCombiOpRange& \
         ); \
      template std::pair<PARAM_T,PARAM_T> AutoCorr \
         (  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arStateT \
         ,  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arStateInit \
         ,  const std::vector<Uin>& \
         ,  const ModeCombiOpRange& \
         ); \
      template std::pair<PARAM_T,PARAM_T> AutoCorr \
         (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arStateT \
         ,  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arStateInit \
         ,  const std::vector<Uin>& \
         ,  const ModeCombiOpRange& \
         ); \
      template std::pair<PARAM_T,PARAM_T> AutoCorrExt \
         (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateT \
         ,  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateInit \
         ,  const std::vector<Uin>& \
         ,  const ModeCombiOpRange& \
         ); \
      template std::pair<PARAM_T,PARAM_T> AutoCorrExt \
         (  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arStateT \
         ,  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arStateInit \
         ,  const std::vector<Uin>& \
         ,  const ModeCombiOpRange& \
         ); \
      template std::pair<PARAM_T,PARAM_T> AutoCorrExt \
         (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arStateT \
         ,  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arStateInit \
         ,  const std::vector<Uin>& \
         ,  const ModeCombiOpRange& \
         ); \
   }; /* namespace midas::tdvcc */ \

// Instantiations.
INSTANTIATE_AUTOCORR(Nb,GeneralMidasVector);
INSTANTIATE_AUTOCORR(std::complex<Nb>,GeneralMidasVector);
INSTANTIATE_AUTOCORR(Nb,GeneralDataCont);
INSTANTIATE_AUTOCORR(std::complex<Nb>,GeneralDataCont);

#undef INSTANTIATE_AUTOCORR
#undef UNPACK
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
