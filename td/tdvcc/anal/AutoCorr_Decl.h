/**
 *******************************************************************************
 * 
 * @file    AutoCorr_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDVCC_AUTOCORR_DECL_H_INCLUDED
#define TDVCC_AUTOCORR_DECL_H_INCLUDED

// Standard headers.
#include <utility>
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"

// Forward declarations.
class ModeCombiOpRange;
template<typename> class GeneralMidasVector;
template<typename> class GeneralDataCont;
namespace midas::tdvcc
{

   // Forward declarations
   template<typename, template<typename> class> class ParamsTimTdvcc;
   template<typename, template<typename> class> class ParamsTimTdvci;
   template<typename, template<typename> class> class ParamsTimTdextvcc;

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateInit
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorr
      (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorrExt
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorrExt
      (  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   std::pair<PARAM_T,PARAM_T> AutoCorrExt
      (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arStateT
      ,  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arStateInit
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

} /* namespace midas::tdvcc */

#endif/*TDVCC_AUTOCORR_DECL_H_INCLUDED*/
