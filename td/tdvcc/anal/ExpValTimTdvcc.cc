/**
 *******************************************************************************
 * 
 * @file    ExpValTimTdvcc.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "td/tdvcc/anal/ExpValTimTdvcc.h"
#include "td/tdvcc/anal/ExpValTimTdvcc_Impl.h"
#include "td/tdvcc/trf/TrfTimTdvccMatRep.h"
#include "td/tdvcc/trf/TrfTimTdvciMatRep.h"
#include "td/tdvcc/trf/TrfTimTdvcc2.h"
#include "td/tdvcc/trf/TrfTimTdvci2.h"
#include "td/tdvcc/trf/TrfTimTdextvccMatRep.h"

#include <complex>

// Define instatiation macro.
#define UNPACK(...) __VA_ARGS__
#define INSTANTIATE_EXPVAL(STEP_T,PARAM_T, CONT_TMPL, TRF_TMPL)      \
      template PARAM_T ExpVal                                        \
         (  STEP_T                                                   \
         ,  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>&                \
         ,  const TRF_TMPL<PARAM_T>&                                 \
         );                                                          \
      template PARAM_T ExpVal                                        \
         (  STEP_T                                                   \
         ,  const ParamsTimTdvci<PARAM_T, CONT_TMPL>&                \
         ,  const TRF_TMPL<PARAM_T>&                                 \
         );                                                          \

#define INSTANTIATE_EXPVAL_EXOTIC(STEP_T,PARAM_T, CONT_TMPL, TRF_TMPL)\
      template PARAM_T ExpVal                                        \
         (  STEP_T                                                   \
         ,  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>&             \
         ,  const TRF_TMPL<PARAM_T>&                                 \
         );                                                          \

// Instantiations.
namespace midas::tdvcc
{
// MatRep versions
INSTANTIATE_EXPVAL(Nb,Nb,GeneralMidasVector,TrfTimTdvccMatRep);
INSTANTIATE_EXPVAL(Nb,Nb,GeneralMidasVector,TrfTimTdvciMatRep);
INSTANTIATE_EXPVAL(Nb,std::complex<Nb>,GeneralMidasVector,TrfTimTdvccMatRep);
INSTANTIATE_EXPVAL(Nb,std::complex<Nb>,GeneralMidasVector,TrfTimTdvciMatRep);

// VCC[2]/H2 versions
INSTANTIATE_EXPVAL(Nb,Nb,GeneralDataCont,TrfTimTdvcc2);
INSTANTIATE_EXPVAL(Nb,Nb,GeneralDataCont,TrfTimTdvci2);
INSTANTIATE_EXPVAL(Nb,std::complex<Nb>,GeneralDataCont,TrfTimTdvcc2);
INSTANTIATE_EXPVAL(Nb,std::complex<Nb>,GeneralDataCont,TrfTimTdvci2);

// Exotic TDVCC flavours.
INSTANTIATE_EXPVAL_EXOTIC(Nb,Nb,GeneralMidasVector,TrfTimTdextvccMatRep);
INSTANTIATE_EXPVAL_EXOTIC(Nb,std::complex<Nb>,GeneralMidasVector,TrfTimTdextvccMatRep);
}

#undef INSTANTIATE_EXPVAL
#undef INSTANTIATE_EXPVAL_EXOTIC
#undef UNPACK
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
