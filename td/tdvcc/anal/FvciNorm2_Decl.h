/**
 *******************************************************************************
 * 
 * @file    FvciNorm2_Decl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDVCC_FVCINORM2_DECL_H_INCLUDED
#define TDVCC_FVCINORM2_DECL_H_INCLUDED

// Standard headers.
#include <type_traits>
#include <vector>

// Midas headers.
#include "util/type_traits/Complex.h"
#include "inc_gen/TypeDefs.h"

// Forward declarations.
class ModeCombiOpRange;
template<typename> class GeneralMidasVector;
template<typename> class GeneralDataCont;
template<typename, template<typename> class> class ParamsTimTdvcc;
template<typename, template<typename> class> class ParamsTimTdvci;
template<typename, template<typename> class> class ParamsTimTdextvcc;

namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState
      );

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState
      );

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertKetToFvci
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   GeneralMidasVector<PARAM_T> ConvertKetToFvci
      (  const ParamsTimTdvcc<PARAM_T, GeneralMidasVector>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   GeneralDataCont<PARAM_T> ConvertKetToFvci
      (  const ParamsTimTdvcc<PARAM_T, GeneralDataCont>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertKetToFvci
      (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertBraToFvci
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertBraToFvci
      (  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertBraToFvci
      (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      );


} /* namespace midas::tdvcc */

#endif/*TDVCC_FVCINORM2_DECL_H_INCLUDED*/
