/**
 *******************************************************************************
 * 
 * @file    FvciNorm2_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TDVCC_FVCINORM2_IMPL_H_INCLUDED
#define TDVCC_FVCINORM2_IMPL_H_INCLUDED

// Midas headers.
#include "td/tdvcc/params/ParamsTimTdvcc.h"
#include "td/tdvcc/params/ParamsTimTdvci.h"
#include "td/tdvcc/params/ParamsTimTdextvcc.h"
#include "util/matrep/MatRepUtils.h"
#include "util/matrep/MatRepTransformers.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState
      )
   {
      return Norm2(arState);
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      )
   {
      return FvciNorm2Ket(arState);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      return Norm2(ConvertKetToFvci(arState,arNModals,arMcr));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Ket
      (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      return Norm2(ConvertKetToFvci(arState,arNModals,arMcr));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState
      )
   {
      return FvciNorm2Ket(arState);
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>&
      ,  const ModeCombiOpRange&
      )
   {
      return FvciNorm2Bra(arState);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      return Norm2(ConvertBraToFvci(arState,arNModals,arMcr));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   midas::type_traits::RealTypeT<PARAM_T> FvciNorm2Bra
      (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      return Norm2(ConvertBraToFvci(arState,arNModals,arMcr));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertKetToFvci
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      const auto fvci_size = midas::util::matrep::Product(arNModals);
      CONT_TMPL<PARAM_T> fvci(arState.Coefs());
      std::vector<PARAM_T> zeros(fvci_size - fvci.Size(), PARAM_T(0));
      fvci.Insert(fvci.Size(), zeros.begin(), zeros.end());
      return fvci;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   GeneralMidasVector<PARAM_T> ConvertKetToFvci
      (  const ParamsTimTdvcc<PARAM_T, GeneralMidasVector>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      return midas::util::matrep::ExpTRef(arNModals, arMcr, arState.ClusterAmps(), arNModals.size());
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   GeneralDataCont<PARAM_T> ConvertKetToFvci
      (  const ParamsTimTdvcc<PARAM_T, GeneralDataCont>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      using midas::util::matrep::ExpTRef;
      GeneralMidasVector<PARAM_T> tmp_mv;
      const GeneralMidasVector<PARAM_T>* p_mv = nullptr;
      const auto& t_amps = arState.ClusterAmps();
      if (t_amps.InMem())
      {
         p_mv = t_amps.GetVector();
      }
      else
      {
         tmp_mv.SetNewSize(t_amps.Size());
         t_amps.DataIo(IO_GET, tmp_mv, tmp_mv.Size());
         p_mv = &tmp_mv;
      }
      return GeneralDataCont<PARAM_T>(ExpTRef(arNModals, arMcr, *p_mv, arNModals.size()));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertKetToFvci
      (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      using midas::util::matrep::ExpTRef;
      return CONT_TMPL<PARAM_T>(ExpTRef(arNModals, arMcr, arState.ClusterAmps(), arNModals.size()));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertBraToFvci
      (  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      GeneralDataCont<PARAM_T> dc(ConvertKetToFvci(arState, arNModals, arMcr));
      return CONT_TMPL<PARAM_T>(dc.GetVector()->Conjugate());
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertBraToFvci
      (  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      return CONT_TMPL<PARAM_T>
         (  midas::util::matrep::Ref1pLExpmT
            (  arNModals
            ,  arMcr
            ,  arState.ClusterAmps()
            ,  arState.LambdaCoefs()
            ,  arNModals.size()
            )
         );
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      >
   CONT_TMPL<PARAM_T> ConvertBraToFvci
      (  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const std::vector<Uin>& arNModals
      ,  const ModeCombiOpRange& arMcr
      )
   {
      return CONT_TMPL<PARAM_T>
         (  midas::util::matrep::RefExpSExpmT
            (  arNModals
            ,  arMcr
            ,  arState.ClusterAmps()
            ,  arState.ExtAmps()
            ,  arNModals.size()
            )
         );
   }


} /* namespace midas::tdvcc */

#endif/*TDVCC_FVCINORM2_IMPL_H_INCLUDED*/
