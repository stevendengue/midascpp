/**
 *******************************************************************************
 * 
 * @file    ExpValTimTdvcc_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef EXPVALTIMTDVCC_IMPL_H_INCLUDED
#define EXPVALTIMTDVCC_IMPL_H_INCLUDED

// Midas headers.
#include "td/tdvcc/params/ParamsTimTdvcc.h"
#include "td/tdvcc/params/ParamsTimTdvci.h"
#include "td/tdvcc/params/ParamsTimTdextvcc.h"
#include "td/tdvcc/params/ParamsTimTdvccUtils.h"

namespace midas::tdvcc
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      >
   PARAM_T ExpVal
      (  STEP_T aTime
      ,  const ParamsTimTdvcc<PARAM_T, CONT_TMPL>& arState
      ,  const TRF_TMPL<PARAM_T>& arTrf
      )
   {
      using cont_t = CONT_TMPL<PARAM_T>;
      const cont_t v = arTrf.ExpValTrf(aTime, arState.ClusterAmps());
      PARAM_T result = 0;
      if constexpr(std::is_same_v<cont_t, GeneralDataCont<PARAM_T>>)
      {
         v.DataIo(IO_GET, 0, result);
      }
      else
      {
         result = v[0];
      }

      result += SumProdElems(arState.LambdaCoefs(), v, 1, arState.SizeParams());

      return result;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      >
   PARAM_T ExpVal
      (  STEP_T aTime
      ,  const ParamsTimTdvci<PARAM_T, CONT_TMPL>& arState
      ,  const TRF_TMPL<PARAM_T>& arTrf
      )
   {
      return Dot(arState.Coefs(), arTrf.ExpValTrf(aTime, arState.Coefs()))/Norm2(arState);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename PARAM_T
      ,  template<typename> class CONT_TMPL
      ,  template<typename> class TRF_TMPL
      >
   PARAM_T ExpVal
      (  STEP_T aTime
      ,  const ParamsTimTdextvcc<PARAM_T, CONT_TMPL>& arState
      ,  const TRF_TMPL<PARAM_T>& arTrf
      )
   {
      // The ref element of this vector is the expectation value.
      auto v = arTrf.HamDerExtAmp(aTime, arState.ClusterAmps(), arState.ExtAmps());
      // Just for returning 0'th element, works uniformly for MidasVector/DataCont.
      return ZeroIthElemAndReturnIt(v, I_0);
   }


} /* namespace midas::tdvcc */

#endif/*EXPVALTIMTDVCC_IMPL_H_INCLUDED*/
