/**
 *******************************************************************************
 * 
 * @file    TimTdvccIfc.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

// Standard headers.

// Midas headers.
#include "td/tdvcc/TimTdvccIfc.h"
#include "mmv/MidasVector.h"
#include "ode/OdeInfoOutput.h"
#include "mpi/Impi.h"
#include "input/Trim.h"
#include "input/OpDef.h"
#include "td/SpectrumCalculator.h"
#include "util/SanityCheck.h"

// Forward declares.


namespace midas::tdvcc
{
   /****************************************************************************
    *
    ***************************************************************************/
   TimTdvccIfc::TimTdvccIfc
      (
      )
      :  mOdeInfo(DefaultOdeInfo())
   {
   }

   /****************************************************************************
    * 
    ***************************************************************************/
   void TimTdvccIfc::PrintSettings
      (  std::ostream& arOs
      ,  const Uin aIndent
      )  const
   {
      // Store old format flags.
      const auto old_flags = arOs.flags();

      // Width, precisions and such.
      const std::string tab(aIndent, ' ');
      const std::string tab2(aIndent + 3, ' ');
      const std::string eq = " = ";
      const Uin w = 33;
      const Uin w2 = w - 3;
      arOs << std::left;
      arOs << std::boolalpha;

      arOs << tab << std::setw(w) << "Name" << eq << Name() << '\n';
      arOs << tab << std::setw(w) << "IoLevel" << eq << IoLevel() << '\n';
      arOs << tab << std::setw(w) << "Timings?" << eq << TimeIt() << '\n';
      arOs << tab << std::setw(w) << "Transformer type" << eq << StringFromEnum(GetTrfType()) << '\n';
      arOs << tab << std::setw(w) << "Correlation type" << eq << StringFromEnum(GetCorrType()) << '\n';
      arOs << tab << std::setw(w) << "Max. exci." << eq << MaxExci() << '\n';
      arOs << tab << std::setw(w) << "Modal basis" << eq << NModals() << '\n';
      arOs << tab << std::setw(w) << "Size, single parameter cont" << eq << SizeParams() << '\n';
      arOs << tab << std::setw(w) << "Size, total" << eq << SizeTotal() << '\n';
      arOs << tab << std::setw(w) << "Has phase?" << eq << HasPhase() << '\n';
      arOs << tab << std::setw(w) << "Num. H contribs" << eq << NumHamiltonianContribs() << '\n';
      arOs << tab << std::setw(w) << "Imaginary time?" << eq << ImagTime() << '\n';
      if (CheckLastDerivNorm())
      {
         arOs << tab << std::setw(w) << "Imag. time hault thr." << eq << ImagTimeHaultThr() << '\n';
      }
      arOs << '\n' << tab << "Tracked properties:\n";
      for(const auto& kv: GetMapStringToEnum<PropID>())
      {
         arOs << tab2 << std::setw(w2) << kv.first << eq << Tracks(kv.second) << '\n';
      }
      arOs << '\n' << tab << "Tracked statistics:\n";
      for(const auto& kv: GetMapStringToEnum<StatID>())
      {
         arOs << tab2 << std::setw(w2) << kv.first << eq << Tracks(kv.second) << '\n';
      }
      arOs << '\n' << tab << "Tracked expectation values (operator names):\n";
      if (TrackedExptVals().empty())
      {
         arOs << tab2 << std::setw(w2) << "<none>" << '\n';
      }
      else
      {
         for(const auto& key: TrackedExptVals())
         {
            arOs << tab2 << std::setw(w2) << key << '\n';
         }
      }

      arOs << '\n' << tab << "ODE info:\n";
      midas::ode::Output(arOs, GetOdeInfo(), 2*tab.size(), w-tab.size(), " = ");

      arOs << std::endl;
      arOs.flags(old_flags);
   }

   /****************************************************************************
    * 
    ***************************************************************************/
   template<typename T>
   void TimTdvccIfc::PrintEqualityLine
      (  std::ostream& arOs
      ,  const std::string& arLeft
      ,  const T& arRight
      ,  const Uin aIndent
      ,  const Uin aWLeft
      ,  const Uin aWRight
      )
   {
      arOs 
         << std::setw(aIndent) << ""
         << std::left << std::setw(aWLeft) << arLeft
         << " = "
         ;
      if constexpr(midas::type_traits::IsComplexV<T>)
      {
         arOs << std::right << std::setw(aWRight) << std::real(arRight);
         if (std::imag(arRight))
         {
            arOs << " +i(" << std::right << std::setw(aWRight) << std::imag(arRight) << ")";
         }
      }
      else
      {
         arOs << std::right << std::setw(aWRight) << arRight;
      }
      arOs << '\n';
   }

   /****************************************************************************
    * 
    ***************************************************************************/
   template<typename T>
   void TimTdvccIfc::SummaryOutputUtil
      (  std::ostream& arOs
      ,  const std::vector<T>& arVec
      ,  const std::vector<ifc_step_t>& arTs
      ,  const std::string& arPrefix
      ,  const Uin aIndent
      ,  const Uin aWLeft
      ,  const Uin aWRight
      )
   {
      const auto& s_key = arPrefix;
      const auto& v = arVec;
      const auto& n_times = arTs.size();

      if (v.size() != n_times)
      {
         MIDASERROR(s_key+"; size() (which is "+std::to_string(v.size())+") != n_times (which is "+std::to_string(n_times)+").");
      }

      if (n_times > 0)
      {
         const auto t_dur = arTs.back() - arTs.front();
         const auto& v_beg = v.front();
         const auto& v_end = v.back();
         const auto change = v_end - v_beg;
         const auto rate = change * ((t_dur != 0.0)? 1.0/t_dur: 0.0);
         PrintEqualityLine(arOs, s_key+", at t_beg", v_beg, aIndent, aWLeft, aWRight);
         PrintEqualityLine(arOs, s_key+", at t_end", v_end, aIndent, aWLeft, aWRight);
         PrintEqualityLine(arOs, s_key+", change", change, aIndent, aWLeft, aWRight);
         PrintEqualityLine(arOs, s_key+", rate", rate, aIndent, aWLeft, aWRight);

         if constexpr(midas::type_traits::IsComplexV<T>)
         {
            const bool all_vals_real = AllValuesReal(v);
            PrintEqualityLine(arOs, s_key+", all real", all_vals_real, aIndent, aWLeft, aWRight);

            const auto [min, max, avg, std, var] = DistAnalReal(v, arTs);
            PrintEqualityLine(arOs, s_key+", min (real)", min      , aIndent, aWLeft, aWRight);
            PrintEqualityLine(arOs, s_key+", max (real)", max      , aIndent, aWLeft, aWRight);
            PrintEqualityLine(arOs, s_key+", span(real)", max - min, aIndent, aWLeft, aWRight);
            PrintEqualityLine(arOs, s_key+", avg (real)", avg      , aIndent, aWLeft, aWRight);
            PrintEqualityLine(arOs, s_key+", std (real)", std      , aIndent, aWLeft, aWRight);
            PrintEqualityLine(arOs, s_key+", var (real)", var      , aIndent, aWLeft, aWRight);
            if (!all_vals_real)
            {
               const auto [min, max, avg, std, var] = DistAnalImag(v, arTs);
               PrintEqualityLine(arOs, s_key+", min (imag)", min      , aIndent, aWLeft, aWRight);
               PrintEqualityLine(arOs, s_key+", max (imag)", max      , aIndent, aWLeft, aWRight);
               PrintEqualityLine(arOs, s_key+", span(imag)", max - min, aIndent, aWLeft, aWRight);
               PrintEqualityLine(arOs, s_key+", avg (imag)", avg      , aIndent, aWLeft, aWRight);
               PrintEqualityLine(arOs, s_key+", std (imag)", std      , aIndent, aWLeft, aWRight);
               PrintEqualityLine(arOs, s_key+", var (imag)", var      , aIndent, aWLeft, aWRight);
            }
         }
         else
         {
            const auto [min, max, avg, std, var] = DistAnal(v, arTs);
            PrintEqualityLine(arOs, s_key+", min ", min      , aIndent, aWLeft, aWRight);
            PrintEqualityLine(arOs, s_key+", max ", max      , aIndent, aWLeft, aWRight);
            PrintEqualityLine(arOs, s_key+", span", max - min, aIndent, aWLeft, aWRight);
            PrintEqualityLine(arOs, s_key+", avg ", avg      , aIndent, aWLeft, aWRight);
            PrintEqualityLine(arOs, s_key+", std ", std      , aIndent, aWLeft, aWRight);
            PrintEqualityLine(arOs, s_key+", var ", var      , aIndent, aWLeft, aWRight);
         }
         arOs << '\n';
      }
   }

   /****************************************************************************
    * 
    ***************************************************************************/
   void TimTdvccIfc::Summary
      (  std::ostream& arOs
      ,  const Uin aIndent
      )  const
   {
      // Store old format flags.
      const auto old_flags = arOs.flags();

      // Width, precisions and such.
      const Uin ind = aIndent;
      const Uin ind2= ind + 3;
      const std::string tab(aIndent, ' ');
      const Uin wnum = 23;
      const Uin w = 33;
      const Uin w2 = w - 3;
      //arOs << std::left;
      arOs << std::boolalpha;

      // Helpful lambdas.
      auto t_avg = [](ifc_step_t first, ifc_step_t last, Uin n)->ifc_step_t
      {
         return (n > 1)? (last - first)/(n-1): 0;
      };

      const auto n_times = mStepTs.size();
      const auto n_interp = mInterpolTs.size();
      if (n_times > 0)
      {
         const auto t_beg = mStepTs.front();
         const auto t_end = mStepTs.back();
         const auto t_dur = t_end - t_beg;

         // Time.
         arOs << tab << "Time:\n";
         PrintEqualityLine(arOs, "t_beg", mStepTs.front(), ind2, w2, wnum);
         PrintEqualityLine(arOs, "t_end", mStepTs.back(), ind2, w2, wnum);
         PrintEqualityLine(arOs, "duration", mStepTs.back() - mStepTs.front(), ind2, w2, wnum);
         PrintEqualityLine(arOs, "avg. t_step", t_avg(mStepTs.front(),mStepTs.back(),mStepTs.size()), ind2, w2, wnum);
         PrintEqualityLine(arOs, "avg. interp. t_step", t_avg(mInterpolTs.front(),mInterpolTs.back(),mInterpolTs.size()), ind2, w2, wnum);
         PrintEqualityLine(arOs, "num. time points", n_times, ind2, w2, wnum);
         PrintEqualityLine(arOs, "num. interpolated points", n_interp, ind2, w2, wnum);
         arOs << '\n';

         // Properties.
         if (!mProps.empty())
         {
            arOs << tab << "Properties:\n";
         }
         for(const auto& kv: mProps)
         {
            const auto& s_key = StringFromEnum(kv.first);
            const auto& v = kv.second;
            SummaryOutputUtil(arOs, v, mStepTs, s_key, ind2, w2, wnum);
         }

         // Statistics.
         if (!mStats.empty())
         {
            arOs << tab << "Statistics:\n";
         }
         for(const auto& kv: mStats)
         {
            const auto& s_key = StringFromEnum(kv.first) + " (tot)";
            const auto& v = kv.second;
            SummaryOutputUtil(arOs, v, mStepTs, s_key, ind2, w2, wnum);

            if (mStatsContribs.at(kv.first).size() > 1)
            {
               for(const auto& contrib_kv: mStatsContribs.at(kv.first))
               {
                  const auto& contr_s_key = StringFromEnum(kv.first) + " (" + StringFromEnum(contrib_kv.first)+ ")";
                  const auto& contr_v = contrib_kv.second;
                  SummaryOutputUtil(arOs, contr_v, mStepTs, contr_s_key, ind2, w2, wnum);
               }
            }
            arOs << '\n';
         }

         // Auto-correlation.
         if (Tracks(PropID::AUTOCORR))
         {
            arOs << tab << "Autocorrelation:\n";
            const PropID id = PropID::AUTOCORR;
            std::vector
               <  std::tuple
                     <  std::string
                     ,  const std::vector<ifc_prop_t>*
                     >
               >
               v_data =
               {  {  StringFromEnum(id) + " (avg)"
                  ,  &mAutoCorr
                  }
               ,  {  StringFromEnum(id) + " (<0|t>)"
                  ,  &mAutoCorrA
                  }
               ,  {  StringFromEnum(id) + " (<t|0>*)"
                  ,  &mAutoCorrB
                  }
               };
            for(const auto& data: v_data)
            {
               SummaryOutputUtil(arOs, *std::get<1>(data), mInterpolTs, std::get<0>(data), ind2, w2, wnum);
            }
         }

         // Auto-correlation, "extended".
         if (Tracks(PropID::AUTOCORR_EXT))
         {
            arOs << tab << "Autocorrelation (\"extended\"):\n";
            const PropID id = PropID::AUTOCORR_EXT;
            std::vector
               <  std::tuple
                     <  std::string
                     ,  const std::vector<ifc_prop_t>*
                     >
               >
               v_data =
               {  {  StringFromEnum(id) + " (avg)"
                  ,  &mAutoCorrExt
                  }
               ,  {  StringFromEnum(id) + " (<0|t>)"
                  ,  &mAutoCorrExtA
                  }
               ,  {  StringFromEnum(id) + " (<t|0>*)"
                  ,  &mAutoCorrExtB
                  }
               };
            for(const auto& data: v_data)
            {
               SummaryOutputUtil(arOs, *std::get<1>(data), mInterpolTs, std::get<0>(data), ind2, w2+6, wnum);
            }
         }

         // Expectation values.
         if (!mExptVals.empty())
         {
            arOs << tab << "Expectation values:\n";
         }
         for(const auto& kv: mExptVals)
         {
            const auto& s_key = kv.first;
            const auto& v = kv.second;
            SummaryOutputUtil(arOs, v, mStepTs, s_key, ind2, w2, wnum);
         }

      }
      else
      {
         arOs << tab << "<no points in time>" << std::endl;
      }

      arOs << std::endl;
      arOs.flags(old_flags);
   }

   /****************************************************************************
    * 
    ***************************************************************************/
   void TimTdvccIfc::PrintToFiles
      (  std::ostream& arOs
      ,  const std::string& arAbsPathDir
      ,  const Uin aIndent
      )  const
   {
      const auto old_flags = arOs.flags();
      arOs << std::left;
      const std::string abs_path = midas::input::RemoveTrailingSlash(arAbsPathDir);

      if (!midas::filesystem::IsDir(arAbsPathDir))
      {
         MIDASERROR("Directory '"+arAbsPathDir+"' not found.");
      }

      // Properties.
      if (!TrackedProps().empty())
      {
         arOs
            << std::setw(aIndent) << ""
            << "Writing properties to file:\n"
            ;
      }
      for(const auto& id: TrackedProps())
      {
         const std::string file_name = abs_path+"/"+GenerateFileName(id);
         if (PrintPropToFile(file_name, id))
         {
            arOs 
               << std::setw(aIndent) << ""
               << " - " << std::setw(20) << StringFromEnum(id)
               << "(file: " << file_name << ")"
               << '\n'
               ;
         }
      }

      // Statistics.
      if (!TrackedStats().empty())
      {
         arOs
            << std::setw(aIndent) << ""
            << "Writing statistics to file:\n"
            ;
      }
      for(const auto& id: TrackedStats())
      {
         const std::string file_name = abs_path+"/"+GenerateFileName(id);
         if (PrintStatToFile(file_name, id))
         {
            arOs 
               << std::setw(aIndent) << ""
               << " - " << std::setw(20) << StringFromEnum(id)
               << "(file: " << file_name << ")"
               << '\n'
               ;
         }
      }

      // Auto-correlation.
      if (!TrackedAutoCorr().empty())
      {
         arOs
            << std::setw(aIndent) << ""
            << "Writing auto-correlation to file:\n"
            ;
      }
      for(const auto& id: TrackedAutoCorr())
      {
         if (  id != PropID::AUTOCORR
            && id != PropID::AUTOCORR_EXT
            )
         {
            MIDASERROR("Writing to file for '"+StringFromEnum(id)+"' not implemented.");
         }
         const std::string file_name = abs_path+"/"+GenerateFileName(id);
         if (PrintAutoCorrToFile(file_name, id))
         {
            arOs 
               << std::setw(aIndent) << ""
               << " - " << std::setw(20) << StringFromEnum(id)
               << "(file: " << file_name << ")"
               << '\n'
               ;
         }
      }

      // Expectation values.
      if (!TrackedProps().empty())
      {
         arOs
            << std::setw(aIndent) << ""
            << "Writing expectation values to file:\n"
            ;
      }
      for(const auto& s: TrackedExptVals())
      {
         const std::string file_name = abs_path+"/"+midas::input::Reduce(Name() + "_exptval_" + s +".dat");
         if (PrintExptValToFile(file_name, s))
         {
            arOs 
               << std::setw(aIndent) << ""
               << " - " << std::setw(20) << s
               << "(file: " << file_name << ")"
               << '\n'
               ;
         }
      }

      arOs << std::flush;
      arOs.flags(old_flags);
   }

   /****************************************************************************
    * 
    ***************************************************************************/
   void TimTdvccIfc::CalcAndWriteSpectra
      (  std::ostream& arOs
      ,  const std::string& arAbsPathDir
      ,  const Uin aIndent
      ,  const TdPropertyDef& arDef
      )  const
   {
      using spec_t = midas::td::SpectrumCalculator<ifc_prop_t>;
      using midas::input::Reduce;
      using midas::input::ToLowerCase;

      spec_t spec(&arDef);

      if (arDef.SpectrumEnergyShift().first == "E0")
      {
         ifc_prop_t e_init = 0;
         try
         {
            e_init = mProps.at(PropID::ENERGY).at(0);
         }
         catch(const std::out_of_range&)
         {
            // Just leave it at zero then.
         }

         if (  midas::util::IsSane(e_init)
            )
         {
            spec.SetSpectrumEnergyShift(std::real(e_init));
            arOs  << std::setw(aIndent) << "" << "Energy shift for TDVCC spectrum set to Re[E(t_beg)] = " << std::real(e_init) << std::endl;
         }  
         else
         {
            MidasWarning("Could not set spectrum energy shift. E0 = " + std::to_string(std::real(e_init)) + " + i * " + std::to_string(std::imag(e_init)));
         }
      }

      std::map<PropID, std::vector<std::tuple<std::string, std::vector<ifc_prop_t>>>> map_v_spec_data =
         {  {  PropID::AUTOCORR
            ,  {  {"avg", mAutoCorr}
               ,  {"typeA", mAutoCorrA}
               ,  {"typeB", mAutoCorrB}
               }
            }
         ,  {  PropID::AUTOCORR_EXT
            ,  {  {"avg", mAutoCorrExt}
               ,  {"typeA", mAutoCorrExtA}
               ,  {"typeB", mAutoCorrExtB}
               }
            }
         };

      for(const auto& id: TrackedAutoCorr())
      {
         const auto old_flags = arOs.flags();
         arOs << std::left;
         const std::string abs_path = midas::input::RemoveTrailingSlash(arAbsPathDir);

         if (!midas::filesystem::IsDir(arAbsPathDir))
         {
            MIDASERROR("Directory '"+arAbsPathDir+"' not found.");
         }

         const std::string filename_base = 
            abs_path+"/"+Reduce(Name()+"_"+ToLowerCase(StringFromEnum(id)))+"_";
         auto& v_spec_data = map_v_spec_data.at(id);
         for(auto& spec_data: v_spec_data)
         {
            spec.CalculateAndWriteSpectrum
               (  filename_base + std::get<0>(spec_data)
               ,  std::get<1>(spec_data)
               ,  this->mInterpolTs
               );
            arOs
               << std::setw(aIndent) << ""
               << " - " << std::setw(20) << StringFromEnum(id) + ", " + std::get<0>(spec_data)
               << "(file: " << filename_base + std::get<0>(spec_data) + "_spectrum.dat" << ")"
               << '\n'
               ;
         }

         arOs << std::flush;
         arOs.flags(old_flags);
      }
   }

   /****************************************************************************
    *
    ***************************************************************************/
   void TimTdvccIfc::SanityCheckState
      (  const std::map<ParamID,GeneralMidasVector<Nb>>& arParams
      )  const
   {
      for(const auto& kv: arParams)
      {
         if (kv.first != ParamID::PHASE && kv.second.Size() != SizeParams())
         {
            std::stringstream ss;
            ss << "Param. container size of " << StringFromEnum(kv.first)
               << " (which is " << kv.second.Size()
               << ") != expected SizeParams (which is " << SizeParams()
               << ")."
               ;
            MIDASERROR(ss.str());
         }
      }
   }

   /****************************************************************************
    *
    ***************************************************************************/
   void TimTdvccIfc::SetInitState
      (  std::map<ParamID,GeneralMidasVector<Nb>> arParams
      )
   {
      SanityCheckState(arParams);
      SetInitStateImpl(std::move(arParams));
   }

   /****************************************************************************
    *
    ***************************************************************************/
   void TimTdvccIfc::SetVccGroundState
      (  std::map<ParamID,GeneralMidasVector<Nb>> arParams
      )
   {
      SanityCheckState(arParams);
      SetVccGroundStateImpl(std::move(arParams));
   }

   /****************************************************************************
    *
    ***************************************************************************/
   void TimTdvccIfc::SetOdeInfo
      (  midas::ode::OdeInfo aOdeInfo
      )
   {
      mOdeInfo = std::move(aOdeInfo);
   }

   /****************************************************************************
    *
    ***************************************************************************/
   void TimTdvccIfc::EnableTracking
      (  PropID aID
      )
   {
      if (  aID == PropID::AUTOCORR
         || aID == PropID::AUTOCORR_EXT
         )
      {
         mTrackedAutoCorr.insert(aID);
      }
      else
      {
         mProps[aID];
         mTrackedProps.insert(aID);
      }
   }

   /****************************************************************************
    *
    ***************************************************************************/
   void TimTdvccIfc::EnableTracking
      (  StatID aID
      )
   {
      mStats[aID];
      mTrackedStats.insert(aID);
      mStatsContribs[aID];
      for(const auto& id: GetParamIDs())
      {
         mStatsContribs.at(aID)[id];
      }
   }

   /****************************************************************************
    *
    ***************************************************************************/
   bool TimTdvccIfc::Tracks
      (  PropID aID
      )  const
   {
      return TrackedProps().count(aID) > 0 || TrackedAutoCorr().count(aID) > 0;
   }

   /****************************************************************************
    *
    ***************************************************************************/
   bool TimTdvccIfc::Tracks
      (  StatID aID
      )  const
   {
      return TrackedStats().count(aID) > 0;
   }

   /****************************************************************************
    *
    ***************************************************************************/
   void TimTdvccIfc::ExptValReserve
      (  const Uin aSize
      )
   {
      ExptValReserveImpl(aSize);
   }

   /****************************************************************************
    *
    ***************************************************************************/
   void TimTdvccIfc::EnableExptVal
      (  const OpDef* apOpDef
      ,  ModalIntegrals<Nb>&& arModInts
      )
   {
      if ((mTrackedExptVals.insert(apOpDef->Name())).second)
      {
         mExptVals[apOpDef->Name()];
         EnableExptValImpl(apOpDef, std::move(arModInts));
      }
      else
      {
         std::stringstream ss;
         ss << "Tried to EnableExptVal for '"+apOpDef->Name()+"', but it already existed.\n"
            << "Enabled expt.val. operators are: "
            << mTrackedExptVals
            ;
         MIDASERROR(ss.str());
      }
   }

   /****************************************************************************
    *
    ***************************************************************************/
   void TimTdvccIfc::Evolve
      (
      )
   {
      // Preparations.
      mHamContribExpVal.resize(NumHamiltonianContribs());
      mHamContribCoef.resize(NumHamiltonianContribs());
      mHamContribDeriv.resize(NumHamiltonianContribs());

      // Things saved at interpolated points, we can reserve space for now.
      const Uin num_interpol = GetOdeInfo().template get<In>("OUTPUTPOINTS");
      if (TrackedAutoCorr().count(PropID::AUTOCORR) > 0)
      {
         mAutoCorrA.reserve(num_interpol);
         mAutoCorrB.reserve(num_interpol);
         mAutoCorr.reserve(num_interpol);
      }
      if (SaveFvciVecs())
      {
         mFvciVecsKet.reserve(num_interpol);
         mFvciVecsBra.reserve(num_interpol);
      }

      // For info/output on the way.
      if (PrintoutInterval() == 0.0)
      {
         const auto& time_int = GetOdeInfo().template get<std::pair<Nb, Nb> >("TIMEINTERVAL");
         mPrintoutInterval = (time_int.second - time_int.first)/10;
      }
      PassSettingsToTrfs();

      // Evolve.
      EvolveImpl();
   }

   /****************************************************************************
    *
    ***************************************************************************/
   std::vector<TimTdvccIfc::ifc_step_t> TimTdvccIfc::ExtractInterpolTs
      (
      )  &&
   {
      return std::move(mInterpolTs);
   }

   /****************************************************************************
    *
    ***************************************************************************/
   std::vector<GeneralMidasVector<TimTdvccIfc::ifc_param_t>> TimTdvccIfc::ExtractFvciVecsKet
      (
      )  &&
   {
      return std::move(mFvciVecsKet);
   }

   /****************************************************************************
    *
    ***************************************************************************/
   std::vector<GeneralMidasVector<TimTdvccIfc::ifc_param_t>> TimTdvccIfc::ExtractFvciVecsBra
      (
      )  &&
   {
      return std::move(mFvciVecsBra);
   }

   /****************************************************************************
    *
    ***************************************************************************/
   const std::set<PropID>& TimTdvccIfc::TrackedProps
      (
      )  const
   {
      return mTrackedProps;
   }

   /****************************************************************************
    *
    ***************************************************************************/
   const std::set<PropID>& TimTdvccIfc::TrackedAutoCorr
      (
      )  const
   {
      return mTrackedAutoCorr;
   }

   /****************************************************************************
    *
    ***************************************************************************/
   const std::set<StatID>& TimTdvccIfc::TrackedStats
      (
      )  const
   {
      return mTrackedStats;
   }

   /****************************************************************************
    *
    ***************************************************************************/
   const std::set<std::string>& TimTdvccIfc::TrackedExptVals
      (
      )  const
   {
      return mTrackedExptVals;
   }

   /****************************************************************************
    *
    ***************************************************************************/
   void TimTdvccIfc::SaveAcceptedTime
      (  ifc_step_t aTime
      )
   {
      mStepTs.emplace_back(aTime);
   }

   /****************************************************************************
    *
    ***************************************************************************/
   void TimTdvccIfc::SaveInterpolatedTime
      (  ifc_step_t aTime
      )
   {
      mInterpolTs.emplace_back(aTime);
   }

   /****************************************************************************
    * @param[in] arContribs
    *    The contributions from each term (TdOperTerm/OpDef) of the
    *    Hamiltonian, the coefficients and coef. derivatives at the time in
    *    question.
    *    Structure: term<{contrib,coef,deriv}>
    ***************************************************************************/
   void TimTdvccIfc::SaveEnergyAndContribs
      (  const std::vector<std::tuple<ifc_prop_t,ifc_param_t,ifc_param_t>>& arContribs
      )
   {
      // Save contributions; calc. and save total.
      ifc_prop_t tot(0);
      for(Uin i = 0; i < arContribs.size(); ++i)
      {
         const auto [val, coef, deriv] = arContribs.at(i);
         mHamContribExpVal.at(i).emplace_back(val);
         mHamContribCoef.at(i).emplace_back(coef);
         mHamContribDeriv.at(i).emplace_back(deriv);
         tot += coef*val;
      }
      mProps.at(PropID::ENERGY).emplace_back(tot);
   }

   /****************************************************************************
    * 
    ***************************************************************************/
   void TimTdvccIfc::SavePhase
      (  ifc_param_t aPhase
      )
   {
      mProps.at(PropID::PHASE).emplace_back(aPhase);
   }

   /****************************************************************************
    * 
    ***************************************************************************/
   void TimTdvccIfc::SaveStat
      (  StatID aStatID
      ,  ifc_absval_t aTotVal
      ,  const std::map<ParamID,ifc_absval_t>& arContribs
      )
   {
      mStats.at(aStatID).emplace_back(aTotVal);
      for(auto&& kv: arContribs)
      {
         mStatsContribs.at(aStatID).at(kv.first).emplace_back(kv.second);
      }
   }

   /****************************************************************************
    * 
    ***************************************************************************/
   void TimTdvccIfc::SaveExpVals
      (  const std::map<std::string,ifc_prop_t>& arMap
      )
   {
      for(const auto& kv: arMap)
      {
         try
         {
            mExptVals.at(kv.first).emplace_back(kv.second);
         }
         catch(const std::out_of_range& oor)
         {
            MIDASERROR("Caught out-of-range for '"+kv.first+"'; "+std::string(oor.what()));
         }
      }
   }

   /****************************************************************************
    * 
    ***************************************************************************/
   void TimTdvccIfc::SaveAutoCorr
      (  PropID aPropID
      ,  const std::pair<ifc_prop_t,ifc_prop_t>& arVals
      )
   {
      if (aPropID == PropID::AUTOCORR)
      {
         const auto& [corrA, corrB] = arVals;
         mAutoCorrA.emplace_back(corrA);
         mAutoCorrB.emplace_back(corrB);
         mAutoCorr.emplace_back(.5*(corrA + corrB));
      }
      else if (aPropID == PropID::AUTOCORR_EXT)
      {
         const auto& [corrA, corrB] = arVals;
         mAutoCorrExtA.emplace_back(corrA);
         mAutoCorrExtB.emplace_back(corrB);
         mAutoCorrExt.emplace_back(.5*(corrA + corrB));
      }
      else
      {
         MIDASERROR("Oops, don't know what to do for PropID '"+StringFromEnum(aPropID)+"'.");
      }
   }

   /****************************************************************************
    * 
    ***************************************************************************/
   void TimTdvccIfc::SaveFvciVec
      (  GeneralMidasVector<ifc_param_t>&& arKet
      ,  GeneralMidasVector<ifc_param_t>&& arBra
      )
   {
      mFvciVecsKet.emplace_back(std::move(arKet));
      mFvciVecsBra.emplace_back(std::move(arBra));
   }

   /****************************************************************************
    * 
    ***************************************************************************/
   void TimTdvccIfc::PrintEvolveStatus
      (
      )  const
   {
      if (IoLevel() > 5 && !mStepTs.empty() && mStepTs.back() > mNextPrintoutAtTime)
      {
         mNextPrintoutAtTime = mStepTs.back() + PrintoutInterval();
         Mout << std::setw(3) << "" << "TimTdvcc evolve status; at t = " << mStepTs.back();
         if (CheckLastDerivNorm())
         {
            Mout << "; |deriv| = " << LastDerivNorm();
         }
         Mout << std::endl;
      }
   }

   /****************************************************************************
    *
    ***************************************************************************/
   midas::ode::OdeInfo TimTdvccIfc::DefaultOdeInfo
      (
      )
   {
      midas::ode::OdeInfo ode_info;
      ode_info["DRIVER"] = std::string("MIDAS");
      ode_info["STEPPER"] = std::string("DOPR853");
      ode_info["IOLEVEL"] = 0;
      ::detail::ValidateOdeInput(ode_info);
      return ode_info;
   }

   /****************************************************************************
    * 
    ***************************************************************************/
   bool TimTdvccIfc::AllValuesReal
      (  const std::vector<ifc_prop_t>& arV
      )
   {
      for(const auto& val: arV)
      {
         if (std::imag(val) != 0)
         {
            return false;
         }
      }
      return true;
   }

   /****************************************************************************
    * @note
    *    Uses the given time points for a simple quadrature using the
    *    trapezoidal rule, to account for non-equidistant time points.
    *
    * @param[in] arV
    *    Values to analyse.
    * @param[in] arF
    *    Functor taking an element from arV, returning a real number, e.g. real
    *    part, imag. part or absval.
    * @return
    *    {min, max, average, standard deviation, variance}
    ***************************************************************************/
   template<typename T, class F>
   std::array<typename TimTdvccIfc::ifc_absval_t,5>
   DistAnalImpl
      (  const std::vector<T>& arV
      ,  const std::vector<TimTdvccIfc::ifc_step_t>& arT
      ,  const F& arF
      )
   {
      using ifc_absval_t = TimTdvccIfc::ifc_absval_t;
      using ifc_step_t = TimTdvccIfc::ifc_step_t;
      using midas::util::AbsVal2;

      if (arV.size() != arT.size())
      {
         MIDASERROR("arV.size() (which is "+std::to_string(arV.size())+") != arT.size() (which is "+std::to_string(arT.size())+").");
      }

      ifc_absval_t min = 0;
      ifc_absval_t max = 0;
      ifc_absval_t avg = 0;
      ifc_absval_t std = 0;
      ifc_absval_t var = 0;
      if (!arV.empty())
      {
         // Initialization/values to use if size = 1.
         ifc_absval_t vi = arF(arV.front());
         min = vi;
         max = vi;
         avg = vi;
         
         // If size > 1, we can do trapezoidal quadrature.
         // We can either calculate variance (and standard dev.) from 
         //    (a) var = <(f(t)-f_avg)^2>
         //    (b)     = <f(t)^2> - <f_avg>^2
         // where (b) only requires one loop. However, (a) is numerically more
         // robust, so we go for that one.
         if (arV.size() > 1)
         {
            const ifc_step_t t_range = arT.back() - arT.front();
            ifc_absval_t sum = 0;
            ifc_absval_t vim1 = 0;
            ifc_step_t dt = 0;
            for(Uin i = 1; i < arV.size(); ++i)
            {
               vi   = arF(arV[i]);
               vim1 = arF(arV[i-1]);
               dt   = arT[i] - arT[i-1];
               sum += 0.5*(vi + vim1)*dt;
               min  = std::min(min, vi);
               max  = std::max(max, vi);
            }
            avg = sum / t_range;

            // Second loop for variance.
            // Actually the variance could/should be integrated by the
            // trapezoidal rule squared, since we know it's a squared function,
            // but meh... this will do for now, since we're anyway just giving
            // a rough estimate based on saved time points.
            ifc_absval_t diff2 = 0;
            for(Uin i = 1; i < arV.size(); ++i)
            {
               vi   = arF(arV[i]);
               vim1 = arF(arV[i-1]);
               dt   = arT[i] - arT[i-1];
               diff2 += 0.5*(AbsVal2(vi-avg) + AbsVal2(vim1-avg))*dt;
            }
            var = diff2 / t_range;
            std = sqrt(var);
         }
      }
      return {min, max, avg, std, var};
   }

   /****************************************************************************
    *
    ***************************************************************************/
   std::array<typename TimTdvccIfc::ifc_absval_t,5> TimTdvccIfc::DistAnal
      (  const std::vector<ifc_absval_t>& arVec
      ,  const std::vector<TimTdvccIfc::ifc_step_t>& arT
      )
   {
      return DistAnalImpl(arVec, arT, [](const ifc_absval_t& v)->ifc_absval_t {return v;});
   }

   /****************************************************************************
    *
    ***************************************************************************/
   std::array<typename TimTdvccIfc::ifc_absval_t,5> TimTdvccIfc::DistAnalReal
      (  const std::vector<ifc_prop_t>& arVec
      ,  const std::vector<TimTdvccIfc::ifc_step_t>& arT
      )
   {
      return DistAnalImpl(arVec, arT, [](const ifc_prop_t& v)->ifc_absval_t {return std::real(v);});
   }

   /****************************************************************************
    *
    ***************************************************************************/
   std::array<typename TimTdvccIfc::ifc_absval_t,5> TimTdvccIfc::DistAnalImag
      (  const std::vector<ifc_prop_t>& arVec
      ,  const std::vector<TimTdvccIfc::ifc_step_t>& arT
      )
   {
      return DistAnalImpl(arVec, arT, [](const ifc_prop_t& v)->ifc_absval_t {return std::imag(v);});
   }

   /****************************************************************************
    *
    ***************************************************************************/
   std::array<typename TimTdvccIfc::ifc_absval_t,5> TimTdvccIfc::DistAnalAbs
      (  const std::vector<ifc_prop_t>& arVec
      ,  const std::vector<TimTdvccIfc::ifc_step_t>& arT
      )
   {
      return DistAnalImpl(arVec, arT, [](const ifc_prop_t& v)->ifc_absval_t {return midas::util::AbsVal(v);});
   }

   /****************************************************************************
    *
    ***************************************************************************/
   std::string TimTdvccIfc::GenerateFileName
      (  PropID aID
      )  const
   {
      using namespace midas::input;
      return Reduce(Name()) + "_" + ToLowerCase(StringFromEnum(aID)) + ".dat";
   }

   /****************************************************************************
    *
    ***************************************************************************/
   std::string TimTdvccIfc::GenerateFileName
      (  StatID aID
      )  const
   {
      using namespace midas::input;
      return Reduce(Name()) + "_" + ToLowerCase(StringFromEnum(aID)) + ".dat";
   }

   /****************************************************************************
    *
    ***************************************************************************/
   std::pair<std::vector<std::string>, std::vector<const std::vector<TimTdvccIfc::ifc_prop_t>*>>
   TimTdvccIfc::PrepEnergyForWriting
      (
      )  const
   {
      std::vector<std::string> v_headers = {"t"};
      std::vector<const std::vector<ifc_prop_t>*> v_vecs;
      v_vecs.reserve(1 + 3*NumHamiltonianContribs());

      // Total.
      v_headers.emplace_back("Re[E]");
      v_headers.emplace_back("Im[E]");
      v_vecs.emplace_back(&mProps.at(PropID::ENERGY));

      // Contribs.
      for(Uin i = 0; i < NumHamiltonianContribs(); ++i)
      {
         const std::string s_i = std::to_string(i);
         v_headers.emplace_back("Re[H_"+s_i+"]");
         v_headers.emplace_back("Im[H_"+s_i+"]");
         v_headers.emplace_back("Re[coef_"+s_i+"]");
         v_headers.emplace_back("Im[coef_"+s_i+"]");
         v_headers.emplace_back("Re[deriv_"+s_i+"]");
         v_headers.emplace_back("Im[deriv_"+s_i+"]");

         v_vecs.emplace_back(&mHamContribExpVal.at(i));
         v_vecs.emplace_back(&mHamContribCoef.at(i));
         v_vecs.emplace_back(&mHamContribDeriv.at(i));
      }
      return std::make_pair(std::move(v_headers), std::move(v_vecs));
   }

   /****************************************************************************
    *
    ***************************************************************************/
   std::pair<std::vector<std::string>, std::vector<const std::vector<TimTdvccIfc::ifc_prop_t>*>>
   TimTdvccIfc::PrepPropForWriting
      (  PropID aID
      )  const
   {
      const std::string s = midas::input::ToLowerCase(StringFromEnum(aID));
      std::vector<std::string> v_headers = {"t", "Re["+s+"]", "Im["+s+"]"};
      std::vector<const std::vector<ifc_prop_t>*> v_vecs;
      try
      {
         v_vecs.emplace_back(&mProps.at(aID));
      }
      catch(const std::out_of_range& oor)
      {
         MIDASERROR("Caught out-of-range for PropID '"+StringFromEnum(aID)+"': "+oor.what());
      }
      return std::make_pair(std::move(v_headers), std::move(v_vecs));
   }

   /****************************************************************************
    *
    ***************************************************************************/
   std::pair<std::vector<std::string>, std::vector<const std::vector<TimTdvccIfc::ifc_absval_t>*>>
   TimTdvccIfc::PrepStatForWriting
      (  StatID aID
      )  const
   {
      using namespace midas::input;
      const std::string s = ToLowerCase(StringFromEnum(aID));
      std::vector<std::string> v_headers = {"t", s};
      std::vector<const std::vector<ifc_absval_t>*> v_vecs;
      try
      {
         v_vecs.emplace_back(&mStats.at(aID));
         
         for(const auto& paramid: GetParamIDs())
         {
            v_headers.emplace_back(s+"("+ToLowerCase(StringFromEnum(paramid))+")");
            const auto& contrib = mStatsContribs.at(aID);
            try
            {
               v_vecs.emplace_back(&contrib.at(paramid));
            }
            catch(const std::out_of_range& oor)
            {
               MIDASERROR("Caught out-of-range for ParamID '"+StringFromEnum(paramid)+"': "+oor.what());
            }
         }
      }
      catch(const std::out_of_range& oor)
      {
         MIDASERROR("Caught out-of-range for StatID '"+StringFromEnum(aID)+"': "+oor.what());
      }


      return std::make_pair(std::move(v_headers), std::move(v_vecs));
   }

   /****************************************************************************
    *
    ***************************************************************************/
   std::pair<std::vector<std::string>, std::vector<const std::vector<TimTdvccIfc::ifc_prop_t>*>>
   TimTdvccIfc::PrepAutoCorrForWriting
      (  PropID aID
      )  const
   {
      std::vector<std::string> v_headers = 
         {  "t"
         ,  "Re[S]"
         ,  "Im[S]"
         ,  "Re[S_{<0|t>}]"
         ,  "Im[S_{<0|t>}]"
         ,  "Re[S_{<t|0>^*}]"
         ,  "Im[S_{<t|0>^*}]"
         };
      std::vector<const std::vector<ifc_prop_t>*> v_vecs;
      if (aID == PropID::AUTOCORR)
      {
         v_vecs =
            {  &mAutoCorr
            ,  &mAutoCorrA
            ,  &mAutoCorrB
            };
      }
      else if(aID == PropID::AUTOCORR_EXT)
      {
         v_vecs =
            {  &mAutoCorrExt
            ,  &mAutoCorrExtA
            ,  &mAutoCorrExtB
            };
      }
      else
      {
         MIDASERROR("Unexpedted PropID: "+StringFromEnum(aID));
      }
      return std::make_pair(std::move(v_headers), std::move(v_vecs));
   }

   /****************************************************************************
    *
    ***************************************************************************/
   std::pair<std::vector<std::string>, std::vector<const std::vector<TimTdvccIfc::ifc_prop_t>*>>
   TimTdvccIfc::PrepExptValForWriting
      (  const std::string& arName
      )  const
   {
      const std::string& s = arName;
      std::vector<std::string> v_headers = {"t", "Re["+s+"]", "Im["+s+"]"};
      std::vector<const std::vector<ifc_prop_t>*> v_vecs;
      try
      {
         v_vecs.emplace_back(&mExptVals.at(s));
      }
      catch(const std::out_of_range& oor)
      {
         MIDASERROR("Caught out-of-range for exp.val name '"+s+"': "+oor.what());
      }
      return std::make_pair(std::move(v_headers), std::move(v_vecs));
   }

   /****************************************************************************
    *
    ***************************************************************************/
   bool TimTdvccIfc::PrintPropToFile
      (  const std::string& arAbsPathFile
      ,  PropID aID
      )  const
   {
      using table_t = std::pair<std::vector<std::string>, std::vector<const std::vector<ifc_prop_t>*>>;
      std::unique_ptr<table_t> p = nullptr;
      switch(aID)
      {
         case PropID::ENERGY:
            p = std::make_unique<table_t>(PrepEnergyForWriting());
            break;
         case PropID::PHASE:
            p = std::make_unique<table_t>(PrepPropForWriting(aID));
            break;
         default:
            MidasWarning("Called PrintPropToFile for '"+StringFromEnum(aID)+"'; don't know what to do.");
            return false;
            break;
      }

      if (p)
      {
         PrintTableToFile(arAbsPathFile, p->first, mStepTs, p->second);
         return true;
      }
      else
      {
         MIDASERROR("nullptr, something wrong.");
         return false;
      }
   }

   /****************************************************************************
    *
    ***************************************************************************/
   bool TimTdvccIfc::PrintStatToFile
      (  const std::string& arAbsPathFile
      ,  StatID aID
      )  const
   {
      using table_t = std::pair<std::vector<std::string>, std::vector<const std::vector<ifc_absval_t>*>>;
      std::unique_ptr<table_t> p = nullptr;
      switch(aID)
      {
         case StatID::FVCINORM2:
         case StatID::NORM2:
         case StatID::DNORM2INIT:
         case StatID::DNORM2VCCGS:
            p = std::make_unique<table_t>(PrepStatForWriting(aID));
            break;
         default:
            MidasWarning("Called PrintStatToFile for '"+StringFromEnum(aID)+"'; don't know what to do.");
            return false;
            break;
      }

      if (p)
      {
         PrintTableToFile(arAbsPathFile, p->first, mStepTs, p->second);
         return true;
      }
      else
      {
         MIDASERROR("nullptr, something wrong.");
         return false;
      }
   }

   /****************************************************************************
    *
    ***************************************************************************/
   bool TimTdvccIfc::PrintAutoCorrToFile
      (  const std::string& arAbsPathFile
      ,  PropID aID
      )  const
   {
      using table_t = std::pair<std::vector<std::string>, std::vector<const std::vector<ifc_prop_t>*>>;
      std::unique_ptr<table_t> p = nullptr;
      switch(aID)
      {
         case PropID::AUTOCORR:     // fall through
         case PropID::AUTOCORR_EXT:
            p = std::make_unique<table_t>(PrepAutoCorrForWriting(aID));
            break;
         default:
            MidasWarning("Called PrintAutoCorrToFile for '"+StringFromEnum(aID)+"'; don't know what to do.");
            return false;
            break;
      }

      if (p)
      {
         PrintTableToFile(arAbsPathFile, p->first, mInterpolTs, p->second);
         return true;
      }
      else
      {
         MIDASERROR("nullptr, something wrong.");
         return false;
      }
   }

   /****************************************************************************
    *
    ***************************************************************************/
   bool TimTdvccIfc::PrintExptValToFile
      (  const std::string& arAbsPathFile
      ,  const std::string& arName
      )  const
   {
      using table_t = std::pair<std::vector<std::string>, std::vector<const std::vector<ifc_prop_t>*>>;
      std::unique_ptr<table_t> p = nullptr;
      if (TrackedExptVals().count(arName) > 0)
      {
         p = std::make_unique<table_t>(PrepExptValForWriting(arName));
      }
      else
      {
         MidasWarning("Called PrintExptValToFile for '"+arName+"'; don't know what to do.");
         return false;
      }

      if (p)
      {
         PrintTableToFile(arAbsPathFile, p->first, mStepTs, p->second);
         return true;
      }
      else
      {
         MIDASERROR("nullptr, something wrong.");
         return false;
      }
   }


} /* namespace midas::tdvcc */
