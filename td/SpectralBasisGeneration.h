/*
************************************************************************
*
* @file                 SpectralBasisGeneration.h
*
* Created:              04-12-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Function for generating spectral basis for a given Hermitian operator
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef SPECTRALBASISGENERATION_H_INCLUDED
#define SPECTRALBASISGENERATION_H_INCLUDED

#include <string>
#include "inc_gen/TypeDefs.h"
#include "mmv/DataCont.h"

// Forward decl
class BasDef;

namespace midas::td
{

/**
 * Run VSCF to generate spectral-basis modals
 *
 * @param aOperName     Name of operator
 * @param apBasDef      Basis to run calculation in
 * @param aIoLevel      IoLevel for VSCF
 *
 * @return
 *    Number of coefs and name of file with modals
 **/
std::pair<In, std::string> GenerateSpectralBasisModals
   (  const std::string& aOperName
   ,  const BasDef* const apBasDef
   ,  In aIoLevel
   );

} /* namespace midas::td */

#endif /* SPECTRALBASISGENERATION_H_INCLUDED */
