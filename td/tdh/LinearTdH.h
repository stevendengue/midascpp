/*
************************************************************************
*
* @file                 LinearTdH.h
*
* Created:              03-04-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk) and Kasper Monrad
*
* Short Description:    LinearTdH class and function declarations.
*
* Last modified:        
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef LINEARTDH_H_INCLUDED
#define LINEARTDH_H_INCLUDED

#include <vector>

#include "TdHBase.h"

#include "mmv/MidasMatrix.h"

/**
 * Implementation of linearly-parametrized TDH
 **/
class LinearTdH
   :  public TdHBase
{
   public:
      // Typdefs
      using param_t = typename TdHBase::param_t;
      using step_t = typename TdHBase::step_t;
      using vec_t = typename TdHBase::vec_t;
      using mat_t = GeneralMidasMatrix<param_t>; // for Fock matrix

   private:
      //! Constraint type
      midas::tdh::constraintID mConstraint = midas::tdh::constraintID::ZERO;

      //! Normalize vector in ProcessNewVectorImpl
      bool mNormalize = true;

      //! Initial wave function (at t = t_0). Niels: Must be mutable due to stupid MidasVector IO design.
      mutable vec_t mInitialOccModals;

      //! Address of the occupied modals in mInitialOccModals (and mOccModalsAndPhase for LinearTdH)
      std::vector<In> mOccModalOffsets;


      //! Implementation of Derivative
      vec_t DerivativeImpl
         (  step_t
         ,  const vec_t&
         ,  size_t&
         )  const override;

      //! Implementation of ProcessNewVector
      bool ProcessNewVectorImpl
         (  vec_t& arY
         ,  const vec_t& aYOld
         ,  step_t& aT
         ,  step_t aTOld
         ,  vec_t& arDyDt
         ,  const vec_t& aDyDtOld
         ,  step_t& arStep
         ,  size_t& arNDerivFail
         )  override;

      //! Implementation of PrepareIntegration
      void PrepareIntegrationImpl
         (  vec_t&
         )  override;

      //! Implementation of ShapedZeroVector
      vec_t ShapedZeroVectorImpl
         (
         )  const override;

      //! Implementation of CalculateAutoCorr
      param_t CalculateAutoCorrImpl
         (  step_t
         ,  const vec_t&
         )  const override;

      //! Implementation of CalculateHalfTimeAutoCorr
      param_t CalculateHalfTimeAutoCorrImpl
         (  step_t
         ,  const vec_t&
         )  const override;

      //! Calculate one-mode wave function at Q point
      vec_t GetWaveFunctionCoefficientsImpl
         (  LocalModeNr
         ,  step_t
         ,  const vec_t&
         )  const override;

      //! Get generalized phase factor (F) for wave function
      param_t GetPhaseFactorImpl
         (  step_t
         ,  const vec_t&
         )  const override;

      //! Implementation of Finalize
      void FinalizeImpl
         (  const std::vector<step_t>&
         ,  const std::vector<vec_t>&
         )  override;

      //! Update H integrals
      void PrepareDerivativeCalculationImpl
         (  const vec_t&
         )  override;

      //! Update property integrals
      void UpdatePropertyIntegralsImpl
         (  const vec_t&
         )  override;

      //! Number of params for mode
      In NParams
         (  LocalModeNr
         )  const override;

      //! Offset for mode
      In Offset
         (  LocalModeNr
         )  const override;

      //! Get transformation matrix for spectral basis
      DataCont ConstructSpectralBasisTransformation
         (  const DataCont&
         )  const override;

      //! Type
      midas::tdh::tdhID TypeImpl
         (
         )  const override
      {
         return midas::tdh::tdhID::LINEAR;
      }

      //! Normalize modals
      bool NormalizeModals
         (  vec_t&
         )  const;

      //! Construct one-mode Fock matrix (with constraint operator)
      mat_t ConstructEffectiveOneModeHamiltonian
         (  LocalModeNr
         ,  size_t&
         )  const;


   public:
      //! Constructor
      LinearTdH
         (  TdHCalcDef*
         ,  OpDef*
         ,  BasDef*
         ,  const std::string&
         );
};

#endif /* LINEARTDH_H_INCLUDED */
