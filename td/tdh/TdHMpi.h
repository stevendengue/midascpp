/*
************************************************************************
*
* @file                 TdHMpi.h
*
* Created:              10-07-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Mpi stuff for TdH
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef TDHMPI_H_INCLUDED
#define TDHMPI_H_INCLUDED

#ifdef VAR_MPI

namespace midas::tdh
{
/**
 * Enum of methods for syncing data between MPI ranks.
 **/
enum class mpiComm : int
{  BCAST = 0
,  IBCAST
};

} /* namespace midas::tdh */

#endif /* VAR_MPI */

#endif /* TDHMPI_H_INCLUDED */
