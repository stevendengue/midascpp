/*
************************************************************************
*
* @file                 TdHIntegrals.h
*
* Created:              13-04-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Time-dependent integrals
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef TDHINTEGRALS_H_INCLUDED
#define TDHINTEGRALS_H_INCLUDED

#include "td/tdh/TdHMpi.h"
#include "td/PrimitiveIntegrals.h"

//! Forward decl
class TdHCalcDef;

/**
 * Base class for TdH integrals
 **/
class TdHIntegralsBase
   :  public midas::td::PrimitiveIntegrals<std::complex<Nb>>
{
   public:
      //! Alias
      using Prim = midas::td::PrimitiveIntegrals<std::complex<Nb>>;
      using param_t = typename Prim::param_t;
      using step_t = typename Prim::step_t;
      using vec_t = typename Prim::vec_t;
      using mat_t = typename Prim::mat_t;

      //! Integral types
      enum class tdhintType : int
      {  BASE = 0
      ,  EXPONENTIAL
      };

   protected:
      //! OpDef pointer
      const OpDef* const mpOpDef;

      /** @name Time-dependent integrals **/
      //!@{
      //! Occupied integrals in time-dependent basis. Accessed with [LocalModeNr][LocalOperNr].
      std::vector<std::vector<param_t>> mOneModeIntOcc;
      //!@}

   private:
      //! Implementation of integral update
      virtual void UpdateImpl
         (  const vec_t&
         ,  const std::vector<In>&
         ,  const std::vector<step_t>& = {}
         );

   protected:
#ifdef VAR_MPI
      //! How to communicate
      midas::tdh::mpiComm mMpiComm = midas::tdh::mpiComm::BCAST;

      //! Sync time-dependent integrals between nodes (calls implementation)
      void SyncTimeDependentIntegrals
         (
         );

      //! Check if this node should calculate integrals
      bool AssignToNode
         (  LocalModeNr
         )  const;

      //! Recv time-dependent integrals
      virtual void RecvTimeDependentIntegrals
         (  LocalModeNr
         ,  In
         );

      //! Send time-dependent integrals
      virtual void SendTimeDependentIntegrals
         (  LocalModeNr
         ,  In
         )  const;

      //! Bcast time-dependent integrals
      virtual void BcastTimeDependentIntegrals
         (  LocalModeNr
         ,  In
         );

      //! Ibcast time-dependent integrals
      virtual void IbcastTimeDependentIntegrals
         (  LocalModeNr
         ,  In
         ,  std::vector<MPI_Request>&
         );
#endif /* VAR_MPI */


   public:
      //! Constructor
      TdHIntegralsBase() = delete;

      //! Constructor from calcdef, opdef, basdef
      TdHIntegralsBase
         (  const TdHCalcDef* const
         ,  const OpDef* const
         ,  const BasDef* const
         );

      //! Destructor
      virtual ~TdHIntegralsBase() = default;

      //! Type
      virtual tdhintType Type
         (
         )  const;

      //! Update time-dependent integrals
      void Update
         (  const vec_t&
         ,  const std::vector<In>&
         ,  const std::vector<step_t>& = {}
         );

      //! Expectation value
      param_t ExpectationValue
         (  LocalModeNr = -I_1
         )  const;

      //! Get integral
      param_t GetOccIntegral
         (  LocalModeNr
         ,  LocalOperNr
         )  const;

      //! Factory
      static std::unique_ptr<TdHIntegralsBase> Factory
         (  TdHCalcDef*
         ,  OpDef*
         ,  BasDef*
         );
};


/**
 * Derived class for exponential TdH
 **/
class ExponentialTdHIntegrals
   :  public TdHIntegralsBase
{
   private:
      //! Virtual-occupied time-dependent integrals. Accessed as [LocalModeNr][LocalOperNr][vir. index]
      std::vector<std::vector<vec_t> > mOneModeIntVirOcc;

      //! Overload of implementation of update
      void UpdateImpl
         (  const vec_t&
         ,  const std::vector<In>&
         ,  const std::vector<step_t>&
         )  override;

      //! Update integrals for one mode
      void UpdateModeIntegrals
         (  LocalModeNr
         ,  const vec_t&
         ,  const std::vector<In>&
         ,  const std::vector<step_t>&
         );

   protected:
#ifdef VAR_MPI
      //! Recv time-dependent integrals
      void RecvTimeDependentIntegrals
         (  LocalModeNr
         ,  In
         )  override;

      //! Send time-dependent integrals
      void SendTimeDependentIntegrals
         (  LocalModeNr
         ,  In
         )  const override;

      //! Bcast time-dependent integrals
      void BcastTimeDependentIntegrals
         (  LocalModeNr
         ,  In
         )  override;

      //! Ibcast time-dependent integrals
      void IbcastTimeDependentIntegrals
         (  LocalModeNr
         ,  In
         ,  std::vector<MPI_Request>&
         )  override;
#endif /* VAR_MPI */


   public:
      //! Constructor from calcdef, opdef, basdef
      ExponentialTdHIntegrals
         (  const TdHCalcDef* const
         ,  const OpDef* const
         ,  const BasDef* const
         );

      //! Type
      TdHIntegralsBase::tdhintType Type
         (
         )  const override;

      //! Get vir-occ integral
      const vec_t& GetVirOccIntegrals
         (  LocalModeNr
         ,  LocalOperNr
         )  const;

      //! Transform time-independent integrals when kappa is reset
      void ResetKappa
         (  const std::set<LocalModeNr>&
         ,  const vec_t&
         ,  const std::vector<In>&
         ,  const std::vector<step_t>&
         );
};

#endif /* TDHINTEGRALS_H_INCLUDED */
