/*
************************************************************************
*
* @file                 TdHBase.cc
*
* Created:              03-04-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    TdHBase class implementation.
*
* Last modified:        Mon Nov 02, 2015  02:36PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "TdHBase.h"
#include "LinearTdH.h"
#include "ExponentialTdH.h"
#include "input/TdHCalcDef.h"
#include "mmv/DataCont.h"
#include "mpi/Impi.h"

#include "vscf/Vscf.h"
#include "input/Input.h"

#include "util/SanityCheck.h"

#include "td/SpectralBasisGeneration.h"

/**
 * Constructor. Initialize integrals.
 *
 * @param apCalcDef     TdH calcdef
 * @param apOpDef       Operator defs
 * @param apBasDef      Basis defs
 * @param aModalsName   Name of file containing initial modals
 **/
TdHBase::TdHBase
   (  TdHCalcDef* apCalcDef
   ,  OpDef* apOpDef
   ,  BasDef* apBasDef
   ,  const std::string& aModalsName
   )
   :  Spectrum
         (  apCalcDef
         )
   ,  mpTdHCalcDef
         (  apCalcDef
         )
   ,  mpOpDef
         (  apOpDef
         )
   ,  mpBasDef
         (  apBasDef
         )
   ,  mName
         (  mpTdHCalcDef->Name()
         )
   ,  mNModes
         (  mpBasDef->GetmNoModesInBas()
         )
   ,  mInitialModalsName
         (  aModalsName
         )
   ,  mIoLevel
         (  mpTdHCalcDef->IoLevel()
         )
   ,  mImagTime
         (  mpTdHCalcDef->ImagTime()
         )
   ,  mPrimitiveModalOffsets
         (  mNModes
         )
   ,  mIntegrals
         (  TdHIntegralsBase::Factory(apCalcDef, apOpDef, apBasDef)
         )
   ,  mWfOutModes
         (  mpTdHCalcDef->WfOutModes()
         )
   ,  mTwoModeDensityPairs
         (  mpTdHCalcDef->TwoModeDensityPairs()
         )
   ,  mWritePrimitiveBasisForModes
         (  mpTdHCalcDef->WritePrimitiveBasisForModes()
         )
   ,  mWfGridDensity
         (  mpTdHCalcDef->WfGridDensity()
         )
   ,  mExptValOpers
         (  mpTdHCalcDef->ExptValOpers()
         )
#ifdef VAR_MPI
   ,  mMpiComm
         (  mpTdHCalcDef->MpiComm()
         )
#endif /* VAR_MPI */
{
   // Check that mode labels exist in mWfOutModes, mTwoModeDensityPairs, mWritePrimitiveBasisForModes
   const auto& mode_map = gOperatorDefs.GetModeMap();
   for(std::set<std::string>::iterator it = mWfOutModes.begin(); it != mWfOutModes.end(); ++it)
   {
      if (  mode_map.find(*it) == mode_map.end()
         )
      {
         MidasWarning("TdHBase: Mode label '" + *it + "' not found in gOperatorDefs. Erase from mWfOutModes!");
         mWfOutModes.erase(it);
      }
   }
   for(std::set<std::string>::iterator it = mWritePrimitiveBasisForModes.begin(); it != mWritePrimitiveBasisForModes.end(); ++it)
   {
      if (  mode_map.find(*it) == mode_map.end()
         )
      {
         MidasWarning("TdHBase: Mode label '" + *it + "' not found in gOperatorDefs. Erase from mWritePrimitiveBasisForModes!");
         mWritePrimitiveBasisForModes.erase(it);
      }
   }
   for(std::set<std::set<std::string>>::iterator it_pair=mTwoModeDensityPairs.begin(); it_pair != mTwoModeDensityPairs.end(); ++it_pair)
   {
      for(std::set<std::string>::iterator it_mode=it_pair->begin(); it_mode != it_pair->end(); ++it_mode)
      {
         if (  mode_map.find(*it_mode) == mode_map.end()
            )
         {
            MidasWarning("TdHBase: Mode label '" + *it_mode + "' not found in gOperatorDefs. Erase from mTwoModeDensityPairs!");
            mTwoModeDensityPairs.erase(it_pair);
            break;
         }
      }
   }

   // Set properties
   static const std::map<std::string, TdHBase::propID> property_map =
   {  {"ENERGY", TdHBase::propID::ENERGY}
   ,  {"PHASE", TdHBase::propID::PHASE}
   ,  {"AUTOCORR", TdHBase::propID::AUTOCORR}
   ,  {"HALFTIMEAUTOCORR", TdHBase::propID::HALFTIMEAUTOCORR}
   ,  {"KAPPANORM", TdHBase::propID::KAPPANORM}
   };
   const auto& prop_string_set = this->mpTdHCalcDef->Properties();
   for(const auto& prop_string : prop_string_set)
   {
      if (  property_map.find(prop_string) != property_map.end()
         )
      {
         this->mProperties.insert(property_map.at(prop_string));
      }
      else
      {
         MidasWarning("TDH: Unrecognized property: '" + prop_string + "'.");
      }
   }

   // Set spectra
   static const std::map<std::string, TdHBase::specID> spectra_map =
   {  {"AUTOCORR", TdHBase::specID::AUTOCORR}
   ,  {"HALFTIMEAUTOCORR", TdHBase::specID::HALFTIMEAUTOCORR}
   };
   const auto& spec_string_set = this->mpTdHCalcDef->Spectra();
   for(const auto& spec_string : spec_string_set)
   {
      if (  spectra_map.find(spec_string) != spectra_map.end()
         )
      {
         this->mSpectra.insert(spectra_map.at(spec_string));
      }
      else
      {
         MidasWarning("TDH: Unrecognized spectrum: '" + spec_string + "'.");
      }
   }

   // Spectral basis
   for(LocalModeNr imode=I_0; imode<this->mNModes; ++imode)
   {
      if (  this->mpBasDef->OneModeType(imode) != "HO"
         )
      {
         if (  gDebug
            || this->mIoLevel >= I_5
            )
         {
            Mout  << " TdHBase: Non-orthogonal basis set. Transform all modes to spectral basis!" << std::endl;
         }
         this->mpTdHCalcDef->SetSpectralBasis(true);
         break;
      }
   }
   const auto& limits = this->mpTdHCalcDef->ModalBasisLimits(); // may be empty
   bool has_limits = !limits.empty();

   // Sanity check
   if (  has_limits
      )
   {
      if (  limits.size() != this->mNModes
         )
      {
         MIDASERROR("TdHBase: We need a modal limit for each mode!");
      }
      else
      {
         for(LocalModeNr imode=I_0; imode<mNModes; ++imode)
         {
            GlobalModeNr i_g_mode               = mpOpDef->GetGlobalModeNr(imode);
            LocalModeNr i_bas_mode              = mpBasDef->GetLocalModeNr(i_g_mode);
            In nbas                             = mpBasDef->Nbas(i_bas_mode);
            
            if (  nbas < limits[imode]
               )
            {
               MIDASERROR("Modal-basis limit > nbas (" + std::to_string(limits[imode]) + " > " + std::to_string(nbas) + ") for mode " + std::to_string(i_g_mode));
            }
         }
      }
   }

   // Set up sizes
   In n_input_modal_coef   = I_0;   // Number of modal coefficients to read in from VSCF
   In n_modal_coef_occ     = I_0;   // Number of modal coefficients in TDH

   for(LocalModeNr i_op_mode=I_0; i_op_mode<mNModes; ++i_op_mode)
   {
      this->mPrimitiveModalOffsets[i_op_mode] = n_input_modal_coef;
      GlobalModeNr i_g_mode               = mpOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr i_bas_mode              = mpBasDef->GetLocalModeNr(i_g_mode);
      In nbas                             = mpBasDef->Nbas(i_bas_mode);
      n_modal_coef_occ                   += has_limits ? std::min(limits[i_op_mode], nbas) : nbas;    // Check if we are limiting the basis
      n_input_modal_coef                 += nbas*nbas;

      Nb time_scale = 2*C_PI/mpOpDef->HoOmega(i_op_mode);

      if (  gDebug
         || this->mIoLevel >= I_8
         )
      {
         Mout << "mode " << i_op_mode << ": omega = " << mpOpDef->HoOmega(i_op_mode) << ", relevant timescale T = " << time_scale << std::endl;
      }
   }
   this->mNOccModalCoef = n_modal_coef_occ;
   this->mNInputModalCoef = n_input_modal_coef;
   if (  gDebug
      || this->mIoLevel >= I_10
      )
   {
      Mout << "mNOccModalCoef = " << mNOccModalCoef << std::endl;
      Mout << "mNInputModalCoef = " << mNInputModalCoef << std::endl;
   }

   // Set up property integrals
   for(const auto& oper_name : mExptValOpers)
   {
      // Find OpDef for name
      auto iop = I_0;
      for(In i=I_0; i<gOperatorDefs.GetNrOfOpers(); ++i)
      {
         if (  gOperatorDefs[i].Name() == oper_name
            )
         {
            iop = i;
            break;
         }
      }

      // Create integrals
      this->mPropertyIntegrals[oper_name] = TdHIntegralsBase::Factory(mpTdHCalcDef, &gOperatorDefs[iop], mpBasDef);

      // Add container for saved values
      this->mExptVals[oper_name] = std::vector<std::vector<param_t> >();
   }


   // Add modes from two-mode pairs to mWfOutModes
   for(const auto& modepair : this->mTwoModeDensityPairs)
   {
      for(const auto& mode : modepair)
      {
         this->mWfOutModes.insert(mode);
      }
   }

   // Reserve space in mAutoCorr and mHalftimeAutoCorr
   const auto& odeinfo = this->mpTdHCalcDef->OdeInfo();
   auto npoints = odeinfo.template get<In>("OUTPUTPOINTS");
   auto save_ys = odeinfo.template get<bool>("DATABASESAVEVECTORS");
   if (  this->mProperties.find(propID::AUTOCORR) != this->mProperties.end()
      && !save_ys
      )
   {
      this->mAutoCorrs.reserve(npoints);
   }
   if (  this->mProperties.find(propID::HALFTIMEAUTOCORR) != this->mProperties.end()
      && !save_ys
      )
   {
      this->mHalftimeAutoCorrs.reserve(npoints);
   }
}

/**
 * @param aModeNr
 * @return
 *    Number of basis functions for mode
 **/
In TdHBase::NBas
   (  LocalModeNr aModeNr
   )  const
{
   const auto& limits = this->mpTdHCalcDef->ModalBasisLimits();
   bool has_limits = !limits.empty();
   if (  has_limits
      )
   {
      return limits[aModeNr];
   }
   else
   {
      GlobalModeNr i_g_mode  = mpOpDef->GetGlobalModeNr(aModeNr);
      LocalModeNr i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
      return mpBasDef->Nbas(i_bas_mode);
   }
}

/**
 * Calculate derivatives: dy/dt
 *
 * @param aT            t
 * @param aY            y vector
 *
 * @return
 *    dy/dt vector
 **/
typename TdHBase::vec_t TdHBase::Derivative
   (  step_t aT
   ,  const vec_t& aY
   )
{
   // Update Hamiltonian integrals and kappa norms
   this->PrepareDerivativeCalculation(aT, aY);

   // Call implementation
   return this->DerivativeImpl(aT, aY, this->mNScreenedTerms);
}

/**
 * Update property integrals and save properties.
 * NB: Assumes that mIntegrals has already been updated!
 *
 * @param aT
 * @param aY
 * @return
 *    True if the ODE integrator should stop here
 **/
bool TdHBase::AcceptedStep
   (  step_t aT
   ,  const vec_t& aY
   )
{
   // Update property integrals
   this->UpdatePropertyIntegralsImpl(aY);

   // Save properties.
   // Here, we assume that mIntegrals has already been updated
   // NB: This is true for the MidasOdeDriver, since the derivative has always been calculated at 
   // the new point. But what about GSL?
   this->SaveProperties(aT, aY);

   // We never stop the ODE integrator early.
   // However, it could be a good idea for imaginary-time propagation...
   return false;
}

/**
 * Save data for interpolated step.
 *
 * @param aT
 * @param aY
 **/
void TdHBase::InterpolatedPoint
   (  step_t aT
   ,  const vec_t& aY
   )
{
   // Save autocorrelation functions
   for(const auto& prop : this->mProperties)
   {
      switch   (  prop
               )
      {
         case TdHBase::propID::AUTOCORR:
         {
            this->mAutoCorrs.emplace_back(this->CalculateAutoCorr(aT, aY));
            break;
         }
         case TdHBase::propID::HALFTIMEAUTOCORR:
         {
            this->mHalftimeAutoCorrs.emplace_back(this->CalculateHalfTimeAutoCorr(aT, aY));
            break;
         }
         default:
         {
            break;
         }
      }
   }
}

/**
 * Process y vector after step. Return false if the vector is not accepted.
 *
 * @param arY           y vector
 * @param aYOld         Previous y vector
 * @param arT           t
 * @param aTOld         Previous t
 * @param arDyDt        dy/dt
 * @param aDyDtOld      Previous dy/dt
 * @param arStep        Step size (modified if step is rejected)
 * @param arNDerivFail  Number of failed derivative calculations before this step.
 *
 * @return
 *    Step accepted?
 **/
bool TdHBase::ProcessNewVector
   (  vec_t& arY
   ,  const vec_t& aYOld
   ,  step_t& arT
   ,  step_t aTOld
   ,  vec_t& arDyDt
   ,  const vec_t& aDyDtOld
   ,  step_t& arStep
   ,  size_t& arNDerivFail
   )
{
   // Check sanity of vector
   for(size_t i=0; i<arY.size(); ++i)
   {
      if (  !midas::util::IsSane(arY[i])
         )
      {
         if (  gDebug
            || this->mIoLevel >= I_8
            )
         {
            Mout  << " New vector contains nan or inf. Retry step with reduced step size." << std::endl;
         }

         arStep /= 2;

         // Reset
         arY = aYOld;
         arT = aTOld;
         arDyDt = aDyDtOld;

         return false;
      }
   }

   // Check if phase is real
   const auto& f = arY[arY.size()-1];
   if (  !libmda::numeric::float_numeq_zero(f.imag(), f.real(), 100)
      && !this->mImagTime
      )
   {
      Mout  << " TdH: Complex F(t)!    F(t) = " << f << std::endl;
      MidasWarning("Complex F(t) detected for TdH!");
   }

   // Call implementation
   return this->ProcessNewVectorImpl(arY, aYOld, arT, aTOld, arDyDt, aDyDtOld, arStep, arNDerivFail);
}

/**
 * Set name
 *
 * @param aName      name
 **/
void TdHBase::SetName
   (  const std::string& aName
   )
{
   this->mName = aName;
}

/**
 * Set name of ref oper
 *
 * @param aName      name
 **/
void TdHBase::SetReferenceOperName
   (  const std::string& aName
   )
{
   this->mReferenceOperName = aName;
}

/**
 * Prepare integration
 *
 * @param arWf    Initial wave function 
 **/
void TdHBase::PrepareIntegration
   (  vec_t& arWf
   )
{
   bool spec = this->mpTdHCalcDef->SpectralBasis();
   auto spec_oper = this->mpTdHCalcDef->SpectralBasisOper();

   // If using spectral basis, get the spectral-basis modals
   if (  spec
      )
   {
      DataCont init_modals;

      if (  spec_oper == ""
         || spec_oper == this->mReferenceOperName
         )
      {
         // Get initial occupied modals
         Mout  << " TdHBase: Getting initial modals from file: " << mInitialModalsName << std::endl;
         init_modals.GetFromExistingOnDisc(this->mNInputModalCoef, mInitialModalsName);
         init_modals.SaveUponDecon(true);
      }
      else
      {
         Mout  << " TdHBase: Run VSCF calculation on operator '" << spec_oper << "' to obtain spectral basis." << std::endl;
         auto [ncoef, label] = midas::td::GenerateSpectralBasisModals(spec_oper, this->mpBasDef, this->mIoLevel);
         init_modals.GetFromExistingOnDisc(ncoef, label);
         init_modals.SaveUponDecon(true);
      }
   
      // Modify spectral-basis modals
      this->mSpectralBasisModals = this->ConstructSpectralBasisTransformation(init_modals);

      // Write spectral basis
      if (  !this->mWritePrimitiveBasisForModes.empty()
         )
      {
         // Loop over modes. Write in separate files.
         for(const auto& mode : this->mWritePrimitiveBasisForModes)
         {
            // Set up some stuff...
            GlobalModeNr i_g_mode  = gOperatorDefs.ModeNr(mode); //mpOpDef->GetGlobalModeNr(mode);
            LocalModeNr i_op_mode = mpOpDef->GetLocalModeNr(i_g_mode);
            LocalModeNr i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
            In nbas = this->mpBasDef->Nbas(i_bas_mode);

            // Check if we have limits. Otherwise, we use the same basis size.
            const auto& limits = this->mpTdHCalcDef->ModalBasisLimits();
            bool has_limits = !limits.empty();

            In lim  = has_limits ? std::min(limits[i_bas_mode], nbas) : nbas;

            // Set up output stream
            midas::mpi::OFileStream wf_out(gAnalysisDir + "/" + this->Name() + "_primbasis_" + mode + ".dat");
            wf_out << std::scientific << std::setprecision(16);
            size_t width = 30;

            // Get transformation matrix.
            MidasMatrix transp_modal_transform(lim, nbas);
            this->mSpectralBasisModals.DataIo(IO_GET, transp_modal_transform, lim*nbas, lim, nbas, this->mPrimitiveModalOffsets[i_op_mode]);

            // Print header
            std::string s1 = "# " + mode;
            wf_out   << std::setw(width) << s1;
            for(In ibas=I_0; ibas<lim; ++ibas)
            {
               std::string s = "chi_" + std::to_string(ibas) + "(" + mode + ")";
               wf_out   << std::setw(width) << s;
            }
            wf_out   << std::endl;
            
   
            // Set up grid. Use grid bounds from OneModeBasDef if Bspline or Gaussian, use turning points for HO
            auto bastype = this->mpBasDef->OneModeType(i_bas_mode);
            Nb r_max = C_0;
            Nb r_min = C_0;

            if (  bastype == "HO"
               )
            {
               In n_max = I_10;  // use HO turning point for 10th eigenstate
               Nb scale = C_2;   // And scale this interval by 2...
               Nb omega = this->mpOpDef->HoOmega(i_op_mode);
            
               r_max = std::sqrt(C_2*(Nb(n_max)+C_I_2)/(omega))*scale;  // classical turning point for n=10 in harmonic oscillator
               r_min = -r_max;
            }
            else
            {
               r_max = this->mpBasDef->GetBasDefForLocalMode(i_bas_mode).GetBasisGridBounds().second;
               r_min = this->mpBasDef->GetBasDefForLocalMode(i_bas_mode).GetBasisGridBounds().first;
            }

            auto length = r_max - r_min;
            In n_pts = std::ceil(length * this->mWfGridDensity);
            Nb step = length/Nb(n_pts-I_1);
            std::vector<Nb> grid(n_pts);
            for (In i_p=0; i_p<n_pts; ++i_p)
            {
               grid[i_p]=r_min + step*i_p;
            }
            
            // Loop over grid points
            for(In ipt=I_0; ipt<n_pts; ++ipt)
            {
               const auto& q = grid[ipt];

               wf_out   << std::setw(width) << q;

               // Loop over basis functions
               for(In ibas=I_0; ibas<lim; ++ibas)
               {
                  // Calculate value of transformed primitive-basis function
                  Nb value = C_0;
                  for(In iprimbas=I_0; iprimbas<nbas; ++iprimbas)
                  {
                     value += transp_modal_transform[ibas][iprimbas]*this->mpBasDef->EvalOneModeBasisFunc(i_bas_mode, iprimbas, q);
                  }

                  wf_out   << std::setw(width) << value;
               }
               wf_out   << std::endl;
            }

            // Close output stream
            wf_out.close();
         }
      }

      // Debug
      if (  gDebug
         )
      {
         Mout  << " mSpectralBasisModals:\n" << mSpectralBasisModals << std::endl;
      }
   }

   // Call implementation for concrete TDH
   // NB: This transforms the initial wave function using mSpectralBasisModals if needed!
   this->PrepareIntegrationImpl(arWf);

   // Transform integrals to spectral basis
   // If we are using spectral basis, we transform integrals
   if (  spec
      )
   {
      if (  this->mIoLevel > I_5
         )
      {
         Mout  << " TdHBase: Transforming integrals to spectral basis." << std::endl;
      }

      const auto& limits = this->mpTdHCalcDef->ModalBasisLimits(); // may be empty

      // Transform H integrals
      this->mIntegrals->TransformModalBasis(mSpectralBasisModals, this->mPrimitiveModalOffsets, limits);

      // Transform property integrals
      for(const auto& oper_name : mExptValOpers)
      {
         this->mPropertyIntegrals[oper_name]->TransformModalBasis(mSpectralBasisModals, this->mPrimitiveModalOffsets, limits);
      }
   }

   // Calculate energy of initial wave packet
   auto init_time = this->GetOdeInfo().template get<std::pair<Nb, Nb>>("TIMEINTERVAL").first;
   this->mInitialEnergy = this->CalculateEnergy(init_time, arWf);

   if (  this->mpTdHCalcDef->SpectrumEnergyShift().first == "E0"
      )
   {
      if (  midas::util::IsSane(this->mInitialEnergy)
         )
      {
         this->SetSpectrumEnergyShift(std::real(this->mInitialEnergy));
         Mout  << " Energy shift for TDH spectrum set to:   " << this->mInitialEnergy << std::endl;
      }
      else
      {
         MidasWarning("Could not set spectrum energy shift. E0 = " + std::to_string(std::real(this->mInitialEnergy)) + " + i * " + std::to_string(std::imag(this->mInitialEnergy)));
      }
   }
}

/**
 * Get OdeInfo
 *
 * @return
 *    OdeInfo from calcdef
 **/
const midas::ode::OdeInfo& TdHBase::GetOdeInfo
   (
   )  const
{
   return this->mpTdHCalcDef->OdeInfo();
}

/**
 * Get shaped vector of zeros
 *
 * @return
 *    y vector
 **/
typename TdHBase::vec_t TdHBase::ShapedZeroVector
   (
   )  const
{
   // Call implementation
   return this->ShapedZeroVectorImpl();
}

/**
 * Calculate energy
 *
 * @param aT   time
 * @param aY   y vector
 * @return
 *    <Phi|H|Phi>
 **/
typename TdHBase::param_t TdHBase::CalculateEnergy
   (  step_t aT
   ,  const vec_t& aY
   )
{
   // Update integrals
   this->PrepareDerivativeCalculation(aT, aY);

   // Call implementation
   auto result = this->CalculateEnergyImpl();

   return result;
}

/**
 * Update H integrals (wrapper)
 * Set time for last integrals update.
 *
 * @param aT
 * @param aY
 **/
void TdHBase::PrepareDerivativeCalculation
   (  step_t aT
   ,  const vec_t& aY
   )
{
   this->PrepareDerivativeCalculationImpl(aY);
   this->mIntegralsTime = aT;
}

/**
 * Calculate energy
 *
 * @return
 *    <Phi|H|Phi>
 **/
typename TdHBase::param_t TdHBase::CalculateEnergyImpl
   (
   )  const
{
   // Init result
   param_t result = CC_0;

   for(In i_term=I_0; i_term<mpOpDef->Nterms(); ++i_term)
   {
      // Init z_t to c_t
      param_t zt = this->mpOpDef->Coef(i_term);
      for(In i_fac=I_0; i_fac<mpOpDef->NfactorsInTerm(i_term); ++i_fac)
      {
         LocalModeNr i_op_mode = mpOpDef->ModeForFactor(i_term, i_fac);
         LocalOperNr i_oper    = mpOpDef->OperForFactor(i_term, i_fac);
         auto h_ii = this->mIntegrals->GetOccIntegral(i_op_mode, i_oper);

         if (  !midas::util::IsSane(h_ii)
            && (  gDebug
               || this->mIoLevel >= I_10
               )
            )
         {
            Mout  << " h_ii for term " << i_term << " factor " << i_fac << " = " << h_ii << std::endl;
         }

         zt *= h_ii;
      }

      result += zt;
   }

   // Check if imag part is numerically zero compared to real part
   if (  !midas::util::IsSane(result)
      )
   {
      if (  gDebug
         || this->mIoLevel >= I_5
         )
      {
         Mout  << " TdH: Bad energy!    E = " << result << std::endl;
      }
      MidasWarning("TdH: Error in energy calculation (nan or inf). This may be due to loose tolerance in the ODE integrator.");
   }
   else if  (  !libmda::numeric::float_numeq_zero(result.imag(), result.real())
            )
   {
      if (  gDebug
         || this->mIoLevel >= I_8
         )
      {
         Mout  << " TdH: Complex energy!    E = " << result << std::endl;
      }
      MidasWarning("TdH: Complex energy calculated! This may be due to loose tolerance in the ODE integrator.");
   }

   return result;
}


/**
 * Calculate autocorrelation function
 *
 * @param aT   t
 * @param aY   y(t) vector
 * @return
 *    S(t)
 **/
typename TdHBase::param_t TdHBase::CalculateAutoCorr
   (  step_t aT
   ,  const vec_t& aY
   )  const
{
   // Call implementation
   return this->CalculateAutoCorrImpl(aT, aY);
}

/**
 * Calculate half-time autocorrelation function
 *
 * @param aT   t
 * @param aY   y(t) vector
 * @return
 *    S½(t)
 **/
typename TdHBase::param_t TdHBase::CalculateHalfTimeAutoCorr
   (  step_t aT
   ,  const vec_t& aY
   )  const
{
   // Call implementation
   return this->CalculateHalfTimeAutoCorrImpl(aT, aY);
}

/**
 * Finalize calculation
 *
 * @param aTs     Vector of ts for all saved steps
 * @param aYs     Vector of ys for all saved steps
 **/
void TdHBase::Finalize
   (  const std::vector<step_t>& aTs
   ,  const std::vector<vec_t>& aYs
   )
{
#ifdef VAR_MPI
   // Only master node does output
   // Niels: We could also use MidasStream instead of std::ostream for output!
   if (  !midas::mpi::IsMaster()
      )
   {
      return;
   }
#endif
   // DEBUG
   if (  gDebug
      )
   {
      Mout  << " TdHBase::Finalize\n"
            << "  aTs.size():    " << aTs.size() << "\n"
            << "  aYs.size():    " << aYs.size() << "\n"
            << std::endl;
   }

   // Screening statistics
   if (  this->mpTdHCalcDef->ScreenZeroCoef() > C_0
      )
   {
      Mout  << " === Screening statistics. ===\n"
            << "   - n_screen_tot:     " << this->mNScreenedTerms << "\n"
            << std::flush;
   }

   // Do general stuff
   bool energy_out   = false;    // energy
   bool phase_out    = false;    // phase
   bool ac_out       = false;    // autocorrelation function
   bool t12ac_out    = false;    // half-time autocorrelation function
   std::string energy_filename = gAnalysisDir + "/" + this->mName + "_energy.dat";
   std::string phase_filename = gAnalysisDir + "/" + this->mName + "_phase.dat";
   std::string ac_filename = gAnalysisDir + "/" + this->mName + "_ac.dat";
   std::string t12ac_filename = gAnalysisDir + "/" + this->mName + "_t12ac.dat";
   std::string kappa_filename = gAnalysisDir + "/" + this->mName + "_kappa_norms.dat";

   Mout  << " === Calculating properties. ===" << std::endl;
   for(const auto& prop : this->mProperties)
   {
      switch   (  prop
               )
      {
         case TdHBase::propID::ENERGY:
         {
            energy_out = true;
            Mout  << "   - Energy                                    (write to file '" << energy_filename << "')" << std::endl;
            break;
         }
         case TdHBase::propID::PHASE:
         {
            phase_out = true;
            Mout  << "   - Phase                                     (write to file '" << phase_filename << "')" << std::endl;
            break;
         }
         case TdHBase::propID::AUTOCORR:
         {
            ac_out = true;
            Mout  << "   - Autocorrelation function                  (write to file '" << ac_filename << "')" << std::endl;
            break;
         }
         case TdHBase::propID::HALFTIMEAUTOCORR:
         {
            t12ac_out = true;
            Mout  << "   - Half-time autocorrelation function        (write to file '" << t12ac_filename << "')" << std::endl;
            break;
         }
         case TdHBase::propID::KAPPANORM:
         {
            if (  this->Type() == midas::tdh::tdhID::EXPONENTIAL
               )
            {
               Mout  << "   - Kappa norms                               (write to file '" << kappa_filename << "')" << std::endl;
            }
            break;
         }
         default:
         {
            MIDASERROR("TdHBase::Finalize: Unrecognized property!");
         }
      }
   }

   // Get number of saved points
   auto n_data_interp = aTs.size();

   // Column width of output
   size_t width = 24;

   // Energy output.
   // NB: Only points saved in TdHBase!
   // E is not calculated for interpolated y vectors.
   if (  energy_out
      )
   {
      midas::mpi::OFileStream energy_ofs(energy_filename);
      energy_ofs << std::scientific << std::setprecision(16);

      // Print header
      energy_ofs  << std::setw(width) << "# t"
                  << std::setw(width) << "Re[E]"
                  << std::setw(width) << "Im[E]"
                  << std::endl;

      auto n=this->mTs.size();

      // Loop over datapoints
      if (  gDebug
         || this->mIoLevel >= I_5
         )
      {
         Mout  << std::endl;
         Mout  << " === Output energy at first and last time point ===" << std::endl;
      }
      for (In i=I_0; i<n; ++i)
      {
         const auto& t_i = this->mTs[i];

         auto energy = this->mEnergies[i];
         energy_ofs  << std::setw(width) << t_i
                     << std::setw(width) << energy.real()
                     << std::setw(width) << energy.imag()
                     << std::endl;

         // Output first and last energy for the tdh_1dho_squeeze test.
         if (  gDebug
            || this->mIoLevel >= I_5
            )
         {
            if (  i == I_0
               || i == n-1
               )
            {
               Mout << "t = " << t_i << ",   E = " << energy << std::endl;
            }
         }
      }

      energy_ofs.close();
   }

   // Phase output
   // NB: Mix of phases stored in TdHBase and those from aYs if aYs are saved
   if (  phase_out
      )
   {
      midas::mpi::OFileStream phase_ofs(phase_filename);
      phase_ofs << std::scientific << std::setprecision(16);
   
      // Print header
      phase_ofs   << std::setw(width) << "# t"
                  << std::setw(width) << "Re[F]"
                  << std::setw(width) << "Im[F]"
                  << std::endl;

      auto it_y = aYs.begin();
      auto it_t = aTs.begin();

      // Loop over datapoints
      auto n = this->mTs.size();
      for (In i=I_0; i<n; ++i)
      {
         const auto& t_i = this->mTs[i];

         // Phase stored in TdHBase
         const auto& phase = this->mPhases[i];

         // Print phase from aYs if it fits here
         while (  it_y != aYs.end()
               && libmda::numeric::float_lt(*it_t, t_i)  // Print for all interpolated times less than t_i
               )
         {
            // Get phase as last element of y vector
            auto f = (*it_y)[it_y->size()-1];

            // Print interpolated phase
            phase_ofs   << std::setw(width) << t_i
                        << std::setw(width) << f.real()
                        << std::setw(width) << f.imag()
                        << std::endl;

            // Move to next interpolated point
            ++it_y;
            ++it_t;
         }

         // Print phase stored in TdHBase
         phase_ofs   << std::setw(width) << t_i
                     << std::setw(width) << phase.real()
                     << std::setw(width) << phase.imag()
                     << std::endl;
      }

      phase_ofs.close();
   }

   // Autocorrelation output
   if (  ac_out
      )
   {
      if (  this->mAutoCorrs.size() != n_data_interp
         )
      {
         MIDASERROR("Wrong size of mAutoCorrs: " + std::to_string(this->mAutoCorrs.size()) + ", aTs.size() = " + std::to_string(aTs.size()));
      }

      midas::mpi::OFileStream ac_ofs(ac_filename);
      ac_ofs << std::scientific << std::setprecision(16);
   
      // Print header
      ac_ofs   << std::setw(width) << "# t"
               << std::setw(width) << "Re[S]"
               << std::setw(width) << "Im[S]"
               << std::setw(width) << "|S|"
               << std::endl;

      // Loop over datapoints
      for (In i=I_0; i<n_data_interp; ++i)
      {

         const auto& t_i = aTs[i];
         param_t ac_i(0.);

         ac_i = this->mAutoCorrs[i];

         // Output
         ac_ofs   << std::setw(width) << t_i
                  << std::setw(width) << ac_i.real()
                  << std::setw(width) << ac_i.imag()
                  << std::setw(width) << std::abs(ac_i)
                  << std::endl;
      }

      ac_ofs.close();
   }

   // Autocorrelation output
   bool t12ac_spec = false;   // Use t12ac for spectrum
   if (  t12ac_out
      )
   {
      if (  this->mHalftimeAutoCorrs.size() != n_data_interp
         )
      {
         MIDASERROR("Wrong size of mHalftimeAutoCorrs: " + std::to_string(this->mHalftimeAutoCorrs.size()) + ", aTs.size() = " + std::to_string(aTs.size()));
      }

      midas::mpi::OFileStream t12ac_ofs(t12ac_filename);
      t12ac_ofs << std::scientific << std::setprecision(16);
   
      // Print header
      t12ac_ofs   << std::setw(width) << "# t"
                  << std::setw(width) << "Re[S_t(1/2)]"
                  << std::setw(width) << "Im[S_t(1/2)]"
                  << std::setw(width) << "|S_t(1/2)|"
                  << std::endl;

      // Loop over datapoints
      for (In i=I_0; i<n_data_interp; ++i)
      {
         const auto& t_i = aTs[i];
         param_t t12ac_i(0.);

         t12ac_i = this->mHalftimeAutoCorrs[i];

         // Output
         t12ac_ofs   << std::setw(width) << 2*t_i           // We have calculated S(2*t)
                     << std::setw(width) << t12ac_i.real()
                     << std::setw(width) << t12ac_i.imag()
                     << std::setw(width) << std::abs(t12ac_i)
                     << std::endl;
      }

      t12ac_ofs.close();
   }

   Mout  << std::endl;

   // Exptectation values
   // NB: Only values stored in TdHBase!
   if (  !mExptVals.empty()
      )
   {
      Mout  << " === Write expectation values for operators ===" << std::endl;
   }
   for(const auto& prop : mExptVals)
   {
      std::string filename = gAnalysisDir + "/" + this->mName + "_mean_" + prop.first + ".dat";

      Mout  << "   - " << std::setw(42) << prop.first << "(write to file '" << filename << "')." << std::endl;

      midas::mpi::OFileStream ofs(filename);
      ofs << std::scientific << std::setprecision(16);

      // Print header
      ofs   << "# This file contains expectation values of operator '" << prop.first << "' at all time points.\n"
            << "# NB: The first columns contain the total expectation values, while the following contain partial values where only terms active to the given mode are taken into account."
            << std::endl;
      ofs   << std::setw(width) << "# t";
      std::ostringstream os_re_tot, os_im_tot;
      os_re_tot   << "Re[<" << prop.first << ">]";
      os_im_tot   << "Im[<" << prop.first << ">]";
      ofs   << std::setw(width) << os_re_tot.str()
            << std::setw(width) << os_im_tot.str();
      for(LocalModeNr imode=0; imode<this->mNModes; ++imode)
      {
         GlobalModeNr i_g_mode = mpOpDef->GetGlobalModeNr(imode);
         std::ostringstream os_re;
         os_re << "Re[<" << prop.first << "_Q" << i_g_mode << "_act>]";
         std::ostringstream os_im;
         os_im << "Im[<" << prop.first << "_Q" << i_g_mode << "_act>]";
         ofs   << std::setw(width) << os_re.str()
               << std::setw(width) << os_im.str();
      }
      ofs   << std::endl;
   
      // Loop over datapoints
      auto n = this->mTs.size();
      for (In i=0; i<n; ++i)
      {
         const auto& t_i = this->mTs[i];
         const auto& vec_i = prop.second[i];

         // Print time
         ofs   << std::setw(width) << t_i;

         // Get total expt val.
         auto val_tot = vec_i.back();

         ofs   << std::setw(width) << val_tot.real()
               << std::setw(width) << val_tot.imag();

         // Loop over modes
         for(In imode=I_0; imode<this->mNModes; ++imode)
         {
            auto val = vec_i[imode];

            ofs   << std::setw(width) << val.real()
                  << std::setw(width) << val.imag();
         }
         ofs   << std::endl;
      }

      ofs.close();
   }

   // Calculate spectrum
   for(const auto& spec : this->mSpectra)
   {
      switch   (  spec
               )
      {
         case specID::AUTOCORR:
         {
            if (  this->mAutoCorrs.size() != aTs.size()
               )
            {
               MidasWarning("AUTOCORR spectrum cannot be calculated!");
            }
            else
            {
               Spectrum::CalculateAndWriteSpectrum(gAnalysisDir + "/" + this->mName + "_ac", this->mAutoCorrs, aTs);
            }
            break;
         }
         case specID::HALFTIMEAUTOCORR:
         {
            if (  this->mHalftimeAutoCorrs.size() != aTs.size()
               )
            {
               MidasWarning("HALFTIMEAUTOCORR spectrum cannot be calculated!");
            }
            else
            {
               std::vector<step_t> mod_ts;
               mod_ts.reserve(aTs.size());
               for(const auto& t : aTs)
               {
                  mod_ts.emplace_back(2*t);
               }
               Spectrum::CalculateAndWriteSpectrum(gAnalysisDir + "/" + this->mName + "_t12ac", this->mHalftimeAutoCorrs, mod_ts);
            }
            break;
         }
         default:
         {
            MIDASERROR("Unrecognized spectrum!");
         }
      }
   }

   // Calculate wave function
   if (  !this->mWfOutModes.empty()
      || !this->mTwoModeDensityPairs.empty()
      )
   {
      if (  aYs.size() == n_data_interp
         )
      {
         this->WriteWaveFunction(aTs, aYs);
      }
      else
      {
         MidasWarning("Cannot calculate wave function without saved vectors!");
      }
   }

   // Call specialized implementation for additional features
   this->FinalizeImpl(aTs, aYs);
}

/**
 * Write wave function
 *
 * @param aTs     Time points
 * @param aYs     Wave-function parameters
 **/
void TdHBase::WriteWaveFunction
   (  const std::vector<step_t>& aTs
   ,  const std::vector<vec_t>& aYs
   )  const
{
   // Get modals if we have transformed to spectral basis
   const auto& spec_modals = this->mSpectralBasisModals;

   // Vector of data for two-mode density
   const auto& modepairs = this->mTwoModeDensityPairs;
   std::map<std::string, std::pair<std::vector<Nb>, std::vector<vec_t> > > mode_densities; // Niels: I know this is an ugly one, but please forgive me...

   // Initialize output streams
   std::string wf_filename = gAnalysisDir + "/" + this->Name() + "_wf.dat";
   midas::mpi::OFileStream wf_out(wf_filename);
   wf_out << std::scientific << std::setprecision(16);
   std::string dens_filename = gAnalysisDir + "/" + this->Name() + "_density.dat";
   midas::mpi::OFileStream dens_out(dens_filename);
   dens_out << std::scientific << std::setprecision(16);
   size_t width = 30;

   Mout  << " === Calculate wave function and density ===\n"
         << "   - WF will be written to file:         '" << wf_filename << "'.\n"
         << "   - Density will be written to file:    '" << dens_filename << "'." << std::endl;

   // Loop over modes
   for(const auto& mode : this->mWfOutModes)
   {
      if (  gDebug
         || this->mIoLevel >= I_8
         )
      {
         Mout  << " Calculate wave function and density for mode: " << mode << std::endl;
      }

      // Set up some stuff...
      GlobalModeNr i_g_mode  = gOperatorDefs.ModeNr(mode); //mpOpDef->GetGlobalModeNr(mode);
      LocalModeNr i_op_mode = mpOpDef->GetLocalModeNr(i_g_mode);
      LocalModeNr i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
      In nbas = this->mpBasDef->Nbas(i_bas_mode);   // Niels: Is this correct?

      // Check if we have limits. Otherwise, we use the same basis size.
      const auto& limits = this->mpTdHCalcDef->ModalBasisLimits();
      bool has_limits = !limits.empty();

      In lim  = has_limits ? std::min(limits[i_bas_mode], nbas) : nbas;
      

      // Get transformation matrix for this mode
      MidasMatrix modal_transform;
      MidasMatrix transp_modal_transform;
      if (  this->mpTdHCalcDef->SpectralBasis()
         )
      {
         modal_transform.SetNewSize(lim, nbas);
         spec_modals.DataIo(IO_GET, modal_transform, nbas*lim, lim, nbas, this->mPrimitiveModalOffsets[i_op_mode]);

         transp_modal_transform.SetNewSize(nbas, lim);
         transp_modal_transform = Transpose(modal_transform);
      }

      // Print header for current mode
      wf_out   << std::setw(width) << "# " << mode;
      dens_out << std::setw(width) << "# " << mode;
      if (  this->mImagTime
         )
      {
         std::ostringstream os_re;
         os_re << std::setprecision(2) << "Re[phi(" << mode << ", t=" << aTs.back() << ")]";
         std::ostringstream os_im;
         os_im << std::setprecision(2) <<"Im[phi(" << mode << ", t=" << aTs.back() << ")]";

         wf_out   << std::setw(width) << os_re.str()
                  << std::setw(width) << os_im.str()
                  << std::endl;

         std::ostringstream os_dens;
         os_dens << std::setprecision(16) << "|phi(" << mode << ", t=" << aTs.back() << "|^2";
         dens_out << std::setw(width) << os_dens.str() << std::endl;
      }
      else
      {
         midas::stream::ScopedPrecision(2, wf_out);
         midas::stream::ScopedPrecision(2, dens_out);
         for(size_t i=0; i<aTs.size(); ++i)
         {
            std::ostringstream os_re;
            os_re << std::setprecision(6) << "Re[phi(" << mode << ", t=" << aTs[i] << ")]";
            std::ostringstream os_im;
            os_im << std::setprecision(6) <<"Im[phi(" << mode << ", t=" << aTs[i] << ")]";

            wf_out   << std::setw(width) << os_re.str()
                     << std::setw(width) << os_im.str();

            std::ostringstream os_dens;
            os_dens << std::setprecision(6) << "|phi(" << mode << ", t=" << aTs[i] << ")|^2";
            dens_out << std::setw(width) << os_dens.str();
         }
         wf_out   << std::endl;
         dens_out << std::endl;
      }

      // Set vectors for different time intervals
      std::vector<vec_t> wf_t_vecs;
      wf_t_vecs.reserve(this->mImagTime ? 1 : aTs.size());
      std::vector<vec_t> dens_t_vecs;
      dens_t_vecs.reserve(this->mImagTime ? 1 : aTs.size());

      // Construct grid for current mode
      In n_max = I_10;  // use HO turning point for 10th eigenstate
      Nb scale = C_2;   // And scale this interval by 2...
      Nb omega = this->mpOpDef->HoOmega(i_op_mode);
      bool no_omega = libmda::numeric::float_numeq_zero(omega, C_1, 2);
      Nb r_max =  no_omega
               ?  this->mpBasDef->GetBasDefForLocalMode(i_bas_mode).GetBasisGridBounds().second
               :  std::sqrt(C_2*(Nb(n_max)+C_I_2)/(this->mpOpDef->HoOmega(i_op_mode)))*scale;  // classical turning point for n=10 in harmonic oscillator
      Nb r_min =  no_omega
               ?  this->mpBasDef->GetBasDefForLocalMode(i_bas_mode).GetBasisGridBounds().first
               :  -r_max;
      auto length = r_max - r_min;
      In n_pts = std::ceil(length * this->mWfGridDensity);
      Nb step = length/Nb(n_pts-I_1);
      std::vector<Nb> grid(n_pts);
      for (In i_p=0; i_p<n_pts; ++i_p)
      {
         grid[i_p]=r_min + step*i_p;
      }

      // Loop over time steps
      for(size_t itime=0; itime<aTs.size(); ++itime)
      {
         const auto& time = aTs[itime];
         const auto& wf_params = aYs[itime];

         // For imaginary time, we only output the end result.
         if (  this->mImagTime
            && itime != aTs.size() - 1
            )
         {
            continue;
         }

         // Make vectors for storing function values
         vec_t wf_vals(n_pts);
         vec_t dens_vals(n_pts);
         step_t int_dens = C_0; // integrated density

         // Get phase (and normalize)
         auto phase = std::exp(CC_M_I*this->GetPhaseFactorImpl(time, wf_params));
         phase /= std::abs(phase);

         // Get WF coefs and transform to correct basis
         vec_t wf_coefs = this->GetWaveFunctionCoefficients(i_op_mode, time, wf_params);

         // Transform back to primitive basis if we have used spectral
         if (  this->mpTdHCalcDef->SpectralBasis()
            )
         {
            vec_t trans = transp_modal_transform * wf_coefs;

            wf_coefs.SetNewSize(nbas);
            wf_coefs = std::move(trans);
         }


         // Loop over grid points and calculate wf and dens
         #pragma omp parallel for reduction(+ : int_dens) shared(wf_coefs, wf_vals, dens_vals)
         for(size_t ipt=0; ipt<n_pts; ++ipt)
         {
            param_t wf_val(0.);
            for (In i_bas=0; i_bas<wf_coefs.size(); ++i_bas)
            {
               wf_val+=wf_coefs[i_bas]*this->mpBasDef->EvalOneModeBasisFunc(i_bas_mode, i_bas, grid[ipt]);
            }

            // Multiply (normalized) phase
            wf_val *= phase;

            wf_vals[ipt] = wf_val;

            dens_vals[ipt] = midas::math::Conj(wf_vals[ipt]) * wf_vals[ipt];

            // Integrate density using rectangles
            int_dens += std::real(dens_vals[ipt] * step);
         }

         // Save data
         wf_t_vecs.emplace_back(wf_vals);
         dens_t_vecs.emplace_back(dens_vals);

         if (  this->mIoLevel > I_10
            )
         {
            Mout  << " Integrated density for time = " << time << " is: |phi|^2 = " << int_dens << std::endl;
         }
      }

      // Do output for current mode
      for(size_t ipt=0; ipt<n_pts; ++ipt)
      {
         wf_out   << std::setw(width) << grid[ipt];
         dens_out << std::setw(width) << grid[ipt];

         for(size_t itime=0; itime<wf_t_vecs.size(); ++itime)
         {
            wf_out   << std::setw(width) << wf_t_vecs[itime][ipt].real()
                     << std::setw(width) << wf_t_vecs[itime][ipt].imag();

            dens_out << std::setw(width) << dens_t_vecs[itime][ipt].real();

         }
         wf_out   << std::endl;
         dens_out << std::endl;
      }

      // Skip to new section in output files
      wf_out << std::endl << std::endl;
      dens_out << std::endl << std::endl;

      // Save dens_t_vecs if we need it
      for(const auto& modepair : modepairs)
      {
         if (  modepair.find(mode) != modepair.end()
            )
         {
            // We need the densities for this mode.
            auto savemode = mode;
            mode_densities.insert(std::make_pair(std::move(savemode), std::make_pair(std::move(grid), std::move(dens_t_vecs))));
            break;
         }
      }
   }

   Mout  << std::endl;

   // Close output streams
   wf_out.close();
   dens_out.close();

   // Do output for two-mode densities
   if (  !modepairs.empty()
      )
   {
      Mout  << " === Write pair densities ===" << std::endl;
   }
   for(const auto& modepair : modepairs)
   {
      std::vector<std::string> modevec(modepair.begin(), modepair.end());
      assert(modevec.size() == 2);
      const auto& m_one = modevec[0];
      const auto& m_two = modevec[1];

      std::string filename = gAnalysisDir + "/" + this->Name() + "_pair_density_" + m_one + "_" + m_two + ".dat";

      std::string msg = "rho(" + m_one + "," + m_two + ") will be written to file:";
      Mout  << "   - " << std::setw(36) << msg << "'" << filename << "')." << std::endl;

      midas::mpi::OFileStream pairdens_out(filename);
      pairdens_out << std::scientific << std::setprecision(16);

      // Write header
      std::string comment_m_one = "# " + m_one;
      pairdens_out << std::setw(width) << comment_m_one << std::setw(width) << m_two;
      if (  this->mImagTime
         )
      {
         std::ostringstream os_dens;
         os_dens << std::setprecision(16) << "rho(t=" << aTs.back() << ")";
         pairdens_out << std::setw(width) << os_dens.str() << std::endl;
      }
      else
      {
         midas::stream::ScopedPrecision(2, dens_out);
         for(size_t i=0; i<aTs.size(); ++i)
         {
            std::ostringstream os_dens;
            os_dens << std::setprecision(16) << "rho(t=" << aTs[i] << ")";
            pairdens_out << std::setw(width) << os_dens.str();
         }
         pairdens_out << std::endl;
      }

      // Grids for both modes
      const auto& grid_one = mode_densities[m_one].first;
      const auto& grid_two = mode_densities[m_two].first;

      for(In i=I_0; i<grid_one.size(); ++i)
      {
         for(In j=I_0; j<grid_two.size(); ++j)
         {
            // Output coordinates
            pairdens_out   << std::setw(width) << grid_one[i]
                           << std::setw(width) << grid_two[j];

            // Output densities
            for(In itime=I_0; itime<aTs.size(); ++itime)
            {
               // Densities for both modes for given time point
               const auto& dens_one = mode_densities[m_one].second[itime];
               const auto& dens_two = mode_densities[m_two].second[itime];
               
               pairdens_out   << std::setw(width) << std::real(dens_one[i]*dens_two[j]);
            }
            pairdens_out   << std::endl;
         }

         // New block for new i value
         pairdens_out   << std::endl;
      }
      pairdens_out.close();
   }
   if (  !modepairs.empty()
      )
   {
      Mout  << std::endl;
   }
}

/**
 * Get coefficient vector for modal in the time-independent basis (spectral or primitive)
 *
 * @param aModeNr    Mode number
 * @param aT         t
 * @param aWfParams  Wave-function parameters at time = aT
 * @return
 *    Value of wave function at Q = aQ
 **/
typename TdHBase::vec_t TdHBase::GetWaveFunctionCoefficients
   (  LocalModeNr aModeNr
   ,  step_t aT
   ,  const vec_t& aWfParams
   )  const
{
   return this->GetWaveFunctionCoefficientsImpl(aModeNr, aT, aWfParams);
}


/**
 * Factory
 *
 * @param apCalcDef
 * @param apOpDef
 * @param apBasDef
 * @param aInitModalsFile
 *
 * @return
 *    std::unique_ptr to concrete TdH
 **/
std::unique_ptr<TdHBase> TdHBase::Factory
   (  TdHCalcDef* apCalcDef
   ,  OpDef* apOpDef
   ,  BasDef* apBasDef
   ,  const std::string& aInitModalsFile
   )
{
   switch   (  apCalcDef->Parametrization()
            )
   {
      case midas::tdh::tdhID::LINEAR:
      {
         return std::make_unique<LinearTdH>(apCalcDef, apOpDef, apBasDef, aInitModalsFile);
         break;
      }
      case midas::tdh::tdhID::EXPONENTIAL:
      {
         return std::make_unique<ExponentialTdH>(apCalcDef, apOpDef, apBasDef, aInitModalsFile);
         break; 
      }
      default:
      {
         MIDASERROR("Unrecognized TDH parametrization!");
         return nullptr;
      }
   }
}

/**
 * Save properties.
 * NB: Integrals must be updated already!
 *
 * @param aT
 * @param aY
 **/
void TdHBase::SaveProperties
   (  step_t aT
   ,  const vec_t& aY
   )
{
   In res = -I_1;
   // Reserve space
   if (  this->mTs.size() == this->mTs.capacity()
      )
   {
      res = std::max<size_t>(this->mTs.size()*2, 10);

      this->mTs.reserve(res);
   }

   // Save time
   this->mTs.emplace_back(aT);

   // Save energy and phase
   for(const auto& prop : this->mProperties)
   {
      switch   (  prop
               )
      {
         case TdHBase::propID::ENERGY:
         {
            if (  res > I_0
               )
            {
               this->mEnergies.reserve(res);
            }
            if (  libmda::numeric::float_neq(this->mIntegralsTime, aT)
               )
            {
               MidasWarning("TdHBase::SaveProperties: Integrals have not been updated to correct time. I will do this before calculating energy!");
               this->mEnergies.emplace_back(this->CalculateEnergy(aT, aY));
            }
            else
            {
               this->mEnergies.emplace_back(this->CalculateEnergyImpl());
            }
            break;
         }
         case TdHBase::propID::PHASE:
         {
            if (  res > I_0
               )
            {
               this->mPhases.reserve(res);
            }
            this->mPhases.emplace_back(aY[aY.size()-1]);
            break;
         }
         default:
         {
            // We have a property, which will not be saved!
         }
      }
   }

   // Save expectation values
   for(const auto& ioper : this->mExptValOpers)
   {
      // Reserve space?
      auto& expt_vals = mExptVals.at(ioper);
      if (  res > I_0
         )
      {
         expt_vals.reserve(res);
      }

      // Calculate expectation values
      std::vector<param_t> vals(mNModes+I_1);
      for(In i=I_0; i<mNModes; ++i)
      {
         vals[i] = this->mPropertyIntegrals.at(ioper)->ExpectationValue(i);
      }

      // The last element will be the total expectation value
      vals[mNModes] = this->mPropertyIntegrals.at(ioper)->ExpectationValue(-I_1);

      // Emplace results
      expt_vals.emplace_back(std::move(vals));
   }

   // Call concrete implementation
   this->SavePropertiesImpl(aT, aY);
}

/**
 * Default implementation of SavePropertiesImpl.
 * Does nothing!
 *
 * @param aT
 * @param aY
 **/
void TdHBase::SavePropertiesImpl
   (  step_t aT
   ,  const vec_t& aY
   )
{
   return;
}




/***************************************************************************
 * MPI stuff begin
 **************************************************************************/
#ifdef VAR_MPI

/**
 * Range of modes to calculate for given process.
 * @param aRank      Rank to calculate interval for
 * @result
 *    pair(m_begin, m_end)
 **/
std::pair<In, In> TdHBase::MpiModeRange
   (  In aRank
   )  const
{
   In nproc = midas::mpi::GlobalSize();
   In nmodes = this->mNModes;

   // Number of modes per rank
   In n = nmodes / nproc;

   // Remainder
   In rest = nmodes % nproc;

   // Begin
   In m_begin = aRank*n + std::min(rest, aRank);

   // End
   In m_end = (aRank+I_1)*n + std::min(rest, (aRank+I_1));

   // Return pair
   return std::make_pair(m_begin, m_end);
}

/**
 * Sync time-dependent integrals between nodes
 **/
void TdHBase::SyncData
   (  vec_t& arVec
   ,  param_t& arPhase
   ,  size_t& arNScreen
   )  const
{
   // Call MPI_Allreduce for arPhase and arNScreen
   param_t total_phase(0.);
   MPI_Allreduce(&arPhase, &total_phase, 1, midas::mpi::DataTypeTrait<param_t>::Get(), MPI_SUM, MPI_COMM_WORLD);
   arPhase = total_phase;

   size_t total_nscreen = 0;
   MPI_Allreduce(&arNScreen, &total_nscreen, 1, midas::mpi::DataTypeTrait<size_t>::Get(), MPI_SUM, MPI_COMM_WORLD);
   arNScreen = total_nscreen;

   // Sync arVec
   auto nproc = midas::mpi::GlobalSize();
   auto nmodes = this->mNModes;

   switch   (  this->mMpiComm
            )
   {
      case midas::tdh::mpiComm::BCAST:
      {
         for(In iproc=I_0; iproc<nproc; ++iproc)
         {
            auto range = this->MpiModeRange(iproc);

            In nparams = I_0;
            for(In imode=range.first; imode<range.second; ++imode)
            {
               nparams += this->NParams(imode);
            }

            arVec.MpiBcast(iproc, nparams, this->Offset(range.first));
         }

         break;
      }
      case midas::tdh::mpiComm::IBCAST:
      {
         // Init requests
         std::vector<MPI_Request> reqs(nproc);

         // Ibcast
         for(In iproc=I_0; iproc<nproc; ++iproc)
         {
            auto range = this->MpiModeRange(iproc);

            In nparams = I_0;
            for(In imode=range.first; imode<range.second; ++imode)
            {
               nparams += this->NParams(imode);
            }

            arVec.MpiIbcastElements(iproc, &reqs[iproc], nparams, this->Offset(range.first));
         }

         // Wait
         for(In iproc=I_0; iproc<nproc; ++iproc)
         {
            midas::mpi::detail::WRAP_Wait(&reqs[iproc], MPI_STATUS_IGNORE);
         }

         break;
      }
      default:
      {
         MIDASERROR("Unrecognized MPI communication!");
      }
   }
}

#endif /* VAR_MPI */
/***************************************************************************
 * MPI stuff end
 **************************************************************************/
