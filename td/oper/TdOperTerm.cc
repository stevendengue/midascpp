/**
 *******************************************************************************
 * 
 * @file    TdOperTerm.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>

#include "td/oper/TdOperTerm.h"
#include "td/oper/TdOperTerm_Impl.h"

#include "inc_gen/TypeDefs.h"

class OpDef;
template<typename> class GeneralMidasMatrix;

// Define instatiation macro.
#define INSTANTIATE_TDOPERTERM(STEP_T,LIN_COEF_T) \
   namespace midas::td \
   { \
      template class TdOperTerm<STEP_T,LIN_COEF_T,OpDef>; \
      template class TdOperTerm<STEP_T,LIN_COEF_T,GeneralMidasMatrix<STEP_T>>; \
      template class TdOperTerm<STEP_T,LIN_COEF_T,GeneralMidasMatrix<std::complex<STEP_T>>>; \
   } /* namespace midas::td */ \
   

// Instantiations.
INSTANTIATE_TDOPERTERM(Nb,Nb);
INSTANTIATE_TDOPERTERM(Nb,std::complex<Nb>);

#undef INSTANTIATE_TDOPERTERM
#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
