/**
 *******************************************************************************
 * 
 * @file    GaussPulse_Impl.h
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef GAUSSPULSE_IMPL_H_INCLUDED
#define GAUSSPULSE_IMPL_H_INCLUDED

// Midas headers.
#include "util/Error.h"

namespace midas::td
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename EVAL_T
      >
   GaussPulse<STEP_T,EVAL_T>::GaussPulse
      (  param_t aAmpl
      ,  param_t aTpeak
      ,  param_t aSigma
      ,  param_t aFreq
      ,  param_t aPhase
      )
      :  mAmpl(aAmpl)
      ,  mTpeak(aTpeak)
      ,  mSigma(aSigma)
      ,  mFreq(aFreq)
      ,  mPhase(aPhase)
   {
   }

   /************************************************************************//**
    * If both FWHM and SIGMA are set, it causes a hard error.
    * If a parameter is not set, default value is used.
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename EVAL_T
      >
   GaussPulse<STEP_T,EVAL_T>::GaussPulse
      (  const std::map<GaussParamID,param_t>& arMap
      )
   {
      if (arMap.count(GaussParamID::SIGMA) > 0 && arMap.count(GaussParamID::FWHM) > 0)
      {
         MIDASERROR("Parameter map contains both SIGAM and FWHM.");
      }

      for(const auto& kv: arMap)
      {
         switch(kv.first)
         {
            case GaussParamID::AMPL:
               SetAmpl(kv.second);
               break;
            case GaussParamID::TPEAK:
               SetTpeak(kv.second);
               break;
            case GaussParamID::SIGMA:
               SetSigma(kv.second);
               break;
            case GaussParamID::FREQ:
               SetFreq(kv.second);
               break;
            case GaussParamID::PHASE:
               SetPhase(kv.second);
               break;
            case GaussParamID::FWHM:
               SetFwhm(kv.second);
               break;
            default:
               MIDASERROR("Unknown key.");
               break;
         }
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename EVAL_T
      >
   typename GaussPulse<STEP_T,EVAL_T>::eval_t GaussPulse<STEP_T,EVAL_T>::operator()
      (  step_t aT
      )  const
   {
      const auto t  = aT;
      const auto A  = Ampl();
      const auto t0 = Tpeak();
      const auto s  = Sigma();
      const auto w  = Freq();
      const auto p  = Phase();
      return A*exp(-0.5*pow((t-t0)/s,2))*cos(w*(t-t0) - p);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename EVAL_T
      >
   typename GaussPulse<STEP_T,EVAL_T>::eval_t GaussPulse<STEP_T,EVAL_T>::Deriv
      (  step_t aT
      )  const
   {
      const auto t  = aT;
      const auto A  = Ampl();
      const auto t0 = Tpeak();
      const auto s  = Sigma();
      const auto w  = Freq();
      const auto p  = Phase();
      return -A*exp(-0.5*pow((t-t0)/s,2))*((t-t0)*cos(w*(t-t0)-p)/pow(s,2) + w*sin(w*(t-t0)-p));
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename STEP_T
      ,  typename EVAL_T
      >
   void GaussPulse<STEP_T,EVAL_T>::PrintSettings
      (  std::ostream& arOs
      ,  const Uin aIndent
      ,  const Uin aWidth
      )  const
   {
      // Store old format flags.
      const auto old_flags = arOs.flags();

      // Width, precisions and such.
      const std::string tab(aIndent, ' ');
      const std::string eq = " = ";
      const Uin w = aWidth;
      arOs << std::left;
      arOs << std::boolalpha;

      arOs << tab << "Gaussian pulse f(t) = A exp(-(t-t0)/(2s^2))cos(w(t-t0)-p)\n";
      arOs << tab << std::setw(w) << "   A (amplitude)" << eq << Ampl() << '\n';
      arOs << tab << std::setw(w) << "   t0 (peak time)" << eq << Tpeak() << '\n';
      arOs << tab << std::setw(w) << "   s (sigma, std.dev.)" << eq << Sigma() << '\n';
      arOs << tab << std::setw(w) << "   w (ang. freq.)" << eq << Freq() << '\n';
      arOs << tab << std::setw(w) << "   p (phase shift)" << eq << Phase() << '\n';
      arOs << tab << std::setw(w) << "   FWHM (= 2sqrt(2ln2) s)" << eq << Fwhm() << '\n';


      arOs << std::endl;
      arOs.flags(old_flags);
   }
} /* namespace midas::td */




#endif/*GAUSSPULSE_IMPL_H_INCLUDED*/
