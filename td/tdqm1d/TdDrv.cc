/**
 *******************************************************************************
 * 
 * @file    TdDrv.cc
 * @date    07-08-2016
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Contains the driver function for the TD (time dependent) module.
 *
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

// Standard headers.
#include <string>
#include <map>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "input/Input.h"
#include "input/TdCalcDef.h"
#include "util/Io.h"
#include "util/Timer.h"

// Declarations.
void TDQM1DDrv();

/**
 * @brief
 *    Runs TD (time dependent) calculations.
 **/
void TdDrv()
{
   Mout << "\n\n";
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," Begin TD calculations ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');
   Mout << "\n\n";

   Mout << "gTdCalcDef.size() = " << gTdCalcDef.size() << endl;

   for(Uin i_calc = I_0; i_calc < gTdCalcDef.size(); ++i_calc)
   {
      if (gDebug)
      {
         Mout << "i_calc: " << i_calc << std::endl;
      }

      TDQM1DDrv();
   }

   Mout << "\n\n";
   Out72Char(Mout,'#','#','#');
   Out72Char(Mout,'#',' ','#');
   OneArgOut72(Mout," TD done.",'#');
   Out72Char(Mout,'#',' ','#');
   Out72Char(Mout,'#','#','#');
}
