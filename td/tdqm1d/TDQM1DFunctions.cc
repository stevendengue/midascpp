// Standard headers.
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
//#include <tgmath.h>

// Midas headers.
#include "util/Io.h"
#include "td/tdqm1d/TDQM1DFunctions.h"

//using namespace std;

int factorial(int n)
{
    if(n!=1 && n!=0)
    {
        return n*factorial(n-1);
    }
    else return 1;
}

double dot(vector<double> vec1, vector<double> vec2)
{
    double prik = 0;
    for (int k = 0; k<vec1.size(); k++)
    {
        prik += vec1[k]*vec2[k];
    }
    return prik;
}

vector<double> harmwave(int n, double mass, double vibfreq, vector<double> vec, double x, double t)
{
    double hbar=1.0545718 * pow(10,-34);
    double pi = 2*std::asin(1);
    double gaussian = std::exp(-mass*vibfreq*x*x/(2*hbar));
    
    vector<double> normconst(n);
    for (int i=0; i<n; i++)
    {
        normconst[i]=1/(std::sqrt(factorial(i)*std::pow(2,i)))*std::pow((mass*vibfreq/(pi*hbar)),0.25);
    }
    
    double inp = 0;
    for (int i = 0; i<vec.size(); i++)
    {
        inp += vec[i]*vec[i];
    }
    double normconst2=1/std::sqrt(inp);
    
    double z = std::sqrt(mass*vibfreq/hbar)*x;
    vector<double> herm(n);
    herm[0] = 1;
    herm[1] = 2*z;
    for (int i=2; i<n; i++)
    {
        herm[i] = 2*z*herm[i-1]-2*(i-1)*herm[i-2];
    }
    
    vector<double> E(n);
    vector<double> timereal(n);
    vector<double> timeimag(n);
    vector<double> trcoeff(n);
    vector<double> ticoeff(n);
    for (int i=0; i<n; i++)
    {
        E[i] = (i + 0.5)*hbar*vibfreq;
        timereal[i] = std::cos(-E[i]*t/hbar);
        timeimag[i] = std::sin(-E[i]*t/hbar);
        trcoeff[i] = timereal[i]*vec[i]*normconst[i];
        ticoeff[i] = timeimag[i]*vec[i]*normconst[i];
    }
    
    double wpreal = normconst2*gaussian*dot(trcoeff,herm);
    double wpimag = normconst2*gaussian*dot(ticoeff,herm);
    
    vector<double> wp(2);
    wp[0] = wpreal;
    wp[1] = wpimag;
    
    return wp;
}

int harmonic(double INPUT_HARMONIC_mass, double INPUT_HARMONIC_vibfreq, int INPUT_HARMONIC_s, int INPUT_HARMONIC_x, int INPUT_HARMONIC_t, vector<double> INPUT_HARMONIC_vec)
{
    double pi = 2*std::asin(1);
    double c10 = 2.998*std::pow(10,10);
    double mu = 1.660538921*std::pow(10,-27);
    
    double vibfreq = INPUT_HARMONIC_vibfreq*(2*pi*c10);
    double mass = INPUT_HARMONIC_mass*mu;
    int x = 10*INPUT_HARMONIC_x;
    int t = 10*INPUT_HARMONIC_t;
    
    ofstream outfile1 ("HarmonicWavepacket.txt");
    outfile1 << "Wave packet" << endl;
    outfile1 << "Position (pm);Time (fs);Wave packet (real part);Wave packet (imaginary part);Square modulus" << endl;
    for (int time = 0; time<t+1; time++)
    {
        for (int pos = -x; pos<x+1; pos++)
        {
            double posm = pos*std::pow(10,-13);
            double timem = time*std::pow(10,-16);
            vector<double> wp = harmwave(INPUT_HARMONIC_s,mass,vibfreq,INPUT_HARMONIC_vec,posm,timem);
            double posout = pos/10.0;
            double timout = time/10.0;
            outfile1 << posout << ";" << timout << ";" << wp[0] << ";" << wp[1] << ";" << wp[0]*wp[0]+wp[1]*wp[1] << endl;
        }
    }
    outfile1.close();
    
    ofstream outfile2 ("HarmonicOverlap.txt");
    outfile2 << "Overlap integral <Ψ(0)|Ψ(t)>" << endl;
    outfile2 << "OTime (fs);Overlap (real part);Overlap (imaginary part)" << endl;
    for (int time = 0; time<t+1; time++)
    {
        vector<double> overlap(2*x+1);
        vector<double> overlapimag(2*x+1);
        for (int pos = -x; pos<x+1; pos++)
        {
            int i = pos + x;
            double posm = pos*std::pow(10,-13);
            double timem = time*std::pow(10,-16);
            vector<double> wp0 = harmwave(INPUT_HARMONIC_s,mass,vibfreq,INPUT_HARMONIC_vec,posm,0.0);
            vector<double> wp = harmwave(INPUT_HARMONIC_s,mass,vibfreq,INPUT_HARMONIC_vec,posm,timem);
            double dx = 1.0*std::pow(10,-13);
            overlap[i] = wp0[0]*wp[0]*dx;
            overlapimag[i] = wp0[0]*wp[1]*dx;
        }
        double timout=time/10.0;
        
        double overint = 0;
        double overintimag = 0;
        for (int f=0; f<2*x+1; f++)
        {
            overint += overlap[f];
            overintimag += overlapimag[f];
        }
        outfile2 << timout << ";" << overint << ";" << overintimag << endl;
        
        if (timout == 0 && overint < 0.999)
        {
            cout << "WARNING! ∫|Ψ(x,0)|²dx = " << overint << " < 1. Consider choosing a larger interval around r=rₑ." << endl;
        }
    }
    outfile2.close();
    
    ofstream outfile3 ("HarmonicExpvalue.txt");
    outfile3 << "Expectation value of x" << endl;
    outfile3 << "ETime (fs);Expectation value (pm)" << endl;
    for (int time = 0; time<t+1; time++)
    {
        vector<double> mean(2*x+1);
        for (int pos = -x; pos<x+1; pos++)
        {
            int i = pos + x;
            double posm = pos*std::pow(10,-13);
            double timem = time*std::pow(10,-16);
            vector<double> wp = harmwave(INPUT_HARMONIC_s,mass,vibfreq,INPUT_HARMONIC_vec,posm,timem);
            double dx = 1.0*std::pow(10,-13);
            mean[i]=(posm*(wp[0]*wp[0]+wp[1]*wp[1]))*dx;
        }
        
        double timout = time/10.0;
        double meansum = 0;
        for (int f=0; f<2*x+1; f++)
        {
            meansum += mean[f];
        }
        
        if (meansum < 1.0*std::pow(10,-18) && meansum > -1.0*std::pow(10,-18))
        {
            meansum = 0;
        }
        double meansumout = meansum*std::pow(10,12);
        outfile3 << timout << ";" << meansumout << endl;
    }
    outfile3.close();
    return 0;
}

vector<double> morsewave(int n, double ke, double de, double mass, vector<double> vec, double posx, double t, double freq)
{
    double pi = 2*std::asin(1);
    double c10 = 2.998*std::pow(10,10);
    double hbar=1.0545718*std::pow(10,-34);
    double h = 2*pi*hbar;
    
    double a = std::sqrt(ke/(2*de));
    double lambda = (std::sqrt(2*mass*de))/(a*hbar);
    double z = 2*lambda*std::exp(-a*posx);
    double xe = freq*h*c10/(4*de);
    
    double expo = std::exp(-0.5*z);
    
    vector<double> normconst(n);
    for (int i=0; i<n; i++)
    {
        normconst[i]=std::sqrt((a*factorial(i)*(2*lambda-2*i-1))/(tgamma(2*lambda-i)))*std::pow(z,lambda-i-0.50);
    }
    
    double inp = 0;
    for (int i = 0; i<vec.size(); i++)
    {
        inp += vec[i]*vec[i];
    }
    double normconst2=1/std::sqrt(inp);
    
    vector<double> b(n);
    for (int i=0; i<n; i++)
    {
        b[i]=2*lambda-2*i-1;
    }
    
    float laguerre[n][n];
    
    for (int j = 0; j<n; j++)
    {
        laguerre[0][j] = 1;
        laguerre[1][j] = 1 + b[j] - z;
    }
    
    for (int k = 2; k<n; k++)
    {
        for (int l = k; l<n; l++)
        {
            laguerre[k][l] = ((2*(k-1)+1+b[l]-z)*laguerre[k-1][l]-(k-1+b[l])*laguerre[k-2][l])/k;
        }
    }
    
    vector<double> lagu(n);
    for (int i=0; i<n; i++)
    {
        lagu[i]=laguerre[i][i];
    }
    
    vector<double> E(n);
    vector<double> timereal(n);
    vector<double> timeimag(n);
    vector<double> trcoeff(n);
    vector<double> ticoeff(n);
    for (int i=0; i<n; i++)
    {
        E[i] = h*c10*(freq*(i+0.5) - freq*xe*(i+0.5)*(i+0.5));
        timereal[i] = std::cos(-E[i]*t/hbar);
        timeimag[i] = std::sin(-E[i]*t/hbar);
        trcoeff[i] = timereal[i]*vec[i]*normconst[i];
        ticoeff[i] = timeimag[i]*vec[i]*normconst[i];
    }
    
    double wpreal = normconst2*expo*dot(trcoeff,lagu);
    double wpimag = normconst2*expo*dot(ticoeff,lagu);
    
    vector<double> wp(2);
    wp[0] = wpreal;
    wp[1] = wpimag;
    
    return wp;
}

int morse(double INPUT_MORSE_mass, double INPUT_MORSE_diss, double INPUT_MORSE_freq, int INPUT_MORSE_s, int INPUT_MORSE_x, int INPUT_MORSE_t, vector<double> INPUT_MORSE_vec)
{
    double pi = 2*std::asin(1);
    double c10 = 2.998*std::pow(10,10);
    double mu = 1.660538921*std::pow(10,-27);
    double ech = 1.60217662*std::pow(10,-19);
    
    double mass = INPUT_MORSE_mass*mu;
    double diss = INPUT_MORSE_diss*ech;
    double force = 4.0*pi*pi*c10*c10*INPUT_MORSE_freq*INPUT_MORSE_freq*mass;
    int x = 10*INPUT_MORSE_x;
    int t = 10*INPUT_MORSE_t;
    
    ofstream outfile1 ("MorseWavepacket.txt");
    outfile1 << "Wave packet" << endl;
    outfile1 << "Position (pm);Time (fs);Wave packet (real part);Wave packet (imaginary part);Square modulus" << endl;
    for (int time = 0; time<t+1; time++)
    {
        for (int pos = -x; pos<x+1; pos++)
        {
            double posm = pos*std::pow(10,-13);
            double timem = time*std::pow(10,-16);
            vector<double> wp = morsewave(INPUT_MORSE_s,force,diss,mass,INPUT_MORSE_vec,posm,timem,INPUT_MORSE_freq);
            double posout = pos/10.0;
            double timout = time/10.0;outfile1 << posout << ";" << timout << ";" << wp[0] << ";" << wp[1] << ";" << wp[0]*wp[0]+wp[1]*wp[1] << endl;
        }
    }
    outfile1.close();
    
    ofstream outfile2 ("MorseOverlap.txt");
    outfile2 << "Overlap integral <Ψ(0)|Ψ(t)>" << endl;
    outfile2 << "OTime (fs);Overlap (real part);Overlap (imaginary part)" << endl;
    for (int time = 0; time<t+1; time++)
    {
        vector<double> overlap(2*x+1);
        vector<double> overlapimag(2*x+1);
        for (int pos = -x; pos<x+1; pos++)
        {
            int i = pos + x;
            double posm = pos*std::pow(10,-13);
            double timem = time*std::pow(10,-16);
            vector<double> wp0 = morsewave(INPUT_MORSE_s,force,diss,mass,INPUT_MORSE_vec,posm,0.0,INPUT_MORSE_freq);
            vector<double> wp = morsewave(INPUT_MORSE_s,force,diss,mass,INPUT_MORSE_vec,posm,timem,INPUT_MORSE_freq);
            double dx = 1.0*std::pow(10,-13);
            overlap[i] = wp0[0]*wp[0]*dx;
            overlapimag[i] = wp0[0]*wp[1]*dx;
        }
        double timout=time/10.0;
        
        double overint = 0;
        double overintimag = 0;
        for (int f=0; f<2*x+1; f++)
        {
            overint += overlap[f];
            overintimag += overlapimag[f];
        }
        outfile2 << timout << ";" << overint << ";" << overintimag << endl;
        
        if (timout == 0 && overint < 0.999)
        {
            cout << "WARNING! ∫|Ψ(x,0)|²dx = " << overint << " < 1. Consider choosing a larger interval around r=rₑ." << endl;
        }
    }
    outfile2.close();
    
    ofstream outfile3 ("MorseExpvalue.txt");
    outfile3 << "Expectation value of x" << endl;
    outfile3 << "ETime (fs);Expectation value (pm)" << endl;
    for (int time = 0; time<t+1; time++)
    {
        vector<double> mean(2*x+1);
        for (int pos = -x; pos<x+1; pos++)
        {
            int i = pos + x;
            double posm = pos*std::pow(10,-13);
            double timem = time*std::pow(10,-16);
            vector<double> wp = morsewave(INPUT_MORSE_s,force,diss,mass,INPUT_MORSE_vec,posm,timem,INPUT_MORSE_freq);
            double dx = 1.0*std::pow(10,-13);
            mean[i]=(posm*(wp[0]*wp[0]+wp[1]*wp[1]))*dx;
        }
        
        double timout = time/10.0;
        double meansum = 0;
        for (int f=0; f<2*x+1; f++)
        {
            meansum += mean[f];
        }
        
        if (meansum < 1.0*std::pow(10,-18) && meansum > -1.0*std::pow(10,-18))
        {
            meansum = 0;
        }
        double meansumout = meansum*std::pow(10,12);
        outfile3 << timout << ";" << meansumout << endl;
    }
    outfile3.close();
    
    return 0;
}
