/**
 *******************************************************************************
 * 
 * @file    TDQM1DDrv.cc
 * @date    07-08-2016
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Contains the driver function for the TDQM1D (time dependent quantum
 *    mechanics in 1 dimension)  module.
 *
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

// Standard headers.
#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <tgmath.h>

// Midas headers.
#include "util/Io.h"
#include "td/tdqm1d/TDQM1DFunctions.h"

/**
 * @brief
 *    Runs TDQM1D (time dependent quantum mechanics in 1 dimension)
 *    calculations.
 **/

void TDQM1DDrv()
{
    char INPUT_model = 'H';
    
    double INPUT_HARMONIC_mass = 0.5; //mu
    double INPUT_HARMONIC_vibfreq = 4401.21; //cm-1
    int INPUT_HARMONIC_s = 1;
    int INPUT_HARMONIC_x = 50; //pm
    int INPUT_HARMONIC_t = 50; //fs
    std::vector<double> INPUT_HARMONIC_vec(INPUT_HARMONIC_s);
    for (int i=0; i<INPUT_HARMONIC_s; i++)
    {
        INPUT_HARMONIC_vec[i] = 1;
    }
    
    double INPUT_MORSE_mass = 0.5; //mu
    double INPUT_MORSE_freq = 4401.21; //cm-1
    double INPUT_MORSE_diss = 4.745; //eV
    int INPUT_MORSE_s = 1;
    int INPUT_MORSE_x = 50; //pm
    int INPUT_MORSE_t = 50; //fs
    std::vector<double> INPUT_MORSE_vec(INPUT_MORSE_s);
    for (int i=0; i<INPUT_MORSE_s; i++)
    {
        INPUT_MORSE_vec[i] = 1;
    }
    
    if (INPUT_model == 'H')
    {
        harmonic(INPUT_HARMONIC_mass, INPUT_HARMONIC_vibfreq, INPUT_HARMONIC_s, INPUT_HARMONIC_x, INPUT_HARMONIC_t, INPUT_HARMONIC_vec);
    }
    else if (INPUT_model == 'M')
    {
        morse(INPUT_MORSE_mass, INPUT_MORSE_diss, INPUT_MORSE_freq, INPUT_MORSE_s, INPUT_MORSE_x, INPUT_MORSE_t, INPUT_MORSE_vec);
    }
}
