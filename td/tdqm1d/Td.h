/**
 *******************************************************************************
 * 
 * @file    Td.h
 * @date    07-08-2016
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    For TD (time dependent) module.
 *
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TD_H_INCLUDED
#define TD_H_INCLUDED

// Declarations.
void TdDrv();

#endif/*TD_H_INCLUDED*/
