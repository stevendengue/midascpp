#ifndef TDQM1DFUNCTIONS_H_INCLUDED
#define TDQM1DFUNCTIONS_H_INCLUDED

// Standard headers.
#include <iostream>
#include <fstream>
#include <vector>

// Midas headers.
#include "util/Io.h"
#include "td/tdqm1d/TDQM1DFunctions.h"

int factorial(int n);
double dot(vector<double> vec1, vector<double> vec2);
vector<double> harmwave(int n, double mass, double vibfreq, vector<double> vec, double x, double t);
int harmonic(double INPUT_HARMONIC_mass, double INPUT_HARMONIC_vibfreq, int INPUT_HARMONIC_s, int INPUT_HARMONIC_x, int INPUT_HARMONIC_t, vector<double> INPUT_HARMONIC_vec);
vector<double> morsewave(int n, double ke, double de, double mass, vector<double> vec, double posx, double t, double freq);
int morse(double INPUT_MORSE_mass, double INPUT_MORSE_diss, double INPUT_MORSE_freq, int INPUT_MORSE_s, int INPUT_MORSE_x, int INPUT_MORSE_t, vector<double> INPUT_MORSE_vec);

#endif/*TDQM1DFUNCTIONS_H_INCLUDED*/
