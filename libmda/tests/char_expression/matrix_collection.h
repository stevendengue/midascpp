#ifndef LIBMDA_TESTS_MATRIX_TEST_CASE_H
#define LIBMDA_TESTS_MATRIX_TEST_CASE_H

#include "addition_test.h"
#include "contraction_test.h"

using namespace libmda::tests::char_expression;

namespace libmda
{
namespace tests
{

template<class matrix_type>
struct matrix_collection: virtual public cutee::collection
{
   matrix_collection()
   {
      // addition/subtraction tests
      add_test<addition_test_square_mat<matrix_type> >("addition_test square matrix");
      add_test<plusequal_test_square_mat<matrix_type>>();
      add_test<minusequal_test_square_mat_trans<matrix_type> >();
      add_test<subtraction_test_square_mat<matrix_type> >();
      add_test<addition_test_square_mat_trans<matrix_type> >();
      add_test<subtraction_test_square_mat_trans<matrix_type> >();
      add_test<addition_test_mat<matrix_type> >();
      add_test<addition_test_mat_trans<matrix_type> >();
      add_test<addition_test_mat_trans_2<matrix_type> >();
      
      // contraction tests
      add_test<contraction_test_square_mat<matrix_type> >();
      add_test<contraction_test_square_mat_trans<matrix_type> >();
      add_test<contraction_test_mat<matrix_type> >();
      add_test<contraction_addition_test_square_mat<matrix_type> >();
   };
};

template<class vector_type>
struct vector_collection: virtual public cutee::collection
{
   vector_collection()
   {

   }
};

template<class tensor_type>
struct tensor_collection: virtual public cutee::collection
{
   tensor_collection()
   {

   }
};

template<class matrix_type, class vector_type>
struct matrix_vector_collection: 
   virtual public cutee::collection, 
   virtual public matrix_collection<matrix_type>, 
   virtual public vector_collection<vector_type>
{ 
   matrix_vector_collection()
   {
      add_test<contraction_test_mat_vec<matrix_type,vector_type> >();
   }
};

template<class tensor_type, class matrix_type, class vector_type>
struct tens3d_matrix_vector_collection:
   virtual public cutee::collection,
   virtual public matrix_vector_collection<matrix_type,vector_type>,
   virtual public tensor_collection<tensor_type>
{
   tens3d_matrix_vector_collection()
   {
      add_test<contraction_test_tens3d_mat_vec<tensor_type,matrix_type,vector_type> >();
   }
};

} // namespace tests
} // namespace libmda

#endif /* LIBMDA_TESTS_MATRIX_TEST_CASE_H */
