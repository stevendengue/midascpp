/**
************************************************************************
* 
* @file                MidasOperatorWriter.h
* 
* Created:             19-05-2014
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Class for writing mop files to disk
* 
* Last modified: Mon May 19, 2014  03:07PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDASOPERATORWRITER_H_INLCUDED
#define MIDASOPERATORWRITER_H_INLCUDED

#include <vector>
#include <utility>
#include <map>
#include <string>
#include <fstream>
#include <tuple>

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "util/conversions/FromString.h"
#include "input/OperatorLessThan.h"

class MidasOperatorReader;

/**
 *
 **/
class MidasOperatorWriter
{
   private:
      
      //!
      std::string                                        mFileName;
      
      //!
      Nb                                                 mReferenceValue;
      //!
      std::vector<std::string>                           mModeNames;
      
      //!
      MidasVector                                        mScalings;
      
      //!
      MidasVector                                        mFrequencies;
      
      //!
      std::string                                        mInfoLabel;

      //!
      std::map<std::string, Nb>                          mConstants;
      
      //! Format : name, function
      std::map<std::string, std::string>                 mFunctions; 

      //! Map with pair of mode numbers and operators/functions and associated linear coefficient 
      std::map<std::pair<std::vector<std::string>, std::vector<std::string> >, Nb, operator_less_than>             mOperators;

      //!
      std::string ReadOperator(In aLevel, const std::string& aName, std::istream& aOpFile);

      //! Standard equal and copy construction works just fine
      MidasOperatorWriter() = delete;

      //! The following functions check a new scaling or frequency vector against the old one
      bool CheckScalings(const MidasVector& aNewScal) const;
      bool CheckFrequencies(const MidasVector& aNewFreq) const;

      //!
      void SortModes();

   public:

      MidasOperatorWriter(const std::string& aName) : mFileName(aName), mReferenceValue(0.0)
      {
      }
      
      //! The destructor writes the file to disk
      ~MidasOperatorWriter()
      {
         SortModes();
         WriteFile();
      }

      //! This function adds a reference value to the operator writer
      void AddReferenceValue(Nb aRef) {mReferenceValue = aRef;}

      //! These functions adds the scaling factors or frequencies, if either is already added they will call MIDASERROR
      void AddScalings(const MidasVector&);
      void AddFrequencies(const MidasVector&);

      //! Write file to disk
      void WriteFile() const;

      //! Interface for adding individual operator terms
      void AddOperatorTerm(Nb aCoeff, const std::vector<std::string>& aFunctions, const std::vector<std::string>& aModes);

      //! Add a property label to the operator file
      void AddPropertyLabel(const std::string& aInfoLabel) 
      {  
         if(mInfoLabel.empty())
         {
            mInfoLabel = aInfoLabel;
         }
         else
         {
            if(mInfoLabel != aInfoLabel)
            {
               MIDASERROR("Setting different info label: mInfoLabel = '" + mInfoLabel + "'   aInfoLabel = '" + aInfoLabel + "'.");
            }
         }
      }

      //! Interface for adding a vector of functions, format of vector is pair<name, function>
      void AddFunctions(const std::vector<std::pair<std::string, std::string> >& aVectOfFunc);

      //! Interface for adding a map of functions, format of map is <name, function>
      void AddFunctions(const std::map<std::string, std::string>& aMapOfFunc);

      //! Interface for adding a single function
      void AddFunction(const std::string& aName, const std::string& aFunc);

      //! Interface for adding a vector of constants
      void AddConstants(const std::vector<std::pair<std::string, Nb> >& aVectOfConst);

      //! Interface for adding a map of constants
      void AddConstants(const std::map<std::string, Nb>& aMapOfConst);

      //! Interface for adding a single constant
      void AddConstant(const std::string& aName, Nb aConst);

      //! Makes a read potential ready for output with the scaling factor aCoeff, which is applied to all operator term coeffs.
      void FromReadPotential(const MidasOperatorReader&, const Nb& aCoeff = 1);

      //! Makes a set of potentials ready for output. This method takes all operator terms of the first operator and saves the MC level. Then all higher MC level terms of the second operator will be included, etc.
      void FromReadPotentials(const std::vector<MidasOperatorReader>&);

      //! Makes a set of potentials ready for output. It assumes that the potentials are of equal MC level. If not it will only merge to the MC level of the first oprator and give a warning in the output file
      void FromReadAndScaledPotentials(const std::vector<MidasOperatorReader>&, const std::vector<Nb>&);

      //! adds another potential (with aCoeff) handling also overlap in modenames and scaling vectors.
      void AddPotential(const MidasOperatorReader& aOper, const Nb& aCoeff);

      //! Add ModeNames
      void AddModeNames(const std::vector<std::string>& aModeNames)  
      {
         for(auto it = aModeNames.begin(); it != aModeNames.end(); ++it)
         {
            mModeNames.push_back(*it);
         }
      }

      //!
      void AddModeName(const std::string& aModeName)
      {
         mModeNames.push_back(aModeName);
      }

      //!
      const std::map<std::pair<std::vector<std::string>, std::vector<std::string> >, Nb, operator_less_than>& GetmOperators() const {return mOperators;}

};

#endif /* MIDASOPERATORWRITER_H_INLCUDED */
