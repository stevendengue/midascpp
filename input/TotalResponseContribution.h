/**
************************************************************************
* 
* @file                TotalRspFunc.h
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   Total response function framework
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TOTALRSPCONTRIB_H
#define TOTALRSPCONTRIB_H

// std headers
#include <string>
#include <vector>
#include <iostream> 
#include <map>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"

// using declarations
using std::map;
using std::string;

/**
* Construct a definition of a total response calculation calculation
* */
class TotalResponseContribution
{
   private:
      map<string,Nb> mMap;

   public:
      TotalResponseContribution();
      void AddTerm(const string& aS, Nb aN) {mMap.insert(make_pair(aS,aN));}
      Nb GetTerm(const string& aS) const 
      {
         map<string,Nb>::const_iterator it=mMap.find(aS);
         if(it!=mMap.end())
            return it->second;
         MIDASERROR("Searched TotalResponseContribution for "+aS+" but found nothing!");
         return C_0;
      }
      map<string,Nb> GetMap() {return mMap;}
      vector<string> GetTypes() 
      {
         vector<string> result;
         map<string,Nb>::iterator it;
         for(it=mMap.begin();it!=mMap.end();it++)
            result.push_back(it->first);
         return result;
      }
      Nb GetTotalValue() const;
      Nb GetTotalEqValue() const;
      void PrintMap(ostream&) const;
      friend ostream& operator<<(ostream&,const TotalResponseContribution&);
};

#endif /* TOTALRSPCONTRIB_H */
