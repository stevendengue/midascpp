/**
 *******************************************************************************
 * 
 * @file    TimTdvccCalcDef.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

// Midas Headers
//#include "inc_gen/TypeDefs.h"
#include "input/TimTdvccCalcDef.h"
#include "input/OdeInput.h"
//#include "input/GetLine.h"
//#include "input/Trim.h"
//#include "input/IsKeyword.h"
//#include "util/conversions/VectorFromString.h"

/**
 * Sanity check
 **/
void TimTdvccCalcDef::SanityCheck
   (
   )
{
   // Check that we have ODE-integrator info.
   if (  mOdeInfo.empty()
      )
   {
      MIDASERROR("No OdeInfo in TimTdvccCalcDef!");
   }

   // Checks on TdPropertyDef
   TdPropertyDef::SanityCheck(this->mOdeInfo, this->mImagTime);

   // Check that valid enum-strings are given.
   // The EnumFromString causes MIDASERROR if invalid string.
   for(const auto& val: this->Properties())
   {
      midas::tdvcc::EnumFromString<midas::tdvcc::PropID>(val);
   }
   for(const auto& val: this->Stats())
   {
      midas::tdvcc::EnumFromString<midas::tdvcc::StatID>(val);
   }
}

/**
 * Read OdeInfo
 *
 * @param arInp
 * @param arS
 * @return
 *    Already read next keyword?
 **/
bool TimTdvccCalcDef::ReadOdeInfo
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   // Overwrite mOdeInfo
   return OdeInput(arInp, arS, 4, this->mOdeInfo);
}

/**
 * Ostream overload for TimTdvccCalcDef
 *
 * @param aOs
 * @param aTcd
 * @return
 *    os
 **/
std::ostream& operator<<
   (  std::ostream& aOs
   ,  const TimTdvccCalcDef& aTcd
   )
{
   aOs << "TimTdvccCalcDef::" << aTcd.Name() << std::endl;
   return aOs;
}
