/**
************************************************************************
* 
* @file                PesCalcDef.cc
*
* Created:             16-07-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Base class for a Pes input-info handler 
* 
* Last modified:       24-11-2014 (Carolin Koenig)
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include "input/PesCalcDef.h"

#include <string>
#include <vector>

#include "input/Input.h"
#include "util/Io.h"
#include "util/FileSystem.h"
#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"
#include "input/GetLine.h"
#include "input/FileConversionInput.h"
#include "mpi/FileToString.h"

PesCalcDef::PesCalcDef()
{
   mPesFitBasisName     = "DEFAULT_FITBASIS_NAME";
   mPesScalCoordInFit   = false;
   mSpecBas             = false;
   mAddPsuedoPotToEn    = false;
   mGMatDiagonal        = false;
   mGMatConst           = false;
   mNoDerivOfGDet       = false;
   mNormalcoordInPes    = true;
   mZmatInPes           = false;
   mCartesiancoordInPes = false;
   mFalconInPes         = false;
   mPscInPes            = false;
   mTanaRunFile         = "";
   mPesNumDalton        = I_0;
   mPesNumAces          = I_0;
   mPesNumCfour         = I_0;
   mPesNumEsp           = I_0;
   mPesNumBs            = I_0;
   mPesNumNucInp        = I_0;
   mPesNumAnalyticOrder = I_0;
   mPesNumMCR           = I_0;
   mNumDerFreqThres     = C_20;
   mPesNumMaxPolOrExp   = I_0;
   mPesExcState         = I_0;
   mPesGridMaxDim       = I_10;
   mPesGridInitialDim   = I_2;
   mAnalyzeStates       = {};
   mDumpSpInterval      = I_100;          ///< Dump calculated single points to disc after specified number of calculations have been carried out
   mCutMesh             = I_10;
   mPesDiatomic         = false;          ///< a diatomic
   mPesVibPol           = false;          ///< Vibrational polarizability
   mPesGrid             = false;          ///< Grid based PES
   mPesAvoidRestart     = false;          ///< Do not avoid restart 
   mSaveItOperFile      = false;          ///< Save operator file (.mop) after each ADGA iteration
   mSaveSpScratchDir    = false;          ///< Save the individuel single point scratch directories
   mSavePscGeomScrDir   = false;          ///< Save the individuel geometry scratch directories for the use of polyspherical coordinates
   mSaveDispGeom        = false;          ///< Save displaced geometries to file
   mDoNotSaveEspOut     = true;           ///< Do not save Dalton.out
   mDoNotSaveInpFiles   = true;           ///< Do not save input files
   mNThreads            = {I_0};
   mPesStepSizeInAu     = C_0;            ///< Stepsize in NumDer
   mPesDispType         = "FREQDISPLACEMENT";
   mIntType             = "";             ///< Type of interpolation to be used
   mPesSplineInt        = false;          ///< Spline interpolation
   mSplineType          = SPLINEINTTYPE::NONE; ///< Type of spline used for spline interpolaton
   mPesSplineIntForExtrap  = false;       ///< Spline interpolation
   mPesShepardInt       = false;          ///< Shepard interpolation
   mPesShepardOrd       = -I_1;           ///< Shepard order
   mShepardExp          = I_0;            ///< Shepard exponent
   mPesShepardIntForExtrap = false;       ///< Shepard interpolation
   mPesPlotAll          = false;          ///< decide wheter a plot of the potentials and properties
   mPesPlotRealESP      = false;          ///< Plot the electronic structure points before they are fitted
   mPesPlotSelected     = false;          ///< decide wheter a plot of selected potentials and properties
   mGenericPes.clear();
   mPesFreqScalCoordInFit  = true;        ///< Use frequency scaled coordinates in fitting routine 
   mPesDoInt               = false;       ///< Do interpolation for multilevel surface
   mPesDoIntForExtrap      = false;       ///< Do interpolation for extrapolated surface
   mPesCalcMuTens          = false;       ///< determines whether calculate mu tensor for subsequent inclusion of watson operators
   mPesCalcCoriolisAndInertia = true;     ///< calculate the Coriolis coupling and moment of inertia
   mPesInertialFrame       = false;       ///< enable the reference structure to be oriented in the inertial frame
   mPesModelPot            = false;       ///< Enable the use of model potentials   
   mPesDoPartridgePot      = false;       ///< Use the Partridge-Schwenke potential surface for water
   mPesDoAmmModelPot       = false;       ///< Use the NH3 model potential surface
   mPesDoTestPot           = false;       ///< Use the Test Potential
   mPesDoArNtwoModelPot    = false;       ///< Use the Ar-N2+ model potential surface
   mPesType                = I_0;         ///< Which PES to generate
   mPesDoMorsePot          = false;       ///< Use the Morse Potential
   mPesDoBrennerPot        = false;       ///< Use the Brenner Potential
   mPesMorseDefs           = "";          ///< Additional infos on the morse potential
   mPesDoDoubleWpot        = false;       ///< Use a double well potential
   mPesDoubleWdefs         = "";          ///< Additional infos on the double well potential
   mPesTaylor              = false;       ///< Do Taylor expansion non-grid-based surface generation 
   mPesStatic              = false;       ///< Do Static grid-based surface generation
   mPesAdga                = false;       ///< Do Adga grid-based surface generation
   mAdgaVscfNames          = {};          ///< Names of Vscf calculations used in the Adga
   mDynamicAdgaExt         = false;       ///< Dynamic extension of Adga grid
   mDoExtInFirstIter       = false;       ///< Do not extent Adga grid beginning from first iteration
   mCalcDensAtEachMcl      = false;       ///< Calculate density each mode combination level
   mDoAdgaExtAtEachMcl     = false;       ///< Check potential bounds for each mode combination level
   mUpdMeanDensAtEachMcl   = false;       ///< Calculate and update density for each mode combination level
   mPesGLQuadPnts          = I_12;        ///< The number of points used in numerical integration by means of Gauss-Legendre quadrature
   mPolyInterPoints        = I_4;         ///< Number of points to be used for polynomial interpolation
   mPesIterMax             = I_10;        ///< Max nr. of iterations in the Adga procedure
   mAnalyzeDensMethod      = "MEAN";      ///< Density analysis method
   mPesUseMaxDens          = false;       ///< Analysize the max. density
   mPesUseMeanDens         = false;       ///< Analysize the mean density (default)
   mPesPropInMem           = true;        ///< Store sorted properties in memory
   mPesItPotThr            = C_I_10_2;    ///< Thresh. for potential in Adga pes
   mPesItDensThr           = C_I_10_2;    ///< Thresh. for density in Adga pes
   mPesItVdensThr          = C_I_10_2;    ///< Thresh. for pot*density in Adga pes
   mPesItNormVdensThr      = C_I_10_2;    ///< Threshold for norm of pot*density for ADGA
   mPesItResEnThr          = C_I_10_6;    ///< Thresh. for residual pot*density in the external intervals 
   mPesItNormResEnThr      = C_I_10_6;    ///< Thresh. for residual norm pot*density in the external intervals 
   mPesItResDensThr        = C_I_10_3;    ///< Thresh. for residual density in the external intervals
   mPesPlotAdga2D          = false;       ///< Plot the 2D potentials  at each iteration 
   mPesUseItPotThr         = false;       ///< Use only v for checking convergency
   mPesUseItDensThr        = false;       ///< Use only rho for checking convergency
   mPesUseItVdensThr       = true;        ///< Use only rho*v for checking convergency
   mPesUseItNormVdensThr   = false;       ///< Use only norm of rho*v to check convergence of ADGA
   mPesCalcPotFromOpFile   = false;       ///< Allow the estimation of the potential from the .mop file
   mPesAdaptivSearch       = false;       ///< Adapt. search crit for grid updates
   mPesNmaxSplittings      = I_0;         ///< How many boxes are splitted. If negative also the symmetric will be splitted 
   mUseMultiStateAdga      = false;       ///< Use multistate Adga
   mPesAdaptiveProps       = {I_1};       ///< the property surfaces to be used in the ADGA algorithm
   mPesBarFileStorageType  = "ONFILE";    ///< The type of storage for bar file info
   mPesItGridExpScalFact   = I_2;         ///< Expansion factor for the Adga grids 
   mPesMultiLevelDone      = false;       ///< Number of levels in the multilevel approach.
   mPesDoLinearComb        = false;
   mPesBoundariesPreopt    = false;
   mPesDoExtrap            = false;       ///< Extrapolate points in the PES
   mMeanDensOnFiles        = false;       ///< Mean density is provided and not calculated
   mUseExtrapStartGuess    = false;       ///< Use extrapolation as start guess fro coupled surfaces
   mExtrapolateFrom        = I_0;         ///< Mode Coupling level to be used for approx.
   mExtrapolateTo          = I_0;         ///< Mode Coupling level to be approximated
   mExtrapOnlyGradient     = false;             ///< Use only gradients in extrapolation
   mPotentialFilename      = "prop_no_1.mop";   ///< Default file to read the potential
   mMmParFilename          = "";                ///< Name of parameter file for MM calculations
   mMmCoordFilename        = "";                ///< Name of coord file for MM calculations
   mPscMaxGridBounds       = {-C_1/C_100, -C_2/C_100, -C_2/C_100};    ///< Modifier for the maximum grid boundaries
   mPesScreenModeCombi     = false;             ///<Allow screening of mode combi based on specific criteria
   mPesScreenThresh        = C_0;               ///< Threshold for screening
   mPesScreenMethod        = -I_1;              ///< Screening method
   mUseSym                 = false;             ///< logical flag for use of symmetry of the pes
   mPesScreenSymPotTerms   = false;             ///< logical flag for use of symmetry of the SCREENING of potential terms
   mSymThr                 = C_10*C_I_10_6;     ///< Threshold for detecting symmetry
   mPesSymLab              = "C1";              ///< Point group symmetry label
   mPesAdgaFgStep          = 0.05;              ///< relative step size in adga Fine Grid definition
   mPesFGScalFact          = {0.9};             ///< Define scaling factors for each dimension in the finer grid
   mPes_Grid_Scal          = {1.0};             ///< Scaling factor for grids
   mPesMsiLambda           = 8;
   mAdgaRelConvScalFacts   = {1.0, 1.0, 20.0, 30.0, 30.0, 30.0};  ///< Scale factors for the relative Adga convergence criterion
   mAdgaAbsConvScalFacts   = {1.0, 1.0, 1.0, 10.0, 10.0, 10.0};   ///< Scale factors for the absolute Adga convergence criterion
   mPesAdgaMsiCut          = false;             ///< Shepard interpolation during adga stop at a given iteration
   mPesAdgaMsiIt           = {I_10, I_10, I_10, I_10, I_10};
   mCurrentESProg          = I_0;
   mPesDoAdgaPreScreen     = false;

   this->AddSinglePoint(""); // first entry is reserved for if we want to do BoundariesPreOpt
   mLevelMc.emplace_back(0);
   mDincrSurface           = false;             ///< Use double incremental scheme for surface generation
   mDincrMethod            = "";                ///< Type of double incremental scheme to use
   mFcInfo                 = "";                ///< File with information on the fragment combinations
   mDincrSurfaceType       = "PROPERTY";        ///< Type of double incremental surface
   mDincrGiven             = "false";

   // @Denis: This is a feature in development, which is not working as intended and, therefore, by default disabled at the moment
   mIcModesScreenThr       = {-I_1, -I_1, -I_1};   ///< Screening threshold for inter-connecting modes  
}

/**
 * Check input to the pes-module
 **/
void PesCalcDef::InputAfterProcessing
   (
   )
{
   // Only one surface generation strategy can be used at one time
   auto check = [](bool key, bool& found)
   {
      if (key)
      {
         if (found) 
         {
            MIDASERROR("Only one of the keywords #2 TAYLORINFO, #2 STATICINFO and #2 ADGAINFO can be set at the same time, check your input please");
         }
         else
         {
            found = true;
         }
      }
   };
   
   bool found = false;
   check(mPesTaylor, found);
   check(mPesStatic, found);
   check(mPesAdga,   found);

   if (!found && !mDincrGiven)
   {
      MIDASERROR("You try to generate a surface but have not specified a method in which to do so, please correct yourself");
   }

   // Symmetry and grids should be set inactive for the use of Taylor expansions
   if (mPesTaylor)
   {
      if (mPesGrid)
      {
         MIDASERROR("The Taylor expansion strategy for surface generation is not compatible with grid-based methods, check you input please");
      }
      
      if (mUseSym)
      {
         MIDASERROR("The Taylor expansion strategy for surface generation is not compatible with using symmetry, check you input please");
      }
      
      if (mUseExtMcr)
      {
         MIDASERROR("Extended mode combination range can only be used with grid based surface generation strategies");
      }
   }
   
   //
   if (mPesStatic)
   {
      // Check that all n-mode grids are correctly defined
      if (mUseExtMcr)
      {
         if (mPesGridInMCLevel.size() != mPesNumMCR + mExtMcrModes.size())
         {
            MIDASERROR("Input error in number of grid points in each dimension under the #2 STATICINFO keyword");
         }
      
         if (mPesFracInMCLevel.size() != mPesNumMCR + mExtMcrModes.size())
         {
            MIDASERROR("Input error in fractional factors under the #2 STATICINFO keyword");
         }
      }
      else
      {
         if (mPesGridInMCLevel.size() != mPesNumMCR)
         {
            MIDASERROR("Input error in number of grid points in each dimension under the #2 STATICINFO keyword");
         }
      
         if (mPesFracInMCLevel.size() != mPesNumMCR)
         {
            MIDASERROR("Input error in fractional factors under the #2 STATICINFO keyword");
         }
      }

      for (In i = I_0; i < mPesGridInMCLevel.size(); ++i)
      {
         if (mPesGridInMCLevel[i] % I_2 != I_0)
         {
            MidasWarning("Found an odd number of grid point in the #2 STATICINFO definition! Note that this number will be subjected to a round-down to nearest even number");
         }
      }
   }
   
   // Make sure that the special Adga related input are sensible
   if (mPesAdga)
   {
      // Define the vibrational density analysis
      if (mAnalyzeDensMethod == "MEAN")
      {
         if (gPesIoLevel > I_4)
         {
            Mout << " The Adga will use the mean vibrational density in determining convergence " << std::endl;
         }
         
         gPesCalcDef.SetmPesUseMeanDens(true);
      }
      else if (mAnalyzeDensMethod == "MAX")
      {
         if (gPesIoLevel > I_4)
         {
            Mout << " The Adga will use the maximum vibrational density in determining convergence " << std::endl;
         }
         
         gPesCalcDef.SetmPesUseMaxDens(true);
      }
      else
      {
         MIDASERROR("The choice of vibrational density analysis method is not recognized");
      }

      // Check if multilevel info makes sense
      if (!mPesBoundariesPreopt && this->MultiLevel() > I_2)
      {
         Mout << " Please note that if different electronic structure methods are used for different multilevel ADGA calculations, then the potential boundaries might end up being different. In order to avoid this problem it is recommended to apply the #2 BoundariesPreOpt keyword with the last indicated electronic structure method under the #2 MutiLevelPes keyword. " << std::endl; 
      }
     
      // The additional dynamic features available to the Adga do not work if new points cannot be calculated continuously
      if (  gPesCalcDef.GetmPesCalcPotFromOpFile() 
         && (  gPesCalcDef.GetmCalcDensAtEachMcl() 
            || gPesCalcDef.GetmDoAdgaExtAtEachMcl()
            )
         )
      {
         MIDASERROR("The #2 PotValsFromOpFile keyword cannot be specified at the same time as the #2 DoAdgaExtAtEachMcl and/or #2 UpdMensDensAtEachMcl keywords!");
      }

      if (mPscInPes)
      {
         if (mPscMaxGridBounds.size() != I_3)
         {
            MIDASERROR("There need to be three numbers specified under the #2 PSCMAXGRIDBOUNDS keyword");
         }
      }

      // Check that the multipliers for the relative Adga convergence criterion are sensible
      if (mAdgaRelConvScalFacts.size() == I_1)
      {
         auto scale_factor = mAdgaRelConvScalFacts[I_0];
         for (In i = I_0; i < mPesNumMCR; ++i)
         {
            mAdgaRelConvScalFacts.push_back(scale_factor);
         }
      }
      else if (mAdgaRelConvScalFacts.size() < mPesNumMCR)
      {
         MIDASERROR("Multipliers for the relative ADGA convergence criterion are not set correct, please ensure that the input given under the #2 ADGARELSCALFACT keyword matches the target mode combination level");
      }
      
      // Check that the multipliers for the absolute Adga convergence criterion are sensible
      if (mAdgaAbsConvScalFacts.size() == I_1)
      {
         auto abs_scale_factor = mAdgaAbsConvScalFacts[I_0];
         for (In i = I_0; i < mPesNumMCR; ++i)
         {
            mAdgaAbsConvScalFacts.push_back(abs_scale_factor);
         }
      }
      else if (mAdgaAbsConvScalFacts.size() < mPesNumMCR)
      {
         MIDASERROR("Multipliers for the absolute ADGA convergence criterion are not set correct, please ensure that the input given under the #2 ADGAABSSCALFACT keyword matches the target mode combination level");
      }
 
      // Additional check need to be done for the use of multistate Adga
      if (mUseMultiStateAdga)
      {
         // Make sure that the weights vector for the ADGA properties sums to one
         double adga_weight_sum = 0.0;

         for (auto& weight : mPesAdaptivePropsWeights)
         {
            adga_weight_sum += weight;
         }  

         for (auto& weight : mPesAdaptivePropsWeights)
         {
            weight /= adga_weight_sum;
         }
   
         // Check if the weights for the properties used in the ADGA is empty, and if not if it has the same amount of elements as the number of properties optimized in ADGA
         if(   !(mPesAdaptivePropsWeights.empty() 
            || mPesAdaptiveProps.size() == mPesAdaptivePropsWeights.size())
            )
         {
            MIDASERROR(" #2ADGAPROPWEIGHTS does not have the same amount of elements as #2ADGAPROPS, check your input file ");
         }
      }
   }
  
   // Check common settings for both static and Adga surface generation methods
   if (mPesGrid)
   {
      // Check that the multilevel input is correct
      if (this->MultiLevel())
      {
         Mout << std::endl;
         if (mLevelMc.size() != this->MultiLevel())
         {
            Mout << " *Wrong* dimension of gPesLevelMc in NumDer " <<  mLevelMc.size() << " != " << this->MultiLevel() << endl;
            Mout << "  It should be equal to the number of level requested in the multilevel procedure." << endl;
            MIDASERROR (" Wrong dimension of gPesLevelMc in NumDer ");
         }

         if (gPesIoLevel > I_9)
         {
            Mout << " Maximum level of mode coupling is set to " << mPesNumMCR << std::endl;
            Mout << " Total number of surfaces to be constructed is " << mLevelMc.size() << std::endl;
         }

         if (mLevelMc[mLevelMc.size() - I_1] != mPesNumMCR )
         {
            Mout << " *Wrong*, the MC for the last level should be equal to " << mPesNumMCR << std::endl;
            MIDASERROR (" The MC for the last level should be equal to gPesNumMCR.");
         }
         Mout << std::endl;
      }
   
      // Check input for fine grid (FG) mesh specifications  
      if (mPesFGMesh.size() == I_1)
      {
         if (mPesFGMesh[I_0] == I_0)
         {
            MidasWarning("The fine grid will be only one point");
         }
         
         In no_fg_points = mPesFGMesh[I_0];
         for (In j = I_1; j < mPesNumMCR; j++)
         {
            PushBackmPesFGMesh(no_fg_points);
         }
      }
      else if (mPesFGMesh.size() == I_0)
      {
         In no_fg_points = I_2;
         if (mPesAdga)
         {
            no_fg_points = I_1;
         }
         for (In j = I_0; j < mPesNumMCR; j++)
         {
            PushBackmPesFGMesh(no_fg_points);
         }
      }
      if (mPesFGMesh.size() < mPesNumMCR && mPesFGMesh.size() != I_0)
      {
         MIDASERROR("The fine grid mesh specification does not cover all of the chosen dimensions, please correct under the #2 PesFGMesh keyword!");
      }
      
      // Check if interpolation is setup correct
      if (!mIntType.empty())
      {
         // Check that the surface or surfaces to be interpolated is indeicated correctly and then set the appropriate bools for further reference
         if (mIntSurface == "SURFACE")
         {
            mPesDoInt = true;
            if (mIntType == "SHEPARD")
            {
               mPesShepardInt = true;
            }
            else
            {
               mPesSplineInt = true;
            }
         }
         else if (mIntSurface == "EXTRAPOLATED")
         {
            mPesDoIntForExtrap = true;
            if (mIntType == "SHEPARD")
            {
               mPesShepardIntForExtrap = true;
            }
            else
            {
               mPesSplineIntForExtrap =true;
            }
            
            // Check that extrapolation is requested
            if (!mPesDoExtrap)
            {
               MIDASERROR("Interpolation for extrapolated surface is requested but the #2 EXTRAPOLATE keyword is not given");
            }
         }
         else if (mIntSurface == "ALL")
         {
            mPesDoInt = true;
            mPesDoIntForExtrap = true;
            if (mIntType == "SHEPARD")
            {
               mPesShepardInt = true;
               mPesShepardIntForExtrap = true;
            }
            else
            {
               mPesSplineInt = true;
               mPesSplineIntForExtrap =true;
            }

            // Check that extrapolation is requested
            if (!mPesDoExtrap)
            {
               MIDASERROR("Interpolation for extrapolated surface is requested but the #2 EXTRAPOLATE keyword is not given");
            }
         }
         else
         {
            MIDASERROR("Surface(s) to be interpolated is not specified correctly");
         }

         // If standard spline interpolation is chosen, check that the type of spline is given correct
         if (mIntType == "SPLINE")
         {
            if (  (SPLINEINTTYPE_MAP::ENUM_TO_STRING.find(mSplineType)->second != "NATURAL")
               && (SPLINEINTTYPE_MAP::ENUM_TO_STRING.find(mSplineType)->second != "GENERAL")
               )
            {
               MIDASERROR("The chosen type of spline is not recognize");
            }
         }
      }

      // Check if extrapolation is setup correct
      if (mPesDoExtrap)
      {
         if (mExtrapolateTo == I_1)
         {
            MIDASERROR("Cannot use extrapolate for non-coupled surface, please rethink your input to the #2 EXTRAPOLATE keyword");
         }
   
         if (mPesCalcPotFromOpFile)
         {
            MIDASERROR("#2POTVALSFROMOPFILE keyword cannot be used with extrapolation, check your input file ");
         }
         
         if (mUseExtrapStartGuess)
         {
            if (gPesIoLevel > I_4)
            {
               Mout << " The extrapolated surface will be used as an initial guess to the " << mExtrapolateTo << "M surface " << std::endl;
            }
         }
      }
   
      // Check that information related to the double incremental scheme make sense
      if (mDincrSurface)
      {
         if (mDincrMethod != "DIF" && mDincrMethod != "DIFACT")
         {
            MIDASERROR("The double incremental scheme indicated under the #2 DOUBLEINCREMENTAL keyword is unknown, got: " + mDincrMethod);
         }

         if (mDincrSurfaceType != "PROPERTY" && mDincrSurfaceType != "HESSIAN" && mDincrSurfaceType != "GIVEN" )
         {
            MIDASERROR("Unknown double incremental surface type indicated under the #2 DOUBLEINCREMENTAL keyword");
         }

         // Make sure that convergence criterion thresholds are defined for all mode combination levels
         if (mIcModesScreenThr.size() < mPesNumMCR)
         {
            MIDASERROR("Did not detect thresholds for all mode combination levels, please make sure that these are given correctly under the #2 ICMODESSCREENTHR keyword");
         }

         if (mPesAdga && mDincrMethod == "DIFACT")
         {
            MIDASERROR("Running the ADGA with the DIFACT scheme is not well-defined, modify your input please!");
         }
      }
      
      if (mPesPlotSelected)
      {
         //Then parse the information in GetmPesPlotSelected()Info to a convenient map
         //Note that only ALLPOT or ONLYFULLPOT are allowed Infoes
         for (auto ci = mPesPlotSelectedInfo.begin(); ci != mPesPlotSelectedInfo.end(); ci++)
         {
            std::string::size_type loc = (*ci).find("ALLPOT");
            std::string::size_type loc1 = (*ci).find("ONLYFULLPOT");
            if (loc != string::npos || loc1 != string::npos)
            {
               std::string::size_type loc2;
               loc2 = loc;
               if (loc1 != std::string::npos) 
               {
                  loc2 = loc1;
               }

               std::vector<In> loc_mode;
               std::string s = (*ci).substr(0, loc2);
               std::istringstream input_s(s);
               std::string s_new;
               while (input_s >> s_new)
               {
                  In mode = InFromString(s_new);
                  loc_mode.push_back(mode);
               }

               std::string info;
               info = "ALLPOT";
               if (loc1 != std::string::npos) 
               {
                  info = "ONLYFULLPOT";
               }
               InsertIntomPesPlotSelectedMap(loc_mode, info);
            }
            else
            {
               MIDASERROR(" Info on GetmPesPlotSelected()Info not of the required type ");
            }
         }
      }
   }

   // If no analysis directory is given we do not plot pes cuts
   if (gAnalysisDir == "")
   {
      Mout << " Analysis directory not provided, plot files will not be saved " << std::endl;
      SetmPesPlotAll(false);
      SetmPesPlotRealESP(false);
      SetmPesPlotSelected(false);
   }
}

/**
 * Validate pes input in relation to number of vibrational modes
 * 
 * @param aNoNuclei     The number of nuclei in the molecule
 * @param aNoVibModes   The number of vibrational modes in the molecule
 **/
void PesCalcDef::ValidateInput
   (  const In& aNoNuclei
   ,  const In& aNoVibModes
   )
{
   // Make sure that the special Adga related input are sensible
   if (mPesAdga && !mDincrSurface)
   {
      // Check that the number of states/modes included in the vibrational density analysis is correct
      if (mAnalyzeStates.size() != aNoVibModes)
      {
         MIDASERROR("The number of elements, as given under the #2 ANALYZESTATES keyword, should be equal to the number of modes in the molecule");
      }
   }
      
   // Check that the number of nuclei is correct when using the special diatomic keyword
   if (mPesDiatomic)
   {
      Mout << " You have specificly requested a diatomic molecule. I assume You have oriented the molecule along the Z-axis! " << std::endl;
  
      if (aNoNuclei != I_2)
      {
         MIDASERROR("You specificly requested a surface calculation for a diatomic molecule, but the number of nuclei is not equal to 2!");   
      }
   }
   
   //
   if (mPesGrid)
   {
      if (  mPesFGScalFact.size() != aNoVibModes 
         && mPesFGScalFact.size() != I_1 
         && mPesFGScalFact.size() != I_0
         )
      {
         Mout << " *Wrong* dimension of gPesFGScalFact in NumDer " << endl;
         Mout << "  It should be either a number or of the size of " << aNoVibModes << std::endl;
         MIDASERROR ("Wrong dimension of gPesFGScalFact in NumDer ");
      }
      else if (   mPesFGScalFact.size() == I_0 
              &&  !mDincrSurface
              )
      {
         for (In j = I_1; j < aNoVibModes; j++)
         {
            mPesFGScalFact.push_back(mPesFGScalFact[I_0]);
         }
      }
      else if (   mPesFGScalFact.size() != aNoVibModes 
               && mPesFGScalFact.size() == I_1
               && !mDincrSurface
               )
      {
         Nb default_value = mPesFGScalFact[I_0];
         for (In j = I_1; j < aNoVibModes; j++)
         {
            mPesFGScalFact.push_back(default_value);
         }
      }

      // Validate that the fit-basis input is correct
      gFitBasisCalcDef.ValidateFitBasisInput(aNoVibModes, mDincrSurface, mPesNumMCR, mPesFitBasisName, mPesCalcMuTens);
   }
}

/**
 * Print Info about Grid or Derivatives. 
 * We can do displacements along normal modes or directly in the Cartesian basis.
 * First we see if we are going to do derivative force fields or not.
 **/
void PesCalcDef::PrintCalcDefInfo
   (
   ) const
{
   if (mPesModelPot)
   {
      Mout << " The calculation will use ";
      if (mPesDoMorsePot)
      {
         Mout << " a Morse potential model " << std::endl;
      }
      else if (mPesDoDoubleWpot)
      {
         Mout << " a double well potential model from J. B. Coon et al. J. Mol. Spectrosc. 20(1966), 107 " << std::endl;
      }
      else if (mPesDoPartridgePot)
      {
         Mout << " the Partridge-Schwenke potential for water " << std::endl;
      }
      else if (mPesDoAmmModelPot)
      {
         Mout << " the model potential for ammonia from JCP 123(2005), 134308  with pes type ";
         std::string pot_lab="";
         if (mPesType == I_0)
         {
            pot_lab = "CBS**-5";
         }
         else if (mPesType == I_1)
         {
            pot_lab = "CBS**-4";
         }
         Mout << pot_lab << std::endl;
      }
      else if (mPesDoTestPot)
      {
         Mout << " a super simple test potential not giving a sensible result!! " << std::endl;
      }
      else if (mPesDoArNtwoModelPot)
      {
         Mout << " a model potential for the (Ar-N2)+ system, with Ipes:  " << mPesType << std::endl;
      }
      else
      {
         MIDASERROR (" Model potential not implemented yet ");
      }
   }

   if (mUseSym && gPesIoLevel > I_4)
   {
      Mout << " Symmetry will be used. The molecule belongs to the Abelian point group: " << mPesSymLab << std::endl;
   }
  
   if (mPesGrid)
   {
      if (gPesIoLevel > I_1)
      {
         Mout << std::endl << " Will do a grid-based potential energy and/or molecular property surface " << std::endl;
      }
      
      if (mCartesiancoordInPes)
      {
         MIDASERROR(" I can only do grid setting based on normal coordinates ");
      }

      if (gPesIoLevel > I_10 && !mPesAdga)
      {
         Mout << " The grid is chosen on the basis of the harmonic oscillator quantum number " << mPesGridMaxDim << std::endl << std::endl;
      }

      // check which variable transformation should be applied to the generated linear grid
      //Adga pes
      if (mPesAdga)
      {
         if (gPesIoLevel > I_1)
         {
            Mout << " The grids will be generated by the Adga " << std::endl;
         }

         if (gPesIoLevel > I_10)
         {
            Mout << " The intially spanned space is set with bounds corresponding to the turning points of a harmonic oscillator with quantum number " << mPesGridInitialDim << std::endl << std::endl;
         }

         if (gPesIoLevel > I_9)
         {
            Mout << " The maximum number of Adga iterations is set to " << mPesIterMax << std::endl;
         }
         
         // Check that only a single set of relative and absolute convergence criteria are activated at the same time 
         if (
            (gPesCalcDef.GetmPesUseItVdensThr() && gPesCalcDef.GetmPesUseItNormVdensThr()) 
            || 
            (!gPesCalcDef.GetmPesUseItVdensThr() && !gPesCalcDef.GetmPesUseItNormVdensThr())
            )
         {
            MIDASERROR("Keywords #2 ItVdensThr and #2 ItResEnThr or #2 ItNormVdensThr and #2 ItNormResEnThr can not be specified at the same time but one pair must be present, choose accordingly!");
         }
      }
      
      // Print informations on the fine grid, interpolation and fitting procedures
      if (gPesIoLevel > I_8)
      {
         Mout << " The fine grid will be defined for each mode combination level by multiplying the coarse grid points by a factor of " << mPesFGMesh << std::endl;   
      }

      if (mPesFGScalFact.size() > I_1)
      {
         if (gPesIoLevel > I_8)
         {
            Mout << " Fine grid scaling factor for each dimension is:  " << std::endl;
            for (In i = I_0; i < mPesFGScalFact.size(); i++)
            {
               Mout << "  Mode: "   << i + I_1 << " scaling factor: " << mPesFGScalFact[i] << std::endl;
            }
         }
      }
      else if (mPesFGScalFact.size() == I_1)
      {
         if (gPesIoLevel > I_8)
         {
            Mout << " Scaling Factors in each dimensions will all be set to the value of  " << mPesFGScalFact[I_0] << std::endl;
         }
      }
      else if (mPesFGScalFact.size() == I_0 && !mPesAdga)
      {
         Mout << " Scaling Factors in each dimensions will all be set to a default value of 0.9  " << std::endl;
      }

      if (gPesIoLevel > I_5)
      {
         Mout << std::endl;
      }

      if (mPesDoInt)
      {
         if (mPesDoExtrap && !mPesDoIntForExtrap)
         {
            if (gPesIoLevel > I_5)
            {
               Mout << " Interpolation of the extrapolated points will not be performed " << std::endl;
            }
         }

         if (mPesSplineInt)
         {
            if (gPesIoLevel > I_5)
            {
               Mout << " The interpolation of the ab-initio points will use "
                    << SPLINEINTTYPE_MAP::ENUM_TO_STRING.find(mSplineType)->second
                    << " spline functions " << std::endl;
            }
         }

         if (mPesDoExtrap && mPesSplineIntForExtrap)
         {
            if (gPesIoLevel > I_5)
            {
               Mout << " The interpolation of the extrapolated points will use "
                    << SPLINEINTTYPE_MAP::ENUM_TO_STRING.find(mSplineType)->second
                    << " spline functions " << std::endl;
            }
         }

         if (mPesShepardInt)
         {
            if (gPesIoLevel > I_5)
            {
               Mout << " Will perform Modified Shepard interpolation of order " << mPesShepardOrd 
                    << " with exponent " << mShepardExp
                    << " for the ab-initio points " 
                    << std::endl;
            }
         }

         if (mPesDoExtrap && mPesShepardIntForExtrap)
         {
            if (gPesIoLevel > I_5)
            {
               Mout << " Will perform Modified Shepard interpolation of order " << mPesShepardOrd 
                    << " with exponent " << mShepardExp
                    << " for the extrapolated points " 
                    << std::endl;
            }
         }
      }
      else
      {
         if (gPesIoLevel > I_5)
         {
            Mout << " Interpolation of the ab-initio points will not be performed " << std::endl;
         }

         if (mPesDoExtrap && !mPesDoIntForExtrap)
         {
            if (gPesIoLevel > I_5)
            {
               Mout << " Interpolation of the extrapolated points will not be performed " << std::endl;
            }
         }
      }

      if (mPesFreqScalCoordInFit && mPesScalCoordInFit)
      {
         MIDASERROR("Keywords #2 FREQSCALCOORDINFIT and #2 SCALCOORDINFIT are both specified but only one can be used at the same time, choose accordingly!");
      }

      if (mPesFreqScalCoordInFit)
      {
         if (!mPesGrid)
         {
            MIDASERROR(" Scaling Frequencies are used only for grid procedure ");
         }

         if (gPesIoLevel > I_5)
         {
            Mout << " The fitting routine will use frequency scaled coordinates " << std::endl;
         }
      }
      else if (mPesScalCoordInFit)
      {
         if (gPesIoLevel > I_5)
         {
            Mout << " The fitting routine will use scaled coordinates " << std::endl;
         }
      }

      if (mPesPlotAll || mPesPlotRealESP)
      {
         Mout << " Directory for plot files: " << gAnalysisDir << std::endl;
         if (gAnalysisDir == "")
         {
            Mout << " Analysis directory not provided, plot files will not be saved " << endl;
         }
      }
      if (mPesPlotSelected)
      {
         if (gPesIoLevel > I_5)
         {
            Mout << std::endl << " Info for surface plotting: " << std::endl;

            for (auto ci = mPesPlotSelectedMap.begin(); ci != mPesPlotSelectedMap.end(); ++ci)
            {
               Mout << "  " << (*ci).second << " for mode combination(s): " << (*ci).first << std::endl;
            }
            Mout << " Directory for plot files: " << std::endl;
            Mout << "  " << gAnalysisDir << std::endl;
         }
      }
   }
   else
   {
      if (gPesIoLevel > I_1)
      {
         Mout << std::endl << " Construct a (" << mPesNumMCR << "M" << mPesNumMaxPolOrExp << "T) force field based on numerical derivatives " << std::endl << std::endl;

         Mout << " Order of analytical derivative:    " << mPesNumAnalyticOrder << std::endl;
         Mout << " Maximum order of mode coupling:    " << mPesNumMCR << std::endl;
         Mout << " Maximum order in Taylor expansion: " << mPesNumMaxPolOrExp << std::endl;
         Mout << std::endl;
      }

      if (mPesAdga) MIDASERROR(" Adga PES generation procedure doesn't work for numerical differences ");
//    We do a little tjecking in case of Cartesian displacements and calculation of Gradient or Hessian
      if (mPesDoExtrap) MIDASERROR(" Extrapolation procedure do not work with numerical differences ");

      if ( mCartesiancoordInPes && (mPesDispType != "SIMPLEDISPLACEMENT") )
      {
         Mout << " I can only do SIMPLEDISPLACEMENT in connection with Cartesian displacements " << endl;
         MIDASERROR(" MIDASERROR in PES input section (Displacement)");
      }
      if ( mCartesiancoordInPes && (mList_Of_Hessians.size() > I_0) ) // Cartesian Hessians required
      {
         if ( (mPesNumMCR < I_2) || (mPesNumMaxPolOrExp < I_2) )
         {
            Mout << " I cannot do the full Cartesian Hessian with the given TAYLORINFO " << endl;
            MIDASERROR("");
         }
      }
      if ( mPesDiatomic && (mPesDispType != "SIMPLEDISPLACEMENT") )
      {
         Mout << " I can only do SIMPLEDISPLACEMENT in connection with the Diatomic input option " << endl;
         MIDASERROR(" MIDASERROR in PES input section (Diatomic)");
      }
   }
   
   if (mPesCalcMuTens)
   {
      if (mCartesiancoordInPes)
      {
         MIDASERROR(" PesCalcMuTens option is not compatible with CartesiancoordInPes option ");
      }

      Mout << std::endl << " Effective inertia inverse tensor will be added to the list of properties " << std::endl;
      Mout << " For a non-linear molecule Watson potential energy term will be added to the energy for the state no: " << GetmPesExcState() << std::endl;
   }
}

void PesCalcDef::SetmDincrSurfaceType(const std::string& aStr) 
{
   mDincrSurfaceType = aStr;
   if (mDincrSurfaceType == "GIVEN")
   {
      mDincrGiven = true; 
   }
}

/**
 * handle readin of exteneded grid settings
 * @param Minp midas input file
 * @param s    working readin string (Input/output)
 * @return indicate whether we have read in next keyword to be processed (false)
 **/
bool PesCalcDef::ReadExtendedGridSettings(std::istream& Minp, std::string& s)
{
   while(midas::input::GetLine(Minp,s) && midas::input::NotKeyword(s))
   {
      std::vector<std::string> local_vector = midas::util::StringVectorFromString(s,' ');
      if(!(local_vector.size() > 0)) MIDASERROR("No data was read"); // assert vi have read some elements
      In mc_level = midas::util::FromString<In>(local_vector[0]); // read mc level
      if (local_vector.size() != 4*mc_level+I_1) // assert we have read the correct number of elements
      {
         MIDASERROR(" Too less/much information in Extended Grid Settings line: '" + s + "'\n"
                  + " Give Dim MC(Dim) GridPoints(Dim) LeftFractioning(Dim) RigthFractioning(Dim)");
      }
      std::vector<In> t_mode;
      std::vector<In> t_grid;
      std::vector<string> t_frac;
      // read in modes
      for (In j = I_1; j <= mc_level; ++j) 
      {
         t_mode.emplace_back(midas::util::FromString<In>(local_vector[j]));
      }
      // read in number of grid points
      for (In j = I_1; j <= mc_level; ++j) 
      {
         t_grid.emplace_back(midas::util::FromString<In>(local_vector[j+mc_level]));
      }
      // read in fractioning
      for (In j = I_1; j <= 2*mc_level; ++j)
      {
         t_frac.emplace_back(local_vector[j+2*mc_level]);
      }

      // add extended grid setting
      mExtendedGridSettings.emplace_back(t_mode, t_grid, t_frac);
   }
   midas::input::AssertSpecificKeyword(s, "#2ENDOFEXTENDEDGRIDSETTINGS");

   return false;
}

/**
 * handle readin of multilevel pes keyword
 * @param Minp midas input file
 * @param s    working readin string (Input/output)
 * @return indicate whether we have read in next keyword to be processed (true)
 **/
bool PesCalcDef::ReadMultiLevelPes(std::istream& Minp, std::string& s)
{
   // read singlepoint names
   while(midas::input::GetLine(Minp,s) && !midas::input::IsKeyword(s))
   {
      auto str_vec = midas::util::StringVectorFromString(s,' ');
      MidasAssert(str_vec.size() == 2, "Input for line: '" + s + "' is wrong. Must give singlepoint name and MC level.");
      this->AddSinglePoint(str_vec[0]); // set singlepoint
      mLevelMc.emplace_back(midas::util::FromString<In>(str_vec[1])); // set mc level
   }
   return true; // we have read next keyword
}

/**
 * handle readin of boundaries pre opt level and set preopt flag to true
 * @param Minp midas input file
 * @param s    working readin string (Input/output)
 * @return indicate whether we have read in next keyword to be processed (false)
 **/
bool PesCalcDef::ReadBoundariesPreOpt(std::istream& Minp, std::string& s)
{
   // read pre opt singlepoint name
   midas::input::GetLine(Minp, s);
   midas::input::AssertNotKeyword(s);
   mPesBoundariesPreopt = true; // set pre opt flag to true
   mSinglePointNames[0] = s; // level 0 is reserved for preopt
   mLevelMc[0] = 1;
   return false; // we have not read next keyword
}

/**
 * set historic adga pes to true
 * @param Minp midas input file
 * @param s    working readin string (Input/output)
 * @return indicate whether we have read in next keyword to be processed (false)
 **/
bool PesCalcDef::ReadHistoricAdgaPes(std::istream& Minp, std::string& s)
{
   mHistoricAdgaScaling = true;
   mHistoricAdgaConv = true;
   mHistoricAdgaGrid = true;
   return false;
}

/**
 * 
 * @param Minp midas input file
 * @param s    working readin string (Input/output)
 * @return indicate whether we have read in next keyword to be processed (false)
 **/
bool PesCalcDef::ReadHistoricAdgaScaling(std::istream& Minp, std::string& s)
{
   mHistoricAdgaScaling = true;
   return false;
}

/**
 * 
 * @param Minp midas input file
 * @param s    working readin string (Input/output)
 * @return indicate whether we have read in next keyword to be processed (false)
 **/
bool PesCalcDef::ReadHistoricAdgaConv(std::istream& Minp, std::string& s)
{
   mHistoricAdgaConv = true;
   return false;
}

/**
 * 
 * @param Minp midas input file
 * @param s    working readin string (Input/output)
 * @return indicate whether we have read in next keyword to be processed (false)
 **/
bool PesCalcDef::ReadHistoricAdgaGrid(std::istream& Minp, std::string& s)
{
   mHistoricAdgaGrid = true;
   return false;
}

/**
 * set historic pes num source to true
 * @param Minp midas input file
 * @param s    working readin string (Input/output)
 * @return indicate whether we have read in next keyword to be processed (false)
 **/
bool PesCalcDef::ReadHistoricPesNumSource(std::istream& Minp, std::string& s)
{
   mHistoricPesNumSource = true;
   return false;
}

/**
 * 
 * @param Minp midas input file
 * @param s    working readin string (Input/output)
 * @return indicate whether we have read in next keyword to be processed (true/false)
 **/
bool PesCalcDef::ReadScreenOperCoef(std::istream& Minp, std::string& s)
{
   midas::input::GetLine(Minp, s);
   if(s == "ON") mScreenOperCoef = true;
   else if(s == "OFF") mScreenOperCoef = false;
   else MIDASERROR("MUST READ EITHER 'ON' OR 'OFF', GOT: '" + s + "'");
   midas::input::GetLine(Minp, s);
   if(midas::input::IsKeyword(s)) return true; // next line is keyword
   else // next line is new threshold
   {  
      mScreenOperCoefThresh = midas::util::FromString<Nb>(s);
      return false;
   }
}

/**
 *
 **/
bool PesCalcDef::ReadSpScratchDirPrefix(std::istream& Minp, std::string& s)
{
   midas::input::GetLine(Minp, s);
   midas::input::AssertNotKeyword(s);
   mSpScratchDirPrefix = s;
   return false;
}

/***************************************************************************//**
 * This is needed e.g. for determining whether it is requested to calculate
 * quantites that can be evaluating utilizing sum-over-product structure.
 *
 * This is the case by default, unless _explicitly_ requested in input, that
 * the historic method should be used.
 * Even if basing the ADGA converge on the "norm-like" quantities
 * (GetmPesUseItNormVdensThr()), (some of) the integrals over density,
 * potential, density*potential stille need to be calculated. ... As far as I
 * can tell, seeing that some tests fail otherwise. Feel free to change this if
 * needed; I just couldn't be bothered looking more into it. MBH, May 2018.
 *
 * @param[in] arPesCalcDef
 *    The object of interest.
 * @return
 *    Whether SOP-calculated quantities are requested for determining ADGA conv.
 ******************************************************************************/
bool AdgaConvergenceFromSumOverProductQuantities
   (  const PesCalcDef& arPesCalcDef
   )
{
   return !arPesCalcDef.GetAdgaUseHistoricNonSopIntegrals();
}

/***************************************************************************//**
 * Whether it is necessary to calculate any integrals not utilizing
 * SOP-structure of potential. This is the case if determining ADGA convergence
 * from any "norm-like" quantities (such as something like density*(potential_i
 * - potential_i-1)^2) _or_ if explicitly requested in input to use old method.
 *
 * @param[in] arPesCalcDef
 *    The object of interest.
 * @return
 *    Whether any non-SOP quantities are requested in ADGA.
 ******************************************************************************/
bool AdgaConvergenceFromNonSumOverProductQuantities
   (  const PesCalcDef& arPesCalcDef
   )
{
   return      arPesCalcDef.GetmPesUseItNormVdensThr()
            || arPesCalcDef.GetAdgaUseHistoricNonSopIntegrals();
}
