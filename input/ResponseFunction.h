#ifndef RESPONSEFUNCTION_H_INCLUDED
#define RESPONSEFUNCTION_H_INCLUDED

#include <vector>
#include <string>
#include <iostream>
#include <assert.h>
#include <set>

#include "inc_gen/TypeDefs.h"

std::vector<std::string> ParseResponseFunctionString(std::string str);

class ResponseFunction
{

   private:
      std::vector<std::string> mOperators;
   
   public:
      ResponseFunction(): mOperators()
      {
      }

      ResponseFunction(const std::string& str): mOperators(ParseResponseFunctionString(str))
      {
      }

      ResponseFunction(const std::vector<std::string>& opers): mOperators(opers)
      {
      }

      ResponseFunction(std::vector<std::string>&& opers): mOperators(std::move(opers))
      {
      }

      const std::vector<std::string>& Operators() const
      {
         return mOperators;
      }
      
      std::vector<std::string>& Operators()
      {
         return mOperators;
      }

      bool IsCompatible(const ResponseFunction& other_rsp) const
      {
         return (Operators() == other_rsp.Operators());
      }

      std::string Type() const;
      std::string TypeForFile() const;

      bool HasOperator(const std::string& oper) const;
      bool HasOperator(const std::vector<std::string>& opers) const;

      //In Order() const
      //{
      //   return mOperators.size();
      //}
      
      friend std::ostream& operator<<(std::ostream&, const ResponseFunction&);
};

/**
 * find unique operators in a set of response functions
 **/
std::set<std::string> UniqueOperators(const std::vector<ResponseFunction>& rsp_funcs);

ResponseFunction CreateResponseFunctionFromString(const std::string& str);

std::ostream& operator<<(std::ostream& os, const ResponseFunction& rsp);

#endif /* RESPONSEFUNCTION_H_INCLUDED */
