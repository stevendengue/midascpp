/**
************************************************************************
* 
* @file                VscfCalcDef.cc
*
* Created:             04-11-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Contains input-info that defines a calculation
* 
* Last modified:       29-06-2014 (Carolin Koenig)
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "input/VscfCalcDef.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/Trim.h"
#include "util/Io.h"
#include "mpi/Impi.h"

typedef string::size_type Sst;

/**
 * Construct a default definition of a  calculation
**/
VscfCalcDef::VscfCalcDef() 
   : RspCalcDef()
   , mOneStateMatrix(I_1,I_1)
   , mOccupancy(I_1,I_1)
{

   mCalcName                            = "";
   mCalcOper                            = "";
   mCalcBasis                           = "";
   mDiagMeth                            = gNumLinAlg;
   mOccVec.clear();
   mIntStorageLevel                     = I_0;  // 0 means all in memory
   mVecStorageLevel                     = I_1;  // 0 means all in memory, 1 means most important in memory.

   mNiterMax                            = I_100;                   // Default is 20 iterations.
   mEnerThr                             = C_10_2*C_NB_EPSMIN19;   // Default is 100 times epsilon! This is hard!
   mResidThr                            = C_10_2*C_NB_EPSMIN19;  // Default is 100 times epsilon! This is hard!
   mRestart                             = false;
   mSave                                = true; 
   mIoLevel                             = I_0;

   mDone                                = false;
   mEfinal                              = C_0;
   mEfinalStateAve                      = C_0;
   mScreenZero                          = C_0;                  // Screening = 0, i.e. off 
   mScreenZeroCoef                      = C_0;                  // Screening = 0, i.e. off 

   mItEqNiterMax                        = I_40;
   mItEqMicroNmax                       = I_10;
   mItEqResidThr                        = C_I_10_10; 
   mItEqMicroThrFac                     = 2*C_I_10_2; 
   mItEqEnerThr                         = C_NB_MAX; // To max number - turned off as default. 
   mRedDimMax                           = I_100;
   mNbreakDim                           = I_10_5;
   mPreDiag                             = false;
   mNpreDiag                            = I_0;
   mImprovedPrecond                     = false;
   mPrecondExciLevel                    = I_0;
   mLevel2Solver                        = gNumLinAlg;
   mTrueHDiag                           = false;
   mTargetingMethod                     = "BESTHITS";
   mOverlapMin                          = C_0;
   mEnergyDiffMax                       = C_5/C_AUTKAYS;
   mResidThrForOthers                   = C_10_3;
   mEnerThrForOthers                    = C_10_3;
   
   mTimeIt                              = false;
   mSizeIt                              = false;
   mUseAvailable                        = false;
   mMapPrevVecs                         = false;
   mSequenceNr                          = I_0; 

   mStateAveVirtVscf                    = false; 
   mStateAveNrs.clear();
   mFundOnlyStateAveVirtVscf            = false; 
   mFastTempAve                         = false;
   mOneStateVscfAve                     = false;   
   mAllVirtExptValues                   = false;
   mPropertyList.clear();
   mCalcDensities                       = false;
   mPlotDensities                       = false;
   mVscfAnalysisDir                     = "";
   mPlotNmodals                         = I_0; 
   mAdgaDensAnalysis                    = false;
   mAnalizeMaxDensity                   = false;
   mAnalizeMeanDensity                  = false;
   mAnharmGuess                         = false;
   mFranckCondon                        = false;
   mUseThermalDensity                   = false;
   mThermalDensityTemp = C_0;
   mTruncateThermalOccup                = false; 
   mThermalOccupThr                     = C_I_10_5; 
   mVariational= true;  
   mSosRsp                              = false;
   
   mNmodalsSetInput                     = false; 
   mNmodals.clear();
   mIntAnalysis                         = false;
   mIntAnalysisLimit                    = I_10_9;
}

/**
 * Define comparison based upon, Oper, Basis, Energy
**/
void VscfCalcDef::ZeroOcc(const In& arNmodes)
{
   for (In i_op=0;i_op<arNmodes;i_op++) mOccVec.push_back(0);
}

/**
 * Define comparison based upon, Oper, Basis, Energy
**/
bool VscfCalcDefLess
   (  const VscfCalcDef& arDef1
   ,  const  VscfCalcDef& arDef2
   )
{
   string name1 = arDef1.Name();
   string name2 = arDef2.Name();
   Sst i_last1 = name1.find_last_of("_");
   Sst i_last2 = name2.find_last_of("_");
   if (i_last1 != name1.npos) name1.erase(i_last1+1,name1.size()-i_last1-1);
   if (i_last2 != name2.npos) name2.erase(i_last2+1,name2.size()-i_last2-1);
   if (arDef1.Oper() != arDef2.Oper())
   {
      return arDef1.Oper() < arDef2.Oper();
   }
   else if (arDef1.Basis() != arDef2.Basis())
   {
      return arDef1.Basis() < arDef2.Basis();
   }
   else if (name1 != name2 )
   {
      return name1 < name2;
   }
   else 
   {
      return arDef1.GetEfinal() < arDef2.GetEfinal();
   }
} 
void VscfCalcDef::Reserve()
{
   mOccVec.reserve(gReserveNmodes);
}

/**
 * Dump energy and property info fraom calculations, with purpose of
 * doing temperatur calculations
**/
void ExtractPropsAndSaveOnFiles
   (  std::vector<VscfCalcDef*> apVscfCalcDefVec
   ,  std::string aLabel
   ) 
{
   In n_states = apVscfCalcDefVec.size();
   if (n_states > I_0) 
   {
      std::string base = apVscfCalcDefVec[I_0]->GetmVscfAnalysisDir() + "/" + aLabel + "_";

      std::vector<In> calcs;
      In n_0_rsp_funcs = apVscfCalcDefVec[I_0]->NrspFuncs();
      if (apVscfCalcDefVec[I_0]->Done())
      {
         calcs.push_back(I_0);
      }

      In i_skipped = I_0;
      for (In i_calc = I_1; i_calc < n_states; i_calc++)
      {
         if (apVscfCalcDefVec[i_calc]->Done())         // only if converged 
         {
            In n_rsp_funcs = apVscfCalcDefVec[i_calc]->NrspFuncs();
            if (n_rsp_funcs!=n_0_rsp_funcs) 
            {

                Mout << " Concerning state: " << apVscfCalcDefVec[i_calc]->Name()  << endl;
                Mout << " State with a different number of response funcs that the ground state . " << endl;
                Mout << " will not be included in the stat. calc. " << endl;
                i_skipped++;
            }
            else
            {
               // mbh: Not all states should be included
               if((apVscfCalcDefVec[i_calc]->Name()).find(aLabel))
                  calcs.push_back(i_calc);
               else
                  Mout << apVscfCalcDefVec[i_calc]->Name() << " not added as it is not a " 
                       << aLabel << " type calc!" << endl;
            }
         }
         else
         {
             Mout << " Concerning state: " << apVscfCalcDefVec[i_calc]->Name()  << endl;
             Mout << " State not Done and skipped from state calc.; " << endl;
             i_skipped++;
         }
      }
      Mout << "\n\n Nr of states for which info is stored for statistical calculations: " << calcs.size() << endl; 
      Mout << " Nr of states calculated but not included:                            " << i_skipped    << endl; 
      if (i_skipped>I_0) 
      {
         string s2 = " There were states not included in the statistical calculations "; 
         MidasWarning(s2);
      }

      // Niels: We need to stop here, if no calculations are included/converged. Otherwise the following code makes no sense (and will segfault...).
      if (  calcs.empty()
         )
      {
         MidasWarning("No calculations included in statistical calculations!");
         return;
      }

      std::string name = base + "Info_Energy";
      midas::mpi::OFileStream energies(name);
      
      name = base + "Info_Prop";
      midas::mpi::OFileStream prop_info(name);
      
      //mbh
      In i_basis=I_0;
      string fast_oper = apVscfCalcDefVec[calcs[I_0]]->Oper();
      for (In i = I_0; i < gBasis.size(); i++)
      {
         if (gBasis[i].GetmBasName() == fast_oper)
         {
            i_basis = i;
         }
      }
      if (aLabel == "Vscf_fast_ave")
      {
         prop_info << calcs.size() + gBasis[i_basis].GetmNoModesInBas() - I_1 << std::endl;
      }
      else
      {
         prop_info << calcs.size() << std::endl;
      }
      //mbh
      prop_info << n_0_rsp_funcs << endl;
      energies.setf(ios::scientific);
      energies.setf(ios::showpoint);
      energies.setf(ios::uppercase);
      midas::stream::ScopedPrecision(22, energies);
      energies << "# Energies relative to "; 
      for (In i_prop =I_0;i_prop<n_0_rsp_funcs;i_prop++) 
      {
         string name=base+"Info_Property_"+std::to_string(i_prop);
         midas::mpi::OFileStream ms(name);
         ms.setf(ios::showpoint);
         ms.setf(ios::uppercase);
         midas::stream::ScopedPrecision(22, ms);
         ms << "# " 
            << apVscfCalcDefVec[calcs[I_0]]->GetRspFunc(i_prop) << " (zero state value) " << endl;
      }

      In i_mode=I_1;
      for (In i_calc2 =I_0;i_calc2<calcs.size();i_calc2++)
      {
         In i_calc=calcs[i_calc2];
         Nb e = (apVscfCalcDefVec[i_calc]->GetEfinal()-apVscfCalcDefVec[calcs[I_0]]->GetEfinal());
         if (i_calc2==I_0 && aLabel=="Vscf_fast_ave") {
            energies << apVscfCalcDefVec[calcs[I_0]]->GetEfinal() << endl; 
            // mbh: If this is a Fast average calculation, then print the number of modes
            //      and number of HOs pr mode
            energies << gBasis[i_basis].GetmNoModesInBas() << std::endl;
            if (gBasis[i_basis].GetmUseHoBasis()) 
            {
               for(In i = I_0; i < gBasis[i_basis].GetmNoModesInBas(); i++)
               {
                  energies << " " << gBasis[i_basis].GetmHoQnrs()[i];
               }
            }
            energies << std::endl;
            // mbh end
         }
         else if (i_calc2 == I_0 && aLabel != "Vscf_fast_ave") 
         {
            energies << apVscfCalcDefVec[calcs[I_0]]->GetEfinal() << std::endl; 
         }
         energies << " " << right << e << std::endl;
         if (aLabel == "Vscf_fast_ave" && gBasis[i_basis].GetmUseHoBasis()) 
         {
            if (  i_calc2 == i_mode*gBasis[i_basis].GetmHoQnrs()[i_mode - I_1]
               && i_mode!=gBasis[i_basis].GetmNoModesInBas()
               ) 
            {
               energies << C_0 << std::endl;
               i_mode++;
            }
         }

         for (In i_prop =I_0;i_prop<n_0_rsp_funcs;i_prop++) 
         {
            string name=base+"Info_Property_"+std::to_string(i_prop);
            midas::mpi::OFileStream ms(name, midas::mpi::OFileStream::StreamType::MPI_MASTER, std::ios::app);
            ms.setf(ios::scientific);
            ms.setf(ios::showpoint);
            ms.setf(ios::uppercase);
            midas::stream::ScopedPrecision(22, ms);
            ms << " " << right 
               << (apVscfCalcDefVec[calcs[i_calc2]]->GetRspFunc(i_prop)).Value() << endl;
         }
      }
   }
}

/**
 * Define the nr of states for analysis
**/
void VscfCalcDef::SetNrStatesForAnalysis(const std::vector<In>& arInVec) 
{
   In n_modes = arInVec.size();
   mAnalStatesVec.resize(n_modes);
   for (In i_e = I_0; i_e < n_modes; i_e++)
   {
      mAnalStatesVec[i_e] = arInVec[i_e];
   }
   if (gIoLevel > I_10 || gDebug)
   {
      Mout << " vector of states to analyse: " << mAnalStatesVec << std::endl;
   }
   return;
}

/**
 * Read general mode-combi vector
 *
 * @param arInp
 * @param arS
 * @param arGenCombis
 * @return
 *    Already read
 **/
bool VscfCalcDef::ReadGenCombi
   (  std::istream& arInp
   ,  std::string& arS
   ,  std::set<std::map<In, In> >& arGenCombis
   )  const
{
   std::vector<std::string> input_vec;
   std::vector<std::string> exci_vec;

   while (  midas::input::GetLine(arInp, arS)
         && midas::input::NotKeyword(arS)
         )
   {
      input_vec = midas::util::StringVectorFromString(arS);

      // Map for one state
      std::map<In, In> state_map;

      for(const auto& exci : input_vec)
      {
         exci_vec = midas::util::StringVectorFromString(exci, '^');

         if (  exci_vec.size() != 2
            )
         {
            MIDASERROR("Expected input of type '<mode>^<index>', but got: " + exci);
         }

         In mode = midas::util::FromString<In>(exci_vec[0]);
         In index = midas::util::FromString<In>(exci_vec[1]);

         if (  state_map.find(mode) != state_map.end()
            )
         {
            MIDASERROR("Excitation index already set for mode '" + std::to_string(mode) + "'.");
         }
         else
         {
            state_map[mode] = index;
         }
      }

      arGenCombis.emplace(state_map);
   }

   return true;
}

/**
 * The intention is to let the VSCF dump the modals, and to have
 * Vcc::DoMatRepCalc() read them in again. This enables the reuse (between VCC
 * calcs. in the same Midas run) of ModalIntegrals and hence operator
 * matrices (midas::util::matrep::OperMat), which are computationally heavy to
 * compute in the matrep framework.
 * In the current Midas setup (Mar 2020), the Vcc itself always has to run a
 * Vscf calc. prior to the Vcc (due to suboptimal inheritance design). This is
 * usually not a problem, since Vscf is anyway much faster than Vcc, but it
 * _can_ lead to slightly different modals if using OpenMP parallelization.
 * The current solution allows using the exact same modals in each calc. (The
 * Vcc still runs a Vscf calc., but it's not used for anything. Silly, but not
 * a performance issue.)
 *
 * Must be used along with `#3 UseVscf` in the Vcc input block.
 *
 * @note
 *    In current Midas setup (Mar 2020), a single VSCF prefix (i.e. a single
 *    VSCF input block) can lead to multiple VSCF calculations if e.g.
 *    requesting all fundamental excitations, all overtones, etc.
 *    Since the DumpModalsForMatRepFileLabel() uses the prefix, this would lead
 *    to dumping potentially different modals in the same file, and has not
 *    been tested at this stage (Mar 2020).
 *    Hence the SetDumpModalsForMatRep() option causes a hard error if
 *    detecting more than one VSCF calc. using the same prefix and having
 *    SetDumpModalsForMatRep() enabled. See SetupVscfForVcc() in
 *    input/InputAdjustments.cc. -MBH, Mar 2020.
 *
 * @param[in] arVscfPrefix
 *    The prefix of the VSCF calc.
 * @return
 *    File label under which modals should be dumped.
 **/
std::string VscfCalcDef::DumpModalsForMatRepFileLabel
   (  const std::string& arVscfPrefix
   )
{
   std::stringstream ss;
   ss << arVscfPrefix << "_Modals" << "_for_matrep";
   return midas::input::DeleteBlanks(ss.str());
}
