/**
************************************************************************
* 
* @file                RotCoordCalcDef.h
*
* 
* Created:             29-11-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk) 
*
* Short Description:   Calculation definition for rotation of coordinates
*                      based on OptVcfCalcDef.h by Bo Thomsen 
* 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef ROTCOORDCALCDEF_H
#define ROTCOORDCALCDEF_H

// std headers
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <memory>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Error.h"
#include "libmda/numeric/float_eq.h"

//! Type of VSCF calculation used for optimizing coordinates
enum class RotCoordVscfCalcType: int
{  GROUNDSTATE = 0
,  STATEAVERAGE
,  HARMONICGROUNDSTATE
};

//! Convergence criterion for coordinate optimization
enum class RotCoordParam: int
{  VALUE = 0      // Energy change
,  GRADIENT       // Max. gradient element
,  GRADIENTNORM   // Norm of gradient
,  GRADHESS       // Gradient and Hessian (to check that a minimum is obtained)
};

/**
 * Definition of grid for optimization of coordinate-rotation parameters.
 **/
class RotCoordGridCalcDef
{
   private:
      //! Convergence criterion
      RotCoordParam mOptParam = RotCoordParam::GRADHESS;

      //! Max. iterations for Newton-Raphson algorithm
      In mMaxNewtIter = I_10;

      //! Convergence threshold for Newton-Raphson algorithm
      Nb mNewtConv = 1.e-10;

      //! Delta for calculating numerical gradients and Hessians
      Nb mNumDiffDelta = 2.e-2;

      //! Number of Fourier points
      In mFourierPt = I_2;

      //! Plot points
      In mPlotPoints = I_0;

      //! Only make plots - no optimization done.
      bool mPlotOnly = false;

   public:
      /* @name Setters and getters */
      //!@{
      void SetFourierPt(In aIn) {mFourierPt = aIn;}
      void SetNewtConv(Nb aNb) {mNewtConv = aNb;}
      void SetMaxNewtIter(In aIn) {mMaxNewtIter = aIn;}
      void SetPlotPoints(In aIn) {mPlotPoints = aIn;}
      void SetOptParam(const std::string& aStr)
      {
         static const std::map<std::string, RotCoordParam> stringtoparam = 
         {  {"VALUE", RotCoordParam::VALUE}
         ,  {"GRADIENT", RotCoordParam::GRADIENT}
         ,  {"GRADIENTNORM", RotCoordParam::GRADIENTNORM}
         ,  {"GRADHESS", RotCoordParam::GRADHESS}
         };
      
         try
         {
            mOptParam = stringtoparam.at(aStr);
         }
         catch (  const std::out_of_range& err
               )
         {
            MIDASERROR("RotCoordParam: '" + aStr + "' not found!");
         }
      }
      void SetPlotOnly(bool aBool) {mPlotOnly = aBool;}
      void SetNumDiffDelta(Nb aNb) {mNumDiffDelta = aNb;}
  
      In GetFourierPt() const {return mFourierPt;}
      Nb GetNewtConv() const {return mNewtConv;}
      RotCoordParam GetOptParam() const {return mOptParam;}
      In GetMaxNewtIter() const {return mMaxNewtIter;}
      In GetPlotPoints() const {return mPlotPoints;}
      bool GetPlotOnly() const {return mPlotOnly;}
      Nb GetNumDiffDelta() const {return mNumDiffDelta;}
      //!@}
};

/**
 * Definition of VSCF calculation for optimizing coordinates
 **/
class RotCoordVscfCalcDef
{
   private:
      //! Operator file
      std::string mOperFile = "NONE";

      //! Type of VSCF calculation
      RotCoordVscfCalcType mCalcType = RotCoordVscfCalcType::GROUNDSTATE;

      //! Name of this calculation
      std::string mName = " ";

      //! Fix the maximum coupling level in the transformed potential (NB: will result in transformation errors)
      bool mKeepMaxCoupLevel = false;

      //! Rotate zero angle in constructor for VscfOptMeasure (Niels: I have no idea why...)
      bool mRotZeroFirst = false;

   public:
      /* @name Setters and getters */
      //!@{
      void SetOperFile(const std::string& aStr) {mOperFile = aStr;}
      void SetCalcType(const std::string& aStr)
      {
         static const std::map<std::string, RotCoordVscfCalcType> stringtotype =
         {  {"GROUNDSTATE", RotCoordVscfCalcType::GROUNDSTATE}
         ,  {"STATEAVERAGE", RotCoordVscfCalcType::STATEAVERAGE}
         ,  {"HARMONICGROUNDSTATE", RotCoordVscfCalcType::HARMONICGROUNDSTATE}
         };
      
         try
         {
            mCalcType = stringtotype.at(aStr);
         }
         catch (  const std::out_of_range& err
               )
         {
            MIDASERROR("RotCoordVscfCalcType: '" + aStr + "' not found!");
         }
      }
      void SetName(const std::string& aStr) {mName=aStr;}
      void SetKeepMaxCoupLevel(const bool& abool=true) {mKeepMaxCoupLevel=abool;}
      void SetRotZeroFirst(const bool& abool=true) {mRotZeroFirst=abool;}

      RotCoordVscfCalcType GetCalcType() const {return mCalcType;}
      const std::string& GetOperFile() const {return mOperFile;}
      const std::string& GetName() const {return mName;}
      bool KeepMaxCoupLevel() const {return mKeepMaxCoupLevel;}
      bool RotZeroFirst() const {return mRotZeroFirst;}
      //!@}
};

/**
 * CalcDef for coordinate rotation
 **/
class RotCoordCalcDef
{
   private:
      //! Definition of grid
      RotCoordGridCalcDef mGridCalcDef;

      //! CalcDefs for VSCF-energy optimizations
      std::vector<RotCoordVscfCalcDef> mVscfCalcDef;

      //! Weights of VSCF energies.
      std::vector<Nb> mEVscfWeights;

      //! Do localization?
      bool mLocalize = false;

      //! Weight of localization measure
      Nb mLocWeight = C_0; 
      
      //! New suffix of operator
      std::string mNewSuffix = "_rotated";

      //! Number of sweeps in the algorithm
      In mNumberOfSweeps = I_800;

      //! Screen rotations based on max. difference between mode frequencies
      bool mDoFreqScreen = false;

      //! Maximum frequency difference for freq. screening.
      Nb mFreqScreenMaxFreq = I_100;

      //! Convergence threshold of the Jacobi sweep algorithm.
      Nb mConvThr = 1.e-10;

      //! Minimum rotation angle (-1 disables)
      Nb mMinRotAng = -C_1;

      //! Use the parallel algorithm (which performes rotations at the end of the sweep)
      bool mSetDoRotsAtSweepEnd = false;

      //! Operators that need rotation
      std::vector<std::string> mOpersForRot;

      //! Name of rotation-matrix file
      std::string mRotMatFile;

      //! Do random iterations
      In mRandomIter = I_0;

   public:
      /* @name Setters and getters */
      //!@{
      void SetNewSuffix(const std::string& aStr) {mNewSuffix = aStr;}
      void SetSweeps(In aIn) {mNumberOfSweeps = aIn;}
      void SetmDoFreqScreen(bool aBool) {mDoFreqScreen = aBool;}
      void SetmFreqScreenMaxFreq(Nb aNb) {mFreqScreenMaxFreq = aNb;}
      void SetConvThr(Nb aNb) {mConvThr = aNb;}
      void SetDoRotsAtSweepEnd(bool aBool) {mSetDoRotsAtSweepEnd = aBool;}
      void AddOperForRot(const std::string& aStr) {mOpersForRot.emplace_back(aStr);}
      void SetRotMatFile(const std::string& aStr) {mRotMatFile = aStr;}
      void SetRandomIter(In aIn) {mRandomIter = aIn;}
      void SetMinRotAng(Nb aNb) {mMinRotAng = aNb;}
      void SetLocalize(bool aBool) { mLocalize = aBool; }
      void SetLocWeight(Nb aNb) {mLocWeight = aNb;} 
      void EmplaceVscfMeasure
         (  RotCoordVscfCalcDef&& aCalcDef
         ,  Nb aWeight
         )
      {
         this->mVscfCalcDef.emplace_back(aCalcDef);
         this->mEVscfWeights.emplace_back(aWeight);
      }
      
      const auto& GetGridCalcDef() const {return mGridCalcDef;}
      auto& GetGridCalcDef() {return mGridCalcDef;}
      Nb GetMinRotAng() const {return mMinRotAng;}
      In GetRandomIter() const {return mRandomIter;}
      const std::string& GetRotMatFile() const {return mRotMatFile;}
      bool GetPrintRotMat() const {return !mRotMatFile.empty();}
      bool GetSetDoRotsAtSweepEnd() const {return mSetDoRotsAtSweepEnd;}
      const std::string& GetNewSuffix() const {return mNewSuffix;}
      Nb GetConvThr() const {return mConvThr;}
      In GetSweeps() const {return mNumberOfSweeps;}
      bool GetmDoFreqScreen() const {return mDoFreqScreen;}
      Nb GetmFreqScreenMaxFreq() const {return mFreqScreenMaxFreq;}
      In GetNoOpersForRot() const {return mOpersForRot.size();}
      const std::string& GetOperForRotI
         (  In aIn
         )  const
      {
         MidasAssert(aIn < mOpersForRot.size(), "Requesting operator out of range!");
         return mOpersForRot[aIn];
      }

      bool GetLocalize() const { return mLocalize; }
      Nb GetLocWeight() const {return mLocWeight;} 

      const auto& GetVscfCalcDef
         (  In i = I_0
         )  const
      {
         MidasAssert(i < mVscfCalcDef.size(), "Requesting RotCoordVscfCalcDef out of range!");
         return mVscfCalcDef[i];
      }
      auto& GetVscfCalcDef
         (  In i = I_0
         )
      {
         MidasAssert(i < mVscfCalcDef.size(), "Requesting RotCoordVscfCalcDef out of range!");
         return mVscfCalcDef[i];
      }
      Nb GetEvscfWeight(In i = I_0) const
      {
         MidasAssert(i < mEVscfWeights.size(), "Requesting VSCF energy weight out of range!");
         return mEVscfWeights[i];
      } 
      //!@}
      
      //! Do we have a VSCF-optimization?
      bool ContainsVscf() const {return this->mVscfCalcDef.size() > 0; }

      //! How many measures do we have?
      In NMeasures
         (
         )  const
      {
         return mLocalize ? mVscfCalcDef.size() + 1 : mVscfCalcDef.size();
      }
      //! Number of VSCF-opt measures
      In NOptMeasures
         (
         )  const
      {
         return mVscfCalcDef.size();
      }

      //! Sanity check (including normalization of weights)
      void SanityCheck
         (
         )
      {
         // Check that the number of calcdefs and weights is the same
         if (  mVscfCalcDef.size() != mEVscfWeights.size()
            )
         {
            MIDASERROR("Number of EVscf weights does not match number of VscfCalcDefs");
         }

         // Check that weights are positive
         if (  libmda::numeric::float_neg(mLocWeight)
            )
         {
            MIDASERROR("Localization weight is negative!");
         }

         // Normalize weights if needed
         Nb total_weight = mLocalize ? mLocWeight : C_0;
         for(const auto& w : mEVscfWeights)
         {
            if (  libmda::numeric::float_neg(w)
               )
            {
               MIDASERROR("Found negative E_vscf weight!");
            }

            total_weight += w;
         }
         if (  libmda::numeric::float_neq(total_weight, C_1, 10)
            )
         {
            MidasWarning("RotCoordCalcDef: Input weights do not sum to 1. Will normalize!");

            mLocWeight /= total_weight;
            for(auto& w : mEVscfWeights)
            {
               w /= total_weight;
            }
         }
      }
};





#endif //ROTCOORDCALCDEF_H
