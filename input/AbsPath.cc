#include "input/AbsPath.h"

#include "util/Path.h"
#include "util/FileSystem.h"
#include "util/Error.h"
#include "input/ProgramSettings.h"

namespace midas
{
namespace input
{

/**
 * Check if path is relative and if it is make it absolute by starting in scratch.
 * 
 * @param aPath    Path to check.
 *
 * @return Returns absolute path.
 **/
std::string AbsPathScratch(const std::string& aPath)
{
   if(midas::path::IsAbsPath(aPath))
   {
      return aPath;
   }
   else
   {
      return midas::input::gProgramSettings.GetScratch() + "/" + aPath;
   }
}

/**
 * Search for file and return absolute path of file.
 *
 * Will first assert whether path is already absolute,
 * and if not will then check 'main' and 'scratch' in that order.
 *
 * When an absolute path is found, will check if the file actually exists.
 * If the file does not exist, will throw an error and program execution.
 *
 * @param aPath   The file path to check.
 * 
 * @return     Return absolute path of the file when found.
 **/
std::string SearchForFile(const std::string& aPath)
{
   if(midas::path::IsAbsPath(aPath))
   {
      if(filesystem::Exists(aPath))
      {
         return aPath;
      }
   }
   else
   {
      auto test_main_path = midas::input::gProgramSettings.GetMainDir() + "/" + aPath;
      if(filesystem::Exists(test_main_path))
      {
         return test_main_path;
      }

      auto test_scratch_path = midas::input::gProgramSettings.GetScratch() + "/" + aPath;
      if(filesystem::Exists(test_scratch_path))
      {
         return test_scratch_path;
      }
   }

   MIDASERROR("Could not find file '" + aPath + "'.");

   return std::string{""};
}

} /* namespace input */
} /* namespace midas */
