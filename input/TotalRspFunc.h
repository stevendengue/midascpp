/**
************************************************************************
* 
* @file                TotalRspFunc.h
*
* Created:             31-04-2007
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   Total response function datatype
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TOTALRSPFUNC_H
#define TOTALRSPFUNC_H

// Standard Headers
#include <string>
using std::string;
// Standard Headers
#include <vector>
using std::vector;

#include <iostream> 
using std::ostream;


// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

/**
* Construct a definition of a response calculation calculation
* */
class TotalRspFunc
{
   private:
      In     mNorder;           ///< Order of response property 
      vector<string> mRspOps;   ///< Response operators as labels 
      vector<Nb> mRspFrqs;      ///< Frequencies corresponding to operators 
      Nb mValue;                ///< The value for the response function
      Nb mZPVC;                        ///< ZPVC contribution to the total response
      Nb mPVLPlus;                        ///< PV contributio to the total response
      Nb mPVLMinus;                        ///< PV contributio to the total response
      Nb mPVQ;                        ///< PV contributio to the total response
      bool mHasBeenEval;        ///< Has the RF been evaluated?
   public:
      TotalRspFunc(In aNorder);                          ///< Constructor
      void SetRspOrder(In aRsp) {mNorder = aRsp;}   ///< Set Order of response func. 
      void SetOp(const string& arString,In aI=I_0) {mRspOps[aI]=arString;}       ///< Set a particular operator  
      void SetFrq(const Nb& arFrq,In aI=I_0) {mRspFrqs[aI]= arFrq;} ///< Set a particular frequency 
      void AddOp(const string& arString) {mRspOps.push_back(arString);}       ///< Add operator 
      void AddFrq(const Nb& arFrq) {mRspFrqs.push_back(arFrq);} ///< Add frequency

      In GetNorder() const {return mNorder;}
      In GetNopers() const {return mRspOps.size();}
      In GetNfrqs()  const {return mRspFrqs.size();}
      string GetRspOp(In aI) const {return mRspOps[aI];} 
      Nb GetRspFrq(In aI)  const {return mRspFrqs[aI];}
      void SetHasBeenEval(bool aHasBeenEval) {mHasBeenEval=aHasBeenEval;}
      void SetValue(Nb aValue) {mValue = aValue;}
      Nb Value() const {return mValue;}
      bool HasBeenEval() const {return mHasBeenEval;}
      void SetZPVC(Nb aN) {mZPVC=aN;}
      Nb GetZPVC() const {return mZPVC;}
      void SetPVL(Nb aN, Nb aM) {mPVLPlus=aN; mPVLMinus=aM;}
      Nb GetPVLPlus() const {return mPVLPlus;}
      Nb GetPVLMinus() const {return mPVLMinus;}
      void SetPVQ(Nb aN) {mPVQ=aN;}
      Nb GetPVQ() const {return mPVQ;}

      friend ostream& operator<<(ostream&, const TotalRspFunc&);  ///< Output overload
};

#endif

