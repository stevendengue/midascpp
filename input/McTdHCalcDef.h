/**
************************************************************************
*
* @file                McTdHCalcDef.h
*
* Created:             05-10-2018
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Contains input-info that defines a MCTDH calculation
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MCTDHCALCDEF_H_INCLUDED
#define MCTDHCALCDEF_H_INCLUDED

// Standard Headers
#include <string>
#include <utility>
#include <set>
#include <vector>
#include <map>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "ode/OdeInfo.h"
#include "input/TdPropertyDef.h"
#include "td/oper/GaussPulse.h"
#include "input/InputMap.h"

/**
* Construct a definition of a McTdH calculation
* */
class McTdHCalcDef
   :  public TdPropertyDef
{
   public:
      /** @name Alias **/
      //!@{
      using integration_info_t = midas::input::InputMap;
      using method_info_t = midas::input::InputMap;
      //!@}

      //! Initial wave-packet types
      enum class initType : int
      {  VSCF = 0          // Start from VSCF wave packet
      ,  VCI               // Start from VCI wave packet with VSCF modals
      ,  TDH               // Start from TDH calculation (imaginary-time propagation/relaxation)
      ,  MCTDH             // Start from another MCTDH calculation (imaginary-time propagation/relaxation)
      };
      
      //! Parameterizations
      enum class mctdhID : int
      {  LCASTDH = 0       // Linear CASTDH / "standard" MCTDH
      ,  LRASTDH           // Linear RASTDH / restricted active space in linear parameterization
      };

      //! Regularization info
      enum class regularizationType : int
      {  NONE = 0          // Do density-matrix inverse using zgelss (SVD-based linear least squares)
      ,  STANDARD          // Standard MCTDH eigenvalue-decomposition-based matrix inversion
      ,  IMPROVED          // Regularize the coefficient tensor (not implemented at this point...)
      };

      //! Types of g-operator optimization (the one that acutally changes the wave function)
      enum class GOptType : int
      {  VARIATIONAL = 0   // Variational optimization of non-redundant blocks
      ,  DENSITYMATRIX     // Use non-redundant blocks to keep density matrix block-diagonal
      ,  NONE              // Do not optimize non-redundant blocks
      ,  ONEMODEH          // Use one-mode H also for non-redundant blocks
      };

      //! Type of gauge choice (only defining the redundant rotations)
      enum class GaugeType : int
      {  ZERO = 0          // Zero all redundant blocks
      ,  ONEMODEH          // Set all redundant blocks to one-mode part of Hamiltonian
      ,  DENSITYMATRIX     // Set all redundant blocks to propagate in natural modals
      };

      //! Types of regularization used for solving linear equations for g-operator optimization
      enum class GOptRegType : int
      {  NONE = 0          // Use a standard (Hermitian) linear solver
      ,  LS                // Use SVD-based linear-least-squares solver with rcond=eps
      ,  STANDARD          // Use the MCTDH regularization based on eigenvalue decomposition
      ,  SVD               // Use SVD-based algorithm where all singular values (sigma) are set to max(sigma, sigma_max*epsilon)
      ,  XSVD              // Use SVD-based algorithm with smooth regularization (eXponential)
      ,  TIKHONOV          // Use Tikhonov regularization (diagonal) on linear system
      };


   private:
      //! Name of calculation
      std::string mName = "mctdh";

      //! Hamiltonian operator
      std::string mOper = "";

      //! Basis set
      std::string mBasis = "";

      //! Directory for analysis
      std::string mAnalysisDir = "";

      //! Define the size of the active space.
      std::vector<unsigned> mActiveSpace;

      //! Truncate the virtual spaces when e.g. using a large bspline basis for initial VSCF.
      std::vector<In> mModalBasisLimits;

      //! Policy for initial wave packet
      initType mInitialWavePacket = initType::VSCF;

      //! Operator to apply to initial wave packet (e.g. a dipole operator to compute IR spectra)
      std::string mApplyOperator = "";

      //! Type of spectral-basis transform. Empty string will give spectral basis of reference potential.
      std::string mSpectralBasisOper = "";

      //! Name of calculation that defines the initial wave packet
      std::string mInitCalcName = "";

      //! Do imaginary-time propagation
      bool mImagTime = false;

      //! Threshold for stopping imag-time prop.
      Nb mImagTimeThreshold = -C_1;

      //! Do exact propagation (i.e. ignore modal derivative)
      bool mExact = false;

      //! Method-specific info
      method_info_t mMethodInfo;

      //! Integration scheme
      integration_info_t mIntegrationInfo;

      //! IO level
      In mIoLevel = I_1;

      //! Save interpolated WFs to disc
      bool mSaveInterpolatedWfs = false;

      //! Save accepted WFs to disc
      bool mSaveAcceptedWfs = false;

      //! Propagate modals for electronic DOF (debug option)
      bool mPropagateElectronicDofModals = false;

      /** @name Definitions for time-dependent Hamiltonian **/
      //!@{
      //! Use Gaussian pulse
      bool mGaussianPulse = false;

      //! Operator for pulse
      std::string mPulseOper = "pulse";

      //! Specs for pulse
      std::map<midas::td::GaussParamID, Nb> mPulseSpecs =
         {  {midas::td::GaussParamID::AMPL, 1.0}
         ,  {midas::td::GaussParamID::TPEAK, 0.0}
         ,  {midas::td::GaussParamID::SIGMA, 1.0}
         ,  {midas::td::GaussParamID::FREQ, 0.0}
         ,  {midas::td::GaussParamID::PHASE, 0.0}
         };
      //!@}


      //! Validate method info
      void ValidateMethodInfo
         (
         );

      //! Validate integration info
      void ValidateIntegrationInfo
         (
         );

   public:
      //! Default c-tor
      McTdHCalcDef() = default;

      //! Clone
      McTdHCalcDef Clone
         (
         )  const
      {
         return *this;
      }

      //! Sanity check. Check that input makes sense.
      void SanityCheck
         (
         );

      //! Read IntegrationInfo
      bool ReadIntegrationInfo
         (  std::istream&
         ,  std::string&
         );

      //! Read policy for initial wave function
      bool ReadInitialWfPolicy
         (  std::istream&
         ,  std::string&
         ,  std::set<std::string>&
         );

      //! Read method name
      bool ReadMethod
         (  std::istream&
         ,  std::string&
         );

      //! Add method info
      template
         <  typename T
         >
      void AddMethodInfo
         (  const std::string& aKey
         ,  const T& aVal
         )
      {
         mMethodInfo[aKey] = aVal;
      }


      //! Macro for Getter and Setter interface
      template<typename T> struct argument_type;
      template<typename T, typename U> struct argument_type<T(U)> { using type = U; };
      #define UNPACK(...) __VA_ARGS__
      #define SET(NAME, TYPE) \
         McTdHCalcDef& Set##NAME(const argument_type<void(TYPE)>::type& a##NAME) \
         { \
            m##NAME = a##NAME; \
            return *this; \
         }
           
      #define GET(NAME, TYPE) \
         const argument_type<void(TYPE)>::type& NAME() const \
         { \
            return m##NAME; \
         }

      #define GETSET(NAME, TYPE) \
         GET(NAME, UNPACK(TYPE)) \
         SET(NAME, UNPACK(TYPE))
         
         GET(MethodInfo, method_info_t);
         GET(IntegrationInfo, integration_info_t);
      
         GETSET(Name, std::string);
         GETSET(Basis, std::string);
         GETSET(Oper, std::string);
         GETSET(AnalysisDir, std::string);
         GETSET(ActiveSpace, std::vector<unsigned>);
         GETSET(ImagTime, bool);
         GETSET(ImagTimeThreshold, Nb);
         GETSET(Exact, bool);
         GETSET(IoLevel, In);
         GETSET(InitialWavePacket, initType);
         GETSET(ApplyOperator, std::string);
         GETSET(InitCalcName, std::string);
         GETSET(SpectralBasisOper, std::string);
         GETSET(SaveInterpolatedWfs, bool);
         GETSET(SaveAcceptedWfs, bool);
         GETSET(GaussianPulse, bool);
         GETSET(PulseOper, std::string);
         GETSET(PulseSpecs, UNPACK(std::map<midas::td::GaussParamID, Nb>));
         GETSET(ModalBasisLimits, std::vector<In>);
         GETSET(PropagateElectronicDofModals, bool);
      #undef SET
      #undef GET
      #undef GETSET
      #undef UNPACK



      //! Are we using exponential parameterization?
      std::pair<bool, bool> ExponentialParameterization
         (
         )  const;

      //! Are we using exponential transformation of modals?
      bool ExponentialModalTransform
         (
         )  const;

      //! Are we using exponential transformation of coefs?
      bool ExponentialCoefsTransform
         (
         )  const;

      //! Ostream
      friend std::ostream& operator<<
         (  std::ostream&
         ,  const McTdHCalcDef&
         );
};

#endif /* MCTDHCALCDEF_H_INCLUDED */
