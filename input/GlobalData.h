#ifndef GLOBALDATA_H_INCLUDED
#define GLOBALDATA_H_INCLUDED

#include <list>
#include <vector>
#include <type_traits>

#include "libmda/util/unique_any_type.h"
#include "util/Error.h"

namespace midas
{
namespace global
{

/**
 * Class for handling global services.
 **/
class ServiceLocator
{
   public:
      //!@{
      //! Defining some types
      using any_type               = libmda::util::unique_any_type;
      using service_container_type = std::vector<any_type>;
      //!@}

   private:
      //! Static container to be "share-able" for all ServiceLocators.
      static service_container_type mServiceContainer;
      
   public:
      //! Default constructor.
      ServiceLocator() = default;

      //! Check whether the service is already in container.
      template<class T>
      bool HasService(const service_container_type& = mServiceContainer) const;
      
      //! Add a service to a container. 
      template<class T>
      bool AddService(T&& t, service_container_type& = mServiceContainer);
      
      //! Get a service from a container.
      template<class T, std::enable_if_t<!std::is_const_v<T>, int> = 0>
      T& GetService(service_container_type& = mServiceContainer) const;
      
      //! Get a service from a container.
      template<class T,  std::enable_if_t<std::is_const_v<T>, int> = 0>
      T& GetService(const service_container_type& = mServiceContainer) const;

      //! Clear service container. Default will clear the statically stored service container (do not use unless you know what you are doing :D).
      void Clear(service_container_type& = mServiceContainer);
};

/**
 * Check whether the service is in container.
 * 
 * @param aServiceContainer   The container to check.
 *
 * @return    Returns true if service is found, otherwise false.
 **/
template<class T>
bool ServiceLocator::HasService
   (  const service_container_type& aServiceContainer
   )  const
{
   for(auto& service : aServiceContainer)
   {
      if(service.type() == typeid(std::decay_t<T>))
      {
         return true;
      }
   }
   return false;
}

/**
 * Add a service to a container.
 * First checks whether the service is already in the container,
 * if so, the service is not added.
 *
 * @param t                   The service to add.
 * @param aServiceContainer   The container to add the service to.
 *
 * @return   Returns true if service was added, false otherwise.
 **/
template<class T>
bool ServiceLocator::AddService
   (  T&& t
   ,  service_container_type& aServiceContainer
   )
{
   if(!this->HasService<T>(aServiceContainer))
   {
      aServiceContainer.emplace_back(std::forward<T>(t));
      return true;
   }
   return false;
}

/**
 * Get a service from a container.
 *
 * @param aServiceContainer   The container to search.
 *
 * @return   Returns the service if found.
 **/
template<class T, std::enable_if_t<!std::is_const_v<T>, int> >
T& ServiceLocator::GetService
   (  service_container_type& aServiceContainer
   )  const
{
   for(auto& service : aServiceContainer)
   {
      if(service.type() == typeid(std::decay_t<T>))
      {
         return service.get<std::decay_t<T> >();
      }
   }
   
   MIDASERROR("Service was not found.");

   // Return something to silence warning
   return aServiceContainer.front().get<T>();
}

/**
 * Get a service from a container.
 *
 * @param aServiceContainer   The container to search.
 *
 * @return   Returns the service if found.
 **/
template<class T, std::enable_if_t<std::is_const_v<T>, int> >
T& ServiceLocator::GetService
   (  const service_container_type& aServiceContainer
   )  const
{
   for(auto& service : aServiceContainer)
   {
      if(service.type() == typeid(std::decay_t<T>))
      {
         return service.get<std::decay_t<T> >();
      }
   }
   
   MIDASERROR("Service was not found.");

   // Return something to silence warning
   return aServiceContainer.front().get<std::decay_t<T> >();
}

} /* namesapce global */
} /* namespace midas */

#endif /* GLOBALDATA_H_INCLUDED */
