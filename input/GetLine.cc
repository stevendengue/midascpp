#include "GetLine.h"
#include "Trim.h"

namespace midas
{
namespace input
{

/**
 * Get line from file and return in str.  Can take different delimeters (e.g.
 * read only until `\t` or smt.).
 * Follows interface of std::getline.
 *
 * @param is    Input stream to read from.
 * @param str   On output the read newly read line.
 * @param delim Custom delimeter, defaulted to `\n` (newline).
 * @return      Returns the input stream after line has been read, such that GetLine can be looped over.
 **/
std::istream& GetLine
   ( std::istream& is
   , std::string& str
   , char delim
   )
{
   auto&& return_val = getline(is,str,delim);
   str = midas::input::Trim(str);
   return return_val;
}

/**
 * Parse input string as a MidasCpp keyword, i.e. set everything to uppercase
 * and delete blanks in front and back of string.
 *
 * @param str            The string to parse.
 * @return               The parsed string.
 **/
std::string ParseInput
   ( const std::string& str
   )
{
   std::string parsed_str = midas::input::ToUpperCase(str);
   parsed_str = midas::input::DeleteBlanks(parsed_str);
   return parsed_str;
}

/**
 * Parse input string as a path, i.e. delete blanks in front and back of string.
 *
 * @param str            The string to parse.
 * @return               The parsed string.
 **/
std::string ParsePathInput
   ( const std::string& str
   )
{
   std::string parsed_str = midas::input::DeleteBlanks(str);
   return parsed_str;
}

/**
 * Combines GetLine() and ParseInput().
 *
 * @param is           Input stream to read from.
 * @param str          On output the read newly read line.
 * @param delim        Custom delimeter, defaulted to '\n' (newline).
 * @return             Returns the input stream after line has been read, such that GetLine can be looped over.
 *                     Follows interface of std::getline.
 **/
std::istream& GetParsedLine
   (  std::istream& is
   ,  std::string& str
   ,  char delim
   )
{
   auto&& return_val = GetLine(is, str, delim);
   str = ParseInput(Trim(str));
   return return_val;
}

/**
 * Combines GetLine() and ParsePathInput().
 *
 * @param is           Input stream to read from.
 * @param str          On output the read newly read line.
 * @param delim        Custom delimeter, defaulted to '\n' (newline).
 * @return             Returns the input stream after line has been read, such that GetLine can be looped over.
 *                     Follows interface of std::getline.
 **/
std::istream& GetParsedPathLine
   (  std::istream& is
   ,  std::string& str
   ,  char delim
   )
{
   auto&& return_val = GetLine(is, str, delim);
   str = ParsePathInput(Trim(str));
   return return_val;
}

/**
 * GetLine and convert to Nb. Returns the read number so function CANNOT be looped.
 *
 * @param is        Input stream to read from.
 * @param s         On output the read newly read line.
 * @return          Returns number read from input stream.
 **/
Nb GetNb
   ( std::istream& is
   , std::string& s
   )
{
   return midas::input::Get<Nb>(is, s);
}

/**
 * GetLine and convert to In. Returns the read integer so function CANNOT be looped.
 *
 * @param is        Input stream to read from.
 * @param s         On output the read newly read line.
 * @return          Returns interger read from input stream.
 **/
In GetIn
   ( std::istream& is
   , std::string& s
   )
{
   return midas::input::Get<In>(is, s);
}

} /* namespace input */
} /* namespace midas */
