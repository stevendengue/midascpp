#ifndef INPUTOPERFROMFILE_H_INCLUDED
#define INPUTOPERFROMFILE_H_INCLUDED

#include <string>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

class OpDef;

namespace midas
{
namespace input
{
 
//! Read Midas .mop format. (This function might go away from this interface at some point, so use it wisely! 20/11-2017)
std::string ReadGenericMidasInput
   (  std::istream& aFile
   ,  OpDef& arOpDef
   ,  const std::string& aFileName
   ,  const In& aFnr
   ,  const In& aInpLevel
   ,  const Nb& aScaling = C_1
   ,  const bool& aSkipHeaderAndFooter = false
   );

//! Read operator file given in different formats.
void InputOperFromFile
   (  OpDef& arOpDef
   ,  In aNrestrictModes = I_0
   ,  Nb aZeroThr        = C_0
   );

} /* namespace input */
} /* namespace midas */

#endif /* INPUTOPERFROMFILE_H_INCLUDED */
