#include "input/InputReader.h"
#include "input/IsKeyword.h"

/**
 * Input reader function.
 *
 * @param Minp Input stream.
 * @param s    String that always holds last read line.
 * @param imap Map of keyword + reader functions.
 *
 * @return     Has next keyword been read?
 **/
bool InputReader
   (  std::istream& Minp
   ,  std::string& s
   ,  const input_map_t& imap
   )
{
   bool already_read = false;

   try
   {
      std::string s_parsed;
      
      // Read input file line by line.
      bool is_lower_level = false;
      while (  (  already_read
               || midas::input::GetLine(Minp,s)
               ) // get new line if not already read
            && !( is_lower_level
                  =  midas::input::IsLowerLevelKeyword
                     (  s_parsed = midas::input::ParseInput(s)
                     ,  imap.Level()
                     )
               ) // parse line and check for lower level keyword
            )
      {
         //Mout << " trying to read: " << s_parsed << std::endl;
         already_read = false;

         // Look for keyword and call handler function.
         auto iter = imap.find(s_parsed);
         if(iter != imap.end())
         {
            already_read = iter->second(Minp, s);
         }
         else
         {
            throw MidasInputException(s);
         }
      }

      // If breaking the loop due to lower level keyword, we must've read the
      // line already.
      if (is_lower_level)
      {
         already_read = true;
      }
   }
   catch(MidasInputException& e) // catch explicit input excpetions
   {
      MIDASERROR("{Input Exception}\n" + std::string(e.what()));
   }
   catch(std::exception& e) // catch std excpetions
   {
      MIDASERROR("{Standard Exception}\n" + std::string(e.what()));
   }
   catch(...) // catch whatever
   {
      MIDASERROR("Reading Midas Input i caught 'something'.");
   }

   return already_read;
}
