/**
************************************************************************
* 
* @file                PesCalcDef.h
*
* Created:             16-07-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Base class for a Pes input-info handler 
* 
* Last modified:       24-11-2014 (Carolin Koenig)
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef PESCALCDEF_H
#define PESCALCDEF_H

// std headers
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include <set>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/FindKeyword.h"
#include "pes/splines/SplineType.h"
#include "input/FlexCoupCalcDef.h"
#include "input/SymmetryThresholds.h"

// using declarations
using std::vector;
using std::map;
using std::string;

extern In gPesIoLevel;

using namespace midas;

namespace midas
{
namespace molecule
{
   class MoleculeInfo;
}
}

// forward declarations
class Molecule;

class PesCalcDef
{
      using extended_grid_t = std::tuple<std::vector<In>, std::vector<In>, std::vector<std::string> >;

   private:
      bool                       mGMatDiagonal;
      bool                       mGMatConst;
      bool                       mNoDerivOfGDet;
      bool                       mNormalcoordInPes;     ///< Flag for normalcoordinates in PES 
      bool                       mZmatInPes;            ///< Flag for Zmat in Pes
      bool                       mCartesiancoordInPes;  ///< Flag for Cartesian coordinates in PES
      bool                       mFalconInPes;          ///< Use FALCON coordinates in potential energy/molecular property surface generation
      bool                       mPscInPes;             ///< Use polyspherical coordinates in potential energy/molecular property surface generation
      std::string                mTanaRunFile;          ///< Name of the Tana runscript
      vector<string>             mPesNumSource;         ///< Electronic structure program used in NumDer
      In                         mPesNumAnalyticOrder;  ///< analytic order in NumDer
      In                         mPesNumMCR;            ///< Mode Combination Range in NumDer
      Nb                         mNumDerFreqThres;      ///< Threshold for numerical freqs in NumDer
      In                         mPesNumMaxPolOrExp;    ///< Maximum polynomial order in expansion of potential (property) surface
      In                         mPesExcState;          ///< Excited state number in Pes
      vector<Nb>                 mPesUserDefinedBound;  ///< User defined boundaries for the grid
      vector<Nb>                 mPes_Grid_Scal;         ///< Scaling factors in Grid setting
      vector<In>                 mPesGridInMCLevel;      ///< No. of grid points in each MC level
      vector<string>             mPesFracInMCLevel;      ///< Fractions in in each MC level
      In                         mPesGridMaxDim;         ///< Maximum dimension of grids in non-Adga calculations
      In                         mPesGridInitialDim;     ///< Defines the intially spanned space for an Adga calculation
      std::vector<In>            mAnalyzeStates;         ///< Number of states to be inlcuded in the density analysis for an Adga calculation
      In                         mDumpSpInterval;        ///< Dump calculated single points to disc after specified number of calculations have been carried out
      In                         mPesNumDalton;          ///< Number of types of dalton calculations (inputs).
      In                         mPesNumAces;            ///< Number of types of aces calculations (inputs).
      In                         mPesNumCfour;           ///< Number of types of aces calculations (inputs).
      In                         mPesNumEsp;            ///< Number of input for  electronic structure programs (inputs).
      In                         mPesNumBs;             ///< Number of basis for  electronic structure calculation.
      In                         mPesNumNucInp;         ///< Number of input of set of nuclei
      std::vector<extended_grid_t> mExtendedGridSettings;
      std::string                mIntType;               ///< Type of interpolation
      std::string                mIntSurface;            ///< Which surface to be interpolated
      bool                       mPesSplineInt;          ///< Spline iterpolation
      bool                       mPesSplineIntForExtrap; ///< Spline iterpolation
      SPLINEINTTYPE              mSplineType;            ///< Spline type for spline interpolation
      bool                       mPesShepardInt;         ///< Shepard interpolation
      bool                       mPesShepardIntForExtrap;       ///< Shepard interpolation
      In                         mPesShepardOrd;         ///< Order of the Shepard interpolation
      In                         mShepardExp;            ///< Exponent of the Shepard interpolation
      vector<In>                 mPesFGMesh;             ///< Define granularity of finer grid
      vector<Nb>                 mPesFGScalFact;         ///< Define scaling factors for each dimension in the finer grid
      bool                       mPesPlotAll;            ///< decide wheter a plot of the potentials and properties
      bool                       mPesPlotRealESP;
      bool                       mPesPlotSelected;       ///< decide wheter a plot of selected potentials and properties
      vector<string>             mPesPlotSelectedInfo;   ///<map for storing info about selected modes and properties to be plotted
      map<vector<In>,string>     mPesPlotSelectedMap;    ///<map for storing info about selected modes and properties to be plotted
      map<In,vector<string> >    mGenericPes;
      bool                       mPesDoInt;              ///< Do interpolation for multilevel surface
      bool                       mPesDoIntForExtrap;     ///< Do interpolation for extrapolated surface
      bool                       mPesFreqScalCoordInFit; ///< Use frequency scaled coordinates for fitting routine
      bool                       mPesCalcMuTens;         ///< determines whether to add or not to calculate mu tensors
      bool                       mPesCalcCoriolisAndInertia; ///< Calculate the Coriolis coupling and moment of inertia for reference geometry
      bool                       mPesInertialFrame;      ///< enable the reference structure to be oriented in the inertial frame
      bool                       mPesModelPot;          ///< Enable the use of model potentials   
      bool                       mPesDoPartridgePot;    ///< Use the Partridge-Schwenke potential surface for water
      bool                       mPesDoAmmModelPot;     ///< Use the NH3 model potential surface
      bool                       mPesDoTestPot;         ///< Use the Test Pot
      bool                       mPesDoArNtwoModelPot;  ///< Use the Ar-N2+ model potential surface
      In                         mPesType;              ///< Which PES to generate
      bool                       mPesDoMorsePot;        ///< Use the Morse Potential
      bool                       mPesDoBrennerPot;      ///< Use the Brenner Potential
      string                     mPesMorseDefs;         ///< Additional infos on the morse potential
      bool                       mPesDoDoubleWpot;      ///< Use a double well potential
      string                     mPesDoubleWdefs;       ///< Additional infos on the double well potential
      bool                       mPesTaylor;            ///< Do Taylor expansion non-grid-based surface generation 
      bool                       mPesStatic;            ///< Do Static grid-based surface generation
      bool                       mPesAdga;              ///< Do Adga grid-based surface generation
      std::vector<std::string>   mAdgaVscfNames;         ///< Name of Vscf calculation used in the Adga
      bool                       mDynamicAdgaExt;       ///< Dynamic extension of Adga grid
      bool                       mDoExtInFirstIter;     ///< Do not extent Adga grid from first iteration
      bool                       mCalcDensAtEachMcl;    ///< Calculate density each mode combination level
      bool                       mDoAdgaExtAtEachMcl;   ///< Check potential bounds for each mode combination level
      bool                       mUpdMeanDensAtEachMcl; ///< ReCalculate density for each mode combination level
      In                         mPesGLQuadPnts;        ///< The number of points used in numerical integration by means of Gauss-Legendre quadrature
      In                         mPolyInterPoints;      ///< Number of points to be used for polynomial interpolation
      std::string                mAnalyzeDensMethod;    ///< Density analysis method
      bool                       mPesUseMaxDens;        ///< Analysize the max. density
      bool                       mPesUseMeanDens;       ///< Analysize the mean density
      In                         mPesIterMax;           ///< Max nr. of iterations in the iterative procedure
      Nb                         mPesItPotThr;          ///< Thresh. for potential in iterative pes
      Nb                         mPesItDensThr;         ///< Thresh. for density in iterative pes
      Nb                         mPesItVdensThr;        ///< Thresh. for pot*density in iterative pes
      Nb                         mPesItResDensThr;      ///< Thresh. for pot*density in iterative pes
      Nb                         mPesItResEnThr;        ///< Thresh. for pot*density in iterative pes
      Nb                         mPesItNormResEnThr;    ///< Thresh. for norm pot*density in iterative pes
      Nb                         mPesItNormVdensThr;    ///< Threshold for norm of pot*density for ADGA
      std::vector<Nb>            mAdgaRelConvScalFacts; ///< Scale factors for the relative Adga convergence criterion
      std::vector<Nb>            mAdgaAbsConvScalFacts; ///< Scale factors for the absolute Adga convergence criterion
      bool                       mPesUseItPotThr;       ///< Use only v for checking convergency
      bool                       mPesUseItDensThr;      ///< Use only rho*v for checking convergency
      bool                       mPesUseItVdensThr;     ///< Use only rho*v for checking convergency
      bool                       mPesUseItNormVdensThr; ///< Use the norm of rho*v to check convergence of ADGA
      bool                       mPesCalcPotFromOpFile; ///< Allow the estimation of the potential from the .mop file
      bool                       mPesAdaptivSearch;       ///< Adaptive search of the boxes to be diveded
      bool                       mPesPlotAdga2D;         ///< Plot 2D potentials at each iteration.
      In                         mPesNmaxSplittings;     ///< How many boxes are splitted. If negative also the symmetric will be splitted 
      bool                       mUseMultiStateAdga;     ///< Use multistate Adga
      vector<In>                 mPesAdaptiveProps = {1};        ///< the property surfaces which are used in the ADGA algorithm
      vector<Nb>                 mPesAdaptivePropsWeights;   
      std::string                mPesBarFileStorageType; ///< The type of storage for bar file info
      In                         mPesItGridExpScalFact;  ///< Expansion factor for the iterative grids
      bool                       mPesMultiLevelDone;    ///< multilevel approach done
      std::vector<In>            mLevelMc;              ///< define the maximum mc that has to be computed for each level of the multilevel appr.
      bool                       mDincrSurface;         ///< Use double incremental scheme for surface generation
      std::string                mDincrMethod;          ///< Type of double incremental scheme to use
      std::string                mFcInfo;                ///< File with information on the fragment combinations
      std::string                mDincrSurfaceType;     ///< Type of double incremental surface
      std::vector<Nb>            mIcModesScreenThr;     ///< Screening threshold for inter-connecting modes
      bool                       mDincrGiven;           ///< Restart from .mop files 
      vector<In>                 mPesBsIndex;           ///< contains the index fo the extrapolation fo the electronic basis set
      vector<Nb>                 mPesLinearComb;        ///< contains the coef.s for the linear combination of multilevel surfaces
      bool                       mPesDoLinearComb;      ///< Perform linear combination of multilevel surfaces
      bool                       mPesBoundariesPreopt;  ///< initilize the iterative procedure with a fast method
      
      std::vector<string>        mMakeCutAnalysis;      ///< Vector containing strings for cut analysis
      In                         mCutMesh;              ///< grid mesh for the cut analysis
      bool                       mPesPropInMem;         ///< Store sorted properties in memory
      bool                       mPesDoExtrap;          ///< Extrapolate in PES
      bool                       mUseExtMcr;            ///< Use extended mode combination range
      std::set<In>               mExtMcrModes;          ///< Modes used in the extended mode combination range
      std::vector<Nb>            mPscMaxGridBounds;     ///< Modifier for the maximum box bounds
      bool                       mMeanDensOnFiles;      ///< Mean density is provided and not calculated
      bool                       mExtrapOnlyGradient;   ///< Extrapolate using only gradients
      bool                       mUseExtrapStartGuess;  ///< Use extrapolation as start guess fro coupled surfaces
      In                         mExtrapolateFrom;      ///< Mode Coupling level to be used for approx. 
      In                         mExtrapolateTo;        ///< Mode Coupling level approximated
      string                     mPotentialFilename;    ///< Potential Filename 
      string                     mMmParFilename;        ///< Parameter file for MM calculation
      string                     mMmCoordFilename;      ///< Parameter file for MM calculation
      bool                       mPesScreenModeCombi;   ///<Allow screening of mode combi based on specific criteria
      Nb                         mPesScreenThresh;      ///< Threshold for screening
      vector<In>                 mPesVibNumVec;         ///< Vibrational quantum numbers
      In                         mPesScreenMethod;      ///< Screening method
      bool                       mUseSym;               ///< logical flag for use of symmetry of the pes
      bool                       mPesScreenSymPotTerms; ///< logical flag for use of symmetry in the screening of the potential terms
      string                     mPesSymLab;            ///< Point group symmetry label
      SymmetryThresholds         mPointGroupSymThrs;    //!< Num. thr. for sym. determination.
      Nb                         mSymThr;               ///< Threshold for detecting symmetry
      Nb                         mPesAdgaFgStep;        ///< relative step size in adga Fine Grid definition 
      bool                       mPesAdgaMsiCut;        ///< Shepard interpolation during adga stop at a given iteration
      std::vector<In>            mPesAdgaMsiIt;          ///< iteration when Shepard interpolation is dropped
      Nb                         mPesMsiLambda;         ///< Lambda
      
      //! N_threads for PES multilevels. 0 means use all (as per --numthreads cmd-line opt.).
      std::vector<Uin>           mNThreads;

      bool                       mPesDiatomic;          ///< Diatomic in PES
      bool                       mPesVibPol;            ///< Vibrational polarizability
      bool                       mPesGrid;              ///< Grid Based PES
      bool                       mPesAvoidRestart;      ///< Avoid using previous calcs.
      Nb                         mPesStepSizeInAu;      ///< Stepsize in Au in NumDer
      string                     mPesDispType;
      bool                       mSaveItOperFile;       ///< Save operator file (.mop) after each ADGA iteration
      bool                       mSaveSpScratchDir;     ///< Save the individuel single point scratch directories
      bool                       mSavePscGeomScrDir;    ///< Save the individuel geometry scratch directories for the use of polyspherical coordinates
      bool                       mSaveDispGeom;         ///< Save displaced geometries to file
      std::string                mSaveDispGeomFormat;
      bool                       mDoNotSaveEspOut;      ///< Do Not Save DALTON.OUT
      bool                       mDoNotSaveInpFiles;    ///< Do Not Save input files
      bool                       mAddPsuedoPotToEn;
      set<string>                mList_Of_Hessians;     ///< List of Hessians to be calculated in NumDer

      bool                       mSaveTrainData = false;   ///< Save training data for ML algorithm(s)
      std::string                mMLDriver      = "";      ///< Reference to ML definition, will make the above obsolete in the future
      std::vector<std::string>   mSinglePointNames;
      In                         mMaxMLRuns = 0;            ///< Maximal number of ML models which should be refined
      bool                       mMLRemoveInitSet = 0;      ///< Specifies if the initial training set should be removed

      In                         mCurrentESProg;         ///< To keep track of which part of mPesNumSource is being used
      bool                       mSpecBas;               ///< Do we read basis defs from PesCalc.info
      bool                       mPesScalCoordInFit;     ///< Use scaled coordinates for fitting routine
      //! Name of the fit basis being used for the calculation
      std::string                mPesFitBasisName;
      bool                       mPesDoAdgaPreScreen;
      FlexCoupCalcDef            mPesFlexCoupCalcDef;

      bool                       mHistoricAdgaConv    = false;
      bool                       mHistoricAdgaScaling = false;
      bool                       mHistoricAdgaGrid    = false;
      bool                       mHistoricPesNumSource = false;
      bool                       mScreenOperCoef = true;
      Nb                         mScreenOperCoefThresh = C_NB_EPSILON * 1e2;
      //! Prefix for where to put singlepoint scratch directories
      std::string mSpScratchDirPrefix = "";
      //! 
      std::string mMidasIfcPropInfo = "";
      //!
      bool                       mAdgaUseHistoricNonSopIntegrals = false;
      //!
      Nb                         mNormalCoordinateThreshold  = 1e-7;
      bool                       mNormalizeNormalCoordinates = false;
      bool                       mOrthoNormalizeNormalCoordinates = false;

   public:
      
      //! Constructor
      PesCalcDef();

      //! Test function
      void TestInput();

      //! Validate pes input in relation to number of vibrational modes
      void ValidateInput(const In& aNoNuclei, const In& aNoVibModes);

      //!
      void PrintCalcDefInfo() const;
      
      //! Check input to the pes-module
      void InputAfterProcessing();

      //Get x functions
      std::string GetmPesFitBasisName() const                        {return mPesFitBasisName;}
      bool GetmPesScalCoordInFit() const                             {return mPesScalCoordInFit;}
      bool GetmSpecBas() const                                       {return mSpecBas;}
      bool GetmAddPsuedoPotToEn() const                              {return mAddPsuedoPotToEn;}
      bool GetmGMatDiagonal() const                                  {return mGMatDiagonal;}
      bool GetmGMatConst() const                                     {return mGMatConst;}
      bool GetmNoDerivOfGDet() const                                 {return mNoDerivOfGDet;}
      Nb GetmSymThr() const                                          {return mSymThr;}
      Nb GetmPesMsiLambda() const                                    {return mPesMsiLambda;}
      In GetmPesNumAnalyticOrder() const                             {return mPesNumAnalyticOrder;}
      const vector<string>& GetmPesPlotSelectedInfo()   const        {return mPesPlotSelectedInfo;}
      In GetmShepardExp() const                                      {return mShepardExp;}
      Nb GetmNumDerFreqThres() const                                 {return mNumDerFreqThres;}
      bool GetmPesScreenSymPotTerms() const                          {return mPesScreenSymPotTerms;}
      bool GetmPesShepardIntForExtrap() const                        {return mPesShepardIntForExtrap;}
      bool GetmPesSplineIntForExtrap() const                         {return mPesSplineIntForExtrap;}
      bool GetmPesSplineInt() const                                  {return mPesSplineInt;}
      Nb   GetmPesAdgaFgStep() const                                 {return mPesAdgaFgStep;}
      const vector<Nb>& GetmPesFGScalFact() const                    {return mPesFGScalFact;}
      bool GetmPesDoIntForExtrap() const                             {return mPesDoIntForExtrap;}
      bool GetmPesDoInt() const                                      {return mPesDoInt;}
      const vector<In>& GetmPesFGMesh() const                        {return mPesFGMesh;}
      In GetmPesNumMaxPolOrExp() const                               {return mPesNumMaxPolOrExp;}
      SPLINEINTTYPE GetmSplineType() const                           {return mSplineType;}
      const vector<Nb> GetmPesUserDefinedBound() const               {return mPesUserDefinedBound;}
      bool GetmPesFreqScalCoordInFit() const                         {return mPesFreqScalCoordInFit;}
      bool GetmExtrapOnlyGradient() const                            {return mExtrapOnlyGradient;}
      const map<vector<In>,string>& GetmPesPlotSelectedMap() const   {return mPesPlotSelectedMap;}
      In GetmPesExcState() const                                     {return mPesExcState;}
      bool GetmPesPlotSelected() const                               {return mPesPlotSelected;}
      bool GetmPesPlotAll() const                                    {return mPesPlotAll;}
      bool GetmPesPlotRealESP() const                                {return mPesPlotRealESP;}
      const vector<In>& GetmPesVibNumVec() const                     {return mPesVibNumVec;}
      In GetmPesScreenMethod() const                                 {return mPesScreenMethod;}
      string GetmPesDispType() const                                 {return mPesDispType;}
      In GetmExtrapolateFrom() const                                 {return mExtrapolateFrom;}
      const bool& GetmUseExtrapStartGuess() const                    {return mUseExtrapStartGuess;}
      string GetmPotentialFilename() const                           {return mPotentialFilename;}
      bool GetmDoAdgaPreScreen() const                               {return mPesDoAdgaPreScreen;}
      In GetmPesType() const                                         {return mPesType;}
      bool GetmPesDoTestPot() const                                  {return mPesDoTestPot;}
      string GetmPesDoubleWdefs() const                              {return mPesDoubleWdefs;}
      string GetmPesMorseDefs() const                                {return mPesMorseDefs;}
      bool GetmPesDoArNtwoModelPot() const                           {return mPesDoArNtwoModelPot;}
      bool GetmPesDoAmmModelPot() const                              {return mPesDoAmmModelPot;}
      bool GetmPesDoPartridgePot() const                             {return mPesDoPartridgePot;}
      bool GetmPesDoDoubleWpot() const                               {return mPesDoDoubleWpot;}
      bool GetmPesDoMorsePot() const                                 {return mPesDoMorsePot;}
      Nb GetmPesScreenThresh() const                                 {return mPesScreenThresh;}
      In GetmPesNmaxSplittings() const                               {return mPesNmaxSplittings;}
      bool GetmPesAdaptivSearch() const                              {return mPesAdaptivSearch;}
      bool GetmPesUseItPotThr() const                                {return mPesUseItPotThr;}
      bool GetmPesUseItDensThr() const                               {return mPesUseItDensThr;}
      bool GetmPesUseItVdensThr() const                              {return mPesUseItVdensThr;}
      bool GetmPesUseItNormVdensThr() const                          {return mPesUseItNormVdensThr;}
      Nb GetmPesItPotThr() const                                     {return mPesItPotThr;}
      Nb GetmPesItDensThr() const                                    {return mPesItDensThr;}
      Nb GetmPesItVdensThr() const                                   {return mPesItVdensThr;}
      Nb GetmPesItNormVdensThr() const                               {return mPesItNormVdensThr;}
      Nb GetmPesItResEnThr() const                                   {return mPesItResEnThr;}
      Nb GetmPesItNormResEnThr() const                               {return mPesItNormResEnThr;}
      Nb GetmPesItResDensThr() const                                 {return mPesItResDensThr;}
      const std::vector<Nb>& GetmAdgaRelConvScalFacts() const        {return mAdgaRelConvScalFacts;}
      const std::vector<Nb>& GetmAdgaAbsConvScalFacts() const        {return mAdgaAbsConvScalFacts;}
      const  map<In,vector<string> >& GetmGenericPes() const         {return mGenericPes;}
      bool GetmPesScreenModeCombi() const                            {return mPesScreenModeCombi;}
      const bool& GetmNormalcoordInPes() const                       {return mNormalcoordInPes;}
      const vector<Nb>& GetmPes_Grid_Scal() const                    {return mPes_Grid_Scal;}
      const In& GetmPesGridMaxDim() const                            {return mPesGridMaxDim;}
      const In& GetmPesGridInitialDim() const                        {return mPesGridInitialDim;}
      const std::vector<In>& GetmAnalyzeStates() const               {return mAnalyzeStates;}
      In GetmDumpSpInterval() const                                  {return mDumpSpInterval;}
      In GetmCutMesh() const                                         {return mCutMesh;}
      Nb GetmPesStepSizeInAu() const                                 {return mPesStepSizeInAu;}
      bool GetmPesVibPol() const                                     {return mPesVibPol;}
      const set<string>& GetmList_Of_Hessians() const                {return mList_Of_Hessians;}
      const bool& GetmPesTaylor() const                              {return mPesTaylor;}
      const bool& GetmPesStatic() const                              {return mPesStatic;}
      const bool& GetmPesAdga() const                                {return mPesAdga;}
      const std::vector<std::string>& GetmAdgaVscfNames() const       {return mAdgaVscfNames;}
      bool GetmDynamicAdgaExt() const                                {return mDynamicAdgaExt;}
      bool GetmDoExtInFirstIter() const                              {return mDoExtInFirstIter;}
      bool GetmCalcDensAtEachMcl() const                             {return mCalcDensAtEachMcl;}
      bool GetmDoAdgaExtAtEachMcl() const                            {return mDoAdgaExtAtEachMcl;}
      bool GetmUpdMeanDensAtEachMcl() const                          {return mUpdMeanDensAtEachMcl;}
      const In& GetmPesGLQuadPnts() const                            {return mPesGLQuadPnts;}
      const In& GetmPolyInterPoints() const                          {return mPolyInterPoints;}
      bool GetmPesDiatomic() const                                   {return mPesDiatomic;}
      bool GetmUseSym() const                                        {return mUseSym;}      
      string GetmPesSymLab() const                                   {return mPesSymLab;}
      SymmetryThresholds& PointGroupSymThrs()                        {return mPointGroupSymThrs;}
      const SymmetryThresholds& PointGroupSymThrs() const            {return mPointGroupSymThrs;}
      std::vector<Uin> GetNThreads() const                           {return mNThreads;}
      string GetCurrESProg() const                                   {return mPesNumSource[mCurrentESProg];}
      In GetmPesNumDalton() const                                    {return mPesNumDalton;}
      In GetmPesNumAces() const                                      {return mPesNumAces;}
      In GetmPesNumNucInp() const                                    {return mPesNumNucInp;}
      In GetmPesNumEsp() const                                       {return mPesNumEsp;}
      In GetmPesNumBs() const                                        {return mPesNumBs;}
      const vector<string>& GetmPesNumSource() const                 {return mPesNumSource;}   
      const vector<In>& GetmPesGridInMCLevel() const                 {return mPesGridInMCLevel;}
      In GetmPesNumMCR() const                                       {return mPesNumMCR;}
      const vector<string>& GetmPesFracInMCLevel() const             {return mPesFracInMCLevel;}
      const bool& GetmUseMultiStateAdga() const                      {return mUseMultiStateAdga;}
      const vector<In>& GetmPesAdaptiveProps() const                 {return mPesAdaptiveProps;}
      const vector<Nb>& GetmPesAdaptivePropsWeights() const          {return mPesAdaptivePropsWeights;}
      std::string GetmPesBarFileStorageType() const                  {return mPesBarFileStorageType;}
      string GetmPesNumSourceElement(In aIn) const                   { if(aIn < mPesNumSource.size()) return mPesNumSource[aIn]; else return "";}
      bool GetmPesModelPot() const                                   {return mPesModelPot;}
      bool GetmPesMultiLevelDone() const                             {return mPesMultiLevelDone;}
      In GetmPesIterMax() const                                      {return mPesIterMax;}
      In GetmPesItGridExpScalFact() const                            {return mPesItGridExpScalFact;}
      bool GetmSaveItOperFile() const                                {return mSaveItOperFile;}
      const bool& GetmSaveSpScratchDir() const                       {return mSaveSpScratchDir;}
      const bool& GetmSavePscGeomScrDir() const                      {return mSavePscGeomScrDir;}
      bool GetmSaveDispGeom() const                                  {return mSaveDispGeom;}
      const std::string& GetSaveDispGeomFormat() const               {return mSaveDispGeomFormat;}
      bool GetmDoNotSaveEspOut() const                               {return mDoNotSaveEspOut;}
      bool GetmPesGrid() const                                       {return mPesGrid;}
      bool GetmPesCalcMuTens() const                                 {return mPesCalcMuTens;}
      bool GetmPesCalcCoriolisAndInertia() const                     {return mPesCalcCoriolisAndInertia;}
      bool GetmPesPropInMem() const                                  {return mPesPropInMem;}
      bool GetmPesDoExtrap() const                                   {return mPesDoExtrap;}
      const bool& GetmUseExtMcr() const                              {return mUseExtMcr;}
      const std::set<In>& GetmExtMcrModes() const                    {return mExtMcrModes;}
      const std::vector<Nb>& GetmPscMaxGridBounds() const            {return mPscMaxGridBounds;}
      bool GetmMeanDensOnFiles() const                               {return mMeanDensOnFiles;}
      bool GetmPesShepardInt() const                                 {return mPesShepardInt;}
      In GetmPesShepardOrd() const                                   {return mPesShepardOrd;}
      bool GetmPesAdgaMsiCut() const                                 {return mPesAdgaMsiCut;}
      const vector<In>& GetmPesAdgaMsiIt() const                     {return mPesAdgaMsiIt;}
      bool GetmPesPlotAdga2D() const                                 {return mPesPlotAdga2D;}
      const vector<string>& GetmMakeCutAnalysis() const              {return mMakeCutAnalysis;}
      bool GetmCartesiancoordInPes() const                           {return mCartesiancoordInPes;}
      const bool& GetmFalconInPes() const                            {return mFalconInPes;}
      const bool& GetmPscInPes() const                               {return mPscInPes;}
      const std::string& GetmTanaRunFile() const                     {return mTanaRunFile;}
      bool GetmPesDoLinearComb() const                               {return mPesDoLinearComb;}
      bool GetmZmatInPes() const                                     {return mZmatInPes;}
      const std::string& GetmAnalyzeDensMethod() const               {return mAnalyzeDensMethod;}
      const bool& GetmPesUseMaxDens() const                          {return mPesUseMaxDens;}
      const bool& GetmPesUseMeanDens() const                         {return mPesUseMeanDens;}
      In GetmExtrapolateTo() const                                   {return mExtrapolateTo;}
      bool GetmPesCalcPotFromOpFile() const                          {return mPesCalcPotFromOpFile;}
      const vector<Nb>& GetmPesLinearComb() const                    {return mPesLinearComb;}
      const vector<extended_grid_t>& GetExtendedGridSettings() const {return mExtendedGridSettings;}
      const std::string& GetSpScratchDirPrefix() const               {return mSpScratchDirPrefix;}
      const std::string& GetMidasIfcPropInfo() const                 {return mMidasIfcPropInfo;}
      const bool& GetmDincrSurface() const                           {return mDincrSurface;}
      const std::string& GetmDincrMethod() const                     {return mDincrMethod;} 
      const std::string& GetmFcInfo() const                          {return mFcInfo;}
      const std::string& GetmDincrSurfaceType() const                {return mDincrSurfaceType;}
      const std::vector<Nb>& GetmIcModesScreenThr() const            {return mIcModesScreenThr;}

      bool GetmSaveTrainData() const                                 {return mSaveTrainData;}
      std::string GetmMLDriver() const                               {return mMLDriver;}
      const In GetMaxMLRuns() const                                  {return mMaxMLRuns;}
      const bool GetMLRemoveInitSet() const                          {return mMLRemoveInitSet;}
 
      //Set x functions
      void SetmPesFitBasisName(const std::string& aStr)        {mPesFitBasisName = aStr;}
      void SetmPesScalCoordInFit(bool aBool)                   {mPesScalCoordInFit = aBool;}
      void SetmSpecBas(bool aBool)                             {mSpecBas = aBool;}
      void SetmAddPsuedoPotToEn(bool aBool)                    {mAddPsuedoPotToEn = aBool;}
      void SetmGMatDiagonal(bool aBool)                        {mGMatDiagonal = aBool;}
      void SetmGMatConst(bool aBool)                           {mGMatConst = aBool;}
      void SetmNoDerivOfGDet(bool aBool)                       {mNoDerivOfGDet = aBool;}
      void AddTomPesNumNucInp()                                {mPesNumNucInp++;}
      void AddTomPesNumDalton()                                {mPesNumDalton++;}
      void SetmCurrentESProg(In aIn)                           {mCurrentESProg = aIn;}
      void SetmNormalcoordInPes(bool aBool)                    {mNormalcoordInPes = aBool;}
      void SetmCartesiancoordInPes(bool aBool)                 {mCartesiancoordInPes = aBool;}
      void SetmZmatInPes(bool aBool)                           {mZmatInPes = aBool;}
      void SetmFalconInPes(const bool& aBool)                  {mFalconInPes = aBool;}
      void SetmPscInPes(const bool& aBool)                     {mPscInPes = aBool;}
      void SetmTanaRunFile(const std::string& aStr)            {mTanaRunFile = aStr;}
      void PushBackmPesUserDefinedBound(Nb aNb)                {mPesUserDefinedBound.push_back(aNb);}
      void PushBackmMakeCutAnalysis(string aString)            {mMakeCutAnalysis.push_back(aString);}
      void SetmCutMesh(string aString)                         {istringstream input_s(aString);
                                                                input_s >> mCutMesh;}
      void SetmPesAvoidRestart(bool aBool)                     {mPesAvoidRestart = aBool;}
      void SetmPesDispTypeSIMPLE()                             {mPesDispType = "SIMPLEDISPLACEMENT";}
      void SetmPesDispTypeFREQ()                               {mPesDispType = "FREQDISPLACEMENT";}
      void SetmPesDispType(string aString)                     {mPesDispType = aString;}
      void SetmPesStepSizeInAu(string aString)                 {istringstream input_s(aString);
                                                                input_s >> mPesStepSizeInAu;}
      void AddmPesNumBs()                                      {mPesNumBs++;}
      void SetTaylorInfo(string aString)                       {istringstream input_s(aString);
                                                                input_s >> mPesNumAnalyticOrder >> mPesNumMCR >> mPesNumMaxPolOrExp;}
      void SetmPesNumMCR(string aString)                       {istringstream input_s(aString);
                                                                input_s >> mPesNumMCR;}
      void SetmPesNumMCR(const In& arIn)                       {mPesNumMCR=arIn;}
      void PushBackmPesGridInMCLevel(In aIn)                   {mPesGridInMCLevel.push_back(aIn);}
      void PushBackmPesFracInMCLevel(string aString)           {mPesFracInMCLevel.push_back(aString);}
      void SetmNumDerFreqThres(string aString)                 {istringstream input_s(aString);
                                                                input_s >> mNumDerFreqThres;}
      void SetmPesDiatomic(bool aBool)                         {mPesDiatomic = aBool;}
      void InsertmList_Of_Hessians(string aString)             {mList_Of_Hessians.insert(aString);}
      void SetmPesVibPol(bool aBool)                           {mPesVibPol = aBool;}
      void SetmPesExcState(string aString)                     {istringstream input_s(aString);
                                                                input_s >> mPesExcState;}
      void SetmSaveItOperFile(bool aBool)                      {mSaveItOperFile = aBool;}
      void SetmSaveSpScratchDir(const bool& aBool)             {mSaveSpScratchDir = aBool;}
      void SetmSavePscGeomScrDir(const bool& aBool)            {mSavePscGeomScrDir = aBool;}
      void SetmSaveDispGeom(bool aBool)                        {mSaveDispGeom = aBool;}
      void SetSaveDispGeomFormat(const std::string& aFormat)   {mSaveDispGeomFormat = aFormat; }
      void SetmSaveTrainData(bool aBool)                       {mSaveTrainData = aBool;}
      void SetmMLDriver(std::string aStr)                      {mMLDriver = aStr;}
      void SetmDoNotSaveEspOut(bool aBool)                     {mDoNotSaveEspOut = aBool;}
      void SetmDoNotSaveInpFiles(bool aBool)                   {mDoNotSaveInpFiles = aBool;}
      void SetmPesGrid(bool aBool)                             {mPesGrid = aBool;}
      void SetmUseSym(bool aBool)                              {mUseSym = aBool;}
      void SetmPesGridMaxDim(const In& aIn)                    {mPesGridMaxDim = aIn;}
      void SetmPesGridInitialDim(const In& aIn)                {mPesGridInitialDim = aIn;}
      void SetmAnalyzeStates(const std::vector<In>& aVec)      {mAnalyzeStates = aVec;}
      void SetmDumpSpInterval(std::string aString)             {istringstream input_s(aString);
                                                                input_s >> mDumpSpInterval;}
      void ClearmPes_Grid_Scal()                               {mPes_Grid_Scal.clear();}
      void ReservemPes_Grid_Scal(In aIn)                       {mPes_Grid_Scal.reserve(aIn);}
      void PushBackmPes_Grid_Scal(Nb aNb)                      {mPes_Grid_Scal.push_back(aNb);}
      void SetmIntType(const std::string& aStr)                {mIntType = aStr;}
      void SetmIntSurface(const std::string& aStr)             {mIntSurface = aStr;}
      void SetmPesDoInt(bool aBool)                            {mPesDoInt = aBool;}
      void SetmPesSplineInt(bool aBool)                        {mPesSplineInt = aBool;}
      void SetmShepardParms(const std::vector<In> aVec)
      {
         mPesShepardOrd = aVec[I_0];
         mShepardExp = aVec[I_1];
      }
      void SetmSplineType(SPLINEINTTYPE aType)
      {
         if (mSplineType == SPLINEINTTYPE::NONE)
         {
            mSplineType = aType;
         }
         else
         {
            MIDASERROR("Input error... ## SplineInt is set twice");
         }
      }
      void SetmSplineTypeFromString(const std::string& aStr)     
      {
         SetmSplineType(midas::input::FindKeyword(SPLINEINTTYPE_MAP::STRING_TO_ENUM,aStr));
         
         if (mSplineType == SPLINEINTTYPE::ERROR)
         {
            MIDASERROR("Type of spline for interpolation is not valid");
         }
      }
      void SetmPesDoIntForExtrap(bool aBool)                   {mPesDoIntForExtrap = aBool;}
      void SetmPesSplineIntForExtrap(bool aBool)               {mPesSplineIntForExtrap = aBool;}
      void SetmPesShepardInt(bool aBool)                       {mPesShepardInt = aBool;}
      void SetmPesShepardIntForExtrap(bool aBool)              {mPesShepardIntForExtrap = aBool;}
      void PushBackmPesFGMesh(In aIn)                          {mPesFGMesh.push_back(aIn);}
      void ClearmPesFGScalFact()                               {mPesFGScalFact.clear();}
      void PushBackmPesFGScalFact(Nb aNb)                      {mPesFGScalFact.push_back(aNb);}
      
      void SetmPesPlotAll(bool aBool)                          {mPesPlotAll = aBool;}
      void SetmPesPlotRealESP(bool aBool)                      {mPesPlotRealESP = aBool;}
      void SetmPesPlotSelected(bool aBool)                     {mPesPlotSelected = aBool;}
      void PushBackmPesPlotSelectedInfo(string aString)        {mPesPlotSelectedInfo.push_back(aString);}
      void SetmPesFreqScalCoordInFit(bool aBool)               {mPesFreqScalCoordInFit = aBool;}
      void SetmPesCalcMuTens(bool aBool)                       {mPesCalcMuTens = aBool;}
      void SetmPesCalcCoriolisAndInertia(bool aBool)           {mPesCalcCoriolisAndInertia = aBool;}
      void SetmPesInertialFrame(bool aBool)                    {mPesInertialFrame = aBool;}
      void SetmPesTaylor(const bool& aBool)                    {mPesTaylor = aBool;}
      void SetmPesStatic(const bool& aBool)                    {mPesStatic = aBool;}
      void SetmPesAdga(const bool& aBool)                      {mPesAdga = aBool;}
      void SetmAdgaVscfNames(const std::vector<std::string>& aVec) {mAdgaVscfNames = aVec;}
      void SetmDynamicAdgaExt(bool aBool)                      {mDynamicAdgaExt = aBool;}
      void SetmDoExtInFirstIter(bool aBool)                    {mDoExtInFirstIter = aBool;}
      void SetmCalcDensAtEachMcl(bool aBool)                   {mCalcDensAtEachMcl = aBool;}
      void SetmDoAdgaExtAtEachMcl(bool aBool)                  {mDoAdgaExtAtEachMcl = aBool;}
      void SetmUpdMeanDensAtEachMcl(bool aBool)                {mUpdMeanDensAtEachMcl = aBool;}
      void SetmPesGLQuadPnts(std::string aString)              {istringstream input_s(aString);
                                                                input_s >> mPesGLQuadPnts;}
      void SetmPolyInterPoints(const In& aIn)                  {mPolyInterPoints = aIn;}                                                          
      void SetmPesIterMax(string aString)                      {istringstream input_s(aString);
                                                                input_s >> mPesIterMax;}
      void SetmAnalyzeDensMethod(const std::string& aStr)      {mAnalyzeDensMethod = aStr;}
      void SetmPesUseMaxDens(const bool& aBool)                {mPesUseMaxDens = aBool;}
      void SetmPesUseMeanDens(const bool& aBool)               {mPesUseMeanDens = aBool;}
      void SetmPesItPotThr(string aString)                     {istringstream input_s(aString);
                                                                input_s >> mPesItPotThr;}
      void SetmPesItDensThr(string aString)                    {istringstream input_s(aString);
                                                                input_s >> mPesItDensThr;}
      void SetmPesItVdensThr(string aString)                   {istringstream input_s(aString);
                                                                input_s >> mPesItVdensThr;}
      void SetmPesItNormVdensThr(string aString)               {istringstream input_s(aString);
                                                                input_s >> mPesItNormVdensThr;}
      void SetmPesItResEnThr(string aString)                   {istringstream input_s(aString);
                                                                input_s >> mPesItResEnThr;}
      void SetmPesItNormResEnThr(string aString)               {istringstream input_s(aString);
                                                                input_s >> mPesItNormResEnThr;}
      void SetmPesItResDensThr(string aString)                 {istringstream input_s(aString);
                                                                input_s >> mPesItResDensThr;}
      void SetmAdgaRelConvScalFacts(const std::vector<Nb>& aVec)  
      {
         mAdgaRelConvScalFacts.clear();
         mAdgaRelConvScalFacts = aVec;
      }
      void SetmAdgaAbsConvScalFacts(const std::vector<Nb>& aVec)
      {
         mAdgaAbsConvScalFacts.clear();
         mAdgaAbsConvScalFacts = aVec;
      }
      void SetmPesUseItPotThr(bool aBool)                      {mPesUseItPotThr = aBool;}
      void SetmPesUseItDensThr(bool aBool)                     {mPesUseItDensThr = aBool;}
      void SetmPesUseItVdensThr(bool aBool)                    {mPesUseItVdensThr = aBool;}
      void SetmPesUseItNormVdensThr(bool aBool)                {mPesUseItNormVdensThr = aBool;}
      void SetmPesCalcPotFromOpFile(bool aBool)                {mPesCalcPotFromOpFile = aBool;}
      void SetmPesAdaptivSearch(bool aBool)                    {mPesAdaptivSearch = aBool;}
      void SetmPesNmaxSplittings(std::string aString)          {istringstream input_s(aString);
                                                                input_s >> mPesNmaxSplittings;}
      void SetmUseMultiStateAdga(const bool& aBool)            {mUseMultiStateAdga = aBool;}
      void SetmPesAdaptiveProps(const string& aString)            {mPesAdaptiveProps = util::VectorFromString<In>(aString);}
      void SetmPesAdaptiveProps(const vector<In>& aInVec)         {mPesAdaptiveProps = aInVec;}
   
      void SetmPesAdaptivePropsWeights(const string& aString)     {mPesAdaptivePropsWeights = util::VectorFromString<Nb>(aString);}
      void SetmPesAdaptivePropsWeights(const vector<Nb>& aNbVec)  {mPesAdaptivePropsWeights = aNbVec;}

      void SetmPesBarFileStorageType(std::string aString)      {mPesBarFileStorageType = aString;}
      void SetmPesPlotAdga2D(bool aBool)                       {mPesPlotAdga2D = aBool;}
      void SetmPesModelPot(bool aBool)                         {mPesModelPot = aBool;}
      void SetmPesDoPartridgePot(bool aBool)                   {mPesDoPartridgePot = aBool;}
      void SetmPesDoAmmModelPot(bool aBool)                    {mPesDoAmmModelPot = aBool;}
      void SetmPesType(string aString)                         {istringstream input_s(aString);
                                                                input_s >> mPesType;}
      void SetmPesDoTestPot(bool aBool)                        {mPesDoTestPot = aBool;}
      void SetmPesDoArNtwoModelPot(bool aBool)                 {mPesDoArNtwoModelPot = aBool;}
      void SetmPesDoMorsePot(bool aBool)                       {mPesDoMorsePot = aBool;}
      void SetmPesDoBrennerPot(bool aBool)                     {mPesDoBrennerPot = aBool;}
      void SetmPesMorseDefs(string aString)                    {mPesMorseDefs = aString;}
      void SetmPesDoDoubleWpot(bool aBool)                     {mPesDoDoubleWpot = aBool;}
      void SetmPesDoubleWdefs(string aString)                  {mPesDoubleWdefs = aString;}
      void SetmPesPropInMem(bool aBool)                        {mPesPropInMem = aBool;}
      void SetmPesItGridExpScalFact(string aString)            {istringstream input_s(aString);
                                                                input_s >> mPesItGridExpScalFact;}
      void ClearmPesNumSource()                                {mPesNumSource.clear();}
      void AddTomPesNumCfour()                                 {mPesNumCfour++;}
      void AddTomPesNumAces()                                  {mPesNumAces++;}
      void AddTomPesNumEsp()                                   {mPesNumEsp++;}
      void InsertIntomGenericPes(vector<string> aStringVec)    {mGenericPes.insert(make_pair(mPesNumSource.size(),aStringVec));}
      void SetmPesDoExtrap(bool aBool)                         {mPesDoExtrap = aBool;}

      void SetmUseExtMcr(const bool& aBool)                    {mUseExtMcr = aBool;}
      void SetmExtMcrModes(const std::set<In>& aSet)           {mExtMcrModes = aSet;}
      void SetmPscMaxGridBounds(const std::vector<Nb>& aVec)   {mPscMaxGridBounds = aVec;}
      void SetmMeanDensOnFiles(bool aBool)                     {mMeanDensOnFiles = aBool;}
      void SetmPesBoundariesPreopt(bool aBool)                 {mPesBoundariesPreopt = aBool;}
      void SetmUseExtrapStartGuess(const bool& aBool)          {mUseExtrapStartGuess = aBool;}
      void SetmExtrapolateFrommExtrapolateTo(string aString)   
      {  
         istringstream input_s(aString);
         input_s >> mExtrapolateFrom >> mExtrapolateTo;
      }
      void SetmExtrapOnlyGradient(bool aBool)                  {mExtrapOnlyGradient = aBool;}
      void SetmPotentialFilename(string aString)               {mPotentialFilename = aString;}
      void SetmDoAdgaPreScreen(bool aBool)                     {mPesDoAdgaPreScreen = aBool;} 
      FlexCoupCalcDef GetmPesFlexCoupCalcDef() const           {return mPesFlexCoupCalcDef;}
      void SetmFlexCoupCalcDef(const FlexCoupCalcDef& arFlexCoupCalcDef)    {mPesFlexCoupCalcDef.Reset(arFlexCoupCalcDef);}
      void SetmPesDoLinearComb(bool aBool)                     {mPesDoLinearComb = aBool;}
      void PushBackmPesLinearComb(Nb aNb)                      {mPesLinearComb.push_back(aNb);}
      void SetmPesScreenModeCombi(bool aBool)                  {mPesScreenModeCombi = aBool;}
      void SetmPesScreenMethod(In aIn)                         {mPesScreenMethod = aIn;}
      void SetmPesScreenThresh(string aString)                 {istringstream input_s(aString);
                                                                input_s >> mPesScreenThresh;}
      void SetmPesVibNumVec(const vector<In>& aVec)            {mPesVibNumVec = aVec;}
      void SetmPesSymLab(string aString)                       {mPesSymLab = aString;}
      void SetNThreads(const std::vector<Uin>& aN)             {mNThreads = aN;}
      void SetmPesScreenSymPotTerms(bool aBool)                {mPesScreenSymPotTerms = aBool;}
      void SetmSymThr(string aString)                          {istringstream input_s(aString);
                                                                input_s >> mSymThr;}
      void SetmPesAdgaFgStep(string aString)                   {istringstream input_s(aString);
                                                                input_s >> mPesAdgaFgStep;}
      void SetmPesAdgaMsiCut(bool aBool)                       {mPesAdgaMsiCut = aBool;}

      void SetmPesAdgaMsiIt(const vector<In>& aVec)            {mPesAdgaMsiIt = aVec;}
      
      void SetmPesMsiLambda(string aString)                    {istringstream input_s(aString);
                                                                input_s >> mPesMsiLambda;}
      void SetMidasIfcPropInfo(const std::string& aMidasIfcPropInfo ) { mMidasIfcPropInfo = aMidasIfcPropInfo; }
      void SetmPesMultiLevelDone(bool aBool)                   {mPesMultiLevelDone = aBool;}
      void InsertIntomPesPlotSelectedMap(vector<In> aVector, string aString)   {mPesPlotSelectedMap[aVector] = aString;}
      const std::string& GetSinglePointName(In i) const 
      { 
         MidasAssert(mSinglePointNames.size() > i,"Requested SinglePointInfo " + std::to_string(i) + " but i only have " + std::to_string(mSinglePointNames.size()) + " stored.");
         
         return mSinglePointNames.at(i); 
      }
      void AddSinglePoint(const std::string& aSp) { mSinglePointNames.emplace_back(aSp); }

      bool HistoricAdgaPes()      const { return mHistoricAdgaScaling && mHistoricAdgaConv && mHistoricAdgaGrid; }  ///< historic calculation???
      bool HistoricAdgaScaling()  const { return mHistoricAdgaScaling; }  ///< historic scaling before fitting 
      bool HistoricAdgaConv()     const { return mHistoricAdgaConv; }  ///< historic absolute convergence check
      bool HistoricAdgaGrid()     const { return mHistoricAdgaGrid; }  ///< historic adga starting grid
      bool HistoricPesNumSource() const { return mHistoricPesNumSource; }    ///< historic pes num source (i.e. old 1 'multi'-level calculation)
      bool BoundariesPreOpt()     const { return mPesBoundariesPreopt; }     ///< check if we are doing preopt of boundaries
      In MultiLevel()             const { return mSinglePointNames.size(); } ///< get number of multilevels
      const std::vector<In>& LevelMc() const { return mLevelMc; } ///< get modecombination level for each multilevel
      void SetLevelMc(const std::vector<In>& vec)  {mLevelMc.clear(); mLevelMc=vec ;} ///< get modecombination level for each multilevel
      void SetSinglePointNames(const std::vector<std::string>& vec)  {mSinglePointNames.clear(); mSinglePointNames=vec ;} ///< get modecombination level for each multilevel
      void SetmDincrSurface(const bool& aBool)                       {mDincrSurface = aBool;}
      void SetmDincrMethod(const std::string& aStr)                  {mDincrMethod = aStr;} 
      void SetmFcInfo(const std::string& aStr)                       {mFcInfo = aStr;}
      void SetmIcModesScreenThr(const std::vector<Nb>& aVec)         {mIcModesScreenThr = aVec;}
      void SetmDincrSurfaceType(const std::string& aStr); 
      
      bool ScreenOperCoef()       const { return mScreenOperCoef; }
      Nb ScreenOperCoefThresh()   const { return mScreenOperCoefThresh; }

      Nb   GetNormalCoordinateThreshold() const                        { return mNormalCoordinateThreshold; }
      void SetNormalCoordinateThreshold(Nb aNormalCoordinateThreshold) { mNormalCoordinateThreshold = aNormalCoordinateThreshold; }
      bool GetNormalizeNormalCoordinates() const                           { return mNormalizeNormalCoordinates; }
      void SetNormalizeNormalCoordinates(bool aNormalizeNormalCoordinates) { mNormalizeNormalCoordinates = aNormalizeNormalCoordinates; }
      bool GetOrthoNormalizeNormalCoordinates() const                           { return mOrthoNormalizeNormalCoordinates; }
      void SetOrthoNormalizeNormalCoordinates(bool aOrthoNormalizeNormalCoordinates) { mOrthoNormalizeNormalCoordinates = aOrthoNormalizeNormalCoordinates; }

      void SetAdgaUseHistoricNonSopIntegrals(bool aVal) {mAdgaUseHistoricNonSopIntegrals = aVal;}
      bool GetAdgaUseHistoricNonSopIntegrals() const {return mAdgaUseHistoricNonSopIntegrals;}

      
      bool ReadExtendedGridSettings(std::istream& Minp, std::string& s);
      bool ReadMultiLevelPes(std::istream& Minp, std::string& s);
      bool ReadBoundariesPreOpt(std::istream& Minp, std::string& s);
      bool ReadHistoricAdgaPes(std::istream& Minp, std::string& s);
      bool ReadHistoricAdgaScaling(std::istream& Minp, std::string& s);
      bool ReadHistoricAdgaConv(std::istream& Minp, std::string& s);
      bool ReadHistoricAdgaGrid(std::istream& Minp, std::string& s);
      bool ReadHistoricPesNumSource(std::istream& Minp, std::string& s);
      bool ReadScreenOperCoef(std::istream& Minp, std::string& s);
      bool ReadSpScratchDirPrefix(std::istream& Minp, std::string& s);

      In IoLevel() const { return gPesIoLevel; } 

      void SetMaxMLRuns(In& aMaxMLRuns) {mMaxMLRuns = aMaxMLRuns;}
      void SetMLRemoveInitSet(const bool& aMLRemoveInitSet) {mMLRemoveInitSet = aMLRemoveInitSet;}
};

//@{
//! Whether to evaluate SOP/non-SOP quantities for determining ADGA convergence.
bool AdgaConvergenceFromSumOverProductQuantities(const PesCalcDef&);
bool AdgaConvergenceFromNonSumOverProductQuantities(const PesCalcDef&);
//@}

#endif /* PESCALCDEF_H */
