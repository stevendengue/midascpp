/**
************************************************************************
* 
* @file                TotalResponseDrv.cc
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   TotalResponseDrv datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;

#include "inc_gen/math_link.h"

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "input/TotalResponseDrv.h"
#include "input/TotalLinRsp.h"
#include "input/TotalQuadRsp.h"
#include "input/TotalCubicRsp.h"
#include "input/Contribution.h"
#include "input/TotalResponseContribution.h"
#include "input/GlobalOperatorDefinitions.h"
#include "input/OpDef.h"

//debugging macro
//#define DBG(ARG) ARG
#define DBG(ARG) void(0);

/**
* Constructor, no action yet...
* */
TotalResponseDrv::TotalResponseDrv(const std::map<string,string>& aMap)
{
   mName="";
}

/**
* Factory for generating total response functions
**/
TotalResponseDrv* TotalResponseDrv::Factory(const In aOrder, const std::map<string,string>& aMap)
{
   switch(aOrder)
   {
      case I_2:
         return new TotalLinRsp(aMap);
      case I_3:
         return new TotalQuadRsp(aMap);
      case I_4:
         return new TotalCubicRsp(aMap);
      default:
         MIDASERROR("Only Linear (order=2), Quadratic (order=3), and cubic (order=4)\nimplemented at this point."); 
   }
   return NULL;
}

void TotalResponseDrv::Evaluate()
{
   Mout << "Evaluate the total response function: " << endl << *this << endl;
   ConstructIntermediateSet();
   // First we let the realizations provide a map
   // of string -> TotalResponseContribution
   map<string,TotalResponseContribution> total_map=ProvideMap();
   // then we print out all the individual
   // TotalResponseContributions
   map<string,TotalResponseContribution>::iterator it;
   for(it=total_map.begin();it!=total_map.end();it++) {
      Mout << "Total Response function: " << it->first << endl;
      Mout << it->second << endl;
   }
   // then we take a look at the total rsp. strings => experimental
   // quantities
   for(In i=0;i<mTotalRspString.size();i++) {
      // 1) convert to a string of numbers
      string s=StringToExpression(mTotalRspString[i],total_map,false);
      // 2) Evaluate this string
      Nb e_v=ConvertAlgebraStringToNb(s);
      // do the same for just the Eq.
      s=StringToExpression(mTotalRspString[i],total_map,true);
      Nb eq=ConvertAlgebraStringToNb(s);
      Mout << "Total experimental quantity:" << endl << mTotalRspString[i] << endl;
      vector<Nb> frq_vec=GetFrqVec();
      for(In j=0;j<frq_vec.size();j++)
         Mout << " Frq[" << j+1 << "] = " << frq_vec[j] << endl;
      Mout << " ZPVC        = " << e_v-eq << endl;
      Mout << " Equilibrium = " << eq << endl;
      Mout << " => ZPVA     = " << e_v << endl;
      // average of the individiual types, e.g.
      // \gamma_{||}^{[\alpha^2]}.
      ReportAllTypes(mTotalRspString[i],total_map);
   }
}

string TotalResponseDrv::StringToExpression(const string& aS,
   const map<string,TotalResponseContribution>& aM,bool aJustEq)
{
   string s=aS;
   while(s.find_first_of("XYZ")!=s.npos) {
      In start=s.find_first_of("XYZ");
      In end=s.find_first_not_of("XYZ",start);
      In len=end-start;
      DBG(Mout << "string: " << s << " start = " << start << " end = " << end << endl;)
      string func=s.substr(start,len);
      // now look in aM for this string
      map<string,TotalResponseContribution>::const_iterator it=aM.find(func);
      if(it==aM.end())
         MIDASERROR("Function not found: "+func+" CANNOT HAPPEN!");
      Nb value;
      if(aJustEq)
         value=(it->second).GetTotalEqValue();
      else
         value=(it->second).GetTotalValue();
      s.replace(start,len,StringForNb(value,22));
      DBG(Mout << "Now s is: " << s << endl;)
   }
   return s;
}

/**
*  Convert a string to response functions and return those
**/
set<string> TotalResponseDrv::ConvertStringToFunctions(string& aS)
{
   /* delimiters are:   "+","-","*"
      functions are:    "sqrt","^2"
      fractions:        "n/m" - discarded here...
   */
   set<string> result;
   string copy=aS;
   while(copy.find_first_not_of("XYZ ")!=copy.npos) {
      copy.replace(copy.find_first_not_of("XYZ "),I_1," ");
   }
   DBG(Mout << "string before split: " << copy << endl;)
   vector<string> basics=SplitString(copy," ");
   for(In i=0;i<basics.size();i++) {
      DBG(Mout << "Will do: " << basics[i] << endl;)
      if(basics[i].size()==0) {
         DBG(Mout << "I found empty..." << endl;)
         continue;
      }
      result.insert(basics[i]);
   }
   return result;
}

void TotalResponseDrv::ReportAllTypes(const string& aS,map<string,TotalResponseContribution> aM)
{
   vector<string> types=(aM.begin()->second).GetTypes();
   vector<string> rsp_s(types.size(),aS);
   for(In i=0;i<types.size();i++) {
      while(rsp_s[i].find_first_of("XYZ")!=rsp_s[i].npos) {
         In start=rsp_s[i].find_first_of("XYZ");
         In end=rsp_s[i].find_first_not_of("XYZ",start);
         In len=end-start;
         DBG(Mout << "string: " << rsp_s[i] << " start = " << start << " end = " << end << endl;)
         string func=rsp_s[i].substr(start,len);
         // now look in aM for this string
         map<string,TotalResponseContribution>::const_iterator it=aM.find(func);
         if(it==aM.end())
            MIDASERROR("Function not found: "+func+" CANNOT HAPPEN!");
         Nb value=(it->second).GetTerm(types[i]);
         rsp_s[i].replace(start,len,StringForNb(value,22));
         DBG(Mout << "Now s is: " << rsp_s[i] << endl;)
      }
   }
   // all types are done, print values
   Mout << "INDIVIDUAL COMPONENTS:" << endl;
   for(In i=0;i<types.size();i++) {
      Mout << types[i] << " = " << ConvertAlgebraStringToNb(rsp_s[i]) << endl;
   }
}

Nb TotalResponseDrv::ConvertAlgebraStringToNb(string& aS)
{
   // evaluate parentheses first, build list of positions
   DBG(Mout << "At entry (ConvertAlgebraStringToNb): " << aS << endl;)
   map<In,In> par_map;
   string s_copy=aS;
   In level=I_0;
   EvalParentheses(level,I_0,s_copy);
   Nb n=EvaluateSimple(s_copy);
   DBG(Mout << "At return (ConvertAlgebraStringToNb): " << n << endl;)
   return n;
}

void TotalResponseDrv::EvalParentheses(In& aLevel,In aStart,string& aS)
{
   //DBG(Mout << "String is: " << aS << " (level " << aLevel << ")" << endl;)
   In i=aS.find_first_of("()",aStart);
   In begin_level=aLevel;
   if(aLevel==0)
      aStart=i;
   while(i!=aS.npos)
   {
      if(aS.at(i)=='(') {
         ++aLevel;
         EvalParentheses(aLevel,i+1,aS);
         return;
      }
      else {
         --aLevel;
         // if next is: "^" then add that to string
         // must be in "()", as: "^(2)"
         if(aS.size()>i+1) {
            if(aS.at(i+1)=='^') {
               i=aS.find(")",i+1);
            }
         }
         Nb n=EvaluateSimple(aS.substr(aStart-1,i-aStart+2));
         aS.replace(aStart-1,i-aStart+2,StringForNb(n,22));
         DBG(Mout << "In else, string: " << aS << endl;)
         // reset and return
         aLevel=0;
         EvalParentheses(aLevel,0,aS);
         return;
      }
   }
}

Nb TotalResponseDrv::EvaluateSimple(string aS)
{
   vector<string> pattern;
   pattern.push_back("sqrt");
   pattern.push_back("*");
   pattern.push_back("/");
   pattern.push_back("-");
   pattern.push_back("+");
   pattern.push_back(")^(");
   pattern.push_back("^(");
   string new_s=aS;
   Nb result=C_0;
   // remove all blanks
   while(new_s.find(" ")!=new_s.npos) {
      new_s.erase(new_s.find(" "),I_1);
   }
   for(In i=0;i<pattern.size();i++) {
      In match=I_0;
      while(new_s.find(pattern[i])!=new_s.npos) {
         DBG(Mout << "String is before: " << new_s << endl;)
         match=new_s.find(pattern[i],match+1);
         if(match==new_s.npos)
            break;
         // to the left of match, we should encounter 0123456789
         In left_begin,right_end;
         string left_sn,right_sn;
         if(pattern[i]!="sqrt") {
            // look for relational operator => [0123456789] to the
            // left and [-0123456789] on the right
            string ss_l=new_s.substr(match-I_1,I_1);
            string ss_r=new_s.substr(match+pattern[i].size(),I_1);
            if(ss_l.find_first_of("0123456789")!=ss_l.npos &&
               ss_r.find_first_of("-0123456789")!=ss_r.npos) {
               left_begin=FindLeftNumber(new_s,match);
               right_end=FindRightNumber(new_s,match);
               DBG(Mout << "Match, left_begin, right_end = (" << match
                    << "," << left_begin << "," << right_end << ")" << endl;)
               left_sn=new_s.substr(left_begin,match-left_begin);
               right_sn=new_s.substr(match+pattern[i].size(),right_end-match-pattern[i].size()+I_1);
            }
            else
               continue;
         }
         else {
            // only find right number
            right_end=FindRightNumber(new_s,match);
            right_sn=new_s.substr(match+pattern[i].size(),right_end-match-pattern[i].size()+I_1);
            left_begin=match;
         }
         DBG(Mout << "Left number is : " << left_sn << endl;)
         DBG(Mout << "Right number is: " << right_sn << endl;)
         if(pattern[i]!="sqrt")
            result=SymbolicMathOperation(pattern[i],atof(left_sn.c_str()),atof(right_sn.c_str()));
         else
            result=SymbolicMathOperation(pattern[i],atof(right_sn.c_str()));
         // now replace everything from left_begin to right_end by result
         new_s.replace(left_begin,right_end-left_begin+I_1,StringForNb(result,22));
         DBG(Mout << "String is after:  " << new_s << endl;)
         match=I_0;
      }
   }
   return result;
}

In TotalResponseDrv::FindLeftNumber(string aS,In aI)
{
   // locate begin of left number
   string local_copy=aS;
   In result=aI;
   bool success=false;
   while(!success) {
      result--;
      if(result<I_0)
         return I_0;
      string ss=local_copy.substr(result,I_1);
      if(ss.find_first_of("01234567890")!=ss.npos)
         continue;
      if(ss.find_first_of("+-")!=ss.npos) {
         // check for Ee
         if(local_copy.at(result-1)=='E' || local_copy.at(result-1)=='e')
            continue;
         else {
            // check for negative number
            if(ss=="+")
               result++;
            else {
               string ss1=local_copy.substr(result-I_1,I_1);
               if(ss1.find_first_of("0123456789")!=ss1.npos)
                  result++;
            }
            success=true;
         }
      }
      if(ss=="(") {
         result++;
         success=true;
      }
   }
   return result;
}

In TotalResponseDrv::FindRightNumber(string aS,In aI)
{
   // locate begin of left number
   string local_copy=aS;
   In result=aI;
   bool success=false;
   while(!success) {
      result++;
      if(result>=local_copy.size())
         return result--;
      string ss=local_copy.substr(result,I_1);
      if(ss.find_first_of("01234567890")!=ss.npos)
         continue;
      if(ss.find_first_of("+-")!=ss.npos) {
         // check for negative number
         if(result==(aI+I_1))
            continue;
         // check for Ee
         if(local_copy.at(result-1)=='E' || local_copy.at(result-1)=='e')
            continue;
         // check for end
         string ss1=local_copy.substr(result-I_1,I_1);
         if(ss1.find_first_of("0123456789")!=ss1.npos) {
            result--;
            success=true;
         }
      }
      if(ss==")") {
         result--;
         success=true;
      }
   }
   return result;
}

Nb TotalResponseDrv::SymbolicMathOperation(const string& aS, Nb n1, Nb n2)
{

   enum types{ERROR,PLUS,MINUS,MULT,DIVIDE,SQRT,POW};
   map<string,In> types_map;
   types_map["+"]=PLUS;
   types_map["-"]=MINUS;
   types_map["*"]=MULT;
   types_map["/"]=DIVIDE;
   types_map["sqrt"]=SQRT;
   types_map["^"]=POW;
   types_map[")^("]=POW;

   string s_copy=aS;
   while(s_copy.find(" ")!=s_copy.npos)
      s_copy.erase(s_copy.find(" "),I_1);
   transform(s_copy.begin(),s_copy.end(),s_copy.begin(),(In(*) (In))tolower);

   In operation=types_map[s_copy];
   switch(operation)
   {
      case PLUS:
      {
         return n1+n2;
      }
      case MINUS:
      {
         return n1-n2;
      }
      case MULT:
      {
         return n1*n2;
      }
      case DIVIDE:
      {
         return n1/n2;
      }
      case SQRT:
      {
         return sqrt(n1);
      }
      case POW:
      {
         return pow(n1,n2);
      }
      default:
      {
         MIDASERROR("UNKNOWN OPERATION");
      }
   }
   return C_0;
}

Nb TotalResponseDrv::SearchForEqVal(const string& aS,vector<Nb> aV)
{
   // create opinfo from input and search
   // gOperators for such an oper.
   string info_line="type=";
   if(aS.size()==1) {
      info_line+=aS+"_DIPOLE";
   }
   else if(aS.size()==I_2) {
      info_line+=aS+"_POL frq="+StringForNb(aV[0]);
   }
   else if(aS.size()==I_3) {
      info_line+=aS+"_POL frq1="+StringForNb(aV[0]);
      info_line+=" frq2="+StringForNb(aV[1]);
   }
   else if(aS.size()==I_4) {
      info_line+=aS+"_POL frq1="+StringForNb(aV[0]);
      info_line+=" frq2="+StringForNb(aV[1]);
      info_line+=" frq3="+StringForNb(aV[2]);
   }
   else
      MIDASERROR(aS+" is unknown.");
   OpInfo oi;
   ParseSetInfoLine(info_line,oi);
   // search gOperators
   for(In i=0;i<gOperatorDefs.GetNrOfOpers();i++) {
      OpInfo oi_test=OpDef::msOpInfo[gOperatorDefs[i].Name()];
      if(oi==oi_test)
         return gOperatorDefs[i].GetEqVal();
   }
   return C_0;
}

/**
*  Check aVDef for default values for use in aV
**/
void TotalResponseDrv::CheckForDefaults(set<Contribution>& aV,set<Contribution>& aVDef)
{
   if(aVDef.size()==0)
      return;
   // add default to aV if not there already
   set<Contribution> updated;
   for(set<Contribution>::iterator i=aV.begin();i!=aV.end();i++) {
      set<Contribution>::iterator j=aVDef.find(*i);
      if(j!=aVDef.end())
         updated.insert(*j);
      else
         updated.insert(*i);
   }
   aV=updated;
}

ostream& operator<<(ostream& aOut, const TotalResponseDrv& aRsp)
{
   return aRsp.Print(aOut);
}

//string TotalResponseDrv::NloOperNameForString(string aS,bool aInit,const vector<Nb> *mpFrq)
//{
//   DBG(Mout << "Looking for: \"" << aS << "\"" << endl;)
//   string s=aS;
//   if(aS.size()==I_1)
//      s+="_DIPOLE";
//   if(aS.size()==I_2)
//      s+="_POL";
//   if(aS.size()==I_3)
//      s+="_POL";
//   if(aS.size()==I_4)
//      s+="_POL";
//   for(In i=0;i<gOperatorDefs.GetNrOfOpers();i++)
//   {
//      OpInfo oi=OpDef::msOpInfo[gOperatorDefs[i].Name()];
//      if(StringNoCaseCompare(oi.GetDescr(),s)) {
//         DBG(Mout << "found!" << endl;)
//         return gOperatorDefs[i].Name();
//      }
//   }
//   if(aInit) {
//      // If the operator is not in the set op operators
//      // add it if aInit. Example: when default values are
//      // present.
//      string name=aS;
//      /*
//      if(mpFrq!=0)
//         for(In i=0;i<mpFrq->size();i++)
//            name+="_"+StringForNb((*mpFrq)[i]);
//      */
//      OpDef od;
//      od.SetName(name);
//      Mout << "Adding oper..." << name << endl;
//      gOperatorDefs.AddOperator(od);
//      // set OpInfo
//      string info_string="type="+aS+"_POL";
//      for(In i=0;i<mpFrq->size();i++)
//         info_string+=" frq"+std::to_string(i+1)+"="+StringForNb((*mpFrq)[i]);
//      OpInfo oper_info;
//      ParseSetInfoLine(info_string,oper_info);
//      OpDef::msOpInfo[od.Name()] = oper_info;
//      //return gOperators.back().Name();
//      return gOperatorDefs[gOperatorDefs.GetNrOfOpers()-1].Name();
//   }
//   MIDASERROR("I Did not find operator with info: "+s+", quitting!");
//   return "";
//}
