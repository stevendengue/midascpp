/**
************************************************************************
*
* @file                TdHCalcDef.h
*
* Created:             30-06-2011
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) and Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Contains input-info that defines a Vcc calculation
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef TDHCALCDEF_H
#define TDHCALCDEF_H

// Standard Headers
#include <string>
#include <utility>
#include <set>
#include <vector>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "input/VscfCalcDef.h"
#include "input/TdPropertyDef.h"
#include "ode/OdeInfo.h"

#ifdef VAR_MPI
#include "td/tdh/TdHMpi.h"
#endif /* VAR_MPI */

namespace midas::tdh
{
//! Constraint (g operator) for equation of motion of L-TDH
enum class constraintID : int
{
   ZERO = 0
,  FOCK
};


//! TDH parametrizations
enum class tdhID : int
{
   LINEAR = 0
,  EXPONENTIAL
};

} /* namespace midas::tdh */

/**
* Construct a definition of a TdH calculation
* */
class TdHCalcDef
   :  public TdPropertyDef
{
   public:
      //! Initial wave-packet types
      enum class initType : int
      {  VSCF = 0
      };

   private:
      //! Name of calculation
      std::string mName;

      //! Operator
      std::string mOper;

      //! Basis set
      std::string mBasis;

      //! Transform to spectral basis for initial state before propagation
      bool mSpectralBasis = false;

      //! Type of spectral-basis transform. Empty string will give spectral basis of reference potential.
      std::string mSpectralBasisOper = "";

      //! Limit the modal-basis size
      std::vector<In> mModalBasisLimits;

      //! Policy for initial wave packet
      initType mInitialWavePacket = initType::VSCF;

      //! Name of VSCF calculation for initial modals
      std::string mVscfName = "";

      //! Info on integrator, time interval, etc.
      midas::ode::OdeInfo mOdeInfo;

      //! Do imaginary-time propagation
      bool mImagTime = false;

      //! TDH parametrization
      midas::tdh::tdhID mParametrization = midas::tdh::tdhID::LINEAR;

      //! Constraint operator for linear TDH
      midas::tdh::constraintID mLinearTdHConstraint = midas::tdh::constraintID::ZERO;

      //! Normalize modals in linear TDH
      bool mLinearTdHNormalizeVectors = true;

      //! Calculate individual projections in Exp. TDH
      bool mExpTdHDerivativeProjectionTerms = false;

      //! Norm threshold for exponential TDH
      Nb mKappaNormThreshold = static_cast<Nb>(0.95);

      //! Max failed derivatives for exponential TDH
      In mExpTdHMaxFailedDerivatives = I_1;

      //! Screening threshold based on operator coefficient
      Nb mScreenZeroCoef = C_0;

      //! IO level
      In mIoLevel = I_1;

#ifdef VAR_MPI
      //! MPI-communication method
      midas::tdh::mpiComm mMpiComm = midas::tdh::mpiComm::BCAST;
#endif /* VAR_MPI */

   public:
      //! Constructor
      TdHCalcDef() = default;

      //! Sanity check. Check that input makes sense.
      void SanityCheck
         (
         );

      //! Read OdeInfo
      bool ReadOdeInfo
         (  std::istream&
         ,  std::string&
         );

      //! Read constraint
      bool ReadLinearTdHConstraint
         (  std::istream&
         ,  std::string&
         );

      //! Read policy for initial wave function
      bool ReadInitialWfPolicy
         (  std::istream&
         ,  std::string&
         );

#ifdef VAR_MPI
      //! Read MPI comm
      bool ReadMpiComm
         (  std::istream&
         ,  std::string&
         );
#endif /* VAR_MPI */

      //! Macro for Getter and Setter interface
      template<typename T> struct argument_type;
      template<typename T, typename U> struct argument_type<T(U)> { using type = U; };
      #define GETSET(NAME, TYPE) \
         TdHCalcDef& Set##NAME(const argument_type<void(TYPE)>::type& a##NAME) \
         { \
            m##NAME = a##NAME; \
            return *this; \
         } \
           \
         const argument_type<void(TYPE)>::type& NAME() const \
         { \
            return m##NAME; \
         }
      
         GETSET(Name, std::string);
         GETSET(Basis, std::string);
         GETSET(Oper, std::string);
         GETSET(SpectralBasis, bool);
         GETSET(SpectralBasisOper, std::string);
         GETSET(ModalBasisLimits, std::vector<In>);
         GETSET(OdeInfo, midas::ode::OdeInfo);
         GETSET(ImagTime, bool);
         GETSET(Parametrization, midas::tdh::tdhID);
         GETSET(LinearTdHConstraint, midas::tdh::constraintID);
         GETSET(LinearTdHNormalizeVectors, bool);
         GETSET(KappaNormThreshold, Nb);
         GETSET(IoLevel, In);
         GETSET(ExpTdHMaxFailedDerivatives, In);
         GETSET(ScreenZeroCoef, Nb);
         GETSET(InitialWavePacket, initType);
         GETSET(VscfName, std::string);
         GETSET(ExpTdHDerivativeProjectionTerms, bool);
#ifdef VAR_MPI
         GETSET(MpiComm, midas::tdh::mpiComm);
#endif /* VAR_MPI */
      #undef GETSET

      //! Ostream
      friend std::ostream& operator<<
         (  std::ostream&
         ,  const TdHCalcDef&
         );
};

#endif

