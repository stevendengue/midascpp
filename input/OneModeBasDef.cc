/**
************************************************************************
* 
* @file                OneModeBasDef.cc
*
* Created:             04-10-2002
*
* Author:              D. Toffoli (toffoli@chem.au.dk) 
*
* Short Description:   Contain definition of one-mode Basis
* 
* Last modified: Wed Apr 07, 2010  10:48AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <algorithm>
using std::transform;

#include "inc_gen/math_link.h"

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "input/Input.h"
#include "input/OneModeBasDef.h"
#include "input/OneModeHoDef.h"
#include "input/OneModeGaussDef.h"
#include "input/OneModeStateBasis.h"

typedef string::size_type Sst;

/**
 * Default constructor
**/
OneModeBasDef::OneModeBasDef()
{
   mBasType=""; 
   mIadd=-1;
}

/**
 * Constructor
**/
OneModeBasDef::OneModeBasDef( string aType
                            , In aAdd
                            )
{
   mBasType=aType; 
   mIadd=aAdd;
}

/**
 * get Lower function result for basistype=HO
**/
In OneModeBasDef::Lower() const
{
   In low=-1;
   if (mIadd==-1) MIDASERROR(" wrong address for the specified basis set ");
   if (mBasType=="HO") //this is Ho basis
      return gOneModeHoDefs[mIadd].Lower();
   else
      MIDASERROR(" Functionality doesn't match basis type ");
   return low;  //never gets here
}

/**
 * Higher function result for basistype=HO
**/
In OneModeBasDef::Higher() const
{
   In high=-1;
   if (mIadd==-1) MIDASERROR(" wrong address for the specified basis set ");
   if (mBasType=="HO") //this is Ho basis
      return gOneModeHoDefs[mIadd].Higher();
   else
      MIDASERROR(" Functionality doesn't match basis type ");
   return high;  //never gets here
}

/**
 * Nbas function result for basistype=HO
**/
In OneModeBasDef::Nbas() const
{
   In n_bas = -I_1;
   if (mIadd == -I_1)
   {
      MIDASERROR("wrong address for the specified basis set ");
   }

   // This is a harmonic oscillator basis
   if (mBasType == "HO")
   {
      return gOneModeHoDefs[mIadd].Nbas();
   }
   // This is a distributed Gauss basis
   else if (mBasType == "Gauss")
   {
      return gOneModeGaussDefs[mIadd].Nbas();
   }
   // This is a B-spline basis
   else if (mBasType == "Bsplines")
   {
      return gOneModeBsplDefs[mIadd].Nbas();
   }
   else if  (  mBasType == "StateBasis"
            )
   {
      return gOneModeStateBasis[mIadd].NBas();
   }
   else
   {
      MIDASERROR(" Functionality doesn't match basis type ");
   }

   // Never gets here
   return n_bas;  
}

/**
 * Omeg function result for basis=HO
**/
Nb OneModeBasDef::Omeg() const
{
   Nb omeg=C_0;
   if (mIadd==-1) MIDASERROR(" wrong address for the specified basis set ");
   if (mBasType=="HO") //this is Ho basis
      return gOneModeHoDefs[mIadd].Omeg();
   else       
      MIDASERROR(" Functionality doesn't match basis type ");
   return omeg;   //never gets here
}

/**
 * Mass function result for basis=HO
**/
Nb OneModeBasDef::Mass() const
{
   Nb mass=C_0;
   if (mIadd==-1) MIDASERROR(" wrong address for the specified basis set ");
   if (mBasType=="HO") //this is Ho basis
      return gOneModeHoDefs[mIadd].Mass();
   else
      MIDASERROR(" Functionality doesn't match basis type ");
   return mass; //never gets here
}

/**
 * Get the exponent vector for basis=Gauss
**/
void  OneModeBasDef::GetVecOfExps(MidasVector& arVec) const
{
   if (mIadd==-1) MIDASERROR(" wrong address for the specified basis set ");
   if (mBasType=="Gauss") //this is Gauss basis
   {
      gOneModeGaussDefs[mIadd].GetVecOfExps(arVec);
      return;
   }
   else
      MIDASERROR(" Functionality doesn't match basis type ");
}

/**
 * Get Dens function result for basis=Gauss
**/
Nb OneModeBasDef::BasDens() const
{
   Nb dens=C_0;
   if (mIadd==-1) MIDASERROR(" wrong address for the specified basis set ");
   if (mBasType=="Gauss") //this is Gauss basis
      return gOneModeGaussDefs[mIadd].GetBasDens();
   else if (mBasType=="Bsplines") //this is B-spline basis
      return gOneModeBsplDefs[mIadd].GetBasDens();
   else       
      MIDASERROR(" Functionality doesn't match basis type ");
   return dens; //never get here
}

/**
 * Get Aux definition for basis set
**/
string OneModeBasDef::Aux() const
{
   string s="";
   if (mIadd==-1) MIDASERROR(" wrong address for the specified basis set ");
   if (mBasType=="HO") return gOneModeHoDefs[mIadd].Aux();
   else if (mBasType=="Gauss") return gOneModeGaussDefs[mIadd].Aux();
   else if (mBasType=="Bsplines") return gOneModeBsplDefs[mIadd].Aux();
   else if (mBasType=="StateBasis") return gOneModeStateBasis[mIadd].Aux();
   else 
      MIDASERROR(" Functionality doesn't match basis type ");
   return s; //never get here
}

/**
 * Get Aux definition for basis set
**/
In OneModeBasDef::Nord() const
{
   In iord=-1;
   if (mIadd==-1) MIDASERROR(" wrong address for the specified basis set ");
   if (mBasType=="Bsplines") return gOneModeBsplDefs[mIadd].Nord();
   else
      MIDASERROR(" Functionality doesn't match basis type ");
   return iord; //never get here
}

/**
 * Get location vector for basis=Gauss
**/
void OneModeBasDef::GetVecOfPts(MidasVector& arVec) const
{
   if (mIadd == -I_1)
   {
      MIDASERROR("Wrong address for the specified basis set ");
   }

   if (mBasType == "Gauss")
   {
      gOneModeGaussDefs[mIadd].GetVecOfPts(arVec);
   }
   else if (mBasType == "Bsplines")
   {
      gOneModeBsplDefs[mIadd].GetGrid(arVec);
   }
   else
   {
      MIDASERROR("Functionality doesn't match basis type ");
   }
}

/**
 * Get bounds for Gauss or B-spline basis grids
**/
std::pair<Nb, Nb> OneModeBasDef::GetBasisGridBounds() const
{
   MidasVector arVec;
   Nb left_bound = I_0;
   Nb right_bound = I_0;
   if (mIadd == -I_1) 
   {
      MIDASERROR(" wrong address for the specified basis set ");
   }

   // In case of a distributed Gaussian primitive basis set
   if (mBasType == "Gauss") 
   {
      gOneModeGaussDefs[mIadd].GetVecOfPts(arVec);
      left_bound = arVec[I_0];
      right_bound = arVec[arVec.Size() - I_1]; 
   }
   // In case of a B-spline primitive basis set
   else if (mBasType == "Bsplines") 
   {
      gOneModeBsplDefs[mIadd].GetGrid(arVec);
      left_bound = arVec[I_0];
      right_bound = arVec[arVec.Size() - I_1];
   }
   // In case of a harmonic oscillator primitive basis set
   else if (mBasType == "HO")
   {
      MIDASERROR("Harmonic oscillator primitive basis set do not have well-defined bounds");
   }
   else
   {
      MIDASERROR("Functionality doesn't match primitive basis type");
   }
   
   return std::make_pair(left_bound, right_bound);
}

/**   
 * Get location vector for basis=Gauss
**/   
Nb OneModeBasDef::GetGaussPnt(In aAdd) const
{
   Nb p_g=C_0;
   if (mIadd==-1) MIDASERROR(" wrong address for the specified basis set ");
   if (mBasType=="Gauss") //this is Gauss basis
      return gOneModeGaussDefs[mIadd].GetGaussPnt(aAdd);
   else
      MIDASERROR(" Functionality doesn't match basis type ");
   return p_g;
}

/**   
 * Get a particular knot value
**/
const Nb OneModeBasDef::GetKnotValue(const In& aAdd) const
{
   if (mIadd == - I_1)
   {
      MIDASERROR("Wrong address for the specified basis set ");
   }

   Nb k_v = C_0;
   if (mBasType == "Bsplines")
   {
      return gOneModeBsplDefs[mIadd].GetKnotValue(aAdd);
   }
   else
   {
      MIDASERROR(" Functionality doesn't match basis type ");
   }
   return k_v;
}

/**   
 * Get the exponent for a particular gaussian
**/   
Nb OneModeBasDef::GetExp(In aAdd) const
{
   Nb g_exp=C_0;
   if (mIadd==-1) MIDASERROR(" wrong address for the specified basis set ");
   if (mBasType=="Gauss") //this is Gauss basis
      return gOneModeGaussDefs[mIadd].GetExp(aAdd);
   else
      MIDASERROR(" Functionality doesn't match basis type ");
   return g_exp;
}

/**
 * Get type of basis. HO: Harmonic oscillator, Gauss: Lgf, B-splines: Bsplines
**/
string OneModeBasDef::Type() const
{
   string s="";
   if (mIadd==-1) MIDASERROR(" wrong address for the specified basis set ");
   if (mBasType=="HO") 
      return gOneModeHoDefs[mIadd].Type();
   else if (mBasType=="Gauss") 
      return gOneModeGaussDefs[mIadd].Type();
   else if (mBasType=="Bsplines")
      return gOneModeBsplDefs[mIadd].Type();
   else if (mBasType=="StateBasis")
      return gOneModeStateBasis[mIadd].Type();
   else
      MIDASERROR(" Functionality doesn't match basis type ");
   return s; //never get here
}

/**
 * Get Global mode Nr.
**/
GlobalModeNr OneModeBasDef::GetGlobalModeNr() const
{
   GlobalModeNr mode_nr=-I_1;
   if (mIadd==-1) MIDASERROR(" wrong address for the specified basis set ");
   if (mBasType=="HO") 
      return gOneModeHoDefs[mIadd].GetGlobalModeNr();
   else if (mBasType=="Gauss") 
      return gOneModeGaussDefs[mIadd].GetGlobalModeNr();
   else if (mBasType=="Bsplines")
      return gOneModeBsplDefs[mIadd].GetGlobalModeNr();
   else if (mBasType=="StateBasis")
      return gOneModeStateBasis[mIadd].GetGlobalModeNr();
   else
      MIDASERROR(" Functionality doesn't match basis type ");
   return mode_nr; // never get here
}

/**
 * Change mode Nr.
**/
void OneModeBasDef::ChangeModeNr(In aGlobalModeNr) 
{
   if (mIadd==-1) MIDASERROR(" wrong address for the specified basis set ");
   if (mBasType=="HO") 
      gOneModeHoDefs[mIadd].ChangeModeNr(aGlobalModeNr);
   else if (mBasType=="Gauss") 
      gOneModeGaussDefs[mIadd].ChangeModeNr(aGlobalModeNr);
   else if (mBasType=="Bsplines")
      gOneModeBsplDefs[mIadd].ChangeModeNr(aGlobalModeNr);
   else
      MIDASERROR(" Functionality doesn't match basis type ");
}

/**
 * Evaluates the one-mode basis function at a given point
**/
Nb OneModeBasDef::EvalBasisFunc
   (  const In& aIbas
   ,  const Nb& aQval
   ,  const bool& aKeepUnboundedBsplines
   )  const
{
   if (mIadd == -I_1)
   {
      MIDASERROR("Wrong address for the specified basis set ");
   }

   Nb f_val = C_0;
   if (mBasType == "HO")
   {
      return gOneModeHoDefs[mIadd].EvalHoBasis(aIbas, aQval);
   }
   else if (mBasType == "Gauss")
   {
      return gOneModeGaussDefs[mIadd].EvalGaussBasis(aIbas, aQval);
   }
   else if (mBasType == "Bsplines")
   {
      return gOneModeBsplDefs[mIadd].EvalSplineBasis(aIbas, aQval, aKeepUnboundedBsplines);
   }
   else
   {
      MIDASERROR("Functionality doesn't match basis type ");
   }

   // Never get here
   return f_val;
}

/**
 * Evaluates the one-mode basis function at a given point
**/
void OneModeBasDef::EvalBasisFunc
   (  const In& aIbas
   ,  const MidasVector& arQvals
   ,  MidasVector& arFuncVals
   ,  const bool& aKeepUnboundedBsplines
   )  const
{
   if (mIadd == -I_1)
   {
      MIDASERROR("Wrong address for the specified basis set ");
   }

   if (mBasType == "HO")
   {
      return gOneModeHoDefs[mIadd].EvalHoBasis(aIbas, arQvals, arFuncVals);
   }
   else if (mBasType == "Gauss")
   {
      return gOneModeGaussDefs[mIadd].EvalGaussBasis(aIbas, arQvals, arFuncVals);
   }
   else if (mBasType == "Bsplines")
   {
      return gOneModeBsplDefs[mIadd].EvalSplineBasis(aIbas, arQvals, arFuncVals, aKeepUnboundedBsplines);
   }
   else
   {
      MIDASERROR("Functionality doesn't match basis type ");
   }
}

/**
 * Get Nr of intervals for basis defs. on grid (Gauss/Bsplines)
**/
const In OneModeBasDef::Nint() const
{
   if (mIadd == -I_1)
   {
      MIDASERROR("Wrong address for the specified basis set ");
   }
   
   In n_int = -I_1;
   if (mBasType == "Gauss")
   {
      return gOneModeGaussDefs[mIadd].Nint();
   }
   else if (mBasType == "Bsplines")
   {
      return gOneModeBsplDefs[mIadd].Nint();
   }
   else
   {
      MIDASERROR("Functionality doesn't match basis type ");
   }
   
   // Never get here
   return n_int;
}

/**
 * Get the index of the last B-spline not null in a given interval
**/
const In OneModeBasDef::GetLastInd(const In& aInt) const
{
   if (mIadd == - I_1)
   {
      MIDASERROR("Wrong address for the specified basis set ");
   }
   
   In ind = -I_1;
   if (mBasType == "Bsplines")
   {
      return gOneModeBsplDefs[mIadd].GetLastInd(aInt);
   }
   else
   {
      MIDASERROR("Functionality doesn't match basis type ");
   }

   // Never get here
   return ind; 
}

/**
 *  Use arOneModeBasDef as template for basis for another mode, aGlobalMode 
**/
void OneModeBasDef::CloneOneBasisToOtherMode( OneModeBasDef& arOneModeBasDef
                                            , In aGlobalMode
                                            )
{
   mBasType = arOneModeBasDef.Type(); 
   if (mBasType=="HO") //this is Ho basis
   {
      In i_add_in = arOneModeBasDef.mIadd; 
      if (i_add_in < gOneModeHoDefs.size()) gOneModeHoDefs.push_back(gOneModeHoDefs[i_add_in]);
      mIadd    = gOneModeHoDefs.size()-1;  
      gOneModeHoDefs[mIadd].ChangeModeNr(aGlobalMode); 
      return; 
   } 
   else if (mBasType=="Gauss") //this is gauss basis
   {
      In i_add_in = arOneModeBasDef.mIadd; 
      if (i_add_in < gOneModeGaussDefs.size()) gOneModeGaussDefs.push_back(gOneModeGaussDefs[i_add_in]);
      mIadd    = gOneModeGaussDefs.size()-1;  
      gOneModeGaussDefs[mIadd].ChangeModeNr(aGlobalMode); 
      return; 
   } 
   else if (mBasType=="Bsplines") //this is B-spline basis
   {
      In i_add_in = arOneModeBasDef.mIadd; 
      if (i_add_in < gOneModeBsplDefs.size()) gOneModeBsplDefs.push_back(gOneModeBsplDefs[i_add_in]);
      mIadd    = gOneModeBsplDefs.size()-1;  
      gOneModeBsplDefs[mIadd].ChangeModeNr(aGlobalMode); 
      return; 
   } 
   else
      MIDASERROR("Unknown basis type in CloneOneBasisToOtherMode"); 
} 

