/**
************************************************************************
* 
* @file                VccCalcDef.cc
*
* Created:             04-11-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Contains all input-info that defines a Vcc calculation
* 
* Last modified: Wed Mar 24, 2010  02:31PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "input/VccCalcDef.h"
#include "input/VscfCalcDef.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "input/NewtonRaphsonInput.h"
#include "input/Trim.h"
#include "input/IntermediateRestrictionsInput.h"
#include "input/LaplaceInput.h"


typedef string::size_type Sst;

/**
* Construct a definition of a Vcc calculation
* */
VccCalcDef::VccCalcDef()
   :  VscfCalcDef()
{
   mVcc                  = false;
   mVccOutOfSpace        = false;
   mVccRemoveSelUnlinked = false;

   mVciH2                = false;               
   mVciH2Roots           = 0;
   mH2Ls                 = false;                 
   mH2LevelShift         = C_0;                   

   mVci                  = false;
   mVciRoots             = 0;
   mRepVciAsVcc          = 0;                  
   mRepVccAsVci          = 0;                 

   mVmp                  = false;
   mPade                 = false;
   mVmpMaxOrder          = 0;
   mLambdaScan           = false;
   mLambdas.clear();

   mVapt                 = false;
   mVaptMaxOrder          = 0;

   mFvciExpH             = false;
 
   mExpHvci              = false;
   mExpHvciExciLevel     = 0;

   mMaxExciLevel         = -1; 
   mMaxExPrMode.clear();
   mMaxSumN              = 0;
   mIso                  = false;
   mIsOrder              = 0;
   mSumNprim             = false;
   mSumNThr              = 0;
   mEmaxprim             = false;
   mEmaxThr              = C_NB_MAX;
   mZeroprim             = false;
   mZeroThr              = -C_NB_MAX;
   mT1TransH             = true;
   mOneModeTrans         = false;
   mTwoModeTrans         = false;
   mTwoModeTransScreen   = C_0;

   mEigValSeqSet         = false;
   mEigValSeq            = I_0;

   mModalFileName.clear();

   mRestDiffExci         = false;

   mPrimitiveBasis       = false;

   mTestTransformer      = false;
   mH2Start              = false;
   mVmpStart             = false;

   mTargetSpace          = false;
   mReference            = false;
   mAllFundamental       = false;
   mAllFirstOvertones    = false;
   mFundamental          = false;
   mFirstOvertones       = false;
   mFunds.clear();
   mFirsts.clear(); 

   mFcVciCalc            = false;
   mFcVciCalcOps.clear();

   mMinTrustScale        = C_I_10_4;                   
   mTrustScaleDown       = C_I_5;                   
   mTrustScaleUp         = C_4;
   
   mV3trans              = false;
   mV3type               = V3::V3UNKNOWN;
   mV3latex              = false;
 
   mCmpV3OrigVcc         = false;
   mCmpV3OrigVci         = false;
   mCmpV3OrigVccJac      = false;
   mCheckV3Left          = false;
   
   mNresvecs             = I_0;
   mHarmonicRR           = false;
   mEnerShift            = C_0;

   mScreenIntermeds       = false;
   mScreenIntermedsThresh = C_0;
}

/**
 *
 **/
void VccCalcDef::StoreVciEigVals(MidasVector arVciEigVals)
{
   mVciEigVals.resize(arVciEigVals.Size());
   for (In i=I_0;i<arVciEigVals.Size();i++) mVciEigVals[i] = arVciEigVals[i];
}

/**
 *
 **/
bool VccCalcDef::ReadUseNewTensorContractions
   (  std::istream& Minp
   ,  std::string& s
   )
{
   mUseNewTensorContractions = true;
   return false;
}

/**
 * Add LaplaceInfo to set
 * @param Minp
 * @param s
 **/
bool VccCalcDef::ReadLaplaceInfoSet
   (  std::istream& Minp
   ,  std::string& s
   )
{
   return LaplaceInput(Minp, s, 4, this->GetLaplaceInfo());
}

/**
 * Adds TensorDecompInfo to mDecompInfoSet and sets the TensorNlSolver
 * @param Minp
 * @param s
 **/
bool VccCalcDef::ReadVccDecompInfoSet
   (  std::istream& Minp
   ,  std::string& s
   )
{
   this->SetupTensorDataContCalculation();
   return NewTensorDecompInput(Minp, s, 4, this->GetDecompInfoSet());
}

/**
 * Adds TensorDecompInfo to mTransformerDecompInfoSet and sets the TensorNLSolver
 * @param Minp
 * @param s
 **/
bool VccCalcDef::ReadVccTransformerDecompInfoSet
   (  std::istream& Minp
   ,  std::string& s
   )
{
   this->SetupTensorDataContCalculation();
   return NewTensorDecompInput(Minp, s, 4, this->GetTransformerDecompInfoSet());
}

/**
 *
 **/
bool VccCalcDef::ReadVccTransformerAllowedRank
   (  std::istream& Minp
   ,  std::string& s
   )
{
   mTransformerAllowedRank = midas::input::GetIn(Minp, s);
   return false;
}

/**
 *
 **/
bool VccCalcDef::ReadNewtonRaphsonInput
   (  std::istream& Minp
   ,  std::string& s
   )
{
   return NewtonRaphsonInput(Minp, s, this->mNrInfo);
}

/**
 *
 **/
bool VccCalcDef::ReadIntermediateRestrictions
   (  std::istream& Minp
   ,  std::string& s
   )
{
   return IntermediateRestrictionsInput(Minp, s, this->mIntermedRestrict);
}

/**
 *
 **/
void VccCalcDef::SetupTensorDataContCalculation()
{
   this->mUseTensorNlSolver = true;
   this->SetRspEigType(RSPEIGTYPE::TENSOR);
}

/**
 *
 **/
void VccCalcDef::SetupDataContCalculation()
{
   this->mUseTensorNlSolver = false;
   this->SetRspEigType(RSPEIGTYPE::STD);
}

namespace midas::input::detail
{

const std::map<std::string, VccCalcDef::MethodT> GetVccMethodMap
   (
   )
{
   static std::map<std::string, VccCalcDef::MethodT> method_map =
   {  {"VCC", VccCalcDef::MethodT::VCC}
   ,  {"CP-VCC", VccCalcDef::MethodT::CPVCC}
   ,  {"CPVCC", VccCalcDef::MethodT::CPVCC}
   ,  {"VCI", VccCalcDef::MethodT::VCI}
   ,  {"VMP", VccCalcDef::MethodT::VMP}
   };

   return method_map;
}

} /* namespace midas::input::detail */

/**
 * Set up method automatically from input strings
 *
 * @param arInp      Input stream
 * @param arS        Input string
 * @return
 *    Pair of bools. Have we read next input line? Are we using CP-VCC?
 **/
std::pair<bool, bool> VccCalcDef::AutoSetupMethod
   (  std::istream& arInp
   ,  std::string& arS
   )
{
   // Get line with method label
   midas::input::GetLine(arInp, arS);

   // Transform to upper case and delete blanks
   arS = midas::input::ParseInput(arS);

   // Check that we get a method
   MidasAssert(midas::input::NotKeyword(arS), "Expected a method label after #3METHOD...");

   // Split string at first [ and set method
   auto s_vec = midas::util::StringVectorFromString(arS, '[');
   MidasAssert(s_vec.size() <= 2, "Did not expect more than one '[' in method name!");

   const auto& method_map = midas::input::detail::GetVccMethodMap();
   VccCalcDef::MethodT method = MethodT::ERROR;
   In vmporder = I_0;

   if (  s_vec[0].substr(0, 3) == "VMP"
      )
   {
      method = VccCalcDef::MethodT::VMP;
      vmporder = midas::util::FromString<In>(s_vec[0].substr(3, 1)) / I_2; // Energy order!
   }
   else
   {
      try
      {
         method = method_map.at(s_vec[0]);
      }
      catch (  const std::out_of_range&
            )
      {
         MIDASERROR("Did not find '" + s_vec[0] + "' in VccMethodMap!");
      }
   }

   // Remove ] and set order
   std::string order = "-1";

   if (  s_vec.size() > 1
      )
   {
      while (  s_vec[1].find(']') != s_vec[1].npos
            ) 
      {
         s_vec[1].erase(s_vec[1].find(']'), 1);
      }
      order = s_vec[1];
   }

   bool cpvcc = false;
   In maxexci = I_0;
   bool twomodetrans = false;
   bool v3trans = false;
   V3 v3type = V3::V3UNKNOWN;

   // Switch on method
   switch   (  method
            )
   {
      case MethodT::CPVCC:
      {
         cpvcc = true;

         // No break! Continue to VCC
      }
      case MethodT::VCC:
      {
         this->SetVcc(true);

         if (  order == "2H2"
            )
         {
            MidasAssert(!cpvcc, "CP-VCC not implemented for hard-coded VCC[2H2]!");
            maxexci = I_2;
            twomodetrans = true;
         }
         else if  (  order == "2PT3"
                  )
         {
            maxexci = I_3;
            v3type = V3::VCC2PT3;
            v3trans = true;
         }
         else if  (  order == "3PT4"
                  )
         {
            maxexci = I_4;
            v3type = V3::VCC3PT4;
            v3trans = true;
         }
         else
         {
            maxexci = midas::util::FromString<In>(order);
            v3type = V3::GEN;
            v3trans = true;
         }

         // Break case
         break;
      }
      case MethodT::VCI:
      {
         // Set to find 1 state as default. This can be changed by specifying NVCISTATES below...
         this->SetVci(true, I_1);
         maxexci = midas::util::FromString<In>(order);

         // For order > 6, we have no tested v3c files
         if (  maxexci > I_6
            )
         {
            MidasWarning("VCI with excitation level > 6 requested. Use the old, general transformer!");
         }
         else
         {
            v3trans = true;
            v3type = V3::GEN;
         }

         // Break case
         break;
      }
      case MethodT::VMP:
      {
         // Set VMP to order
         this->SetVmp(true, vmporder);

         // Set max exci for VMP
         maxexci = midas::util::FromString<In>(order);

         // Break case
         break;
      }
      default:
      {
         MIDASERROR("No match for given MethodT obtained from string '" + s_vec[0] + "'.");
      }
   }

   // Set variables
   this->SetMaxExciLevel(maxexci);     // Exci level
   this->SetVccOutOfSpace(false);      // Out of space (off)
   this->SetT1TransH(true);            // Always use T1 transform
   this->SetV3FilePrefix(input::gProgramSettings.GetEnvironmentSettings().GetSetting("MIDAS_INSTALL_DATADIR") + "/");
   if (  twomodetrans
      )
   {
      this->SetTwoModeTrans(true);
   }
   else if  (  v3trans
            )
   {
      this->SetV3trans(true);
      this->SetV3type(v3type);
   }


   // Read more lines
   while (  midas::input::GetLine(arInp, arS)
         && midas::input::NotKeyword(arS)
         )
   {
      // Delete blanks and transform to upper case
      auto s = midas::input::ToUpperCase(arS);

      // Split into vector
      auto arg_vec = midas::util::StringVectorFromString(s);
      auto arg_vec_orig = midas::util::StringVectorFromString(arS);
      MidasAssert(!arg_vec.empty(), "Empty argument vector. This should not happen!");
      const auto& key = arg_vec[0];

      if (  key == "NVCISTATES"
         )
      {
         MidasAssert(this->Vci(), "NVCISTATES only makes sense when using VCI");
         MidasAssert(arg_vec.size() == 2, "Set one integer for NVCISTATES.");
         this->mVciRoots = midas::util::FromString<In>(arg_vec[1]);
      }
      else
      {
         MIDASERROR("Unknown key for #3Method: '" + key + "'.");
      }
   }

   // We have read, but not processed the next input line
   bool already_read = true;

   // Return bools
   return std::make_pair(already_read, cpvcc);
}
