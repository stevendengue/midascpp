/**
************************************************************************
* 
* @file                MolStruct.cc
*
* Created:             03-11-2010
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*                      Kristian Sneskov (sneskov@chem.au.dk) 
*
* Short Description:   Contains input-info that defines a calculation
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "input/MolStruct.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "mmv/MidasVector.h"
#include "nuclei/Vector3D.h"

typedef string::size_type Sst;

/**
* Construct a default definition of a  calculation
* */
MolStruct::MolStruct() : mOrigo(C_0,C_0,C_0)
{
   mNucleiExist = false;
   mNuclei.clear();
 
}
/**
* Add to set of reference nuclei 
* */
void MolStruct::AddNucleus(Nuclei& arNuc)
{
   mNucleiExist=true;
   //Mout << " add nucleus to nuclei " << arNuc << endl;
   //Mout << " nr of nuclei " << mNuclei.size() << endl;
   mNuclei.push_back(arNuc);
}

std::string MolStruct::GetAtomLabel(In arINuc) 
{
   Nuclei atom = mNuclei[arINuc];
   return atom.AtomLabel(); 
}

void MolStruct::GetEndpoints(MidasVector& arEndPoints)
{
  for (In i_nuc=I_0; i_nuc<mNuclei.size(); i_nuc++)
  { 
    if (mNuclei[i_nuc].X() < arEndPoints[0] ) arEndPoints[0]=mNuclei[i_nuc].X();
    if (mNuclei[i_nuc].X() > arEndPoints[1] ) arEndPoints[1]=mNuclei[i_nuc].X();
    if (mNuclei[i_nuc].Y() < arEndPoints[2] ) arEndPoints[2]=mNuclei[i_nuc].Y();
    if (mNuclei[i_nuc].Y() > arEndPoints[3] ) arEndPoints[3]=mNuclei[i_nuc].Y();
    if (mNuclei[i_nuc].Z() < arEndPoints[4] ) arEndPoints[4]=mNuclei[i_nuc].Z();
    if (mNuclei[i_nuc].Z() > arEndPoints[5] ) arEndPoints[5]=mNuclei[i_nuc].Z();
  }  
}

MidasVector MolStruct::GetNucleusCoordinates(In arINuc) const
{
    MidasVector result(I_3,C_0);
    result[0]=mNuclei[arINuc].X();
    result[1]=mNuclei[arINuc].Y();
    result[2]=mNuclei[arINuc].Z();
    return result;
}

Vector3D MolStruct::GetNucleusCoordinates3D(In arINuc)
{
    Vector3D result;
    mNuclei[arINuc].PutNucToVector3D(result);

    return result;
}

Nb MolStruct::ClosestNuc(string& arAtomLabel, Vector3D arPoint)
{
  Nb min_dist=mNuclei[I_0].Distance(arPoint);
  //Mout << "Dist to first atom: " << min_dist << endl;
  In closest=I_0;
  Nb tmp=C_0;
  for (In i=I_1; i<mNuclei.size() ; i++)
  { 
    tmp=mNuclei[i].Distance(arPoint);
   // Mout << "Debugging MolStruct: Distances: " << tmp << endl;
    if ( tmp < min_dist ) 
    {
      min_dist=tmp;
      closest=i;
    }
  }
  arAtomLabel=mNuclei[closest].AtomLabel();
  return min_dist;
}

