/**
************************************************************************
* 
* @file                TransformPotCalcDef.h
*
* 
* Created:             07-12-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk) 
*
* Short Description:   Calculation definition for transformation of potentials  
* 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef TRANSFORMPOTCALCDEF_H
#define TRANSFORMPOTCALCDEF_H

#include <string>

#include "pes/molecule/MoleculeFile.h"

using namespace midas;

class TransformPotCalcDef
{
   private:
      molecule::MoleculeFileInfo mInMolFileInfo = molecule::MoleculeFileInfo("","");
      std::string                mOperatorFileName;
      bool                       mKeepMaxCoupLevel = false;
   public:
      TransformPotCalcDef():
             mOperatorFileName("DUMMY") {;}
      virtual ~TransformPotCalcDef() {};

      void SetInMolFileInfo (const molecule::MoleculeFileInfo& arFileInfo) {mInMolFileInfo=arFileInfo;}
      void SetOperatorFileName(const std::string& arName) {mOperatorFileName=arName;}
      void SetKeepMaxCoupLevel(const bool& arBool=true) {mKeepMaxCoupLevel=arBool;}
  
      molecule::MoleculeFileInfo GetInMolFileInfo() const {return mInMolFileInfo;}
      std::string GetOperatorFileName() const {return mOperatorFileName;}
      bool KeepMaxCoupLevel() const {return mKeepMaxCoupLevel;}

};

#endif //TRANSFORMPOTCALCDEF_H
