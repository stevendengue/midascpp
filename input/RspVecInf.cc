/**
************************************************************************
* 
* @file                RspVecInf.cc
*
* Created:             11-08-2004
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Contains Rsp Vec info
* 
* Last modified: Tue Sep 08, 2009  10:57AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "input/RspVecInf.h"

typedef string::size_type Sst;

/**
* Construct a default definition of a  calculation
* */
RspVecInf::RspVecInf(In aNorder,bool aRight, bool aIsNonStandardType) : RspFunc(aNorder)
{
   mRight = aRight;
   mIsNonStandardType = aIsNonStandardType;
}
/**
* << overload for ostream
* */
ostream& operator<<(ostream& arOut, const RspVecInf& arRspVecInf)
{ 
   arOut.setf(ios::scientific);
   arOut.setf(ios::uppercase);
   midas::stream::ScopedPrecision(16, arOut);
   arOut.setf(ios::showpoint);

   In n_ord = arRspVecInf.GetNorder();
   if (!arRspVecInf.mIsNonStandardType)
   {
      if (arRspVecInf.GetRight()) arOut << "R";
      if (!arRspVecInf.GetRight()) arOut << "L";
   }
   else 
   {
      arOut << arRspVecInf.GetNonStandardType(); 
   }
   arOut << "[";
   //for (In i=I_0;i<abs(n_ord);i++) MBH
   for (In i=I_0;i<abs(n_ord)%10;i++)
   {
      arOut << arRspVecInf.GetRspOp(i);
      //if (i!=n_ord-I_1) arOut << ","; MBH
      if (i<n_ord-I_1) arOut << ",";
   }
   arOut << "]";
   if (n_ord>I_0)
   { 
      arOut << "(";
      for (In i=I_0;i<n_ord;i++)
      {
         arOut << arRspVecInf.GetRspFrq(i);
         if(arRspVecInf.Gamma())
         {
            arOut << " + i*" << arRspVecInf.Gamma();
         }
         if (i!=n_ord-I_1) arOut << ",";
      }
      arOut << ")";
   }

   if (arRspVecInf.HasBeenEval()) arOut << " - already solved/calculated ";
   else arOut << " - not solved yet";

   return arOut;
}

bool operator<(const RspVecInf& aR1, const RspVecInf& aR2)
{
   if(static_cast<const RspFunc&>(aR1)==static_cast<const RspFunc&>(aR2))
   {
      return aR1.GetNonStandardType()<aR2.GetNonStandardType();
   }
   return operator<(static_cast<const RspFunc&>(aR1),static_cast<const RspFunc&>(aR2));
}
