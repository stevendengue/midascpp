/**
************************************************************************
* 
* @file                OpInfo.h
*
* Created:             14-09-2006
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Class for holding information about a given operator
* 
* ???? Last modified: Fri Sep 01, 2006  09:06PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef OPINFO_H
#define OPINFO_H

#include <string>
#include <map>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"

#include "operator/PropertyType.h"

using namespace midas;

/**
 * Operator information.
 *
 * Holds type of operator, frequencies, and assymetric property.
 **/
class OpInfo
{
   //! Max number of frequencies (can be increased if nescessary)
   static constexpr In n_frequencies = 3;

   private:
      //! Operator property type, e.g. 'oper::energy'
      oper::PropertyType mPropertyType = oper::unknown;
      //! Operator "electronic" frequencies
      Nb mFrequencies[n_frequencies] = { 0.0 };
      //! Is the operator assymetric
      bool mAsymmetric = false;

   public:
      //! Constructor
      OpInfo() = default;
        
      //@{
      //! OpInfo class setters
      void SetType(const oper::PropertyType& arType)  { mPropertyType = arType; }
      void SetFrq (In aI, Nb aFrq)               
      {
         if (  aI >= mPropertyType.GetOrder() 
            || aI >= n_frequencies
            )
         {
            MIDASERROR("SetFrq() : Not so many frq's for this OpInfo...");
         }
         mFrequencies[aI] = aFrq; 
      }
      void SetAsymmetric(const bool& aB)        { mAsymmetric = aB; }
      //@}
      
      //@{
      //! OpInfo class getters
      const oper::PropertyType& GetType()     const  { return mPropertyType; }
      const Nb&                 GetFrq(In aI) const 
      {
         if (  aI >= mPropertyType.GetOrder() 
            || aI >= n_frequencies
            )
         {
            MIDASERROR("GetFrq(): Not so many frq's for this OpInfo...");
         }
         return mFrequencies[aI];
      }
      const bool& GetAsymmetric() const      {return mAsymmetric;}
      //@}

      //@{
      //! OpInfo class inquire type functions
      bool IsDipole() const;
      bool IsAlpha() const;
      //@}

      //! Operator ==
      friend bool operator==(const OpInfo&,const OpInfo&);

      //! Operator output
      friend ostream& operator<<(ostream&,const OpInfo&);
};

#endif /* OPINFO_H */
