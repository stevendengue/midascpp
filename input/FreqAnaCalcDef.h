/**
************************************************************************
* 
* @file                FreqAnaCalcDef.h
*
* 
* Created:             04-05-2015
*
* Author:              Mads Boettger Hansen (mb.hansen@chem.au.dk)
*
* Short Description:   Calculation definition frequency analysis
* 
* Last modified:       09-06-2015 (Mads Boettger Hansen)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef FREQANACALCDEF_H_INCLUDED
#define FREQANACALCDEF_H_INCLUDED

// std headers
#include <string>
#include <vector>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/DataCont.h"


/* ************************************************************************
************************************************************************ */
class FreqAnaCalcDef
{
   private:
      //General options:
      Nb             mDispFactor;
      bool           mNonSymmetricHessian;
      bool           mCompareSymAndNonSym;
      std::string    mSinglePointName;
      In             mNumNodes;
      In             mProcPerNode;
      In             mDumpInterval;
      Nb             mRotationThr;
      std::string    mMolInfoOutName;
      //Targeting options (below is only used if mTargeting == true:
      bool           mTargeting;
      Nb             mItEqEnerThr;
      Nb             mItEqResThr;
      In             mItEqBreakDim;
      In             mItEqMaxIt;
      Nb             mOverlapSumMin;
      In             mOverlapNMax;
      bool           mAlreadyMassWeighted;

   public:
      //constructor/destructor
      FreqAnaCalcDef()
         :  mDispFactor(C_I_10_2)
         ,  mNonSymmetricHessian(false)
         ,  mCompareSymAndNonSym(false)
         ,  mSinglePointName("SP_FreqAna") 
         ,  mNumNodes(I_1)
         ,  mProcPerNode(I_1)
         ,  mDumpInterval(I_10)
         ,  mRotationThr(C_I_10_6)
         ,  mMolInfoOutName("MoleculeInfo_FreqAna.mmol")
         ,  mTargeting(false)
         ,  mItEqEnerThr(C_I_10_8)
         ,  mItEqResThr(C_I_10_8)
         ,  mItEqBreakDim(I_10_3)
         ,  mItEqMaxIt(I_10_3)
         ,  mOverlapSumMin(C_8 * C_I_10_1)
         ,  mOverlapNMax(C_IN_MAX)
         ,  mAlreadyMassWeighted(false)
      {
      }

      //setting
      void SetDispFactor          (const Nb& arNb)              {mDispFactor = arNb;}
      void SetNonSymmetricHessian (const bool& arBool)          {mNonSymmetricHessian = arBool;}
      void SetCompareSymAndNonSym (const bool& arBool)          {mCompareSymAndNonSym = arBool;}
      void SetSinglePointName     (const std::string& arString) {mSinglePointName = arString;}
      void SetNumNodes            (const In& arIn)              {mNumNodes = arIn;}
      void SetProcPerNode         (const In& arIn)              {mProcPerNode = arIn;}
      void SetDumpInterval        (const In& arIn)              {mDumpInterval = arIn;}
      void SetRotationThr         (const Nb& arNb)              {mRotationThr = arNb;}
      void SetMolInfoOutName      (const std::string& arString) {mMolInfoOutName = arString;}
      void SetTargeting           (const bool& arBool)          {mTargeting = arBool;}
      void SetItEqEnerThr         (const Nb& arNb)              {mItEqEnerThr = arNb;}
      void SetItEqResThr          (const Nb& arNb)              {mItEqResThr = arNb;}
      void SetItEqBreakDim        (const In& arIn)              {mItEqBreakDim = arIn;}
      void SetItEqMaxIt           (const In& arIn)              {mItEqMaxIt = arIn;}
      void SetOverlapSumMin       (const Nb& arNb)              {mOverlapSumMin = arNb;}
      void SetOverlapNMax         (const In& arIn)              {mOverlapNMax = arIn;}
      void SetAlreadyMassWeighted (const bool& arBool)          {mAlreadyMassWeighted = arBool;}
      
      //getting
      Nb          GetDispFactor()          const {return mDispFactor;}
      bool        GetNonSymmetricHessian() const {return mNonSymmetricHessian;}
      bool        GetCompareSymAndNonSym() const {return mCompareSymAndNonSym;}
      std::string GetSinglePointName()     const {return mSinglePointName;}
      In          GetNumNodes()            const {return mNumNodes;}
      In          GetProcPerNode()         const {return mProcPerNode;}
      In          GetDumpInterval()        const {return mDumpInterval;}
      Nb          GetRotationThr()         const {return mRotationThr;}
      std::string GetMolInfoOutName()      const {return mMolInfoOutName;}
      bool        GetTargeting()           const {return mTargeting;}
      Nb          GetItEqEnerThr()         const {return mItEqEnerThr;}
      Nb          GetItEqResThr()          const {return mItEqResThr;}
      In          GetItEqBreakDim()        const {return mItEqBreakDim;}
      In          GetItEqMaxIt()           const {return mItEqMaxIt;}
      Nb          GetOverlapSumMin()       const {return mOverlapSumMin;}
      In          GetOverlapNMax()         const {return mOverlapNMax;}
      bool        GetAlreadyMassWeighted() const {return mAlreadyMassWeighted;}
};



#endif //FREQANACALCDEF_H_INCLUDED
