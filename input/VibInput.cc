/**
************************************************************************
* 
* @file                VibInput.cc
* 
* Created:             17-08-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   VibInput reader for the midas program.
* 
* Last modified: Mon Jul 12, 2010  03:07PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "input/VibInput.h"

// std headers
#include <map>
#include <algorithm>
#include <string>
#include <fstream>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "util/Io.h"
#include "util/Isums.h"
#include "input/Input.h"
#include "input/OneModeBasDef.h"
#include "input/BasDef.h"
#include "input/OpDef.h"
#include "operator/ActiveTerms.h"
#include "nuclei/Nuclei.h"
#include "mmv/Diag.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "input/OpInfo.h"
#include "input/GlobalOperatorDefinitions.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/MidasOperatorInput.h"
#include "input/FindKeyword.h"
#include "input/TensorDecompInput.h"
#include "input/BasisInput.h"
#include "input/OperatorInput.h"
#include "input/InputOperFromFile.h"
#include "mpi/FileToString.h"

// using declarations
using namespace midas::input;
using Sst = std::string::size_type;

/**
* Local Declarations
* */
std::string VscfInput(std::istream&);
std::string VccInput(std::istream&);
std::string TdHInput(std::istream&);
std::string McTdHInput(std::istream&);
std::string TimTdvccInput(std::istream&);
void InitializeVscf();
void InitializeVcc();
void InitializeTdH();
void InitializeMcTdH();


/**
 * Read and check vib input.
 * Read until next s = "#1..." which is returned.
 **/
std::string VibInput
   (  std::istream& Minp
   ,  bool passive
   )
{
   if (gDebug) Mout << " Function: VibInput, passive =  " << passive << endl;
   if (gDoVib) {
      Mout << " Warning! I have read Vib input once "<< endl;
   }
   if (gDoVib) MidasWarning(" WARNING!  I have read Vib input more than once "); 
   gDoVib     = true; 
   enum INPUT {ERROR, VSCF, VCC, IOLEVEL, OPERATOR, BASIS, TDH, MCTDH, TIMTDVCC};
   const std::map<std::string, INPUT> input_word =
   {  {"#2IOLEVEL",IOLEVEL}
   ,  {"#2OPERATOR",OPERATOR}
   ,  {"#2BASIS",BASIS}
   ,  {"#2VSCF",VSCF}
   ,  {"#2VCC",VCC}
   ,  {"#2TDH",TDH}
   ,  {"#2MCTDH",MCTDH}
   ,  {"#2TIMTDVCC",TIMTDVCC}
   };

   In input_level = 2;
   std::string s="";
   getline(Minp,s);
   while (!Minp.eof() && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))          // readin until input levels point up
   {

      if (passive) 
      {
         getline(Minp,s);
         continue;                                  // If passive loop simple to next #
      }
      std::string s_orig = s;
      std::transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      INPUT input = midas::input::FindKeyword(input_word,s);

      switch(input)
      {
         case IOLEVEL:
         {
            getline(Minp,s);                                  // Get new line with input
            gVibIoLevel = midas::util::FromString<In>(s);
            getline(Minp,s);
            break;
         }
         case OPERATOR:
         {
            s = midas::input::OperatorInput(Minp);
            break;
         }
         case BASIS:
         {
            s = BasisInput(Minp);
            break;
         }
         case VSCF:
         {
            s = VscfInput(Minp);
            break;
         }
         case VCC:
         {
            s = VccInput(Minp);
            break;
         }
         case TDH:
         {
            s = TdHInput(Minp);
            break;
         }
         case MCTDH:
         {
            s = McTdHInput(Minp);
            break;
         }
         case TIMTDVCC:
         {
            s = TimTdvccInput(Minp);
            break;
         }
         default:
         {
            Mout << " Keyword " << s_orig << 
               " is not a Vib level 2 Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
            getline(Minp,s);
         }
      }
   }
   return s;
}

/**
* Initialize Oper variables
* */
void InitializeOper()
{
   gDoOper              = false; 
   gOperIoLevel         = 2;
}
/**
* Initialize Basis variables
* */
void InitializeBasis()
{
   gBasisIoLevel        = 2;
   gReserveNdefs        = I_100;
}
/**
* Initialize Vib variables
* */
void InitializeVib()
{
   // Mout << " Initializing vib variables: " << endl;
   gDoVib               = false; 
   gVibIoLevel          = 0;
   gReserveNmodes       = I_1;
   gReserveNterms       = I_1;
   InitializeOper();  ///< Initialize Oper variables
   InitializeBasis(); ///< Initialize Basis variables
   InitializeVscf();  ///< Initialize Vscf variables
   InitializeVcc();   ///< Initialize Vcc  variables
   InitializeTdH();   ///< Initialize TDH variables
   InitializeMcTdH(); ///< Initialize MCTDH variables
}
