/**
************************************************************************
* 
* @file                TestInput.cc
*
* 
* Created:             19-11-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Input reader for the test module
* 
* Last modified: Wed Nov 25, 2009  02:52PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <sstream>

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Version.h"
#include "test/TestDrivers.h"
#include "util/Io.h"
#include "nuclei/Nuclei.h"
#include "input/Input.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/FindKeyword.h"

using std::map;
using std::vector;
using std::string;
using std::transform;
using std::sort;
using std::unique;
using std::find;

/**
 * Read and check Test input.
 * Read until next s = "#1..." which is returned (or #0).
 **/
string TestInput(std::istream& Minp,bool passive)
{
   //Mout << " Function: TestInput " << endl;
   if (gDoTest) Mout << " Warning! I have read Test input once "<< endl;
   gDoTest    = true; 

   // NB! Input enum and map are legacy for the Test input, but could be
   // relevant in the future, so I'll leave the framework for now. 
   // -MBH, Sep 2018.
   enum INPUT 
      {  ERROR
      };
   const map<string,INPUT> input_word =
      {
      };

   string s;
   In input_level = 2;
   // readin until input levels point up
   while (  midas::input::GetLine(Minp, s)
         && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level)
         )
   {
      if (passive) continue;                                  // If passive loop simple to next #

      std::string s_orig = s;
      s = midas::input::ParseInput(s);

      // If not a #-prefixed keyword, it's interpreted as a unit test driver
      // identifier; add it accordingly.
      if (!(s.substr(I_0,I_1)=="#"))
      {
         // If appending doesn't succeed, identifier was invalid. Print allowed
         // identifiers and abort.
         if (!gTestDrivers.AppendDriver(s))
         {
            std::string err = "The test driver identifier '" + s_orig + "' is invalid.\n";
            Mout << err;
            midas::test::TestDrivers::Help(Mout);
            Mout << std::endl;
            MIDASERROR(err + "Refer to output for help.");
         }
      }
      // If keyword, interpret it.
      // NB! This is legacy, but could be relevant in the future, so I'll leave
      // the framework for now. -MBH, Sep 2018.
      else
      {
         INPUT input = midas::input::FindKeyword(input_word, s);
         switch(input)
         {
            case ERROR: // Fall through.
            default:
            {
               MIDASERROR  (  "Keyword "+s_orig
                           +  " is not a Test level "+std::to_string(input_level)
                           +  " keyword."
                           );
            }
         }
      }
   }
   return s;
}

/**
 * Initialize Test variables
 **/
void InitializeTest
   (
   )
{
   gTestIoLevel         = I_0;
   gTestVectorDim       = I_10_4;
   gTestMatrixDimM      = I_10_2;
   gTestMatrixDimN      = I_10_2;
}
