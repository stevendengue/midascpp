#ifndef LAPLACEINPUT_H_INCLUDED
#define LAPLACEINPUT_H_INCLUDED

#include <string>

#include "inc_gen/TypeDefs.h"

#include "tensor/LaplaceInfo.h"

///>
bool LaplaceInput(std::istream& Minp, std::string& s, In input_level, LaplaceInfo::Set& laplaceinfo_set);

namespace detail 
{

//! Validate input
void ValidateLaplaceInput
   ( midas::tensor::LaplaceInfo&
   );

} /* namespace detail */
#endif /* LAPLACEINPUT_H_INCLUDED */
