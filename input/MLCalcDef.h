/**
************************************************************************
* 
* @file                MLCalcDef.h
*
* Created:             03-07-2018
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk) 
*
* Short Description:   Class for Machine Learning input 
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MLCALCDEF_H
#define MLCALCDEF_H

// Standard Headers
#include <string>
using std::string;
// Standard Headers
#include <vector>
using std::vector;

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"

#include "geoopt/CoordType.h"
using namespace geo_coordtype;

/**
* Construct a definition of a optimization
* */
class MLCalcDef
{

   private:
      /**
       * General
       **/
      std::string mCalcName;    ///< Calc name
      In mIoLevel;         ///< Io level for scf calculation.

      /**
       * Restart options 
       **/
       bool     mRestart;        ///< Restart 
      
      /**
       * Info on single points  
       **/ 
      std::string mDatabase;
      std::string mCoordFile;

      std::string mIcoordDefFile;
      bool mUseIcoordDef = false;

      /**
       * Info on modus for calculations
       **/ 
      bool mDoSample  = false;
      bool mDoGauPro  = false;

      Nb mGPRShift = 0.0;
      In mNumInducingPoints = -1;
      In mNumLayer = 1;

      bool mDoKmeans     = false;
      int  mNumCenter    = 1;

      bool mDoGauMixMod  = false;
      int  mNumMix = 1;

      int  mNumSample = 1;

      /**
       * Special flags for analysis mode
       **/  
      bool mOnlyCoordAna = false;
      bool mOnlySimCheck = false;

      /**
       * Coordinate type
       **/ 
      CoordType coordtype = MINT;

      /**
       * Reading an storing Covariance matrix
       **/    
      bool mSaveCovar = false;
      bool mReadCovar = false;
      std::string mFileCovarRead;
      std::string mFileCovarSave;  

      /**
       * Reading an storing weights
       **/    
      bool mSaveWeights = false;
      bool mReadWeights = false;
      std::string mFileWeightsRead;
      std::string mFileWeightsSave;  

      /**
       * Special flag for tesing SOAP kernel ... remove me later!!!
       */
      bool mDoSOAP = false;

      /**
       * Hyper parameter optimization
       **/
      bool mDoHOpt = true;
      std::string mFileHyperParam = "";

      bool mDoTrain = true;

      /**
       * Options influencing the prediction (algorithm..)
       */
      int mPredictMode = 0;

      In mMaxIter = 500;
      Nb mGradEps = 1e-2;
      Nb mFuncEps = 1e-3;  

      std::string mCoVarAlgo = "CHOL";

      /**
       * For saving information on internal coordinate handling 
       **/ 
      bool mDumpIcoordDef = false;
      std::string mDumpIcoordDefFile = "";


      /**
       * Information on the used kernel
       **/      
      std::vector<std::string> mKernel;

      Nb mNoise = 1.e-8;

      int mLmax = 8;

      bool mNormalizeKernel = false;

      bool mAdaptNoise = false;

      /**
       * Information on parametric mean functions
       **/   
      std::string mMeanFunction = "ZERO";
      std::vector<std::string> mMeanFunctionFiles;

      bool mUseNoDensity = false;
      Nb mTolSelect = 1e-4;
      In mMaxModeCombAdd = 6;

      bool mScaleSigma2 = false;

 
   public:
      MLCalcDef();                            
      ///< Constructor
//Gets and sets

//General
      void SetName(const string& arName) {mCalcName=arName;}
      ///< Set the calc name
      string GetName() const {return mCalcName;}
      ///< Get Name
      void SetIoLevel(const In& arIoLevel) {mIoLevel =arIoLevel;}
      ///< Set the IoLevel
      In GetIoLevel() const {return mIoLevel;}
      ///< Get the iolevel 

//Restart info 
      void SetRestart(bool aRestart) {mRestart=aRestart;}
      bool Restart() const {return mRestart;}

//Reading single points
      void SetDatabase(const string& arName) {mDatabase=arName;}
      string GetDatabase() const {return mDatabase;}

      void SetCoordFile(const string& arName) {mCoordFile=arName;}
      string GetCoordFile() const {return mCoordFile;}

//Options
      void SetCompModeSample(const bool& arDo)   {mDoSample  = arDo;}
      void SetCompModeGauPro(const bool& arDo)   {mDoGauPro  = arDo;}
      void SetCompModeSOAP(const bool& arDo)     {mDoSOAP    = arDo;}

      void SetKmeans(const int& arNum)    {mDoKmeans = true; mNumCenter = arNum;}
      void SetGauMixMod(const int& arNum) {mDoGauMixMod = true; mNumMix = arNum;}

      bool GetCompModeSample()   const {return mDoSample;}
      bool GetCompModeGauPro()   const {return mDoGauPro;}
      bool GetCompModeSOAP()     const {return mDoSOAP;}

      void SetOnlyCoordAna(const bool& arDo) {mOnlyCoordAna = arDo;}
      bool GetOnlyCoordAna()           const {return mOnlyCoordAna;}

      void SetOnlySimCheck(const bool& arDo) {mOnlySimCheck = arDo;}
      bool GetOnlySimCheck()           const {return mOnlySimCheck;}

      void SetCoordinateType(CoordType arType) {coordtype = arType;}
      CoordType GetCoordinateType() {return coordtype;}

      bool GetUseGauMixMod() {return mDoGauMixMod;}
      int  GetNumMix()       {return mNumMix;}

      bool GetUseKmeans()    {return mDoKmeans;}
      int  GetNumCenter()    {return mNumCenter;}

      int  GetNumberOfSamples()                 {return mNumSample;}
      void SetNumberOfSamples(const int& aNum)  {mNumSample = aNum;}

      void SetSaveCovar(const bool& arDo) {mSaveCovar = arDo;}
      void SetReadCovar(const bool& arDo) {mReadCovar = arDo;}
      bool GetSaveCovar() {return mSaveCovar;}
      bool GetReadCovar() {return mReadCovar;}
      void SetFileCovarSave(const string& arfile) {mFileCovarSave = arfile;}
      void SetFileCovarRead(const string& arfile) {mFileCovarRead = arfile;}
      std::string GetFileCovarSave() {return mFileCovarSave;}
      std::string GetFileCovarRead() {return mFileCovarRead;}

      void SetSaveWeights(const bool& arDo) {mSaveWeights = arDo;}
      void SetReadWeights(const bool& arDo) {mReadWeights = arDo;}
      bool GetSaveWeights() {return mSaveWeights;}
      bool GetReadWeights() {return mReadWeights;}
      void SetFileWeightsSave(const string& arfile) {mFileWeightsSave = arfile;}
      void SetFileWeightsRead(const string& arfile) {mFileWeightsRead = arfile;}
      std::string GetFileWeightsSave() {return mFileWeightsSave;}
      std::string GetFileWeightsRead() {return mFileWeightsRead;}

      void AddKernel(const string& arkern) {mKernel.push_back(arkern);}
      std::string GetKernel(const int& aIdx) {return mKernel[aIdx];}

      void SetNoise(const Nb& arNoise) {mNoise = arNoise;}
      Nb GetNoise() {return mNoise;}

      void SetLmax(const int& aLmax) {mLmax = aLmax;}
      int GetLmax() {return mLmax;}

      void SetDoHOpt(const bool& arDo) {mDoHOpt = arDo;}
      const bool GetDoHOpt()     {return mDoHOpt;}

      void SetDoTrain(const bool& arDo) {mDoTrain = arDo;}
      const bool GetDoTrain()           {return mDoTrain;}

      void SetPredictMode(const int& arNum)  {mPredictMode = arNum;}
      int  GetPredictMode()                  {return mPredictMode;}

      void SetCoVarAlgo(const std::string& arStr)  {mCoVarAlgo = arStr;}
      std::string GetCoVarAlgo(      )             {return mCoVarAlgo;}

      void SetIcoordDefFile(const std::string& arStr) {mIcoordDefFile = arStr;}
      std::string GetIcoordDefFile()                  {return mIcoordDefFile;}

      bool UseIcoordDefintion()                    {return mUseIcoordDef;}
      void SetUseIcoordDefintion(const bool& arDo) {mUseIcoordDef = arDo;}

      bool GetDumpIcoordDef()                   {return mDumpIcoordDef;}
      void SetDumpIcoordDef(const bool& arDo)   {mDumpIcoordDef = arDo;}

      std::string GetDumpIcoordDefFile()                  {return mDumpIcoordDefFile;}
      void SetDumpIcoordDefFile(const std::string& aStr)  {mDumpIcoordDefFile = aStr;}
  
      std::string GetFileHyperParam()                  {return mFileHyperParam;}
      void SetFileHyperParam(const std::string& aStr)  {mFileHyperParam = aStr;}

      std::string GetMeanFunction()                      {return mMeanFunction;}
      void SetMeanFunction(const std::string& aStr)      {mMeanFunction = aStr;}

      std::vector<std::string> GetMeanFunctionFiles()                   {return mMeanFunctionFiles;}
      void SetMeanFunctionFiles(const std::vector<std::string>& afiles) {mMeanFunctionFiles = afiles;}


      In   GetMaxIter()                {return mMaxIter;}
      void SetMaxIter(const In& aInt)  {mMaxIter = aInt;}

      Nb    GetGradEps()               {return mGradEps;}
      void  SetGradEps(const Nb& aNum) {mGradEps = aNum;}

      Nb    GetFuncEps()               {return mFuncEps;}
      void  SetFuncEps(const Nb& aNum) {mFuncEps = aNum;}

      bool GetUseNoDensity()                    {return mUseNoDensity;}
      void SetUseNoDensity(const bool& aYes)    {mUseNoDensity = aYes;}

      Nb   GetTolSelect()                 {return mTolSelect;}
      void SetTolSelect(const Nb& aNum)   {mTolSelect = aNum;}

      In   GetMaxModeCombAdd()               {return mMaxModeCombAdd;}
      void SetMaxModeCombAdd(const In& aIn)  {mMaxModeCombAdd = aIn;}

      Nb   GetGPRShift()                 {return mGPRShift;}
      void SetGPRShift(const Nb& aNum)   {mGPRShift = aNum;}

      In   GetNumInducingPoints()                  {return mNumInducingPoints;}
      void SetNumInducingPoints(const In& aInt)    {mNumInducingPoints = aInt;}

      bool GetmNormalizeKernel()                   {return mNormalizeKernel;}
      void SetmNormalizeKernel(const bool& aBool)  {mNormalizeKernel = aBool;}

      bool GetmAdaptNoise()                   {return mAdaptNoise;}
      void SetmAdaptNoise(const bool& aBool)  {mAdaptNoise = aBool;}

      bool GetScaleSigma2()                    {return mScaleSigma2;}
      void SetScaleSigma2(const bool& aYes)    {mScaleSigma2 = aYes;}

      In   GetNumLayer()                  {return mKernel.size();}

};

#endif
