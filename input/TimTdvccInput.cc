/**
 *******************************************************************************
 * 
 * @file    TimTdvccInput.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

// Standard headers:
#include <vector>
#include <map>
#include <algorithm>
#include <string>


// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/TimTdvccCalcDef.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "util/conversions/TupleFromString.h"
#include "input/FindKeyword.h"
#include "td/tdvcc/TimTdvccEnums.h"


/*
* Read and check TIM-TDVCC input.
* Read until next s = "$i..." i<3 which is returned.
*/
std::string TimTdvccInput
   (  std::istream& Minp
   )
{
   if (  gDoTimTdvcc
      && gDebug
      )
   {
      Mout << " I have read one TimTdvcc input - reading a new TimTdvcc input "<< endl;
   }

   gDoTimTdvcc = true;
   gTimTdvccCalcDef.push_back(TimTdvccCalcDef());   // Add to vector of TimTdvcc-calcs.
   TimTdvccCalcDef& new_timtdvcc = gTimTdvccCalcDef[gTimTdvccCalcDef.size()-I_1]; 

   // Enum/map with input flags
   enum INPUT
      {  ERROR
      ,  NAME
      ,  CORRMETHOD
      ,  MAXEXCI
      ,  TRANSFORMER
      ,  MODALBASISFROMVSCF
      ,  LIMITMODALBASIS
      ,  VCCGS
      ,  INITSTATE
      ,  OPER
      ,  PULSE
      ,  BASIS
      ,  IMAGTIME
      ,  IMAGTIMEHAULTTHR
      ,  IOLEVEL
      ,  TIMEIT
      ,  PRINTOUTINTERVAL
      ,  COMPAREFVCIVECS
      ,  ANALYZEHERMPARTJACVCCGS
      ,  INTEGRATOR
      ,  PROPERTIES
      ,  EXPECTATIONVALUES
      ,  STATISTICS
      // Spectra:
      ,  NOAUTOCORRCONVOLUTION
      ,  FFTPADLEVEL
      ,  NORMALIZESPECTRUM
      ,  SPECTRUMENERGYSHIFT
      ,  SPECTRUMOUTPUTSCREENING
      ,  SPECTRUMINTERVAL
      };
   const map<string,INPUT> input_word =
      {  {  {"#3ERROR"}, ERROR}
      ,  {  {"#3NAME"}, NAME}
      ,  {  {"#3CORRMETHOD"}, CORRMETHOD}
      ,  {  {"#3MAXEXCI"}, MAXEXCI}
      ,  {  {"#3TRANSFORMER"}, TRANSFORMER}
      ,  {  {"#3MODALBASISFROMVSCF"}, MODALBASISFROMVSCF}
      ,  {  {"#3LIMITMODALBASIS"}, LIMITMODALBASIS}
      ,  {  {"#3VCCGS"}, VCCGS}
      ,  {  {"#3INITSTATE"}, INITSTATE}
      ,  {  {"#3OPER"}, OPER}
      ,  {  {"#3PULSE"}, PULSE}
      ,  {  {"#3BASIS"}, BASIS}
      ,  {  {"#3IMAGTIME"}, IMAGTIME}
      ,  {  {"#3IMAGTIMEHAULTTHR"}, IMAGTIMEHAULTTHR}
      ,  {  {"#3IOLEVEL"}, IOLEVEL}
      ,  {  {"#3TIMEIT"}, TIMEIT}
      ,  {  {"#3PRINTOUTINTERVAL"}, PRINTOUTINTERVAL}
      ,  {  {"#3COMPAREFVCIVECS"}, COMPAREFVCIVECS}
      ,  {  {"#3ANALYZEHERMPARTJACVCCGS"}, ANALYZEHERMPARTJACVCCGS}
      ,  {  {"#3INTEGRATOR"}, INTEGRATOR}
      ,  {  {"#3PROPERTIES"}, PROPERTIES}
      ,  {  {"#3EXPECTATIONVALUES"}, EXPECTATIONVALUES}
      ,  {  {"#3STATISTICS"}, STATISTICS}
      ,  {  {"#3NOAUTOCORRCONVOLUTION"}, NOAUTOCORRCONVOLUTION}
      ,  {  {"#3FFTPADLEVEL"}, FFTPADLEVEL}
      ,  {  {"#3NORMALIZESPECTRUM"}, NORMALIZESPECTRUM}
      ,  {  {"#3SPECTRUMENERGYSHIFT"}, SPECTRUMENERGYSHIFT}
      ,  {  {"#3SPECTRUMOUTPUTSCREENING"}, SPECTRUMOUTPUTSCREENING}
      ,  {  {"#3SPECTRUMINTERVAL"}, SPECTRUMINTERVAL}
      };

   std::string s = "";
   std::string s_orig = "";
   In input_level = 3;
   bool already_read = false;
   while (  (already_read || midas::input::GetLine(Minp, s))
         && !midas::input::IsLowerLevelKeyword(s, input_level)
         )
   {
      already_read=false;

      s_orig = s;
      s = midas::input::ParseInput(s); // Transform to uppercase and delete blanks
      INPUT input = midas::input::FindKeyword(input_word, s);

      switch   (  input
               )
      {
         case NAME:
         {
            midas::input::GetLine(Minp, s);
            new_timtdvcc.SetName(s);
            break;
         }
         case CORRMETHOD:
         {
            midas::input::GetParsedLine(Minp, s);
            new_timtdvcc.SetCorrMethod(midas::tdvcc::EnumFromString<midas::tdvcc::CorrType>(s));
            break;
         }
         case MAXEXCI:
         {
            midas::input::GetLine(Minp, s);
            new_timtdvcc.SetMaxExci(midas::util::FromString<Uin>(s));
            break;
         }
         case TRANSFORMER:
         {
            midas::input::GetParsedLine(Minp, s);
            new_timtdvcc.SetTransformer(midas::tdvcc::EnumFromString<midas::tdvcc::TrfType>(s));
            break;
         }
         case MODALBASISFROMVSCF:
         {
            midas::input::GetLine(Minp, s);
            new_timtdvcc.SetModalBasisFromVscf(s);
            break;
         }
         case LIMITMODALBASIS:
         {
            midas::input::GetLine(Minp, s);
            new_timtdvcc.SetLimitModalBasis(midas::util::VectorFromString<Uin>(s));
            break;
         }
         case VCCGS:
         {
            midas::input::GetLine(Minp, s);
            new_timtdvcc.SetVccGs(s);
            break;
         }
         case INITSTATE:
         {
            midas::input::GetParsedLine(Minp, s);
            new_timtdvcc.SetInitState(midas::tdvcc::EnumFromString<midas::tdvcc::InitType>(s));
            //if (new_timtdvcc.InitState() == midas::tdvcc::InitType::VSCFREF)
            //{
            //   midas::input::GetLine(Minp, s);
            //   new_timtdvcc.SetInitStateVscfName(s);
            //}
            //else
            //{
            //   MIDASERROR("Don't know how to process INITSTATE input '"+s+"'.");
            //}
            break;
         }
         case OPER:
         {
            midas::input::GetLine(Minp, s);
            new_timtdvcc.SetOper(s);
            break;
         }
         case PULSE:
         {
            new_timtdvcc.SetUsePulse(true);
            midas::input::GetLine(Minp, s);
            new_timtdvcc.SetOperPulse(s);
            midas::input::GetLine(Minp, s);
            const auto v = midas::util::VectorFromString<Nb>(s);
            if (v.size() != 5)
            {
               std::stringstream ss;
               ss << "Expected v.size() == " << v.size() << " != 5; v = " << v;
               MIDASERROR(ss.str());
            }
            std::map<midas::td::GaussParamID,Nb> m;
            m[midas::td::GaussParamID::AMPL] = v.at(0);
            m[midas::td::GaussParamID::TPEAK] = v.at(1);
            m[midas::td::GaussParamID::SIGMA] = v.at(2);
            m[midas::td::GaussParamID::FREQ] = v.at(3);
            m[midas::td::GaussParamID::PHASE] = v.at(4);
            new_timtdvcc.SetPulseSpecs(m);
            break;
         }
         case BASIS:
         {
            midas::input::GetLine(Minp, s);
            new_timtdvcc.SetBasis(s);
            break;
         }
         case IMAGTIME:
         {
            new_timtdvcc.SetImagTime(true);
            break;
         }
         case IMAGTIMEHAULTTHR:
         {
            auto thr = midas::input::GetNb(Minp, s);
            new_timtdvcc.SetImagTimeHaultThr(thr);
            break;
         }
         case IOLEVEL:
         {
            auto io = midas::input::GetIn(Minp, s);
            new_timtdvcc.SetIoLevel(io);
            break;
         }
         case TIMEIT:
         {
            new_timtdvcc.SetTimeIt(true);
            break;
         }
         case PRINTOUTINTERVAL:
         {
            auto val = midas::input::GetNb(Minp, s);
            new_timtdvcc.SetPrintoutInterval(val);
            break;
         }
         case COMPAREFVCIVECS:
         {
            new_timtdvcc.SetCompareFvciVecs(true);
            break;
         }
         case ANALYZEHERMPARTJACVCCGS:
         {
            Uin val = midas::input::GetIn(Minp, s);
            new_timtdvcc.SetAnalyzeHermPartJacVccGs(std::make_pair(true,val));
            break;
         }
         case INTEGRATOR:
         {
            already_read = new_timtdvcc.ReadOdeInfo(Minp, s);
            break;
         }
         case PROPERTIES:
         {
            already_read = new_timtdvcc.ReadProperties(Minp, s);
            break;
         }
         case EXPECTATIONVALUES:
         {
            already_read = new_timtdvcc.ReadExptValOpers(Minp, s);
            break;
         }
         case STATISTICS:
         {
            already_read = new_timtdvcc.ReadStats(Minp, s);
            break;
         }
         case NOAUTOCORRCONVOLUTION:
         {
            new_timtdvcc.SetConvoluteAutoCorr(false);
            break;
         }
         case FFTPADLEVEL:
         {
            new_timtdvcc.SetPaddingLevel(midas::input::GetIn(Minp, s));
            break;
         }
         case NORMALIZESPECTRUM:
         {
            new_timtdvcc.SetNormalizeSpectrum(true);
            break;
         }
         case SPECTRUMENERGYSHIFT:
         {
            Nb e_shift = C_0;
            std::string type = "FIXED";

            midas::input::GetLine(Minp, s);
            auto s_vec = midas::util::StringVectorFromString(s);
            assert(s_vec.size() > 0);

            if (  s_vec[0] == "FIXED"
               )
            {
               assert(s_vec.size() == 2);
               type = s_vec[0];
               e_shift = midas::util::FromString<Nb>(s_vec[1]);
            }
            else if  (  s_vec[0] == "E0"
                     )
            {
               assert(s_vec.size() == 1);
               type = "E0";
               e_shift = C_0;
            }
            else
            {
               MIDASERROR("Unrecognized energy-shift type: " + s_vec[0]);
            }
            
            new_timtdvcc.SetSpectrumEnergyShift(std::pair<std::string, Nb>(type, e_shift));
            break;
         }
         case SPECTRUMOUTPUTSCREENING:
         {
            Nb thresh = midas::input::GetNb(Minp, s);
            new_timtdvcc.SetSpectrumOutputScreeningThresh(thresh);
            break;
         }
         case SPECTRUMINTERVAL:
         {
            midas::input::GetLine(Minp, s);
            auto [f0, f1] = midas::util::TupleFromString<Nb, Nb>(s);
            
            new_timtdvcc.SetSpectrumInterval(std::pair<Nb, Nb>(f0, f1));

            break;
         }
         case ERROR: // Fall through to default
         default:
         {
            Mout << " Keyword " << s_orig << " is not a TIMTDVCC level 3 Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }

   // Check sanity
   new_timtdvcc.SanityCheck();

   return s;
}

/**
 * Initialize TimTdvcc variables
 * */
void InitializeTimTdvcc()
{
   gDoTimTdvcc = false; 
}
