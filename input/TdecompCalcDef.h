/**
************************************************************************
* 
* @file                TdecompCalcDef.h
*
* Created:             30-05-2016
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk) 
*
* Short Description:   Class for a Tensor decomposition wf input-info handler 
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TDECOMPCALCDEF_H_INCLUDED
#define TDECOMPCALCDEF_H_INCLUDED

// Standard Headers
#include <string>
#include <vector>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "tensor/TensorDecompInfo.h"

/**
* Construct a definition of a  calculation
* */
class TdecompCalcDef
{
   private:
      /**
       * General
       **/
      std::string mCalcName;    ///< Calc name
      //string mBasis;       ///< basis name
      In mIoLevel;         ///< Io level for scf calculation.

      // for reading tensors
      std::string mTensorFileType;
      std::string mTensorFileName;
      
      midas::tensor::TensorDecompInfo::Set mDecompInfoSet;  ///< Tensor decomposition information

   public:
      TdecompCalcDef();                            
      ///< Constructor

//Gets and sets
      void SetTensorFileType(const std::string& arType) {mTensorFileType=arType;}
      void SetTensorFileName(const std::string& arName) {mTensorFileName=arName;}

      std::string GetTensorFileType() const {return mTensorFileType;}
      std::string GetTensorFileName() const {return mTensorFileName;}

//General
      void SetName(const string& arName) {mCalcName=arName;}
      ///< Set the calc name
      string GetName() const {return mCalcName;}
      ///< Get Name
      void SetIoLevel(const In& arIoLevel) {mIoLevel =arIoLevel;}
      ///< Set the IoLevel
      In GetIoLevel() const {return mIoLevel;}
      ///< Get the iolevel 

//Decompinfo
      bool ReadDecompInfoSet(std::istream&, std::string&);
      midas::tensor::TensorDecompInfo::Set& GetDecompInfoSet() { return mDecompInfoSet; }
 
};

#endif   /* TDECOMPCALCDEF_H_INCLUDED */

