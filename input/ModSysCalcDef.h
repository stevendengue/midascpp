/**
************************************************************************
* 
* @file                ModSysCalcDef.h
*
* 
* Created:             28-01-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk) 
*
* Short Description:   Calculation definition for generation of coordinates
* 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef MODSYSCALCDEF_H
#define MODSYSCALCDEF_H

// std headers
#include <string>
#include <vector>
#include <memory>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "input/RotCoordCalcDef.h"
#include "input/TransformPotCalcDef.h"
#include "input/FalconCalcDef.h"
#include "input/FreqAnaCalcDef.h"
#include "input/LhaCalcDef.h"


// using declarations
using std::string;
using std::vector;

enum class VibCoordScheme: int
{  ERROR = 0
,  ROTCOORD                   // Rotate rectiliear coordinates to obtain localized, optimized, HOLC, etc. coordiates
,  FALCON                     // Obtain FALCON coordinates
,  ROTATEMOLECULE             // Rotate the molecule
,  FREQANA                    // Do frequency analysis
,  TRANSFORMPOT               // Transform potential surface
,  LHA                        // Diagonalize and transform harmonic potentials (LHA = Local Harmonic Approximation)
};

/* ************************************************************************
************************************************************************ */
class ModSysCalcDef
{
   private:
      std::vector<std::string>     mSystemNames;
      bool                         mDoGeoOpt;
      bool                         mDoVibCoordOpt;
      bool                         mRemoveGlobalTR;
      bool                         mRotateMolecule;
      bool                         mSymmetryDefined;
      VibCoordScheme               mVibCoordScheme;
      RotCoordCalcDef              mRotCoordCalcDef;
      FalconCalcDef                mFalconCalcDef;
      FreqAnaCalcDef               mFreqAnaCalcDef;
      TransformPotCalcDef          mTransformPotCalcDef;
      LhaCalcDef                   mLhaCalcDef;
      std::string                  mAxis;
      std::string                  mPointGroup;
      std::string                  mSymmetryAxis;
      Nb                           mAngle;
   public:
      //constructor/destructor
      ModSysCalcDef(): mDoGeoOpt(false),
             mDoVibCoordOpt(false), 
             mRemoveGlobalTR(false),
             mRotateMolecule(false),
             mSymmetryDefined(false),
             mVibCoordScheme(VibCoordScheme::ERROR),
             mAxis("x"),
             mAngle(C_0)
             {;}


      ~ModSysCalcDef() {;}

      // Set stuff
      void SetDoVibCoordOpt(const bool& arVibCoordOpt = true)     {mDoVibCoordOpt = arVibCoordOpt;}
      void SetRemoveGlobalTR(const bool& arBool = true)           {mRemoveGlobalTR = arBool;}
      void SetRotateMolecule(const bool& aRotateMolecule = true)  {mRotateMolecule = aRotateMolecule;}
      void SetVibCoordScheme(const std::string&);
      void ReserveSystemNames(const In& arIn)                     {mSystemNames.reserve(arIn);}
      void AddSysName(const std::string& arName)                  {mSystemNames.push_back(arName);}
      void SetAxis(const std::string& arAxis)                     {mAxis = arAxis;}
      void SetAngle(Nb arAngle){mAngle=arAngle;}
      void SetmPointGroup(std::string aPointGroup)                {mPointGroup = aPointGroup;}
      void SetmSymmetryAxis(std::string aSymmetryAxis)            {mSymmetryAxis = aSymmetryAxis;}
      void SetmSymmetryDefined(bool aBool)                        {mSymmetryDefined = aBool;}
      void AddAxisAngle(const std::string& arAxis, Nb arAngle)    
      {
         Mout << arAxis << " " << arAngle << std::endl; 
         mAxis = arAxis; 
         mAngle = arAngle;
      } 
   

      //Get stuff
      bool DoUpdatePotential() {return (mFalconCalcDef.GetpRotCoordCalcDef()->NMeasures() > 0);}

      // Get stuff
      FreqAnaCalcDef* GetpFreqAnaCalcDef()                        {return &mFreqAnaCalcDef;}
      FalconCalcDef* GetpFalconCalcDef()                          {return &mFalconCalcDef;}
      RotCoordCalcDef* GetpRotCoordCalcDef()                      {return &mRotCoordCalcDef;}
      TransformPotCalcDef* GetpTransformPotCalcDef()              {return &mTransformPotCalcDef;}
      const LhaCalcDef* const pLhaCalcDef() const                 {return &mLhaCalcDef;}
      LhaCalcDef* pLhaCalcDef()                                   {return &mLhaCalcDef;}
      VibCoordScheme GetVibCoordScheme()                          {return mVibCoordScheme;}
      bool DoGeoOpt()                                             {return mDoGeoOpt;}
      bool DoVibCoordOpt()                                        {return mDoVibCoordOpt;}
      bool RemoveGlobalTR()                                       {return mRemoveGlobalTR;}
      bool RotateMolecule()                                       {return mRotateMolecule;}
      bool GetmSymmetryDefined() const                            {return mSymmetryDefined;}
      const std::string& GetAxis() const                          {return mAxis;}
      std::string GetmPointGroup() const                          {return mPointGroup;}
      std::string GetmSymmetryAxis() const                        {return mSymmetryAxis;}
      const std::vector<std::string>& GetSysNames()                      {return mSystemNames;} 
      Nb GetAngle() const                                         {return mAngle;}
};

#endif //MODSYSCALCDEF_H
