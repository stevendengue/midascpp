/**
 *******************************************************************************
 * 
 * @file    TimTdvccCalcDef.cc
 * @date    12-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TIMTDVCCCALCDEF_H
#define TIMTDVCCCALCDEF_H

// Standard Headers
#include <string>
#include <utility>
#include <set>
#include <vector>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "input/VscfCalcDef.h"
#include "input/TdPropertyDef.h"
#include "ode/OdeInfo.h"
#include "td/tdvcc/TimTdvccEnums.h"
#include "td/oper/GaussPulse.h"

/**
 * Construct a definition of a TimTdvcc calculation
 **/
class TimTdvccCalcDef
   :  public TdPropertyDef
{
   public:
      //! Constructor
      TimTdvccCalcDef() = default;

      //! Sanity check. Check that input makes sense.
      void SanityCheck
         (
         );

      //! Read OdeInfo
      bool ReadOdeInfo
         (  std::istream&
         ,  std::string&
         );

      midas::ode::OdeInfo GetOdeInfo() const {return mOdeInfo;}

      //! Macro for Getter and Setter interface
      template<typename T> struct argument_type;
      template<typename T, typename U> struct argument_type<T(U)> { using type = U; };
      #define UNPACK(...) __VA_ARGS__
      #define GETSET(NAME, TYPE) \
         TimTdvccCalcDef& Set##NAME(const argument_type<void(TYPE)>::type& a##NAME) \
         { \
            m##NAME = a##NAME; \
            return *this; \
         } \
           \
         const argument_type<void(TYPE)>::type& NAME() const \
         { \
            return m##NAME; \
         }
      
         GETSET(Name, std::string);
         GETSET(CorrMethod, midas::tdvcc::CorrType);
         GETSET(MaxExci, Uin);
         GETSET(Transformer, midas::tdvcc::TrfType);
         GETSET(ModalBasisFromVscf, std::string);
         GETSET(LimitModalBasis, std::vector<Uin>);
         GETSET(VccGs, std::string);
         GETSET(InitState, midas::tdvcc::InitType);
         GETSET(Oper, std::string);
         GETSET(Basis, std::string);
         GETSET(ImagTime, bool);
         GETSET(ImagTimeHaultThr, Nb);
         GETSET(IoLevel, Uin);
         GETSET(TimeIt, bool);
         GETSET(PrintoutInterval, Nb);
         GETSET(CompareFvciVecs, bool);
         GETSET(UsePulse, bool);
         GETSET(OperPulse, std::string);
         GETSET(PulseSpecs, UNPACK(std::map<midas::td::GaussParamID,Nb>));
         GETSET(AnalyzeHermPartJacVccGs, UNPACK(std::pair<bool,Uin>));
      #undef GETSET
      #undef UNPACK

      bool LoadVccGs() const
      {
         using midas::tdvcc::EnumFromString;
         using midas::tdvcc::StatID;
         using midas::tdvcc::InitType;
         bool val = false;
         for(const auto& id: this->Stats())
         {
            if (EnumFromString<StatID>(id) == StatID::DNORM2VCCGS)
            {
               val = true;
            }
         }
         if (this->InitState() == InitType::VCCGS)
         {
            val = true;
         }
         return val;
      }

   private:
      std::string mName = "timtdvcc";
      midas::tdvcc::CorrType mCorrMethod = midas::tdvcc::CorrType::VCC;
      Uin mMaxExci = 2;
      midas::tdvcc::TrfType mTransformer = midas::tdvcc::TrfType::VCC2H2;
      std::string mModalBasisFromVscf = "vscf";
      std::vector<Uin> mLimitModalBasis = {6};
      std::string mVccGs = "vcc";
      midas::tdvcc::InitType mInitState = midas::tdvcc::InitType::VSCFREF;
      std::string mOper = "oper";
      std::string mBasis = "basis";
      bool mImagTime = false;
      Nb mImagTimeHaultThr = 0.0;
      Uin mIoLevel = 1;
      bool mTimeIt = false;
      Nb mPrintoutInterval = 0.0;
      bool mCompareFvciVecs = false;
      bool mUsePulse = false;
      std::string mOperPulse = "pulse";
      std::map<midas::td::GaussParamID,Nb> mPulseSpecs =
         {  {midas::td::GaussParamID::AMPL, 1.0}
         ,  {midas::td::GaussParamID::TPEAK, 0.0}
         ,  {midas::td::GaussParamID::SIGMA, 1.0}
         ,  {midas::td::GaussParamID::FREQ, 0.0}
         ,  {midas::td::GaussParamID::PHASE, 0.0}
         };
      std::pair<bool,Uin> mAnalyzeHermPartJacVccGs = {false,0};

      //! Info on integrator, time interval, etc.
      midas::ode::OdeInfo mOdeInfo;
};

//! Ostream
std::ostream& operator<<
   (  std::ostream&
   ,  const TimTdvccCalcDef&
   );

#endif /* TIMTDVCCCALCDEF_H */

