/**
************************************************************************
* 
* @file                EcorCalcDef.h
*
* Created:             18-02-2014
*
* Author:              Ove Christiansen (ove@chem.au.dk) 
*
* Short Description:   Class for a Elec Ecor wf input-info handler 
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ECORCALCDEF_H
#define ECORCALCDEF_H

// Standard Headers
#include <string>
#include <vector>

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "tensor/TensorDecompInfo.h"
#include "input/LaplaceInput.h"

using std::string;
using std::vector;

/**
* Construct a definition of a  calculation
* */
class EcorCalcDef
{
   private:
      /**
       * General
       **/
      string mCalcName;    ///< Calc name
      //string mBasis;       ///< basis name
      In mIoLevel;         ///< Io level for scf calculation.

      /**
       * Restart options 
       **/
       bool     mRestart;               ///< Restart 
      
      /**
       * Macro Solver
       **/
      In mMacroMaxIter;    ///< Maximum Number of macro iterations
      
      /**
       * Tensor decomposition info ? out in seperate class...
       **/
      // Not the first three one should be removed and only mDecompInfoSet should be used in the future
      bool mDoCP;
      Nb mCpAlsDiffThr;     ///< Threshold for difference between iterations for cpals fit 
      In mCpAlsMaxIter;     ///< Max Nr Iterations during CpAls iterations

      midas::tensor::TensorDecompInfo::Set mDecompInfoSet;  ///< Tensor decomposition information

      /**
       * RI-MP2 info
       **/
      bool mDoRIMP2;
      bool mUseOvlpMetric = true;    ///< Switch for using the Overlap Metric for density fitting
      string mPTAlgo;                ///< Which RI-MP2 algorithm to use

      /**
       * C2T/T2C and ri_svd info
       **/
      Nb mThrSVD = 1.e-12;  ///< threshold for ri_svd decomposition
      Nb mThrErr = 1.e-5;   ///< maximum error in C2T/T2C algorithm 

      /**
       * info for Laplace quadrature
       **/
      Nb mLapConv = 0.00000001;  ///< threshold for the Laplace error function

      midas::tensor::LaplaceInfo::Set mLaplaceInfoSet;

      /**
       * Do I have files from Dalton?
       * */ 
      bool mNoDalton;

      /**
       * Coupled Cluster model info
       **/
      string mCCModel;     // Default is CCSD

   /* 
   * Reference State Information
   */
      In mNAux;             ///< Nr of auxiliary basis functions
      In mNBas;             ///< Nr of basis functions
      In mNOrb;             ///< Nr of orbitals
      In mNOcc;             ///< Nr of occupied orbitals (not including frozen core)  
      In mNVir;             ///< Nr of virtual orbitals (not including frozen virtuals). 
      In mNFrozenOcc;       ///< Nr of frozen core/frozen occupied  orbitals 
      In mNFrozenVir;       ///< Nr of frozen virtual orbitals 

      string mOrbitalsInfoFile;
      string mOrbitalsFile;
      string mEnergiesFile;
      string mOverlapIntegralsFile;
      string mOneElectronIntegralsFile;
      string mTwoElectronIntegralsFile;
 
   public:
      EcorCalcDef();                            
      ///< Constructor
//Gets and sets

//General
      void SetName(const string& arName) {mCalcName=arName;}
      ///< Set the calc name
      string GetName() const {return mCalcName;}
      ///< Get Name
      void SetIoLevel(const In& arIoLevel) {mIoLevel =arIoLevel;}
      ///< Set the IoLevel
      In GetIoLevel() const {return mIoLevel;}
      ///< Get the iolevel 

//Restart info 
      void SetRestart(bool aRestart) {mRestart=aRestart;}
      bool Restart() const {return mRestart;}

//Tensor decomposition info 
      void SetDoCP(const bool& do_cp) {mDoCP = do_cp;}
      bool GetDoCP() const {return mDoCP;}
      void SetCpAlsDiffThr(const Nb& aNb) {mCpAlsDiffThr=aNb;} 
      Nb CpAlsDiffThr() {return mCpAlsDiffThr;} 
      void SetCpAlsMaxIter(const In& aM) {mCpAlsMaxIter=aM;} 
      In CpAlsMaxIter() {return mCpAlsMaxIter;} 

//RIMP2 settings
      void SetDoRIMP2(const bool& do_rimp2)     {mDoRIMP2 = do_rimp2;}
      bool GetDoRIMP2()                   const {return mDoRIMP2;}

      void SetUseOvlpMetric(const bool& useovlp){mUseOvlpMetric = useovlp; }
      bool GetUseOvlpMetric()             const {return mUseOvlpMetric;}

      void SetPTAlgo(const string& pt_algo)     {mPTAlgo = pt_algo;}
      string GetPTAlgo()                  const {return mPTAlgo;}

//macro solver
      void SetMacroMaxIter(const In& arMaxIter) {mMacroMaxIter =arMaxIter;}
      ///< Set the maximum nr. of iterations
      In GetMacroMaxIter() {return mMacroMaxIter;}
      ///< Get the maximem nr. of iterations

//CC model
      void SetCCModel(const string& cc_model) {mCCModel = cc_model;}
      string GetCCModel() const {return mCCModel;}

//Basis info
      In NAux      () const{return mNAux      ;};
      In NBas      () const{return mNBas      ;};
      In NOrb      () const{return mNOrb      ;};
      In NOcc      () const{return mNOcc      ;};
      In NVir      () const{return mNVir      ;};
      In NFrozenOcc() const{return mNFrozenOcc;};
      In NFrozenVir() const{return mNFrozenVir;};

      bool HaveDalton () const {return mNoDalton;};

      const string& OrbitalsInfoFile        () const {return mOrbitalsInfoFile        ;};
      const string& OrbitalsFile            () const {return mOrbitalsFile            ;};
      const string& EnergiesFile            () const {return mEnergiesFile            ;};
      const string& OverlapIntegralsFile    () const {return mOverlapIntegralsFile    ;};
      const string& OneElectronIntegralsFile() const {return mOneElectronIntegralsFile;};
      const string& TwoElectronIntegralsFile() const {return mTwoElectronIntegralsFile;};

      friend string ElecInput(std::istream& Minp); 
     

// C2T/T2C and ri_svd info
      Nb GetThrSVD() const{return mThrSVD;};
      Nb GetThrErr() const{return mThrErr;};
      void SetThrSVD(const Nb& aNb) {mThrSVD=aNb;} 
      void SetThrErr(const Nb& aNb) {mThrErr=aNb;} 

// Info for Laplace quadrature
      Nb GetLapConv() const{return mLapConv;};
      void SetLapConv(const Nb& aNb) {mLapConv=aNb;} 

// Decompinfo
 
      bool ReadEcorDecompInfoSet(std::istream&, std::string&);
      midas::tensor::TensorDecompInfo::Set& GetDecompInfoSet() { return mDecompInfoSet; }

// Laplace

      bool ReadLaplaceInfoSet(std::istream&, std::string&);
      midas::tensor::LaplaceInfo::Set& GetLaplaceInfoSet() { return mLaplaceInfoSet; }
 

};

#endif
