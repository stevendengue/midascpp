/**
************************************************************************
* 
* @file                ThermalSpectrumDef.cc
*
* Created:             18-11-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   Base class for a thermal spectrum
* 
* Last modified: Fri Mar 11, 2011  08:41PM mbh
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <string>
#include <vector>
#include <set>
#include <map>

// midas headers
#include "input/Input.h"
#include "util/Io.h"
#include "util/conversions/FromString.h"
#include "util/Error.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/ThermalSpectrumDef.h"

// using declarations
using std::vector;
using std::string;
using std::set;
using std::map;

ThermalSpectrumDef::ThermalSpectrumDef()
{
   mOps.clear();
   mTemp=C_0;
   mMaxFrq=C_0;
   mNstep=I_0;
   mGamma=C_0;
   mPrefix="ThermalSpectrum_";
}

ThermalSpectrumDef::~ThermalSpectrumDef()
{
   mOps.clear();
}

void ThermalSpectrumDef::Setup(const std::map<string,string>& aM)
{
   CheckKeys(aM);
   InitOpers(aM);
   InitTemp(aM);
   InitMaxFrq(aM);
   InitNstep(aM);
   InitGamma(aM);
   mPrefix+=StringForNb(mMaxFrq);
   mPrefix+="_"+StringForNb(mGamma);
   mPrefix+="_"+StringForNb(mTemp);
   mPrefix+="_"+std::to_string(mNstep);
}

void ThermalSpectrumDef::CheckKeys(const std::map<string,string>& aM)
{
   set<string> all_keys;
   all_keys.insert("OPERS");
   all_keys.insert("GAMMA");
   all_keys.insert("TEMPERATURE");
   all_keys.insert("MAX_FRQ");
   all_keys.insert("NSTEPS");
   // now perform check
   map<string,string>::const_iterator map_it;
   set<string>::iterator keys_it;
   bool found_error=false;
   for(map_it=aM.begin();map_it!=aM.end();map_it++) {
      keys_it=all_keys.find(map_it->first);
      if(keys_it==all_keys.end()) {
         found_error=true;
         break;
      }
   }
   if(!found_error)
      return;
   Mout << "Unknown keyword detected: " << map_it->first << endl;
   Mout << "Possible values are:" << endl;
   for(keys_it=all_keys.begin();keys_it!=all_keys.end();keys_it++)
   {
      Mout << *keys_it << endl;
   }
   MIDASERROR("Unknown keyword detected, see above.");
}

void ThermalSpectrumDef::InitOpers(const std::map<string,string>& aM)
{
   map<string,string>::const_iterator it = aM.find("OPERS");
   if (it == aM.end())
   {
      MIDASERROR(" No operators specified.");
   }
   const string& s = it->second;
   if (s[0] != '(' || s[s.size()-1] != ')')
   {
      MIDASERROR(" Operators must be enclosed in (...).");
   }
   string opstr = s.substr(1,s.size()-2);
   vector<string> ops = SplitString(opstr, ",");
   if (ops.size() != 3)
   {
      Mout << " Only " << ops.size() << " operators specified. Rest assumed zero." << endl;
   }
   mOps = ops;
}

void ThermalSpectrumDef::InitTemp(const std::map<string,string>& aM)
{
   map<string,string>::const_iterator it = aM.find("TEMPERATURE");
   if (it == aM.end()) {
      Mout << "Warning: No temperature specified for thermal spectrum. Assume 0 K." << endl;
      return;
   }

   // remove eventual ( )
   string s = it->second;
   if (s[0] == '(' && s[s.size()-1] == ')')
   {
      s = s.substr(1,s.size()-2);
   }

   mTemp = midas::util::FromString<Nb>(s);
}

void ThermalSpectrumDef::InitMaxFrq(const std::map<string,string>& aM)
{
   map<string,string>::const_iterator it = aM.find("MAX_FRQ");
   if (it == aM.end())
   {
      MIDASERROR("Max frq. not specified.");
   }

   // remove eventual ( )
   string s = it->second;
   if (s[0] == '(' && s[s.size()-1] == ')')
   {
      s = s.substr(1,s.size()-2);
   }

   mMaxFrq = midas::util::FromString<Nb>(s);
}

void ThermalSpectrumDef::InitGamma(const std::map<string,string>& aM)
{
   map<string,string>::const_iterator it = aM.find("GAMMA");
   if (it == aM.end()) 
   {
      Mout << "Warning: No gamma specified for thermal spectrum. Assume 0 cm^-1." << endl;
      return;
   }

   // remove eventual ( )
   string s = it->second;
   if (s[0] == '(' && s[s.size()-1] == ')')
   {
      s=s.substr(1,s.size()-2);
   }

   mGamma = midas::util::FromString<Nb>(s);
}

void ThermalSpectrumDef::InitNstep(const std::map<string,string>& aM)
{     
   map<string,string>::const_iterator it = aM.find("NSTEPS");
   if (it == aM.end()) 
   {
      MIDASERROR("Number of steps not specified.");
      return;
   }

   // remove eventual ( )
   string s = it->second;
   if (s[0] == '(' && s[s.size()-1] == ')')
   {
      s = s.substr(1,s.size()-2);
   }
   
   mNstep = midas::util::FromString<In>(s);
}
