/**
************************************************************************
* 
* @file                TensorDecompDef.h
*
* Created:             28-10-2011
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Store input logical information 
* 
* Last modified: Mon Jul 12, 2011  11:19AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef TENSORDECOMPDEF_H_INCLUDED
#define TENSORDECOMPDEF_H_INCLUDED

// std headers
#include<vector>
#include<string>
#include<iostream>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "input/FindKeyword.h"
#include "util/Io.h"

enum class DECOMPCALCTYPE { NONE, VCC_BENCHMARK, RSP_BENCHMARK, RSP_EIG, ERROR };

struct DECOMPCALCTYPE_MAP
{
   static const std::map<DECOMPCALCTYPE,std::string> ENUM_TO_STRING;

   static const std::map<std::string,DECOMPCALCTYPE> STRING_TO_ENUM;
};

class TensorDecompDef
{
   private:
      DECOMPCALCTYPE mType;

      std::string mName;

      size_t mCpRank;
      Nb     mCpAlsThreshold;
      size_t mCpAlsMaxIter;

      size_t mFindCpMaxRank;
      Nb     mFindCpResThreshold;

      size_t mDecompToExciLvl;

      Nb     mPseudoInverseThreshold;

      bool   mSingleCpAnalysis;
      size_t mSingleCpAnalysisMaxRank;

      bool                mSpecificCpAnalysis;
      std::vector<size_t> mSpecificCpAnalysisRanks;

      bool mDoCgDecomp;

      bool mSaveDecomp;
      
   public:
      TensorDecompDef(); // default ctor
      
      void SetType(DECOMPCALCTYPE aType) 
      {
         if(mType == DECOMPCALCTYPE::NONE)
         { 
            mType = aType;
         }
         else
         {
            MIDASERROR("INPUT ERROR REDEFINING DECOMPTYPE");
         }
      }

      void SetTypeFromString(std::string& aTypeString) 
      {
         SetType(midas::input::FindKeyword(DECOMPCALCTYPE_MAP::STRING_TO_ENUM,aTypeString));
         if(mType == DECOMPCALCTYPE::ERROR)
         {
            MIDASERROR("TENSORDECOMP TYPE "+aTypeString+" NOT SUPPORTED");
         }
      }

      DECOMPCALCTYPE GetType() const { return mType; }
      // version for better syntax
      DECOMPCALCTYPE Type() const { return mType; }
      
      void SetName(const std::string& aName) { mName = aName; }
      const std::string& GetName() const { return mName; }

      void   SetCpRank(size_t aCpRank) { mCpRank = aCpRank; }
      size_t GetCpRank() const         { return mCpRank; }

      void SetCpAlsThreshold(Nb aCpAlsThreshold) { mCpAlsThreshold = aCpAlsThreshold; }
      Nb   GetCpAlsThreshold() const          { return mCpAlsThreshold; }

      void   SetCpAlsMaxIter(size_t aCpAlsMaxIter) { mCpAlsMaxIter = aCpAlsMaxIter; }
      size_t GetCpAlsMaxIter() const            { return mCpAlsMaxIter; }

      void   SetFindCpMaxRank(size_t aFindCpMaxRank) { mFindCpMaxRank = aFindCpMaxRank; }
      size_t GetFindCpMaxRank() const                { return mFindCpMaxRank; }

      void SetFindCpResThreshold(Nb aFindCpResThreshold) { mFindCpResThreshold = aFindCpResThreshold; }
      Nb   GetFindCpResThreshold() const                 { return mFindCpResThreshold; }
      
      void   SetDecompToExciLvl(size_t aDecompToExciLvl) { mDecompToExciLvl = aDecompToExciLvl; }
      size_t GetDecompToExciLvl() const                  { return mDecompToExciLvl; }
      
      void SetPseudoInverseThreshold(Nb aPseudoInverseThreshold) 
      { mPseudoInverseThreshold = aPseudoInverseThreshold; }
      Nb   GetPseudoInverseThreshold() const                     
      { return mPseudoInverseThreshold; }
      
      void SetSingleCpAnalysis(bool aSingleCpAnalysis) { mSingleCpAnalysis = aSingleCpAnalysis; }
      bool GetSingleCpAnalysis() const                 { return mSingleCpAnalysis; }
      
      void SetSingleCpAnalysisMaxRank(size_t aSingleCpAnalysisMaxRank)
      { mSingleCpAnalysisMaxRank = aSingleCpAnalysisMaxRank; }
      size_t GetSingleCpAnalysisMaxRank() const
      { return mSingleCpAnalysisMaxRank; }

      void SetSpecificCpAnalysis(bool aSpecificCpAnalysis) { mSpecificCpAnalysis = aSpecificCpAnalysis; }
      bool GetSpecificCpAnalysis() const                   { return mSpecificCpAnalysis; }
      
      void PushBackSpecificCpAnalysisRanks(size_t aRank) { mSpecificCpAnalysisRanks.push_back(aRank); }
      const std::vector<size_t>& GetSpecificCpAnalysisRanks() const
      { return mSpecificCpAnalysisRanks; }
      
      void SetDoCgDecomp(bool aDoCgDecomp) { mDoCgDecomp = aDoCgDecomp; }
      bool GetDoCgDecomp() const           { return mDoCgDecomp; }
      
      void SetSaveDecomp(bool aSaveDecomp) { mSaveDecomp = aSaveDecomp; }
      bool GetSaveDecomp() const           { return mSaveDecomp; }

      void Output(std::ostream&) const;

      friend std::ostream& operator<<(std::ostream&, const TensorDecompDef&);
};


#endif /* TENSORDECOMPDEF_H_INCLUDED */
