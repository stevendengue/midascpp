/**
************************************************************************
* 
* @file                TotalLinRsp.cc
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   TotalLinRsp datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// Standard Headers
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <exception>
#include <list>

#include "inc_gen/math_link.h"

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "util/conversions/FromString.h"
#include "input/TotalLinRsp.h"
#include "input/TotalResponseDrv.h"
#include "input/TotalResponseContribution.h"
#include "input/OpInfo.h"

using std::string;
using std::vector;
using std::map;
using std::make_pair;
using std::exception;
using std::list;

#define DBG(ARG) ARG
//#define DBG(ARG) void(0);

static In PRECISION=8;


/**
* Constructor, no action yet...
* */
TotalLinRsp::TotalLinRsp(const std::map<string,string>& aM) : TotalResponseDrv(aM)
{
   Mout << "Entering total lin rsp..." << endl;
   CheckKeys(aM);
   InitFrq(aM);
   InitExperimental(aM);
   InitContributions(aM);
   InitTypes(aM);
   InitRules(aM);
}

void TotalLinRsp::CheckKeys(const std::map<string,string>& aM)
{
   set<string> all_keys;
   all_keys.insert("ORDER");
   all_keys.insert("PVL");
   all_keys.insert("ZPVC");
   all_keys.insert("FRQ");
   all_keys.insert("CONTRIBUTIONS");
   all_keys.insert("SET_VALUE");
   all_keys.insert("EXPERIMENTAL");
   // now perform check
   map<string,string>::const_iterator map_it;
   set<string>::iterator keys_it;
   bool found_error=false;
   for(map_it=aM.begin();map_it!=aM.end();map_it++) {
      keys_it=all_keys.find(map_it->first);
      if(keys_it==all_keys.end()) {
         found_error=true;
         break;
      }
   }
   if(!found_error)
      return;
   Mout << "Unknown keyword detected: " << map_it->first << endl;
   Mout << "Possible values are:" << endl;
   for(keys_it=all_keys.begin();keys_it!=all_keys.end();keys_it++)
      Mout << *keys_it << endl;
   ConstructionError("Unknown keyword detected, see above.",aM);
}

void TotalLinRsp::InitExperimental(const std::map<string,string>& aM)
{
   map<string,string>::const_iterator it = aM.find("EXPERIMENTAL");
   if (it != aM.end() && StringNoCaseCompare(it->second,"TRUE"))
   {
      // experimental quantities
      string iso="(1/3)*(XX+YY+ZZ)";
      string aniso="(1/2)*sqrt((XX-YY)^(2)+(XX-ZZ)^(2)+(YY-ZZ)^(2))";
      mTotalRspString.push_back(iso);
      mTotalRspString.push_back(aniso);
   }
}

void TotalLinRsp::InitContributions(const std::map<string,string>& aM)
{
   map<string,string>::const_iterator it = aM.find("CONTRIBUTIONS");
   if(it==aM.end())
      return;
   // look for contributions.
   const string& s = it->second;
   if (s[0] != '(' || s[s.size()-1] != ')')
      ConstructionError(" Contributions must be enclosed in (...).", aM);
   string contrib_string = s.substr(1,s.size()-2);
   vector<string> contribs = SplitString(contrib_string, ",");
   for(In i=0;i<contribs.size();i++) {
      if(contribs[i].find_first_of("XYZ")==contribs[i].npos)
         continue;
      mContribSet.insert(contribs[i]);
   }
}

void TotalLinRsp::InitTypes(const map<string,string>& aM)
{
   mPvl=true;
   mZpvc=true;
   map<string,string>::const_iterator it = aM.find("PVL");
   if (it != aM.end() && StringNoCaseCompare(it->second,"FALSE")) {
      mPvl=false;
   }
   it = aM.find("ZPVA");
   if (it != aM.end() && StringNoCaseCompare(it->second,"FALSE")) {
      mZpvc=false;
   }
   if (!mPvl && !mZpvc) {
      ConstructionError("Either PVL, ZPVC, or both must be set. Both set by default.",aM);
   }
}

void TotalLinRsp::InitFrq(const map<string,string>& aM)
{
   map<string,string>::const_iterator it = aM.find("FRQ");
   if (it == aM.end())
   {
      ConstructionError(" No frequency specified.", aM);
   }

   mFrq = midas::util::FromString<Nb>(it->second);
}

void TotalLinRsp::InitRules(const map<string,string>& aM)
{
   if(!mIso)
      return;
   map<string,string>::const_iterator it = aM.find("SET_VALUE");
   if (it == aM.end())
      return;
   // look for rules.
   const string& s = it->second;
   if (s[0] != '(' || s[s.size()-1] != ')')
      ConstructionError(" Values must be enclosed in (...).", aM);
   string rule_string = s.substr(1,s.size()-2);
   vector<string> rule_vec = SplitString(rule_string, ",");
   for(In i=0;i<rule_vec.size();i++) {
      vector<string> equal_contribs=SplitString(rule_vec[i],"=");
      // for each rule, check for initializing values,
      // i.e. xx=yy=0.0
      bool initial_val=false;
      Nb val = 0.0;
      for(In j=0;j<equal_contribs.size();j++) {
         if(auto optional_val = midas::util::FromStringOptional<Nb>(equal_contribs[j]))
         {
            val = *optional_val;
            Mout << "Found initializing value..." << endl;
            initial_val=true;
            break;
         }
      }
      string s_base="type=";
      if(initial_val) {
         for(In j=0;j<equal_contribs.size()-1;j++) {
            // add linear response function
            vector<OpInfo> infos;
            OpInfo oi;
            string s=s_base+equal_contribs[j].substr(I_0,I_1)+"_DIPOLE";
            ParseSetInfoLine(s,oi);
            infos.push_back(oi);
            s=s_base+equal_contribs[j].substr(I_1,I_1)+"_DIPOLE";
            ParseSetInfoLine(s,oi);
            infos.push_back(oi);
            vector<Nb> frq_vec;
            frq_vec.push_back(mFrq);
            Contribution pv_contrib(infos,frq_vec);
            pv_contrib.SetHasBeenEval(true);
            pv_contrib.SetValue(val);
            s=s_base+equal_contribs[j]+"_POL frq="+StringForNb(mFrq);
            ParseSetInfoLine(s,oi);
            infos.clear();
            infos.push_back(oi);
            frq_vec.clear();
            Contribution zpv_contrib(infos,frq_vec);
            zpv_contrib.SetHasBeenEval(true);
            zpv_contrib.SetValue(val);
            mDefault.insert(pv_contrib);
            mDefault.insert(zpv_contrib);
         }
      }
      else {
         vector<Contribution> equivalent;
         for(In j=0;j<equal_contribs.size();j++) {
            // add linear response function
            vector<OpInfo> infos;
            OpInfo oi;
            string s=s_base+equal_contribs[j].substr(I_0,I_1)+"_DIPOLE";
            ParseSetInfoLine(s,oi);
            infos.push_back(oi);
            s=s_base+equal_contribs[j].substr(I_1,I_1)+"_DIPOLE";
            ParseSetInfoLine(s,oi);
            infos.push_back(oi);
            vector<Nb> frq_vec;
            frq_vec.push_back(mFrq);
            Contribution pv_contrib(infos,frq_vec);
            s=s_base+equal_contribs[j]+"_POL frq="+StringForNb(mFrq);
            ParseSetInfoLine(s,oi);
            infos.clear();
            infos.push_back(oi);
            frq_vec.clear();
            Contribution zpv_contrib(infos,frq_vec);
            equivalent.push_back(pv_contrib);
            equivalent.push_back(zpv_contrib);
         }
         mEquivalent.push_back(equivalent);
      }
   }
}

void TotalLinRsp::ConstructionError(const string aS,
                                    const map<string,string>& aM)
{
   Mout << " TotalLinRsp construction error:" << endl
        << aS << endl << endl
        << " Constructor parameters:" << endl;

   for (map<string,string>::const_iterator it=aM.begin(); it!=aM.end(); ++it)
      Mout << " " << it->first << " -> " << it->second << endl;

   MIDASERROR("");
}

void TotalLinRsp::ConstructIntermediateSet()
{
   // given mPvl, mZpvc, mTotalRspString, and mContribSet
   // construct the intermediate set of contributions
   set<string> c_set;
   vector<Nb> frq_vec;
   frq_vec.push_back(mFrq);
   for(In i=0;i<mTotalRspString.size();i++) {
      c_set=ConvertStringToFunctions(mTotalRspString[i]);
      for(set<string>::iterator it=c_set.begin();it!=c_set.end();it++) {
         mContribSet.insert(*it);
      }
   }
   set<Contribution> help_set;
   // now the mContribSet
   for(set<string>::iterator it=mContribSet.begin();it!=mContribSet.end();it++) {
      string c_s=*it;
      OpInfo oi;
      string info_line;
      // first zpv
      info_line="type="+c_s+"_POL frq="+StringForNb(mFrq);
      ParseSetInfoLine(info_line,oi);
      vector<OpInfo> oi_vec;
      oi_vec.push_back(oi);
      frq_vec.clear();
      Contribution c(oi_vec,frq_vec);
      help_set.insert(c);
      // and then pure vib
      oi_vec.clear();
      info_line="type="+c_s.substr(I_0,I_1)+"_DIPOLE";
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      info_line="type="+c_s.substr(I_1,I_1)+"_DIPOLE";
      ParseSetInfoLine(info_line,oi);
      oi_vec.push_back(oi);
      frq_vec.push_back(mFrq);
      Contribution c_a(oi_vec,frq_vec);
      help_set.insert(c_a);
   }
   // values are not set, we do this by searching the basic set
   for(set<Contribution>::iterator it=help_set.begin();it!=help_set.end();it++) {
      set<Contribution>::iterator basic_it=mBasicSet.find(*it);
      Contribution c(*it);
      if(basic_it!=mBasicSet.end())
         c.SetValue(basic_it->GetValue());
      mIntermediateSet.insert(c);
   }
   // now modify this according to mPvl and mZpva
   if(mPvl && mZpvc) {
      Mout << "At end of day, intermediate set is:" << endl;
      for(set<Contribution>::iterator it=mIntermediateSet.begin();it!=mIntermediateSet.end();it++)
         Mout << *it << endl;
      Mout << "Done in construct interm. set." << endl;
      return;
   }
   if(!mZpvc) {
      // remove all vib rsp order=1
      set<Contribution> help_set;
      for(set<Contribution>::iterator it=mIntermediateSet.begin();it!=mIntermediateSet.end();it++)
      {
         if(it->GetVibRspOrder()!=I_1)
            help_set.insert(*it);
      }
      mIntermediateSet=help_set;
   }
   if(!mPvl) {
      // remove all vib rsp order=2
      set<Contribution> help_set;
      for(set<Contribution>::iterator it=mIntermediateSet.begin();it!=mIntermediateSet.end();it++)
      {
         if(it->GetVibRspOrder()!=I_2)
            help_set.insert(*it);
      }
      mIntermediateSet=help_set;
   }
   Mout << "At end of day, intermediate set is:" << endl;
   for(set<Contribution>::iterator it=mIntermediateSet.begin();it!=mIntermediateSet.end();it++)
      Mout << *it << endl;
   Mout << "Done in construct interm. set." << endl;
}

map<string,TotalResponseContribution> TotalLinRsp::ProvideMap()
{
   map<string,TotalResponseContribution> result;
   // Need to loop through mContribSet and create
   // TotalResponseContrib's from that
   vector<Nb> frq_vec;
   frq_vec.push_back(mFrq);
   set<Contribution>::iterator c_it;
   for(set<string>::iterator it=mContribSet.begin();it!=mContribSet.end();it++)
   {
      TotalResponseContribution t_rsp;
      // we have a contribution
      string func=*it;
      if(mZpvc) {
         // look in mIntermediateSet for contribution
         OpInfo oi;
         vector<OpInfo> oi_vec;
         string info_line="type="+func+"_POL frq="+StringForNb(mFrq);
         ParseSetInfoLine(info_line,oi);
         oi_vec.push_back(oi);
         frq_vec.clear();
         Contribution c(oi_vec,frq_vec);
         c_it=mIntermediateSet.find(c);
         if(c_it==mIntermediateSet.end()) {
            Mout << "Did not find contribution" << c << endl;
            MIDASERROR("");
         }
         Nb zpv=c_it->GetValue();
         // add (ZPVC,value) pair to TotalRsp
         t_rsp.AddTerm("ZPVC",zpv);
      }
      if(mPvl) {
         // look in mIntermediateSet for contribution
         OpInfo oi;
         vector<OpInfo> oi_vec;
         string info_line="type="+func.substr(I_0,I_1)+"_DIPOLE";
         ParseSetInfoLine(info_line,oi);
         oi_vec.push_back(oi);
         info_line="type="+func.substr(I_1,I_1)+"_DIPOLE";
         ParseSetInfoLine(info_line,oi);
         oi_vec.push_back(oi);
         frq_vec.push_back(mFrq);
         Contribution c(oi_vec,frq_vec);
         c_it=mIntermediateSet.find(c);
         if(c_it==mIntermediateSet.end()) {
            Mout << "Did not find contribution" << c << endl;
            MIDASERROR("");
         }
         Nb pvl=c_it->GetValue();
         // add (ZPVC,value) pair to TotalRsp
         t_rsp.AddTerm("PVL",pvl);
      }
      // also eq. value
      frq_vec.clear();
      frq_vec.push_back(mFrq);
      Nb eq_val=SearchForEqVal(func,frq_vec);
      t_rsp.AddTerm("EQ",eq_val);
      result.insert(make_pair(func,t_rsp));
   }
   return result;
}

ostream& TotalLinRsp::Print(ostream& aOut) const
{
   aOut << "Order = 2" << endl;
   aOut << "Frq   = " << mFrq << endl;
   return aOut;
}
