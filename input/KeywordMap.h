#ifndef MIDAS_INPUT_KEYWORDMAP_H_INCLUDED
#define MIDAS_INPUT_KEYWORDMAP_H_INCLUDED

#include <string>
#include <set>
#include <map>

#include "libmda/util/any_type.h"

namespace detail
{
/* define some types */
///> keyword/value pair
using keyword_pair = std::pair<std::string, libmda::util::any_type>;
///> special less for sorting keyword_pairs, equivalent to just keyword 'less-than'
struct special_less
{
   bool operator()(const keyword_pair& p1, const keyword_pair& p2) const
   {
      return (p1.first < p2.first);
   }
};
///> set of keyword/value pairs 
using keyword_set = std::set<keyword_pair, special_less>;
///> map of keyword sets, such that each type of decomposition can have its own set of keywords
using keyword_map = std::map<std::string, keyword_set>;
} /* namespace detail */

#endif /* MIDAS_INPUT_KEYWORDMAP_H_INCLUDED */
