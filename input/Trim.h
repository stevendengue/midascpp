#ifndef TRIM_H_INCLUDED
#define TRIM_H_INCLUDED

#include<string>

namespace midas
{
namespace input
{

//! Copy string and delete blanks in front and back from copy 
std::string Trim(const std::string& str
               , const std::string& whitespace = " \t");

//!
std::string Reduce(const std::string& str
                 , const std::string& fill = " "
                 , const std::string& whitespace = " \t");

//! Copy string and delete blanks in copy
std::string DeleteBlanks(std::string str);

//! Copy string and turn copy to upper case
std::string ToUpperCase(std::string str);

//! Copy string and turn copy to lower case
std::string ToLowerCase(std::string str);

//! Copy string and remove trailing slash
std::string RemoveTrailingSlash(std::string str);

} /* namespace input */
} /* namespace midas */

#endif /* TRIM_H_INCLUDED */
