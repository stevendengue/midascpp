/**
************************************************************************
* 
* @file                TdecompCalcDef.cc
*
* Created:             30-05-2016
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk) 
*
* Short Description:   Contains input-info that defines a calculation
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/


// Standard Headers
#include <string>
using std::string;
#include <vector>
using std::vector;

// Midas Headers
#include "inc_gen/TypeDefs.h"
#include "input/TdecompCalcDef.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "util/Io.h"

typedef string::size_type Sst;


/**
* Construct a default definition of a  calculation
* */
TdecompCalcDef::TdecompCalcDef()
   : mCalcName("")
   , mIoLevel (I_0)
{
}

/**
 *  Adds TensorDecompInfo to mDecompInfoSet 
 *  @param Minp
 *  @param s
 **/
bool TdecompCalcDef::ReadDecompInfoSet(std::istream& Minp, std::string& s)
{
   return NewTensorDecompInput(Minp, s, 3, this->GetDecompInfoSet());
}
