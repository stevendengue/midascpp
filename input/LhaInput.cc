/*
************************************************************************
*  
* @file                LhaCalcDef.h
*
* Created:             10-01-2020
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Input for Local Harmonic Approximation.
*                      I.e. tools for diagonalizing and transforming 
*                      harmonic potentials (and writing mop files). 
* 
* Last modified:
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <map>
#include <string>

#include "input/Input.h"
#include "input/Trim.h"
#include "input/IsKeyword.h"
#include "input/FindKeyword.h"

/**
 * @param Minp          Input stream
 * @param apCalcDef     CalcDef
 * @param arInputLevel  Input level
 * @return
 *    Last read string
 **/
std::string LhaInput
   (  std::istream& Minp
   ,  LhaCalcDef* apCalcDef
   ,  const In& arInputLevel
   )
{
   In input_level = arInputLevel + I_1;
   std::string s; //To be returned when we are done reading this level

   // Keywords
   enum INPUT
   {  ERROR
   ,  REFERENCESYSTEM
   ,  PROJECTOUTTRANSROT
   ,  DISABLEHESSIANCHECK
   ,  WRITEBOUNDS
   ,  IGNOREGRADIENT
   };
   
   // Conversion map
   const std::map<std::string,INPUT> input_word =
   {  {"#"+std::to_string(input_level)+"ERROR", ERROR}
   ,  {"#"+std::to_string(input_level)+"REFERENCESYSTEM", REFERENCESYSTEM}
   ,  {"#"+std::to_string(input_level)+"PROJECTOUTTRANSROT", PROJECTOUTTRANSROT}
   ,  {"#"+std::to_string(input_level)+"DISABLEHESSIANCHECK", DISABLEHESSIANCHECK}
   ,  {"#"+std::to_string(input_level)+"WRITEBOUNDS", WRITEBOUNDS}
   ,  {"#"+std::to_string(input_level)+"IGNOREGRADIENT", IGNOREGRADIENT}
   };

   // Read lines
   bool already_read=false;
   std::string s_orig = "";
   while (  (already_read || midas::input::GetLine(Minp, s))
         && !midas::input::IsLowerLevelKeyword(s, input_level)
         )
   {
      already_read=false;

      s_orig = s;
      s = midas::input::ParseInput(s); // Transform to uppercase and delete blanks
      INPUT input = midas::input::FindKeyword(input_word, s);

      switch   (  input
               )
      {
         case REFERENCESYSTEM:
         {
            midas::input::GetLine(Minp, s);
            apCalcDef->SetReferenceSystem(s);
            break;
         }
         case PROJECTOUTTRANSROT:
         {
            midas::input::GetLine(Minp, s);
            if (  midas::input::NotKeyword(s)
               )
            {
               In n_it = midas::util::FromString<In>(s);
               apCalcDef->SetProjectOutTransRot(n_it);
            }
            else
            {
               apCalcDef->SetProjectOutTransRot(I_1);
               already_read = true;
            }

            break;
         }
         case DISABLEHESSIANCHECK:
         {
            apCalcDef->SetCheckHessian(false);
            break;
         }
         case WRITEBOUNDS:
         {
            apCalcDef->SetWriteBounds(midas::input::GetIn(Minp, s));
            break;
         }
         case IGNOREGRADIENT:
         {
            apCalcDef->SetIgnoreGradient(true);
            break;
         }
         case ERROR:
         default:
         {
            Mout << " Keyword " << s_orig << " is not a LHA level " << input_level << " Input keyword - input flag ignored! " << std::endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }

   // Return read string
   return s;
}
