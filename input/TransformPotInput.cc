/**
************************************************************************
* 
* @file                TransformPotInput.cc
*
* 
* Created:             05-12-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Input reader for the TransformPot module based on the
* 
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// Standard headers:
#include <sstream>
#include <iostream>
#include <map>



#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/TransformPotCalcDef.h"
#include "input/GeneralInputParser.h"
#include "input/FindKeyword.h"
//#include "pes/molecule/MoleculeReader.h"
#include "input/FileConversionInput.h"


string TransformPotInput(std::istream& Minp, TransformPotCalcDef* apCalcDef, const In& arInputLevel)
{

   In input_level = arInputLevel + I_1;
   string s;//To be returned when we are done reading this level
   enum INPUT {ERROR, INMOLFILE, OPERFILENAME, KEEPMAXCOUPLEVEL}; 
   
   const std::map<string,INPUT> input_word =
   {
      {"#"+std::to_string(input_level)+"INMOLFILE",INMOLFILE}
      , {"#"+std::to_string(input_level)+"OPERFILENAME",OPERFILENAME}
      , {"#"+std::to_string(input_level)+"KEEPMAXCOUPLEVEL",KEEPMAXCOUPLEVEL}
   };

   bool already_read=false;
   while ((already_read || getline(Minp,s)) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))  // readin until input levels point up
   {
      string s_orig = s;
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      
      INPUT input = midas::input::FindKeyword(input_word, s);
      already_read = false;
      
      switch(input)
      {
         case INMOLFILE:
         {
            apCalcDef->SetInMolFileInfo(ReadMoleculeFileInput(Minp,s,already_read,true));
            break;
         }
         case OPERFILENAME:
         {
            getline(Minp,s);
            apCalcDef->SetOperatorFileName(s);
            break;
         }
         case KEEPMAXCOUPLEVEL:
         {
            apCalcDef->SetKeepMaxCoupLevel(true);
            break;
         }
         case ERROR: //FALL THROUGH TO DEFAULT!
         default:
         {
            Mout << " Keyword " << s_orig <<
               " is not a TransformPot level "+std::to_string(input_level)+" Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }
   return s;
}
