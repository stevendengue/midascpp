/**
************************************************************************
* 
* @file                NewtonRaphsonInput.h
*
* 
* @date                12-09-2016
*
* @author              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief               Input for Newton-Raphson method in TensorNlSolver
* 
* 
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef NEWTON_RAPHSON_INPUT_H_INCLUDED
#define NEWTON_RAPHSON_INPUT_H_INCLUDED

#include <string>

#include "inc_gen/TypeDefs.h"
#include "it_solver/nl_solver/NewtonRaphsonInfo.h"

bool NewtonRaphsonInput(std::istream&, std::string&, NewtonRaphsonInfo&);

#endif /* NEWTON_RAPHSON_INPUT_H_INCLUDED */
