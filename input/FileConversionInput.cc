/**
************************************************************************
* 
* @file                FileConversionInput.cc
*
* 
* Created:             02-10-2012
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Input reader for the response information 
* 
* Last modified: Tue Oct 2, 2012  05:55PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "input/FileConversionInput.h"

// std headers
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "input/Input.h"
#include "input/FindKeyword.h"
#include "input/IsKeyword.h"
#include "input/GetLine.h"
#include "input/FileConversionCalcDef.h"
#include "pes/molecule/MoleculeFile.h"
#include "pes/molecule/MoleculeReader.h"
#include "pes/molecule/MoleculeWriter.h"

/**
 * Parse FileConversion input.
 *
 * @param Minp   The input file stream.
 *
 * @return       Returns the last read string.
 **/
std::string FileConversionInput(std::istream& Minp)
{
   // Define all keywords
   // ----------------------------------------------------------------------
   enum INPUT 
      {  ERROR
      ,  MOLECULEINPUT
      ,  MOLECULEOUTPUT
      ,  OPERATORINPUT
      ,  MIDASPOT
      ,  MIDASDYNLIB
      ,  REFERENCEVALUES
      ,  DESCRIPTION
      ,  TENSOROPERATORINPUT
      ,  CREATEUNITTESTCODE
      ,  USEINCLUDESTATEMENTS
      };
   
   In input_level = 2;
   auto&& calc_def = CreateFileConversionCalcDef();

   const std::map<string,INPUT> input_word =
      {  {"#"+std::to_string(input_level)+"MOLECULEINPUT", MOLECULEINPUT}
      ,  {"#"+std::to_string(input_level)+"MOLECULEOUTPUT", MOLECULEOUTPUT}
      ,  {"#"+std::to_string(input_level)+"OPERATORINPUT", OPERATORINPUT}
      ,  {"#"+std::to_string(input_level)+"MIDASPOT", MIDASPOT}
      ,  {"#"+std::to_string(input_level)+"MIDASDYNLIB", MIDASDYNLIB}
      ,  {"#"+std::to_string(input_level)+"REFERENCEVALUES", REFERENCEVALUES}
      ,  {"#"+std::to_string(input_level)+"DESCRIPTION", DESCRIPTION}
      ,  {"#"+std::to_string(input_level)+"TENSOROPERATORINPUT", TENSOROPERATORINPUT}
      ,  {"#"+std::to_string(input_level)+"CREATEUNITTESTCODE", CREATEUNITTESTCODE}
      ,  {"#"+std::to_string(input_level)+"USEINCLUDESTATEMENTS", USEINCLUDESTATEMENTS}
      };
   
   // Initialization of default values for quantities read in later &  Local params.
   std::string s;
   std::string s_orig;
   bool already_read = false;
   
   // readin until input levels point up
   // ----------------------------------------------------------------------
   while (  ( already_read || midas::input::GetLine(Minp,s) ) 
         && !midas::input::IsLowerLevelKeyword(s,input_level)
         )
   {
      already_read = false;
      s_orig = s;
      s = midas::input::ParseInput(s);
      
      INPUT input = midas::input::FindKeyword(input_word, s);

      switch(input)
      {
         case MOLECULEINPUT:
         {
            calc_def.AddInputFile(ReadMoleculeFileInput<molecule::MoleculeReaderFactory>(Minp,s,already_read,true));
            break;
         }
         case MOLECULEOUTPUT:
         {
            calc_def.AddOutputFile(ReadMoleculeFileInput<molecule::MoleculeWriterFactory>(Minp,s,already_read,false));
            break;
         }
         case OPERATORINPUT:
         {
            already_read = calc_def.ReadPotentialInput(Minp, s, false);
            break;
         }
         case TENSOROPERATORINPUT:
         {
            already_read = calc_def.ReadPotentialInput(Minp, s, true);
            break;
         }
         case MIDASPOT:
         {
            calc_def.SetConversionType(FileConversionCalcDef::ConversionType::MIDASPOT);
            break;
         }
         case MIDASDYNLIB:
         {
            calc_def.SetConversionType(FileConversionCalcDef::ConversionType::MIDASDYNLIB);
            break;
         }
         case REFERENCEVALUES:
         {
            already_read = calc_def.ReadReferenceValues(Minp, s);
            break;
         }
         case DESCRIPTION:
         {
            already_read = calc_def.ReadDescription(Minp, s);
            break;
         }
         case CREATEUNITTESTCODE:
         {
            calc_def.SetCreateUnitTestCode(true);
            break;
         }
         case USEINCLUDESTATEMENTS:
         {
            calc_def.SetMidasPotUseInclude(true);
            break;
         }
         case ERROR: // fall-through to default
         default:
         {
            midas::input::InputKeywordError(s_orig, " FileConversion ", input_level); // will throw error
         }
      }
   }
   
   return s;
}
