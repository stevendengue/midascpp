/**
************************************************************************
* 
* @file                FlexCoupCalcDef.h
*
* Created:             10-02-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk) 
*
* Short Description:   Handling input for flexible Coupling schemes in both
*                      PES and VCC calculations 
* 
* Last modified:       08-12-2014 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "input/FlexCoupCalcDef.h"
#include "input/GetLine.h"
#include "util/Io.h"
#include "pes/molecule/MoleculeReader.h"

/**
 *
 **/
FlexCoupCalcDef::FlexCoupCalcDef(const FlexCoupCalcDef& arFlexCoupCalcDef)
{
    Reset(arFlexCoupCalcDef);
}

/**
 *
 **/
void FlexCoupCalcDef::Reset(const FlexCoupCalcDef& arFlexCoupCalcDef)
{
    mSetFlexCoup        = arFlexCoupCalcDef.GetmSetFlexCoup();
    mSetGroupCouplings  = arFlexCoupCalcDef.GetmSetGroupCouplings();
    mModeGroups         = arFlexCoupCalcDef.GetmModeGroups();
    mGroupCombis        = arFlexCoupCalcDef.GetmGroupCombis();
    mSetSystemCoup      = arFlexCoupCalcDef.GetmSetSystemCoup();
    mMoleculeFileInfo   = arFlexCoupCalcDef.GetMoleculeFileInfo();
    mSetEdiff           = arFlexCoupCalcDef.GetmSetEdiff();
    mEdiffThreshs       = arFlexCoupCalcDef.GetmEdiffThreshs();
    mSetLocal           = arFlexCoupCalcDef.GetmSetLocal();
    mLocalThreshs       = arFlexCoupCalcDef.GetmLocalThreshs();
    mSetMcScreen        = arFlexCoupCalcDef.GetmMcScreen();
    mMcScreenFile       = arFlexCoupCalcDef.GetmMcScreenFile();
    mMcScreenThr        = arFlexCoupCalcDef.GetmMcScreenThr();
    mSetAdgaPreScreen   = arFlexCoupCalcDef.GetmAdgaPreScreen();
    mAdgaPreScreenFile  = arFlexCoupCalcDef.GetmAdgaPreScreenFile();
    mAdgaPreScreenThr   = arFlexCoupCalcDef.GetmAdgaPreScreenThr();
    mVccPertOrder       = arFlexCoupCalcDef.GetmVccPertOrder();
    mMaxEx              = arFlexCoupCalcDef.GetmMaxEx();
    mVccPertFiles       = arFlexCoupCalcDef.GetmVccPertFiles();
    mVccPertThr         = arFlexCoupCalcDef.GetmVccPertThr();
}

/**
 *
 **/
bool FlexCoupCalcDef::ReadMoleculeFile(std::istream& Minp, std::string& s)
{
   // read file type
   std::string file_type;
   midas::input::GetLine(Minp,file_type);
   file_type = midas::input::ParseInput(file_type); // to upper, and delete blanks
   MidasAssert(molecule::MoleculeReaderFactory::check_key(file_type),"#2 MoleculeFile: type " + file_type + " not supported");
   
   // read file name
   std::string file_name;
   midas::input::GetLine(Minp,file_name);
   MidasAssert(InquireFile(file_name),"#2 MoleculeFile: filename: " + file_name + " not found");
   
   mMoleculeFileInfo = molecule::MoleculeFileInfo(file_name,file_type);

   return false;
}
