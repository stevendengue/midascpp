/*
************************************************************************
*  
* @file                LhaCalcDef.h
*
* Created:             10-01-2020
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   CalcDef for Local Harmonic Approximation.
*                      I.e. tools for diagonalizing and transforming 
*                      harmonic potentials (and writing mop files). 
* 
* Last modified:
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef LHACALCDEF_H_INCLUDED
#define LHACALCDEF_H_INCLUDED

/**
 *
 **/
class LhaCalcDef
{
   private:
      //! Reference system (e.g. different electronic state at same geometry)
      std::string mReferenceSystem = "";

      //! Project out translation and rotation (how many iterations)
      In mProjectOutTransRot = I_1;

      //! Write bounds for transformed potential using turning points for this HO function
      In mWriteBounds = I_10;

      //! Check Hessian for negative eigenvalues
      bool mCheckHessian = true;

      //! Ignore gradient in mop files and bounds
      bool mIgnoreGradient = false;

   public:

      //! Macro for Getter and Setter interface
      template<typename T> struct argument_type;
      template<typename T, typename U> struct argument_type<T(U)> { using type = U; };
      #define UNPACK(...) __VA_ARGS__
      #define GETSET(NAME, TYPE) \
         LhaCalcDef& Set##NAME(const argument_type<void(TYPE)>::type& a##NAME) \
         { \
            m##NAME = a##NAME; \
            return *this; \
         } \
           \
         const argument_type<void(TYPE)>::type& NAME() const \
         { \
            return m##NAME; \
         }
      
      GETSET(ReferenceSystem, std::string);
      GETSET(ProjectOutTransRot, In);
      GETSET(WriteBounds, In);
      GETSET(CheckHessian, bool);
      GETSET(IgnoreGradient, bool);

      #undef GETSET
      #undef UNPACK
};


#endif /* LHACALCDEF_H_INCLUDED */
