#ifndef LINRSPRAMANINPUT_H_INCLUDED
#define LINRSPRAMANINPUT_H_INCLUDED

#include "input/OpInfo.h"
#include "util/Error.h"

class LinRspRamanInput
{
   private:
      std::string mName;
      oper::PropertyType mType;
      Nb mFrq;

   public:
      LinRspRamanInput(const std::string& name, oper::PropertyType type, Nb frq):
         mName(name)
       , mType(type)
       , mFrq(frq)
      {
      }

      const std::string& Name() const
      {
         return mName;
      }

      oper::PropertyType Type() const
      {
         return mType;
      }

      Nb Frq() const
      {
         return mFrq;
      }
};

LinRspRamanInput ParseLinRspRamanInput(const std::string&);

#endif /* LINRSPRAMANINPUT_H_INCLUDED */
