/**
 *******************************************************************************
 * 
 * @file    TdCalcDef.cc
 * @date    07-08-2016
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Calculation definitions for TD module (time dependent).
 *
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "input/TdCalcDef.h"
