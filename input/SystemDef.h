/**
************************************************************************
* 
* @file                SystemDef.h
*
* 
* Created:             05-01-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk) 
*
* Short Description:   Input definition for the initial set up of a system 
* 
* Last modified: March, 26th 2015 (carolin)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef SYSTEMDEF_H
#define SYSTEMDEF_H

// std headers
#include <string>
#include <vector>
#include <memory>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "input/RotCoordCalcDef.h"
#include "pes/molecule/MoleculeFile.h"


// using declarations
using std::string;
using std::vector;

using namespace midas;

enum class CalcModeType: int  {ERROR,NO,CART,LOCALTRANSROT};
enum class FragmentType: int  {ERROR,ATOM,PREDEFINED};

class SystemDef 
{
   private:
      std::string                   mName;
      molecule::MoleculeFileInfo    mMoleculeFileInfo = molecule::MoleculeFileInfo("","");
      CalcModeType                  mCalcModeType;
      FragmentType                  mFragmentType;
      bool                          mAddLocalTransRot;

   public:
      //constructor/destructor
      SystemDef(): 
             mName(""),
             mCalcModeType(CalcModeType::NO),
             mFragmentType(FragmentType::PREDEFINED),
             mAddLocalTransRot(false)
             {;}

      ~SystemDef() {;}

      // setting stuff
      void SetName(const std::string& arName)                  {mName = arName;}
      void SetMoleculeFileInfo(const molecule::MoleculeFileInfo& arFileInfo) {mMoleculeFileInfo = arFileInfo;}
      void SetCalcModeType(const std::string& arModeType);
      void SetFragmentType(const std::string& arSubSysDefType);
      void SetAddLocalTransRot(const bool& arBool = true)      {mAddLocalTransRot = arBool;}

      // getting stuff
      std::string Name() const                                 {return mName;}
      molecule::MoleculeFileInfo GetMoleculeFileInfo() const   {return mMoleculeFileInfo;}
      FragmentType GetFragmentType() const                     {return mFragmentType;}
      CalcModeType GetCalcModeType() const                     {return mCalcModeType;}
      bool AddLocalTransRot() const                            {return mAddLocalTransRot;}
};

#endif //SYSTEMDEF_H
