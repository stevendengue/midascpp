/**
************************************************************************
*
* @file                TdPropertyDef.h
*
* Created:             05-10-2018
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Contains input-info that defines properties of time-dependent
*                      wave functions.
*
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef TDPROPERTYDEF_H_INCLUDED
#define TDPROPERTYDEF_H_INCLUDED

#include <set>
#include <string>
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "ode/OdeInfo.h"

/**
 * Class that contains and reads input on time-dependent properties of wave functions.
 **/
class TdPropertyDef
{
   protected:
      //! Properties to calculate
      std::set<std::string> mProperties;

      //! Spectra to construct
      std::set<std::string> mSpectra;

      //! Expectation-value operators
      std::set<std::string> mExptValOpers;

      //! Statistics to calculate, e.g. norm of parameters.
      std::set<std::string> mStats;

      //! Write wave function for all time steps for following modes
      std::set<std::string> mWfOutModes;

      //! Mode pairs to write two-mode densities for
      std::set<std::set<std::string> > mTwoModeDensityPairs;

      //! Write primitive basis functions for modes
      std::set<std::string> mWritePrimitiveBasisForModes;

      //! Convolute the autocorrelation function before FFT to prevent Gibbs' phenomenon
      bool mConvoluteAutoCorr = true;

      //! Level of zero-padding of FFT
      In mPaddingLevel = I_16;

      //! Shift the spectrum
      std::pair<std::string, Nb> mSpectrumEnergyShift = std::pair<std::string, Nb>("FIXED", C_0);

      //! Screen spectrum output
      Nb mSpectrumOutputScreeningThresh = C_0;

      //! Set interval for spectrum output
      std::pair<Nb,Nb> mSpectrumInterval = std::pair<Nb,Nb>(C_0, C_0);

      //! Grid density for plotting wf
      In mWfGridDensity = I_10;

      //! Normalize max peak of spectrum to 1
      bool mNormalizeSpectrum = false;

      //! Abs all intensities in spectra
      bool mAbsIntensities = false;

      //! Life time used for Lorenzian broadening
      Nb mLifeTime = -C_1;

   public:
      //! Default c-tor
      TdPropertyDef() = default;

      //! Macro for Getter and Setter interface
      template<typename T> struct argument_type;
      template<typename T, typename U> struct argument_type<T(U)> { using type = U; };
      #define GETSET(NAME, TYPE) \
         TdPropertyDef& Set##NAME(const argument_type<void(TYPE)>::type& a##NAME) \
         { \
            m##NAME = a##NAME; \
            return *this; \
         } \
           \
         const argument_type<void(TYPE)>::type& NAME() const \
         { \
            return m##NAME; \
         }

         GETSET(Properties, std::set<std::string>);
         GETSET(Spectra, std::set<std::string>);
         GETSET(Stats, std::set<std::string>);
         GETSET(WfOutModes, std::set<std::string>);
         GETSET(WritePrimitiveBasisForModes, std::set<std::string>);
         GETSET(WfGridDensity, In);
         GETSET(ExptValOpers, std::set<std::string>);
         GETSET(ConvoluteAutoCorr, bool);
         GETSET(TwoModeDensityPairs, std::set<std::set<std::string>>);
         GETSET(SpectrumEnergyShift, (std::pair<std::string, Nb>));
         GETSET(SpectrumOutputScreeningThresh, Nb);
         GETSET(SpectrumInterval, (std::pair<Nb,Nb>));
         GETSET(PaddingLevel, In);
         GETSET(NormalizeSpectrum, bool);
         GETSET(AbsIntensities, bool);
         GETSET(LifeTime, Nb);
      #undef GETSET

      //! Sanity check. Check that input makes sense.
      void SanityCheck
         (  midas::ode::OdeInfo&
         ,  bool
         );

      //! Read properties
      bool ReadProperties
         (  std::istream&
         ,  std::string&
         );

      //! Read spectra
      bool ReadSpectra
         (  std::istream&
         ,  std::string&
         );

      //! Add property to mProperties
      void AddProperty
         (  std::string
         );

      //! Read operators for expt values
      bool ReadExptValOpers
         (  std::istream&
         ,  std::string&
         );

      //! Read operators for expt values
      bool ReadStats
         (  std::istream&
         ,  std::string&
         );

      //! Read two-mode-density pairs
      bool ReadTwoModeDensityPairs
         (  std::istream&
         ,  std::string&
         );
};

#endif /* TDPROPERTYDEF_H_INCLUDED */
