/**
************************************************************************
* 
* @file                FalconCalcDef.h
*
* 
* Created:             10-02-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk) 
*
* Short Description:   Calculation definition for generation of coordinates
* 
* Last modified:       07-07-2015 (carolin)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef FALCONCALCDEF_H
#define FALCONCALCDEF_H

// std headers
#include <string>
#include <vector>
#include <memory>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "input/FreqAnaCalcDef.h"
#include "input/RotCoordCalcDef.h"


// using declarations
using std::string;
using std::vector;

enum class ModeOptType: int  {ERROR,NO,HESSIAN, ROTCOORD};
enum class HessType: int  {ERROR,NO, GIVEN, CALC};
enum class CoupEstType: int  {ERROR,DIST,DISTACT,NUCCOUPHESSNORM,NUCCOUPHESSNORMACT};

ostream& operator<<(ostream& os,const ModeOptType& arModeOptType);
ostream& operator<<(ostream& os,const HessType& arHessType);
ostream& operator<<(ostream& os,const CoupEstType& arCoupEstType);

/* ************************************************************************
************************************************************************ */
class FalconCalcDef
{
   private:
      ModeOptType             mModeOptType;
      HessType                mHessType;
      bool                    mRestart;
      // about fusion 
      Nb                      mDegThresh;
      bool                    mSlowFusion;
      bool                    mFusionInactive;
      CoupEstType             mCoupEstType;

      //Thresholds:
      // connection
      Nb                      mMaxDistForConn;
      // fusion
      In                      mMaxSubSysForFusion;  
      Nb                      mMinCouplingForFusion;
      // relaxation
      In                      mMaxSubSysForRelax;  /*<max number of subsys in fusion group for relaxation*/ 
      Nb                      mMinCouplingForRelax;
      //  Addition of modes 
      In                      mMaxSubSysForFullAdd;  
      Nb                      mMinCouplingForFullAdd;

      FreqAnaCalcDef          mFreqAnaCalcDef;
      RotCoordCalcDef         mRotCoordCalcDef;

      // other relaxation setting (more advanced)
      bool                    mCalcAllSigmas;
      bool                    mRelaxAll;
      bool                    mRotateAll;

      bool                    mPrepRun;

      //Writing of the Incremental Input
      In                      mMaxIncrLevel;
      Nb                      mMaxIncrDist;
      Nb                      mIncrBondDist;
      bool                    mProj=false;
      bool                    mAddRotToAux=true;

   public:
      //constructor/destructor
      FalconCalcDef();

      //setting
      void SetMaxSubSysForRelax(const In& arIn) {mMaxSubSysForRelax=arIn;}
      void SetMaxSubSysForFullAdd(const In& arIn) {mMaxSubSysForFullAdd=arIn;}
      void SetMaxSubSysForFusion(const In& arIn) {mMaxSubSysForFusion=arIn;}
      void SetMinCouplingForRelax(const Nb& arNb) {mMinCouplingForRelax=arNb;}
      void SetMinCouplingForFullAdd(const Nb& arNb) {mMinCouplingForFullAdd=arNb;}
      void SetMinCouplingForFusion(const Nb& arNb) {mMinCouplingForFusion=arNb;}
      void SetMaxDistForConn(const Nb& arNb) {mMaxDistForConn=arNb;}
      void SetModeOptType (const string& arStr);
      void SetHessType (const string& arStr);
      void SetCoupEstType (const string& arStr);
      void SetDegThresh(const Nb& arNb) {mDegThresh=arNb;}
      void SetSlowFusion(const bool& arBool=true) {mSlowFusion=arBool;}
      void SetCalcAllSigmas(const bool& arBool=true) {mCalcAllSigmas=arBool;}
      void SetRelaxAll(const bool& arBool=true) {mRelaxAll=arBool;}
      void SetRotateAll(const bool& arBool=true) {mRotateAll=arBool;}
      void SetFusionInactive(const bool& arBool=true) {mFusionInactive=arBool;}
      void SetPrepRun(const bool& arBool=true) {mPrepRun=arBool;}
      void SetRestart(const bool& arBool=true) {mRestart=arBool;}

      void SetMaxIncrLevel(const In& arIn) {mMaxIncrLevel=arIn;}
      void SetMaxIncrDist(const Nb& arNb) {mMaxIncrDist=arNb;}
      void SetIncrBondDist(const Nb& arNb) {mIncrBondDist=arNb;}
      void SetProj(const bool& arBool=true) {mProj=arBool;}
      void SetAddRotToAux(const bool& arBool=true) {mAddRotToAux=arBool;}
      //getting
      In GetMaxSubSysForRelax() const {return mMaxSubSysForRelax;}
      In GetMaxSubSysForFullAdd() const {return mMaxSubSysForFullAdd;}
      In GetMaxSubSysForFusion() const {return mMaxSubSysForFusion;}
      Nb GetMinCouplingForRelax() const {return mMinCouplingForRelax;}
      Nb GetMinCouplingForFullAdd() const {return mMinCouplingForFullAdd;}
      Nb GetMinCouplingForFusion() const {return mMinCouplingForFusion;}
      Nb GetMaxDistForConn() const {return mMaxDistForConn;}
      Nb GetDegThresh() const {return mDegThresh;}
      bool SlowFusion() const {return mSlowFusion;}
      bool Restart() const {return mRestart;}
      bool CalcAllSigmas() const {return mCalcAllSigmas;}
      bool RelaxAll() const {return mRelaxAll;}
      bool RotateAll() const {return mRotateAll;}
      bool FusionInactive() const {return mFusionInactive;}
      bool DoPrepRun() const {return mPrepRun;}
      In MaxIncrLevel() const {return mMaxIncrLevel;}
      Nb MaxIncrDist() const {return mMaxIncrDist;}
      Nb IncrBondDist() const {return mIncrBondDist;}
      bool DoProj() const {return mProj;}
      bool AddRotToAux() const {return mAddRotToAux;}
      ModeOptType GetModeOptType() const {return mModeOptType;}
      HessType GetHessType() const {return mHessType;}
      CoupEstType GetCoupEstType() const {return mCoupEstType;}
      FreqAnaCalcDef* GetpFreqAnaCalcDef() {return &mFreqAnaCalcDef;}
      RotCoordCalcDef* GetpRotCoordCalcDef() {return &mRotCoordCalcDef;}
};



#endif //FALCONCALCDEF_H
