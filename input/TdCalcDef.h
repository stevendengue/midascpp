/**
 *******************************************************************************
 * 
 * @file    TdCalcDef.h
 * @date    07-08-2016
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Calculation definitions for TD module (time dependent).
 *
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TDCALCDEF_H_INCLUDED
#define TDCALCDEF_H_INCLUDED

/**
 * @brief
 *    Calculation definition for TD (time dependent) module.
 **/
class TdCalcDef
{
   private:

   public:
};

#endif/*TDCALCDEF_H_INCLUDED*/
