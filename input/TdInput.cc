/**
 *******************************************************************************
 * 
 * @file    TdInput.cc
 * @date    07-08-2016
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Input for TD (time dependent) module.
 *
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "input/Input.h"
//#include "input/TdCalcDef.h"

/**
 * @brief
 *    Reads input for TD (time-dependent) module.
 **/
std::string TdInput
   (  std::istream& Minp
   ,  bool passive
   ,  std::string aInputFlag
   )
{
   if(gDoTd)
   {
      MidasWarning("TD (time dependent) input read twice.");
   }

   // Enable calculation and add a calc. def.
   gDoTd = true;
   gTdCalcDef.push_back(TdCalcDef());
   auto&& new_td = gTdCalcDef.back();

   // Set up input words and such.
   string s; //To be returned when we are done reading this level
   In input_level = I_2;
   enum INPUT {ERROR, TDQM1D}; 
   const map<string,INPUT> input_word =
   {
      {"#"+std::to_string(input_level)+"TDQM1D",TDQM1D},
   };

   // Read input.
   bool already_read=false;
   while ((already_read || getline(Minp,s)) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))  // readin until input levels point up
   {
      string s_orig = s;
      if (passive) continue;                                  // If passive loop simple to next #
      transform(s.begin(),s.end(),s.begin(),(In(*) (In))toupper);         // Transform to all upper.
      while(s.find(" ")!= s.npos) s.erase(s.find(" "),I_1);   // Delete ALL blanks
      
      INPUT input = midas::input::FindKeyword(input_word, s);
      already_read = false;
      switch(input)
      {
         case TDQM1D:
         {
// MBH-NB
//            s = TDQM1DInput(Minp,input_level);
//            already_read=true;
            break;
         }
         case ERROR: //FALL THROUGH TO DEFAULT!
         default:
         {
            Mout << " Keyword " << s_orig;
            Mout << " is not a Td level " << input_level << " input keyword";
            Mout << " - input flag ignored! " << std::endl;
            MIDASERROR("Check your input please.");
         }
      }
   }
   return s;
}

void InitializeTd()
{
   gDoTd       = false; 
   gTdIoLevel  = I_0;
}
