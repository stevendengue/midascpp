#ifndef MIDAS_INPUT_MAKEABSPATH_H_INCLUDED
#define MIDAS_INPUT_MAKEABSPATH_H_INCLUDED

#include <string>

namespace midas
{
namespace input
{

//! Check if path is relative and if it is make it absolute by starting in scratch.
std::string AbsPathScratch(const std::string& aPath);

//! Search for file.
std::string SearchForFile(const std::string& aPath);

} /* namespace input */
} /* namespace midas */

#endif /* MIDAS_INPUT_MAKEABSPATH_H_INCLUDED */
