/**
************************************************************************
* 
* @file                VscfInput.cc
*
* 
* Created:             19-11-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Input reader for the Mvscf module 
* 
*
* Last modified:       02-06-2014 (Carolin Koenig)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <vector>
#include <map>
#include <algorithm>
#include <string>


// Link to standard headers:
#include "inc_gen/math_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "input/Input.h"
#include "input/VscfCalcDef.h"
#include "input/ModeCombi.h"
#include "input/ModeCombiOpRange.h"
#include "input/MaxHoEner.h"
#include "input/ThermalSpectrumDef.h"
#include "input/GeneralInputParser.h"
#include "util/MultiIndex.h"
#include "input/GlobalOperatorDefinitions.h"
#include "input/OpDef.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/FindKeyword.h"
#include "mpi/Impi.h"
#include "mpi/FileToString.h"

using std::vector;
using std::map;
using std::string;
using std::transform;
using std::sort;
using std::unique;
using std::find;
string RspInput(std::istream&,VscfCalcDef&, bool);
/**
* Read and check VSCF input.
* Read until next s = "#i..." i<3 which is returned.
* */
string VscfInput(std::istream& Minp)
{
   if (gDoVscf && gDebug) 
      Mout << " I have read one Vscf input - reading a new Vscf input "<< endl;
   gDoVscf    = true; 
   gVscfCalcDef.push_back(VscfCalcDef());
   VscfCalcDef& new_vscf = gVscfCalcDef[gVscfCalcDef.size()-1];  // new_oper is a reference to the new operator
   new_vscf.Reserve();                                           // Reservations of STL space;


   // Enum/map with input flags
   enum INPUT {ERROR, IOLEVEL, NAME, OPER, BASIS, OCCUPATION, OCCUMAX, OCCMAXSUMN, OCCMAXEXCI,
      OCCMAXEXPRMODE, NOTSAVE, RESTART, MAXITER, RESIDTHR, ENERTHR, DIAGMETH, 
      DUPLICATE, SCREENZERO, SCREENZEROCOEF,
      ITEQMAXIT,ITEQMAXDIM,ITEQBREAKDIM,ITEQRESIDTHR,ITEQENERTHR,IMPROVEDPRECOND,
      TARGETINGMETHOD,OVERLAPMIN,OVERLAPSUMMIN,INTSTORAGE,VECSTORAGE,TIMEIT,RSP,
      OCCGROUNDSTATE,OCCALLFUND,OCCFUNDAMENTALS,OCCALLFIRSTOVER,OCCFIRSTOVER,OCCGENCOMBI,
      NMODES,NOOLSEN,OCCALLWITHHOMAXE,ANHARMGUESS,FASTTEMPAVE,ONESTATEVSCFAVE,ALLVIRTEXPTVALUES,CALCDENS,PLOTDENS, VSCFANALYSISDIR,
      ENERGYDIFFMAX,RESIDTHRFOROTHERS,ENERTHRFOROTHERS,
      MAPPREVIOUSVECTORS,LEVEL2SOLVER,USETHERMALDENSITY,THERMALOFFSET,THERMALSPECTRUM,THERMALOCCUPLIMIT,
      STATEAVERAGE,FUNDONLYSTATEAVERAGE,INTANALYSIS,MODALLIMITSFORPOSTVSCF,DUMPMODALSFORMATREP};
   const map<string,INPUT> input_word =
   {
      {"#3IOLEVEL",IOLEVEL},
      {"#3NAME",NAME},
      {"#3OPER",OPER},
      {"#3BASIS",BASIS},
      {"#3OCCUP",OCCUPATION},
      {"#3OCCUMAX",OCCUMAX},
      {"#3OCCMAXSUMN",OCCMAXSUMN},
      {"#3OCCMAXEXCI",OCCMAXEXCI},
      {"#3OCCMAXEXPRMODE",OCCMAXEXPRMODE},
      {"#3DONOTSAVE",NOTSAVE},
      {"#3RESTART",RESTART},
      {"#3MAPPREVIOUSVECTORS",MAPPREVIOUSVECTORS},
      {"#3MAXITER",MAXITER},
      {"#3THRESHOLD",ENERTHR},
      {"#3THRESHOLD-RESID",RESIDTHR},
      {"#3THRESHOLD-ENER",ENERTHR},
      {"#3DIAGMETH",DIAGMETH},
      {"#3DUPLICATE",DUPLICATE},
      {"#3SCREENZERO",SCREENZERO},
      {"#3SCREENZEROCOEF",SCREENZEROCOEF},
      {"#3ITEQMAXIT",ITEQMAXIT},
      {"#3ITEQMAXDIM",ITEQMAXDIM},
      {"#3ITEQBREAKDIM",ITEQBREAKDIM},
      {"#3ITEQRESIDTHR",ITEQRESIDTHR},
      {"#3ITEQENERTHR",ITEQENERTHR},
      {"#3INTSTORAGE",INTSTORAGE},
      {"#3VECSTORAGE",VECSTORAGE},
      {"#3TIMEIT",TIMEIT},
      {"#3RSP",RSP},
      {"#3OCCGROUNDSTATE",OCCGROUNDSTATE},
      {"#3OCCALLFUND",OCCALLFUND},
      {"#3OCCFUNDAMENTALS",OCCFUNDAMENTALS},
      {"#3OCCALLFIRSTOVER",OCCALLFIRSTOVER},
      {"#3OCCFIRSTOVER",OCCFIRSTOVER},
      {"#3OCCGENCOMBI",OCCGENCOMBI},
      {"#3NMODES",NMODES},
      {"#3NOOLSEN",NOOLSEN},
      {"#3OCCALLWITHHOMAXE",OCCALLWITHHOMAXE},
      {"#3ANHARMGUESS",ANHARMGUESS},
      {"#3FASTTEMPAVE",FASTTEMPAVE},
      {"#3ONESTATEVSCFAVE",ONESTATEVSCFAVE},
      {"#3ALLVIRTEXPTVALUES",ALLVIRTEXPTVALUES},
      {"#3CALCDENSITIES",CALCDENS},
      {"#3PLOTDENSITIES",PLOTDENS},
      {"#3VSCFANALYSISDIR",VSCFANALYSISDIR},
      {"#3IMPROVEDPRECOND",IMPROVEDPRECOND},
      {"#3TARGETINGMETHOD",TARGETINGMETHOD},
      {"#3OVERLAPMIN",OVERLAPMIN},
      {"#3OVERLAPSUMMIN",OVERLAPSUMMIN},
      {"#3ENERGYDIFFMAX",ENERGYDIFFMAX},
      {"#3RESIDTHRFOROTHERS",RESIDTHRFOROTHERS},
      {"#3ENERTHRFOROTHERS",ENERTHRFOROTHERS},
      {"#3LEVEL2SOLVER",LEVEL2SOLVER},
      {"#3USETHERMALDENSITY",USETHERMALDENSITY},
      {"#3THERMALOFFSET",THERMALOFFSET},
      {"#3THERMALSPECTRUM",THERMALSPECTRUM},
      {"#3THERMALOCCUPLIMIT",THERMALOCCUPLIMIT},
      {"#3STATEAVERAGE",STATEAVERAGE}, 
      {"#3FUNDONLYSTATEAVERAGE",FUNDONLYSTATEAVERAGE},
      {"#3INTANALYSIS",INTANALYSIS}
      , {"#3MODALLIMITSFORPOSTVSCF",MODALLIMITSFORPOSTVSCF}
      , {"#3DUMPMODALSFORMATREP",DUMPMODALSFORMATREP}
   };

   // Initialization of default values for quantities read in later &  Local params.
   vector<In> occ_vec;
   vector<In> occmax_vec;
   vector<In> occmaxexprmode_vec;
   occ_vec.reserve(gReserveNmodes);
   occmax_vec.reserve(gReserveNmodes);
   occmaxexprmode_vec.reserve(gReserveNmodes);
   In max_sum_n = 0;
   In max_exci  = 0;
   In n_dup     = 1;
   bool max_sum_n_set = false;
   bool max_exci_set  = false;
   bool b_ground_state =false;
   bool b_all_fund     =false;
   bool b_fundamentals =false;
   bool b_all_firstover=false;
   bool b_firstover    =false;
   std::vector<In> fundamentals;
   std::vector<In> first_overtones;
   std::set<std::map<In, In> > gen_combis;
   In n_modes_inp = I_0;
   bool occ_all_with_ho_maxe = false;
   Nb e_occ_all_with_ho_maxe = C_0;
   bool oper_needed=true;

   string s="";
   In input_level = 3;
   bool already_read = false;
   while ((already_read || midas::input::GetLine(Minp,s)) && !(s.substr(I_0,I_1)=="#"&& atoi(s.substr(I_1,I_1).c_str()) < input_level))          // readin until input levels point up
   {
      string s_orig = s;
      s = midas::input::ParseInput(s);
      INPUT input = midas::input::FindKeyword(input_word,s);
      if (already_read) already_read = false;

      switch(input)
      {
         case IOLEVEL:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetIoLevel(midas::util::FromString<In>(s));
            break;
         }
         case NAME:
         {
            midas::input::GetLine(Minp, s); // Get new line with input
            new_vscf.SetName(s);
            new_vscf.SetPrefix(s);
            for(In i=I_0; i<gVscfCalcDef.size()-1; ++i)
            {
               const auto& vscf = gVscfCalcDef[i];
               MidasAssert(vscf.Prefix() != new_vscf.Prefix(), "Vscf calculations name '" + vscf.Prefix() + "' is already occupied");
            }
            break;
         }
         case OPER:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string s_oper="";
            input_s >> s_oper;
            //if (s_oper=="NONE")
            //{
            //   oper_needed=false;
            //   break;
            //}
            new_vscf.SetOper(s_oper);
            //// Check if operator info is available
            //string info=s_oper+".INFO";
            //if (InquireFile(info))
            //{
            //   Mout << " Vscf Input: Operator info file found: ";
            //   std::istringstream oper_info = midas::mpi::FileToStringStream(info);
            //   midas::input::GetLine(oper_info,info);
            //   midas::input::GetLine(oper_info,info);
            //   oper_info >> n_modes_inp; 
            //   if (gIoLevel > I_10 || gDebug)
            //   {
            //      Mout << " Vscf input: Operator info file found: ";
            //      Mout << " - Nr. of modes in operator: " << n_modes_inp << std::endl;
            //   }
            //}
            //else
            //{
            //   Mout << " Vscf Input: Operator info file not found" << endl;
            //}
            break;
         }
         case BASIS:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string s_basis="";
            input_s >> s_basis;
            new_vscf.SetBasis(s_basis);
            break;
         }
         case OCCUPATION:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            occ_vec = midas::util::VectorFromString<In>(s);
            break;
         }
         case OCCUMAX:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            occmax_vec = midas::util::VectorFromString<In>(s);
            break;
         }
         case OCCMAXSUMN:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            max_sum_n = midas::util::FromString<In>(s);
            max_sum_n_set=true;
            break;
         }
         case OCCMAXEXPRMODE:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            occmaxexprmode_vec = midas::util::VectorFromString<In>(s);
            break;
         }
         case OCCMAXEXCI:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            max_exci = midas::util::FromString<In>(s);
            max_exci_set=true;
            break;
         }
         case NOTSAVE:
         {
            bool save = false;
            new_vscf.SetSave(save);
            break;
         }
         case RESTART:
         {
            bool restart = true;
            new_vscf.SetRestart(restart);
            break;
         }
         case MAPPREVIOUSVECTORS:
         {
            // Check if restart info is available
            if (InquireFile(gAnalysisDir+"/VSCFRESTART.INFO"))
            {
               Mout << " Vscf Input: Restart info file found." << endl;
               bool map_it = true;
               new_vscf.SetMapPreviousVectors(map_it);
            }
            else MIDASERROR(" Vscf Input: Restart info file not found.");
            break;
         }
         case MAXITER:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetMaxIter(midas::util::FromString<In>(s));
            break;
         }
         case RESIDTHR:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetResidThr(midas::util::FromString<Nb>(s));
            break;
         }
         case ENERTHR:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetEnerThr(midas::util::FromString<Nb>(s));
            break;
         }
         case DIAGMETH:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string diag_meth="";
            input_s >> diag_meth;
            transform(diag_meth.begin(),diag_meth.end(),diag_meth.begin(),(In(*) (In))toupper);         // Transform to all upper.
            new_vscf.SetDiagMeth(diag_meth);
            break;
         }
         case DUPLICATE:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            n_dup = midas::util::FromString<In>(s);
            break;
         }
         case SCREENZERO:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetScreenZero(midas::util::FromString<Nb>(s));
            break;
         }
         case SCREENZEROCOEF:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetScreenZeroCoef(midas::util::FromString<Nb>(s));
            break;
         }

         case ITEQMAXIT:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetItEqMaxIter(midas::util::FromString<In>(s));
            break;
         }
         case ITEQMAXDIM:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetRedDimMax(midas::util::FromString<In>(s));
            break;
         }
         case ITEQBREAKDIM:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetRedBreakDim(midas::util::FromString<In>(s));
            break;
         }
         case ITEQRESIDTHR:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetItEqResidThr(midas::util::FromString<Nb>(s));
            break;
         }
         case ITEQENERTHR:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetItEqEnerThr(midas::util::FromString<Nb>(s));
            break;
         }
         case INTSTORAGE:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetIntStorage(midas::util::FromString<In>(s));
            break;
         }
         case VECSTORAGE:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            In ist=midas::util::FromString<In>(s); 
            if(midas::mpi::GlobalSize() >1 && ist>1) {
               Mout << "Vecstorage 0 or 1 when using MPI." << endl;
               Mout << "Resetting to 1 from " << ist << endl;
               ist=I_1;
            }
            new_vscf.SetVecStorage(ist);
            break;
         }
         case TIMEIT: 
         {
            new_vscf.SetTimeIt(true);
            break;
         }
         case RSP:
         {
            s = RspInput(Minp,new_vscf,true);
            already_read = true;
            break;
         }
         case OCCGROUNDSTATE:
         {
            b_ground_state = true;
            break;
         }
         case OCCALLFUND:
         {
            b_all_fund= true;
            break;
         }
         case OCCFUNDAMENTALS:
         {
            b_fundamentals = true;
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            fundamentals = midas::util::VectorFromString<In>(s);
            break;
         }
         case OCCALLFIRSTOVER:
         {
            b_all_firstover = true;
            break;
         }
         case OCCFIRSTOVER:
         {
            b_firstover= true;
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            first_overtones = midas::util::VectorFromString<In>(s);
            break;
         }
         case OCCGENCOMBI:
         {
            already_read = new_vscf.ReadGenCombi(Minp, s, gen_combis);

            break;
         }
         case NMODES:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            n_modes_inp = midas::util::FromString<In>(s);
            break;
         }
         case NOOLSEN:
         {
            new_vscf.SetOlsen(false);                           
            break;
         }
         case OCCALLWITHHOMAXE:
         {
            occ_all_with_ho_maxe = true;                        
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            e_occ_all_with_ho_maxe = midas::util::FromString<Nb>(s);
            break;
         }
         case ANHARMGUESS:
         {
            //Mout << " initial value of gVscfHarmGuess: " << gVscfHarmGuess << endl;
            new_vscf.SetAnharmGuess(true);
            //Mout << " setting gVscfHarmGuess to: " << gVscfHarmGuess << endl;
            break;
         }
         case FASTTEMPAVE:
         {
            new_vscf.SetFastTempAve(true);
            break;
         }
         case ONESTATEVSCFAVE:
         {
            new_vscf.SetOneStateVscfAve(true);
            break;
         }
         case ALLVIRTEXPTVALUES:
         {
            new_vscf.SetAllVirtExptValues(true);
            break;
         }
         case CALCDENS:
         {
            new_vscf.SetCalcDensities(true);
            break;
         }
         case PLOTDENS:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetPlotNmodals(midas::util::FromString<In>(s));
            new_vscf.SetPlotDensities(true);
            break;
         }
         case VSCFANALYSISDIR:
         {
            midas::input::GetLine(Minp, s);
            new_vscf.SetmVscfAnalysisDir(s);
         }
         case IMPROVEDPRECOND: 
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetImprovedPrecond(midas::util::FromString<In>(s));
            break;
         }
         case LEVEL2SOLVER:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string meth="";
            input_s >> meth;
            transform(meth.begin(),meth.end(),meth.begin(),(In(*) (In))toupper);         // Transform to all upper.
            new_vscf.SetLevel2Solver(meth);
            break;
         }
         case TARGETINGMETHOD:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            istringstream input_s(s);
            string meth="";
            input_s >> meth;
            transform(meth.begin(),meth.end(),meth.begin(),(In(*) (In))toupper);         // Transform to all upper.
            new_vscf.SetTargetingMethod(meth);
            break;
         }
         case OVERLAPMIN:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetOverlapMin(midas::util::FromString<Nb>(s));
            break;
         }
         case OVERLAPSUMMIN:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetOverlapSumMin(midas::util::FromString<Nb>(s));
            break;
         }
         case ENERGYDIFFMAX:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetEnergyDiffMax(midas::util::FromString<Nb>(s)/C_AUTKAYS); // energy conversion cm^{-1} to au
            break;
         }
         case RESIDTHRFOROTHERS:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetResidThrForOthers(midas::util::FromString<Nb>(s));
            break;
         }
         case ENERTHRFOROTHERS:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetEnerThrForOthers(midas::util::FromString<Nb>(s));
            break;
         }
         case USETHERMALDENSITY:
         {
            // read a line containing the temperature
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            if(s.find("#")!=s.npos) 
            {
               Mout << "#3 UseThermalDensity expects one line with"
                    << " a number (the temperature) and (optionally)"
                    << " one line with modal offsets." << endl;
               MIDASERROR("");
            }
            new_vscf.SetUseThermalDensity(true);
            new_vscf.SetThermalDensityTemp(midas::util::FromString<Nb>(s));
            break;
         }
         case THERMALOFFSET:
         {
            // possibly also get a vector of integers (modal offsets)
            //In start_pos=Minp.tellg();
            midas::input::GetLine(Minp,s);
            new_vscf.SetThermalDensityOffset(midas::util::VectorFromString<In>(s));
            break;
         }
         case THERMALSPECTRUM:
         {
            // read in operators and frq range
            // for now: n op1 op2 ... opn frq_begin frq_end frq_step temp gamma
            // example: 3 x y z 0 4000 2 298.15 10.0
            map<string,string> global_map;
            vector< map<string,string> > vec_of_local;
            GeneralInputParser parser(I_4,Minp,&global_map,"SPECTRUM",&vec_of_local);
            for(In i=0;i<vec_of_local.size();i++) 
            {
               ThermalSpectrumDef spec_def;
               spec_def.Setup(vec_of_local[i]);
               new_vscf.AddThermalSpectrum(spec_def);
            }
            break;
         }
         case THERMALOCCUPLIMIT:
         {
            midas::input::GetLine(Minp,s);                                  // Get new line with input
            new_vscf.SetThermalOccupThr(midas::util::FromString<Nb>(s));
            break;
         }
         case STATEAVERAGE:
         {
            // get a vector of integers with the number of modals averaged over per mode 
            midas::input::GetLine(Minp,s);
            new_vscf.SetStateAveNrs(midas::util::VectorFromString<In>(s));
            break;
         }
         case FUNDONLYSTATEAVERAGE:
         {
            new_vscf.SetFundOnlyStateAve(); 
            break;
         } 
         case MODALLIMITSFORPOSTVSCF:
         {
            new_vscf.SetModalLimitsInput(true);
            midas::input::GetLine(Minp,s);
            new_vscf.SetModalLimits(midas::util::VectorFromString<In>(s));
            break;
         }
         case INTANALYSIS:
         {
            new_vscf.SetIntAnalysis(true);
            midas::input::GetLine(Minp,s);
            new_vscf.SetIntAnalysisLimit(midas::util::FromString<In>(s));
            break;
         }
         case DUMPMODALSFORMATREP:
         {
            new_vscf.SetDumpModalsForMatRep(true);
            break;
         }
         default:
         {
            Mout << " Keyword " << s_orig << 
               " is not a Vscf level 3 Input keyword - input flag ignored! " << endl;
            MIDASERROR(" Check your input please ");
         }
      }
   }

   // Check that the VSCF specific analysis directory is set, else it will be initialized to the global analysis directory
   if (new_vscf.GetmVscfAnalysisDir().empty())
   {
      new_vscf.SetmVscfAnalysisDir(gAnalysisDir);
   }

   // Check that the analysis directory for this vscf calculation exists, otherwise create it
   auto analysis_dir = new_vscf.GetmVscfAnalysisDir();
   if (!midas::filesystem::IsDir(analysis_dir) && !midas::filesystem::IsSymlink(analysis_dir))
   {
      midas::filesystem::Mkdir(analysis_dir);
   }

   // Are we using general mode combis?
   bool use_gen_combi = !gen_combis.empty();

   if (occ_vec.size()!=I_0) n_modes_inp = occ_vec.size();
   if (n_modes_inp == I_0) 
   {
      if (gOperatorDefs.GetNrOfOpers() != 0 && new_vscf.Oper() != "") 
      {
         //In oper_nr = gOperatorDefs.GetOperatorNr(new_vscf.Oper());
         //OpDef* op_def = &gOperatorDefs[oper_nr];
         OpDef* op_def = &(gOperatorDefs.GetOperator(new_vscf.Oper()));
         n_modes_inp = op_def->NmodesInOp();
      }
      else if(gOperatorDefs.GetNrOfOpers() == 1)
      {
         Mout  << " Nr. of modes and operator in VSCF is still not set. \n"
               << " I will assume the one and only operator available at this stage will be used " 
               << std::endl;
         std::string s_oper = gOperatorDefs[I_0].Name();
         new_vscf.SetOper(s_oper);
         //In oper_nr = gOperatorDefs.GetOperatorNr(new_vscf.Oper());
         //OpDef* op_def = &gOperatorDefs[oper_nr];
         OpDef* op_def = &(gOperatorDefs.GetOperator(new_vscf.Oper()));
         n_modes_inp = op_def->NmodesInOp();
      }
      else
      {
         Mout << " No operator given in Vscf Input " << endl;
         if (gOperatorDefs.GetNrOfOpers()>1) Mout 
            << " Do not know which operator to choose among the available " 
            << gOperatorDefs.GetNrOfOpers() << endl;
         MIDASERROR("No operators given/can be guessed in vscf input" );
      }
   }
   string s_name = new_vscf.Name();
   if (s_name.size()==0)
   {
      s_name = new_vscf.Oper()+"_"+new_vscf.Basis(); // Default name (base) is oper_basis
      new_vscf.SetName(s_name);
   }

   // if DUPLICATION of input is wanted then do it 
   if (n_dup > 1)
   {
      Mout << " Creating occupation input in " << n_dup << " duplicates " << endl;
      In noccv = occ_vec.size();
      In noccm = occmax_vec.size();
      In noccme= occmaxexprmode_vec.size();
      for (In i_dup=0;i_dup<n_dup-1;i_dup++)
      {
         for (In i=0;i<noccv;i++) occ_vec.push_back(occ_vec[i]);
         for (In i=0;i<noccm;i++) occmax_vec.push_back(occmax_vec[i]);
         for (In i=0;i<noccme;i++) occmaxexprmode_vec.push_back(occmaxexprmode_vec[i]);
      }
   }

   // Construct name for Vscf and store the Vscf input information on a VscfCalcDef on the
   // global gVscfCalcDef vector.
   // If occui was given more vscf inputs are construct.

   In nvscf = I_1;
   // check increment maxima 

   if (gIoLevel > I_10 || gDebug)
   {
      Mout << " occ: [";
      In n_mod = occ_vec.size() - I_1;
      for (In i_op_mode = I_0; i_op_mode < n_mod; i_op_mode++)
      {
         Mout << occ_vec[i_op_mode] << ",";
      }
      if (occ_vec.size() > I_0)
      {
         Mout << occ_vec[occ_vec.size() - I_1];
      }
      Mout << "]" << std::endl;
      Mout << " max: [";
      n_mod = occmax_vec.size() - I_1;
      for (In i_op_mode = I_0; i_op_mode < n_mod; i_op_mode++)
      {
         Mout << occmax_vec[i_op_mode] << ",";
      }
      if (occmax_vec.size() > I_0)
      {
         Mout << occmax_vec[occmax_vec.size() - I_1];
      }
      Mout << "]" << std::endl;
   }

   bool old_style=false;
   if (!occ_all_with_ho_maxe && occmax_vec.size()>0 )
   {
      old_style = true;
      if (occmax_vec.size() != occ_vec.size())
         MIDASERROR("Input error, occupation and occuincrement vector not same size ");
      for (In i=0;i<occmax_vec.size();i++) 
      {
         nvscf *= occmax_vec[i]+1;
         if (nvscf >1) break;
      }
      if (gDebug && nvscf > 1) Mout  << " Constructing vscf inputs from occ & occuin vectors" << endl;
   }
   if (occmaxexprmode_vec.size()>0 )
      if (occmax_vec.size() != occ_vec.size()) 
         MIDASERROR("Input error, occupation and exprmode vector not same size ");

   // Info for new type fundamentals and overtone input 
   if (b_all_fund) 
   {
      fundamentals.resize(n_modes_inp);
      for (In i_f = I_0;i_f<fundamentals.size();i_f++) fundamentals[i_f]=i_f;
   }
   if (b_all_firstover) 
   {
      first_overtones.resize(n_modes_inp);
      for (In i_f = I_0;i_f<first_overtones.size();i_f++) first_overtones[i_f]=i_f;
   }
   In n_auto_types = I_0;
   if (b_ground_state) n_auto_types ++;
   if (b_all_fund || b_fundamentals) n_auto_types += fundamentals.size();
   if (b_all_firstover || b_firstover ) n_auto_types += first_overtones.size();

   nvscf += gen_combis.size();
   n_auto_types += gen_combis.size();

   nvscf += n_auto_types;

   if (nvscf==1 && n_auto_types==I_0)
   {
      string s_name2 = s_name +"_"+std::to_string(0)+"."+std::to_string(0); 
      new_vscf.SetName(s_name2);
      new_vscf.SetOccVec(occ_vec);
   }

   // The old way of generating many different types of states. 
   if (old_style && nvscf>1)
   {
      In first_new = gVscfCalcDef.size()-1;
      string s_name2 = s_name +"_"+std::to_string(0)+"."+std::to_string(0); 
      gVscfCalcDef[first_new].SetName(s_name2);
      gVscfCalcDef[first_new].SetOccVec(occ_vec);

      // Find max_exci_level from max_exci and/or occmax_vec
      In max_exci_level = 0;
      for (In i=0;i<occmax_vec.size();i++)
         if (occmax_vec[i]>occ_vec[i]) max_exci_level++;
      if (max_exci_set) max_exci_level = min(max_exci,max_exci_level);

      ModeCombiOpRange range = ConstructModeCombiOpRange(max_exci_level,occmaxexprmode_vec,occ_vec.size());
      In n_mode_combi = range.Size();

      // i_pur = 0, count, i_pur = 1, do addition
      // the count round will count the number of vscf and reserve space for the 
      // subsequent addition of the VccCalcDef 
      bool countfirst=true;
      In i_pur_first = 0;
      if (countfirst) i_pur_first=1;
      for (In i_pur=i_pur_first;i_pur<2;i_pur++)
      {
         // count variable.
         In i_vscf_count = 1;
         for (In i_mode_combi=1;i_mode_combi<n_mode_combi;i_mode_combi++)
         {
            const ModeCombi& modes_excited = range.GetModeCombi(i_mode_combi);
            In n_excited = modes_excited.Size();
            if (gIoLevel > I_10) 
            {
               Mout << " The modes excited are: ";
               for (In i = I_0; i < n_excited; i++)
               {
                  Mout << " " << modes_excited.Mode(i);
               }
               Mout << std::endl;
            }
   
            vector<In> occ(n_excited);
            vector<In> occmax(n_excited);
   
            for (In i=0;i<n_excited;i++) 
            {
               In mode   = modes_excited.Mode(i);
               occ[i]    = occ_vec[mode]; 
               occmax[i] = occmax_vec[mode];
            }
            if (gIoLevel > I_10 || gDebug )
            {
               Mout << " partial occ ";
               for (In i = I_0; i < n_excited; i++)
               {
                  Mout << " " << occ[i];
               }
               Mout << std::endl;
               Mout << " partial occmax ";
               for (In i = I_0; i < n_excited; i++)
               {
                  Mout << " " << occmax[i];
               }
               Mout << std::endl;
            }
   
            MultiIndex mi_for_modecombi(occ,occmax,"LOWHIG",true);
   
            vector<In> occ2(occ);          // The active part.
            vector<In> occ_vec_new(occ_vec);  // The full occupation vector.
   
            In n_vscf = mi_for_modecombi.Size();
            for (In i_vscf=0;i_vscf<n_vscf;i_vscf++)
            {
               mi_for_modecombi.IvecForIn(occ2,i_vscf);
               for (In i=0;i<n_excited;i++) 
               {
                  In mode   = modes_excited.Mode(i);
                  occ_vec_new[mode] = occ2[i]; 
               }
   
               if (max_sum_n_set)
            {
                  In sum_n=0;
                  for (In i=0;i<occ_vec_new.size();i++) sum_n += occ_vec_new[i];
                  if (max_sum_n < sum_n) continue;
               }
   
               if (max_exci_set)
               {
                  In sum_excimodes=0;
                  for (In i=0;i<occ_vec_new.size();i++) 
                     if (occ_vec_new[i] != occ_vec[i]) sum_excimodes++;
                  if (max_exci < sum_excimodes) continue;
               }
   
               if (gIoLevel > I_10 || gDebug)
               {
                  Mout << " Occ: [";
                  In n_mod = occ_vec_new.size() - I_1;
                  for (In i_op_mode = I_0; i_op_mode < n_mod; i_op_mode++)
                  {
                     Mout << occ_vec_new[i_op_mode] << ",";
                  }
                  if (occ_vec_new.size() > I_0)
                  {
                     Mout << occ_vec_new[occ_vec_new.size() - I_1];
                  }
                  Mout << "]" << std::endl;
               }
   
               if (i_pur == I_1)
               {
                  string s_name2 = s_name +"_"+std::to_string(i_mode_combi)+"."+std::to_string(i_vscf); 
                  gVscfCalcDef.push_back(gVscfCalcDef[first_new]);  // Use direct adressing since reference may gone wrong by now due to push_back
                  In new_add = gVscfCalcDef.size()-1;
                  gVscfCalcDef[new_add].SetName(s_name2);
                  gVscfCalcDef[new_add].Reserve();
                  gVscfCalcDef[new_add].SetOccVec(occ_vec_new);
               }
               i_vscf_count++;
   
            }
         }
         if (i_pur == I_0)
         {
            In nvscf_tot = first_new + I_1 + i_vscf_count;
            gVscfCalcDef.reserve(nvscf_tot);
            if (gIoLevel > I_4)
            {
               Mout << i_vscf_count << " vscf input will be constructed " << std::endl;
            }
         }
         if (i_pur == I_1)
         {
            if (gIoLevel > I_4)
            {
               Mout << i_vscf_count << " vscf input has been constructed " << std::endl;
            }
         }
      }
   }
 
   if (n_auto_types>I_0)
   {
      if (old_style) MIDASERROR ( " Do not mix input styles - either occumax or other way, not both");
      In i_vscf = I_0;
      In first_new = gVscfCalcDef.size()-1;
      vector<In> occ_vec_new(n_modes_inp);
      bool first = true;
      if (b_ground_state)
      {
         string s_name2 = s_name +"_"+std::to_string(i_vscf); 
         if (!first) gVscfCalcDef.push_back(gVscfCalcDef[first_new]);  
         In new_add = gVscfCalcDef.size()-1;
         gVscfCalcDef[new_add].SetName(s_name2);
         gVscfCalcDef[new_add].Reserve();
         for (In i=I_0;i<occ_vec_new.size();i++) occ_vec_new[i]=I_0; // Zero. 
         if (gIoLevel > I_10 || gDebug)
         {
            Mout << " Occupation Vector: " << occ_vec_new << std::endl;
         }
         gVscfCalcDef[new_add].SetOccVec(occ_vec_new);
         i_vscf++;
         first=false;
      }
      if (b_fundamentals||b_all_fund)
      {
         for (In i_f = I_0;i_f<fundamentals.size();i_f++)
         {
            string s_name2 = s_name +"_"+std::to_string(i_vscf); 
            if (!first) gVscfCalcDef.push_back(gVscfCalcDef[first_new]);  
            In new_add = gVscfCalcDef.size()-1;
            gVscfCalcDef[new_add].SetName(s_name2);
            gVscfCalcDef[new_add].Reserve();
            for (In i=I_0;i<occ_vec_new.size();i++) occ_vec_new[i]=I_0; // Zero. 
            if (fundamentals[i_f]>n_modes_inp) 
                MIDASERROR(" Mode nr. beyond nr of modes for fundamentals ");
            occ_vec_new[fundamentals[i_f]] = I_1;
            if (gIoLevel > I_10 || gDebug)
            {
               Mout << " Occupation Vector: " << occ_vec_new << std::endl;
            }
            gVscfCalcDef[new_add].SetOccVec(occ_vec_new);
            i_vscf++;
            first=false;
         }
      }
      if (b_firstover||b_all_firstover)
      {
         for (In i_f = I_0;i_f<first_overtones.size();i_f++)
         {
            string s_name2 = s_name +"_"+std::to_string(i_vscf); 
            if (!first) gVscfCalcDef.push_back(gVscfCalcDef[first_new]);  
            In new_add = gVscfCalcDef.size()-1;
            gVscfCalcDef[new_add].SetName(s_name2);
            gVscfCalcDef[new_add].Reserve();
            for (In i=I_0;i<occ_vec_new.size();i++) occ_vec_new[i]=I_0; // Zero. 
            if (first_overtones[i_f] > n_modes_inp)
            {
               MIDASERROR(" Mode nr. beyond nr of modes for first overtone inp ");
            }
            occ_vec_new[first_overtones[i_f]] = I_2;
            if (gIoLevel > I_10 || gDebug)
            {
               Mout << " Occupation Vector: " << occ_vec_new << std::endl;
            }
            gVscfCalcDef[new_add].SetOccVec(occ_vec_new);
            i_vscf++;
            first=false;
         }
      }
      if (  use_gen_combi
         )
      {
         Mout << " Nr of gen. combis: " << gen_combis.size() << std::endl;

         bool first = true;

         // Loop over input vectors
         for (const auto& gen_combi : gen_combis)
         {
            if (  gen_combi.size() > n_modes_inp
               )
            {
               MIDASERROR("Number of excited modes in gen. combi. is larger than the total number of modes!");
            }

            // Emplace new VSCF calculation
            std::string s_name2 = s_name +"_"+std::to_string(i_vscf); 
            if (  !first
               )
            {
               gVscfCalcDef.emplace_back(gVscfCalcDef[first_new]);
            }
            gVscfCalcDef.back().SetName(s_name2);
            gVscfCalcDef.back().Reserve();

            // Zero occ vec
            for(auto& occ : occ_vec_new)
            {
               occ = I_0;
            }

            // Set excitation elements
            for(const auto& exci : gen_combi)
            {
               const auto& exci_mode = exci.first;
               const auto& exci_level = exci.second;

               if (  exci_mode > n_modes_inp - I_1
                  )
               {
                  MIDASERROR("Excitation '" + std::to_string(exci_mode) + "^" + std::to_string(exci_level) + "' is not valid for a system with " + std::to_string(n_modes_inp) + " vibrational modes!");
               }

               if (  occ_vec_new[exci_mode] == I_0
                  )
               {
                  occ_vec_new[exci_mode] = exci_level;
               }
               else
               {
                  MIDASERROR("The same mode cannot be excited multiple times!");
               }
            }

            // Set occ vector in new VSCF
            gVscfCalcDef.back().SetOccVec(occ_vec_new);

            first = false;
            ++i_vscf;
         }
      }
   }
   if (occ_all_with_ho_maxe)
   {
      In i_vscf = I_0;
      In first_new = gVscfCalcDef.size()-1;
      vector<In> max_occ_vec(n_modes_inp);
      if (occmax_vec.size()>0 || occmax_vec.size()== max_occ_vec.size()) 
         max_occ_vec = occmax_vec;
      Mout << " Create VSCF input for states with HO energy below " << e_occ_all_with_ho_maxe << endl;
      string oper_name = gVscfCalcDef[first_new].Oper();
      MaxHoEner getting_occvecs(max_occ_vec,oper_name,e_occ_all_with_ho_maxe);
      vector<InVector> i_vec_vec;
      getting_occvecs.GetAllOccVecs(i_vec_vec);
      In n_new_states=i_vec_vec.size();
      bool first = true;
      for (In i_state=I_0; i_state<n_new_states;i_state++)
      {
         string s_name2 = s_name +"_"+std::to_string(i_vscf); 
         if (!first) gVscfCalcDef.push_back(gVscfCalcDef[first_new]);  
         In new_add = gVscfCalcDef.size()-1;
         gVscfCalcDef[new_add].SetFastTempAve(false);
         gVscfCalcDef[new_add].SetName(s_name2);
         gVscfCalcDef[new_add].Reserve();
         gVscfCalcDef[new_add].SetOccVec(i_vec_vec[i_state]);
         first=false;
         i_vscf++;
      }
   }
   return s;
}

/**
 * Initialize Vscf variables
 **/
void InitializeVscf()
{
   // Mout << " Initializing vscf variables: " << endl;
   gDoVscf              = false; 
}
