#include <numeric>
#include <cmath>

/**
 *
 * @brief Build Polynomial used in expansion for the modified spherical Bessel function of first kind 
 *
 * Purpose: Calculates the polynomial g_n(x) in order to evaluate the modified spherical Bessel 
 *          function of first kind. 
 *
 *          \f[
 *             g_{n+1} =  g_{n-1} - (2n+1) x^{-1} g_n 
 *          \f]
 * 
 * @param order gives the order of polynomial 
 * @param the x value (must be greater than zero)
 *
 **/
template <class T>
std::vector<T> GetGPolynomial
   (  int& order
   ,  T& x
   )
{
   int nlen = std::abs(order) + 1;
   std::vector<T> g(nlen);

   if (x <= 0) MIDASERROR("GetPolynomial only implemented for x > 0");

   g[0] = 1.0 / x;

   if (order > 0) 
   {
      if (order >= 1) 
      {
         g[1] = - std::pow(x,-2.0);
      }

      // g_{n+1} =  g_{n-1} - (2n+1) x^(-1) g_n 
      if (order > 1) 
      {
         T x1m = std::pow(x,-1.0);
         for (int i = 2; i < nlen; i++)
         {
            //g[i] = g[i-2] - (2 * (i-1) + 1) * std::pow(x,-1.0) * g[i-1];
            g[i] = g[i-2] - (2 * (i-1) + 1) * x1m * g[i-1];
         }
      }
   }
   else
   {
      if (order <= -1) 
      {
         g[1] = 0;
      }

      // g_{n-1} = (2n+1) x^(-1) g_n + g_{n+1}
      if (order < -1) 
      {
         T x1m = std::pow(x,-1.0);
         for (int i = 2; i < nlen; i++)
         {
            //g[i] = (-2 * (i-1) + 1) * std::pow(x,-1.0) * g[i-1] + g[i-2];
            g[i] = (-2 * (i-1) + 1) * x1m * g[i-1] + g[i-2];
         }
      }
   }

   return g;
}




/**
 *
 * @brief Calculates modified spherical Bessel function of first kind
 *
 * Purpose: Calculates the modified spherical Bessel function of first kind
 *          using the formula
 *
 *          \f[
 *              i_n(x) = g_n(x) * \sinh x + g_{-(n+1)} \cosh x
 *          \f]
 *          which holds for positive x. 
 * 
 * @param l order of the modified spherical Bessel function of first kind
 * @param x value for which modified spherical Bessel function should be evaluated. Should be positive! 
 *
 **/
template <class T>
T ModSphBes1knd
   (  int l
   ,  T& x
   )
{
   T retval = static_cast<T>(0.0);

   // quick return
   if (x <= std::numeric_limits<T>::epsilon())
   {
      if (l == 1) 
      {
         return static_cast<T>(1.0);
      }
      else
      {
         return static_cast<T>(0.0);
      }
   }
   
   switch (l)
   {
      /*
       * For the first 4 orders we use explicit expresions 
       */
      case 0:
      {
         retval = std::sinh(x) / x;
         break;
      }
      case 1:
      {
         retval = (x*std::cosh(x) - std::sinh(x)) / std::pow(x,2.0);
         break;
      }
      case 2:
      {
         retval = ( (std::pow(x,2.0) + 3.0) * std::sinh(x) - 3.0 * x * std::cosh(x)) / std::pow(x,3.0);
         break;
      }
      case 3:
      {
         retval = ( (std::pow(x,3.0) + 15.0) * std::cosh(x) - (6.0 * std::pow(x,2) + 15.0) * std::sinh(x)) / std::pow(x,4.0);
         break;
      }
      case 4:
      {
         retval = ( (std::pow(x,4.0) + 45.0 * std::pow(x,2.0) + 105.0) * std::sinh(x) 
                  - (10.0 * std::pow(x,3) + 105.0) * std::cosh(x)) / std::pow(x,5.0);
         break;
      }
      /*
       * General case, for which we build the polynomials in front of cosh and sinh recursively 
       */
      default:
      {

         // i_n(x) = g_n(x) * sinh x + g_{-(n+1)} cosh x
         std::vector<T> gcosh = GetGPolynomial(-(l+1),x);
         std::vector<T> gsinh = GetGPolynomial(l,x);

         retval = gsinh[l] * std::sinh(x) + gcosh[l+1] * std::cosh(x);
         break;
      }
   }

   return retval;
}

template <class T>
std::vector<T> ModSphBes1kndArr
   (  int lmax
   ,  T& x
   )
{
   std::vector<T> retvec(lmax+1); 

   int mlmax = -(lmax+1);

   // i_n(x) = g_n(x) * sinh x + g_{-(n+1)} cosh x
   std::vector<T> gcosh = GetGPolynomial(mlmax,x);
   std::vector<T> gsinh = GetGPolynomial( lmax,x);

   // fill in array retvec
   for (int lval = 0; lval <= lmax; lval++)
   {
      retvec[lval] = gsinh[lval] * std::sinh(x) + gcosh[lval+1] * std::cosh(x);
   }

   return retvec;
}

template <class T>
std::vector<T> ModSphBes1kndRecur
   (  int lmax
   ,  T& x
   )
{
   std::vector<T> in(lmax+1); 

   int mlmax = -(lmax+1);

   if (lmax == 0)
   {
      in[0] = std::sinh(x) / x;
      return in;
   }

   if (lmax >= 1)
   {
      in[0] = std::sinh(x) / x;
      in[1] = (x*std::cosh(x) - std::sinh(x)) / std::pow(x,2.0);
   }

   // i_n+1 = i_n-1 - (2n + 1)/x i_n 
   for (int n = 1; n < lmax; n++)
   {
      in[n+1] = in[n-1] - (2 * n + 1) / x * in[n];
   }

   return in;
}


