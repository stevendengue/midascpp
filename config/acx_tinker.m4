dnl @synopsis ACX_TINKER([ACTION-IF-FOUND[, ACTION-IF-NOT-FOUND]])
dnl
dnl Enable the use of Tinker with MidasCpp
dnl
dnl For Tinker, go to: http://dasher.wustl.edu/tinker/
dnl
dnl Added by MBH: Nov. 19th, 2010
dnl
dnl Version: 0.1, Nov. 19th, 2010
dnl

AC_DEFUN([ACX_TINKER], [
AC_PREREQ(2.59) dnl for AC_LANG_CASE

AC_MSG_NOTICE(I try path to Tinker = $enable_tinker)

dnl check the given path to tinker, must be owned by
dnl the user due to the need for recompilation

if ! test -e $enable_tinker/source; then
   AC_MSG_WARN(The path specified for Tinker did not contain
      source directory! PATH = "$enable_tinker")
   tinker_path=""
   enable_tinker=0
elif ! test -w $enable_tinker/source; then
   tinker_path=""
   enable_tinker=0
else
   tinker_path=$enable_tinker
   enable_tinker=1
fi

if test x = x"$tinker_path"; then
        $2
        :
fi

])dnl ACX_MPI
