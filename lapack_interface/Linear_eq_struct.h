#ifndef LINEAR_EQ_STRUCT_H_INCLUDED
#define LINEAR_EQ_STRUCT_H_INCLUDED

#include "util/MidasStream.h"
extern MidasStream Mout;

namespace detail
{

template<class T> struct Linear_eq_struct_traits
{
   using a_type = T;
};

} /* namespace detail */

/**
 * LINEAR EQUATION SOLVERS
 **/
template<typename T>
struct Linear_eq_struct
{
   using a_type = typename detail::Linear_eq_struct_traits<T>::a_type;
   
   int info = -1;
   int n = 0, n_rhs = 0;
   //std::unique_ptr<a_type[]> solution = nullptr;
   std::unique_ptr<a_type[]> solution; 
   
   void Output()
   {
      Mout << " Linear solutions: " << std::endl;
      int counter = 0;
      for(int i=0; i<n; ++i)
      {   
         for(int j=0; j<n_rhs; ++j)
         {
            Mout << " " << solution[counter];
            ++counter;
         }
         Mout << "\n";
      }
   }
};

/**
 * load single rhs into vector
 **/
template<class T>
void LoadSolution(const Linear_eq_struct<T>& sol, GeneralMidasVector<T>& vec)
{
   assert(sol.n_rhs == 1); // check we have only one rhs
   vec.SetNewSize(static_cast<In>(sol.n));
   // copy elements
   for(In i = 0; i < sol.n; ++i)
   {
      vec[i] = sol.solution[i]; 
   }
}

/**
 * load single rhs into vector
 **/
template<class T>
void LoadSolution(const Linear_eq_struct<T>& sol, GeneralMidasMatrix<T>& mat)
{
   mat.SetNewSize(static_cast<In>(sol.n), static_cast<In>(sol.n_rhs));
   In counter = 0;
   // copy elements
   for(In i = 0; i < sol.n_rhs; ++i)
   {
      for(In j = 0; j < sol.n; ++j)
      {
         mat[j][i] = sol.solution[counter]; 
         ++counter;
      }
   }
}

#endif /* LINEAR_EQ_STRUCT_H_INCLUDED */
