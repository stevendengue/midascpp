#ifndef GESV_H_INCLUDED
#define GESV_H_INCLUDED

#include "inc_gen/Warnings.h"

#include "lapack_interface/Linear_eq_struct.h"
#include "lapack_interface/ColMajPtrFromMidasMatrix.h"
#include "lapack_interface/PtrFromMidasVector.h"
#include "lapack_interface/math_wrappers.h"

/**
 * DGESV solve A*X = B where A is general N x N
 **/
template<class T>
Linear_eq_struct<T> GESV(T* arMatrix, T* brMatrix, int aN, int aNrhs)
{
   int n=aN;          // this is stupid :O
   int nrhs=aNrhs;    // this is stupid :O
   int lda=max(1,aN);
   int ldb=max(1,aN);
   std::unique_ptr<int[]> ipiv(new int[aN]);
   int info;

   midas::lapack_interface::gesv_(&n,&nrhs,arMatrix,&lda,ipiv.get(),brMatrix,&ldb,&info);
   if(info != 0)
   {
      MidasWarning("GESV info=" + std::to_string(info));
   }
   
   Linear_eq_struct<T> sol;
   sol.info = info;
   sol.n = aN;
   sol.n_rhs = aNrhs;
   sol.solution = std::unique_ptr<T[]>(new T[aN*aNrhs]);
   //T* p = new T[aN*aNrhs];
   for(int i=0; i<aN*aNrhs; ++i)
   {
      sol.solution[i] = brMatrix[i];
   }
   //for(int i=0; i<aNrhs; ++i)
   //{
   //   sol._solution[i] = p + i*aN;
   //}
   
   return sol;
}

template<class T>
Linear_eq_struct<T> GESV(T** arMatrix, T** brMatrix, int aN, int aNrhs)
{
   return GESV(arMatrix[0],brMatrix[0],aN,aNrhs);
};

/**
 * Overload for GESV for solving MidasMatrix*x = MidasVector
 **/
template<class T>
Linear_eq_struct<T> GESV(const GeneralMidasMatrix<T>& arMatrix, const GeneralMidasVector<T>& brVec)
{
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);
   auto b = PtrFromMidasVector(brVec);

   return GESV(a.get(),b.get(),arMatrix.Nrows(),1);
}

/**
 * Overload for GESV for solving MidasMatrix*x = MidasMatrix
 **/
template<class T>
Linear_eq_struct<T> GESV(const GeneralMidasMatrix<T>& arMatrix, const GeneralMidasMatrix<T>& brMatrix)
{
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);
   auto b = ColMajPtrFromMidasMatrix2(brMatrix);

   return GESV(a.get(),b.get(),arMatrix.Nrows(),brMatrix.Ncols());
}

#endif /* GESV_H_INCLUDED */
