#ifndef PTRFROMMIDASVECTOR_H_INCLUDED
#define PTRFROMMIDASVECTOR_H_INCLUDED

#include <memory>

#include "mmv/MidasVector.h"

template<class T>
std::unique_ptr<T[]> PtrFromMidasVector(const GeneralMidasVector<T>& aVec)
{
   std::unique_ptr<T[]> ptr(new T[aVec.Size()]);
   for(int i=0; i<aVec.Size(); ++i)
   {
      ptr[i] = aVec[i];
   }
   return ptr;
}

#endif /* PTRFROMMIDASVECTOR_H_INCLUDED */
