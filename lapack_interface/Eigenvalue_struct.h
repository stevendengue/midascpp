#ifndef EIGENVALUE_STRUCT_H_INCLUDED
#define EIGENVALUE_STRUCT_H_INCLUDED

#include "libmda/numeric/float_eq.h"

#include "ILapack.h"
#include "ILapackUtils.h"
#include "SetMidasMatrixFromColMajPtr.h"

/**
 * EIGENVALUE SOLVERS
 **/

/**
 * Structure for holding eigen solutions of symmetric/Hermitian problems where all eigenvalues are real.
 **/
template
   <  typename T = double
   >
struct Eigenvalue_struct
{
   // One could do "get" and "set" functions for less error prone use
   // will do when I'm not the only one using this :O (this will add some re-work later 3:-O )
   using type = T;
   using real_t = midas::type_traits::RealTypeT<type>;
   
   int _info;
   int _n, _num_eigval;
   real_t* _eigenvalues;
   T** _eigenvectors;

   explicit Eigenvalue_struct(): _n(0), _num_eigval(0), _eigenvalues(nullptr), _eigenvectors(nullptr) 
   { 
   }
   
   //Copy constructor/assignment:
   Eigenvalue_struct(const Eigenvalue_struct&) = delete;
   Eigenvalue_struct& operator=(const Eigenvalue_struct&) = delete;

   //Move constructor/assignment:
   Eigenvalue_struct(Eigenvalue_struct&& other)
      : _info(other._info)
      , _n(other._n)
      , _num_eigval(other._num_eigval)
      , _eigenvalues(other._eigenvalues)
      , _eigenvectors(other._eigenvectors)
   {
      other._eigenvalues = nullptr;
      other._eigenvectors = nullptr;
   }

   Eigenvalue_struct& operator=(Eigenvalue_struct&& other)
   {
      if(this != &other)
      {
         _info = other._info;
         _n = other._n;
         _num_eigval = other._num_eigval;
         _eigenvalues = other._eigenvalues;
         _eigenvectors = other._eigenvectors;

         other._eigenvalues = nullptr;
         other._eigenvectors = nullptr;
      }
      return *this;
   }


   ~Eigenvalue_struct()
   {
      if(_eigenvalues)
      {
         delete[] _eigenvalues;
      }
      if(_eigenvectors)
      {
         delete[] _eigenvectors[0];
         delete[] _eigenvectors;
      }
   }
};

/**
 * structure for holding extended eigenvalue struct
 **/
template<class T=double>
struct Eigenvalue_ext_struct
{
   using type = T;
   
   int info_;  
   int n_, num_eigval_;
   T* re_eigenvalues_;
   T* im_eigenvalues_;
   T** lhs_eigenvectors_;
   T** rhs_eigenvectors_;
   bool lhs_conjugated_ = false;

   //Constructors, etc.:
   explicit Eigenvalue_ext_struct(): n_(0), num_eigval_(0), re_eigenvalues_(nullptr), im_eigenvalues_(nullptr)
                               , lhs_eigenvectors_(nullptr), rhs_eigenvectors_(nullptr)
   { 
   }

   Eigenvalue_ext_struct(const Eigenvalue_ext_struct&) = delete;
   Eigenvalue_ext_struct& operator=(const Eigenvalue_ext_struct&) = delete;

   Eigenvalue_ext_struct(Eigenvalue_ext_struct&& other)
      :  info_(other.info_)
      ,  n_(other.n_)
      ,  num_eigval_(other.num_eigval_)
      ,  re_eigenvalues_(other.re_eigenvalues_)
      ,  im_eigenvalues_(other.im_eigenvalues_)
      ,  lhs_eigenvectors_(other.lhs_eigenvectors_)
      ,  rhs_eigenvectors_(other.rhs_eigenvectors_)
      ,  lhs_conjugated_(other.lhs_conjugated_)
   {
      other.re_eigenvalues_ = nullptr;
      other.im_eigenvalues_ = nullptr;
      other.lhs_eigenvectors_ = nullptr;
      other.rhs_eigenvectors_ = nullptr;
   }

   Eigenvalue_ext_struct& operator=(Eigenvalue_ext_struct&& other)
   {
      if(this != &other)
      {
         info_ = other.info_;
         n_ = other.n_;
         num_eigval_ = other.num_eigval_;
         re_eigenvalues_ = other.re_eigenvalues_;
         im_eigenvalues_ = other.im_eigenvalues_;
         lhs_eigenvectors_ = other.lhs_eigenvectors_;
         rhs_eigenvectors_ = other.rhs_eigenvectors_;
         lhs_conjugated_ = other.lhs_conjugated_;

         other.re_eigenvalues_ = nullptr;
         other.im_eigenvalues_ = nullptr;
         other.lhs_eigenvectors_ = nullptr;
         other.rhs_eigenvectors_ = nullptr;
      }
      return *this;
   }

   ~Eigenvalue_ext_struct()
   {
      if(re_eigenvalues_)
      {
         delete[] re_eigenvalues_;
      }
      if(im_eigenvalues_)
      {
         delete[] im_eigenvalues_;
      }
      if(lhs_eigenvectors_)
      {
         delete[] lhs_eigenvectors_[0];
         delete[] lhs_eigenvectors_;
      }
      if(rhs_eigenvectors_)
      {
         delete[] rhs_eigenvectors_[0];
         delete[] rhs_eigenvectors_;
      }
   }

};

/**
 *
 **/
template<class T>
void NormalizeEigenvectors(Eigenvalue_struct<T>& eig_sol, ilapack::normalize_t)
{
   ilapack::util::normalize_columns(eig_sol._eigenvectors[0], eig_sol._n, eig_sol._num_eigval);
}

/**
 *
 **/
template<class T>
void NormalizeEigenvectors(Eigenvalue_ext_struct<T>& eig_sol, ilapack::normalize_rhs_t)
{
   ilapack::util::normalize_columns_complex(eig_sol.rhs_eigenvectors_[0], eig_sol.im_eigenvalues_, eig_sol.n_, eig_sol.num_eigval_);
}

/**
 *
 **/
template<class T>
void NormalizeEigenvectors(Eigenvalue_ext_struct<T>& eig_sol, ilapack::normalize_lhs_t)
{
   ilapack::util::normalize_columns_complex(eig_sol.lhs_eigenvectors_[0], eig_sol.im_eigenvalues_, eig_sol.n_, eig_sol.num_eigval_);
}

/**
 *
 **/
template<class T>
void NormalizeEigenvectors(Eigenvalue_ext_struct<T>& eig_sol, ilapack::normalize_t)
{
   ilapack::util::normalize_columns_complex(eig_sol.rhs_eigenvectors_[0], eig_sol.im_eigenvalues_, eig_sol.n_, eig_sol.num_eigval_);
   ilapack::util::normalize_columns_complex(eig_sol.lhs_eigenvectors_[0], eig_sol.im_eigenvalues_, eig_sol.n_, eig_sol.num_eigval_);
}

/**
 *
 **/
template<class T>
void NormalizeEigenvectors(Eigenvalue_ext_struct<T>& eig_sol, ilapack::binormalize_lhs_t)
{
   if(!eig_sol.lhs_conjugated_)
   {
      ilapack::util::columns_complex_conjugate(eig_sol.lhs_eigenvectors_[0],eig_sol.im_eigenvalues_,eig_sol.n_,eig_sol.num_eigval_);
      eig_sol.lhs_conjugated_ = true;
   }
   ilapack::util::binormalize_columns_unbalanced(eig_sol.lhs_eigenvectors_[0], eig_sol.rhs_eigenvectors_[0]
                                               , eig_sol.im_eigenvalues_, eig_sol.n_, eig_sol.num_eigval_);
}

/**
 *
 **/
template<class T>
void NormalizeEigenvectors(Eigenvalue_ext_struct<T>& eig_sol, ilapack::binormalize_rhs_t)
{
   if(!eig_sol.lhs_conjugated_)
   {
      ilapack::util::columns_complex_conjugate(eig_sol.lhs_eigenvectors_[0],eig_sol.im_eigenvalues_,eig_sol.n_,eig_sol.num_eigval_);
      eig_sol.lhs_conjugated_ = true;
   }
   ilapack::util::binormalize_columns_unbalanced(eig_sol.rhs_eigenvectors_[0], eig_sol.lhs_eigenvectors_[0]
                                               , eig_sol.im_eigenvalues_, eig_sol.n_, eig_sol.num_eigval_);
}

/**
 *
 **/
template<class T>
void NormalizeEigenvectors(Eigenvalue_ext_struct<T>& eig_sol, ilapack::binormalize_t)
{
   if(!eig_sol.lhs_conjugated_)
   {
      ilapack::util::columns_complex_conjugate(eig_sol.lhs_eigenvectors_[0],eig_sol.im_eigenvalues_,eig_sol.n_,eig_sol.num_eigval_);
      eig_sol.lhs_conjugated_ = true;
   }
   ilapack::util::binormalize_columns_balanced(eig_sol.lhs_eigenvectors_[0], eig_sol.rhs_eigenvectors_[0]
                                             , eig_sol.im_eigenvalues_, eig_sol.n_, eig_sol.num_eigval_);
}

/**
 * @brief
 *    Transfer eig.vals. to vector.
 *
 * Transfer the eigenvalues from an Eigenvalue_struct<T> to a
 * GeneralMidasVector<T>.
 * 
 * @param[in] eig_sol
 *    The Eigenvalue_struct<T> from which to get eigenvalues.
 * @param[out] vec
 *    Write the eigenvalues to here, deleting any previous contents.
 **/
template<class T>
void LoadEigenvalues(const Eigenvalue_struct<T>& eig_sol, GeneralMidasVector<T>& vec)
{
   vec.SetNewSize(In(eig_sol._num_eigval));
   for(int i = 0; i < eig_sol._num_eigval; ++i)
   {
      vec[i] = eig_sol._eigenvalues[i];
   }
}

/**
 * @brief
 *    Transfer real part of eig.vals. to vector.
 *
 * Transfer the real part of the eigenvalues from an Eigenvalue_ext_struct<T>
 * to a GeneralMidasVector<T>.
 * 
 * @param[in] eig_sol
 *    The Eigenvalue_struct<T> from which to get eigenvalues.
 * @param[out] vec
 *    Write the eigenvalues to here, deleting any previous contents.
 **/
template<class T>
void LoadEigenvaluesRe(const Eigenvalue_ext_struct<T>& eig_sol, GeneralMidasVector<T>& vec)
{
   vec.SetNewSize(eig_sol.num_eigval_);
   for(int i = 0; i < eig_sol.num_eigval_; ++i)
   {
      vec[i] = eig_sol.re_eigenvalues_[i];
   }
}

/**
 * @brief
 *    Transfer imaginary part of eig.vals. to vector.
 *
 * Transfer the imaginary part of the eigenvalues from an
 * Eigenvalue_ext_struct<T> to a GeneralMidasVector<T>.
 * 
 * @param[in] eig_sol
 *    The Eigenvalue_struct<T> from which to get eigenvalues.
 * @param[out] vec
 *    Write the eigenvalues to here, deleting any previous contents.
 **/
template<class T>
void LoadEigenvaluesIm(const Eigenvalue_ext_struct<T>& eig_sol, GeneralMidasVector<T>& vec)
{
   vec.SetNewSize(eig_sol.num_eigval_);
   for(int i = 0; i < eig_sol.num_eigval_; ++i)
   {
      vec[i] = eig_sol.im_eigenvalues_[i];
   }
}

/**
 * @brief
 *    Transfer eig.vecs. to the _columns_ of a matrix.
 *
 * Transfer the eigenvectors from Eigenvalue_struct<T> to
 * GeneralMidasMatrix<T>. The eigenvectors will be stored as _column_ in the
 * matrix.
 *
 * @param[in] eig_sol
 *    The Eigenvalue_struct<T> from which to get eigenvectors.
 * @param[out] arMat
 *    Write the eigenvectors to here, as columns, deleting any previous
 *    contents.
 **/
template<class T>
void LoadEigenvectors(const Eigenvalue_struct<T>& eig_sol, GeneralMidasMatrix<T>& arMat)
{
   SetMidasMatrixFromColumnMajorPtr(arMat, eig_sol._eigenvectors, eig_sol._n, eig_sol._num_eigval);
}

/**
 * @brief
 *    Transfer right-hand-side eig.vecs. to the _columns_ of a matrix.
 *
 * Transfer the right-hand-side eigenvectors from Eigenvalue_ext_struct<T> to
 * GeneralMidasMatrix<T>. The eigenvectors will be stored as _columns_ in the
 * matrix.
 *
 * @note
 *    Pure data transfer, no processing wrt. complex eig.vals/vecs.
 *
 * @param[in] eig_sol
 *    The Eigenvalue_struct<T> from which to get eigenvectors.
 * @param[out] arMat
 *    Write the eigenvectors to here, as columns, deleting any previous
 *    contents.
 **/
template<class T>
void LoadEigenvectorsRhs(const Eigenvalue_ext_struct<T>& eig_sol, GeneralMidasMatrix<T>& arMat)
{
   SetMidasMatrixFromColumnMajorPtr(arMat, eig_sol.rhs_eigenvectors_, eig_sol.n_, eig_sol.num_eigval_);
}

/**
 * @brief
 *    Transfer left-hand-side eig.vecs. to the _columns_ of a matrix.
 *
 * Transfer the left-hand-side eigenvectors from Eigenvalue_ext_struct<T> to
 * GeneralMidasMatrix<T>. The eigenvectors will be stored as _columns_ in the
 * matrix.
 *
 * @note
 *    Pure data transfer, no processing wrt. complex eig.vals/vecs.
 *
 * @param[in] eig_sol
 *    The Eigenvalue_struct<T> from which to get eigenvectors.
 * @param[out] arMat
 *    Write the eigenvectors to here, as columns, deleting any previous
 *    contents.
 **/
template<class T>
void LoadEigenvectorsLhs(const Eigenvalue_ext_struct<T>& eig_sol, GeneralMidasMatrix<T>& arMat)
{
   SetMidasMatrixFromColumnMajorPtr(arMat, eig_sol.lhs_eigenvectors_, eig_sol.n_, eig_sol.num_eigval_);
}

#endif /* EIGENVALUE_STRUCT_H_INCLUDED */
