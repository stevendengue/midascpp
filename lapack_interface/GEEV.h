#ifndef GEEV_H_INCLUDED
#define GEEV_H_INCLUDED

#include <memory> // for std::unique_ptr
#include <type_traits> // for std::is_floating_point

#include "inc_gen/Warnings.h"

#include "lapack_interface/Eigenvalue_struct.h"
#include "lapack_interface/ColMajPtrFromMidasMatrix.h"
#include "lapack_interface/PtrFromMidasVector.h"
#include "lapack_interface/math_wrappers.h"

/**
 * GEEV
 **/

// GEEV: 
// computes eigenvalues for:
//    A*x=lambda*x
// where A is a general N*N matrix saved in column major format!
// define either userfriendly or efficient (not both :O ).
#define USERFRIENDLY_GEEV
template<class T>
Eigenvalue_ext_struct<T> GEEV(T* arMatrix, int aN, char aJobvl = 'V', char aJobvr = 'V')
{
   static_assert(std::is_floating_point<T>::value, "GEEV only implemented for floating point at the moment.");

   char jobvl = aJobvl;  // a little extra work but adds some clarity...
   char jobvr = aJobvr;  
   int lda = max(1,aN);
   int ldvl = max(1,aN);
   int ldvr = max(1,aN);
   std::unique_ptr<T[]> wr(new T[aN]);
   std::unique_ptr<T[]> wi(new T[aN]);
   std::unique_ptr<T[]> vl(new T[ldvl*aN]);
   std::unique_ptr<T[]> vr(new T[ldvr*aN]);
   int lwork = max(10000,4*aN); // can perhaps be optimized by using ilaenv... 
   std::unique_ptr<T[]> work(new T[lwork]);
   int info;
   
   // do solution
   midas::lapack_interface::geev_(&jobvl, &jobvr, &aN, arMatrix, &lda, wr.get(), wi.get(), vl.get(), &ldvl, vr.get(), &ldvr, work.get(), &lwork, &info);
   if(info != 0)
   {
      MidasWarning("GEEV INFO=" + std::to_string(info));
   }
   
   /*cout << " eigenvalues are: \n";
   for(int i=0; i<aN; ++i)
      cout << w[i] << endl;*/
   
   //
   // put solution into eigenvalue struct
   //
   Eigenvalue_ext_struct<T> eigen;
   eigen.info_ = info;
   eigen.n_ = aN;
   eigen.num_eigval_ = aN;
   eigen.re_eigenvalues_ = wr.release();
   eigen.im_eigenvalues_ = wi.release();
   eigen.lhs_eigenvectors_ = new T*[aN];
   eigen.rhs_eigenvectors_ = new T*[aN];
#ifdef USERFRIENDLY_GEEV // userfriendly but less efficient
   for(int i=0; i<aN; ++i)
   {
      eigen.lhs_eigenvectors_[i] = vl.get() + i*aN;
      eigen.rhs_eigenvectors_[i] = vr.get() + i*aN;
   }
   vl.release(); // release ownership
   vr.release(); // release ownership
#endif /* USERFRIENDLY_GEEV */
#ifdef EFFICIENT_GEEV // more efficient (not implemented)
   exit(42);
   //for(int i=0; i<aN; ++i)
   //   eigen._eigenvectors[i] = arMatrix + i*aN;
   //arMatrix = nullptr;
#endif /* EFFICIENT_GEEV */

   return eigen;
}

template<class T>
Eigenvalue_ext_struct<T> GEEV(const GeneralMidasMatrix<T>& arMatrix, char aJobvl = 'V', char aJobvr = 'V')
{
   assert((arMatrix.Ncols()>0) && arMatrix.Nrows()>0);
   auto a = ColMajPtrFromMidasMatrix2(arMatrix);

   // Do DSYEVD
   return GEEV(a.get(), arMatrix.Ncols(), aJobvl, aJobvr);
}

#endif /* GEEV_H_INCLUDED */
