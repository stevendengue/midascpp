/**
 *******************************************************************************
 * 
 * @file    FunctionTraitsTest.cc
 * @date    11-12-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "FunctionTraitsTest.h"

namespace midas::test
{
   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void FunctionTraitsTest()
   {
      // Create test_suite object.
      cutee::suite suite("FunctionTraits test");

      // Add all the tests to the suite.
      suite.add_test<function_traits::StandardFunctionTraitsTest>("function traits");
      suite.add_test<function_traits::LambdaFunctionTraitsTest>("lambda function traits");
      suite.add_test<function_traits::MemberFunctionTraitsTest>("member function traits");

      // Run the tests.
      RunSuite(suite);
   }

} /* namespace midas::test */
