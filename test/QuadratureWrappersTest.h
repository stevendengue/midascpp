/**
 *******************************************************************************
 * 
 * @file    QuadratureWrappersTest.h
 * @date    23-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Unit tests for quadrature wrappers.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef QUADRATUREWRAPPERSTEST_H_INCLUDED
#define QUADRATUREWRAPPERSTEST_H_INCLUDED

#include <functional>

#include "test/CuteeInterface.h"
#include "inc_gen/TypeDefs.h"

#include "quadrature_wrappers/integration/GaussLegendre.h"

namespace midas::test::quadrature_wrappers
{

#ifdef ENABLE_GSL
/***************************************************************************//**
 * @brief Namespace for testing stuff under quadrature_wrappers::function
 ******************************************************************************/
namespace function
{
   //@{
   //! Tests.
   struct TestGslFunctionFromFunctor : public cutee::test {void run() override;};
   //@}
} /* namespace function */
#endif /* ENABLE_GSL */

/***************************************************************************//**
 * @brief Namespace for testing stuff under quadrature_wrappers::integration
 ******************************************************************************/
namespace integration
{
   using function_t = std::function<Nb(Nb)>;

   //! Returns some test functions and associated anti-derivatives for testing.
   std::pair<std::vector<function_t>, std::vector<function_t>> TestFunctionsAndAntiDerivatives();

   //! Some intervals used for testing.
   std::vector<std::pair<Nb,Nb>> TestIntervals();

   //! Given an antiderivative and a range, computes integral.
   Nb IntegralFromAntiDerivative(const function_t&, Nb, Nb);

   //@{
   //! Tests.
   struct TestGaussLegendre : public cutee::test {void run() override;};
   //@}
} /* namespace integration */

} /* namespace midas::test::quadrature_wrappers */

#endif/*QUADRATUREWRAPPERSTEST_H_INCLUDED*/
