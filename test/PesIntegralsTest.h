/**
 *******************************************************************************
 * 
 * @file    PesIntegralsTest.cc
 * @date    20-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Unit tests for pes/integrals classes.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef PESINTEGRALSTEST_H_INCLUDED
#define PESINTEGRALSTEST_H_INCLUDED

#include <vector>

#include "test/CuteeInterface.h"

#include "inc_gen/TypeDefs.h"

#include "pes/integrals/OneModeIntegrals.h"

namespace midas::test::pes_integrals
{

using midas::pes::integrals::OneModeIntegrals;
using function_t = OneModeIntegrals<std::function<Nb(Nb)>>::function_t;

std::pair<std::vector<function_t>, std::vector<function_t>> TestFunctionsAndAntiDerivatives();
std::vector<std::pair<Nb,Nb>> TestIntervals();
Nb IntegralFromAntiDerivative(const function_t&, Nb, Nb);

/***************************************************************************//**
 * @brief Namespace for testing pes::integrals::OneModeIntegrals.
 ******************************************************************************/
namespace onemodeintegrals{
   /************************************************************************//**
    * @brief
    *    Struct to provide functions of given template type, for templated
    *    testing.
    *
    * Must provide functions and corresponding anti-derivatives. In principle
    * doesn't matter which as long as they are in correspondance, but let's
    * stick to the following five:
    * \f{align*}{
    *    f(x) &= 1 + x        &F(x) &= x + x^2/2     \\
    *    f(x) &= x^2          &F(x) &= x^3/3         \\
    *    f(x) &= 1 + \sin(x)  &F(x) &= x - \cos(x)   \\
    *    f(x) &= 1 + \cos(x)  &F(x) &= x + \sin(x)   \\
    *    f(x) &= \exp(x)      &F(x) &= \exp(x)       \\
    * \f}
    * Chosen such that integrals on the intervals are of the order of 1
    * (specifically they are not 0, since small absolute errors then are large
    * relatively).
    ***************************************************************************/
   template<typename T>
   struct FunctionGenerator
   {
      using function_t = T;
      std::vector<std::pair<Nb,Nb>> intervals =
         {  { -1.0, 1.0}
         ,  {  0.0, 1.0}
         ,  {-C_PI, C_PI}
         ,  {  0.0, C_PI}
         };
      std::vector<T> Functions() const;

      std::vector<std::function<Nb(Nb)>> anti_derivatives =
         {  [](Nb x){return x + pow(x,2)/2.;}
         ,  [](Nb x){return pow(x,3)/3.;}
         ,  [](Nb x){return x - cos(x);}
         ,  [](Nb x){return x + sin(x);}
         ,  [](Nb x){return exp(x);}
         };
      Nb IntegralFromAntiDerivative(Uin aFunc, Nb aBeg, Nb aEnd, Nb aScale = C_1);
   };

   struct Constructor : public cutee::test {void run() override;};
   struct IntervalBeginEndPoints : public cutee::test {void run() override;};
   template<typename F> struct Integrals : public cutee::test {void run() override;};
   template<typename F> struct IntegralsScaling : public cutee::test {void run() override;};

} /* namespace onemodeintegrals */

/***************************************************************************//**
 * @brief Namespace for testing pes::integrals::SumOverProductIndexer.
 ******************************************************************************/
namespace sumoverproductindexer{
   /************************************************************************//**
    * @brief
    *    Struct for use in ConstructorTempl<T>::run().
    *
    * The idea is that the struct takes a template parameter T, which should
    * then be a functor type, e.g. std::function, GenericFunctionWrapper, etc.
    *
    * The functions Functions() and Equality() must then be specialized for
    * those template parameters accordingly. Then the
    * ConstructorTempl<T>::run() can serve as a template test function for
    * various functor types.
    *
    * @note
    *    It is hardcoded so that for the test to pass the Functions() _must_ be
    *    (where a, b, c are just some non-specific functions, that do not
    *    compare equal):
    *        a, a, a
    *        b, b, a
    *        a, b, a
    *        b, a, a
    *        c, b, a
    *        c, a, a
    *
    ***************************************************************************/
   template<typename T>
   struct SumOverProductGenerator
   {
      std::vector<Nb> coefficients = {0., 1., 2., 3., 4., 5.};
      std::vector<Uin> ctrl_num_funcs = {3, 2, 1};
      Uin ctrl_num_modes = ctrl_num_funcs.size();
      Uin ctrl_num_terms = coefficients.size();
      std::vector<std::vector<Uin>> ctrl_indices = 
         {  {0,0,0}
         ,  {1,1,0}
         ,  {0,1,0}
         ,  {1,0,0}
         ,  {2,1,0}
         ,  {2,0,0}
         };

      std::vector<std::vector<T>> Functions() const;
      static bool Equality(const T&, const T&);
   };

   struct ConstructBasic : public cutee::test {void run() override;};
   template<typename T> struct ConstructAndCheck : public cutee::test {void run() override;};
} /* namespace sumoverproductindexer */

namespace modecombiintegrals{
   /************************************************************************//**
    * Generates sum-over-product with coefficients and functions:
    *      1   x     y
    *     -2   x^2   y
    *      3   x     y^2
    *     -4   x^2   y^2
    *  and intervals
    *     [-2, 0], [0, 1]
    *     [-1, 0], [0, 2]
    *
    *  One-mode functions integrated on intervals:
    *                      [-2,0]   [0,1]
    *       x: [1/2 x^2]   -2       1/2
    *     x^2: [1/3 x^3]   8/3      1/3
    *
    *                      [-1,0]   [0,2]
    *       y: [1/2 y^2]   -1/2     2
    *     y^2: [1/3 y^3]   1/3      8/3
    *
    *  Integrals:
    *      (0,0); [-2,0]x[-1,0]:
    *            1(-2)(-1/2) -2*8/3(-1/2) +3(-2)(1/3) -4*8/3*1/3
    *          = 1 +8/3 -2 -32/9 = -1 -8/9 = -17/9
    *      (0,1); [-2,0]x[0,2]:
    *            1(-2)*2 -2*8/3*2 +3(-2)8/3 -4*8/3*8/3
    *          = -4 -32/3 -16 -256/9 = -20 -(96 + 256)/9 = -(180 + 352)/9 = 532/9
    *      (1,0); [0,1]x[-1,0]:
    *            1(1/2)(-1/2) -2*1/3(-1/2) +3(1/2)(1/3) -4*1/3*1/3
    *          = -1/4 +1/3 +1/2 -4/9 = 1/4 - 1/9 = (9 - 4)/36 = 5/36
    *      (1,1); [0,1]x[0,2]:
    *            1(1/2)(2) -2*1/3(2) +3(1/2)(8/3) -4*1/3*8/3
    *          = 1 -4/3 +4 -32/9 = 5 - (12 + 32)/9 = (45 -44)/9 = 1/9
    ***************************************************************************/
   template<typename T>
   struct SumOverProductGenerator
   {
      std::vector<Nb> coefficients = {1., -2., 3., -4.};
      std::vector<std::vector<std::pair<Nb,Nb>>> intervals =
         {  {  {-2., 0.},  {0.,  1.}   }
         ,  {  {-1., 0.},  {0.,  2.}   }
         }; 
      std::vector<std::vector<Nb>> ctrl =
         {  {  -17./9.           // (0,0)
            ,  -532./9.          // (0,1)
            }
         ,  {  5./36.            // (1,0)
            ,  1./9.             // (1,1)
            }
         };
      std::vector<std::vector<T>> Functions() const;
      static bool Equality(const T&, const T&);
   };

   struct ConstructBasic : public cutee::test {void run() override;};
   template<typename T> struct ConstructAndCheck : public cutee::test {void run() override;};
} /* namespace modecombiintegrals */

} /* namespace midas::test::pes_integrals */

#include "test/PesIntegralsTest_Impl.h"

#endif/*PESINTEGRALSTEST_H_INCLUDED*/
