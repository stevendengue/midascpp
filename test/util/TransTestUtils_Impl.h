/**
 *******************************************************************************
 * 
 * @file    TransTestUtils_Impl.h
 * @date    18-03-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRANSTESTUTILS_IMPL_H_INCLUDED
#define TRANSTESTUTILS_IMPL_H_INCLUDED

#include "input/ModeCombiOpRange.h"
#include "input/GlobalOperatorDefinitions.h"
#include "input/OpDef.h"
#include "util/matrep/MatRepVibOper.h"
#include "util/RandomNumberGenerator.h"
#include "util/type_traits/Complex.h"
#include "test/Random.h"
#include "test/util/OpDefsForTesting.h"
#include "vcc/ModalIntegrals.h"
#include "util/matrep/MatRepUtils.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"

namespace midas::test::util
{
   /************************************************************************//**
    * @param[in] arMcr
    *    Contains the ModeCombi%s.
    * @param[in] arNumModals
    *    The number of modals per mode.
    * @param[in] aZeroRefParam
    *    Whether to set the parameter for the empty ModeCombi equal to zero
    *    instead of some random number.
    * @return
    *    Vector of vectors (one for each ModeCombi) of random parameters; for a
    *    given ModeCombi `{m0, m1,...}` there will be 
    *    `(N_m0 - 1)*(N_m1 - 1)* ...` parameters, where `N_m = arNumModals[m]`.
    * @param[in] aAddend
    *    Number to be _added_ to each number of modals before multiplying them.
    *    `aAddend = -1` is the typical usage for getting the number of excitation
    *    parameters for the ModeCombi.
    ***************************************************************************/
   template<typename T>
   std::vector<std::vector<T>> GetRandomParams
      (  const ModeCombiOpRange& arMcr
      ,  const std::vector<Uin>& arNumModals
      ,  const bool aZeroRefParam
      ,  const In aAddend
      )
   {
      std::vector<std::vector<T>> v;
      v.reserve(arMcr.Size());
      for(const auto& mc: arMcr)
      {
         Uin n_params = NumParams(mc, arNumModals, aAddend);
         if (mc.Size() == 0 && aZeroRefParam)
         {
            v.emplace_back(n_params, T(0));
         }
         else
         {
            v.emplace_back(random::RandomStdVector<T>(n_params));
         }
      }
      return v;
   }

   /************************************************************************//**
    * Constructs OpDef with given (real) coefficients and product terms as
    * specified by ModeCombiOpRange. For each ModeCombi all possible
    * products of 1-mode operators are made, each with their own
    * coefficient.
    * (The empty ModeCombi is neglected even if present in the
    * ModeCombiOpRange.)
    *
    * The 1-mode operators `Q^1(Qx),...,Q^N(Qx)` are just dummy operators in
    * the sense that these functions will not be used for anything, since
    * we'll provide custom modal integrals later. It's only for specifying
    * distinctive 1-mode operators, i.e. for setting up the sum-over-product
    * structure of the operator.
    *
    * The registered number of modes of the OpDef (OpDef::NmodesInOp()) will be
    * equal to the size of arNumOpers, even if the ModeCombiOpRange doesn't
    * contain all modes.
    *
    * See midas::test::vcc2trans::DetailGetOpDefTest for examples.
    *
    * @note
    *    I think this is an awkward way of constructing the OpDef (reading
    *    it in from an istream), but it's the only way I feel confident
    *    everything gets set up consistently in the OpDef object. -MBH, Feb
    *    2019.
    *
    * @param[in] arMcr
    *    Desired ModeCombiOpRange for the operator.
    * @param[in] arNumOpers
    *    Desired number of 1-mode operators for each mode (_not_ including
    *    the identity operator of the mode, which _may_ be provided
    *    automatically by the OpDef methods).
    *    I.e. if `arNumOpers = {N,...}` mode 0 will have the set of 1-mode
    *    operators `Q^1(Q0)`,... ,`Q^N(Q0)`.
    *    (Possibly, there will be an extra operator `Q^0(Q0)` making the OpDef
    *    say there are `N+1` 1-mode operators, but if so, that operator will
    *    _not_ enter in the actual sum-over-product structure.)
    * @param[in] arCoefs
    *    Vector of vectors (one for each ModeCombi) of coefficients.
    ***************************************************************************/
   template<typename T>
   OpDef GetOpDefFromCoefs
      (  const ModeCombiOpRange& arMcr
      ,  const std::vector<Uin>& arNumOpers
      ,  const std::vector<std::vector<T>> arCoefs
      )
   {
      static_assert(!midas::type_traits::IsComplexV<T>, "Only real OpDef coefs. supported.");
      using absval_t = midas::type_traits::RealTypeT<T>;
      using midas::util::matrep::MatRepVibOper;
      const auto n_modes = arNumOpers.size();
      if (arMcr.NumberOfModes() > 0)
      {
         MidasAssert(arMcr.SmallestMode() >= 0, "Smallest mode out-of-range; < 0.");
         MidasAssert(arMcr.LargestMode() < n_modes, "Largest mode out-of-range; >= n_modes.");
      }

      // The mode labels to use.
      std::vector<std::string> v_mode_labels;
      for(Uin m = 0; m < arNumOpers.size(); ++m)
      {
         v_mode_labels.emplace_back(std::string("Q")+std::to_string(m));
      }

      // Set up stringstream formatting.
      std::stringstream ss;
      Uin ssp = 22;
      Uin ssw = ssp + 7; // ssp decimals + sign (1) + X. (2) + E-XX (4).
      ss << std::scientific;
      ss << std::setprecision(ssp);

      // Operator input begin.
      ss << "#0MIDASMOPINPUT\n";

      // Set up mode names; "Q0 Q1 Q2... ".
      ss << "#1MODENAMES\n";
      for(const auto& m_label: v_mode_labels)
      {
         ss << m_label << ' ';
      }
      ss << '\n';

      // Set up trivial scale factors (unimportant for this case, than will
      // anyway use customized modal integrals).
      ss << "#1SCALEFACTORS\n";
      for(Uin m = 0; m < n_modes; ++m)
      {
         ss << absval_t(1) << ' ';
      }
      ss << '\n';

      // The operator terms, where we just construct all multi-mode
      // products. Uses Q^1,...,Q^N for ease of construction/destinction,
      // but the actual functions are not used for anything since we provide
      // customized modal integrals later. Only important things are the
      // sum-over-product structure and the coefficients.
      ss << "#1OPERATORTERMS\n";
      MidasAssert(arCoefs.size() == arMcr.Size(), "Size mismatch; arCoefs and arMcr.");
      for(auto it = arMcr.Begin(1), end = arMcr.end(); it != end; ++it)
      {
         const auto i_mc = std::distance(arMcr.begin(), it);
         const auto& v_mc_coefs = arCoefs.at(i_mc);
         const auto s_mc = midas::util::matrep::SetFromVec(it->MCVec());
         const auto n_opers_mc = midas::util::matrep::SubsetDims(s_mc, arNumOpers);
         const auto n_prods = midas::util::matrep::Product(n_opers_mc);
         MidasAssert(n_prods == v_mc_coefs.size(), "Size mismatch; n_prods ("+std::to_string(n_prods)+") and v_mc_coefs ("+std::to_string(v_mc_coefs.size())+"). i_mc = "+std::to_string(i_mc)+".");
         for(Uin i_prod = 0; i_prod < n_prods; ++i_prod)
         {
            const auto mi_prod = midas::util::matrep::MultiIndex(i_prod, n_opers_mc);
            auto it_m = s_mc.begin(), end_m = s_mc.end();
            auto it_i = mi_prod.begin(), end_i = mi_prod.end();
            ss << std::setw(ssw) << v_mc_coefs.at(i_prod);
            for(; it_m != end_m && it_i != end_i; ++it_m, ++it_i)
            {
               Uin q_power = *it_i + 1;
               ss << " Q^" << q_power << "(" << v_mode_labels.at(*it_m) << ")";
            }
            ss << '\n';
         }
      }

      // Operator input end.
      ss << "#0MIDASMOPINPUTEND\n";

      // Debug!
      //Mout << "GetOpDefFromCoefs, stringstream = \n" << ss.str() << std::endl;

      return SetupOpDef(ss, v_mode_labels);
   }

   /************************************************************************//**
    * Will set up modal integral matrices for each mode and 1-mode operator in
    * the OpDef, each matrix being of dimension `N_m * N_m`, where `N_m =
    * arNumModalsGlobalModes[m]`.
    *
    * The modal integral matrix elements will in _no way_ be determined by the
    * actual 1-mode operators in the OpDef, nor will they have any link to any
    * primitive basis.
    *
    * @param[in] arOpDef
    *    The operator containing
    * @param[in] arNumModalsGlobalModes
    *    The number of modals for each global mode.
    * @param[in] aSymType
    *    'h' (Hermitian), 'a' (anti-Hermitian) or 'g' (general)
    * @return
    *    ModalIntegrals<T> object with random matrix elements and dimensions
    *    (num. modes, num. operators, num. modals) consistent with arOpDef and
    *    arNumModalsGlobalModes.
    ***************************************************************************/
   template<typename T>
   ModalIntegrals<T> GetRandomModalIntegrals
      (  const OpDef& arOpDef
      ,  const std::vector<Uin>& arNumModalsGlobalModes
      ,  char aSymType
      )
   {
      const Uin n_modes = arOpDef.NmodesInOp();

      std::vector<std::vector<typename ModalIntegrals<T>::mat_t>> v;
      v.reserve(n_modes);
      Uin n_modals = 0;
      for(LocalModeNr m = 0; m < n_modes; ++m)
      {
         try
         {
            n_modals = arNumModalsGlobalModes.at(arOpDef.GetGlobalModeNr(m));
         }
         catch(const std::out_of_range& oor)
         {
            MIDASERROR("LocalModeNr m = "+std::to_string(m)+" out-of-range: "+std::string(oor.what()));
         }
         const Uin n_opers = arOpDef.NrOneModeOpers(m);
         std::vector<typename ModalIntegrals<T>::mat_t> v_mats;
         v_mats.reserve(n_opers);
         for(LocalOperNr o = 0; o < n_opers; ++o)
         {
            switch (aSymType)
            {
               case 'h':
               {
                  v_mats.emplace_back(midas::test::random::RandomHermitianMidasMatrix<T>(n_modals));
                  break;
               }
               case 'a':
               {
                  v_mats.emplace_back(midas::test::random::RandomAntiHermitianMidasMatrix<T>(n_modals));
                  break;
               }
               case 'g':
               {
                  v_mats.emplace_back(midas::test::random::RandomMidasMatrix<T>(n_modals, n_modals));
                  break;
               }
               default:
               {
                  MIDASERROR(std::string("aSymType = '")+aSymType+"'; must be 'h', 'a' or 'g'.");
               }
            }
         }
         v.emplace_back(std::move(v_mats));
      }
      return v;
   }

   /************************************************************************//**
    * Constructs vector object (of given size) containing the given
    * parameters in the order specified by the ModeCombi addresses of the
    * ModeCombiOpRange.
    *
    * @param[out] arVecCont
    *    Will be the desired object at end.
    * @param[in] arMcr
    *    Contains the ModeCombi%s defining the parameter order.
    * @param[in] aTotSize
    *    Total number of parameters (as returned by e.g. SetAddresses()).
    * @param[in] arParams
    *    Vector of vectors (one for each ModeCombi) of random parameters. The
    *    number of parameters for each ModeCombi must be consistent with the
    *    "jumps" in ModeCombi addresses.
    ***************************************************************************/
   template<typename T>
   void ConstructParamsVecCont
      (  GeneralMidasVector<T>& arVecCont
      ,  const ModeCombiOpRange& arMcr
      ,  const Uin aTotSize
      ,  const std::vector<std::vector<T>>& arParams
      )
   {
      MidasAssert(arMcr.Size() == arParams.size(), "Size mismatch.");
      GeneralMidasVector<T> vec(aTotSize, T(0));

      auto it_mc = arMcr.begin();
      const auto end_mc = arMcr.end();
      auto it_params = arParams.begin();
      const auto end_params = arParams.end();
      Uin addr = 0;
      for(; it_mc != end_mc && it_params != end_params; ++it_mc, ++it_params)
      {
         MidasAssert(it_mc->Address() == addr, "Unexpected ModeCombi address.");
         MidasAssert(addr + it_params->size() <= vec.Size(), "addr + it_params->size() > vec.Size()");
         for(Uin i = 0; i < it_params->size(); ++i)
         {
            vec[addr + i] = (*it_params)[i];
         }
         addr += it_params->size();
      }
      MidasAssert(it_mc == end_mc && it_params == end_params, "Iterators didn't reach end.");
      MidasAssert(addr == aTotSize, "Final address and aTotSize unequal.");
      
      arVecCont = std::move(vec);
   }

   template<typename T>
   void ConstructParamsVecCont
      (  GeneralDataCont<T>& arVecCont
      ,  const ModeCombiOpRange& arMcr
      ,  const Uin aTotSize
      ,  const std::vector<std::vector<T>>& arParams
      )
   {
      GeneralMidasVector<T> tmp_mv;
      ConstructParamsVecCont(tmp_mv, arMcr, aTotSize, arParams);
      arVecCont = GeneralDataCont<T>(std::move(tmp_mv));
   }

} /* namespace midas::test::util */

#endif/*TRANSTESTUTILS_IMPL_H_INCLUDED*/
