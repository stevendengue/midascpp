/**
 *******************************************************************************
 * 
 * @file    TransTestUtils.cc
 * @date    18-03-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#include <utility>
#include "test/util/TransTestUtils.h"
#include "inc_gen/TypeDefs.h"
#include "input/ModeCombiOpRange.h"
#include "util/RandomNumberGenerator.h"

//==============================================================================
// NON-TEMPLATED DEFINITIONS
//==============================================================================
namespace midas::test::util
{
   /************************************************************************//**
    * Will set up a "regular" ModeCombiOpRange (based on number of modes and
    * maximum excitation level), then erase from it up to aNumErase randomly
    * picked ModeCombi%s with at least aMinLevelErase modes.
    *
    * Sets ModeCombi addresses according to arNumModals.
    *
    * @param[in] arNumModals
    *    The number of modals per mode; implicitly gives the number of modes,
    *    and is used for setting ModeCombi addresses according to excitation
    *    space.
    * @param[in] aMaxExci
    *    The maximum excitation level of the ModeCombiOpRange (can end up being
    *    lower though, depending on which ModeCombi%s are randomly erased).
    * @param[in] aNumErase
    *    Will randomly erase up to aNumErase ModeCombi%s from the constructed
    *    range.
    * @param[in] aMinLevelErase
    *    The minimum excitation level of ModeCombi%s that may be erased.
    * @return
    *    Pair of ModeCombiOpRange and number of excitation parameters
    *    corresponding to it, based on the given arNumModals.
    ***************************************************************************/
   std::pair<ModeCombiOpRange,Uin> GetMcrRandomMcsErased
      (  const std::vector<Uin>& arNumModals
      ,  Uin aMaxExci
      ,  Uin aNumErase
      ,  Uin aMinLevelErase
      )
   {
      // Standard ModeCombiOpRange.
      ModeCombiOpRange mcr(aMaxExci, arNumModals.size());

      // Set of random indices for ModeCombi%s to erase.
      Uin n_redsize = mcr.ReducedSize(mcr.GetMaxExciLevel(), aMinLevelErase);
      std::set<Uin> s;
      for(; aNumErase > 0; --aNumErase)
      {
         s.insert(midas::util::RandomInt<Uin>() % n_redsize);
      }

      // Erase from ModeCombiOpRange.
      std::vector<ModeCombi> v_remove;
      for(const auto& i: s)
      {
         v_remove.emplace_back(*(mcr.Begin(aMinLevelErase)+i));
      }
      mcr.Erase(std::move(v_remove));

      // Set addresses and return.
      Uin size = ::midas::test::util::SetAddresses(mcr, arNumModals);
      return std::make_pair(mcr, size);
   }

   /************************************************************************//**
    * @param[in,out] arMcr
    *    The ModeCombiOpRange for which to assign ModeCombi addresses.
    * @param[in] arNumModals
    *    The number of modals for each mode.
    * @return
    *    The total number of parameters for the ModeCombi%s in the range. In
    *    other words, the address that would be assigned to a ModeCombi
    *    appended to the range.
    ***************************************************************************/
   Uin SetAddresses
      (  ModeCombiOpRange& arMcr
      ,  const std::vector<Uin>& arNumModals
      )
   {
      return ::SetAddresses(arMcr, arNumModals);
   }

} /* namespace midas::test::util */


//==============================================================================
// TEMPLATE INSTANTIATIONS
//==============================================================================
#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include <complex>
#include "test/util/TransTestUtils_Impl.h"

// Define instantiation macro.
#define INSTANTIATE_TRANSTESTUTILS(T) \
   namespace midas::test::util \
   { \
      template std::vector<std::vector<T>> GetRandomParams \
         (  const ModeCombiOpRange& arMcr \
         ,  const std::vector<Uin>& arNumModals \
         ,  const bool aZeroRefParam \
         ,  const In aAddend \
         ); \
      template ModalIntegrals<T> GetRandomModalIntegrals \
         (  const OpDef& arOpDef \
         ,  const std::vector<Uin>& arNumModals \
         ,  char aSymType \
         ); \
      template void ConstructParamsVecCont \
         (  GeneralMidasVector<T>& arVecCont \
         ,  const ModeCombiOpRange& arMcr \
         ,  const Uin aTotSize \
         ,  const std::vector<std::vector<T>>& arParams \
         ); \
      template void ConstructParamsVecCont \
         (  GeneralDataCont<T>& arVecCont \
         ,  const ModeCombiOpRange& arMcr \
         ,  const Uin aTotSize \
         ,  const std::vector<std::vector<T>>& arParams \
         ); \
   } /* namespace midas::test::util */ \


#define INSTANTIATE_TRANSTESTUTILS_REALONLY(T) \
   namespace midas::test::util \
   { \
      template OpDef GetOpDefFromCoefs<T> \
         (  const ModeCombiOpRange& arMcr \
         ,  const std::vector<Uin>& arNumOpers \
         ,  const std::vector<std::vector<T>> arCoefs \
         ); \
   } /* namespace midas::test::util */ \

// Instantiations.
INSTANTIATE_TRANSTESTUTILS(Nb);
INSTANTIATE_TRANSTESTUTILS(std::complex<Nb>);
INSTANTIATE_TRANSTESTUTILS_REALONLY(Nb);

#undef INSTANTIATE_TRANSTESTUTILS
#undef INSTANTIATE_TRANSTESTUTILS_REALONLY

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
