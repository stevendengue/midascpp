#include "RangeTest.h"


namespace midas::test
{

void RangeTest()
{
   cutee::suite suite("Range test");
   
   //
   suite.add_test<StringRangeIntegerTest>("Range string integer test");
   suite.add_test<StringRangeFloatTest>("Range string float test test");
   suite.add_test<RangeConstructionTest>("Range construction test");
   suite.add_test<RangeExpandTest>("Range expand test");
   
   //
   RunSuite(suite);
}

} /* namespace midas::test */
