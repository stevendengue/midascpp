/**
 *******************************************************************************
 * 
 * @file    TypeTraitsTest.h
 * @date    13-07-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Unit tests for midas::type_traits.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TYPETRAITSTEST_H_INCLUDED
#define TYPETRAITSTEST_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "util/type_traits/Complex.h"
#include "util/type_traits/TypeName.h"
#include "test/CuteeInterface.h"

namespace midas::test::type_traits
{
   /************************************************************************//**
    * @brief
    *    Tests for complex types.
    *
    * If one of the static_asserts doesn't pass, you'll probably also see (lots
    * of) errors at compile-time in rest of the code. You could try just
    * compiling this file alone to get an idea about what is broken if the
    * other compiler errors are too obscure.
    * 
    * @note
    *    Checks at compile-time using static_assert. The UNIT_ASSERT is just a
    *    dummy, doesn't really need to be there.
    ***************************************************************************/
   struct Complex: public cutee::test
   {
      void run() override
      {
         using namespace midas::type_traits;
         static_assert(!IsComplexV<Nb>, "Expected: IsComplexV<Nb> == false.");
         static_assert(IsComplexV<std::complex<Nb>>, "Expected: IsComplexV<std::complex<Nb>> == true.");

         static_assert(std::is_same_v<RealTypeT<Nb>, Nb>, "Expected: RealType<Nb> == Nb.");
         static_assert(std::is_same_v<RealTypeT<std::complex<Nb>>, Nb>, "Expected: RealType<std::complex<Nb>> == Nb.");

         UNIT_ASSERT(true, "This should never fail, it's just to have a passed test in output.");
      }
   };

   /************************************************************************//**
    * @brief
    *    Tests for TypeName<A>(), A being a class template.
    ***************************************************************************/
   template<template<class...> class A>
   struct TypeNameTmplTest: public cutee::test
   {
      private:
         std::string mExpect;

      public:
         TypeNameTmplTest(std::string aExpect): mExpect(std::move(aExpect)) {}

         void run() override
         {
            using namespace midas::type_traits;
            UNIT_ASSERT_EQUAL(TypeName<A>(), mExpect, "TypeName<"+mExpect+">() failed.");
         }
   };

   /************************************************************************//**
    * @brief
    *    Tests for TypeName<T>(), T being a type.
    ***************************************************************************/
   template<class T>
   struct TypeNameTest: public cutee::test
   {
      private:
         std::string mExpect;

      public:
         TypeNameTest(std::string aExpect): mExpect(std::move(aExpect)) {}

         void run() override
         {
            using namespace midas::type_traits;
            UNIT_ASSERT_EQUAL(TypeName<T>(), mExpect, "TypeName<"+mExpect+">() failed.");
         }
   };

   /************************************************************************//**
    * @brief
    *    Tests for TypeName(const T&), T being a type.
    *
    * @note
    *    T must be default constructible and must be fully specified, i.e.
    *    forward decl. is not enough.
    ***************************************************************************/
   template<class T>
   struct TypeNameVarTest: public cutee::test
   {
      private:
         std::string mExpect;

      public:
         TypeNameVarTest(std::string aExpect): mExpect(std::move(aExpect)) {}

         void run() override
         {
            using namespace midas::type_traits;
            T a;
            UNIT_ASSERT_EQUAL(TypeName(a), mExpect, "TypeName(const "+mExpect+"&) failed.");
         }
   };


} /* namespace midas::test::type_traits */


#endif/*TYPETRAITSTEST_H_INCLUDED*/
