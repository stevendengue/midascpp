/**
************************************************************************
* 
* @file                SplineNDTest.cc
*
* Created:             11-10-2006
*
* Author:              Daniele Toffoli
*
* Short Description:   Test SplineND class 
* 
* Last modified: Thu Dec 03, 2009  02:05PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<string>
#include<valarray>
#include<fstream>
#include<math.h>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "pes/splines/SplineND.h"
#include "pes/fitting/PolyFit.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "util/Timer.h"
#include "util/Io.h"
#include "test/CuteeInterface.h"

namespace midas::test
{

namespace polyfitnd
{
/**
* Controls the testing of stuff 
* */
void fnd1 (const MidasVector&  arXog1, const MidasVector& arXog2, const MidasVector& arXog3, const MidasVector& arXog4, MidasVector& arYog) 
{
   In nog1 = arXog1.Size();
   In nog2 = arXog2.Size();
   In nog3 = arXog3.Size();
   In nog4 = arXog4.Size();
   MidasVector xog1_tmp = arXog1;
   MidasVector xog2_tmp = arXog2;
   MidasVector xog3_tmp = arXog3;
   MidasVector xog4_tmp = arXog4;
   xog1_tmp.Pow(I_3);
   xog2_tmp.Pow(I_3);
   xog3_tmp.Pow(I_3);
   xog4_tmp.Pow(I_3);
   In nog1234 = nog1 * nog2 * nog3 * nog4;
   if ( nog1234 != arYog.Size())
   {
     arYog.SetNewSize(nog1234,false);
   }
   In iog1234 = I_0;
   for (In iog1 =I_0; iog1 < nog1; iog1++)
   {
      for (In iog2 =I_0; iog2 < nog2; iog2++)
      { 
         for (In iog3 =I_0; iog3 < nog3; iog3++)
         {
            for (In iog4 =I_0; iog4 < nog4; iog4++)
            {
               arYog[iog1234] = xog1_tmp[iog1] + xog2_tmp[iog2] + xog3_tmp[iog3] + xog4_tmp[iog4];
               iog1234+=I_1;
            }
         }
      }
   }
}

struct PolyFitNDTestImpl
   :  public cutee::test
{
   void run() override
   {
      Mout << "****** Performing test of PolyFitND class " << endl << endl;

   // *** then defines the function to be interpolated (a normal cubic in three dimensions)

      Nb* Xig1_tmp;
      Nb* Xig2_tmp;
      Nb* Xig3_tmp;
      Nb* Xig4_tmp;
      Nb low_boundX1_ig   = -C_5;
      Nb upper_boundX1_ig = C_5;
      Nb low_boundX2_ig   = -C_5;
      Nb upper_boundX2_ig = C_5;
      Nb low_boundX3_ig   = -C_5;
      Nb upper_boundX3_ig = C_5;
      Nb low_boundX4_ig   = -C_5;
      Nb upper_boundX4_ig = C_5;
      In num_of_stepX1_ig = I_20;
      In num_of_stepX2_ig = I_20;
      In num_of_stepX3_ig = I_20;
      In num_of_stepX4_ig = I_20;
      Nb hX1_ig;
      Nb hX2_ig;
      Nb hX3_ig;
      Nb hX4_ig;

   // *** input lower and upper bounds and the # of steps
    
   //   Minp << Nb low_bound << Nb upper_bound << In num_of_steps << endl;
      Mout << "Lower bound in X1 direction in SplineND interpolation : " << low_boundX1_ig << endl << endl;
      Mout << "Upper bound in X1 direction in SplineND interpolation : " << upper_boundX1_ig << endl << endl;
      Mout << "# of steps in X1  direction in SplineND interpolation : " << num_of_stepX1_ig << endl << endl;
      Mout << "Lower bound in X2 direction in SplineND interpolation : " << low_boundX2_ig << endl << endl;
      Mout << "Upper bound in X2 direction in SplineND interpolation : " << upper_boundX2_ig << endl << endl;
      Mout << "# of steps in  X2 direction in SplineND interpolation : " << num_of_stepX2_ig << endl << endl;
      Mout << "Lower bound in X3 direction in SplineND interpolation : " << low_boundX3_ig << endl << endl;
      Mout << "Upper bound in X3 direction in SplineND interpolation : " << upper_boundX3_ig << endl << endl;
      Mout << "# of steps in  X3 direction in SplineND interpolation : " << num_of_stepX3_ig << endl << endl;
      Mout << "Upper bound in X4 direction in SplineND interpolation : " << upper_boundX3_ig << endl << endl;
      Mout << "# of steps in  X4 direction in SplineND interpolation : " << num_of_stepX3_ig << endl << endl;
               

      hX1_ig = (upper_boundX1_ig - low_boundX1_ig)/num_of_stepX1_ig;
      hX2_ig = (upper_boundX2_ig - low_boundX2_ig)/num_of_stepX2_ig;
      hX3_ig = (upper_boundX3_ig - low_boundX3_ig)/num_of_stepX3_ig;
      hX4_ig = (upper_boundX4_ig - low_boundX4_ig)/num_of_stepX4_ig;

      In nig1 = num_of_stepX1_ig + I_1;
      In nig2 = num_of_stepX2_ig + I_1;
      In nig3 = num_of_stepX3_ig + I_1;
      In nig4 = num_of_stepX4_ig + I_1;

      Xig1_tmp = new Nb [nig1];
      Xig2_tmp = new Nb [nig2];
      Xig3_tmp = new Nb [nig3];
      Xig4_tmp = new Nb [nig4];

      for (In i = I_0; i < nig1; i++)
      { 
         Xig1_tmp[i] = low_boundX1_ig + i*hX1_ig;
      }
      for (In i = I_0; i < nig2; i++)
      {
         Xig2_tmp[i] = low_boundX2_ig + i*hX2_ig;
      }
      for (In i = I_0; i < nig3; i++)
      {
         Xig3_tmp[i] = low_boundX3_ig + i*hX3_ig;
      }
      for (In i = I_0; i < nig4; i++)
      {
         Xig4_tmp[i] = low_boundX4_ig + i*hX4_ig;
      }
      
      vector<MidasVector> Xig(I_4);
      vector<MidasVector> Xog(I_4);
      MidasVector Xig1(nig1, Xig1_tmp);
      MidasVector Xig2(nig2, Xig2_tmp);
      MidasVector Xig3(nig3, Xig3_tmp);
      MidasVector Xig4(nig4, Xig4_tmp);
      Xig[I_0].SetNewSize(nig1,false);
      Xig[I_1].SetNewSize(nig2,false);
      Xig[I_2].SetNewSize(nig3,false);
      Xig[I_3].SetNewSize(nig4,false);
      
      delete[] Xig1_tmp;
      delete[] Xig2_tmp;
      delete[] Xig3_tmp;
      delete[] Xig4_tmp;

      Xig[I_0] = Xig1;
      Xig[I_1] = Xig2;
      Xig[I_2] = Xig3;
      Xig[I_3] = Xig4;

      In nig1234 = nig1 * nig2 * nig3 * nig4;
      MidasVector Yig(nig1234, C_0);
      MidasVector YigDerivs(nig1234, C_0);

      fnd1(Xig[I_0], Xig[I_1], Xig[I_2], Xig[I_3], Yig);

   // *** then construct the 3D cubic natural spline interpolant

      MidasVector FirstDerivLeft(I_4,75.);
      MidasVector FirstDerivRight(I_4,75.);

      SplineND CubicApprox(SPLINEINTTYPE::GENERAL, Xig, Yig, I_1, 75., 75.);
      CubicApprox.GetYigDerivs(YigDerivs);

   // and evaluate at few selected points

      Nb* x1_int;
      Nb* x2_int;
      Nb* x3_int;
      Nb* x4_int;
      Nb low_boundX1_og   = -C_5;
      Nb upper_boundX1_og = C_5;
      Nb low_boundX2_og   = -C_5;
      Nb upper_boundX2_og = C_5;
      Nb low_boundX3_og   = -C_5;
      Nb upper_boundX3_og = C_5;
      Nb low_boundX4_og   = -C_5;
      Nb upper_boundX4_og = C_5;
      In num_of_stepX1_og = I_30;
      In num_of_stepX2_og = I_30;
      In num_of_stepX3_og = I_30;
      In num_of_stepX4_og = I_30;
      Nb hX1_og;
      Nb hX2_og;
      Nb hX3_og;
      Nb hX4_og;

      hX1_og = (upper_boundX1_og - low_boundX1_og)/num_of_stepX1_og;
      hX2_og = (upper_boundX2_og - low_boundX2_og)/num_of_stepX2_og;
      hX3_og = (upper_boundX3_og - low_boundX3_og)/num_of_stepX3_og;
      hX4_og = (upper_boundX4_og - low_boundX4_og)/num_of_stepX4_og;

      In nog1 = num_of_stepX1_og + I_1;
      In nog2 = num_of_stepX2_og + I_1;
      In nog3 = num_of_stepX3_og + I_1;
      In nog4 = num_of_stepX4_og + I_1;
      In nog1234 = nog1 * nog2 *nog3 *nog4;
      x1_int = new Nb [nog1];
      x2_int = new Nb [nog2];
      x3_int = new Nb [nog3];
      x4_int = new Nb [nog4];

      for (In i = I_0; i < nog1; i++)
      {
         x1_int[i] = low_boundX1_og + i*hX1_og;
      }
      for (In i = I_0; i < nog2; i++)
      {     
         x2_int[i] = low_boundX2_og + i*hX2_og;
      }
      for (In i = I_0; i < nog3; i++)
      {     
         x3_int[i] = low_boundX3_og + i*hX3_og;
      }
      for (In i = I_0; i < nog4; i++)
      {     
         x4_int[i] = low_boundX4_og + i*hX4_og;
      }
      MidasVector Xog1(nog1, x1_int);
      MidasVector Xog2(nog2, x2_int);
      MidasVector Xog3(nog3, x3_int);
      MidasVector Xog4(nog4, x4_int);
      Xog[I_0].SetNewSize(nog1, false);
      Xog[I_1].SetNewSize(nog2, false);
      Xog[I_2].SetNewSize(nog3, false);
      Xog[I_3].SetNewSize(nog4, false);
      Xog[I_0] = Xog1;
      Xog[I_1] = Xog2;
      Xog[I_2] = Xog3;
      Xog[I_3] = Xog4;

      delete[] x1_int;
      delete[] x2_int;
      delete[] x3_int;
      delete[] x4_int;

      MidasVector Yog(nog1234, C_0);
      MidasVector Sig(nog1234, C_1);
      MidasVector yog_anal(nog1234, C_0);
      GetYog(Yog, Xog, Xig, Yig, YigDerivs, I_1, SPLINEINTTYPE::GENERAL, FirstDerivLeft, FirstDerivRight);

      fnd1(Xog[I_0], Xog[I_1], Xog[I_2], Xog[I_3], yog_anal);
      
      Mout.setf(ios_base::scientific, ios_base::floatfield);
      Mout.setf(ios_base::uppercase);
      midas::stream::ScopedPrecision(12, Mout);
      Mout.width(20);


      Mout << "**** results from SplineND interpolation **** " << endl << endl;

   // *** Then find out some statistics
    
      Nb Rms;
      RmsDevPerElement(Rms, Yog, yog_anal, nog1234);
      Mout << "root mean squared deviation is: " << Rms << endl << endl;


      /*Mout << " Xog1            " <<  "                 "   <<  "Xog2             " << "                    "  <<  "Xog3             " << "               " 
          <<  "Xog4             " <<  "                 " <<    "Yog              " << " yog_anal           "  << endl << endl;

      In iog1234=I_0;
      for (In k = I_0; k<nog1; k++)
      {
         for (In j = I_0; j<nog2; j++)
         {
            for (In l = I_0; l<nog3; l++)
            {
               for (In m = I_0; m<nog4; m++)
               {
                  Mout << Xog[I_0][k] << "                "  << Xog[I_1][j] <<   "                     "  << Xog[I_2][l] << "                " 
                       << Xog[I_3][m] << "                "  << Yog[iog1234] <<  "                     " <<  yog_anal[iog1234] << endl;

                  iog1234+=I_1;
               }
            }        
         }
      }
      Mout  << endl << endl;*/

      Mout << "********** end of SplineNDTest *****************" << endl;
      Mout << "********** Start ND linear least squares fitting *****************" << endl;

   //    *** set up vector for polynomial exponents
      
      vector<In> func1(I_4);
      func1[I_0] = I_3;
      func1[I_1] = I_0;
      func1[I_2] = I_0;
      func1[I_3] = I_0;
      vector<In> func2(I_4);
      func2[I_0] = I_0;
      func2[I_1] = I_3;
      func2[I_2] = I_0;
      func2[I_3] = I_0;
      vector<In> func3(I_4);
      func3[I_0] = I_0;
      func3[I_1] = I_0;
      func3[I_2] = I_3;
      func3[I_3] = I_0;
      vector<In> func4(I_4);
      func4[I_0] = I_0;
      func4[I_1] = I_0;
      func4[I_2] = I_0;
      func4[I_3] = I_3;
      vector<In> func5(I_4);
      func5[I_0] = I_0;
      func5[I_1] = I_3;
      func5[I_2] = I_0;
      func5[I_3] = I_3;
      vector<InVector> arNvecVec(I_5);
      arNvecVec[I_0] = func1;
      arNvecVec[I_1] = func2;
      arNvecVec[I_2] = func3;
      arNvecVec[I_3] = func4;
      arNvecVec[I_4] = func5;
      
      MidasVector YogFit(nog1234, C_0);
      /*Mout << "**** arNvecVec vector is : " << endl << endl;
      for (In i=I_0; i<arNvecVec.size(); i++)
      {
         Mout << "*** element " << i << " is " << endl << endl;
         for (In j = I_0; j<arNvecVec[i].size(); j++)
         {
            Mout << " arNvecVec[ " << i << " ].func[ " << j << " ] = " << arNvecVec[i][j] << endl << endl;
         }
      }*/
      // *** then make appropriate calls
      enum{SVD,NORMALEQ};
      /*PolyFit CubicFit(arNvecVec.size(), SVD);
      CubicFit.MakeFit(Yog, Xog, Sig, Xig, arNvecVec);
      CubicFit.PlotFit(YogFit, Xog, arNvecVec);
      
      // *** then print out some statistics
      
      Mout << CubicFit;*/

      /*Mout << "  ******* fitting results  *********** " << endl << endl;
      Mout << " Yog               " <<  "                 "   <<  " YogFit                 " << endl << endl;
      for (In iog=I_0; iog<nog1234; iog++)
      {
         Mout << Yog[iog]    <<   "                "  <<  YogFit[iog] <<  "  " << endl << endl;
     
      }*/
   }
};
} /* namespace polyfitnd */


void PolyFitNDTest()
{
   // Create test_suite object.
   cutee::suite suite("PolyFitND test");

   // Add all the tests to the suite.
   using namespace polyfitnd;
   suite.add_test<PolyFitNDTestImpl> ("PolyFitND test");

   // Run the tests.
   RunSuite(suite);
}



} /* namespace midas::test */
