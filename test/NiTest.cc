/**
************************************************************************
* 
* @file                NiTest.cc
*
* Created:             17-08-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Test driver for numerical integration classes and routines.
* 
* Last modified: Fri Oct 27, 2006  11:17AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard Headers.
#include<iomanip>
using std::setw; 

// Link to Standard Headers 
#include "inc_gen/math_link.h"

// My Headers.
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "ni/NumInt1D.h"
#include "ni/Ode1D.h"
#include "ni/NonLinEq1D.h"
using NonLinEq1D::Bisect;
using NonLinEq1D::Newton;
#include "util/Io.h"
#include "test/CuteeInterface.h"

namespace midas::test
{

namespace ni::detail
{
inline Nb square(Nb d){ return d*d;}
inline Nb gauss(Nb d){ return exp(-d*d);}
inline Nb sinxox(Nb d){ return sin(d)/d;} 
inline Nb fex(Nb t,Nb x){return x*(C_1-exp(t))/(C_1+exp(t));} // sin x over x
inline Nb exact(Nb t) {return (C_12*exp(t))/(pow(C_1+exp(t),C_2));}
inline Nb xinvmtanx(Nb x) {return (C_1/x-tan(x));}
inline Nb xmexpmx(Nb x) {return (x-exp(-x));}
inline Nb xmexpmxd(Nb x) {return (C_1+exp(-x));}
const In lambda = -I_10_2;
inline Nb gex(Nb t,Nb x){return x*lambda;} 
inline Nb exactg(Nb t) {return exp(lambda*t);}
inline Nb mysin(Nb t) {return sin(t);}
inline Nb exLi(Nb t,Nb x);
} /* namespace ni::detail */


namespace ni
{
/***************************************************************************//**
 * @brief
 *    Controls the testing of numerical integration classes and routines.
 ******************************************************************************/
struct NiTestImpl
   :  public cutee::test
{
   void run() override
   {
      using namespace ni::detail;
      Mout << " In TestNi: Performing test of numerical integration features in Midas" << endl;

      bool integrate_test = true;
      if (integrate_test)              // n could be different, but else check is consistent.
      {
         Mout << " LETS INTEGRATE" << endl << endl;
         Nb a=C_0;
         Nb b=C_2;
         
         midas::stream::ScopedPrecision(20, Mout);
         Mout << " NumInt1D over square " << endl;
         for (In n=I_10_2;n<=I_10_3;n+=I_300)
         {
            NumInt1D dat(a,b,n,square);
            Mout << " Integral from " << dat.lowbd() << " to " << dat.upbd();
            Mout << " is = " << setw(I_20) << dat.Simpson() 
               << " using simpson with n = " << setw(I_5) << n << endl;
            Mout << " Integral from " << dat.lowbd() << " to " << dat.upbd();
            Mout << " is = " << setw(I_20) << dat.Trapez() 
               << " using trapez  with n = " << setw(I_5) << n << endl;
         }
         Mout << " Exact int. over square:   " << setw(I_20) 
            << (pow(b,C_3)-pow(a,C_3))/I_3 << endl;

         Mout << " NumInt1D over sinus " << endl;
         for (In n=I_10_2;n<=I_10_3;n+=I_300)
         {
            NumInt1D di(a,b,n,mysin);
            Mout << " Integral from " << di.lowbd() << " to " << di.upbd();
            Mout << " is = " << setw(I_20) << di.Simpson() 
               << " using simpson with n = " << setw(I_5) << n << endl;
            Mout << " Integral from " << di.lowbd() << " to " << di.upbd();
            Mout << " is = " << setw(I_20) << di.Trapez() 
               <<  " using trapez  with n = " << setw(I_5) << n << endl;
         }
         Mout <<    " Exact int. over sinus:    " << setw(I_20) 
            << (-cos(b)+cos(a)) << endl;

      
         Mout << " NumInt1D over gaussian " << endl;
         for (In n=I_10_2;n<=I_10_3;n+=I_300)
         {
            NumInt1D dat(a,b,n,gauss);
            Mout << " Integral from " << dat.lowbd() << " to " << dat.upbd();
            Mout << " is = " << setw(I_20) << dat.Simpson() 
               << " using simpson with n = " << setw(I_5) << n << endl;
            Mout << " Integral from " << dat.lowbd() << " to " << dat.upbd();
            Mout << " is = " << setw(I_20) << dat.Trapez() 
               << " using trapez  with n = " << setw(I_5) << n << endl;
         }
         Mout << " Reference given by Yang:  " << setw(I_20) 
            << 0.8820813907 << " (10 digits only)" << endl;
          
         a = C_1;
         b = C_2;
         Mout << " NumInt1D over sinx/x " << endl;
         for (In n=I_100;n<=I_10_3;n+=I_300)
         {
            NumInt1D dat(a,b,n,sinxox);
            Mout << " Integral from " << dat.lowbd() << " to " << dat.upbd();
            Mout << " is = " << setw(I_20) << dat.Simpson() 
               << " using simpson with n = " << setw(I_5) << n << endl;
            Mout << " Integral from " << dat.lowbd() << " to " << dat.upbd();
            Mout << " is = " << setw(I_20) << dat.Trapez() 
               << " using trapez  with n = " << setw(I_5) << n << endl;
         }


         Mout << " Checking pointer calls to some integrals " << endl;
         a = C_0;
         b = C_2;
         NumInt1D di(a,b,I_100,square);
         NumInt1D dat(a,b,I_100,gauss);
         di.TestPointer(&di,dat);

      }

      bool RootSearchTest = true;
      if (RootSearchTest) 
      {
         Nb a;
         Nb b;
         Nb delta = C_I_10_4;
         Nb epsn  = C_I_10_6;
         In MaxIt = I_100;
         In iter  = I_0;
         Nb xz;
         midas::stream::ScopedPrecision(10, Mout);
         a = C_I_2;
         b = C_2;
         if (Bisect(a,b,xz,xmexpmx,delta,epsn,MaxIt,iter,false))
         {
            Mout << " Bisect algorithm converged: " << endl;
         } 
         else
         {
            Mout << " Bisect algorithm dit not converge a solution: " << endl;
         }
         Mout << " Bisect1 - best root:" << setw(I_10) << xz 
            << " with f value " << setw(I_10) << xmexpmx(xz)
            << " in " << iter << " iterations (OR) " << endl;
         if (Bisect(a,b,xz,xmexpmx,delta,epsn,MaxIt,iter,true))
         {
            Mout << " Bisect algorithm converged: " << endl;
         } 
         else
         {
            Mout << " Bisect algorithm dit not converge a solution: " << endl;
         }
         Mout << " Bisect2 - root is found at :" << setw(I_10) << xz 
            << " with f value " << setw(I_10) << xmexpmx(xz) 
            << " in " << iter << " iterations (AND) " << endl;
         xz = C_200;
         if (Newton(xz,xmexpmx,xmexpmxd,delta,epsn,MaxIt,iter,false))
         {
            Mout << " Newton algorithm converged: " << endl;
         } 
         else
         {
            Mout << " Newton algorithm dit not converge a solution: " << endl;
         }
         Mout << " Newton1- best root is at :" << setw(I_10) << xz 
              << " with f value " << setw(I_10) << xmexpmx(xz) 
              << " in " << iter << " iterations (OR) " << endl;
         xz = C_200;
         if (Newton(xz,xmexpmx,xmexpmxd,delta,epsn,MaxIt,iter,true))
         {
            Mout << " Newton algorithm converged: " << endl;
         } 
         else
         {
            Mout << " Newton algorithm dit not converge a solution: " << endl;
         }
         Mout << " Newton2- root is found at :" << setw(I_10) << xz 
            << " with f value " << setw(I_10) << xmexpmx(xz) 
            << " in " << iter << " iterations (AND) " << endl;
         Mout << " Notice that the AND option is safer in the sense that it will not artificially return"
            << " something close to the one endpoint" << endl;
      }

      bool OdeTest = true;
      if (OdeTest) 
      {
         {
            Mout << " Testing numerical solution of ordinate differentical equations (Ode) " << endl;
            Nb t0   = C_0;
            Nb x0   = C_3;
            Nb tend = C_2;
            Ode1D test(t0,x0,tend,fex);
      
            In NStep  = I_100;
            Nb h = (tend-t0)/NStep;
            Nb* xEuler    = test.Euler(NStep);
            Nb* xEulerImp = test.EulerImp(NStep);
            Nb* xEulerPc  = test.EulerPc(NStep);
            Nb* xRk2      = test.Rk2(NStep);
            Nb* xRk4      = test.Rk4(NStep);
            Nb MeEuler    = C_0;
            Nb MeEulerImp = C_0;
            Nb MeEulerPc  = C_0;
            Nb MeRk2      = C_0;
            Nb MeRk4      = C_0;
            for (In i=I_0;i<=NStep;i++) 
            {
               Nb t = t0 + i*h;
               Nb ex       = exact(t);
               Nb EEuler   = (xEuler[i]-ex)/ex;
               Nb EEulerImp= (xEulerImp[i]-ex)/ex;
               Nb EEulerPc = (xEulerPc[i]-ex)/ex;
               Nb ERk2     = (xRk2[i]-ex)/ex;
               Nb ERk4     = (xRk4[i]-ex)/ex;
               MeEuler    = max(MeEuler,fabs(EEuler));
               MeEulerImp = max(MeEulerImp,fabs(EEulerImp));
               MeEulerPc  = max(MeEulerPc,fabs(EEulerPc));
               MeRk2      = max(MeRk2,fabs(ERk2));
               MeRk4      = max(MeRk4,fabs(ERk4));
            }
            Mout << " Maximum error by Euler:   " << MeEuler << endl;
            Mout << " Maximum error by EulerImp:" << MeEulerImp << endl;
            Mout << " Maximum error by EulerPc: " << MeEulerPc << endl;
            Mout << " Maximum error by Rk2:     " << MeRk2   << endl;
            Mout << " Maximum error by Rk4:     " << MeRk4   << endl;
            delete[] xEuler;// the lack of encapsulation forces us to manually delete these
            delete[] xEulerImp;// the lack of encapsulation forces us to manually delete these
            delete[] xEulerPc;// the lack of encapsulation forces us to manually delete these
            delete[] xRk2;// the lack of encapsulation forces us to manually delete these
            delete[] xRk4;// the lack of encapsulation forces us to manually delete these
         }
         {
            Mout << " Testing dx/dt = lambda*x  " << endl;
            Nb t0   = C_0;
            Nb x0   = C_1;
            Nb tend = C_2;
            Ode1D test(t0,x0,tend,gex);
      
            In NStep  = I_400;
            Nb h = (tend-t0)/NStep;
            Nb* xEuler    = test.Euler(NStep);
            Nb* xEulerImp = test.EulerImp(NStep);
            Nb* xEulerPc  = test.EulerPc(NStep);
            Nb* xRk2      = test.Rk2(NStep);
            Nb* xRk4      = test.Rk4(NStep);
            Nb MeEuler    = C_0;
            Nb MeEulerImp = C_0;
            Nb MeEulerPc  = C_0;
            Nb MeRk2      = C_0;
            Nb MeRk4      = C_0;
            for (In i=I_0;i<=NStep;i++) 
            {
               Nb t = t0 + i*h;
               Nb ex       = exactg(t);
               Nb EEuler   = xEuler[i]-ex;
               Nb EEulerImp= xEulerImp[i]-ex;
               Nb EEulerPc = xEulerPc[i]-ex;
               Nb ERk2     = xRk2[i]-ex;
               Nb ERk4     = xRk4[i]-ex;
               midas::stream::ScopedPrecision(10, Mout);
               MeEuler    = max(MeEuler,fabs(EEuler));
               MeEulerImp = max(MeEulerImp,fabs(EEulerImp));
               MeEulerPc  = max(MeEulerPc,fabs(EEulerPc));
               MeRk2      = max(MeRk2,fabs(ERk2));
               MeRk4      = max(MeRk4,fabs(ERk4));
            }
            Mout << " Maximum relative error by Euler:   " << MeEuler << endl;
            Mout << " Maximum relative error by EulerImp:" << MeEulerImp << endl;
            Mout << " Maximum relative error by EulerPc: " << MeEulerPc << endl;
            Mout << " Maximum relative error by Rk2:     " << MeRk2   << endl;
            Mout << " Maximum relative error by Rk4:     " << MeRk4   << endl;
            delete[] xEuler;// the lack of encapsulation forces us to manually delete these
            delete[] xEulerImp;// the lack of encapsulation forces us to manually delete these
            delete[] xEulerPc;// the lack of encapsulation forces us to manually delete these
            delete[] xRk2;// the lack of encapsulation forces us to manually delete these
            delete[] xRk4;// the lack of encapsulation forces us to manually delete these
         }
         {
            Mout << " Testing Ms. Li " << endl;
            Nb t0   = C_0;
            Nb x0   = C_10_4;
            Nb tend = C_50;
            Ode1D test(t0,x0,tend,exLi);
      
            In NStep  = I_10_5;
            Nb h = (tend-t0)/NStep;
            Nb* xEuler    = test.Euler(NStep);
            Nb* xEulerImp = test.EulerImp(NStep);
            Nb* xEulerPc  = test.EulerPc(NStep);
            Nb* xRk2      = test.Rk2(NStep);
            Nb* xRk4      = test.Rk4(NStep);
            bool DoEuler  = true;
            bool DoEulerI = true;
            bool DoEulerPc= true;
            bool DoRk2    = true;
            bool DoRk4    = true;
            for (In i=I_0;i<=NStep;i++) 
            {
               Nb t = t0 + i*h;
               Nb ex       = C_0;
               Nb EEuler   = xEuler[i]-ex;
               Nb EEulerImp= xEulerImp[i]-ex;
               Nb EEulerPc = xEulerPc[i]-ex;
               Nb ERk2     = xRk2[i]-ex;
               Nb ERk4     = xRk4[i]-ex;
               midas::stream::ScopedPrecision(10, Mout); 
               if (DoEuler && EEuler<C_0) 
               {
                  Mout << " Euler    zero between " << setw(I_10) << t-h << " and " << setw(I_10) << t << endl;
                  DoEuler = false;
               }
               if (DoEulerI && EEulerImp<C_0) 
               {
                  Mout << " EulerImp zero between " << setw(I_10) << t-h << " and " << setw(I_10) << t << endl;
                  DoEulerI = false;
               }
               if (DoEulerPc && EEulerPc<C_0) 
               {
                  Mout << " EulerPC  zero between " << setw(I_10) << t-h << " and " << setw(I_10) << t << endl;
                  DoEulerPc = false;
               }
               if (DoRk2 && ERk2<C_0) 
               {
                  Mout << " RK2      zero between " << setw(I_10) << t-h << " and " << setw(I_10) << t << endl;
                  DoRk2 = false;
               }
               if (DoRk4 && ERk4<C_0) 
               {
                  Mout << " RK4      zero between " << setw(I_10) << t-h << " and " << setw(I_10) << t << endl;
                  DoRk4 = false;
               }
            }
            Mout << " Reference value: 30.004(8)" << endl;
            delete[] xEuler;// the lack of encapsulation forces us to manually delete these
            delete[] xEulerImp;// the lack of encapsulation forces us to manually delete these
            delete[] xEulerPc;// the lack of encapsulation forces us to manually delete these
            delete[] xRk2;// the lack of encapsulation forces us to manually delete these
            delete[] xRk4;// the lack of encapsulation forces us to manually delete these
         }
      }
   }
};
} /* namespace ni */

Nb ni::detail::exLi(Nb t,Nb x)
{
   if (t < C_10) 
   {
      return x/C_20;
   } else
   {
       //return x/C_20-C_10_3; // Version 1
       return x/C_20-C_10_3*exp(C_3/C_100*(t-C_10)); // Version 2
   }
}


void NiTest()
{
   // Create test_suite object.
   cutee::suite suite("NI test");

   // Add all the tests to the suite.
   using namespace ni;
   suite.add_test<NiTestImpl> ("NI test");

   // Run the tests.
   RunSuite(suite);
}


} /* namespace midas::test */
