#include "test/TaylorTest.h"

namespace midas::test
{

void TaylorTest()
{
   cutee::suite suite("Taylor test");
   
   // add tests
   suite.add_test<SimpleTaylorTestCase>("Simple Taylor Test");
   suite.add_test<TaylorTestCase2D>("2D Taylor Test");
   suite.add_test<CrazyTaylorTestCase>("Crazy Taylor Test");
   
   // run tests
   RunSuite(suite);
}

} /* namespace midas::test */
