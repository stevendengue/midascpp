#include "test/GenericFunctionTests.h"

namespace midas::test
{

void GenericFunctionTests()
{
   cutee::suite suite("GenericFunction test");
   
   // =================== Test of pow ================
   suite.add_test<RaiseTest< 0,double> >("RaiseTest  0  double");
   suite.add_test<RaiseTest< 1,double> >("RaiseTest  1  double");
   suite.add_test<RaiseTest< 2,double> >("RaiseTest  2  double");
   suite.add_test<RaiseTest< 3,double> >("RaiseTest  3  double");
   suite.add_test<RaiseTest< 4,double> >("RaiseTest  4  double");
   suite.add_test<RaiseTest< 5,double> >("RaiseTest  5  double");
   suite.add_test<RaiseTest< 6,double> >("RaiseTest  6  double");
   suite.add_test<RaiseTest< 7,double> >("RaiseTest  7  double");
   suite.add_test<RaiseTest< 8,double> >("RaiseTest  8  double");
   suite.add_test<RaiseTest< 9,double> >("RaiseTest  9  double");
   suite.add_test<RaiseTest<10,double> >("RaiseTest 10  double");
   
   //suite.add_test<RaiseTest< 0,float> >("RaiseTest  0  float");
   //suite.add_test<RaiseTest< 1,float> >("RaiseTest  1  float");
   //suite.add_test<RaiseTest< 2,float> >("RaiseTest  2  float");
   //suite.add_test<RaiseTest< 3,float> >("RaiseTest  3  float");
   //suite.add_test<RaiseTest< 4,float> >("RaiseTest  4  float");
   //suite.add_test<RaiseTest< 5,float> >("RaiseTest  5  float");
   //suite.add_test<RaiseTest< 6,float> >("RaiseTest  6  float");
   //suite.add_test<RaiseTest< 7,float> >("RaiseTest  7  float");
   //suite.add_test<RaiseTest< 8,float> >("RaiseTest  8  float");
   //suite.add_test<RaiseTest< 9,float> >("RaiseTest  9  float");
   //suite.add_test<RaiseTest<10,float> >("RaiseTest 10  float");
  
   // ================= unary functions ===============
   suite.add_test<CosTest <double> >("CosTest  double");   
   suite.add_test<SinTest <double> >("SinTest  double");   
   suite.add_test<TanTest <double> >("TanTest  double");   
   suite.add_test<ExpTest <double> >("ExpTest  double");   
   suite.add_test<SqrtTest<double> >("SqrtTest  double");   
   
   //suite.add_test<CosTest <float> >("CosTest  float");   
   //suite.add_test<SinTest <float> >("SinTest  float");   
   //suite.add_test<TanTest <float> >("TanTest  float");   
   //suite.add_test<ExpTest <float> >("ExpTest  float");   
   //suite.add_test<SqrtTest<float> >("SqrtTest  float");   
   
   // ================= binary operators =============
   suite.add_test<PlusTest  <double> >("PlusTest  double");   
   suite.add_test<MinusTest <double> >("MinusTest  double");   
   suite.add_test<ProdTest  <double> >("ProdTest  double");   
   suite.add_test<DivideTest<double> >("DivideTest  double");   
   
   //suite.add_test<PlusTest  <float> >("PlusTest  float");   
   //suite.add_test<MinusTest <float> >("MinusTest  float");   
   //suite.add_test<ProdTest  <float> >("ProdTest  float");   
   //suite.add_test<DivideTest<float> >("DivideTest  float");   
   
   // ================= unary operators ==============
   //suite.add_test<UnaryPlusTest  <double> >("UnaryPlusTest  double");   
   suite.add_test<UnaryMinusTest <double> >("UnaryMinusTest  double");   

   //suite.add_test<UnaryPlusTest  <float> >("UnaryPlusTest  float");   
   suite.add_test<UnaryMinusTest <float> >("UnaryMinusTest  float");   

   // ================= the rest =====================
   suite.add_test<TestGenericFunctions>("Generic Function Test");
   suite.add_test<TestMidasFunctions>("Midas Function Test");
   suite.add_test<TestGenericFunctionsStrongPrec>("Generic Function Test, Large Prec");
   
   
   // ======= Test shunting yard algorithm ===========
   suite.add_test<FunctionContainerShuntingYardTest>("Shunting Yard with a FunctionContainer test");
   suite.add_test<ShuntingYardTest>                 ("Function factory test");

   RunSuite(suite);
}
} /* namespace midas::test */
