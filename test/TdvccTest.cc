/**
 *******************************************************************************
 * 
 * @file    TdvccTest.cc
 * @date    15-04-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/TdvccTest.h"
#include "util/type_traits/TypeName.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"

namespace midas::test
{
   namespace tdvcc::detail
   {
      template
         <  typename T
         ,  template<typename> class CONT_T
         >
      void AddParamsTimTdvccImpl
         (  cutee::suite& arSuite
         )
      {
         using midas::type_traits::TypeName;
         std::string s = "ParamsTimTdvcc<"+TypeName<T>()+","+TypeName<CONT_T>()+">; ";

         using namespace tdvcc::params;
         arSuite.add_test<ParamsTimTdvccConstructorEmpty<T,CONT_T>>(s+"Constructor, empty");
         arSuite.add_test<ParamsTimTdvccConstructorZero<T,CONT_T>>(s+"Constructor, zero");
         arSuite.add_test<ParamsTimTdvccConstructorCustom<T,CONT_T>>(s+"Constructor, custom");
         arSuite.add_test<ParamsTimTdvcc_Scale<T,CONT_T>>(s+"Scale");
         arSuite.add_test<ParamsTimTdvcc_Zero<T,CONT_T>>(s+"Zero");
         arSuite.add_test<ParamsTimTdvcc_Axpy<T,CONT_T>>(s+"Axpy");
         arSuite.add_test<ParamsTimTdvcc_SetShape<T,CONT_T>>(s+"SetShape");
         arSuite.add_test<ParamsTimTdvcc_Norm<T,CONT_T>>(s+"Norm");
         arSuite.add_test<ParamsTimTdvcc_Size<T,CONT_T>>(s+"Size");
         arSuite.add_test<ParamsTimTdvcc_OdeMeanNorm2<T,CONT_T>>(s+"OdeMeanNorm2");
         arSuite.add_test<ParamsTimTdvcc_OdeMaxNorm2<T,CONT_T>>(s+"OdeMaxNorm2");
         arSuite.add_test<ParamsTimTdvcc_DataToPointer<T,CONT_T>>(s+"DataToPointer");
         arSuite.add_test<ParamsTimTdvcc_DataFromPointer<T,CONT_T>>(s+"DataFromPointer");
         arSuite.add_test<ParamsTimTdvcc_DiffNorm2<T,CONT_T>>(s+"DiffNorm2");
      }

      template
         <  typename PARAM_T
         ,  template<typename> class CONT_TMPL
         ,  template<typename, template<typename> class> class PARAM_CONT_TMPL
         ,  template<typename> class TRF_TMPL
         ,  template<typename, template<typename> class, template<typename> class, bool> class DERIV_TMPL
         ,  bool IMAG_TIME = false
         >
      void AddDerivativeTestsImpl
         (  cutee::suite& arSuite
         ,  const std::string& arStrBase
         )
      {
         //using param_t = PARAM_T;
         //using trf_t = TRF_TMPL<PARAM_T>;
         //using deriv_t = DERIV_TMPL<PARAM_T, trf_t::template cont_tmpl, TRF_TMPL, IMAG_TIME>;

         using midas::type_traits::TypeName;
         const std::string s = " ("+TypeName<PARAM_T>()+", "+(IMAG_TIME? "imag t": "real t")+")";

         using namespace tdvcc::params;
         using namespace tdvcc::derivative;
         // Add for
         //    -  ParamsTimTdvci<T,GeneralMidasVector>, DerivTimTdvciMatRep
         //    -  ParamsTimTdvci<T,GeneralDataCont>,    DerivTimTdvci2
         //    -  ParamsTimTdvcc<T,GeneralMidasVector>, DerivTimTdvccMatRep
         //    -  ParamsTimTdvcc<T,GeneralDataCont>,    DerivTimTdvcc2

         std::vector<Uin> n_modals = {3,2,3};
         std::vector<Uin> n_opers  = {4,2,3};
         Uin max_exci = 2;
         Uin max_coup = 2;

         auto info = [](const std::vector<Uin>& nm, const std::vector<Uin>& no, Uin maxexci, Uin maxcoup) -> std::string
            {
               std::stringstream ss;
               ss << ", Nm = " << nm
                  << ", Om = " << no
                  << ", VCC[" << maxexci << "]/H" << maxcoup
                  ;
               return ss.str();
            };

         std::string s_info = info(n_modals, n_opers, max_exci, max_coup);
         arSuite.add_test
            <  DerivativeTest
               <  PARAM_T
               ,  CONT_TMPL
               ,  PARAM_CONT_TMPL
               //,  trf_t::template cont_tmpl
               //,  deriv_t::template param_cont_tmpl
               ,  TRF_TMPL
               ,  DERIV_TMPL
               ,  IMAG_TIME
               >
            >("DerivativeTest; "+arStrBase+s+s_info, n_modals, n_opers, max_exci, max_coup);
      }

      template
         <  typename PARAM_T
         ,  bool IMAG_TIME
         ,  template
               <  typename
               ,  template<typename> class
               ,  template<typename> class
               ,  template<typename, template<typename> class, template<typename> class, bool> class
               ,  bool
               >
               class TEST
         ,  class... Args
         >
      void AddTimTdvccTestImpl
         (  cutee::suite& arSuite
         ,  const std::string& arStr
         ,  Args&&... args
         )
      {
         arSuite.add_test
            <  TEST<PARAM_T, GeneralMidasVector, TrfTimTdvccMatRep, DerivTimTdvcc, IMAG_TIME>
            >(arStr+", VCC MatRep", std::forward<Args>(args)...);
         arSuite.add_test
            <  TEST<PARAM_T, GeneralDataCont, TrfTimTdvcc2, DerivTimTdvcc, IMAG_TIME>
            >(arStr+", VCC[2]/H2", std::forward<Args>(args)...);
         arSuite.add_test
            <  TEST<PARAM_T, GeneralMidasVector, TrfTimTdvciMatRep, DerivTimTdvci, IMAG_TIME>
            >(arStr+", VCI MatRep", std::forward<Args>(args)...);
         arSuite.add_test
            <  TEST<PARAM_T, GeneralDataCont, TrfTimTdvci2, DerivTimTdvci, IMAG_TIME>
            >(arStr+", VCI[2]/H2", std::forward<Args>(args)...);
      }

      template
         <  typename PARAM_T
         ,  bool IMAG_TIME
         >
      void AddTimTdvccTestsImpl
         (  cutee::suite& arSuite
         )
      {
         using namespace tdvcc::timtdvcc;
         //using namespace tdvcc::params;
         //using namespace tdvcc::derivative;
         // Add for
         //    -  ParamsTimTdvci<T,GeneralMidasVector>, DerivTimTdvciMatRep
         //    -  ParamsTimTdvci<T,GeneralDataCont>,    DerivTimTdvci2
         //    -  ParamsTimTdvcc<T,GeneralMidasVector>, DerivTimTdvccMatRep
         //    -  ParamsTimTdvcc<T,GeneralDataCont>,    DerivTimTdvcc2

         using midas::type_traits::TypeName;
         const std::string s = "TimTdvcc ("+TypeName<PARAM_T>()+", "+(IMAG_TIME? "imag t": "real t")+"); ";

         std::vector<Uin> n_modals = {3,2,3};
         std::vector<Uin> n_opers  = {4,2,3};
         Uin max_exci = 2;
         Uin max_coup = 2;

         AddTimTdvccTestImpl<PARAM_T,IMAG_TIME,InitialTest>(arSuite, s+"InitialTest", n_modals, n_opers, max_exci, max_coup);
         AddTimTdvccTestImpl<PARAM_T,IMAG_TIME,TdOperTest>(arSuite, s+"TdOperTest", n_modals, n_opers, max_exci, max_coup);
         AddTimTdvccTestImpl<PARAM_T,IMAG_TIME,ExpValTest>(arSuite, s+"ExpValTest", n_modals, n_opers, max_exci, max_coup);
         AddTimTdvccTestImpl<PARAM_T,IMAG_TIME,WaterPotHExpValTest>(arSuite, s+"WaterPotHExpValTest");
      }

   } /* namespace tdvcc::detail */


   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void TdvccTest()
   {
      // Create test_suite object.
      cutee::suite suite("Tdvcc test");

      // Add all the tests to the suite.
      using namespace tdvcc;
      tdvcc::detail::AddParamsTimTdvccImpl<Nb,GeneralMidasVector>(suite);
      tdvcc::detail::AddParamsTimTdvccImpl<Nb,GeneralDataCont>(suite);
      tdvcc::detail::AddParamsTimTdvccImpl<std::complex<Nb>,GeneralMidasVector>(suite);
      tdvcc::detail::AddParamsTimTdvccImpl<std::complex<Nb>,GeneralDataCont>(suite);

      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralMidasVector,ParamsTimTdvcc,TrfTimTdvccMatRep,DerivTimTdvcc,false>(suite, "TimTdvcc, MatRep");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralMidasVector,ParamsTimTdvcc,TrfTimTdvccMatRep,DerivTimTdvcc,true>(suite, "TimTdvcc, MatRep");
      tdvcc::detail::AddDerivativeTestsImpl<Nb,GeneralMidasVector,ParamsTimTdvcc,TrfTimTdvccMatRep,DerivTimTdvcc,true>(suite, "TimTdvcc, MatRep");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralDataCont,ParamsTimTdvcc,TrfTimTdvcc2,DerivTimTdvcc,false>(suite, "TimTdvcc, VCC[2]/H2");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralDataCont,ParamsTimTdvcc,TrfTimTdvcc2,DerivTimTdvcc,true>(suite, "TimTdvcc, VCC[2]/H2");
      tdvcc::detail::AddDerivativeTestsImpl<Nb,GeneralDataCont,ParamsTimTdvcc,TrfTimTdvcc2,DerivTimTdvcc,true>(suite, "TimTdvcc, VCC[2]/H2");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralMidasVector,ParamsTimTdvci,TrfTimTdvciMatRep,DerivTimTdvci,false>(suite, "TimTdvci, MatRep");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralMidasVector,ParamsTimTdvci,TrfTimTdvciMatRep,DerivTimTdvci,true>(suite, "TimTdvci, MatRep");
      tdvcc::detail::AddDerivativeTestsImpl<Nb,GeneralMidasVector,ParamsTimTdvci,TrfTimTdvciMatRep,DerivTimTdvci,true>(suite, "TimTdvci, MatRep");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralDataCont,ParamsTimTdvci,TrfTimTdvci2,DerivTimTdvci,false>(suite, "TimTdvci, VCI[2]/H2");
      tdvcc::detail::AddDerivativeTestsImpl<std::complex<Nb>,GeneralDataCont,ParamsTimTdvci,TrfTimTdvci2,DerivTimTdvci,true>(suite, "TimTdvci, VCI[2]/H2");
      tdvcc::detail::AddDerivativeTestsImpl<Nb,GeneralDataCont,ParamsTimTdvci,TrfTimTdvci2,DerivTimTdvci,true>(suite, "TimTdvci, VCI[2]/H2");

      tdvcc::detail::AddTimTdvccTestsImpl<std::complex<Nb>,false>(suite);
      tdvcc::detail::AddTimTdvccTestsImpl<std::complex<Nb>,true>(suite);
      tdvcc::detail::AddTimTdvccTestsImpl<Nb,true>(suite);

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
