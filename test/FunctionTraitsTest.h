/**
 *******************************************************************************
 * 
 * @file    FunctionTraitsTest.h
 * @date    11-12-2019
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef FUNCTIONTRAITSTEST_H_INCLUDED
#define FUNCTIONTRAITSTEST_H_INCLUDED

#include "test/CuteeInterface.h"
#include "util/type_traits/Function.h"

namespace midas::test::function_traits
{

namespace detail
{
double f0() { return 0.0; }
double f1(double a0) { return a0; }
double f2(double a0, int a1) { return a1*a0; }

struct A
{
   private:
      int mInt = 42;

   public:
      double f0() { return 0.0; }
      double f1(double a0) { return a0; }
      double f2(double a0, int a1) { return a1*a0; }

      const int& GetInt() const { return mInt; }
};

}

/**
 *
 **/
struct StandardFunctionTraitsTest
   :  public cutee::test
{
   void run
      (
      )  override
   {
      constexpr size_t argc_0 = midas::type_traits::ArgumentCount<decltype(detail::f0)>;
      static_assert(argc_0 == 0, "Argument count for 0 args failed.");
      constexpr size_t argc_1 = midas::type_traits::ArgumentCount<decltype(detail::f1)>;
      static_assert(argc_1 == 1, "Argument count for 1 args failed.");
      constexpr size_t argc_2 = midas::type_traits::ArgumentCount<decltype(detail::f2)>;
      static_assert(argc_2 == 2, "Argument count for 2 args failed.");

      constexpr bool ret_0 = std::is_same_v<double, midas::type_traits::ReturnType<decltype(detail::f0)>>;
      static_assert(ret_0, "Wrong deduction of return type.");
      constexpr bool ret_1 = std::is_same_v<double, midas::type_traits::ReturnType<decltype(detail::f1)>>;
      static_assert(ret_1, "Wrong deduction of return type.");
      constexpr bool ret_2 = std::is_same_v<double, midas::type_traits::ReturnType<decltype(detail::f2)>>;
      static_assert(ret_2, "Wrong deduction of return type.");

      constexpr bool arg_1 = std::is_same_v<double, midas::type_traits::ArgumentType<decltype(detail::f1), 0>>;
      static_assert(arg_1, "Wrong deduction of argument type.");
      constexpr bool arg_2_0 = std::is_same_v<double, midas::type_traits::ArgumentType<decltype(detail::f2), 0>>;
      static_assert(arg_2_0, "Wrong deduction of argument type.");
      constexpr bool arg_2_1 = std::is_same_v<int, midas::type_traits::ArgumentType<decltype(detail::f2), 1>>;
      static_assert(arg_2_1, "Wrong deduction of argument type.");
   }
};

/**
 *
 **/
struct LambdaFunctionTraitsTest
   :  public cutee::test
{
   void run
      (
      )  override
   {
      auto l0 = []() -> double { return 0.0; };
      auto l1 = [](double a0) -> double { return a0; };
      auto l2 = [](double a0, int a1) -> double { return a1*a0; };

      constexpr size_t argc_0 = midas::type_traits::ArgumentCount<decltype(l0)>;
      static_assert(argc_0 == 0, "Argument count for 0 args failed.");
      constexpr size_t argc_1 = midas::type_traits::ArgumentCount<decltype(l1)>;
      static_assert(argc_1 == 1, "Argument count for 1 args failed.");
      constexpr size_t argc_2 = midas::type_traits::ArgumentCount<decltype(l2)>;
      static_assert(argc_2 == 2, "Argument count for 2 args failed.");

      constexpr bool ret_0 = std::is_same_v<double, midas::type_traits::ReturnType<decltype(l0)>>;
      static_assert(ret_0, "Wrong deduction of return type.");
      constexpr bool ret_1 = std::is_same_v<double, midas::type_traits::ReturnType<decltype(l1)>>;
      static_assert(ret_1, "Wrong deduction of return type.");
      constexpr bool ret_2 = std::is_same_v<double, midas::type_traits::ReturnType<decltype(l2)>>;
      static_assert(ret_2, "Wrong deduction of return type.");

      constexpr bool arg_1 = std::is_same_v<double, midas::type_traits::ArgumentType<decltype(l1), 0>>;
      static_assert(arg_1, "Wrong deduction of argument type.");
      constexpr bool arg_2_0 = std::is_same_v<double, midas::type_traits::ArgumentType<decltype(l2), 0>>;
      static_assert(arg_2_0, "Wrong deduction of argument type.");
      constexpr bool arg_2_1 = std::is_same_v<int, midas::type_traits::ArgumentType<decltype(l2), 1>>;
      static_assert(arg_2_1, "Wrong deduction of argument type.");
   }
};

/**
 *
 **/
struct MemberFunctionTraitsTest
   :  public cutee::test
{
   void run
      (
      )  override
   {
      constexpr size_t argc_0 = midas::type_traits::ArgumentCount<decltype(&detail::A::f0)>;
      static_assert(argc_0 == 0, "Argument count for 0 args failed.");
      constexpr size_t argc_1 = midas::type_traits::ArgumentCount<decltype(&detail::A::f1)>;
      static_assert(argc_1 == 1, "Argument count for 1 args failed.");
      constexpr size_t argc_2 = midas::type_traits::ArgumentCount<decltype(&detail::A::f2)>;
      static_assert(argc_2 == 2, "Argument count for 2 args failed.");
      constexpr size_t argc_0_r = midas::type_traits::ArgumentCount<decltype(&detail::A::GetInt)>;
      static_assert(argc_0_r == 0, "Argument count for 0 args failed.");

      constexpr bool ret_0 = std::is_same_v<double, midas::type_traits::ReturnType<decltype(&detail::A::f0)>>;
      static_assert(ret_0, "Wrong deduction of return type.");
      constexpr bool ret_1 = std::is_same_v<double, midas::type_traits::ReturnType<decltype(&detail::A::f1)>>;
      static_assert(ret_1, "Wrong deduction of return type.");
      constexpr bool ret_2 = std::is_same_v<double, midas::type_traits::ReturnType<decltype(&detail::A::f2)>>;
      static_assert(ret_2, "Wrong deduction of return type.");
      constexpr bool ret_0_r = std::is_same_v<const int&, midas::type_traits::ReturnType<decltype(&detail::A::GetInt)>>;
      static_assert(ret_0_r, "Wrong deduction of return type.");

      constexpr bool arg_1 = std::is_same_v<double, midas::type_traits::ArgumentType<decltype(&detail::A::f1), 0>>;
      static_assert(arg_1, "Wrong deduction of argument type.");
      constexpr bool arg_2_0 = std::is_same_v<double, midas::type_traits::ArgumentType<decltype(&detail::A::f2), 0>>;
      static_assert(arg_2_0, "Wrong deduction of argument type.");
      constexpr bool arg_2_1 = std::is_same_v<int, midas::type_traits::ArgumentType<decltype(&detail::A::f2), 1>>;
      static_assert(arg_2_1, "Wrong deduction of argument type.");
   }
};

} /* namespace midas::test */

#endif /* FUNCTIONTRAITSTEST_H_INCLUDED */
