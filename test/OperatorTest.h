/**
 *******************************************************************************
 * 
 * @file    OperatorTest.h 
 * @date    26-03-2020
 * @author  Ian Heide Godtliebsen (ian@chem.au.dk)
 *
 * @brief
 *    Unit tests for MidasCpp operators
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef OPERATOR_TEST_H_INCLUDED 
#define OPERATOR_TEST_H_INCLUDED 

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "test/CuteeInterface.h"

#include "operator/PropertyType.h"

namespace midas::test::operator_test
{
   //! Implementation details for your test, e.g. utility functions.
   namespace detail
   {
   }

   /************************************************************************//**
    * @brief
    *    Tests should be simple, so brief descriptions should suffice.
    *
    * Otherwise put some more details here. //CHANGE! comments
    ***************************************************************************/
   struct PropertyTypeTest
      :  public cutee::test
   {
      void run() override
      {
         auto property_type = oper::StringToPropertyType("dipole_x");

         UNIT_ASSERT_EQUAL    (property_type, oper::dipole_x, "Property type not equal to 'dipole_x'.");
         UNIT_ASSERT_NOT_EQUAL(property_type, oper::dipole_y, "Property type "  "equal to 'dipole_y'.");
         UNIT_ASSERT_NOT_EQUAL(property_type, oper::dipole_z, "Property type "  "equal to 'dipole_z'.");

         UNIT_ASSERT( property_type.Is(oper::dipole ),           "Is not 'dipole'.");
         UNIT_ASSERT( property_type.Is(oper::dipole_x),          "Is not 'dipole_x'.");
         UNIT_ASSERT(!property_type.Is(oper::dipole_y),          "Is "  "'dipole_y'.");
         UNIT_ASSERT(!property_type.Is(oper::dipole_z),          "Is "  "'dipole_z'.");
         UNIT_ASSERT(!property_type.Is(oper::polarizability2),   "Is "  "'polarizability2'.");
         UNIT_ASSERT(!property_type.Is(oper::polarizability_xx), "Is "  "'polarizability_xx'.");
         UNIT_ASSERT( property_type.Is(oper::axis_x),            "Is not 'axis_x'.");

         UNIT_ASSERT_EQUAL(oper::PropertyTypeToString(property_type), std::string{"dipole_x"}, "String not correct.");
      }
   };

} /* namespace midas::test::your_test_namespace */ //CHANGE! your_test_namespace

#endif/*UNITTESTTEMPLATE_H_INCLUDED*/ //CHANGE! header guard
