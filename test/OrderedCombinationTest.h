/**
 *******************************************************************************
 * 
 * @file    OrderedCombinationTest.h
 * @date    16-02-2017
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Unit tests for OrderedCombination.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef ORDEREDCOMBINATIONTEST_H_INCLUDED
#define ORDEREDCOMBINATIONTEST_H_INCLUDED

#include <iostream>

#include "test/CuteeInterface.h"

#include "inc_gen/TypeDefs.h"
#include "util/OrderedCombination.h"
#include "input/ModeCombi.h"

namespace midas
{
namespace test
{

// Utility functions used in the testing:
namespace detail
{
   //! Returns whether arguments contain equal combinations, with error info.
   bool Equality(OrderedCombination, const std::vector<std::vector<In>>&, std::string&);
}  /* namespace detail */

/***************************************************************************//**
 * @brief
 *    Constructor from begin size, end size, and total number of elements.
 ******************************************************************************/
struct ConstrDefaultNonFixed: public cutee::test
{
   void run()
   {
      std::string err;
      std::vector<std::vector<In>> control =
         {  {}
         ,  {0}
         ,  {1}
         ,  {2}
         ,  {0,1}
         ,  {0,2}
         ,  {1,2}
         ,  {0,1,2}
         };
      bool equal = detail::Equality(OrderedCombination(I_0,I_4,I_3), control, err);
      UNIT_ASSERT(equal, "\nOrderedCombination(0,4,3) inequality:\n"+err);

      control =
         {  {0,1,2}
         ,  {0,1,3}
         ,  {0,1,4}
         ,  {0,2,3}
         ,  {0,2,4}
         ,  {0,3,4}
         ,  {1,2,3}
         ,  {1,2,4}
         ,  {1,3,4}
         ,  {2,3,4}
         ,  {0,1,2,3}
         ,  {0,1,2,4}
         ,  {0,1,3,4}
         ,  {0,2,3,4}
         ,  {1,2,3,4}
         };
      equal = detail::Equality(OrderedCombination(I_3,I_5,I_5), control, err);
      UNIT_ASSERT(equal, "\nOrderedCombination(3,5,5) inequality:\n"+err);

      control =
         {
         };
      equal = detail::Equality(OrderedCombination(I_0,I_0,I_0), control, err);
      UNIT_ASSERT(equal, "\nOrderedCombination(0,0,0) inequality:\n"+err);
   }
};

/***************************************************************************//**
 * @brief
 *    Constructor from begin size, end size, and supplied elements.
 *
 * The supplied elements are positive, negative, with duplicates and not
 * sorted; they shall be sorted by in the construction of the object.
 ******************************************************************************/
struct ConstrSuppliedNonFixed: public cutee::test
{
   void run()
   {
      std::string err;
      std::vector<In> elements = {314, -1, -1, -1, 1337, 314, 314, 42, -1};
      std::vector<std::vector<In>> control =
         {  {-1}
         ,  {42}
         ,  {314}
         ,  {1337}
         ,  {-1,  42}
         ,  {-1,  314}
         ,  {-1,  1337}
         ,  {42,  314}
         ,  {42,  1337}
         ,  {314, 1337}
         };
      bool equal = detail::Equality(OrderedCombination(I_1,I_3,elements), control, err);
      UNIT_ASSERT(equal, "\nOrderedCombination(1,3,{314,1337,42,-1}) inequality:\n"+err);
   }
};

/***************************************************************************//**
 * @brief
 *    Constructor from begin size, end size, total number of elements and some
 *    supplied fixed elements.
 *
 * The supplied fixed elements are positive, negative, with duplicates and not
 * sorted; they shall be sorted by in the construction of the object.
 ******************************************************************************/
struct ConstrFixedDefaultNonFixed: public cutee::test
{
   void run()
   {
      std::string err;
      // fixed    = {-3, -2, 0, 1, 2, 5, 6, 10}
      std::vector<In> fixed = {-2, -3, -3, 1, 0, 0, 0, 2, 10, 5, 6};
      // nonfixed = {3, 4, 7, 8}
      Uin num_total = 12;
      Uin begin = 0;
      Uin end   = 3;
      std::vector<std::vector<In>> control =
         // 0 non-fixed elements.
         {  {-3, -2, 0, 1, 2, 5, 6, 10}
         // 1 non-fixed element.
         ,  {-3, -2, 0, 1, 2, 3, 5, 6, 10}
         ,  {-3, -2, 0, 1, 2, 4, 5, 6, 10}
         ,  {-3, -2, 0, 1, 2, 5, 6, 7, 10}
         ,  {-3, -2, 0, 1, 2, 5, 6, 8, 10}
         // 2 non-fixed elements.
         ,  {-3, -2, 0, 1, 2, 3, 4, 5, 6, 10}
         ,  {-3, -2, 0, 1, 2, 3, 5, 6, 7, 10}
         ,  {-3, -2, 0, 1, 2, 3, 5, 6, 8, 10}
         ,  {-3, -2, 0, 1, 2, 4, 5, 6, 7, 10}
         ,  {-3, -2, 0, 1, 2, 4, 5, 6, 8, 10}
         ,  {-3, -2, 0, 1, 2, 5, 6, 7, 8, 10}
         };
      bool equal = detail::Equality(OrderedCombination(begin, end, num_total, fixed), control, err);
      UNIT_ASSERT(equal, "\nOrderedCombination(..., num_total, fixed) inequality:\n"+err);
   }
};

/***************************************************************************//**
 * @brief
 *    Constructor from begin size, end size, supplied non-fixed and fixed
 *    elements.
 *
 * The supplied elements are positive, negative, with duplicates and not
 * sorted; they shall be sorted by in the construction of the object.
 * They also overlap; occurences of fixed elements among the supplied non-fixed
 * ones shall be removed.
 ******************************************************************************/
struct ConstrFixedSuppliedNonFixed: public cutee::test
{
   void run()
   {
      std::string err;
      // fixed    = {-3, -2, 0, 1, 2, 5, 6, 10}
      std::vector<In> fixed = {-2, -3, -3, 1, 0, 0, 0, 2, 10, 5, 6};
      // nonfixed = {-5, -2, 0, 8, 11}
      //        --> {-5, 8, 11} after removing fixed ones.
      std::vector<In> nonfixed = {11, 0, 0, 0, 8, 11, 11, -2, -5, -2};
      Uin begin = 0;
      Uin end   = 3;
      std::vector<std::vector<In>> control =
         // 0 non-fixed elements.
         {  {-3, -2, 0, 1, 2, 5, 6, 10}
         // 1 non-fixed element.
         ,  {-5, -3, -2, 0, 1, 2, 5, 6, 10}
         ,  {-3, -2, 0, 1, 2, 5, 6, 8, 10}
         ,  {-3, -2, 0, 1, 2, 5, 6, 10, 11}
         // 2 non-fixed elements.
         ,  {-5, -3, -2, 0, 1, 2, 5, 6, 8, 10}
         ,  {-5, -3, -2, 0, 1, 2, 5, 6, 10, 11}
         ,  {-3, -2, 0, 1, 2, 5, 6, 8, 10, 11}
         };
      bool equal = detail::Equality(OrderedCombination(begin, end, nonfixed, fixed), control, err);
      UNIT_ASSERT(equal, "\nOrderedCombination(..., nonfixed, fixed) inequality:\n"+err);
   }
};

/***************************************************************************//**
 * @brief
 *    Test a call to utility function SizeBegin().
 ******************************************************************************/
struct UtilitySizeBegin: public cutee::test
{
   void run()
   {
      OrderedCombination oc(0,4,5);
      UNIT_ASSERT_EQUAL(oc.SizeBegin(), static_cast<Uin>(0), "SizeBegin() failed.");
   }
};

/***************************************************************************//**
 * @brief
 *    Test a call to utility function SizeEnd().
 ******************************************************************************/
struct UtilitySizeEnd: public cutee::test
{
   void run()
   {
      OrderedCombination oc(0,4,5);
      UNIT_ASSERT_EQUAL(oc.SizeEnd(), static_cast<Uin>(4), "SizeEnd() failed.");
   }
};

/***************************************************************************//**
 * @brief
 *    Test a call to utility function NumElementsNonFixed().
 ******************************************************************************/
struct UtilityNumElementsNonFixed: public cutee::test
{
   void run()
   {
      OrderedCombination oc(0,4,8,std::vector<In>({0,1,2}));
      UNIT_ASSERT_EQUAL(oc.NumElementsNonFixed(), static_cast<Uin>(5), "NumElementsNonFixed() failed.");
   }
};

/***************************************************************************//**
 * @brief
 *    Test a call to utility function NumElementsTotal().
 ******************************************************************************/
struct UtilityNumElementsTotal: public cutee::test
{
   void run()
   {
      OrderedCombination oc(0,4,8,std::vector<In>({0,1,2}));
      UNIT_ASSERT_EQUAL(oc.NumElementsTotal(), static_cast<Uin>(8), "NumElementsTotal() failed.");
   }
};

/***************************************************************************//**
 * @brief
 *    Test that the OrderedCombination::operator++() implicitly corresponds to
 *    operator<(const ModeCombi&, const ModeCombi&).
 *
 * I.e. looping through an OrderedCombination using the increment operator
 * should give vectors that correspond to succesively larger ModeCombi%s
 * according to the operator< comparison of the latter.
 *
 * In other words, if you were to change operator<(const ModeCombi&, const
 * ModeCombi&), maybe you should consider changing how OrderedCombination is
 * incrementet, since -- originally, at least -- OrderedCombination was
 * implemented to correspond to operator<(const ModeCombi&, const ModeCombi&).
 ******************************************************************************/
struct ModeCombiOperatorLessThan: public cutee::test
{
   void run()
   {
      OrderedCombination oc(0,4,std::vector<In>({-3,-2,0,2,3,5,7}));
      ModeCombi previous_mc(oc.GetCombination(), -I_1);
      ++oc;
      std::stringstream err;
      while (!oc.AtEnd())
      {
         ModeCombi current_mc(oc.GetCombination(), -I_1);
         bool less_than = (previous_mc < current_mc);
         if (!less_than)
         {
            err   
               << '\n'
               << "With current implementation of\n"
               << "   operator<(const ModeCombi&, const ModeCombi&)\n"
               << "   OrderedCombination::operator++()\n"
               << "the OrderedCombination vectors do not correspond to succesively\n"
               << "larger ModeCombi.\n"
               << "Specifically, previous ModeCombi not less than current ModeCombi;\n"
               << "   previous = " << previous_mc << '\n'
               << "   current  = " << current_mc  << '\n'
               ;
         }
         UNIT_ASSERT(less_than, err.str());

         ++oc;
         previous_mc = std::move(current_mc);
      }
   }
};

} /* namespace test */
} /* namespace midas */

#endif /* ORDEREDCOMBINATIONTEST_H_INCLUDED */
