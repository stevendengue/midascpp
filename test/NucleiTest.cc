#include "test/NucleiTest.h"

namespace midas::test
{

void NucleiTest()
{
   cutee::suite suite("Nuclei test");
   
   suite.add_test<NucleiInternalCoordTest>("Nuclei coordinates test");
   suite.add_test<NucleiMassTest>("Nuclei mass test");
   suite.add_test<NucleiRotationTest>("Nuclei rotation test");
   
   RunSuite(suite);
}

} /* namespace midas::test */
