/**
 *******************************************************************************
 * 
 * @file    PathTest.h 
 * @date    NOW
 * @author  Ian Heide Godtliebsen (ian@chem.au.dk)
 *
 * @brief
 *    Test functionality in util/Path.h
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef TEST_PATHTEST_H_INCLUDED 
#define TEST_PATHTEST_H_INCLUDED 

#include "test/CuteeInterface.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Path.h"

namespace midas::test::path_test
{
   //! Implementation details for your test, e.g. utility functions.
   namespace detail
   {
   }
   /************************************************************************//**
    * @brief
    *    Test that path::HasDirEnd() function return correctly
    ***************************************************************************/
   struct TestHasDirEnd
      :  public cutee::test
   {
      void run() override
      {
         std::string path1 = "/path/to/";
         std::string path2 = "/path/to";

         UNIT_ASSERT( path::HasDirEnd(path1), "Function path::HasDirEnd() incorrect for has directory end."); 
         UNIT_ASSERT(!path::HasDirEnd(path2), "Function path::HasDirEnd() incorrect for no  directory end."); 
      }
   };

   /************************************************************************//**
    * @brief
    *    Test that path::HasDirBegin() function return correctly
    ***************************************************************************/
   struct TestHasDirBegin
      :  public cutee::test
   {
      void run() override
      {
         std::string path1 = "/path/to/";
         std::string path2 = "path/to";

         UNIT_ASSERT( path::HasDirBegin(path1), "Function path::HasDirBegin() incorrect for has directory begin."); 
         UNIT_ASSERT(!path::HasDirBegin(path2), "Function path::HasDirBegin() incorrect for no  directory begin."); 
      }
   };

   /************************************************************************//**
    * @brief
    *    Test that path::FileName() function return correct file name.
    ***************************************************************************/
   struct TestFileName 
      :  public cutee::test
   {
      void run() override
      {
         std::string path = "/path/to/file.ext";

         UNIT_ASSERT_EQUAL(path::FileName(path), std::string{"file.ext"}, "Function path::FileName() incorrect."); 
      }
   };
   
   /************************************************************************//**
    * @brief
    *    Test that path::BaseName() function return correct base name.
    ***************************************************************************/
   struct TestBaseName
      :  public cutee::test
   {
      void run() override
      {
         std::string path = "/path/to/file.ext";

         UNIT_ASSERT_EQUAL(path::BaseName(path), std::string{"file"}, "Function path::BaseName() incorrect."); 
      }
   };
   
   /************************************************************************//**
    * @brief
    *    Test that path::DirName() function return correct directory name.
    ***************************************************************************/
   struct TestDirName
      :  public cutee::test
   {
      void run() override
      {
         std::string path = "/path/to/file.ext";

         UNIT_ASSERT_EQUAL(path::DirName(path), std::string{"/path/to"}, "Function path::DirName() incorrect."); 
      }
   };
   
   /************************************************************************//**
    * @brief
    *    Test that path::RelPath() function return correct directory name.
    ***************************************************************************/
   struct TestRelPath
      :  public cutee::test
   {
      void run() override
      {
         std::string path_abs = "/path/to/file.ext";
         std::string path_rel = "path/to/file.ext";

         UNIT_ASSERT(!path::IsRelPath(path_abs), "Function path::IsRelPath() incorrect for abs path."); 
         UNIT_ASSERT( path::IsRelPath(path_rel), "Function path::IsRelPath() incorrect for rel path."); 
      }
   };
   
   /************************************************************************//**
    * @brief
    *    Test that path::AbsPath() function return correct directory name.
    ***************************************************************************/
   struct TestAbsPath
      :  public cutee::test
   {
      void run() override
      {
         std::string path_abs = "/path/to/file.ext";
         std::string path_rel = "path/to/file.ext";

         UNIT_ASSERT( path::IsAbsPath(path_abs), "Function path::IsAbsPath() incorrect for abs path."); 
         UNIT_ASSERT(!path::IsAbsPath(path_rel), "Function path::IsAbsPath() incorrect for rel path."); 
      }
   };
   
   /************************************************************************//**
    * @brief
    *    Test that path::Join() function return correct directory name.
    ***************************************************************************/
   struct TestJoin
      :  public cutee::test
   {
      void run() override
      {
         std::string path1 = "/path/to";
         std::string path2 = "/path/to/";
         std::string path3 = "file.ext";
         std::string path4 = "/file.ext";

         UNIT_ASSERT_EQUAL(path::Join(path1, path3), std::string{"/path/to/file.ext"}, "Function path::Join() incorrect for no  directory end  no directory begin."); 
         UNIT_ASSERT_EQUAL(path::Join(path2, path3), std::string{"/path/to/file.ext"}, "Function path::Join() incorrect for has directory end  no directory begin."); 
         UNIT_ASSERT_EQUAL(path::Join(path1, path4), std::string{"/path/to/file.ext"}, "Function path::Join() incorrect for no  directory end has directory begin."); 
         UNIT_ASSERT_EQUAL(path::Join(path2, path4), std::string{"/path/to/file.ext"}, "Function path::Join() incorrect for has directory end has directory begin."); 
      }
   };

} /* namespace midas::test::path_test */ 

#endif/* TEST_PATHTEST_H_INCLUDED*/ 
