/**
 *******************************************************************************
 * 
 * @file    MatRepSolversTest.cc
 * @date    07-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#ifndef MATREPSOLVERSTEST_H_INCLUDED
#define MATREPSOLVERSTEST_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "ni/OneModeInt.h"
#include "input/VccCalcDef.h"
#include "vcc/Vcc.h"
#include "vcc/ModalIntegralsFuncs.h"
#include "td/tdvcc/trf/TrfTimTdvccMatRep.h"
#include "td/tdvcc/trf/TrfTimTdextvccMatRep.h"
#include "td/tdvcc/params/ParamsTimTdextvcc.h"
#include "td/tdvcc/anal/FvciNorm2.h"
#include "test/util/OpDefsForTesting.h"
#include "tensor/EnergyDifferencesTensor.h"
#include "it_solver/nl_solver/SubspaceSolver.h"
#include "util/matrep/MatRepVibOper.h"
#include "util/matrep/MatRepUtils.h"
#include "lapack_interface/SYEVD.h"
#include "vcc/subspacesolver/ExtVccSolver.h"
#include "vcc/subspacesolver/TradVccSolver.h"
#include "mmv/VectorAngle.h"
#include "test/util/MpiSyncedAssertions.h"
#include "test/CuteeInterface.h"

namespace midas::test::matrepsolver
{
   //! Implementation details for your test, e.g. utility functions.
   namespace detail
   {
   }

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   class SolverTestBase
      :  public cutee::test
   {
      public:
         using param_t = PARAM_T;
         using absval_t = midas::type_traits::RealTypeT<param_t>;
         using real_t = midas::type_traits::RealTypeT<param_t>;
         using vec_t = GeneralMidasVector<param_t>;

      private:
         //@{
         //! Fixed settings for the molecular system.
         const Uin n_modes = 3;
         const Uin n_modals_per_mode = 3;
         const Uin oper_coup = 3;
         const Uin n_prim_bas = 15;
         const Uin max_exci = 3;
         const std::vector<In> n_modals_in = std::vector<In>(n_modes, n_modals_per_mode);
         const std::vector<Uin> n_modals = std::vector<Uin>(n_modes, n_modals_per_mode);
         //@}

         //@{
         //! Subspace solver settings.
         const Uin sub_dim = 3;
         const Uin max_it = 50;
         const absval_t threshold = 2.5e-16;
         const bool crop_not_diis = false;
         const bool diag_precon = true;
         //@}

         //@{
         //! Misc settings.
         const bool unmute = true;
         bool mout_was_muted = false;
         //@}

         //@{
         //! Controls. (Energies are for oper_coup = 3, n_prim_bas = 15, max_exci = 3, n_modals = 3.)
         const Nb ctrl_e_vscf = 2.1059032746196405E-02;
         const Nb ctrl_e_vcc = 2.0987894595305995E-02;
         //@}

         template<class... Args>
         std::unique_ptr<midas::vcc::subspacesolver::VibCorrSubspaceSolver<param_t>> GetSolver(const Vcc&arVccCalc, Args&&... args) const
         {
            auto p = GetSolverImpl(std::forward<Args>(args)...);
            p->SubspaceDim() = sub_dim;
            p->MaxIter() = max_it;
            p->ResNormThr() = threshold;
            if (crop_not_diis) p->EnableCrop();
            if (diag_precon) p->EnableInvDiagJac0Precon(arVccCalc.GetEigVal(), arVccCalc.GetOccModalOffSet());
            return p;
         }
         virtual std::unique_ptr<midas::vcc::subspacesolver::VibCorrSubspaceSolver<param_t>> GetSolverImpl
            (  const std::vector<Uin>& arNModals
            ,  const OpDef& arOpDef
            ,  const ModalIntegrals<param_t>& arModInts
            ,  const ModeCombiOpRange& arMcr
            )  const = 0;

         virtual std::vector<vec_t> ResidualVector
            (  const std::vector<vec_t>& arSolVecs
            ,  const std::vector<Uin>& arNModals
            ,  const OpDef& arOpDef
            ,  const ModalIntegrals<param_t>& arModInts
            ,  const ModeCombiOpRange& arMcr
            )  const = 0;

         virtual Uin CtrlNIter
            (  Uin aMaxExci
            ,  bool aCropNotDiis
            ,  bool aDiagPrecon
            )  const = 0;

         bool FvciLimit() const {return max_exci == 3;}

      public:
         SolverTestBase
            (  Uin aMaxExci = 3
            ,  bool aCrop = false
            ,  bool aDiagPrecon = true
            )
            :  max_exci(aMaxExci)
            ,  crop_not_diis(aCrop)
            ,  diag_precon(aDiagPrecon)
         {
         }

         void setup() override
         {
            gOperatorDefs.ReSet();
            if (unmute)
            {
               mout_was_muted = Mout.Muted();
               Mout.Unmute();
            }
         }
         void teardown() override
         {
            if(unmute && mout_was_muted)
            {
               Mout.Mute();
            }
            gOperatorDefs.ReSet();
         }
         void run() override
         {
            using namespace midas::test::mpi_sync;
            if (unmute)
            {
               Mout << "test name = " << this->name() << std::endl;
            }

            // Setup.
            OpDef opdef = midas::test::util::WaterH0_ir_h2o_vci(oper_coup, true, false);
            BasDef basdef = midas::test::util::HoBasis(opdef, n_prim_bas, false);
            OneModeInt omi = midas::test::util::SetupOneModeInts(opdef, basdef);
            VccCalcDef calcdef = midas::test::util::SetupVccCalcDef(n_modals_in, max_exci, false);

            // VSCF.
            Vcc calc(&calcdef, &opdef, &basdef, &omi);
            midas::test::util::RunVscf(calc, !unmute);
            MPISYNC_UNIT_ASSERT(calc.Converged(), "VSCF not converged.");
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(calc.GetEtot(), ctrl_e_vscf, 10, "Wrong VSCF energy.");

            // Setup for vib.corr. calc.
            ModalIntegrals<real_t> modints_re = *calc.pIntegrals();;
            ModalIntegrals<param_t> modints = midas::vcc::modalintegrals::detail::ConvertFromReal<param_t>::Convert(*calc.pIntegrals());
            ModeCombiOpRange mcr(max_exci, n_modes);
            const Uin tot_size = SetAddresses(mcr, n_modals);

            // Control: diagonalize the Hessian. (Only if FVCI limit.)
            real_t ctrl_E = 0;
            GeneralMidasVector<real_t> ctrl_fvci_ket_re(midas::util::matrep::Product(n_modals), real_t(0));
            if (FvciLimit())
            {
               midas::util::matrep::MatRepVibOper<real_t> mr(n_modals);
               mr.FullOperator(opdef, modints_re);
               auto eigsol = SYEVD(mr.GetMatRep(), 'V');
               MPISYNC_UNIT_ASSERT_EQUAL(In(eigsol._info), In(0), "Bad SYEVD info.");
               NormalizeEigenvectors(eigsol, ilapack::normalize_t());

               GeneralMidasVector<real_t> eigvals;
               LoadEigenvalues(eigsol, eigvals);
               MPISYNC_UNIT_ASSERT_EQUAL(Uin(eigvals.Size()), mr.FullDim(), "Wrong eigvals size.");
               ctrl_E = eigvals[0];

               GeneralMidasMatrix<real_t> eigvecs;
               LoadEigenvectors(eigsol, eigvecs);
               GeneralMidasVector<real_t> eigvec;
               eigvec.SetNewSize(eigvecs.Nrows());
               eigvecs.GetCol(eigvec, 0);
               ctrl_fvci_ket_re = mr.ExtractToMcrSpace(eigvec, mcr);
               
               MPISYNC_UNIT_ASSERT_EQUAL(Uin(ctrl_fvci_ket_re.Size()), mr.FullDim(), "Wrong ctrl_fvci_ket_re size.");
               MPISYNC_UNIT_ASSERT(ctrl_fvci_ket_re[0] != real_t(0), "Ref. elem. = 0 (from full mat. diag.).");
               ctrl_fvci_ket_re.Scale(ctrl_fvci_ket_re[0] < 0? -1.: 1.);
               MPISYNC_UNIT_ASSERT_FEQUAL_PREC(absval_t(ctrl_fvci_ket_re.Norm()), absval_t(1), 2, "Unnormalized eig.vec.");
            }
            vec_t ctrl_fvci_ket(ctrl_fvci_ket_re);
            vec_t ctrl_fvci_bra = ctrl_fvci_ket.Conjugate();
            if(FvciLimit()) MPISYNC_UNIT_ASSERT_FEQUAL_PREC(ctrl_E, decltype(ctrl_E)(ctrl_e_vcc), 20, "Wrong ctrl_E (from full mat. diag.).");

            // Run the iterative solver.
            auto p_solver = GetSolver(calc, n_modals, opdef, modints, mcr);
            p_solver->FvciAnalysis() = true;
            bool ret_conv = p_solver->Solve();
            MPISYNC_UNIT_ASSERT_EQUAL(Uin(ret_conv), Uin(p_solver->Conv()), "solver: ret_conv != Conv()");
            MPISYNC_UNIT_ASSERT(p_solver->Conv(), "solver: not converged.");
            MPISYNC_UNIT_ASSERT_EQUAL(p_solver->NIter(), CtrlNIter(max_exci, crop_not_diis, diag_precon), "solver: wrong num. iters.");
            if(FvciLimit()) MPISYNC_UNIT_ASSERT_FEQUAL_PREC(p_solver->ExpVal(), param_t(ctrl_E), 20, "solver: wrong ExpVal()");

            // Check residual vector.
            absval_t norm2 = 0;
            for(const auto& v: ResidualVector(p_solver->SolVecs(), n_modals, opdef, modints, mcr))
            {
               norm2 += Norm2(v);
            }
            const absval_t norm = sqrt(norm2);
            std::stringstream ss;
            ss << std::scientific << "(norm = " << norm << ", thr = " << threshold << ")";
            MPISYNC_UNIT_ASSERT_FEQUAL_PREC(norm, p_solver->LastResNorm(), 4, "norm != LastResNorm()");
            MPISYNC_UNIT_ASSERT(norm < threshold, "Error-vector norm >= thr "+ss.str());

            // Check FVCI conversions.
            std::array<vec_t,2> v_fvci = {p_solver->FvciVec(), p_solver->FvciVec(true)};
            std::array<vec_t,2> v_ctrl_fvci = {ctrl_fvci_ket, ctrl_fvci_bra};
            std::array<std::string,2> v_fvci_str = {"ket", "bra"};
            for(Uin i = 0; i < v_fvci.size(); ++i)
            {
               const auto& fvci = v_fvci.at(i);
               const auto& ctrl = v_ctrl_fvci.at(i);
               const std::string s = "FVCI ("+v_fvci_str.at(i)+"): ";
               MPISYNC_UNIT_ASSERT_EQUAL(fvci.Size(), ctrl.Size(), s+"Wrong Size()");
               MPISYNC_UNIT_ASSERT_FEQUAL_PREC(Norm(fvci), absval_t(1), 2, s+"Wrong Norm()");
               MPISYNC_UNIT_ASSERT(std::real(fvci[0]) >= 0, s+"Re(fvci[0]) < 0");
               MPISYNC_UNIT_ASSERT_FZERO_PREC(std::imag(fvci[0]), absval_t(1), 0, s+"Im(fvci[0]) != 0");
               if(FvciLimit()) MPISYNC_UNIT_ASSERT_FZERO_PREC(sqrt(DiffNorm2(fvci,ctrl)), absval_t(1), 20, s+"Wrong DiffNorm2");
            }

            // Check FVCI vec. analysis.
            if (p_solver->Conv())
            {
               using midas::util::matrep::MatRepVibOper;
               const vec_t fvci_ket = p_solver->FvciVec(false, false);
               const vec_t fvci_bra = p_solver->FvciVec(true, false);
               const vec_t fvci_bra_c = fvci_bra.Conjugate();
               const vec_t n_fvci_ket = fvci_ket/Norm(fvci_ket);
               const vec_t n_fvci_bra_c = fvci_bra_c/Norm(fvci_bra_c);

               const absval_t ctrl_fvcinorm2_ket = Norm2(fvci_ket);
               const absval_t ctrl_fvcinorm2_bra = Norm2(fvci_bra);
               const absval_t ctrl_fvcinorm2_product = ctrl_fvcinorm2_ket * ctrl_fvcinorm2_bra;
               const absval_t ctrl_angle_ketbra = VectorAngle(fvci_bra_c, fvci_ket);
               const absval_t ctrl_angle_ketbra_in_space = VectorAngle(fvci_bra_c, fvci_ket, 0, tot_size);

               MPISYNC_UNIT_ASSERT_FEQUAL_PREC(p_solver->FvciNorm2(false), ctrl_fvcinorm2_ket, 0, "FvciNorm2(ket)");
               MPISYNC_UNIT_ASSERT_FEQUAL_PREC(p_solver->FvciNorm2(true), ctrl_fvcinorm2_bra, 0, "FvciNorm2(bra)");
               MPISYNC_UNIT_ASSERT_FEQUAL_PREC(p_solver->FvciNorm2Product(), ctrl_fvcinorm2_product, 0, "FvciNorm2Product()");
               MPISYNC_UNIT_ASSERT_FEQUAL_PREC(p_solver->HilbertSpaceAngleKetBra(), ctrl_angle_ketbra, 0, "HilbertSpaceAngleKetBra()");
               MPISYNC_UNIT_ASSERT_FEQUAL_PREC(p_solver->HilbertSpaceAngleKetBra(true), ctrl_angle_ketbra_in_space, 0, "HilbertSpaceAngleKetBra(in space)");
            }
         }
   };

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   struct SolverExtVccDiagPreconWater
      :  public SolverTestBase<PARAM_T>
   {
      public:
         using typename SolverTestBase<PARAM_T>::param_t;
         using typename SolverTestBase<PARAM_T>::absval_t;
         using typename SolverTestBase<PARAM_T>::vec_t;

         template<class... Args>
         SolverExtVccDiagPreconWater(Args&&... args)
            :  SolverTestBase<PARAM_T>(std::forward<Args>(args)...)
         {
         }


      private:
         std::unique_ptr<midas::vcc::subspacesolver::VibCorrSubspaceSolver<param_t>> GetSolverImpl
            (  const std::vector<Uin>& arNModals
            ,  const OpDef& arOpDef
            ,  const ModalIntegrals<param_t>& arModInts
            ,  const ModeCombiOpRange& arMcr
            )  const override
         {
            return std::make_unique<midas::vcc::subspacesolver::ExtVccSolver<param_t>>(arNModals, arOpDef, arModInts, arMcr);
         }

         std::vector<vec_t> ResidualVector
            (  const std::vector<vec_t>& arSolVecs
            ,  const std::vector<Uin>& arNModals
            ,  const OpDef& arOpDef
            ,  const ModalIntegrals<param_t>& arModInts
            ,  const ModeCombiOpRange& arMcr
            )  const override
         {
            midas::tdvcc::TrfTimTdextvccMatRep<param_t> trf(arNModals, arOpDef, arModInts, arMcr);
            const auto& t_amps = arSolVecs.at(0);
            const auto& s_amps = arSolVecs.at(1);
            auto trf_a = trf.HamDerExtAmp(0., t_amps, s_amps);
            auto trf_b = trf.HamDerClustAmp(0., t_amps, s_amps);
            if (trf_a.Size() > 0 && trf_b.Size() > 0)
            {
               trf_a[0] = 0;
               trf_b[0] = 0;
            }
            return {trf_a, trf_b};
         }

         Uin CtrlNIter
            (  Uin aMaxExci
            ,  bool aCropNotDiis
            ,  bool aDiagPrecon
            )  const override
         {
            // MBH, Jan 2020: Add values for aDiagPrecon = false if ever
            // getting convergence with that one.
            if (aCropNotDiis)
            {
               if (aMaxExci == 3)      return 16;
               else if(aMaxExci == 2)  return 16;
               else                    return 0;
            }
            else
            {
               if (aMaxExci == 3)      return 18;
               else if(aMaxExci == 2)  return 18;
               else                    return 0;
            }
         }

   };

   /************************************************************************//**
    * 
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   struct SolverTradVccDiagPreconWater
      :  public SolverTestBase<PARAM_T>
   {
      public:
         using typename SolverTestBase<PARAM_T>::param_t;
         using typename SolverTestBase<PARAM_T>::absval_t;
         using typename SolverTestBase<PARAM_T>::vec_t;

         template<class... Args>
         SolverTradVccDiagPreconWater(Args&&... args)
            :  SolverTestBase<PARAM_T>(std::forward<Args>(args)...)
         {
         }


      private:
         std::unique_ptr<midas::vcc::subspacesolver::VibCorrSubspaceSolver<param_t>> GetSolverImpl
            (  const std::vector<Uin>& arNModals
            ,  const OpDef& arOpDef
            ,  const ModalIntegrals<param_t>& arModInts
            ,  const ModeCombiOpRange& arMcr
            )  const override
         {
            return std::make_unique<midas::vcc::subspacesolver::TradVccSolver<param_t>>(arNModals, arOpDef, arModInts, arMcr);
         }

         std::vector<vec_t> ResidualVector
            (  const std::vector<vec_t>& arSolVecs
            ,  const std::vector<Uin>& arNModals
            ,  const OpDef& arOpDef
            ,  const ModalIntegrals<param_t>& arModInts
            ,  const ModeCombiOpRange& arMcr
            )  const override
         {
            midas::tdvcc::TrfTimTdvccMatRep<param_t> trf(arNModals, arOpDef, arModInts, arMcr);
            const auto& t_ampls = arSolVecs.at(0);
            const auto& l_coefs = arSolVecs.at(1);
            auto trf_a = trf.ErrVec(0., t_ampls);
            auto trf_b = trf.EtaVec(0., t_ampls);
            trf_b += trf.LJac(0., t_ampls, l_coefs);
            if (trf_a.Size() > 0 && trf_b.Size() > 0)
            {
               trf_a[0] = 0;
               trf_b[0] = 0;
            }
            return {trf_a, trf_b};
         }

         Uin CtrlNIter
            (  Uin aMaxExci
            ,  bool aCropNotDiis
            ,  bool aDiagPrecon
            )  const override
         {
            // MBH, Jan 2020: Add values for aDiagPrecon = false if ever
            // getting convergence with that one.
            if (aCropNotDiis)
            {
               if (aMaxExci == 3)      return 29;
               else if(aMaxExci == 2)  return 27;
               else                    return 0;
            }
            else
            {
               if (aMaxExci == 3)      return 32;
               else if(aMaxExci == 2)  return 29;
               else                    return 0;
            }
         }

   };

} /* namespace midas::test::matrepsolver */

#endif/*MATREPSOLVERSTEST_H_INCLUDED*/
