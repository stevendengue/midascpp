#include "test/Spline2DTestCase.h"

namespace midas::test
{

void Spline2DTest()
{
   cutee::suite suite("Spline2D test");
   
   suite.add_test<Spline2DTestCase>("Spline2D test");
   
   RunSuite(suite);
}

} /* namespace midas::test */
