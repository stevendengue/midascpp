#ifndef PARALTEST_H_INCLUDED
#define PARALTEST_H_INCLUDED

#include "test/CuteeInterface.h"

#include "util/paral/run_async.h"
#include "util/stream/NullStream.h"
#include "util/MidasSystemCaller.h"

namespace midas::test
{

std::mutex os_mutex;
mutex_ostream null_mutex_stream = std::make_pair(std::ref(Mnull), std::ref(os_mutex));

/**
 *
 **/
struct RunAsyncTest: public cutee::test
{
   void run() 
   {
      std::vector<std::string> cmd = {"/bin/ls","-lth"};
      auto f = run_async(cmd,null_mutex_stream);
      UNIT_ASSERT(((f.get().status) == 0),"STATUS WAS NOT 0");
   }
};

/**
 *
 **/
struct RunAsyncFailTest: public cutee::test
{
   void run() 
   {
      std::vector<std::string> cmd = {"/bin/ls","--this-should-fail"}; // no '-e' for ls, this is meant to return an error
      auto f = run_async(cmd,Mlog);
      
      auto ret = f.get();
#ifdef __APPLE__
      UNIT_ASSERT((((ret.status) == 256) || ((ret.status) == 1)), "STATUS WAS NOT 1"); // check that we actually catch the error
#else // not apple
      UNIT_ASSERT((((ret.status) == 512) || ((ret.status) == 2)), "STATUS WAS NOT 2"); // check that we actually catch the error
#endif /* __APPLE__ */
   }
};

} /* namespace midas::test */

#endif /* PARALTEST_H_INCLUDED */
