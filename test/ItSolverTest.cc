#include "test/ItSolverTest.h"

#include "../it_solver/transformer/MidasDataContTransformer.h"

namespace midas::test
{

std::vector<DataCont> ConstructTrials(In num, In size)
{
   std::vector<DataCont> vec(num);
   for(size_t i=0; i<vec.size(); ++i)
   {
      vec[i].SetNewSize(size);
      vec[i].Zero();
      vec[i].DataIo(IO_PUT,i,1.0);
   }
   return vec;
}

//MidasDataContTransformer<> ConstructMidasDataContTransformer(Transformer& t)
MidasDataContTransformer ConstructMidasDataContTransformer(Transformer& t)
{
   //return MidasDataContTransformer<>(t);
   return MidasDataContTransformer{t};
}


void ItSolverTest()
{
   cutee::suite suite("ItSolver test");
   suite.add_test<ReducedGeneralEigenvalueEquationTest>("RGEE");
   //suite.add_test<GeneralEigenTest>("GE");
   RunSuite(suite);
}

} /* namespace midas::test */
