/**
************************************************************************
* 
* @file                RangeTest.h
*
* Created:             02-08-2013
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   
                       
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef RANGETEST_H_INCLUDED
#define RANGETEST_H_INCLUDED

#include <vector>

#include "test/CuteeInterface.h"

// midas headers
#include "util/conversions/Ranges.h"
#include "util/Range.h"
#include "test/check/ElemsInVector.h"
#include "test/check/Compare.h"

namespace midas::test
{

struct StringRangeIntegerTest : public cutee::test
{
   void run() 
   {
      auto range = util::CreateVectorFromRange("[1..5;1]");
      UNIT_ASSERT(check::ElemsInVector(range,std::make_tuple("1","2","3","4","5")),"Expand failed for integer range");
   }
};

struct StringRangeFloatTest : public cutee::test
{
   void run() 
   {
      auto range = util::CreateVectorFromRange("[0.1..0.5;0.1]");
      UNIT_ASSERT(check::ElemsInVector(range,std::make_tuple("0.100000","0.200000","0.300000","0.400000","0.500000")),"Expand failed for float range");
   }
};

struct RangeConstructionTest : public cutee::test
{
   void run() 
   {
      auto range = util::CreateRangeFromString<Nb>("[0.1..0.5;0.1]");
      UNIT_ASSERT(cutee::numeric::float_eq(range.Begin(),0.1),"begin not correct");
      UNIT_ASSERT(cutee::numeric::float_eq(range.End(),0.5)  ,"end not correct");
      UNIT_ASSERT(cutee::numeric::float_eq(range.Step(),0.1) ,"step not correct");
   }
};

struct RangeExpandTest : public cutee::test
{
   void run() 
   {
      auto range = util::CreateRangeFromString<Nb>("[0.1..0.5;0.1]");
      auto range_vec = range.Expand();
      UNIT_ASSERT(check::ElemsInVector(range_vec,std::make_tuple(0.1,0.2,0.3,0.4,0.5),check::FloatCompare(1)),"Expand failed");
   }
};

} /* namespace midas::test */

#endif /* RANGETEST_H_INCLUDED */
