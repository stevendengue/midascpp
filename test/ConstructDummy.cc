#include "test/ConstructDummy.h"

#include <memory>

#include "input/VccCalcDef.h"
#include "vcc/TransformerV3.h"
#include "vcc/v3/IntermediateMachine.h"

namespace midas
{
namespace test
{

/**
 *
 **/
VccCalcDef ConstructDummyVccCaclDef()
{
   VccCalcDef vcd;
   return vcd;
}

/**
 *
 **/
std::shared_ptr<Transformer> ConstructTransformerV3()
{
   std::shared_ptr<Transformer> ptv3(new midas::vcc::TransformerV3(nullptr,nullptr,nullptr,nullptr,0.0));
   return ptv3;
}

} /* namespace test */
} /* namespace midas */
