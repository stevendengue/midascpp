/**
 *******************************************************************************
 * 
 * @file    RandomTest.cc
 * @date    06-02-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "test/RandomTest.h"
#include "util/type_traits/TypeName.h"

namespace midas::test
{
   namespace random_test::detail
   {
      template<typename T>
      void AddRandomTestsImpl
         (  cutee::suite& arSuite
         )
      {
         using real_t = midas::type_traits::RealTypeT<T>;
         constexpr bool is_float = std::is_floating_point_v<real_t>;
         std::string s = "("+midas::type_traits::TypeName<T>()+") ";
         arSuite.add_test<RandomNumberTest<T>>(s+"RandomNumberTest");
         if constexpr(is_float)
         {
            arSuite.add_test<RandomStdVectorTest<T>>(s+"RandomStdVectorTest");
            arSuite.add_test<RandomMidasVectorTest<T>>(s+"RandomMidasVectorTest");
            arSuite.add_test<RandomMidasMatrixTest<T>>(s+"RandomMidasMatrixTest");
            arSuite.add_test<RandomHermitianMidasMatrixTest<T>>(s+"RandomHermitianMidasMatrixTest");
            arSuite.add_test<RandomAntiHermitianMidasMatrixTest<T>>(s+"RandomAntiHermitianMidasMatrixTest");
            arSuite.add_test<RandomDataContTest<T>>(s+"RandomDataContTest");
            arSuite.add_test<LoadRandomTest<T>>(s+"LoadRandomTest");
         }
      }
   } /* namespace random_test::detail */


   /************************************************************************//**
    * @brief
    *    The driver for these unit tests.
    ***************************************************************************/
   void RandomTest()
   {
      // Create test_suite object.
      cutee::suite suite("Random test");

      // Add all the tests to the suite.
      using random_test::detail::AddRandomTestsImpl;
      AddRandomTestsImpl<bool>(suite);
      AddRandomTestsImpl<short>(suite);
      AddRandomTestsImpl<int>(suite);
      AddRandomTestsImpl<long>(suite);
      AddRandomTestsImpl<unsigned short>(suite);
      AddRandomTestsImpl<unsigned int>(suite);
      AddRandomTestsImpl<unsigned long>(suite);
      AddRandomTestsImpl<float>(suite);
      AddRandomTestsImpl<double>(suite);
      AddRandomTestsImpl<std::complex<float>>(suite);
      AddRandomTestsImpl<std::complex<double>>(suite);
      // NB! 'long double' doesn't work with cutee's float_eq (cannot convert
      // to proper integral type for ULPs comparison). -MBH, Feb 2020.

      // Run the tests.
      RunSuite(suite);
   }
} /* namespace midas::test */
