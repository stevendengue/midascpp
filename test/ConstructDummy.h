#ifndef CONSTRUCTDUMMY_H_INCLUDED
#define CONSTRUCTDUMMY_H_INCLUDED

#include <memory>
#include "input/VccCalcDef.h"
#include "vcc/TransformerV3.h"

namespace midas
{
namespace test
{

VccCalcDef ConstructDummyVccCaclDef();
std::shared_ptr<Transformer> ConstructTransformerV3();

} /* namespace test */
} /* namespace midas */

#endif /* CONSTRUCTDUMMY_H_INCLUDED */
