#ifndef ILAPACKTEST_H_INCLUDED
#define ILAPACKTEST_H_INCLUDED

#include "test/CuteeInterface.h"

#include "libmda/numeric/float_eq.h"

#include "test/RandomMatrix.h"
#include "test/check/MatricesAreEqual.h"
#include "lapack_interface/ILapack.h"
#include "lapack_interface/ILapackUtils.h"
#include "lapack_interface/LapackInterface.h"
#include "lapack_interface/GELSS.h"
#include "test/Random.h"

namespace midas::test
{
namespace detail
{

template<class T>
bool CheckEigenvectorBiorthonormality(const Eigenvalue_ext_struct<T>& eig_sol, cutee::numeric::integer_type<T> ulps = 4)
{
   bool imag_i = false;
   for(int i=0; i<eig_sol.num_eigval_; ++i)
   {
      for(int j=0; j<eig_sol.num_eigval_; ++j)
      {
         if(!eig_sol.im_eigenvalues_[i] && !eig_sol.im_eigenvalues_[j])
         {
            // not complex
            T dot = 0.0;
            
            for(int k=0; k<eig_sol.n_; ++k)
            {
               dot += eig_sol.lhs_eigenvectors_[i][k]*eig_sol.rhs_eigenvectors_[j][k];
            }

            
            if( (i==j ? cutee::numeric::float_neq(dot,T(1.0), ulps) : !cutee::numeric::float_numeq_zero(dot,T(1.0), ulps)) )
            {
               std::cout << i << " " << j << " " << dot << std::endl;
               return false;
            }
         }
         else if(!eig_sol.im_eigenvalues_[j])
         {
            assert(i!=j);

            // lhs complex pair
            T re_dot = 0.0;
            T im_dot = 0.0;

            // check i,j
            for(int k=0; k<eig_sol.n_; ++k)
            {
               re_dot += eig_sol.lhs_eigenvectors_[i][k]*eig_sol.rhs_eigenvectors_[j][k];
               im_dot += eig_sol.lhs_eigenvectors_[i+1][k]*eig_sol.rhs_eigenvectors_[j][k];
            }
            if( (!cutee::numeric::float_numeq_zero(re_dot,T(1.0),ulps)) 
             || (!cutee::numeric::float_numeq_zero(im_dot,T(1.0),ulps)) )
            {
               std::cout << i << " " << j << std::endl;
               std::cout << " check 2 " << re_dot << " " << im_dot << std::endl;
               return false;
            }
            // check i+1,j (checked in above)

            imag_i = true;
         }
         else if(!eig_sol.im_eigenvalues_[i])
         {
            assert(i!=j);
            // rhs complex pair
            T re_dot = 0.0;
            T im_dot = 0.0;

            // check i,j
            for(int k=0; k<eig_sol.n_; ++k)
            {
               re_dot += eig_sol.lhs_eigenvectors_[i][k]*eig_sol.rhs_eigenvectors_[j][k];
               im_dot += eig_sol.lhs_eigenvectors_[i][k]*eig_sol.rhs_eigenvectors_[j+1][k];
            }
            if( (!cutee::numeric::float_numeq_zero(re_dot,T(1.0),ulps)) 
             || (!cutee::numeric::float_numeq_zero(im_dot,T(1.0),ulps)) )
            {
               std::cout << "check 3 " << re_dot << " " << im_dot << std::endl;
               return false;
            }

            // check i,j+1 (checked in above)

            ++j;
         }
         else
         {
            // both complex pair
            T re_dot = 0.0;
            T im_dot = 0.0;
            
            // check i,j
            for(int k=0; k<eig_sol.n_; ++k)
            {
               re_dot += eig_sol.lhs_eigenvectors_[i][k]*eig_sol.rhs_eigenvectors_[j][k] - eig_sol.lhs_eigenvectors_[i+1][k]*eig_sol.rhs_eigenvectors_[j+1][k];
               im_dot += eig_sol.lhs_eigenvectors_[i][k]*eig_sol.rhs_eigenvectors_[j+1][k] + eig_sol.lhs_eigenvectors_[i+1][k]*eig_sol.rhs_eigenvectors_[j][k];
            }
            if( (i==j ? ( !cutee::numeric::float_eq(re_dot,T(1.0), ulps) || !cutee::numeric::float_numeq_zero(im_dot,T(1.0), ulps) )
                      : ( !cutee::numeric::float_numeq_zero(re_dot,T(1.0), ulps) || !cutee::numeric::float_numeq_zero(im_dot,T(1.0), ulps) )
                )
              )
            {
               std::cout << "check 4 " << re_dot << " " << im_dot << std::endl;
               return false;
            }
            re_dot = 0.0;
            im_dot = 0.0;
            // check i,j+1
            for(int k=0; k<eig_sol.n_; ++k)
            {
               re_dot   += eig_sol.lhs_eigenvectors_[i][k] * eig_sol.rhs_eigenvectors_[j][k]   
                        +  eig_sol.lhs_eigenvectors_[i+1][k] * eig_sol.rhs_eigenvectors_[j+1][k];
               im_dot   += -eig_sol.lhs_eigenvectors_[i][k] * eig_sol.rhs_eigenvectors_[j+1][k] 
                        +   eig_sol.lhs_eigenvectors_[i+1][k] * eig_sol.rhs_eigenvectors_[j][k];
            }
            if(!cutee::numeric::float_numeq_zero(re_dot,T(1.0), ulps) || !cutee::numeric::float_numeq_zero(im_dot,T(1.0), ulps))
            {
               std::cout << " check 5 " << re_dot << " " << im_dot << std::endl;
               return false;
            }

            imag_i = true;
            ++j;
         }
      }
      if(imag_i)
      {
         ++i;
      }
      imag_i = false;
   }
   return true;
}

} /* namespace detail */


template<class T>
struct ColumnComplexTestBase: public cutee::test
{
   void fill_random(T* t, size_t size) const
   {
      midas::test::random::LoadRandom(t,size);
      // Shift from range [-1;1] -> [0;1].
      static_assert(std::is_floating_point_v<T>, "Not impl. for complex atm (imag part not handled).");
      for(size_t i = 0; i < size; ++i)
      {
         t[i] += T(1);
         t[i] /= T(2);
      }
   }

   std::unique_ptr<T[]> construct_data(size_t col_size, size_t n_col) const
   {
      auto size = n_col*col_size;
      std::unique_ptr<T[]> data(new T[size]);
      fill_random(data.get(),size);
      return data;
   }

   std::unique_ptr<bool[]> construct_imagbool(size_t n_col) const
   {
      std::unique_ptr<bool[]> imag(new bool[n_col]);
      for(size_t i=0; i<n_col; ++i)
      {
         imag[i] = false;
      }
      return imag;
   }

   std::unique_ptr<bool[]> construct_imagbool_random(size_t n_col) const
   {
      std::unique_ptr<bool[]> imag = construct_imagbool(n_col);
      for(size_t i=0; i<n_col-1; ++i)
      {
         if(midas::test::random::RandomBool())
         {  // not complex
         }
         else
         {  // complex pair
            imag[i] = true;
            imag[i+1] = true;
            ++i;
         }
      }
      return imag;
   }

   std::unique_ptr<T[]> construct_imag_random(size_t n_col) const
   {
      std::unique_ptr<T[]> imag(new T[n_col]);
      for(size_t i=0; i<n_col; ++i)
      {
         imag[i] = 0.0;
      }
      for(size_t i=0; i<n_col-1; ++i)
      {
         if(midas::test::random::RandomBool())
         { // not complex
         }
         else
         {
            imag[i] = midas::test::random::RandomNumber<T>();
            imag[i+1] = -imag[i];
            ++i;
         }
      }
      return imag;
   }

   void copy_data(const std::unique_ptr<T[]>& in, std::unique_ptr<T[]>& out, size_t size) const
   {
      for(size_t i=0; i<size; ++i)
      {
         out[i] = in[i];
      }
   }

   static void check_columns_binormalized(T* l, T* r, size_t n, size_t m, Uin prec = 4)
   {
      for(size_t i = 0, idx = 0; i < m; ++i, idx += n)
      {
         T dot = ilapack::util::column_dot_offset(l,r,n,idx);
         UNIT_ASSERT_FEQUAL_PREC(dot, 1.0, prec, "columns not binormalized");
      }
   }

};

////////////////////////////////////////////////////////////////////////////////////////
//
// Actual tests
//
//
////////////////////////////////////////////////////////////////////////////////////////
template<class T>
struct ComplexConjugateTest: public cutee::test
{
   void run() 
   {
      T re = 2.3; T im = 1.7;
      T re_conj; T im_conj;

      libmda::util::ret(re_conj,im_conj) = ilapack::util::complex_conjugate(re,im);
      
      UNIT_ASSERT_FEQUAL_PREC(re_conj, re,2,"Real part not correct");
      UNIT_ASSERT_FEQUAL_PREC(im_conj,-im,2,"Imag part not correct");
   }
};

template<class T>
struct ComplexSqrtTest: public cutee::test
{
   T complex_squared_re(T re, T im)
   {
      return re*re - im*im;
   }
   
   T complex_squared_im(T re, T im)
   {
      return 2*re*im;
   }

   void run() 
   {
      T re = 2.3; T im = 1.7;
      T re_sqrt; T im_sqrt;

      libmda::util::ret(re_sqrt,im_sqrt) = ilapack::util::complex_sqrt(re,im);
      
      T re_new = complex_squared_re(re_sqrt,im_sqrt);
      T im_new = complex_squared_im(re_sqrt,im_sqrt);
   
      UNIT_ASSERT_FEQUAL_PREC(re_new,re,2,"Real part not correct");
      UNIT_ASSERT_FEQUAL_PREC(im_new,im,2,"Imag part not correct");
   }
};

template<class T>
struct ColumnDotTest: public cutee::test
{
   int size = 3;

   T internal_dot_function(T* col1, T* col2)
   {
      T dot = 0.0;
      for(size_t i=0; i<size; ++i)
      {
         dot += col1[i]*col2[i];
      }
      return dot;
   }

   void run() 
   {
      std::unique_ptr<T[]> col1(new T[size]);
      std::unique_ptr<T[]> col2(new T[size]);

      midas::test::random::LoadRandom(col1.get(),size);
      midas::test::random::LoadRandom(col2.get(),size);

      auto dot = ilapack::util::column_dot_offset(col1.get(),col2.get(),size,0);
      auto dot_internal = internal_dot_function(col1.get(),col2.get());

      UNIT_ASSERT_FEQUAL_PREC(dot,dot_internal,4,"Dot not correct");
   }
};

template<class T>
struct ColumnDot2Test: public ColumnComplexTestBase<T>
{
   size_t n_cols = 3;
   size_t col_sizes = 5;

   size_t lhs_col = 0; size_t lhs_col_idx = col_sizes*lhs_col;
   size_t rhs_col = 1; size_t rhs_col_idx = col_sizes*rhs_col;

   T internal_dot_function(T* col1, T* col2)
   {
      T dot = 0.0;
      size_t lhs_idx = lhs_col_idx;
      size_t rhs_idx = rhs_col_idx;
      for(size_t i=0; i<col_sizes; ++i)
      {
         dot += col1[lhs_idx]*col2[rhs_idx];
         ++lhs_idx;
         ++rhs_idx;
      }
      return dot;
   }

   void run() 
   {
      auto data_lhs = this->construct_data(col_sizes,n_cols);
      auto data_rhs = this->construct_data(col_sizes,n_cols);

      auto dot = ilapack::util::column_dot_offset2(data_lhs.get(),data_rhs.get(),col_sizes,lhs_col_idx,rhs_col_idx);
      auto dot_internal = internal_dot_function(data_lhs.get(),data_rhs.get());

      UNIT_ASSERT_FEQUAL_PREC(dot,dot_internal,4,"dot2 not correct");
   }
};

template<class T>
struct ColumnComplexConjugateTest: public ColumnComplexTestBase<T> //public cutee::test
{
   size_t n_cols = 3;
   size_t col_sizes = 5;
   
   template<class U>
   bool check_columns_complex_conjugate(T* res, T* check, U* imag, size_t n, size_t m) const
   {
      size_t idx = 0;
      for(size_t i=0; i<m; ++i)
      {
         if(imag[i])
         {  // complex pair
            size_t idx_im = idx + n;

            for(size_t j=0; j<n; ++j)
            {
               if(res[idx]!=check[idx])
               {
                  return false;
               }
               if(res[idx_im]!=-check[idx_im])
               {
                  return false;
               }
               ++idx;
               ++idx_im;
            }

            idx += n;
            ++i;
         }
         else
         { // not complex
            for(size_t j=0; j<n; ++j)
            {
               if(res[idx]!=check[idx])
               {
                  return false;
               }
               ++idx;
            }
         }
      }
      return true;
   }

   void run() 
   {
      std::unique_ptr<T[]> col1 = this->construct_data(col_sizes,n_cols);
      std::unique_ptr<T[]> check(new T[col_sizes*n_cols]);
      this->copy_data(col1,check,col_sizes*n_cols);

      auto imag = this->construct_imag_random(n_cols);
      
      ilapack::util::columns_complex_conjugate(col1.get(),imag.get(),col_sizes,n_cols);

      UNIT_ASSERT(check_columns_complex_conjugate(col1.get(),check.get(),imag.get(),col_sizes,n_cols),"column complex conjugate failed");
   }
};

template<class T>
struct ColumnNormalizeTest: public ColumnComplexTestBase<T>
{
   size_t n_col = 4;
   size_t col_size = 8;
   
   bool check_normalized_columns(T* t, size_t n, size_t m) const
   {
      size_t idx = 0;
      for(size_t i=0; i<m; ++i)
      {
         T dot = ilapack::util::column_dot_offset(t,t,n,idx);
         if(cutee::numeric::float_neq(dot,1.0,8))
         {
            return false;
         }
         idx+=n;
      }
      return true;
   }

   void run() 
   {
      auto data = this->construct_data(col_size,n_col);
      ilapack::util::normalize_columns(data.get(),col_size,n_col);

      UNIT_ASSERT(check_normalized_columns(data.get(),col_size,n_col)," columns not normalized");
   }
};

template<class T>
struct ColumnComplexNormalizeTest: public ColumnComplexTestBase<T>
{
   size_t n_col = 3;
   size_t col_size = 5;

   template<class U>
   bool check_columns_normalized_complex(T* t, U* u, size_t n, size_t m)
   {
      size_t idx = 0;
      for(size_t i =0; i<m; ++i)
      {
         if(!u[i])
         { // not complex
            T dot = ilapack::util::column_dot_offset(t,t,n,idx);
            if(cutee::numeric::float_neq(dot,1.0,8))
            {
               std::cout << " dot : " << dot << std::endl;
               return false;
            }
            idx+=n;
         }
         else
         { // complex pair
            size_t idx_im = idx + n;
            T re_dot = ilapack::util::column_dot_offset(t,t,n,idx) + ilapack::util::column_dot_offset(t,t,n,idx_im);
            
            if(cutee::numeric::float_neq(re_dot,1.0,8))
            {
               std::cout << " re_dot : " << re_dot << std::endl;
               return false;
            }

            idx += 2*n;
            ++i;  
         }
      }
      return true;
   }

   void run() 
   {
      auto data = this->construct_data(col_size,n_col);
      auto imag = this->construct_imag_random(n_col);

      ilapack::util::normalize_columns_complex(data.get(),imag.get(),col_size,n_col);

      UNIT_ASSERT(check_columns_normalized_complex(data.get(),imag.get(),col_size,n_col),"columns not normalized");
   }
};

template<class T>
struct ColumnUnbalancedLhsBinormalizeTest: public ColumnComplexTestBase<T>
{
   size_t n_col = 3;
   size_t col_size = 5;

   void run() 
   {
      auto data_rhs = this->construct_data(col_size,n_col);
      auto data_lhs = this->construct_data(col_size,n_col);
      auto imag = this->construct_imagbool(n_col); // filled with false

      ilapack::util::binormalize_columns_unbalanced(data_lhs.get(),data_rhs.get(),imag.get(),col_size,n_col);

      this->check_columns_binormalized(data_lhs.get(),data_rhs.get(),col_size,n_col);
   }
};

template<class T>
struct ColumnUnbalancedRhsBinormalizeTest: public ColumnComplexTestBase<T>
{
   size_t n_col = 3;
   size_t col_size = 5;

   void run() 
   {
      auto data_rhs = this->construct_data(col_size,n_col);
      auto data_lhs = this->construct_data(col_size,n_col);
      auto imag = this->construct_imagbool(n_col); // filled with false

      ilapack::util::binormalize_columns_unbalanced(data_rhs.get(),data_lhs.get(),imag.get(),col_size,n_col);

      this->check_columns_binormalized(data_lhs.get(),data_rhs.get(),col_size,n_col);
   }
};

template<class T>
struct ColumnComplexBinormalizationTestBase: public ColumnComplexTestBase<T>
{
   template<class U>
   bool check_columns_binormalized_complex(T* l, T* r, U* u, size_t n, size_t m)
   {
      size_t idx = 0;
      for(size_t i =0; i<m; ++i)
      { // complex pair
         if(!u[i])
         {
            T dot = ilapack::util::column_dot_offset(l,r,n,idx);
            if(cutee::numeric::float_neq(dot,1.0,8))
            {
               std::cout << " dot : " << dot << std::endl;
               return false;
            }
            idx+=n;
         }
         else
         { // complex pair
            size_t idx_im = idx + n;
            T re_dot = ilapack::util::column_dot_offset(l,r,n,idx) - ilapack::util::column_dot_offset(l,r,n,idx_im);
            T im_dot = ilapack::util::column_dot_offset2(l,r,n,idx,idx_im) + ilapack::util::column_dot_offset2(l,r,n,idx_im,idx);
            if( cutee::numeric::float_neq(re_dot,1.0,8) ) 
            {
               std::cout << " re dot not unit " << std::endl;
               std::cout << " re_dot : " << re_dot << std::endl;
               return false;
            }
            if( !cutee::numeric::float_numeq_zero(im_dot,1.0,8) )
            {  
               std::cout << " im dot not zero " << std::endl;
               std::cout << " im_dot : " << im_dot << std::endl;
               return false;
            }
            
            idx+=2*n;
            ++i;
         }
      }
      return true;
   }
};

template<class T>
struct ColumnUnbalancedLhsComplexBinormalizeTest: public ColumnComplexBinormalizationTestBase<T>
{
   size_t n_col = 3;
   size_t col_size = 5;

   void run() 
   {
      auto data_rhs = this->construct_data(col_size,n_col);
      auto data_lhs = this->construct_data(col_size,n_col);
      auto imag = this->construct_imagbool_random(n_col); // filled with false

      ilapack::util::binormalize_columns_unbalanced(data_lhs.get(),data_rhs.get(),imag.get(),col_size,n_col);

      UNIT_ASSERT(this->check_columns_binormalized_complex(data_lhs.get(),data_rhs.get(),imag.get(),col_size,n_col),"columns not normalized");
   }
};

template<class T>
struct ColumnUnbalancedRhsComplexBinormalizeTest: public ColumnComplexBinormalizationTestBase<T>
{
   size_t n_col = 3;
   size_t col_size = 5;

   void run() 
   {
      auto data_rhs = this->construct_data(col_size,n_col);
      auto data_lhs = this->construct_data(col_size,n_col);
      auto imag = this->construct_imagbool_random(n_col); // filled with false

      ilapack::util::binormalize_columns_unbalanced(data_rhs.get(),data_lhs.get(),imag.get(),col_size,n_col);

      UNIT_ASSERT(this->check_columns_binormalized_complex(data_lhs.get(),data_rhs.get(),imag.get(),col_size,n_col),"columns not normalized");
   }
};

template<class T>
struct ColumnBalancedComplexBinormalizeTest: public ColumnComplexBinormalizationTestBase<T>
{
   size_t n_col = 3;
   size_t col_size = 5;

   void run() 
   {
      auto data_rhs = this->construct_data(col_size,n_col);
      auto data_lhs = this->construct_data(col_size,n_col);
      auto imag = this->construct_imagbool_random(n_col); // filled with false

      ilapack::util::binormalize_columns_balanced(data_lhs.get(),data_rhs.get(),imag.get(),col_size,n_col);

      UNIT_ASSERT(this->check_columns_binormalized_complex(data_lhs.get(),data_rhs.get(),imag.get(),col_size,n_col),"columns not normalized");
   }
};

/***************************************************************************//**
 * The individual subtests of this test
 * - constructs a matrix M
 * - constructs a direct copy C of M
 * - compares the result of M^T*M (passing the same M pointer to GEMM) and
 *   M^T*C (passing different points, but to the same date)
 *
 * Since the data is exactly the same in both cases (this is also checked), the
 * results must be the same. However, for some implementations of GEMM it has
 * been observed that M^T*M and M^T*C can differ with about 1 ULP (unit of
 * least precision), which can again cause other tests to fail.
 *
 * Thus this test acts a test of the GEMM implementation (MKL/LAPACK/etc.
 * installation). Netlib LAPACK version 3.6.0 or newer should pass this test,
 * so in case of the native GEMM impl. failing, it is worth trying configuring
 * --enable-lapack=3.6.0, recompiling and retesting.
 *
 * @note
 *    The data/matrices used in this test have all been shown to cause a failure
 *    for the native GEMM implementation on MacOS 10.13.6, but of course the
 *    list is not exhaustive, and you're welcome to add more cases if finding
 *    any of relevance.
 ******************************************************************************/
struct GEMMSelfCopy
   :  public cutee::test
{
   using T = double;

   /**
    * Function to do some of the argument handling before calling GEMM.
    * Exploits that we're doing a M^T * M, so some number of rows/cols can
    * be reused.
    * For the record GEMM is
    *     C = alpha*op(A)*op(B) + beta*C
    * where op(X) = X or X^T, depending on the arguments to GEMM. Here we have
    * op(A) = A^T and op(B) = B.
    **/
   void call_gemm
      (  int num_rows
      ,  int num_cols
      ,  T* mat_A
      ,  T* mat_B
      ,  T* result_C
      )
   {
      // Common GEMM settings for these sub-tests.
      T alpha = 1.0;
      T beta  = 0.0;
      char tc = 'T';
      char nc = 'N';

      // Extra GEMM settings depending on matrix dimensions.
      int lda = num_rows;
      int ldb = num_rows;
      int ldc = num_cols;
      midas::lapack_interface::gemm
         (  &tc         // First  matrix transposed or not?
         ,  &nc         // Second matrix transposed or not?
         ,  &num_cols   // Num. rows, first matrix.
         ,  &num_cols   // Num. cols, second matrix.
         ,  &num_rows   // Num. cols, first matrix = num. rows second matrix.
         ,  &alpha      // The GEMM alpha: C = alpha*op(A)*op(B) + beta*C
         ,  mat_A       // First matrix A.
         ,  &lda        // First dimension of A, max(1,k) because transposed 'T'.
         ,  mat_B       // Second matrix B.
         ,  &ldb        // First dimension of B, max(1,k) because not-transposed 'N'.
         ,  &beta       // The GEMM beta:  C = alpha*op(A)*op(B) + beta*C
         ,  result_C    // Result matrix C.
         ,  &ldc        // First dimension of C, max(1,m).
         );
   }

   /**
    * @param[in] num_rows
    *    Number of rows of matrix.
    * @param[in] num_cols
    *    Number of columns matrix.
    * @param[in] matrix
    *    Pointer to data of matrix, column major; must have size num_rows * num_cols.
    * @param[in] iteration
    *    The iteration number within this unit test, just for outputting which
    *    one failed, if any.
    **/
   void do_subtest
      (  int num_rows
      ,  int num_cols
      ,  T* matrix
      ,  int iteration
      )
   {
      // Some helpful information in case of failure.
      std::stringstream err;
      err   << "This can't fail due to numerical inaccuracies. (See documentation for details.)\n"
            << "Thus, if it fails, it may indicate a problem with your GEMM implementation\n"
            << "(i.e. your MKL/LAPACK/etc. installation).\n"
            << "Try reconfiguring with --enable-lapack=3.6.0 (or newer), recompiling, then\n"
            << "retesting to see if that solves the issue.\n"
            ;


      // Calculate sizes.
      int size = num_rows * num_cols;
      int result_size = num_cols * num_cols;

      // Calculate mat^T * mat.
      T mat_T_mat[result_size];
      call_gemm(num_rows, num_cols, matrix, matrix, mat_T_mat);

      // Copy matrix, then compute mat^T * copy.
      T copy[size];
      for(unsigned int i = 0; i < size; ++i)
      {
         copy[i] = matrix[i];
      }
      T mat_T_copy[result_size];
      call_gemm(num_rows, num_cols, matrix, copy, mat_T_copy);

      // Check that original matrix and copy were exactly equal.
      for(unsigned int i = 0; i < size; ++i)
      {
         UNIT_ASSERT_EQUAL
            (  matrix[i]
            ,  copy[i]
            ,  "Error, mat != copy, subtest = "+std::to_string(iteration)
               +  ", i = "+std::to_string(i)+".\n"
               +  err.str()
            );
      }

      // Check that the two results are exactly equal.
      for(unsigned int i = 0; i < result_size; ++i)
      {
         UNIT_ASSERT_EQUAL
            (  mat_T_mat[i]
            ,  mat_T_copy[i]
            ,  "Error, mat^T*mat != mat^T*copy, subtest = "+std::to_string(iteration)
               +  ", i = "+std::to_string(i)+".\n"
               +  err.str()
            );
      }
   }

   void run() override
   {
      int iteration = 0;

      // (2 x 7) matrix.
      {
         constexpr int n_rows = 2;
         constexpr int n_cols = 7;
         constexpr int size = n_rows * n_cols;
         T mat[size] =
            {  +9.5628839776764818e-01
            ,  -5.9008011631436652e-01
            ,  -8.5275697890966129e-01
            ,  -7.5308249372827873e-01
            ,  -9.5254904449743893e-01
            ,  -1.1550869911804529e-01
            ,  -9.2341456793328158e-01
            ,  -7.4254903936352323e-01
            ,  +7.3716174991269634e-01
            ,  +5.5773210934310513e-01
            ,  -1.6456426800334922e-01
            ,  -5.3203867551219619e-01
            ,  +5.3373801650799302e-01
            ,  -9.7698001888975972e-01
            };
         do_subtest(n_rows, n_cols, mat, iteration++);
      }

      // (4 x 7) matrix.
      {
         constexpr int n_rows = 4;
         constexpr int n_cols = 7;
         constexpr int size = n_rows * n_cols;
         T mat[size] =
            {  -9.6948807732423026e-01
            ,  +4.8597611381811467e-01
            ,  +9.4861179914991656e-01
            ,  -8.9795801367097483e-01
            ,  +9.5196408451347714e-01
            ,  +6.3239219380365475e-02
            ,  -6.5654223101598741e-02
            ,  +1.8468414670431255e-02
            ,  -9.4953854241164837e-01
            ,  -4.2916661068544881e-01
            ,  -8.7542969730576259e-01
            ,  -5.7424974617880065e-01
            ,  -5.5175669760251345e-01
            ,  +3.8245191573688109e-02
            ,  +4.1921314397342813e-01
            ,  -7.9691875046047356e-02
            ,  +1.7796782757573948e-01
            ,  +8.7834918216763747e-01
            ,  -3.4672431772265688e-01
            ,  +8.8575281246699222e-01
            ,  -7.3767206299530153e-01
            ,  -9.7925362130144000e-01
            ,  -7.7607928676905091e-01
            ,  +8.1090334239669692e-01
            ,  -4.3532633162926093e-01
            ,  +4.6527268073178663e-01
            ,  -4.6687936118498430e-01
            ,  +8.6994214702163397e-02
            };
         do_subtest(n_rows, n_cols, mat, iteration++);
      }

      // (4 x 7) matrix.
      {
         constexpr int n_rows = 4;
         constexpr int n_cols = 7;
         constexpr int size = n_rows * n_cols;
         T mat[size] =
            {  +1.4058841628501906e-02
            ,  -7.7612074179950186e-01
            ,  +7.0488696165962317e-01
            ,  -6.5338710966831703e-01
            ,  -3.7946921898505404e-01
            ,  -8.2918616659687516e-01
            ,  +9.5050073693285242e-01
            ,  -5.2683309873725126e-01
            ,  -2.1139732403946043e-01
            ,  +5.1037603977843560e-01
            ,  -5.9282783199866018e-02
            ,  -5.1760854653958432e-01
            ,  -3.5222687324328039e-01
            ,  -1.0147318269626082e-01
            ,  +2.6334752544372986e-01
            ,  -1.2156137244812248e-01
            ,  +8.3730021743972260e-02
            ,  +6.7743350231960275e-01
            ,  -8.2299273270717654e-01
            ,  +8.2918936103330676e-01
            ,  +3.7765972022378347e-01
            ,  -2.1169049367580806e-01
            ,  +7.4113105743684127e-01
            ,  +1.5310214952405121e-01
            ,  -9.8564224829562996e-01
            ,  -9.0583133183089815e-01
            ,  -7.4203628109349784e-01
            ,  -6.8743875010112276e-01
            };
         do_subtest(n_rows, n_cols, mat, iteration++);
      }

      // (2 x 6) matrix.
      {
         constexpr int n_rows = 2;
         constexpr int n_cols = 6;
         constexpr int size = n_rows * n_cols;
         T mat[size] =
            {  -3.5995185779406502e-01
            ,  -1.1233960863024450e-01
            ,  -8.5564525375506961e-01
            ,  -8.8964905307852871e-01
            ,  -2.8650429688545509e-01
            ,  -2.6547997567464598e-01
            ,  -2.5307486701129811e-01
            ,  -5.7848505898809177e-01
            ,  -4.0350463483564192e-02
            ,  -9.1983519352968668e-01
            ,  -5.4976072012208421e-01
            ,  -9.5267497607336260e-01
            };
         do_subtest(n_rows, n_cols, mat, iteration++);
      }

      // (3 x 6) matrix.
      {
         constexpr int n_rows = 3;
         constexpr int n_cols = 6;
         constexpr int size = n_rows * n_cols;
         T mat[size] =
            {  -9.1712631888620699e-01
            ,  -5.5090675841805270e-02
            ,  -6.8028213309130681e-01
            ,  -4.6154555107037198e-01
            ,  -3.0622167356922791e-01
            ,  -9.6437537389909278e-01
            ,  -7.5751279894204648e-01
            ,  -1.7251690923527740e-01
            ,  -6.2797193686197783e-01
            ,  -5.1312092773456142e-01
            ,  -7.0312808726521392e-01
            ,  -3.7599349519610259e-01
            ,  -7.2781415905985414e-02
            ,  -4.9349224835017047e-01
            ,  -3.9251933884632761e-01
            ,  -9.1868695521696631e-01
            ,  -8.2866885415945868e-01
            ,  -7.8650675499497558e-02
            };
         do_subtest(n_rows, n_cols, mat, iteration++);
      }

      // (3 x 6) matrix.
      {
         constexpr int n_rows = 3;
         constexpr int n_cols = 6;
         constexpr int size = n_rows * n_cols;
         T mat[size] =
            {  -8.9406452791161473e-01
            ,  -2.7558280394309742e-02
            ,  -2.7516753582171338e-01
            ,  -6.5681084796246392e-01
            ,  -1.4074417067243328e-02
            ,  -9.3787729971527056e-01
            ,  -2.3482220229572204e-01
            ,  -7.1973136875772226e-02
            ,  -5.0899460574355782e-01
            ,  -6.0621479470427486e-01
            ,  -2.3583820956662260e-01
            ,  -8.4016984976832054e-01
            ,  -4.3015541961187387e-01
            ,  -8.9916358327007939e-01
            ,  -1.9108693375975982e-01
            ,  -5.4276668502547931e-01
            ,  -2.0782357063326606e-01
            ,  -7.5330196035869934e-01
            };
         do_subtest(n_rows, n_cols, mat, iteration++);
      }
   }
};


template<class T>
struct GEEVBiOrthoTest: public cutee::test
{
   void run() 
   {
      auto eig_sol = GEEV(midas::test::NonHermitianMatrix<T>(3));
      NormalizeEigenvectors(eig_sol,ilapack::binormalize_rhs);
      
      UNIT_ASSERT(detail::CheckEigenvectorBiorthonormality(eig_sol, 10),"Eigenvectors are not bi-orthonormal");
   }
};

/**
 *
 **/
template<class T>
struct GESVDTest: public cutee::test
{
   GESVDTest()
   {
   }

   void run() 
   {
      auto mata = midas::test::RandomFixedMatrix5x5<T>();
      auto svd = GESVD(mata);
      
      GeneralMidasMatrix<T> mat_recon;
      LoadA(svd,mat_recon);
      
      UNIT_ASSERT(check::MatricesAreEqual(mata,mat_recon, 4096),"NOT EQUAL AFTER GESVD (TYPE: " + libmda::util::type_of(T()) +").");
   }
};

/**
 *
 **/
template<class T>
struct GESDDTest: public cutee::test
{
   GESDDTest()
   {
   }

   void run() 
   {
      auto mata = midas::test::RandomFixedMatrix5x5<T>();
      auto svd = GESDD(mata);
      
      GeneralMidasMatrix<T> mat_recon;
      LoadA(svd,mat_recon);
      
      UNIT_ASSERT(check::MatricesAreEqual(mata,mat_recon, 4096),"NOT EQUAL AFTER GESDD (TYPE: " + libmda::util::type_of(T()) +").");
   }
};

/**
 * @note
 *    For real numbers the SVD is generally only unique up to a change of sign
 *    of corresponding columns of U and rows of V^T.  For complex numbers I
 *    guess uniqueness is generally up to a phase, but we haven't observed such
 *    problems for complex numbers, so we'll apply the same
 *    uniqueness-up-to-a-sign to complex as well. Fix it if it ever becomes a
 *    problem.
 *    Thus, the test checks signs between (real parts of) cols. of u_svd and
 *    u_sdd, and apply the flips to both U and V^T before assertions.
 *    It's not strictly an error if they fail, though, both SVDs can still be valid.
 *    See e.g. 
 *    https://software.intel.com/en-us/articles/checking-correctness-of-lapack-svd-eigenvalue-and-one-sided-decomposition-routines
 *    An alternative solution would be to enforce extra criteria in SVD_struct,
 *    such as phase-shifting columns/rows such that 0'th element of U is always
 *    real-positive or something. -MBH, Mar 2020.
 **/
template<class T>
struct GESVD_GESDDTest: public cutee::test
{
   GESVD_GESDDTest()
   {
   }

   void run() 
   {
      auto mata = midas::test::RandomFixedMatrix5x5<T>();
      auto svd_svd = GESVD(mata);
      auto svd_sdd = GESDD(mata);
      
      // Load U
      GeneralMidasMatrix<T> u_svd; 
      LoadU(svd_svd, u_svd);
      GeneralMidasMatrix<T> u_sdd; 
      LoadU(svd_sdd, u_sdd);
      
      // Load Vt
      GeneralMidasMatrix<T> vt_svd; 
      LoadVt(svd_svd,vt_svd);
      GeneralMidasMatrix<T> vt_sdd; 
      LoadVt(svd_sdd,vt_sdd);

      // Load S
      GeneralMidasVector<typename decltype(svd_svd)::s_type> s_svd; 
      LoadS(svd_svd,s_svd);
      GeneralMidasVector<typename decltype(svd_svd)::s_type> s_sdd; 
      LoadS(svd_sdd,s_sdd);
      
      // Cross-SVD/SDD comparison of dimensions.
      UNIT_ASSERT_EQUAL(u_svd.Nrows(), u_sdd.Nrows(), "u_svd/u_sdd Nrows()");
      UNIT_ASSERT_EQUAL(u_svd.Ncols(), u_sdd.Ncols(), "u_svd/u_sdd Ncols()");
      UNIT_ASSERT_EQUAL(vt_svd.Nrows(), vt_sdd.Nrows(), "vt_svd/vt_sdd Nrows()");
      UNIT_ASSERT_EQUAL(vt_svd.Ncols(), vt_sdd.Ncols(), "vt_svd/vt_sdd Ncols()");
      UNIT_ASSERT_EQUAL(s_svd.Size(), s_sdd.Size(), "s_svd/s_sdd Size()");

      // Dimensions must fit A = USV^T
      UNIT_ASSERT_EQUAL(mata.Nrows(), u_svd.Nrows(), "A.Nrows() != U.Nrows()");
      UNIT_ASSERT_EQUAL(u_svd.Ncols(), s_svd.Size(), "U.Ncols() != S.Size()");
      UNIT_ASSERT_EQUAL(s_svd.Size(), vt_svd.Ncols(), "S.Size() != Vt.Ncols()");
      UNIT_ASSERT_EQUAL(vt_svd.Ncols(), mata.Ncols(), "Vt.Ncols() != A.Ncols()");

      // Flip signs before equality assertions, see note in documentation.
      for(Uin i = 0; i < u_svd.Ncols(); ++i)
      {
         midas::util::AbsVal_t<T> re_u_svd = std::real(u_svd[0][i]);
         midas::util::AbsVal_t<T> re_u_sdd = std::real(u_sdd[0][i]);
         assert(re_u_sdd != 0);
         const T sign = re_u_svd/re_u_sdd > 0? T(1): T(-1);
         u_sdd.ScaleCol(sign, i);
         vt_sdd.ScaleRow(sign, i);
      }

      
      // Check U.
      UNIT_ASSERT(check::MatricesAreEqual(u_svd, u_sdd, 256),"U NOT EQUAL (TYPE: " + libmda::util::type_of(T()) + ").");

      // Check V^T.
      UNIT_ASSERT(check::MatricesAreEqual(vt_svd, vt_sdd, 256),"V**T NOT EQUAL" + libmda::util::type_of(T()) + ").");
      
      // Check S
      for(Uin i = 0; i < s_svd.Size(); ++i)
      {
         UNIT_ASSERT_FZERO_PREC(s_svd[i] - s_sdd[i], s_sdd[0], 6, "i = "+std::to_string(i)+", S NOT EQUAL (TYPE: " + libmda::util::type_of(T()) + ").");
      }
   }
};

/**
 *
 **/
template<class T>
struct GESVDRankNullityTest: public cutee::test
{
   void run() 
   {
      GeneralMidasMatrix<T> mata(I_2,I_2);
      mata(0,0) = 0.96; mata(0,1) = 1.72;
      mata(1,0) = 2.28; mata(1,1) = 0.96;

      auto svd = GESVD(mata);
      
      UNIT_ASSERT_EQUAL(Rank(svd),size_t(2),"GESVD rank not correct FOR (TYPE: " + libmda::util::type_of(T()) + ").");
      UNIT_ASSERT_EQUAL(Nullity(svd),size_t(0),"GESVD nullity not correct FOR (TYPE: " + libmda::util::type_of(T()) + ").");
   }
};

/**
 *
 **/
template<class T>
struct GESDDRankNullityTest: public cutee::test
{
   void run() 
   {
      GeneralMidasMatrix<T> mata(I_2,I_2);
      mata(0,0) = 0.96; mata(0,1) = 1.72;
      mata(1,0) = 2.28; mata(1,1) = 0.96;

      auto svd = GESDD(mata);
      
      UNIT_ASSERT_EQUAL(Rank(svd),size_t(2),"GESVD rank not correct FOR (TYPE: " + libmda::util::type_of(T()) + ").");
      UNIT_ASSERT_EQUAL(Nullity(svd),size_t(0),"GESVD nullity not correct FOR (TYPE: " + libmda::util::type_of(T()) + ").");
   }
};

/**
 *
 **/
template<class T>
struct GELSSTest: public cutee::test
{
   static_assert(std::is_floating_point<T>::value,"GELSSTest only works for floating point.");

   void run() 
   {
      // setup test
      std::vector<std::function<T(T)> > func = { [](T x){ return x; } // f(x) = x
                                               , [](T x){ return x*x; } // f(x) = x^2;
                                               };
      std::vector<T> x = {0.1, 0.3, 0.4, 0.7, 0.91};
      std::vector<T> y = {func[1](x[0]), func[1](x[1]), func[1](x[2]), func[1](x[3]), func[1](x[4])};
      std::vector<T> sig = {1.0, 1.0, 1.0, 1.0, 1.0};

      GeneralMidasMatrix<T> A(In(x.size()), In(func.size()));
      GeneralMidasVector<T> B(In(x.size()));
      for(In i=I_0; i<A.Nrows(); ++i)
      {
         B[i] = y[i]/sig[i];
         for(In j=I_0; j<A.Ncols(); ++j)
            A[i][j] = func[j](x[i])/sig[i];
      }
      
      // do fitting 
      auto fit = GELSS(A,B,T(1e-15));
      
      // do assertions
      UNIT_ASSERT(cutee::numeric::float_numeq_zero(fit.B_[0],T(1.0),4),"coef 1 not zero");
      UNIT_ASSERT_FEQUAL_PREC(fit.B_[1], T(1.0), 6, "coef 2 not 1.0"); // MKL needs looser precision (6) to pass the test
   }
};

template<class T>
struct GETRFSquareTest: public cutee::test
{
   void run() 
   {
      GeneralMidasMatrix<T> mata(I_3,I_3);
      mata(0,0) = 1.0; mata(0,1) = 4.0; mata(0,2) = 7.0;
      mata(1,0) = 2.0; mata(1,1) = 5.0; mata(1,2) = 8.0;
      mata(2,0) = 3.0; mata(2,1) = 6.0; mata(2,2) = 10.0;
      
      auto lu = GETRF(mata);
      
      GeneralMidasMatrix<T> mat_recon;
      LoadA(lu,mat_recon);

      UNIT_ASSERT(check::MatricesAreEqual(mata,mat_recon,4),"CANNOT LOADA AFTER GETRF ON SQUARE MATRIX");
      UNIT_ASSERT_FEQUAL_PREC(lu.Determinant(),T(-3.0),8,"GETRF Determinant not correct");
   }
};

template<class T>
struct GETRFRectangleMNTest: public cutee::test
{
   void run() 
   {
      MidasMatrix mata(I_3,I_2);
      mata(0,0) = 1.0; mata(0,1) = 2.0;
      mata(1,0) = 3.0; mata(1,1) = 4.0;
      mata(2,0) = 5.0; mata(2,1) = 6.0;

      auto lu = GETRF(mata);
      
      MidasMatrix mat_recon;
      LoadA(lu,mat_recon);
         
      UNIT_ASSERT(check::MatricesAreEqual(mata,mat_recon,4),"NOT EQUAL AFTER GETRF ON RECTANGULAR MATRIX M>N");
   }
};

template<class T>
struct GETRFRectangleNMTest: public cutee::test
{
   void run() 
   {
      GeneralMidasMatrix<T> mata(I_2,I_3);
      mata(0,0) = 1.0; mata(0,1) = 2.0; mata(0,2) = 3.0;
      mata(1,0) = 4.0; mata(1,1) = 5.0; mata(1,2) = 6.0;
      
      auto lu = GETRF(mata);
      
      GeneralMidasMatrix<T> mat_recon;
      LoadA(lu,mat_recon);

      UNIT_ASSERT(check::MatricesAreEqual(mata,mat_recon,4),"CANNOT LOADA AFTER GETRF ON RECTANGULAR MATRIX N>M");
   }
};

template<class T>
struct GEQRFSquareTest: public cutee::test
{
   void run() 
   {
      GeneralMidasMatrix<T> mata(I_3,I_3);
      mata(0,0) = 1.0; mata(0,1) = 4.0; mata(0,2) = 7.0;
      mata(1,0) = 2.0; mata(1,1) = 5.0; mata(1,2) = 8.0;
      mata(2,0) = 3.0; mata(2,1) = 6.0; mata(2,2) = 10.0;
      
      auto qr = GEQRF(mata);
      
      GeneralMidasMatrix<T> mat_recon;
      LoadA(qr,mat_recon);

      //Mout << " MAT A: " << std::endl << mata << std::endl;
      //Mout << " MAT RECON: " << std::endl << mat_recon << std::endl;

      //Mout << libmda::util::type_of(T()) << std::endl;
      //Mout << libmda::util::type_of(qr.Determinant()) << std::endl;
      UNIT_ASSERT(check::MatricesAreEqual(mata,mat_recon,8),"CANNOT LOADA AFTER GETRF ON SQUARE MATRIX FOR (TYPE: " + libmda::util::type_of(T()) + ").");
      UNIT_ASSERT_FEQUAL_PREC(qr.Determinant(),T(-3.0),50,"GEQRF Determinant not correct FOR (TYPE: " + libmda::util::type_of(T()) + ").");
   }
};

/**
 *
 **/
template<class T>
struct GESVTest: public cutee::test
{
   void run() 
   {
      GeneralMidasVector<T> actual_sol(I_3);
      actual_sol(0) =  3.0;
      actual_sol(1) = -0.2;
      actual_sol(2) = -1.775;

      GeneralMidasMatrix<T> mata(I_3,I_3);
      mata(0,0) = 2.0; mata(0,1) = 0.0; mata(0,2) = 0.0;
      mata(1,0) = 1.0; mata(1,1) = 5.0; mata(1,2) = 0.0;
      mata(2,0) = 7.0; mata(2,1) = 9.0; mata(2,2) = 8.0;
      
      GeneralMidasVector<T> rhs(I_3);
      rhs(0) = 6.0;
      rhs(1) = 2.0;
      rhs(2) = 5.0;
   
      auto lin_sol = GESV(mata,rhs);
      GeneralMidasVector<T> sol;
      LoadSolution(lin_sol,sol);

      UNIT_ASSERT(check::VectorsAreEqual(sol,actual_sol,8), "GESV SOLUTION NOT CORRECT FOR (TYPE: " + libmda::util::type_of(T()) + ").");
   }
};

/**
 *
 **/
template<class T>
struct POSVTest: public cutee::test
{
   void run() 
   {
      GeneralMidasVector<T> actual_sol(I_3);
      actual_sol(0) =  8.0;
      actual_sol(1) = 13.0;
      actual_sol(2) = 10.0;

      GeneralMidasMatrix<T> mata(I_3,I_3);
      mata(0,0) =  2.0; mata(0,1) = -1.0; mata(0,2) =  0.0;
      mata(1,0) = -1.0; mata(1,1) =  2.0; mata(1,2) = -1.0;
      mata(2,0) =  0.0; mata(2,1) = -1.0; mata(2,2) =  2.0;

      GeneralMidasVector<T> rhs(I_3);
      rhs(0) = 3.0;
      rhs(1) = 8.0;
      rhs(2) = 7.0;

      auto lin_sol = POSV(mata,rhs);

      GeneralMidasVector<T> sol;
      LoadSolution(lin_sol,sol);

      UNIT_ASSERT(check::VectorsAreEqual(sol,actual_sol,8), "POSV SOLUTION NOT CORRECT FOR (TYPE: " + libmda::util::type_of(T()) + ").");
   }
};

/**
 *
 **/
template<class T>
struct SYGVTest: public cutee::test
{
   void run() 
   {
      GeneralMidasVector<T> actual_eigenvalues(I_3);
      actual_eigenvalues(0) = 0.5857864376269049;
      actual_eigenvalues(1) = 2.0;
      actual_eigenvalues(2) = 3.414213562373095;

      GeneralMidasMatrix<T> mata(I_3,I_3);
      mata(0,0) =  2.0; mata(0,1) = -1.0; mata(0,2) =  0.0;
      mata(1,0) = -1.0; mata(1,1) =  2.0; mata(1,2) = -1.0;
      mata(2,0) =  0.0; mata(2,1) = -1.0; mata(2,2) =  2.0;
      
      GeneralMidasMatrix<T> matb(I_3,I_3);
      matb(0,0) =  1.0; matb(0,1) =  0.0; matb(0,2) =  0.0;
      matb(1,0) =  0.0; matb(1,1) =  1.0; matb(1,2) =  0.0;
      matb(2,0) =  0.0; matb(2,1) =  0.0; matb(2,2) =  1.0;

      auto eig_sol = SYGV(mata,matb);
      
      GeneralMidasVector<T> eigenvalues;
      LoadEigenvalues(eig_sol, eigenvalues);

      UNIT_ASSERT(check::VectorsAreEqual(eigenvalues,actual_eigenvalues,8), "SYGV SOLUTION NOT CORRECT FOR (TYPE: " + libmda::util::type_of(T()) + ").");
   }
};

/**
 *
 **/
template<class T>
struct SYGVDTest: public cutee::test
{
   void run() 
   {
      GeneralMidasVector<T> actual_eigenvalues(I_3);
      actual_eigenvalues(0) = 0.5857864376269049;
      actual_eigenvalues(1) = 2.0;
      actual_eigenvalues(2) = 3.414213562373095;

      GeneralMidasMatrix<T> mata(I_3,I_3);
      mata(0,0) =  2.0; mata(0,1) = -1.0; mata(0,2) =  0.0;
      mata(1,0) = -1.0; mata(1,1) =  2.0; mata(1,2) = -1.0;
      mata(2,0) =  0.0; mata(2,1) = -1.0; mata(2,2) =  2.0;
      
      GeneralMidasMatrix<T> matb(I_3,I_3);
      matb(0,0) =  1.0; matb(0,1) =  0.0; matb(0,2) =  0.0;
      matb(1,0) =  0.0; matb(1,1) =  1.0; matb(1,2) =  0.0;
      matb(2,0) =  0.0; matb(2,1) =  0.0; matb(2,2) =  1.0;

      auto eig_sol = SYGVD(mata,matb);
      
      GeneralMidasVector<T> eigenvalues;
      LoadEigenvalues(eig_sol, eigenvalues);

      UNIT_ASSERT(check::VectorsAreEqual(eigenvalues,actual_eigenvalues,8), "SYGVD SOLUTION NOT CORRECT FOR (TYPE: " + libmda::util::type_of(T()) + ").");
   }
};

/**
 *
 **/
template<class T>
struct SYEVDTest: public cutee::test
{
   void run() 
   {
      GeneralMidasVector<T> actual_eigenvalues(I_3);
      actual_eigenvalues(0) = -3.204786611849726;
      actual_eigenvalues(1) =  9.595009999953609;
      actual_eigenvalues(2) = 15.609776611896118;

      GeneralMidasMatrix<T> mata(I_3,I_3);
      mata(0,0) =  4.0; mata(0,1) = -2.0; mata(0,2) = -8.0;
      mata(1,0) = -2.0; mata(1,1) =  9.0; mata(1,2) = -4.0;
      mata(2,0) = -8.0; mata(2,1) = -4.0; mata(2,2) =  9.0;
      
      auto eig_sol = SYEVD(mata);
      
      GeneralMidasVector<T> eigenvalues;
      LoadEigenvalues(eig_sol, eigenvalues);

      UNIT_ASSERT(check::VectorsAreEqual(eigenvalues,actual_eigenvalues,20), "SYEVD SOLUTION NOT CORRECT FOR (TYPE: " + libmda::util::type_of(T()) + ").");
   }
};


} /* namespace midas::test */

#endif /* ILAPACKTEST_H_INCLUDED */
