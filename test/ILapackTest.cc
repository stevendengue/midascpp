#include <iostream>
#include <complex>

#include "inc_gen/TypeDefs.h"
#include "test/ILapackTest.h"

namespace midas::test
{

void ILapackTest()
{
   cutee::suite suite("ILapack test");
   
   // basic
   suite.add_test<ComplexConjugateTest<Nb> >("ILAPACK TEST: ComplexConjugate ");
   suite.add_test<ComplexSqrtTest<Nb> >("ILAPACK TEST: ComplexSqrt");
   suite.add_test<ColumnDotTest<Nb> >("ILAPACK TEST: column dot");
   suite.add_test<ColumnDot2Test<Nb> >("ILAPACK TEST: column dot2");
   suite.add_test<ColumnComplexConjugateTest<Nb> >("ILAPACK TEST: column complex conj");
   suite.add_test<ColumnNormalizeTest<Nb> >("ILAPACK TEST: column normalize");
   suite.add_test<ColumnComplexNormalizeTest<Nb> >("ILAPACK TEST: column complex normalize");
   suite.add_test<ColumnUnbalancedLhsBinormalizeTest<Nb> >("ILAPACK TEST: unbalanced binormalization test lhs");
   suite.add_test<ColumnUnbalancedRhsBinormalizeTest<Nb> >("ILAPACK TEST: unbalanced binormalization test rhs");
   suite.add_test<ColumnUnbalancedLhsComplexBinormalizeTest<Nb> >("ILAPACK TEST: unbalanced complex binormalization test lhs");
   suite.add_test<ColumnUnbalancedRhsComplexBinormalizeTest<Nb> >("ILAPACK TEST: unbalanced complex binormalization test rhs");
   suite.add_test<ColumnBalancedComplexBinormalizeTest<Nb> >("ILAPACK TEST: balanced complex binormalization test");

   // matrix multiplication
   suite.add_test<GEMMSelfCopy>("ILAPACK TEST: GEMM of object and copy");
   
   // eigenvalue
   suite.add_test<GEEVBiOrthoTest<float> >("ILAPACK TEST: GEEV BIORTHO TEST FLOAT");
   suite.add_test<GEEVBiOrthoTest<double> >("ILAPACK TEST: GEEV BIORTHO TEST DOUBLE");
   suite.add_test<SYGVTest<float> >("ILAPACK TEST: SYGV TEST FLOAT");
   suite.add_test<SYGVTest<double> >("ILAPACK TEST: SYGV TEST DOUBLE");
   suite.add_test<SYGVDTest<float> >("ILAPACK TEST: SYGVD TEST FLOAT");
   suite.add_test<SYGVDTest<double> >("ILAPACK TEST: SYGVD TEST DOUBLE");
   suite.add_test<SYEVDTest<float> >("ILAPACK TEST: SYEVD TEST FLOAT");
   suite.add_test<SYEVDTest<double> >("ILAPACK TEST: SYEVD TEST DOUBLE");

   // svd
	suite.add_test<GESVDTest<double> >("ILAPACK TEST: GESVD TEST DOUBLE");
	suite.add_test<GESVDTest<float> >("ILAPACK TEST: GESVD TEST FLOAT");
	suite.add_test<GESVDTest<std::complex<double> > >("ILAPACK TEST: GESVD TEST COMPLEX<DOUBLE>");
	suite.add_test<GESVDTest<std::complex<float> > >("ILAPACK TEST: GESVD TEST COMPLEX<FLOAT>");
	suite.add_test<GESVDRankNullityTest<double> >("ILAPACK TEST: GESVD RANK NULLITY TEST DOUBLE");
	suite.add_test<GESVDRankNullityTest<float> >("ILAPACK TEST: GESVD RANK NULLITY TEST FLOAT");
	suite.add_test<GESVDRankNullityTest<std::complex<double> > >("ILAPACK TEST: GESVD RANK NULLITY TEST COMPLEX<DOUBLE>");
	suite.add_test<GESVDRankNullityTest<std::complex<float> > >("ILAPACK TEST: GESVD RANK NULLITY TEST COMPLEX<FLOAT>");
	suite.add_test<GESDDTest<double> >("ILAPACK TEST: GESDD TEST DOUBLE");
	suite.add_test<GESDDTest<float> >("ILAPACK TEST: GESDD TEST FLOAT");
	suite.add_test<GESDDTest<std::complex<double> > >("ILAPACK TEST: GESDD TEST COMPLEX<DOUBLE>");
	suite.add_test<GESDDTest<std::complex<float> > >("ILAPACK TEST: GESDD TEST COMPLEX<FLOAT>");
	suite.add_test<GESDDRankNullityTest<double> >("ILAPACK TEST: GESDD RANK NULLITY TEST DOUBLE");
	suite.add_test<GESDDRankNullityTest<float> >("ILAPACK TEST: GESDD RANK NULLITY TEST FLOAT");
	suite.add_test<GESDDRankNullityTest<std::complex<double> > >("ILAPACK TEST: GESDD RANK NULLITY TEST COMPLEX<DOUBLE>");
	suite.add_test<GESDDRankNullityTest<std::complex<float> > >("ILAPACK TEST: GESDD RANK NULLITY TEST COMPLEX<FLOAT>");
	suite.add_test<GESVD_GESDDTest<double> >("ILAPACK TEST: GESVD GESDD TEST DOUBLE");
	suite.add_test<GESVD_GESDDTest<float> >("ILAPACK TEST: GESVD GESDD TEST FLOAT");
	suite.add_test<GESVD_GESDDTest<std::complex<double> > >("ILAPACK TEST: GESVD GESDD TEST COMPLEX<DOUBLE>");
	suite.add_test<GESVD_GESDDTest<std::complex<float> > >("ILAPACK TEST: GESVD GESDD TEST COMPLEX<FLOAT>");

   // linear least squares
	suite.add_test<GELSSTest<double> >("ILAPACK TEST: GELSS TEST DOUBLE");
	suite.add_test<GELSSTest<float> >("ILAPACK TEST: GELSS TEST FLOAT");
	//suite.add_test<GELSSTest<std::complex<double> > >("ILAPACK TEST: GELSS TEST COMPLEX<DOUBLE>");
	
   // lu
   suite.add_test<GETRFSquareTest<double> >("ILAPACK TEST: GETRF SQUARE TEST DOUBLE");
   suite.add_test<GETRFSquareTest<float> >("ILAPACK TEST: GETRF SQUARE TEST FLOAT");
   suite.add_test<GETRFSquareTest<std::complex<double> > >("ILAPACK TEST: GETRF SQUARE TEST COMPLEX<DOUBLE>");
   suite.add_test<GETRFSquareTest<std::complex<float> > >("ILAPACK TEST: GETRF SQUARE TEST COMPLEX<FLOAT>");
   suite.add_test<GETRFRectangleMNTest<double> >("ILAPACK TEST: GETRF RECTANGLE M>N TEST DOUBLE");
   suite.add_test<GETRFRectangleMNTest<float > >("ILAPACK TEST: GETRF RECTANGLE M>N TEST FLOAT");
   suite.add_test<GETRFRectangleMNTest<std::complex<double> > >("ILAPACK TEST: GETRF RECTANGLE M>N TEST COMPLEX<DOUBLE>");
   suite.add_test<GETRFRectangleMNTest<std::complex<float> > >("ILAPACK TEST: GETRF RECTANGLE M>N TEST COMPLEX<FLOAT>");
   suite.add_test<GETRFRectangleNMTest<double> >("ILAPACK TEST: GETRF RECTANGLE N>M TEST DOUBLE");
   suite.add_test<GETRFRectangleNMTest<float > >("ILAPACK TEST: GETRF RECTANGLE N>M TEST FLOAT");
   suite.add_test<GETRFRectangleNMTest<std::complex<double> > >("ILAPACK TEST: GETRF RECTANGLE N>M TEST COMPLEX<DOUBLE>");
   suite.add_test<GETRFRectangleNMTest<std::complex<float> > >("ILAPACK TEST: GETRF RECTANGLE N>M TEST COMPLEX<FLOAT>");
   
   // qr
   suite.add_test<GEQRFSquareTest<float> >("ILAPACK TEST: GEQRF SQUARE TEST FLOAT");
   suite.add_test<GEQRFSquareTest<double> >("ILAPACK TEST: GEQRF SQUARE TEST DOUBLE");
   suite.add_test<GEQRFSquareTest<std::complex<float> > >("ILAPACK TEST: GEQRF SQUARE TEST COMPLEX<FLOAT>");
   suite.add_test<GEQRFSquareTest<std::complex<double> > >("ILAPACK TEST: GEQRF SQUARE TEST COMPLEX<DOUBLE>");
   
   // linear equations 
   suite.add_test<GESVTest<double> >("ILAPACK TEST: GESV TEST DOUBLE");
   suite.add_test<GESVTest<float> >("ILAPACK TEST: GESV TEST FLOAT");
   suite.add_test<GESVTest<std::complex<double> > >("ILAPACK TEST: GESV TEST COMPLEX<DOUBLE>");
   suite.add_test<GESVTest<std::complex<float> > >("ILAPACK TEST: GESV TEST COMPLEX<FLOAT>");
   suite.add_test<POSVTest<double> >("ILAPACK TEST: POSV TEST DOUBLE");
   suite.add_test<POSVTest<float> >("ILAPACK TEST: POSV TEST FLOAT");
   suite.add_test<POSVTest<std::complex<double> > >("ILAPACK TEST: POSV TEST COMPLEX<DOUBLE>");
   suite.add_test<POSVTest<std::complex<float> > >("ILAPACK TEST: POSV TEST COMPLEX<FLOAT>");

   RunSuite(suite);
}

} /* namespace midas::test */
