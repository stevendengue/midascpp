/**
************************************************************************
* 
* @file                PesTest.h
*
* Created:             16-12-2017
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Unit tests for Pes module.
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDAS_TEST_PESTEST_H_INCLUDED
#define MIDAS_TEST_PESTEST_H_INCLUDED

#include "test/CuteeInterface.h"

#include "input/PesCalcDef.h"
#include "pes/PropertyInfo.h"

namespace midas
{
namespace test
{
namespace pes_test
{

/**
 * Test that PropertyInfo can be correctly constructed from a midasifc.propinfo string.
 *    
 **/
struct PropertyInfoTest
   :  public cutee::test
{
   //! Do the test
   void run()
   {
      // Default calc def
      PesCalcDef pes_calc_def;

      // Create prop info string.
      std::string midasifc_propinfo
         =  "tens_order=(0),descriptor=(GROUND_STATE_ENERGY)\n"
            "tens_order=(1),descriptor=(X_DIPOLE),Rot_group=(0),element=(0)\n"
            "tens_order=(1),descriptor=(Y_DIPOLE),Rot_group=(0),element=(1)\n"
            "tens_order=(1),descriptor=(Z_DIPOLE),Rot_group=(0),element=(2)\n";
      
      // Construct PropertyInfo.
      pes::PropertyInfo property_info;
      property_info.Update(pes_calc_def, midasifc_propinfo, false);
      
      // Assertions
      // Assert number of properties
      UNIT_ASSERT_EQUAL(property_info.Size(), I_4, "Number of properties not correct.");
      
      // Assert that descriptors are correct
      UNIT_ASSERT_EQUAL(property_info.GetPropertyInfoEntry(0).GetDescriptor(), std::string("GROUND_STATE_ENERGY"), "Descriptor wrong for entry 0.")
      UNIT_ASSERT_EQUAL(property_info.GetPropertyInfoEntry(1).GetDescriptor(), std::string("X_DIPOLE")           , "Descriptor wrong for entry 1.")
      UNIT_ASSERT_EQUAL(property_info.GetPropertyInfoEntry(2).GetDescriptor(), std::string("Y_DIPOLE")           , "Descriptor wrong for entry 2.")
      UNIT_ASSERT_EQUAL(property_info.GetPropertyInfoEntry(3).GetDescriptor(), std::string("Z_DIPOLE")           , "Descriptor wrong for entry 3.")
      
      // Assert that order is correct.
      UNIT_ASSERT_EQUAL(property_info.GetPropertyInfoEntry(0).GetOrder(), I_0, "Order wrong for entry 0.")
      UNIT_ASSERT_EQUAL(property_info.GetPropertyInfoEntry(1).GetOrder(), I_1, "Order wrong for entry 1.")
      UNIT_ASSERT_EQUAL(property_info.GetPropertyInfoEntry(2).GetOrder(), I_1, "Order wrong for entry 2.")
      UNIT_ASSERT_EQUAL(property_info.GetPropertyInfoEntry(3).GetOrder(), I_1, "Order wrong for entry 3.")
      
      // Assert that rotation groups are correct
      UNIT_ASSERT_EQUAL(property_info.GetPropertyInfoEntry(0).GetRotGroup(), -I_1, "Rotation group wrong for entry 0.")
      UNIT_ASSERT_EQUAL(property_info.GetPropertyInfoEntry(1).GetRotGroup(),  I_0, "Rotation group wrong for entry 1.")
      UNIT_ASSERT_EQUAL(property_info.GetPropertyInfoEntry(2).GetRotGroup(),  I_0, "Rotation group wrong for entry 2.")
      UNIT_ASSERT_EQUAL(property_info.GetPropertyInfoEntry(3).GetRotGroup(),  I_0, "Rotation group wrong for entry 3.")
      
      // Assert that element is correct.
      UNIT_ASSERT_EQUAL(property_info.GetPropertyInfoEntry(0).GetElement(), std::vector<In>{} , "Element wrong for entry 0.")
      UNIT_ASSERT_EQUAL(property_info.GetPropertyInfoEntry(1).GetElement(), std::vector<In>{0}, "Element wrong for entry 1.")
      UNIT_ASSERT_EQUAL(property_info.GetPropertyInfoEntry(2).GetElement(), std::vector<In>{1}, "Element wrong for entry 2.")
      UNIT_ASSERT_EQUAL(property_info.GetPropertyInfoEntry(3).GetElement(), std::vector<In>{2}, "Element wrong for entry 3.")
   }
};

} /* namespace pes_test */
} /* namespace test */
} /* namespace midas */

#endif /* MIDAS_TEST_PESTEST_H_INCLUDED */
