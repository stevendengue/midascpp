/**
************************************************************************
* 
* @file                MoleculeTest.h
*
* Created:             16-12-2016
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Unit tests MoleculeInfo.
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDAS_TEST_MOLECULETEST_H_INCLUDED
#define MIDAS_TEST_MOLECULETEST_H_INCLUDED

#include "test/CuteeInterface.h"

#include "pes/molecule/MoleculeInfo.h"
#include "pes/molecule/MoleculeFile.h"
#include "pes/molecule/MoleculeUtility.h"
#include "pes/molecule/PointGroupSymmetry.h"
#include "pes/molecule/SymInfo.h"

#include "util/Rotation.h"

namespace midas::test
{

namespace detail
{

/**
 * Calculate mass weighted dot product between normal coordinates.
 **/
auto calculate_coordinate_dot(const molecule::MoleculeInfo& molecule, const MidasMatrix& mat, int i, int j)
{
   auto number_of_nuclei = molecule.GetNumberOfNuclei();

   auto dot_prod1 = C_0;
   auto dot_prod2 = C_0;
   auto dot_prod3 = C_0;

   for(int k = 0; k < number_of_nuclei; ++k)
   {
      int  index = 3 * k;
      auto mass  = molecule.GetMasses().at(k);

      dot_prod1 += mat[i][index    ] * mass * mat[j][index    ];
      dot_prod2 += mat[i][index + 1] * mass * mat[j][index + 1];
      dot_prod3 += mat[i][index + 2] * mass * mat[j][index + 2];
   }

   auto dot_prod = dot_prod1 + dot_prod2 + dot_prod3;

   return dot_prod;
}

/**
 * Calculate mass weighted dot product between normal coordinates.
 **/
auto calculate_coordinate_dot
   (  const molecule::MoleculeInfo& molecule
   ,  const MidasMatrix& mat1
   ,  const MidasMatrix& mat2
   ,  int i
   ,  int j
   )
{
   auto number_of_nuclei = molecule.GetNumberOfNuclei();

   auto dot_prod1 = C_0;
   auto dot_prod2 = C_0;
   auto dot_prod3 = C_0;

   for(int k = 0; k < number_of_nuclei; ++k)
   {
      int  index = 3 * k;
      auto mass  = molecule.GetMasses().at(k);

      dot_prod1 += mat1[i][index    ] * mass * mat2[j][index    ];
      dot_prod2 += mat1[i][index + 1] * mass * mat2[j][index + 1];
      dot_prod3 += mat1[i][index + 2] * mass * mat2[j][index + 2];
   }

   auto dot_prod = dot_prod1 + dot_prod2 + dot_prod3;

   return dot_prod;
}

/**
 * This creates a water molecule originating from a ORCA calculation.
 *
 * The special thing about this molecule is that the normalcoordinates 
 * have a "wrong" (compared to MidasCpp input) normalization for the
 * normalcoordinates. The normal coordinates are normalized when not mass weighted,
 * instead of when mass weighted.
 *
 * @return   Returns ORCA water molecule.
 **/
auto create_orca_molecule
   (
   )
{
   molecule::MoleculeFileInfo fileinfo("", "MIDAS");
   fileinfo.SetContents
      (  "#0 MoleculeInput\n"
         "\n"
         "#1 XYZ\n"
         "3   au\n"
         "comment\n"
         "H     1.417951176316266          1.621631314542519          0.000000000000000\n"
         "O     0.000000000011618          0.536189638748573          0.000000000000000\n"
         "H    -1.417951176327883          1.621631314551412          0.000000000000000\n"
         "\n"
         "#1 FREQ\n"
         "3 cm-1\n"
         "1750.36\n"
         "4148.48\n"
         "4244.72\n"
         "\n"
         "#1 VIBCOORD\n"
         "au\n"
         "#2 COORDINATE\n"
         " 0.432967 -0.556847  0.000000\n"
         "-0.000000  0.070167  0.000000\n"
         "-0.432967 -0.556847  0.000000\n"
         "#2 COORDINATE\n"
         " 0.581119  0.401276  0.000000\n"
         " 0.000000 -0.050564  0.000000\n"
         "-0.581119  0.401276  0.000000\n"
         "#2 COORDINATE\n"
         " 0.560081  0.428742  0.000000\n"
         "-0.070575  0.000000  0.000000\n"
         " 0.560081 -0.428742  0.000000\n"
         "\n"
         "#0 MoleculeInputEnd\n"
       );
   
   // create molecule
   return molecule::MoleculeInfo{ molecule::MoleculeFileInfo::Set{fileinfo} };
}

} /* namespace detail */

/**
 * Test creation of rotation matrix around X-axis.
 *
 *         / 1.0     0.0     0.0    \
 * R(x) =  | 0.0     cos(a) -sin(a) |
 *         \ 0.0     sin(a)  cos(a) /
 *
 **/
struct CreateRotationXTest
{
   void run()
   {
      auto angle           = C_PI/3.0;
      auto rotation_matrix = util::CreateRotationMatrixX(angle);

      using float_type = std::decay_t<decltype(rotation_matrix[0][0])>;
   
      UNIT_ASSERT_FEQUAL(rotation_matrix[0][0], float_type(1.0)             , "Element 0,0 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[0][1], float_type(0.0)             , "Element 0,1 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[0][2], float_type(0.0)             , "Element 0,2 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[1][0], float_type(0.0)             , "Element 1,0 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[1][1], float_type( std::cos(angle)), "Element 1,1 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[1][2], float_type( std::sin(angle)), "Element 1,2 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[2][0], float_type(0.0)             , "Element 2,0 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[2][1], float_type(-std::sin(angle)), "Element 2,1 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[2][2], float_type( std::cos(angle)), "Element 2,2 not correct.");
   }
};

/**
 * Test creation of rotation matrix around Y-axis.
 * 
 *         / cos(b)  0.0     sin(b) \
 * R(y) =  | 0.0     1.0     0.0    |
 *         \-sin(b)  0.0     cos(b) /
 *
 **/
struct CreateRotationYTest
{
   void run()
   {
      auto angle           = C_PI/3.0;
      auto rotation_matrix = util::CreateRotationMatrixY(angle);

      using float_type = std::decay_t<decltype(rotation_matrix[0][0])>;
   
      UNIT_ASSERT_FEQUAL(rotation_matrix[0][0], float_type( std::cos(angle)), "Element 0,0 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[0][1], float_type(0.0)             , "Element 0,1 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[0][2], float_type(-std::sin(angle)), "Element 0,2 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[1][0], float_type(0.0)             , "Element 1,0 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[1][1], float_type(1.0)             , "Element 1,1 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[1][2], float_type(0.0)             , "Element 1,2 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[2][0], float_type( std::sin(angle)), "Element 2,0 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[2][1], float_type(0.0)             , "Element 2,1 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[2][2], float_type( std::cos(angle)), "Element 2,2 not correct.");
   }
};

/**
 * Test creation of rotation matrix around Z-axis.
 *
 *         / cos(c) -sin(c)  0.0    \
 * R(z) =  | sin(c)  cos(c)  0.0    |
 *         \ 0.0     0.0     1.0    /
 *
 **/
struct CreateRotationZTest
{
   void run()
   {
      auto angle           = C_PI/3.0;
      auto rotation_matrix = util::CreateRotationMatrixZ(angle);

      using float_type = std::decay_t<decltype(rotation_matrix[0][0])>;
   
      UNIT_ASSERT_FEQUAL(rotation_matrix[0][0], float_type( std::cos(angle)), "Element 0,0 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[0][1], float_type( std::sin(angle)), "Element 0,1 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[0][2], float_type(0.0)             , "Element 0,2 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[1][0], float_type(-std::sin(angle)), "Element 1,0 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[1][1], float_type( std::cos(angle)), "Element 1,1 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[1][2], float_type(0.0)             , "Element 1,2 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[2][0], float_type(0.0)             , "Element 2,0 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[2][1], float_type(0.0)             , "Element 2,1 not correct.");
      UNIT_ASSERT_FEQUAL(rotation_matrix[2][2], float_type(1.0)             , "Element 2,2 not correct.");
   }
};

/**
 * Test that water:
 *
 *      O
 *     / \
 *    H   H
 *
 * Is found to be C2V point group.
 * Because of how the axis' of the C2V point group is defined in midas,
 * the normalmodes should be labelled:
 *    
 *    bend        symmetric stretch        assymetric stretch
 *
 *      O                 O                       O
 *     / \               / \                     / \
 *    /   \             /   \                   /   \
 *   H-> <-H           H     H                 H     H
 *                    /       \               /       ^
 *                   v         v             v         \
 *
 *     A1                A1                       B1
 *
 * for bend, symmetric-stretch, and assymetric strech respectively.
 **/
struct WaterSymC2VTest
   :  public cutee::test
{
   //! Do the test
   void run()
   {
      // create input
      molecule::MoleculeFileInfo fileinfo("", "MIDAS");
      fileinfo.SetContents
         (  "#0 MOLECULE INPUT\n"
            "#1 XYZ\n"
            "3 Aa\n"
            "comment\n"
            "O#1    0.000000000000    0.000000000000      0.0\n"
            "H#1    0.757390973309    0.586352753497      0.0\n"
            "H#2   -0.757390973309    0.586352753497      0.0\n"
            "\n"
            "#1 FREQ\n"
            "3 cm-1\n"
            " 1.6494968413962467E+03\n"
            " 3.8337828001194184E+03\n"
            " 3.9453552233505602E+03\n"
            "\n"
            "#1 VIBCOORD\n"
            "au\n"
            "#2 COORDINATE\n"
            " -2.1856071206933261E-17   -6.8064280142906802E-02   0.0000000000000000E+00\n"
            " -4.0955238123917437E-01    5.4025995916333314E-01   0.0000000000000000E+00\n"
            "  4.0955238123917476E-01    5.4025995916333380E-01   0.0000000000000000E+00\n"
            "#2 COORDINATE\n"
            " -8.8974954529188906E-17    4.8625096195909628E-02   0.0000000000000000E+00\n"
            " -5.7328278772467722E-01   -3.8596084656763274E-01   0.0000000000000000E+00\n"
            "  5.7328278772467856E-01   -3.8596084656763396E-01   0.0000000000000000E+00\n"
            "#2 COORDINATE\n"
            " -6.7575790584075535E-02   -5.4225158422113038E-17   0.0000000000000000E+00\n"
            "  5.3638134704449603E-01    4.1525288027304541E-01   0.0000000000000000E+00\n"
            "  5.3638134704449458E-01   -4.1525288027304452E-01   0.0000000000000000E+00\n"
            "#0 MOLECULE INPUT END\n"
         );

      // create molecule
      molecule::MoleculeInfo molecule( molecule::MoleculeFileInfo::Set{fileinfo} );

      // do symmetry analysis
      auto syminfo = SymmetryAnalysis(molecule, SymmetryThresholds());
      
      // check some stuff
      UNIT_ASSERT_EQUAL(syminfo.GetPointGroup(), std::string("C2V"), "Point group not correct.");

      UNIT_ASSERT_EQUAL(molecule.GetModeSymInfo(0), std::string("A1"), "Label 1 not correct");
      UNIT_ASSERT_EQUAL(molecule.GetModeSymInfo(1), std::string("A1"), "Label 2 not correct");
      UNIT_ASSERT_EQUAL(molecule.GetModeSymInfo(2), std::string("B1"), "Label 3 not correct");
   }
};

/**
 * Test that trans-C2H2F2:
 *
 *    H       F
 *     \     /
 *      C = C
 *     /     \
 *    F       H
 *
 * Is found to be C2H point group (E, C_2, I, sigma). 
 * Also check that modes are correctly labelled with Irreps.
 **/
struct C2H2F2SymC2HTest
   :  public cutee::test
{
   //! Do the test
   void run()
   {
      // create input
      molecule::MoleculeFileInfo fileinfo("", "MIDAS");
      fileinfo.SetContents
         (  "#0 MOLECULEINPUT\n"
            "#1 XYZ\n"
            "6 au\n"
            "comment\n"
            " C        1.2045176511            0.3235446428            0.0000000000\n"
            " C       -1.2045176511           -0.3235446428            0.0000000000\n"
            " F        1.9642430647            2.7702850015            0.0000000000\n"
            " F       -1.9642430647           -2.7702850015            0.0000000000\n"
            " H        2.7183000658           -1.0735664983            0.0000000000\n"
            " H       -2.7183000658            1.0735664983            0.0000000000\n"
            "\n"
            "#1 FREQ\n"
            "12 cm-1\n"
            " 0.3693208169159836E+04 Ag\n"
            " 0.3672449741739032E+04 Au\n"
            " 0.2093560389467223E+04 Ag\n"
            " 0.1546222151618395E+04 Au\n"
            " 0.1507493446653296E+04 Ag\n"
            " 0.1368833366188383E+04 Au\n"
            " 0.1334393556879199E+04 Ag\n"
            " 0.1147895301624211E+04 Bu\n"
            " 0.9424772093514414E+03 Bg\n"
            " 0.6041745796585541E+03 Ag\n"
            " 0.3642677757476277E+03 Bu\n"
            " 0.3355582660063809E+03 Au\n"
            "\n"
            "#1 VIBCOORD\n"
            "au\n"
            "#2 COORDINATE\n"
            " 5.1526629743226922E-02-3.6964170771329963E-02 0.0000000000000000E+00\n"
            "-5.1526629743345272E-02 3.6964170771098231E-02 0.0000000000000000E+00\n"
            " 9.5481378531515421E-05 5.1172214579717906E-04 0.0000000000000000E+00\n"
            "-9.5481378410640472E-05-5.1172214568715737E-04 0.0000000000000000E+00\n"
            "-4.8984985941219367E-01 4.5637532264322817E-01 0.0000000000000000E+00\n"
            " 4.8984985941288833E-01-4.5637532264253972E-01 0.0000000000000000E+00\n"
            "#2 COORDINATE\n"
            "-4.1326478849195498E-02 3.8576225307486722E-02 0.0000000000000000E+00\n"
            "-4.1326478849175043E-02 3.8576225307443486E-02 0.0000000000000000E+00\n"
            "-3.0519664394393066E-04-4.2701001467927312E-05 0.0000000000000000E+00\n"
            "-3.0519664395340539E-04-4.2701001440908702E-05 0.0000000000000000E+00\n"
            " 4.9782054922834329E-01-4.5851556851991992E-01 0.0000000000000000E+00\n"
            " 4.9782054922832203E-01-4.5851556852005376E-01 0.0000000000000000E+00\n"
            "#2 COORDINATE\n"
            " 1.6243119986373863E-01 1.0497778314306769E-01 0.0000000000000000E+00\n"
            "-1.6243119986373850E-01-1.0497778314306708E-01 0.0000000000000000E+00\n"
            "-1.1116097832852161E-02-2.4196525588238097E-02 0.0000000000000000E+00\n"
            " 1.1116097832851713E-02 2.4196525588237604E-02 0.0000000000000000E+00\n"
            "-6.2022408878269153E-02-1.8313738337753371E-01 0.0000000000000000E+00\n"
            " 6.2022408878271373E-02 1.8313738337753438E-01 0.0000000000000000E+00\n"
            "#2 COORDINATE\n"
            "-8.0231902735259805E-02-1.0980981270208283E-01 0.0000000000000000E+00\n"
            "-8.0231902732306001E-02-1.0980981270748952E-01 0.0000000000000000E+00\n"
            " 3.3870323462982424E-02 5.2407225538063894E-02 0.0000000000000000E+00\n"
            " 3.3870323463255643E-02 5.2407225542046749E-02 0.0000000000000000E+00\n"
            " 3.1682164851626166E-01 3.1956357655588397E-01 0.0000000000000000E+00\n"
            " 3.1682164850992356E-01 3.1956357654209377E-01 0.0000000000000000E+00\n"
            "#2 COORDINATE\n"
            "-9.0782617879909544E-02 6.5152176214825636E-02 0.0000000000000000E+00\n"
            " 9.0782617880011365E-02-6.5152176214806901E-02 0.0000000000000000E+00\n"
            "-5.3700834733853356E-03-3.6315925937481851E-03 0.0000000000000000E+00\n"
            " 5.3700834735700186E-03 3.6315925937374840E-03 0.0000000000000000E+00\n"
            "-4.8428937541206679E-01-3.3483925676328807E-01 0.0000000000000000E+00\n"
            " 4.8428937541213130E-01 3.3483925676326814E-01 0.0000000000000000E+00\n"
            "#2 COORDINATE\n"
            "-3.9445539919066748E-03 9.9472770311166983E-02 0.0000000000000000E+00\n"
            "-3.9445539919108954E-03 9.9472770311186731E-02 0.0000000000000000E+00\n"
            "-1.2222688541248793E-02-8.4332352395452059E-02 0.0000000000000000E+00\n"
            "-1.2222688541289750E-02-8.4332352395420321E-02 0.0000000000000000E+00\n"
            " 2.7737574534492054E-01 4.0533502643008684E-01 0.0000000000000000E+00\n"
            " 2.7737574534493498E-01 4.0533502643012631E-01 0.0000000000000000E+00\n"
            "#2 COORDINATE\n"
            " 6.4010536572238375E-02-1.1986790302941031E-01 0.0000000000000000E+00\n"
            "-6.4010536574801449E-02 1.1986790302455989E-01 0.0000000000000000E+00\n"
            " 6.1357619410987154E-03 8.9560712190039013E-02 0.0000000000000000E+00\n"
            "-6.1357619385763625E-03-8.9560712187691668E-02 0.0000000000000000E+00\n"
            "-1.1943855424777250E-01-3.3176771316922665E-01 0.0000000000000000E+00\n"
            " 1.1943855426304650E-01 3.3176771318329773E-01 0.0000000000000000E+00\n"
            "#2 COORDINATE\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00-5.7684146320390477E-02\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00-5.7684146320391393E-02\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00 5.9379444433398988E-04\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00 5.9379444433411792E-04\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00 6.7564171328561151E-01\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00 6.7564171328561429E-01\n"
            "#2 COORDINATE\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00 1.4553532260074162E-01\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00-1.4553532260162816E-01\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00-2.0719299723822276E-02\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00 2.0719299722934927E-02\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00-4.8562581996924742E-01\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00 4.8562581996835635E-01\n"
            "#2 COORDINATE\n"
            "-8.7467847198110637E-03 8.7877664531955696E-02 0.0000000000000000E+00\n"
            " 8.7467847198833427E-03-8.7877664531957236E-02 0.0000000000000000E+00\n"
            " 1.0272959239236437E-01 9.9337972339005445E-02 0.0000000000000000E+00\n"
            "-1.0272959239229726E-01-9.9337972339005765E-02 0.0000000000000000E+00\n"
            " 3.4314340874338857E-02 1.3083372264147825E-01 0.0000000000000000E+00\n"
            "-3.4314340874297238E-02-1.3083372264144016E-01 0.0000000000000000E+00\n"
            "#2 COORDINATE\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00 1.5072241490182181E-01\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00 1.5072241490182176E-01\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00-1.0341996617198469E-01\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00-1.0341996617198511E-01\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00 1.5493286707501694E-01\n"
            " 0.0000000000000000E+00 0.0000000000000000E+00 1.5493286707501580E-01\n"
            "#2 COORDINATE\n"
            " 1.3373149038734269E-01-5.1025042219087657E-02 0.0000000000000000E+00\n"
            " 1.3373149037839652E-01-5.1025042185682122E-02 0.0000000000000000E+00\n"
            "-9.6950275457950347E-02 2.8940926396179380E-02 0.0000000000000000E+00\n"
            "-9.6950275534829336E-02 2.8940926450694588E-02 0.0000000000000000E+00\n"
            " 2.3528144365614753E-01 6.1984097506853790E-02 0.0000000000000000E+00\n"
            " 2.3528144368611820E-01 6.1984097582411038E-02 0.0000000000000000E+00\n"
            "#0 MOLECULEINPUTEND\n"
         );

      // create molecule
      molecule::MoleculeInfo molecule( molecule::MoleculeFileInfo::Set{fileinfo} );

      // do symmetry analysis
      auto syminfo = SymmetryAnalysis(molecule, SymmetryThresholds());
      
      // check some stuff
      UNIT_ASSERT_EQUAL(syminfo.GetPointGroup(), std::string("C2H"), "Point group not correct.");
      
      UNIT_ASSERT_EQUAL(molecule.GetModeSymInfo(0) , std::string("Ag"), "Label  1 not correct");
      UNIT_ASSERT_EQUAL(molecule.GetModeSymInfo(1) , std::string("Bu"), "Label  2 not correct");
      UNIT_ASSERT_EQUAL(molecule.GetModeSymInfo(2) , std::string("Ag"), "Label  3 not correct");
      UNIT_ASSERT_EQUAL(molecule.GetModeSymInfo(3) , std::string("Bu"), "Label  4 not correct");
      UNIT_ASSERT_EQUAL(molecule.GetModeSymInfo(4) , std::string("Ag"), "Label  5 not correct");
      UNIT_ASSERT_EQUAL(molecule.GetModeSymInfo(5) , std::string("Bu"), "Label  6 not correct");
      UNIT_ASSERT_EQUAL(molecule.GetModeSymInfo(6) , std::string("Ag"), "Label  7 not correct");
      UNIT_ASSERT_EQUAL(molecule.GetModeSymInfo(7) , std::string("Au"), "Label  8 not correct");
      UNIT_ASSERT_EQUAL(molecule.GetModeSymInfo(8) , std::string("Bg"), "Label  9 not correct");
      UNIT_ASSERT_EQUAL(molecule.GetModeSymInfo(9) , std::string("Ag"), "Label 10 not correct");
      UNIT_ASSERT_EQUAL(molecule.GetModeSymInfo(10), std::string("Au"), "Label 11 not correct");
      UNIT_ASSERT_EQUAL(molecule.GetModeSymInfo(11), std::string("Bu"), "Label 12 not correct");
   }
};

/**
 * Test mass weighted normalization of normal coordinates.
 **/
struct NormalizeNormalCoordinatesTest
{

   void run()
   {
      // create molecule
      molecule::MoleculeInfo molecule = detail::create_orca_molecule();

      // Normalize coordinates
      molecule::NormalizeNormalCoordinates(molecule);
      
      // Test
      auto coordinates = molecule.NormalCoord();

      UNIT_ASSERT_EQUAL(coordinates.Nrows(), 3, "Number of rows wrong in normalcoordinate matrix."  );
      UNIT_ASSERT_EQUAL(coordinates.Ncols(), 9, "Number of colums wrong in normalcoordinate matrix.");

      UNIT_ASSERT_FEQUAL(detail::calculate_coordinate_dot(molecule, coordinates, 0, 0), 1.0, "Coordinate 0 NOT normalized.");
      UNIT_ASSERT_FEQUAL(detail::calculate_coordinate_dot(molecule, coordinates, 1, 1), 1.0, "Coordinate 1 NOT normalized.");
      UNIT_ASSERT_FEQUAL(detail::calculate_coordinate_dot(molecule, coordinates, 2, 2), 1.0, "Coordinate 2 NOT normalized.");
   }
};

/**
 * Test mass weighted ortho normalization of normal coordinates.
 **/
struct OrthoNormalizeNormalCoordinatesTest
{

   void run()
   {
      // create molecule
      molecule::MoleculeInfo molecule = detail::create_orca_molecule();

      // Normalize coordinates
      molecule::OrthoNormalizeNormalCoordinates(molecule);
      
      // Test
      auto coordinates = molecule.NormalCoord();

      UNIT_ASSERT_EQUAL(coordinates.Nrows(), 3, "Number of rows wrong in normalcoordinate matrix."  );
      UNIT_ASSERT_EQUAL(coordinates.Ncols(), 9, "Number of colums wrong in normalcoordinate matrix.");

      UNIT_ASSERT_FEQUAL(detail::calculate_coordinate_dot(molecule, coordinates, 0, 0), 1.0, "Coordinate 0 NOT normalized.");
      UNIT_ASSERT_FEQUAL(detail::calculate_coordinate_dot(molecule, coordinates, 1, 1), 1.0, "Coordinate 1 NOT normalized.");
      UNIT_ASSERT_FEQUAL(detail::calculate_coordinate_dot(molecule, coordinates, 2, 2), 1.0, "Coordinate 2 NOT normalized.");
      
      UNIT_ASSERT_FZERO(detail::calculate_coordinate_dot(molecule, coordinates, 0, 1), 1.0, "Coordinates 0,1 NOT orthogonal.");
      UNIT_ASSERT_FZERO(detail::calculate_coordinate_dot(molecule, coordinates, 0, 2), 1.0, "Coordinates 0,2 NOT orthogonal.");
      UNIT_ASSERT_FZERO(detail::calculate_coordinate_dot(molecule, coordinates, 1, 0), 1.0, "Coordinates 1,0 NOT orthogonal.");
      UNIT_ASSERT_FZERO(detail::calculate_coordinate_dot(molecule, coordinates, 1, 2), 1.0, "Coordinates 1,2 NOT orthogonal.");
      UNIT_ASSERT_FZERO(detail::calculate_coordinate_dot(molecule, coordinates, 2, 0), 1.0, "Coordinates 2,0 NOT orthogonal.");
      UNIT_ASSERT_FZERO(detail::calculate_coordinate_dot(molecule, coordinates, 2, 1), 1.0, "Coordinates 2,1 NOT orthogonal.");
   }
};

/**
 *
 **/
struct CreateTranslationalCoordinatesTest
{
   void run()
   {
      auto molecule    = detail::create_orca_molecule();
      auto trans_coord = molecule::CreateTranslationalCoordinates(molecule);

      UNIT_ASSERT_FEQUAL(detail::calculate_coordinate_dot(molecule, trans_coord, 0, 0), 1.0, "X Coordinate NOT normalized.");
      UNIT_ASSERT_FEQUAL(detail::calculate_coordinate_dot(molecule, trans_coord, 1, 1), 1.0, "Y Coordinate NOT normalized.");
      UNIT_ASSERT_FEQUAL(detail::calculate_coordinate_dot(molecule, trans_coord, 2, 2), 1.0, "Z Coordinate NOT normalized.");
   }
};

/**
 * NOT FINISHED!
 **/
struct CreateRotationalCoordinatesTest
{
   void run()
   {
      auto molecule    = detail::create_orca_molecule();
      auto trans_coord = molecule::CreateTranslationalCoordinates(molecule);
      auto rot_coord   = molecule::CreateRotationalCoordinates(molecule);

      UNIT_ASSERT_FEQUAL(detail::calculate_coordinate_dot(molecule, trans_coord, 0, 0), 1.0, "X Coordinate NOT normalized.");
      UNIT_ASSERT_FEQUAL(detail::calculate_coordinate_dot(molecule, trans_coord, 1, 1), 1.0, "Y Coordinate NOT normalized.");
      UNIT_ASSERT_FEQUAL(detail::calculate_coordinate_dot(molecule, trans_coord, 2, 2), 1.0, "Z Coordinate NOT normalized.");

      std::cout << rot_coord << std::endl;

      for(int i = 0; i < trans_coord.Nrows(); ++i)
      {
         for(int j = 0; j < trans_coord.Nrows(); ++j)
         {
            UNIT_ASSERT_FZERO(detail::calculate_coordinate_dot(molecule, trans_coord, rot_coord, i, j), 1.0, "Coordinates " + std::to_string(i) + "," + std::to_string(j) + " NOT orthogonal.");
         }
      }
   }
};

/**
 * Test contruction of molecule fields.
 **/
struct MoleculeFieldConstructionTest
{
   void run()
   {
      molecule::MoleculeFields fields
      {  molecule::molecule_field_t::XYZ
      ,  molecule::molecule_field_t::FREQ
      ,  molecule::molecule_field_t::XYZ
      ,  molecule::molecule_field_t::VIBCOORD
      };
      
      // Check size 
      UNIT_ASSERT_EQUAL(fields.size(), 3, "Size not correct after construction.");

      // Check ordering
      UNIT_ASSERT_EQUAL(fields[0], molecule::molecule_field_t::XYZ     , "Order not correct for element 0.");
      UNIT_ASSERT_EQUAL(fields[1], molecule::molecule_field_t::FREQ    , "Order not correct for element 1.");
      UNIT_ASSERT_EQUAL(fields[2], molecule::molecule_field_t::VIBCOORD, "Order not correct for element 2.");
   }
};

/**
 * Test insertion function of MoleculeFields class.
 **/
struct MoleculeFieldInsertionTest
{
   void run()
   {
      molecule::MoleculeFields fields;
   
      // Insert and check sizes
      UNIT_ASSERT_EQUAL(fields.size(), 0, "Size not 0 from beginning");

      fields.InsertMoleculeField(molecule::molecule_field_t::XYZ);
      UNIT_ASSERT_EQUAL(fields.size(), 1, "Size not 1 after first insertion.");

      fields.InsertMoleculeField(molecule::molecule_field_t::FREQ);
      UNIT_ASSERT_EQUAL(fields.size(), 2, "Size not 2 after second insertion.");
      
      fields.InsertMoleculeField(molecule::molecule_field_t::XYZ);
      UNIT_ASSERT_EQUAL(fields.size(), 2, "Size not 2 after duplicate insertion.");
      
      // Check ordering
      UNIT_ASSERT_EQUAL(fields[0], molecule::molecule_field_t::XYZ     , "Order not correct for element 0.");
      UNIT_ASSERT_EQUAL(fields[1], molecule::molecule_field_t::FREQ    , "Order not correct for element 1.");
   }
};

/**
 * Test shift to center of mass.
 **/
struct ShiftToCenterOfMassTest
{
   void run()
   {
      std::vector<Nuclei> structure = {
         { 0.0000, 0.00000, 0.0, 1.0, "O"}
      ,  { 0.7578, 0.61004, 0.0, 1.0, "H"}
      ,  {-0.7578, 0.61004, 0.0, 1.0, "H"}
      };
      
      // Calculate center of mass and displace structure.
      auto center_of_mass_displacement = molecule::FindCenterOfMass(structure);

      center_of_mass_displacement[0] = -center_of_mass_displacement[0];
      center_of_mass_displacement[1] = -center_of_mass_displacement[1];
      center_of_mass_displacement[2] = -center_of_mass_displacement[2];
      
      molecule::ShiftStructure(structure, center_of_mass_displacement);

      auto origo = molecule::FindCenterOfMass(structure);
      
      // Assert
      UNIT_ASSERT_EQUAL(center_of_mass_displacement.Size(), 3, "Center of mass displacement not a 3D vector.");
      UNIT_ASSERT_EQUAL(origo.Size(),                       3, "Origo displacement not a 3D vector.");

      UNIT_ASSERT_FZERO(origo[0], 1.0, "Origo x incorrect.");
      UNIT_ASSERT_FZERO(origo[1], 1.0, "Origo y incorrect.");
      UNIT_ASSERT_FZERO(origo[2], 1.0, "Origo z incorrect.");
   }
};

/**
 * Test rotation to inertia frame for diagonal tensor of inertia.
 *
 * This should "just" swap axes.
 **/
struct DiagonalRotateToInertiaFrameTest
{
   void run()
   {
      std::vector<Nuclei> structure = {
         {0.0, 0.00000,  0.0000, 1.0, "O"}
      ,  {0.0, 0.61004,  0.7578, 1.0, "H"}
      ,  {0.0, 0.61004, -0.7578, 1.0, "H"}
      };
      
      // Check rotation matrix
      auto rot_matrix = molecule::FindRotationToInertiaFrame(structure);

      UNIT_ASSERT_FEQUAL(rot_matrix[0][0], 0.0, " Element 0,0 not 0.0");
      UNIT_ASSERT_FEQUAL(rot_matrix[0][1], 0.0, " Element 0,1 not 0.0");
      UNIT_ASSERT_FEQUAL(rot_matrix[1][0], 0.0, " Element 1,0 not 0.0");
      UNIT_ASSERT_FEQUAL(rot_matrix[1][2], 0.0, " Element 1,2 not 0.0");
      UNIT_ASSERT_FEQUAL(rot_matrix[2][1], 0.0, " Element 2,1 not 0.0");
      UNIT_ASSERT_FEQUAL(rot_matrix[2][2], 0.0, " Element 2,2 not 0.0");
      
      UNIT_ASSERT_FEQUAL(rot_matrix[0][2], 1.0, " Element 0,2 not 1.0");
      UNIT_ASSERT_FEQUAL(rot_matrix[1][1], 1.0, " Element 1,1 not 1.0");
      UNIT_ASSERT_FEQUAL(rot_matrix[2][0], 1.0, " Element 2,0 not 1.0");

      
      // Check that inertia tensor is correct after rotation
      molecule::RotateCoordinates(structure, rot_matrix);
      
      auto inertia_tensor = molecule::CalculateInertiaTensor(structure);

      UNIT_ASSERT_FEQUAL(inertia_tensor[0][0], 1.3673883073126269E+03, "Inertia tensor element 0,0 not correct");
      UNIT_ASSERT_FEQUAL(inertia_tensor[1][1], 2.1100096375092762E+03, "Inertia tensor element 1,1 not correct");
      UNIT_ASSERT_FEQUAL(inertia_tensor[2][2], 3.4773979448219038E+03, "Inertia tensor element 2,2 not correct");

      UNIT_ASSERT_FEQUAL(inertia_tensor[0][1], 0.0, "Inertia tensor element 0,1 not equal to 0.0");
      UNIT_ASSERT_FEQUAL(inertia_tensor[0][2], 0.0, "Inertia tensor element 0,2 not equal to 0.0");
      UNIT_ASSERT_FEQUAL(inertia_tensor[1][0], 0.0, "Inertia tensor element 1,0 not equal to 0.0");
      UNIT_ASSERT_FEQUAL(inertia_tensor[1][2], 0.0, "Inertia tensor element 1,2 not equal to 0.0");
      UNIT_ASSERT_FEQUAL(inertia_tensor[2][0], 0.0, "Inertia tensor element 2,0 not equal to 0.0");
      UNIT_ASSERT_FEQUAL(inertia_tensor[2][1], 0.0, "Inertia tensor element 2,1 not equal to 0.0");
   }
};

/**
 * Test calculation of inertia tensor.
 **/
struct RotateToInertiaFrameTest
{
   void run()
   {
      std::vector<Nuclei> structure = {
         { 0.0000, 0.00000, 0.0, 1.0, "O"}
      ,  { 0.7578, 0.61004, 0.0, 1.0, "H"}
      ,  {-0.7578, 0.61004, 0.0, 1.0, "H"}
      };

      auto structure_copy = structure;

      // Rotate system out of the "lowest inertia" structure
      auto rot_matrix_x = util::CreateRotationMatrixX(30, util::Unit::Degrees);
      auto rot_matrix_y = util::CreateRotationMatrixX(45, util::Unit::Degrees);
      auto rot_matrix_z = util::CreateRotationMatrixX(60, util::Unit::Degrees);

      molecule::RotateCoordinates(structure, rot_matrix_x  );
      molecule::RotateCoordinates(structure, rot_matrix_y  );
      molecule::RotateCoordinates(structure, rot_matrix_z  );
      
      // Find rotation back to the inertia frame
      auto rot_matrix     = molecule::FindRotationToInertiaFrame(structure);
      
      molecule::RotateCoordinates(structure, rot_matrix);
      
      // Assert the structure
      UNIT_ASSERT_FZERO(structure[0].X(), 1.0, "Oxygen not in x = 0.0.");
      UNIT_ASSERT_FZERO(structure[0].Y(), 1.0, "Oxygen not in y = 0.0.");
      UNIT_ASSERT_FZERO(structure[0].Z(), 1.0, "Oxygen not in z = 0.0.");

      UNIT_ASSERT_FEQUAL(std::fabs(structure[1].X()), std::fabs(structure_copy[1].X()), "Hydrogen 1, x not correct.");
      UNIT_ASSERT_FEQUAL(std::fabs(structure[1].Y()), std::fabs(structure_copy[1].Y()), "Hydrogen 1, y not correct.");
      UNIT_ASSERT_FZERO (structure[1].Z(), 1.0, "Hydrogen 1 not in z = 0.0.");
      
      UNIT_ASSERT_FEQUAL(std::fabs(structure[2].X()), std::fabs(structure_copy[2].X()), "Hydrogen 2, x not correct.");
      UNIT_ASSERT_FEQUAL(std::fabs(structure[2].Y()), std::fabs(structure_copy[2].Y()), "Hydrogen 2, y not correct.");
      UNIT_ASSERT_FZERO (structure[2].Z(), 1.0, "Hydrogen 2 not in z = 0.0.");
   }
};


} /* namespace midas::test */

#endif /* MIDAS_TEST_MOLECULETEST_H_INCLUDED */
