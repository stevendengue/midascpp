/**
************************************************************************
* 
* @file    TensorDecompInfo.cc
*
* @date    06-09-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Helper functions for TensorDecompInfo
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#include "inc_gen/Const.h"
#include "util/Error.h"
#include "tensor/TensorDecompInfo.h"
#include "input/TensorDecompInput.h"
#include "util/FindInMap.h"

namespace midas::tensor
{

namespace detail
{
void UpdateDecompThresholds
   (  TensorDecompInfo& aInfo
   ,  Nb aNewThresh
   ,  Nb aPreprocThresh = -C_1
   );
}  /* namespace detail */

/**
 * Constructor for TensorDecompInfo using preset and threshold.
 *
 * @param aPreset       Name of the preset
 * @param aThresh       Threshold for decomposer
 **/
TensorDecompInfo::TensorDecompInfo
   (  const std::string& aPreset
   ,  Nb aThresh
   )
{
   // Get presets
   const auto& preset_map = ::detail::GetTensorDecompPresetMap();

   // Get the keywords for the given preset
   auto& preset = util::FindElseError
                     (  aPreset
                     ,  preset_map
                     ,  "PRESET: '" + std::string(aPreset) + "' not found in preset map."
                     ).second;

   // Set keywords from preset
   for(const auto& elem : preset)
   {
      (*this)[elem.first] = elem.second;
   }

   // Set the rest
   ::detail::ValidateTensorDecompInput(*this);

   // Update thresholds
   detail::UpdateDecompThresholds(*this, aThresh);
}

//! Get the smallest or largest decomp threshold
Nb GetDecompThreshold
   (  const TensorDecompInfo::Set& aInfo
   ,  bool aSmallest
   )
{
   Nb result = aSmallest ? C_10_10 : C_0;
   for(auto& decompinfo_const : aInfo)
   {
      TensorDecompInfo& decompinfo = const_cast<TensorDecompInfo&>(decompinfo_const);

      // If no TensorDecompInfo is given
      if (  decompinfo.empty()
         )
      {
         result = -C_1;
         break;
      }

      auto driver = decompinfo.at("DRIVER").get<std::string>();
      if (  driver == "FINDBESTCP"
         )
      {
         auto thresh = decompinfo["FINDBESTCPRESTHRESHOLD"].get<Nb>();
         result = aSmallest ? std::min(thresh, result) : std::max(thresh, result);
      }
      else if  (  driver == "NESTEDCP"
               )
      {
         auto thresh = decompinfo["NESTEDCPRESTHRESHOLD"].get<Nb>();
         result = aSmallest ? std::min(thresh, result) : std::max(thresh, result);
      }
      else if  (  driver == "CPID"
               )
      {
         auto thresh = decompinfo["CPIDRESTHRESHOLD"].get<Nb>();
         result = aSmallest ? std::min(thresh, result) : std::max(thresh, result);
      }
      else if  (  driver == "FIXEDRANKCP"
               )
      {
         auto fitter = decompinfo.at("FITTER").get<std::string>();
         if (  fitter == "ALS"
            )
         {
            auto thresh = decompinfo["CPALSTHRESHOLD"].get<Nb>();
            result = aSmallest ? std::min(thresh, result) : std::max(thresh, result);
         }
         else if  (  fitter == "NCG"
                  )
         {
            auto thresh = decompinfo["CPNCGTHRESHOLD"].get<Nb>();
            result = aSmallest ? std::min(thresh, result) : std::max(thresh, result);
         }
         else if  (  fitter == "ASD"
                  )
         {
            auto thresh = decompinfo["CPASDTHRESHOLD"].get<Nb>();
            result = aSmallest ? std::min(thresh, result) : std::max(thresh, result);
         }
         else if  (  fitter == "NOFITTER"
                  )
         {
            // do nothing...
         }
         else
         {
            MIDASERROR("No match for the fitter: " + fitter);
         }
      }
      else
      {
         MIDASERROR("No match for driver: " + driver);
      }
   }

   return result;
}

namespace detail
{
//! Update decomp thresholds
void UpdateDecompThresholds
   (  TensorDecompInfo& aInfo
   ,  Nb aNewThresh
   ,  Nb aPreprocThresh
   )
{
   // If no TensorDecompInfo is given
   if (  aInfo.empty()
      )
   {
      return;
   }

   // Modify preprocessor threshold
   auto preproc = aInfo.at("PREPROCESSOR").get<std::string>();
   if (  preproc == "C2T"
      && aInfo.at("C2TADAPTIVETHRESHOLD").get<bool>()
      )
   {
      // Set threshold for Tucker format 100 times lower as default
      if (  aPreprocThresh < C_0
         )
      {
         aInfo["C2TSVDTHRESHOLD"] = aNewThresh * C_I_10_2;
      }
      else
      {
         aInfo["C2TSVDTHRESHOLD"] = aPreprocThresh;
      }
   }

   // Modify driver threshold
   auto driver = aInfo.at("DRIVER").get<std::string>();
   if (  driver == "FINDBESTCP"
      )
   {
      aInfo["FINDBESTCPRESTHRESHOLD"] = aNewThresh;
   }
   else if  (  driver == "NESTEDCP"
            )
   {
      aInfo["NESTEDCPRESTHRESHOLD"] = aNewThresh;
   }
   else if  (  driver == "CPID"
            )
   {
      aInfo["CPIDRESTHRESHOLD"] = aNewThresh;
   }

   // Modify fitter threshold
   auto fitter = aInfo.at("FITTER").get<std::string>();
   if (  fitter == "ALS"
      )
   {
      // The CPALS threshold is a threshold scaling factor lower than the requested residual threshold.
      // This is equivalent to using #3CPALSRELATIVETHRESHOLD
      aInfo["CPALSTHRESHOLD"] = aNewThresh*C_I_10_2;
   }
   else if  (  fitter == "NCG"
            )
   {
      aInfo["CPNCGTHRESHOLD"] = aNewThresh;
   }
   else if  (  fitter == "ASD"
            )
   {
      aInfo["CPASDTHRESHOLD"] = aNewThresh;
   }
   else if  (  fitter == "NOFITTER"
            )
   {
      // do nothing...
   }
   else
   {
      MIDASERROR("No match for the fitter:" + fitter + " during threshold update!");
   }
}
} /* namespace detail */

//! Update decomp thresholds
void UpdateDecompThresholds
   (  TensorDecompInfo::Set& aInfo
   ,  Nb aNewThresh
   ,  Nb aPreprocThresh
   )
{
   for(auto& decompinfo_const : aInfo)
   {
      TensorDecompInfo& decompinfo = const_cast<TensorDecompInfo&>(decompinfo_const);

      detail::UpdateDecompThresholds(decompinfo, aNewThresh, aPreprocThresh);
   }
}

} /* namespace midas::tensor */
