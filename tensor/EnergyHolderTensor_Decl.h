/**
************************************************************************
* 
* @file    EnergyHolderTensor_Decl.h
*
* @date    27-10-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Tensor class for holding modal/orbital energies 
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef ENERGYHOLDERTENSOR_DECL_H_INCLUDED
#define ENERGYHOLDERTENSOR_DECL_H_INCLUDED

#include "tensor/BaseTensor.h"
#include "mmv/DataCont.h"

class ModeCombi;

template
   <  class T
   >
class DotEvaluator;

/**
 *
 **/
template
   <  class T
   >
class ModalEnergyHolder
{
   private:
      //! Mode combination
      const std::vector<In>& mModes;

      //! VSCF eigenvalues
      const DataCont& mEigVals;

      //! VSCF-modal offsets (in mEigVals)
      const std::vector<In>& mModalOffsets;

   public:
      //! Constructor from MC and Vscf energies
      explicit ModalEnergyHolder
         (  const std::vector<In>& aMc
         ,  const DataCont& aEigVals
         ,  const std::vector<In>& aModalOffsets
         );

      //! Energy access
      T GetEnergy
         (  const std::vector<unsigned>& aIndices
         )  const;
};


/**
 * Tensor for holding modal or orbital energies for constructing energy denominators, etc.
 **/
template
   <  class T
   ,  template<class> class IMPL
   >
class EnergyHolderTensorBase
   :  public BaseTensor<T>
   ,  private IMPL<T>
{
   private:
      using Base = BaseTensor<T>;
      using real_t = typename Base::real_t;

      //! Return inverse energies
      bool mInverse = false;

      //! Square all values
      bool mSquare = false;

      //! Energy shift (to be subtracted from all energies)
      T mShift = static_cast<T>(0.);

      //! Private interfaces BEGIN
      //============================================================================================
      //! Contraction interface
      BaseTensor<T>* Contract_impl
         (  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         ,  const BaseTensor<T>&
         ,  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         ,  bool conjugate_left = false
         ,  bool conjugate_right = false
         )  const override
      {
         MIDASERROR("Not implemented");
         return nullptr;
      }

      //! Called by ContractInternal().
      BaseTensor<T>* ContractInternal_impl
         (  const std::vector<std::pair<unsigned int, unsigned int>>&
         ,  const std::vector<unsigned int>&
         )  const override
      {
         MIDASERROR("Not implemented");
         return nullptr;
      }

      //! Called by Reorder()
      BaseTensor<T>* Reorder_impl
         (  const std::vector<unsigned int>&
         ,  const std::vector<unsigned int>&
         )  const override
      {
         MIDASERROR("Not implemented");
         return nullptr;
      }

      //! Called by Slice().
      BaseTensor<T>* Slice_impl
         (  const std::vector<std::pair<unsigned int,unsigned int>>&
         )  const override
      {
         MIDASERROR("Not implemented");
         return nullptr;
      }
      //============================================================================================
      //! Private interfaces END

   public:
      //! Constructor from dimensions, inverse, and arguments for IMPL
      template
         <  class... Us
         >
      EnergyHolderTensorBase
         (  const std::vector<unsigned>& aDims
         ,  bool aInverse
         ,  bool aSquare
         ,  T aShift
         ,  Us&&... aArgs
         );

      //! Type info
      typename BaseTensor<T>::typeID Type() const override;

      //! Type info
      std::string ShowType() const override;

      //! Access elements (calls IMPL::GetEnergy)
      T Element
         (  const std::vector<unsigned>& aIndices
         )  const override;

      //! Clone the tensor
      EnergyHolderTensorBase<T, IMPL>* Clone
         (
         )  const override;

      //! Norm2
      real_t Norm2
         (
         )  const override;

      //! Dot
      typename DotEvaluator<T>::dot_t DotDispatch
         (  const CanonicalTensor<T>* const other
         )  const override;

      //! Dump into pointer
      void DumpInto
         (  T*
         )  const override;

      //! Public interfaces BEGIN
      //============================================================================================

      //! Perform down contraction and return SimpleTensor
      BaseTensor<T>* ContractDown
         (  const unsigned int idx
         ,  const BaseTensor<T>* vect_in
         )  const override;

      BaseTensor<T>* ContractForward(const unsigned int, const BaseTensor<T>*) const override { MIDASERROR("NOT Implemented"); return nullptr; };
      BaseTensor<T>* ContractUp     (const unsigned int, const BaseTensor<T>*) const override { MIDASERROR("NOT Implemented"); return nullptr; };

      //! Dot
      typename DotEvaluator<T>::dot_t Dot
         (  const BaseTensor<T>* const other
         )  const override
      {
         MIDASERROR("EnergyHolderTensorBase::Dot with BaseTensor not implemented!");
         return static_cast<typename DotEvaluator<T>::dot_t>(0.);
      }

      EnergyHolderTensorBase* operator*=(const T) override { MIDASERROR("NOT Implemented"); return nullptr; }
      EnergyHolderTensorBase* operator+=(const BaseTensor<T>&) override { MIDASERROR("NOT Implemented"); return nullptr; }
      EnergyHolderTensorBase* operator*=(const BaseTensor<T>&) override { MIDASERROR("NOT Implemented"); return nullptr; }
      EnergyHolderTensorBase* operator/=(const BaseTensor<T>&) override { MIDASERROR("NOT Implemented"); return nullptr; }

      std::ostream& write_impl(std::ostream& output) const override { MIDASERROR("Not Implemented."); return output; }
      //============================================================================================
      //! Public interfaces END
};

#endif  /* ENERGYHOLDERTENSOR_DECL_H_INCLUDED */
