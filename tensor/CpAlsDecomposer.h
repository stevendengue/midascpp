#ifndef CPALSDECOMPOSER_H_INCLUDED
#define CPALSDECOMPOSER_H_INCLUDED

#include "tensor/TensorDecomposer.h"
#include "tensor/TensorDecompInfo.h"
#include "tensor/CpGuesser.h"

namespace midas
{
namespace tensor
{

class CpAlsDecomposer
   : public detail::ConcreteCpDecomposerBase
{
   private:
      //! Max iterations in CPALS algorithm
      In mCpAlsMaxIter;

      //! Max iterations for fit to max rank
      In mMaxRankMaxIter;

      //! Threshold for CPALS algorithm
      Nb mCpAlsThreshold;

      //! Set threshold relative to argument threshold ( only if aThresh > 0 )
      Nb mCpAlsRelativeThreshold;

      //! Relative threshold for checkiter in FitALS
      Nb mCpAlsRelativeCheckThresh;

      //! Use Pivotised Alternating Least-Squares algorithm
      bool mPivotised;

      //! Const ref to TensorDecompInfo used for construction of CpAlsInput
      const TensorDecompInfo& mInfo; 


      ///> Implementation of CheckDecomposition
      bool CheckDecompositionImpl
         (  const NiceTensor<Nb>&
         )  const override;
      
      ///> Implementation of Decompose with argument guess.
      FitReport<Nb> DecomposeImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  NiceTensor<Nb>&
         ,  Nb
         )  const override;

      ///> Implementation of Type
      std::string TypeImpl
         (
         )  const override;

   public:
      ///> c-tor from TensorDecompInfo
      CpAlsDecomposer(const TensorDecompInfo&);
};

} /* namespace tensor */
} /* namespace midas */

#endif /* CPALSDECOMPOSER_H_INCLUDED */
