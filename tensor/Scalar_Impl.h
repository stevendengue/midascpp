#ifndef SCALAR_IMPL_H_INCLUDED
#define SCALAR_IMPL_H_INCLUDED

#include "tensor/SimpleTensor.h"
#include "tensor/CanonicalTensor.h"

#include "mpi/Impi.h"

/*!
 *
 */
template<class T>
BaseTensor<T>* Scalar<T>::ContractUp
   ( const unsigned int idx
   , const BaseTensor<T>* other
   ) const
{
   //MIDASERROR("Contracting up a scalar doesn't make much sense, does it? YES IT DOES?");
   assert(idx == 0); // on scalar we can only Contract up idx 0
   auto other_simple_ptr = dynamic_cast<const SimpleTensor<T>* >(other);
   assert(other_simple_ptr && (other_simple_ptr->NDim() == 1)); // assert that other is SimpleTensor, and that it has dimension 1
   // do up contraction
   if(mEvaluateUpToCanonical)
   {
      // return type canonical
      auto other_copy_ptr = new CanonicalTensor<T>(other->GetDims(), 1);
      auto mode_matrices = other_copy_ptr->GetModeMatrices();
      auto size = other->TotalSize();
      for(int i = 0; i < size; ++i)
      {
         mode_matrices[0][i] = other_simple_ptr->GetData()[i];
      }
      other_copy_ptr->Scale(mValue); 
      return other_copy_ptr;
   }
   else
   { 
      // return type simple
      auto other_copy_ptr = other_simple_ptr->Clone(); 
      other_copy_ptr->Scale(mValue); // up contraction is just a copy of other scaled with mValue
      return other_copy_ptr;
   }
}

/*!
 *
 */
template<class T>
void Scalar<T>::Axpy(const BaseTensor<T>& arX, const T& arA)
{
   // No assertion of identical shapes as long as this is only impl. for other
   // scalars. But assert shape if implementing it for other BaseTensor
   // derivatives!!
   MidasAssert(arX.Type() == BaseTensor<T>::typeID::SCALAR, "In Scalar<T>::Axpy: Not impl. for other tensor being of type '"+arX.ShowType()+"'.");

   mValue += dynamic_cast<const Scalar<T>&>(arX).GetValue() * arA;
}

/*!
 *
 */
template<class T>
typename DotEvaluator<T>::dot_t Scalar<T>::Dot
   (  const BaseTensor<T>* const other
   )  const
{
   return midas::math::Conj(other->DotDispatch(this));
}

#ifdef VAR_MPI
/**
 * Overload of MpiSend for Scalar
 *
 * @param aRecievingRank  The rank to send the SimpleTensor to.
 **/
template<class T>
void Scalar<T>::MpiSend
   (  In aRecievingRank
   )  const
{
   // send basetensor stuff
   BaseTensor<T>::MpiSendDimensions(aRecievingRank);
   
   // send scalar stuff
   //auto comm = midas::mpi_util::get_mpi_comm_manager().GetCommunicator();
   
   midas::mpi::detail::WRAP_Send(&mValue, 1, midas::mpi::DataTypeTrait<T>::Get(), aRecievingRank, 0, MPI_COMM_WORLD);

   int up_to_canonical = mEvaluateUpToCanonical;
   midas::mpi::detail::WRAP_Send(&up_to_canonical, 1, MPI_INT, aRecievingRank, 0, MPI_COMM_WORLD);
}

/**
 * Overload of MpiRecv for Scalar
 *
 * @param aSendingRank  The rank to recieve the SimpleTensor from.
 **/
template<class T>
void Scalar<T>::MpiRecv
   (  In aSendingRank
   )
{
   // recv basetensor stuff
   BaseTensor<T>::MpiRecvDimensions(aSendingRank);
   
   // recv simpletensor stuff
   //auto comm = midas::mpi_util::get_mpi_comm_manager().GetCommunicator();
   MPI_Status status;
   
   midas::mpi::detail::WRAP_Recv(&mValue, 1, midas::mpi::DataTypeTrait<T>::Get(), aSendingRank, 0, MPI_COMM_WORLD, &status);

   int up_to_canonical;
   midas::mpi::detail::WRAP_Recv(&up_to_canonical, 1, MPI_INT, aSendingRank, 0, MPI_COMM_WORLD, &status);
   mEvaluateUpToCanonical = up_to_canonical;
}

/**
 * Overload of MpiBcast for Scalar
 *
 * @param aRoot      Root rank
 **/
template
   <  class T
   >
void Scalar<T>::MpiBcast
   (  In aRoot
   )
{
   // Bcast dims
   BaseTensor<T>::MpiBcastDimensions(aRoot);

   // Bcast value
   midas::mpi::detail::WRAP_Bcast(&mValue, 1, midas::mpi::DataTypeTrait<T>::Get(), aRoot, MPI_COMM_WORLD);

   // Bcast "up to canonical"
   int up_to_cp = this->mEvaluateUpToCanonical;
   midas::mpi::detail::WRAP_Bcast(&up_to_cp, 1, MPI_INT, aRoot, MPI_COMM_WORLD);
   mEvaluateUpToCanonical = bool(up_to_cp);
}

#endif /* VAR_MPI */

#endif /* SCALAR_IMPL_H_INCLUDED */
