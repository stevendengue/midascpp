/**
 ************************************************************************
 * 
 * @file                NiceContractor.h
 *
 * Created:             30.04.2015
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * \brief               NiceContractor: Wraps together a NiceTensor and a
 *                      ContractionIndices vector.
 *
 * Detailed Description: Enables notation like `c=contract(a[i,j], b[j,k])` for
 *                       contractions, and convenient contraction chaining.
 *
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef NICECONTRACTOR_H_INCLUDED
#define NICECONTRACTOR_H_INCLUDED

#include<vector>

#include"ContractionIndices.h"

template <class T>
class NiceTensor;

//! Wraps together a NiceTensor and a ContractionIndices vector.
/*! NiceContractor is a wrapper that puts together a tensor and a set of
 *  contraction indices. Its main purpose is convenient contractions, of the form
 *
 *      c=contract(contract(a[i,j], b[j,k]), u[i,a]);
 *
 *  In normal production code it should not be declared: NiceContractor's are
 *  created with NiceTensor::operator[]() and as the result of contractions.
 *  NiceContractor's can be automatically converted into NiceTensor's, carrying
 *  out all necessary internal contractions and then dropping the indices.
 */
template <class T>
class NiceContractor
{
   private:
      //! Pointer to either a pre-existing NiceTensor or a new one.
      const NiceTensor<T>* mTensor;
      //! Contraction indices.
      const ContractionIndices mIndices;
      //! True if mTensor should be deleted upon destruction.
      const bool mIsNew;

   public:
      //! Construct from pre-existing or newly created NiceTensor.
      /*! See NiceContractor<T> contract(const NiceContractor<T>&, const NiceContractor<T>&)
       *  and NiceTensor::operator[]
       */
      NiceContractor(const NiceTensor<T>* aTensor,
                     const ContractionIndices aIndices,
                     const bool aIsNew=false):
          mTensor(aTensor)
         ,mIndices(aIndices)
         ,mIsNew(aIsNew)
      {
      }

      //! Get #mTensor .
      const NiceTensor<T>* GetTensor() const
      {
          return mTensor;
      }

      //! Get #mIndices .
      const ContractionIndices& GetIndices() const
      {
          return mIndices;
      }

      //! Destructor. Deletes #mTensor if #mIsNew `==true`.
      ~NiceContractor<T>()
      {
         if(mTensor && mIsNew)
            delete mTensor;
      }

      //! Multiply with a constant. First converts to NiceTensor.
      NiceTensor<T> operator*(const T coeff) const
      {
         return NiceTensor<T>(*this)*coeff;
      }

      //! Addition. First converts to NiceTensor.
      NiceTensor<T> operator+(const NiceContractor& right) const
      {
         return NiceTensor<T>(*this) + NiceTensor<T>(right);
      }

      //! Addition with NiceTensor. First converts to NiceTensor.
      NiceTensor<T> operator+(const NiceTensor<T>& right) const
      {
         return NiceTensor<T>(*this) + right;
      }

      //! Subtraction. First converts to NiceTensor.
      NiceTensor<T> operator-(const NiceContractor& right) const
      {
         return NiceTensor<T>(*this) - NiceTensor<T>(right);
      }

      //! Subtraction with NiceTensor. First converts to NiceTensor.
      NiceTensor<T> operator-(const NiceTensor<T>& right) const
      {
         return NiceTensor<T>(*this) - right;
      }
};

//! Tensor contraction.
//* A wrapper around contract()
template <class T>
NiceContractor<T> contract(const NiceContractor<T>& t1, const NiceContractor<T>& t2, bool conjugate_left = false, bool conjugate_right = false)
{
   const ContractionIndices& indices1=t1.GetIndices();
   const ContractionIndices& indices2=t2.GetIndices();

   return NiceContractor<T>(
         new NiceTensor<T>(contract(*(t1.GetTensor()->GetTensor()), indices1.ToUIntVector(),
                                    *(t2.GetTensor()->GetTensor()), indices2.ToUIntVector(),
                                    conjugate_left, conjugate_right)),
         sorted_symmetric_difference(indices1, indices2),
         true);
}

template <class T>
NiceTensor<T> operator*(const T coeff, const NiceContractor<T>& right)
{
   return right*coeff;
}

template <class T>
NiceTensor<T> operator+(const NiceTensor<T>& left, const NiceContractor<T>& right)
{
   return left + NiceTensor<T>(right);
}

template <class T>
NiceTensor<T> operator-(const NiceTensor<T>& left, const NiceContractor<T>& right)
{
   return left - NiceTensor<T>(right);
}

#endif /* NICECONTRACTOR_H_INCLUDED */
