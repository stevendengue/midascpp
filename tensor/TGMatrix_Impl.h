#include <stdexcept>

#include"lapack_interface/math_wrappers.h"

template <class T>
TGMatrix<T>::TGMatrix(const unsigned int aNRow, const unsigned int aNCol):
   mNRow  {aNRow}
  ,mNCol  {aNCol}
  ,mMatrix{new T[aNRow*aNCol]}
{
}

template <class T>
TGMatrix<T>::TGMatrix(const unsigned int aNRow, const unsigned int aNCol, const T* aMatrix):
   mNRow  {aNRow}
  ,mNCol  {aNCol}
  ,mMatrix{aMatrix}
{
}

template <class T>
TGMatrix<T> matmul(const TGMatrix<T>& left,  const char left_mode,
                   const TGMatrix<T>& right, const char right_mode)
{
   // We need to get the right Fortran matrix layout; if we ask for a transpose
   // it is already transposed in memory.
   int fortran_left_nrow =(left_mode !='N')? left .mNRow:left .mNCol;
   int fortran_left_ncol =(left_mode !='N')? left .mNCol:left .mNRow;
   int fortran_right_nrow=(right_mode!='N')? right.mNRow:right.mNCol;
   int fortran_right_ncol=(right_mode!='N')? right.mNCol:right.mNRow;

   if( fortran_right_ncol != fortran_left_nrow )
   {
      throw std::invalid_argument(
            "The number of rows of left does not match the number of columns of right");
   }

   // We swap the arguments around to make sure that the result is not transposed.
   T* result=new double[ fortran_right_nrow * fortran_left_ncol ];
   T one (1.0);
   T zero(0.0);
   int ld_left =left .mNCol;
   int ld_right=right.mNCol;
   int ld_result=fortran_right_nrow;
   
   midas::lapack_interface::gemm(const_cast<char*> (&right_mode),
                                 const_cast<char*> (&left_mode)
                               , &fortran_right_nrow, &fortran_left_ncol, &fortran_right_ncol 
                               , &one
                               , const_cast<T*>( right.mMatrix.get() ), &ld_right
                               , const_cast<T*>( left .mMatrix.get() ), &ld_left
                               , &zero
                               , result,        &ld_result
                               );
   return TGMatrix<T>(fortran_left_ncol, fortran_right_nrow, result);

}

template <class T>
std::string TGMatrix<T>::Prettify() const
{
   std::stringstream output;

   const int width=10;

   output << std::setw(4) << this->mNRow << " X " << std::setw(4) << this->mNCol << std::endl;

   for(unsigned int j=0;j<( width+2 )*this->mNCol+2;j++)
      output << "_";

   const T* ptr=this->mMatrix.get();
   for(unsigned int i=0;i<this->mNRow;i++)
   {
      output << std::endl << "|";
      for(unsigned int j=0;j<this->mNCol;j++)
      {
         output << std::setw(width) << *(ptr++) << "  ";
      }
      output << "|";
   }

   output << std::endl << "|";
   for(unsigned int j=0;j<( width+2 )*this->mNCol;j++)
      output << "_";
   output << "|";

   return output.str();
}
