/**
************************************************************************
* 
* @file    C2tPreprocessor.cc
*
* @date    25-01-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Implementation of C2T rank-reduction methods.
*
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#include "tensor/C2tPreprocessor.h"
#include "tensor/TensorDirectProduct.h"
#include "tensor/TensorSum.h"
#include "util/CallStatisticsHandler.h"

#include "lapack_interface/GESVD.h"

namespace midas
{
namespace tensor
{

//! Register C2tPreprocessor
detail::ConcreteCpPreprocessorRegistration<C2tPreprocessor> registerC2tPreprocessor("C2T");


/**
 * Implementation of Type
 *
 * @return
 *    The type of preprocessor
 **/
std::string C2tPreprocessor::TypeImpl
   (
   )  const
{
   return std::string("C2tPreprocessor");
}

/**
 * Implementation of ModifiesTargetNorm
 *
 * @return
 *    True if the norm will be modified (always false for C2T)
 **/
bool C2tPreprocessor::ModifiesTargetNormImpl
   (
   )  const
{
   return false;
}

/**
 * Implementation of PreprocessImpl.
 * Calls the different rank-reduction methods.
 *
 * @param aTensor    The target tensor
 * @param aFitTensor The fitting tensor
 * @return
 *    True if tensor has been preprocessed
 **/
bool C2tPreprocessor::PreprocessImpl
   (  const NiceTensor<Nb>& aTensor
   ,  NiceTensor<Nb>& aFitTensor
   )
{
   auto ndim = aTensor.NDim();

   // We do not use C2T on vectors and matrices
   if (  ndim < 3
      )
   {
      return false;
   }

   // Save info about the original tensor for the later postprocessing
   this->mU.resize(ndim);
   this->mOriginalExtents = aTensor.GetDims();

   // Call specialized functions for different tensor formats
   bool processed = false;
   switch   (  aTensor.Type()
            )
   {
      case BaseTensor<Nb>::typeID::CANONICAL:
      {
         if (  aTensor.StaticCast<CanonicalTensor<Nb>>().GetRank() > 0
            )
         {
            processed = this->PreprocessCanonical(aTensor, aFitTensor);
         }

         break;
      }
      case BaseTensor<Nb>::typeID::DIRPROD:
      {
         if (  aTensor.StaticCast<TensorDirectProduct<Nb>>().Rank() > 0
            )
         {
            processed = this->PreprocessDirProd(aTensor, aFitTensor);
            break;
         }

         break;
      }
      case BaseTensor<Nb>::typeID::SUM:
      {
         if (  aTensor.StaticCast<TensorSum<Nb>>().Rank() > 0
            )
         {
            processed = this->PreprocessSum(aTensor, aFitTensor);
         }

         break;
      }
      default:
      {
         MidasWarning("C2tPreprocessor: No match for tensor type: " + aTensor.ShowType());
         break; // just break and return false
      }
   }

   return processed;
}

/**
 * Implementation of PreprocessImpl.
 * Calls the different rank-reduction methods.
 *
 * @param aTensor    The target tensor
 * @param aFitTensor The fitting tensor
 * @param aGuess     The guess tensor
 * @param aFitGuess  The preprocessed guess
 * @return
 *    True unless something went wrong.
 **/
bool C2tPreprocessor::PreprocessImpl
   (  const NiceTensor<Nb>& aTensor
   ,  NiceTensor<Nb>& aFitTensor
   ,  const NiceTensor<Nb>& aGuess
   ,  NiceTensor<Nb>& aFitGuess
   )
{
   MIDASERROR("C2tPreprocessor::PreprocessImpl: NOT IMPLEMENTED WITH GUESS!");
   return false;
}

/**
 * Rank reduction on CanonicalTensor
 *
 * @param aTensor       The target tensor
 * @param aFitTensor    The fitting tensor
 * @return
 *    True unless something went wrong.
 **/
bool C2tPreprocessor::PreprocessCanonical
   (  const NiceTensor<Nb>& aTensor
   ,  NiceTensor<Nb>& aFitTensor
   )
{
   LOGCALL("C2T CANONICAL");
   // do svd's
   const CanonicalTensor<Nb>& canonical_target_tensor = aTensor.StaticCast<CanonicalTensor<Nb> >();
   auto mode_matrices = canonical_target_tensor.GetModeMatrices();
   auto rank          = canonical_target_tensor.GetRank();
   auto& extents       = canonical_target_tensor.GetDims();
   auto ndim = aTensor.NDim();
   auto allocator_tp = canonical_target_tensor.GetAllocatorTp();
   auto allocator_tpp = canonical_target_tensor.GetAllocatorTpp();

   // Get threshold from error estimate
   // c2t_err <= sqrt(rank)*\sum_d (svd_err_d)
   auto thresh = this->mSvdThreshold / (Nb(ndim)*std::sqrt(Nb(rank)));

   std::vector<unsigned int> new_extents(ndim);
   Nb** new_mode_matrices = allocator_tpp.Allocate(ndim);
   for(int i = 0; i < ndim; ++i)
   { 
      auto extent = extents[i];
      auto size = extent*rank;
      auto mode_matrix = allocator_tp.AllocateUniqueArray(size);
      for(int j = 0; j < size; ++j)
      {
         mode_matrix[j] = mode_matrices[i][j];
      }
      auto svd = this->mSafeSvd ? GESVD(mode_matrix.get(), extent, rank) : GESDD(mode_matrix.get(), extent, rank);
      
      // Abort if SVD is not converged
      if (  svd.info != 0
         )
      {
         std::string routine = this->mSafeSvd ? "GESVD" : "GESDD";
         MidasWarning("CanonicalTensor C2T: "+routine+" info = "+std::to_string(svd.info)+". Abort preprocessing!");
         if (  svd.info > 0
            )
         {
            Nb* s_ptr = svd.s.get();
            Mout << " Print singular values:\n" << std::endl;
            auto num_vals = std::min(rank, extent);
            for(size_t j=0; j<num_vals; ++j)
            {
               Mout << *s_ptr++ << "   ";
            }
            Mout << std::endl;
         }
         Mout << " Tensor:\n" << aTensor << std::endl;

         return false;
      }

      auto svd_rank = RankAcc(svd, thresh);

      // DEBUG
//      {
//         auto svd_rank_old = Rank(svd, this->mSvdThreshold);
//
//         if (  svd_rank_old != svd_rank
//            )
//         {
//            Nb err_sq = C_0;
//            Nb err_sq_old = C_0;
//            for(int is=svd.Min()-1; is>=svd_rank; --is)
//            {
//               err_sq += svd.s[is]*svd.s[is];
//            }
//            for(int is=svd.Min()-1; is>=svd_rank_old; --is)
//            {
//               err_sq_old += svd.s[is]*svd.s[is];
//            }
//            Mout  << " C2T Canonical: Different rank for new method.\n"
//                  << "    SVD rank:          " << svd_rank << "\n"
//                  << "    SVD err:           " << std::sqrt(err_sq) << "\n"
//                  << "    Thresh:            " << thresh << "\n"
//                  << "    Old version rank:  " << svd_rank_old << "\n"
//                  << "    Old err:           " << std::sqrt(err_sq_old) << "\n"
//                  << std::flush;
//         }
//      }
      
      if (  svd_rank == 0
         )
      {
         MidasWarning(" C2T: PreprocessCanonical gives SVD rank = 0.");
         aFitTensor = NiceCanonicalTensor<Nb>(extents, 0);
         return true;
      }

      auto new_size = svd_rank * rank;
      new_mode_matrices[i] = allocator_tp.Allocate(new_size);

      // 'G = (U)^T * G
      {
         LOGCALL("gemm");
         char transa = 'T';
         char transb = 'N';
         Nb alpha = static_cast<Nb>(1.0);
         Nb beta  = static_cast<Nb>(0.0);
         int gemm_svd_rank = svd_rank;
         int gemm_rank = rank;
         int gemm_extent = extent;

         midas::lapack_interface::gemm( &transa, &transb, &gemm_svd_rank, &gemm_rank, &gemm_extent
                                      , &alpha 
                                      , svd.u.get(), &gemm_extent
                                      , mode_matrices[i], &gemm_extent
                                      , &beta , new_mode_matrices[i], &gemm_svd_rank
                                      );

      }

      this->mU[i] = std::move(svd.u);
      new_extents[i] = svd_rank;
   }
   
   // create projected tensor
   aFitTensor = NiceCanonicalTensor<Nb>(new_extents, rank, new_mode_matrices);

   return true;
}


/**
 * Rank reduction on TensorDirectProduct
 *
 * @param aTensor       The target tensor
 * @param aFitTensor    The fitting tensor
 * @return
 *    True unless something went wrong.
 **/
bool C2tPreprocessor::PreprocessDirProd
   (  const NiceTensor<Nb>& aTensor
   ,  NiceTensor<Nb>& aFitTensor
   )
{
   LOGCALL("C2T DIRPROD");

   // Flag for error in SVD
   bool svd_error = false;

   // Exit if not all tensors are CanonicalTensor
   const TensorDirectProduct<Nb>& tensor_dirprod = aTensor.StaticCast<const TensorDirectProduct<Nb> >();
   if(!tensor_dirprod.AllCanonical())
   {
      return false;
   }
   
   auto& tensors = tensor_dirprod.GetTensors();
   auto ntensors = tensors.size();
   auto threshold = this->mSvdThreshold;
   std::vector<std::vector<std::unique_ptr<Nb[]> > > u(ntensors);
   std::vector<std::unique_ptr<BaseTensor<Nb> > > new_tensors(ntensors);
   for(int itensors = 0; itensors < ntensors; ++itensors)
   {
      LOGCALL("projected tensor");
      new_tensors[itensors] = std::unique_ptr<BaseTensor<Nb> >
                                 (  this->ProjectedTensorCanonicalPtr
                                       (  static_cast<CanonicalTensor<Nb>*>(tensors[itensors].get())
                                       ,  u[itensors]
                                       ,  svd_error
                                       )
                                 );
   }
   
   // Abort preprocessing if an error has occured in the SVD
   if (  svd_error   )
   {
      MidasWarning("C2tPreprocessor: Error in TensorDirectProduct preprocessing. Abort!");
      return false;
   }

   // create new dims
   auto ndim = tensor_dirprod.NDim();
   auto connection = tensor_dirprod.GetConnection();
   std::vector<unsigned int> new_extents(ndim);
   for(int i = 0; i < ndim; ++i)
   {
      new_extents[i] = new_tensors[connection[i].first]->GetDims()[connection[i].second];
   }

   // Create U matrices
   auto& connection2 = tensor_dirprod.GetConnection();
   for(int i = 0; i < ndim; ++i)
   {
      this->mU[i] = std::move(u[connection2[i].first][connection2[i].second]);
   }
   
   aFitTensor = NiceTensorDirectProduct<Nb>(new_extents, tensor_dirprod.Coef(), std::move(new_tensors), std::move(connection));

   return true;
}

/**
 * Perform SVDs on mode matrices.
 *
 * @param aTensor    Pointer to CanonicalTensor in TensorDirectProduct
 * @param aAuxU      Auxiliary U matrices
 * @param arError    Will be set true if an error occurs during the SVD
 * @return
 *    Pointer to projected tensor.
 **/
CanonicalTensor<Nb>* C2tPreprocessor::ProjectedTensorCanonicalPtr
   (  const CanonicalTensor<Nb>* aTensor
   ,  std::vector<std::unique_ptr<Nb[]>>& aAuxU
   ,  bool& arError
   )
{
   // do svd's
   const CanonicalTensor<Nb>& canonical_target_tensor = *aTensor;
   auto mode_matrices = canonical_target_tensor.GetModeMatrices();
   auto rank          = canonical_target_tensor.GetRank();
   auto& extents       = canonical_target_tensor.GetDims();
   auto ndim = aTensor->NDim();
   auto allocator_tp = canonical_target_tensor.GetAllocatorTp();
   auto allocator_tpp = canonical_target_tensor.GetAllocatorTpp();

   // Get threshold from error estimate
   // c2t_err <= sqrt(rank)*\sum_d (svd_err_d)
   auto full_dim = this->mOriginalExtents.size();
   auto thresh = this->mSvdThreshold / (Nb(full_dim)*std::sqrt(Nb(rank)));

   aAuxU.resize(ndim);
   std::vector<unsigned int> new_extents(ndim);
   Nb** new_mode_matrices = allocator_tpp.Allocate(ndim);
   for(int i = 0; i < ndim; ++i)
   { 
      auto extent = extents[i];
      auto size = extent*rank;
      auto mode_matrix = allocator_tp.AllocateUniqueArray(size);
      for(int j = 0; j < size; ++j)
      {
         mode_matrix[j] = mode_matrices[i][j];
      }
      auto svd = this->mSafeSvd ? GESVD(mode_matrix.get(), extent, rank) : GESDD(mode_matrix.get(), extent, rank);

      if (  svd.info != 0  )
      {
         std::string routine = this->mSafeSvd ? "GESVD" : "GESDD";
         MidasWarning(" TensorDirectProduct C2T: "+routine+" info = "+std::to_string(svd.info)+".");

         if (  svd.info > 0
            )
         {
            Nb* s_ptr = svd.s.get();
            Mout << " Print singular values:\n" << std::endl;
            auto num_vals = std::min(rank, extent);
            for(size_t j=0; j<num_vals; ++j)
            {
               Mout << *s_ptr++ << "   ";
            }
            Mout << std::endl;
         }

         arError = true;
      }

      auto svd_rank = RankAcc(svd, thresh);

      // DEBUG
//      {
//         auto svd_rank_old = Rank(svd, this->mSvdThreshold);
//         if (  svd_rank_old != svd_rank
//            )
//         {
//            Nb err_sq = C_0;
//            Nb err_sq_old = C_0;
//            for(int is=svd.Min()-1; is>=svd_rank; --is)
//            {
//               err_sq += svd.s[is]*svd.s[is];
//            }
//            for(int is=svd.Min()-1; is>=svd_rank_old; --is)
//            {
//               err_sq_old += svd.s[is]*svd.s[is];
//            }
//            Mout  << " C2T dirprod: Different rank with new method.\n"
//                  << "    SVD rank:          " << svd_rank << "\n"
//                  << "    SVD err:           " << std::sqrt(err_sq) << "\n"
//                  << "    Thresh:            " << thresh << "\n"
//                  << "    Old version rank:  " << svd_rank_old << "\n"
//                  << "    Old err:           " << std::sqrt(err_sq_old) << "\n"
//                  << std::flush;
//         }
//      }

      auto new_size = svd_rank * rank;
      new_mode_matrices[i] = allocator_tp.Allocate(new_size);

      // 'G = (U)^T * G
      {
         char transa = 'T';
         char transb = 'N';
         Nb alpha = static_cast<Nb>(1.0);
         Nb beta  = static_cast<Nb>(0.0);
         int gemm_svd_rank = svd_rank;
         int gemm_rank = rank;
         int gemm_extent = extent;

         midas::lapack_interface::gemm( &transa, &transb, &gemm_svd_rank, &gemm_rank, &gemm_extent
                                      , &alpha 
                                      , svd.u.get(), &gemm_extent
                                      , mode_matrices[i], &gemm_extent
                                      , &beta , new_mode_matrices[i], &gemm_svd_rank
                                      );

      }

      aAuxU[i] = std::move(svd.u);
      new_extents[i] = svd_rank;
   }
   
   // create projected tensor
   return new CanonicalTensor<Nb>(new_extents, rank, new_mode_matrices);
}


/**
 * Preprocess TensorSum
 * 
 * @param aTensor    The target tensor
 * @param aFitTensor The fitting tensor
 * @return
 *    True unless something went wrong.
 **/
bool C2tPreprocessor::PreprocessSum
   (  const NiceTensor<Nb>& aTensor
   ,  NiceTensor<Nb>& aFitTensor
   )
{
   LOGCALL("C2T SUM");
   // Exit if not all tensors are CanonicalTensor
   const TensorSum<Nb>& tensor_sum = aTensor.StaticCast<TensorSum<Nb> >();
   if(!tensor_sum.AllCanonical())
   {
      return false;
   }

   // make CanonicalTensor
   NiceTensor<Nb> canonical_tensor;
   {
   LOGCALL("construct mode matrices");
   canonical_tensor = NiceTensor<Nb>(tensor_sum.ConstructModeMatrices());
   }

   // do rank reduction
   bool result = true;
   {
   LOGCALL("c2t new canonical");
   result = this->PreprocessCanonical(canonical_tensor, aFitTensor);
   }

   return result;
}


/**
 * Postprocess the tensor by contracting the mode matrices
 * with mU.
 *
 * @param aTensor    The decomposed core tensor.
 * @return
 *    True unless something went wrong.
 **/
bool C2tPreprocessor::PostprocessImpl
   (  NiceTensor<Nb>& aTensor
   )  const
{
   LOGCALL("calls");

   // "up"-contract to full tensor
   const CanonicalTensor<Nb>& canonical_fit = aTensor.StaticCast<const CanonicalTensor<Nb> >();
   auto ndim = aTensor.NDim();
   auto& new_extents = aTensor.GetDims();
   auto fit_mode_matrices = canonical_fit.GetModeMatrices();
   auto fit_rank = canonical_fit.GetRank();
   auto allocator_tp = canonical_fit.GetAllocatorTp();
   auto allocator_tpp = canonical_fit.GetAllocatorTpp();
   
   Nb** up_contracted_mode_matrices = allocator_tpp.Allocate(ndim);

   for(int i = 0; i < ndim; ++i)
   {
      auto extent = this->mOriginalExtents[i];
      auto new_size = extent * fit_rank;
      up_contracted_mode_matrices[i] = allocator_tp.Allocate(new_size);
      
      // G = U * 'G
      {
         char transa = 'N';
         char transb = 'N';
         Nb alpha = static_cast<Nb>(1.0);
         Nb beta  = static_cast<Nb>(0.0);
         int gemm_extent = extent;
         int gemm_fit_rank = fit_rank;
         int gemm_svd_rank = new_extents[i];

         midas::lapack_interface::gemm( &transa, &transb, &gemm_extent, &gemm_fit_rank, &gemm_svd_rank
                                      , &alpha 
                                      , this->mU[i].get(), &gemm_extent
                                      , fit_mode_matrices[i], &gemm_svd_rank
                                      , &beta , up_contracted_mode_matrices[i], &gemm_extent
                                      );

      }
   }
   
   aTensor = NiceCanonicalTensor<Nb>(this->mOriginalExtents, fit_rank, up_contracted_mode_matrices);

   return true;
}


/**
 * Constructor from TensorDecompInfo.
 *
 * @param aInfo      The TensorDecompInfo
 **/
C2tPreprocessor::C2tPreprocessor
   (  const TensorDecompInfo& aInfo
   )
   :  mSvdThreshold           (const_cast<TensorDecompInfo&>(aInfo)["C2TSVDTHRESHOLD"].get<Nb>())
   ,  mSafeSvd                (const_cast<TensorDecompInfo&>(aInfo)["C2TSAFESVD"].get<bool>())
{
}

} /* namespace tensor */
} /* namespace midas */
