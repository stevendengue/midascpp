/**
 ************************************************************************
 * 
 * @file                CanonicalTensor_Fits.h
 *
 * Created:             25.11.2014
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * Short Description:   FitALS, FitOptimize
 * 
 * Last modified: Wed Nov 26, 2014  18:44
 * 
 * Detailed Description: FitXXX fits to another Tensor target of "any" kind
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef CANONICALTENSORFITS_H_INCLUDED
#define CANONICALTENSORFITS_H_INCLUDED

#include <fstream>

#include "util/Io.h"
#include "util/CallStatisticsHandler.h"

#include"CanonicalTensor_SolverWrapper.h"
#include"lapack_interface/math_wrappers.h"

#include"solver/steepest_descent.h"
#include"solver/fletcher_reeves.h"
#include"solver/polak_ribiere.h"
#include"solver/fr_pr.h"
#include"solver/hestenes_stiefel.h"
#include"solver/dai_yuang.h"
#include"solver/hager_zhang.h"

#include "tensor/FitALS.h"
#include "tensor/FitNCG.h"
#include "tensor/GammaMatrix.h"
#include "tensor/Squeezer.h"

#include "util/CallStatisticsHandler.h"

/**
 * Direction to minimize L^2 error to a reference tensor.
 * The requirement for the class of the target tensor T is to implement
 * SqueezeNewModeMatrix
 *
 * @param gradient      Pre-allocated tensor to store gradient
 * @param intermeds     Intermediates for the gamma matrix
 * @param squeezer      Squeezer to construct overlaps
 * @param gamma_work    Workspace to store GammaMatrix
 * @param squeezer_work Workspace to store SqueezedModeMatrix
 * @param scaling       Scale gradient by this factor
 * @param apDisable
 **/
template
   <  class T
   >
template
   <  typename SmartPtr
   ,  typename U
   >
void CanonicalTensor<T>::Gradient
   (  CanonicalTensor<T>& gradient
   ,  GeneralGammaIntermediates<U>& intermeds
   ,  Squeezer<U>& squeezer
   ,  SmartPtr& gamma_work
   ,  SmartPtr& squeezer_work
   ,  T scaling
   ,  midas::type_traits::DisableIfComplexT<U>* apDisable
   )  const
{
   auto ndim = this->NDim();

   for(unsigned idim=0; idim<ndim; ++idim)
   {
      // Contract all but one
      squeezer.SqueezeModeMatrix(idim, squeezer_work, true);

      // Put mode matrix
      gradient.PutNewTransposeModeMatrix(squeezer_work.get(), idim);

      // Gamma matrix
      intermeds.GammaMatrix(idim, gamma_work);

      // Multiply mode matrix with gamma matrix
      char nc='N';
      T one =static_cast<T>(1.0)*scaling;
      T m_one =static_cast<T>(-1.0)*scaling;
      int m=gradient.mDims[idim];
      int n=gradient.mRank;
      int k=gradient.mRank;
      midas::lapack_interface::gemm(&nc, &nc, &m, &n, &k,
                                    &one,
                                    this->mModeMatrices[idim], &m,
                                    gamma_work.get(),         &k,
                                    &m_one,
                                    gradient.mModeMatrices[idim], &m );
   }
}


/**
 * Single block of CanonicalTensor gradient.
 * Store gradient block in squeezed.
 *
 * @param idim       The block to calculate
 * @param gamma      Gamma matrix
 * @param squeezed   Squeezed mode matrix for RHS calculation. Used to store result.
 * @param scaling    Scale result
 * @param transpose  Calculate the transposed gradient mode matrix (NB: squeezed must be transposed beforehand)
 * @param apDisable
 **/
template
   <  class T
   >
template
   <  typename SmartPtr
   ,  typename U
   >
void CanonicalTensor<T>::Gradient
   (  unsigned idim
   ,  SmartPtr& gamma
   ,  SmartPtr& squeezed
   ,  T scaling
   ,  bool transpose
   ,  midas::type_traits::DisableIfComplexT<U>* apDisable
   )  const
{
   if (  transpose   )
   {
      char nc='N';
      char nt='T';
      T alpha =static_cast<T>(1.0)*scaling;
      T beta =static_cast<T>(-1.0)*scaling;
      int m=this->mRank;
      int n=this->mDims[idim];
      int k=this->mRank;
      midas::lapack_interface::gemm(&nc, &nt, &m, &n, &k,
                                    &alpha,
                                    gamma.get(), &m,
                                    this->mModeMatrices[idim], &n,
                                    &beta,
                                    squeezed.get(), &m );
   }
   else
   {
      // Multiply mode matrix with gamma matrix and add to -1*squeezed
      char nc='N';
      T alpha =static_cast<T>(1.0)*scaling;
      T beta =static_cast<T>(-1.0)*scaling;
      int m=this->mDims[idim];
      int n=this->mRank;
      int k=this->mRank;
      midas::lapack_interface::gemm(&nc, &nc, &m, &n, &k,
                                    &alpha,
                                    this->mModeMatrices[idim], &m,
                                    gamma.get(),         &k,
                                    &beta,
                                    squeezed.get(), &m );
   }
}

/**
 * Compute gradient without passing intermediates
 *
 * @param ref           The reference tensor
 * @param apDisable
 * @return
 *    The gradient as CanonicalTensor
 **/
template <class T>
template <class U>
CanonicalTensor<T> CanonicalTensor<T>::Gradient
   (  const BaseTensor<T>& ref
   ,  midas::type_traits::DisableIfComplexT<U>* apDisable
   )  const
{
   assert_same_shape(*this, ref);

   CanonicalTensor<T> result{this->mDims, this->mRank};
   auto max_extent = this->MaxExtent();
   auto rank = this->mRank;
   auto ndim = this->NDim();

   unsigned prev_num_threads = 1;

//   #pragma omp parallel shared(result)
   {
      auto new_mat = Base::Allocator_Tp::AllocateUniqueArray(rank * max_extent);
      auto gamma = Base::Allocator_Tp::AllocateUniqueArray(rank * rank);

//      #pragma omp for
      for(unsigned int idim=0; idim<ndim; ++idim)
      {
         unsigned extent = result.mDims[idim];

         // Contract all but one
         SqueezeNewModeMatrix(*this, ref, idim, new_mat.get());

         // Gamma matrix
         GammaMatrix(this, this, idim, gamma);

         // Multiply mode matrix with gamma matrix
         char nc='N';
         T one =static_cast<T>(1.0);
         T zero=static_cast<T>(0.0); // This can be changed to lambda (Tikhonov regularization term (16) in Kolda)
         int m=extent;
         int n=result.mRank;
         int k=result.mRank;
         midas::lapack_interface::gemm(&nc, &nc, &m, &n, &k,
                                       &one,
                                       mModeMatrices[idim], &m,
                                       gamma.get(),         &k,
                                       &zero,
                                       result.mModeMatrices[idim], &m );
         // Subtract new_mat
         T *out_ptr=result.mModeMatrices[idim];
         T *in_ptr =new_mat.get();
         for (unsigned int irank = 0; irank < result.mRank; ++irank)
         {
            for (unsigned int i = 0; i < extent; ++i)
            {
               *(out_ptr++) -= *(in_ptr++);
            }
         }
      }
   }

   return result;
}

/**
 * @param target
 *    Target tensor
 * @param maxiter
 *    Max. iterations
 * @param maxerr
 *    Max. error
 * @param apDisable
 * @return
 *    FitReport of fitting
 **/
template <class T>
template <class U>
FitReport<U> CanonicalTensor<T>::FitALS
   (  const BaseTensor<T>& target
   ,  const unsigned int maxiter
   ,  const real_t maxerr
   ,  midas::type_traits::DisableIfComplexT<U>* apDisable
   )
{
   return this->FitALS(target, target.Norm2(), maxiter, maxerr, 1e-12);
}

/** 
 * For each dimension j, solve the system of linear equations
 *  \f[
 *     \mathbf{Q}^{(j)}=\mathsf{T}^{(j)}\mathbf{\Gamma}^{(j)}
 *  \f]
 *
 * Q is computed via CanonicalTensor<T>::SqueezeNewModeMatrix and \f$Gamma\f$ by
 * CanonicalTensor<T>::gamma_matrix.
 *
 * This is done iteratively for all k's, until converging or running out of iterations.
 *
 * @param target     Target tensor
 * @param norm2      Squared norm of target
 * @param maxiter    Max iterations
 * @param maxerr     Max error
 * @param rcond      Rcond for GELSS routine
 * @param apDisable
 * @return
 *    Fit report
 * */
template <class T>
template <class U>
FitReport<U> CanonicalTensor<T>::FitALS
   (  const BaseTensor<T>& target
   ,  const real_t norm2
   ,  const unsigned int maxiter
   ,  const real_t maxerr
   ,  const real_t rcond
   ,  midas::type_traits::DisableIfComplexT<U>* apDisable
   )
{
   return ::FitALS(&target, norm2, this, maxiter, maxerr, rcond);
}

/**
 * @param target     Target tensor
 * @param maxiter    Max. iterations
 * @param maxerr     Max. error
 * @param apDisable
 * @return           FitReport of final fit
 **/
template <class T>
template <class U>
FitReport<U> CanonicalTensor<T>::FitSteepestDescent
   (  const BaseTensor<T>& target
   ,  const unsigned int maxiter
   ,  const real_t maxerr
   ,  midas::type_traits::DisableIfComplexT<U>* apDisable
   )
{
   assert_same_shape(*this, target);
   auto target_norm2 = target.Norm2();
   auto norm = std::sqrt(target_norm2);
   if(this->mRank==0)
   {
      return FitReport<T>{norm, norm, 0};
   }

   *this=
      steepest_descent( CTEvaluator<BaseTensor<T>, T>(target, target_norm2),
                     CanonicalTensor_SolverWrapper<T>(*this),
                     1.e-13, false).mTensor;
   return FitReport<T>{diff_norm_new(this,this->Norm2(),&target,target_norm2), norm, 0};
}

/**
 * @param target     Target tensor
 * @param maxiter    Max. iterations
 * @param maxerr     Max. error
 * @param apDisable
 * @return
 *    FitReport of final fit.
 **/
template <class T>
template <class U>
FitReport<U> CanonicalTensor<T>::FitNCG
   (  const BaseTensor<T>& target
   ,  const unsigned int maxiter
   ,  const real_t maxerr
   ,  midas::type_traits::DisableIfComplexT<U>* apDisable
   )
{
   return ::FitNCG(&target, target.Norm2(), this, CpNCGInput<T>().SetMaxerr(maxerr));
}

#endif /* CANONICALTENSORFITS_H_INCLUDED */
