/**
************************************************************************
* 
* @file    NestedCpDriver.h
*
* @date    28-03-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Driver for fitting a tensor to CP format with a given accuracy using 
*     the nested CP scheme by Ostrowski et al.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef NESTEDCPDRIVER_H_INCLUDED
#define NESTEDCPDRIVER_H_INCLUDED

#include "tensor/TensorDecomposer.h"
#include "tensor/TensorDecompInfo.h"
#include "tensor/FitReport.h"

namespace midas
{
namespace tensor
{

/**
 * Driver class for nested CP decomposition.
 **/
class NestedCpDriver
   :  public detail::TensorDecompDriverBase
{
   private:
      //! Maximum allowed rank
      In mNestedCpMaxRank;

      //! Rank incr.
      In mNestedCpRankIncr;

      //! Scale rank incr by tensor order (starting from order 3)
      bool mNestedCpScaleRankIncr;

      //! Scale rank incr by tensor order (starting from order 3) and smallest dimension
      bool mNestedCpAdaptiveRankIncr;

      //! Requested accuracy
      Nb mNestedCpResThreshold;

      //! Re-calculate residual norm in each iteration
      bool mSafeResidualNorm;

      //! Balance fit and residual after updates
      bool mBalance;

      //! Allow refitting of total result if the error is too large
      bool mAllowRefit;

      //! Print FitReport after decomposition
      bool mPrintFitReport;

      //! Check decomposition
      bool CheckDecompositionImpl
         (  const NiceTensor<Nb>&
         )  const override;

      //! Type
      std::string TypeImpl
         (
         )  const override;

      //! Decompose
      NiceTensor<Nb> DecomposeImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  Nb
         )  const override;

      //! Decompose with argument guess
      NiceTensor<Nb> DecomposeImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  const NiceTensor<Nb>&
         ,  Nb
         )  const override;

      //! Check if tensor should be fitted to rank-0
      bool ScreenImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  Nb
         )  const override;

      //! Get threshold
      Nb GetThreshold
         (
         )  const override;

   public:
      //! Constructor from TensorDecompInfo
      NestedCpDriver
         (  const TensorDecompInfo&
         );
};

} /* namespace tensor */
} /* namespace midas */

#endif /* NESTEDCPDRIVER_H_INCLUDED */
