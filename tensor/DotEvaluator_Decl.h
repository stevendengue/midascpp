#ifndef DOTEVALUATOR_DECL_H_INCLUDED
#define DOTEVALUATOR_DECL_H_INCLUDED

template
   <  class T
   >
struct DotType
{
   using type = T;
};

template
   <  class T
   >
class DotEvaluator
{
   public:
      using dot_t = typename DotType<T>::type;

      // BaseTensor
      dot_t Dot
         (  const BaseTensor<T>* const
         ,  const BaseTensor<T>* const
         )  const;

      dot_t Dot
         (  const BaseTensor<T>* const
         ,  const SimpleTensor<T>* const
         )  const;

      dot_t Dot
         (  const BaseTensor<T>* const
         ,  const CanonicalTensor<T>* const
         )  const;

      dot_t Dot
         (  const BaseTensor<T>* const
         ,  const Scalar<T>* const
         )  const;

      dot_t Dot
         (  const BaseTensor<T>* const
         ,  const ZeroTensor<T>* const
         )  const;

      dot_t Dot
         (  const BaseTensor<T>* const
         ,  const TensorSum<T>* const
         )  const;

      // SimpleTensor
      dot_t Dot
         (  const SimpleTensor<T>* const
         ,  const SimpleTensor<T>* const
         )  const;

      dot_t Dot
         (  const SimpleTensor<T>* const
         ,  const CanonicalTensor<T>* const
         )  const;

      dot_t Dot
         (  const SimpleTensor<T>* const
         ,  const Scalar<T>* const
         )  const;

      dot_t Dot
         (  const SimpleTensor<T>* const
         ,  const ZeroTensor<T>* const
         )  const;
      
      // CanonicalTensor
      dot_t Dot
         (  const CanonicalTensor<T>* const
         ,  const CanonicalTensor<T>* const
         )  const;

      dot_t Dot
         (  const CanonicalTensor<T>* const
         ,  const Scalar<T>* const
         )  const;

      dot_t Dot
         (  const CanonicalTensor<T>* const
         ,  const ZeroTensor<T>* const
         )  const;

      dot_t Dot
         (  const CanonicalTensor<T>* const
         ,  const TensorDirectProduct<T>* const
         )  const;

      template
         <  template<class> class IMPL
         >
      dot_t Dot
         (  const CanonicalTensor<T>* const
         ,  const EnergyHolderTensorBase<T, IMPL>* const
         )  const;

      // TensorDirectProduct
      dot_t Dot
         (  const TensorDirectProduct<T>* const
         ,  const TensorDirectProduct<T>* const
         )  const;
      
      // TensorSum
      dot_t Dot
         (  const TensorSum<T>* const
         ,  const TensorSum<T>* const
         )  const;
};

#endif /* DOTEVALUATOR_DECL_H_INCLUDED */
