/**
 ************************************************************************
 * 
 * @file                ContractionIndices.h
 *
 * Created:             30.04.2015
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * \brief               ContractionIndex and ContractionIndices: indices
 *                      for convenient contraction of NiceTensor's.
 * 
 * Last modified: Wed Oct 22, 2014  18:44
 * 
 * Detailed Description: BaseTensor and derived types are not supposed
 *                       to be used directly, except when directly dealing
 *                       with the particular tensor representation. Use
 *                       NiceTensor.
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef CONTRACTIONINDICES_H_INCLUDED
#define CONTRACTIONINDICES_H_INCLUDED

#include<vector>

/// Wrapper around `char` to generate NiceContractors. Element of ContractionIndices.
class ContractionIndex
{
   private:
      char mChar;
   public:
      ContractionIndex(const char aChar);
      char GetChar() const;
      bool operator ==(const ContractionIndex& other);
};

/// Vector of ContractionIndex's. See NiceTensor::operator[]().
class ContractionIndices: public std::vector<ContractionIndex>
{
   public:
      /// Default (length-0) constructor.
      ContractionIndices();
      /// Copy constructor.
      ContractionIndices(const std::vector<ContractionIndex>& orig);
      /// Append a ContractionIndex into a ContractionIndices instance.
      ContractionIndices operator ,(ContractionIndex i);
      /// Convert to std::vector<unsigned int> (needed to interface with the low level BaseTensor \link BaseTensor.h::contract() contract()\endlink).
      std::vector<unsigned int> ToUIntVector() const;
};

/// Create a ContractionIndices instance from two ContractionIndex's.
ContractionIndices operator ,(ContractionIndex i1, ContractionIndex i2);
/// Returns all indices present in indices1 which are not present in indices2.
ContractionIndices set_difference(const ContractionIndices& indices1, const ContractionIndices& indices2 );
/// Returns all ContractionIndices present in either indices1 or indices2 but not both.
ContractionIndices sorted_symmetric_difference(const ContractionIndices& indices1, const ContractionIndices& indices2 );

/// The alphabet, just for convenience.
/*! Suggested usage:
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
 *      ...
 *      namespace X=contraction_indices;
 *      ...
 *      contract(a[X::i,X::a,X::b,X::j], b[X::q,X::p,X::i,X::j]);
 *      ...
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  Of course it is possible to import the whole namespace, i.e.,
 *  `using namespace contraction_indices`. However, this is not recommended
 *  because it will clash with variables called `a` or `i` etc.
 */
namespace contraction_indices
{
   const ContractionIndex a('a');
   const ContractionIndex b('b');
   const ContractionIndex c('c');
   const ContractionIndex d('d');
   const ContractionIndex e('e');
   const ContractionIndex f('f');
   const ContractionIndex g('g');
   const ContractionIndex h('h');
   const ContractionIndex i('i');
   const ContractionIndex j('j');
   const ContractionIndex k('k');
   const ContractionIndex l('l');
   const ContractionIndex m('m');
   const ContractionIndex n('n');
   const ContractionIndex o('o');
   const ContractionIndex p('p');
   const ContractionIndex q('q');
   const ContractionIndex r('r');
   const ContractionIndex s('s');
   const ContractionIndex t('t');
   const ContractionIndex u('u');
   const ContractionIndex v('v');
   const ContractionIndex w('w');
   const ContractionIndex x('x');
   const ContractionIndex y('y');
   const ContractionIndex z('z');
}

#endif /* CONTRACTIONINDICES_H_INCLUDED */
