#ifndef ENERGYDENOMINATORTENSOR_INCLUDED
#define ENERGYDENOMINATORTENSOR_INCLUDED

#include<cmath>
#include<vector>
#include<utility>

#include "CanonicalTensor.h"
#include "SimpleTensor.h"
#include "tensor/EnergyHolderTensor.h"
#include "LaplaceQuadrature.h"
#include "tensor/TensorFits.h"

// Forward declarations
std::vector<unsigned int> edt_dims
   (  const unsigned int order
   ,  const unsigned int nvir
   ,  const unsigned int nocc
   );

std::vector<unsigned int> edt_dims
   (  const std::vector<In>&
   ,  const std::vector<In>&
   );

template
   <  class T
   >
class ExactEnergyDenominatorTensor;

/**
 *
 **/
template
   <  class T
   >
class EnergyDenominatorTensor
   :  public CanonicalTensor<T>
{
   private:
      //! Construct tensor for 1st-order, rank-1 case (the tensor is just a vector)
      void ConstructEnergyDenominatorVector
         (  In aMode
         ,  const DataCont& aEigVals
         ,  const std::vector<In>& aOffsets
         ,  T aDiagEnergy
         )
      {
         // Allocate rank-1 mode matrices
         if (  this->mRank != 1
            )
         {
            MidasWarning("LaplaceQuadrature for 1st-order tensor has more than 1 point!");
            this->SetNewRank(1);
         }

         // Set values of mModeMatrices
         Nb val = C_0;
         auto size = this->Extent(0);
         auto offset = aOffsets[aMode];
         for(size_t imodal=0; imodal<size; ++imodal)
         {
            // Get eigenvalue
            aEigVals.DataIo(IO_GET, offset+imodal+1, val);

            // Set tensor element
            this->mModeMatrices[0][imodal] = static_cast<T>(1.) / (val - aDiagEnergy);
         }
      }

   public:
      //! Constructor for Electronic-Structure calculations. Takes order, orbital energies and LaplaceQuadrature.
      EnergyDenominatorTensor
         (  const unsigned int order
         ,  const std::vector<T>& vir
         ,  const std::vector<T>& occ
         ,  const LaplaceQuadrature<T>& quad
         ,  T shift = static_cast<T>(0.)
         )
         :  CanonicalTensor<T>
               (  edt_dims
                     (  order
                     ,  vir.size()
                     ,  occ.size()
                     )
               ,  quad.NumPoints()
               )
  
      {
         if (  quad.IsNullPtr()
            )
         {
            MIDASERROR("Empty LaplaceQuadrature passed to EnergyDenominatorTensor constructor...");

            // Niels: We could consider implementing a ConstructByCpDecomposition method for electronic-structure theory as well...
            // ...
         }
         else
         {
            auto shift_part = shift / this->NDim();

            // First matrix for occupied
            quad.MakeMatrix(vir, shift_part, this->mModeMatrices[0]);
            // Second matrix for virtual
            quad.MakeMatrix(occ, shift_part, this->mModeMatrices[1]);
            // Copy matrices for higher order
            for(In iorder=1; iorder<order; ++iorder)
            {
               for(In i=0;i<2;++i)
               {
                  T* in =this->mModeMatrices[i];
                  T* out=this->mModeMatrices[2*iorder+i];
                  while( in < this->mModeMatrices[i] + this->mRank * this->mDims[i] )
                  {
                     *(out++)=*(in++);
                  }
               }
            }

            // Calculate weight roots to be multiplied on each mode matrix to keep the norms similar
            auto ndim = this->NDim();
            auto num_points = quad.NumPoints();
            std::vector<Nb> weight_roots(num_points);
            for(In i = 0; i<num_points; ++i)
            {
               // Take the roots of the positive weights. If a sign occurs, multiply -1 on the first mode matrix later.
               weight_roots.at(i) = std::pow(std::abs(quad.Weights()[i]), C_1/ndim);
            }

            // Distribute weights
            for(size_t imat=0; imat<ndim; ++imat)
            {
               T* ptr=this->mModeMatrices[imat];
               for(unsigned int irank=0;irank<this->mRank;++irank)
               {
                  T sign = (quad.Weights().at(irank) > 0) ? static_cast<T>(C_1) : static_cast<T>(-C_1);
                  for(unsigned int idim=0;idim<this->mDims[imat];++idim)
                  {
                     if(imat == 0)  *(ptr++)*=sign*weight_roots[irank];
                     else           *(ptr++)*=weight_roots[irank];
                  }
               }
            }
         }
      }

      //! Constructor for Electronic-Structure calculations from order, orbital energies, and number of quadrature points
      EnergyDenominatorTensor
         (  const unsigned int order
         ,  const std::vector<T>& vir
         ,  const std::vector<T>& occ
         ,  const unsigned int numquadpoints
         ,  T shift = static_cast<T>(0.)
         )
         :  EnergyDenominatorTensor<T>
               (  order
               ,  vir
               ,  occ
               ,  LaplaceQuadrature<T>
                  (  midas::tensor::LaplaceInfo("BESTLAP", 1.e-1)
                  ,  order
                  ,  vir
                  ,  occ
                  ,  shift
                  )
               ,  shift
               )
      {
      }

      //! Constructor from LaplaceInfo
      EnergyDenominatorTensor
         (  const midas::tensor::LaplaceInfo& lapinfo
         ,  unsigned order
         ,  const std::vector<T>& vir
         ,  const std::vector<T>& occ
         ,  T shift = static_cast<T>(0.)
         )
         :  EnergyDenominatorTensor<T>
               (  order
               ,  vir
               ,  occ
               ,  LaplaceQuadrature<T>
                     (  lapinfo
                     ,  order
                     ,  vir
                     ,  occ
                     ,  shift
                     )
               ,  shift
               )
      {
      }


      ///> Constructor for vibrational structure calculations.
      EnergyDenominatorTensor
         (  const std::vector<In>& aModes
         ,  const std::vector<In>& aNmodals
         ,  const DataCont& aEigVals
         ,  const std::vector<In>& aOffsets
         ,  const LaplaceQuadrature<T>& arQuad
         ,  T aDiagEnergy = static_cast<T>(0.)
         )
         :  CanonicalTensor<T>
               (  edt_dims
                     (  aModes
                     ,  aNmodals
                     )
               ,  arQuad.IsNullPtr() ? 1 : arQuad.NumPoints()
               )
      {
         auto num_dims = aModes.size();

         // Construct vectors exactly
         if (  num_dims == 1
            )
         {
            this->ConstructEnergyDenominatorVector(aModes[0], aEigVals, aOffsets, aDiagEnergy);
         }
         else if  (  arQuad.IsNullPtr()
                  )
         {
            if (  gDebug
               )
            {
               Mout  << "Empty quadrature passed to EnergyDenominatorTensor constructor. Use CP decomposition!" << std::endl;
            }
            this->ConstructByCpDecomposition(aModes, aEigVals, aOffsets, 4, aDiagEnergy);
         }
         else
         {
            // Definitions
            const auto& weights = arQuad.Weights();

            // Create mode matrices from LaplaceQuadrature. The aDiagEnergy is distributed to keep mode matrices similar in norm.
            const Nb diag_energ_part = aDiagEnergy/Nb(num_dims); ///< The part of the diagonal energy that will be subtracted from each exponent
            for(In i=I_0; i<num_dims; ++i)
            {
               arQuad.MakeMatrix(aEigVals, aOffsets, aModes.at(i), this->Extent(i), diag_energ_part, this->mModeMatrices[i]);
            }


            // Calculate weight roots to be multiplied on each mode matrix to keep the norms similar
            auto num_points = arQuad.NumPoints();
            std::vector<Nb> weight_roots(num_points);
            for(In i = 0; i<num_points; ++i)
            {
               // Take the roots of the positive weights. If a sign occurs, multiply -1 on the first mode matrix later.
               weight_roots.at(i) = std::pow(std::abs(weights.at(i)), C_1/num_dims);
            }

            // Multiply the weight roots on all mode matrices. The sign of the weight is multiplied on mModeMatrices[0].
            // If the tensor has been optimized on negative interval, change sign of all weights
            T* ptr = nullptr;
            T sign = static_cast<T>(1.);
            for(In imode = 0; imode<num_dims; ++imode)
            {
               ptr = this->mModeMatrices[imode];
               for(unsigned int irank=0;irank<this->mRank;++irank)
               {
                  sign = (weights.at(irank) > 0) ? static_cast<T>(1.) : static_cast<T>(-1.);

                  // flip sign for negative interval
                  if (  arQuad.NegativeInterval()
                     )
                  {
                     sign *= static_cast<T>(-1.);
                  }

                  for(unsigned int idim=0;idim<this->mDims[imode];++idim)
                  {
                     // Multiply signs of weights on the first mode matrix
                     if(imode==0) *(ptr++) *= sign*weight_roots.at(irank);
                     else         *(ptr++) *= weight_roots.at(irank);
                  }
               }
            }
         }

         // Sanity check
         if (  this->CheckModeMatrices()
            )
         {
            MidasWarning("Ill-conditioned LaplaceQuadrature EnergyDenominatorTensor!");
            Mout  << " LaplaceQuadrature of bad tensor:\n" << arQuad << std::endl;
            Mout  << *this << std::endl;
            
            this->ConstructByCpDecomposition(aModes, aEigVals, aOffsets, arQuad.NumPoints(), aDiagEnergy);

            // If CP decomp also fails...
            if (  this->CheckModeMatrices()
               )
            {
               MIDASERROR("Bad EnergyDenominatorTensor! CP decomp also failed...");
            }
         }

         // Check conditioning
         T max_thresh = static_cast<T>(1.e20);
         auto max_elem = this->MaxModeMatrixElement().second;
         if (  max_elem > max_thresh
            )
         {
            MidasWarning("Large element observed in Laplace EnergyDenominatorTensor. I will use CP decomposition instead due to possible ill-conditioning...");
            Mout  << " Largest element: " << max_elem << std::endl;
         
            this->ConstructByCpDecomposition(aModes, aEigVals, aOffsets, arQuad.NumPoints(), aDiagEnergy);
         }
      }

      ///> Constructor for vibrational structure calculations. Takes a ModeCombi and a Vcc object to get the modal energies.
      EnergyDenominatorTensor
         (  const std::vector<In>& aModes
         ,  const std::vector<In>& aNmodals
         ,  const DataCont& aEigVals
         ,  const std::vector<In>& aOffsets
         ,  const unsigned int aNumQuadPoints
         ,  const Nb aDiagEnergy=C_0
         )
         :  CanonicalTensor<T>
               (  edt_dims
                     (  aModes
                     ,  aNmodals
                     )
               , aModes.size() > 1 ? aNumQuadPoints : 1
               )
      {
         auto ndim = aModes.size();

         // We construct vectors exactly
         if (  ndim == 1
            )
         {
            this->ConstructEnergyDenominatorVector(aModes[0], aEigVals, aOffsets, aDiagEnergy);
         }
         else
         {
            // Construct default LaplaceInfo
            midas::tensor::LaplaceInfo lapinfo("NOOPT");
            lapinfo["NOOPTALLOWMOREPOINTS"] = false;
            lapinfo["NUMPOINTS"] = static_cast<In>(aNumQuadPoints);

            // Calculate interval [1,R]
            Nb max_denom = C_0;
            Nb min_denom = C_0;
            Nb val = C_0;
            for(In imode=0; imode<ndim; ++imode)
            {
               aEigVals.DataIo(IO_GET, aOffsets[imode]+aNmodals[imode], val);
               max_denom += val;
               aEigVals.DataIo(IO_GET, aOffsets[imode]+I_1, val);
               min_denom += val;
            }

            // If all energy denominators are positive
            if (  (min_denom - aDiagEnergy) > C_0
               )
            {
               max_denom -= aDiagEnergy;
               min_denom -= aDiagEnergy;
               const Nb relative_interval_length = max_denom/min_denom;

               // If interval too short: Do CP-ALS
               if(std::round(relative_interval_length) < C_2)
               {
                  MidasWarning("EnergyDenominatorTensor: Relative interval too short! Running CP-ALS instead of Laplace fitting!");

                  this->ConstructByCpDecomposition(aModes, aEigVals, aOffsets, aNumQuadPoints, aDiagEnergy);
               }
               else if  (  relative_interval_length > C_10_3
                        && min_denom < C_I_10_4
                        )
               {
                  MidasWarning("Interval very long. Use rank-1 cross approximation!");
                  Mout  << " Relative interval length:      " << relative_interval_length << "\n"
                        << " Min denominator:               " << min_denom << "\n"
                        << std::flush;


                  auto energy_holder = NiceTensor<T>(new ModalEnergyHolderTensor<T>(this->GetDims(), true, false, aDiagEnergy, aModes, aEigVals, aOffsets));

                  std::vector<unsigned> pivot(this->NDim(), 0);

                  auto cross = NiceTensor<T>(cross_approximation<T>( energy_holder.GetTensor(), pivot, 0));

                  // DEBUG
                  if (  gDebug
                     )
                  {
                     auto exact = NiceTensor<T>( new ExactEnergyDenominatorTensor<T>(aModes, aNmodals, aEigVals, aOffsets, aDiagEnergy));
                     auto err2 = safe_diff_norm2(exact.GetTensor(), cross.GetTensor());
                     auto err = std::sqrt(err2);
                     auto norm = exact.Norm();
                     Mout  << " Abserr:   " << err << "\n"
                           << " Relerr:   " << err/norm << std::endl;
                  }

                  *this = *static_cast<EnergyDenominatorTensor<T>*>(cross.GetTensor());
               }
               else
               {
                  *this =  EnergyDenominatorTensor<T>
                              (  aModes
                              ,  aNmodals
                              ,  aEigVals
                              ,  aOffsets
                              ,  LaplaceQuadrature<T>
                                    (  lapinfo
                                    ,  aModes
                                    ,  aNmodals
                                    ,  aEigVals
                                    ,  aOffsets
                                    ,  aDiagEnergy
                                    )
                              ,  aDiagEnergy
                              );
               }
            }
            // If all energy denominators are negative
            else if  (  (max_denom - aDiagEnergy) < C_0
                     )
            {  

               // Construct min and max denominators on negative axis
               const Nb new_max_denom = std::abs(min_denom - aDiagEnergy);
               const Nb new_min_denom = std::abs(max_denom - aDiagEnergy);
               const Nb relative_interval_length = new_max_denom/new_min_denom;

               // If interval too short: Do CP-ALS
               if(relative_interval_length < C_2)
               {
                  MidasWarning("EnergyDenominatorTensor: Relative interval too short! Running CP-ALS instead of Laplace fitting!");

                  this->ConstructByCpDecomposition(aModes, aEigVals, aOffsets, aNumQuadPoints, aDiagEnergy);
               }
               // Otherwise do Laplace with negative scaling factor
               // The exponents should be constructed correctly since the exponents change sign in the LaplaceQuadrature::MakeMatrix function.
               // The sign of the weights is changed in the EnergyDenominatorTensor constructor.
               else
               {
                  *this = EnergyDenominatorTensor<T>
                        (  aModes
                        ,  aNmodals
                        ,  aEigVals
                        ,  aOffsets
                        ,  LaplaceQuadrature<T>
                              (  lapinfo
                              ,  aModes
                              ,  aNmodals
                              ,  aEigVals
                              ,  aOffsets
                              ,  aDiagEnergy
                              )
                        , aDiagEnergy
                        );

                  // DEBUG
                  if (  gDebug
                     )
                  {
                     auto exact = NiceTensor<T>(new ExactEnergyDenominatorTensor<T>(aModes, aNmodals, aEigVals, aOffsets, aDiagEnergy));
                     auto approx = NiceTensor<T>(this->Clone());

                     auto err = diff_norm_new(*approx.GetTensor(), *exact.GetTensor());
                     auto norm = exact.Norm();

                     Mout  << " DEBUG: Negative-interval Laplace denominator:\n"
                           << "    abserr:   " << err << "\n"
                           << "    relerr:   " << err/norm << "\n"
                           << "    dims:     " << this->ShowDims() << "\n"
                           << "    npoints:  " << aNumQuadPoints << "\n"
                           << std::flush;
                  }
               }
            }
            // If the interval includes 0: Laplace is problematic!
            else
            {
               MidasWarning("EnergyDenominatorTensor: Laplace interval includes zero!");

               auto fit_report = this->ConstructByCpDecomposition(aModes, aEigVals, aOffsets, aNumQuadPoints, aDiagEnergy);

               if (  gDebug
                  )
               {
                  Mout  << " Results of CP-ALS fit:\n"
                        << "    Abserr:   " << fit_report.error << "\n"
                        << "    Relerr:   " << fit_report.error / fit_report.norm << "\n"
                        << std::flush;
               }
            }
         }
      }

      //! Constructor from LaplaceInfo
      EnergyDenominatorTensor
         (  const midas::tensor::LaplaceInfo& lapinfo
         ,  const std::vector<In>& aModes
         ,  const std::vector<In>& aNmodals
         ,  const DataCont& aEigVals
         ,  const std::vector<In>& aOffsets
         ,  T shift = static_cast<T>(0.)
         )
         :  EnergyDenominatorTensor<T>
               (  aModes
               ,  aNmodals
               ,  aEigVals
               ,  aOffsets
               ,  LaplaceQuadrature<T>
                     (  lapinfo
                     ,  aModes
                     ,  aNmodals
                     ,  aEigVals
                     ,  aOffsets
                     ,  shift
                     )
               ,  shift
               )
      {
      }

      //! Construct EnergyDenominatorTensor by direct decomposition of full tensor
      FitReport<T> ConstructByCpDecomposition
         (  const std::vector<In>& aModes
         ,  const DataCont& aEigVals
         ,  const std::vector<In>& aOffsets
         ,  const unsigned int aNumQuadPoints
         ,  const Nb aDiagEnergy
         )
      {
         NiceTensor<T> exact(new ModalEnergyHolderTensor<T>(this->GetDims(), true, false, aDiagEnergy, aModes, aEigVals, aOffsets));
         auto target_norm2 = exact.Norm2();
         auto target_norm = std::sqrt(target_norm2);
         NiceTensor<T> guess(random_canonical_tensor<T>(exact.GetDims(), aNumQuadPoints));
         guess.Scale(target_norm);

         CpAlsInput<T> input;
         input.SetMaxerr(1.e-8);
         input.SetMaxiter(10);
         input.SetRcond(1.e-12);
         input.SetDistanceConvCheck(false);
         input.SetBalanceResult(true);

         auto fit_report = FitCPALS(exact, target_norm2, guess, input);

         if (  fit_report.error > target_norm
            )
         {
            MidasWarning("EnergyDenominatorTensor: Bad CP-ALS fit!");
            Mout  << " Error:       " << fit_report.error << "\n"
                  << " Exact norm:  " << target_norm << "\n"
                  << std::flush;
         }

         *this = *static_cast<EnergyDenominatorTensor<T>* >(guess.GetTensor());

         return fit_report;
      }
};




/**
 *
 **/
template
   <  class T
   >
class ExactEnergyDenominatorTensor
   :  public SimpleTensor<T>
{
   public:
      ExactEnergyDenominatorTensor
         (  const unsigned int order
         ,  const std::vector<T>& vir
         ,  const std::vector<T>& occ
         )
         :  SimpleTensor<T>(edt_dims(order, vir.size(), occ.size()))
      {
         std::vector<typename std::vector<T>::const_iterator> iters;
         std::vector<typename std::vector<T>::const_iterator> begins;
         std::vector<typename std::vector<T>::const_iterator> ends;
         for(In iorder=0;iorder<order;++iorder)
         {
            iters.push_back(vir.begin());
            iters.push_back(occ.begin());
            begins.push_back(vir.begin());
            begins.push_back(occ.begin());
            ends .push_back(vir.end());
            ends .push_back(occ.end());
         }

         T* ptr=this->GetData();
         In idim=0; // This is declared here so we can check for the condition at the end
         do
         {
            T tmp=0.0;
            for(In iorder=0;iorder<order;++iorder)
            {
               tmp+= *(iters[2*iorder]) - *(iters[2*iorder+1]);
            }
            *(ptr++)=1./tmp;

            for(idim=2*order-1;idim>=0;--idim)
            {
               if(iters[idim]<ends[idim]-1)
               {
                  ++iters[idim];
                  for(In jdim=idim+1;jdim<2*order;++jdim)
                  {
                     iters[jdim]=begins[jdim];
                  }
                  break;
               };
            }
         } while(idim>=0);
      }


      /**
       *
       **/
      explicit ExactEnergyDenominatorTensor
         (  const std::vector<In>& aModes
         ,  const std::vector<In>& aNmodals
         ,  const DataCont& aEigVals
         ,  const std::vector<In>& aOffsets
         ,  Nb aDiagEnergy=C_0
         )
         :  SimpleTensor<T>
               (  edt_dims
                     (  aModes
                     ,  aNmodals
                     )
               )
      {
         NiceTensor<T> holder(new ModalEnergyHolderTensor<T>(this->GetDims(), true, false, aDiagEnergy, aModes, aEigVals, aOffsets));

         auto ptr = std::make_unique<T[]>(holder.TotalSize());

         holder.DumpInto(ptr.get());

         this->SwapData(ptr);
      }
};

#endif /* ENERGYDENOMINATORTENSOR_INCLUDED */
