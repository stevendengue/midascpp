#include "tensor/LaplaceInfo.h"
#include "input/LaplaceInput.h"

namespace midas
{
namespace tensor
{

/**
 * Constructor
 *
 * @param type    Type of LaplaceQuadrature
 * @param delta   Spacing of energy histogram
 **/
LaplaceInfo::LaplaceInfo
   (  const std::string& type
   ,  Nb delta
   )
{
   (*this)["TYPE"] = type;
   (*this)["HISTOGRAMDELTA"] = delta;
   detail::ValidateLaplaceInput(*this);
}

/**
 * Overload of output operator for LapalceDecompInfo.
 * @param os             Output stream.
 * @param info           LaplaceInfo to output.
 * @return               Returns output stream so operator<<'s can be chained.
 **/
std::ostream& operator<<(std::ostream& os, const LaplaceInfo& info)
{
   os << " LAPLACEINFO: \n";
   for(auto& elem : info)
   {
      os << " KEYWORD = " << elem.first << ", VALUE = " << elem.second << "\n";
   }
   return os;
}

} /* namespace tensor */
} /* namespace midas */
