/**
 ************************************************************************
 * 
 * @file                NiceTensor_Impl.h
 *
 * Created:             19.12.2014
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * Short Description:   Implementation of NiceTensor class.
 * 
 * Last modified:       Fri Dec 19 2014
 * 
 * Detailed Description: ...
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef NICETENSOR_IMPL_H_INCLUDED
#define NICETENSOR_IMPL_H_INCLUDED

#include <utility>
#include <stdexcept>
#include <memory>

#include "util/read_write_binary.h"
#include "util/CallStatisticsHandler.h"
#include "util/Io.h"
#include "util/Math.h"
#include "tensor/BaseTensor.h"
#include "tensor/SimpleTensor.h"
#include "tensor/CanonicalTensor.h"
#include "tensor/Scalar.h"
#include "tensor/ZeroTensor.h"
#include "tensor/CanonicalIntermediate.h"

#include "mpi/Impi.h"

/*!
 *
 **/
template <class T>
bool NiceTensor<T>::IsType
   ( typename BaseTensor<T>::typeID id
   ) const
{
   return (id == mTensor->Type());
}

/*!
 *
 **/
template <class T>
NiceTensor<T>::NiceTensor
   (
   )
   : mTensor(nullptr)
{
}

/*!
 *
 **/
template <class T>
NiceTensor<T>::NiceTensor
   ( BaseTensor<T>* aTensor
   )
   : mTensor(aTensor)
{
}

/*! Constructs a zero NiceTensor holding appropriate BaseTensor type.
 *
 **/
template <class T>
NiceTensor<T>::NiceTensor
   ( const std::vector<unsigned int>& arDims
   , typename BaseTensor<T>::typeID aType
   )
   : mTensor(nullptr)
{
   switch(aType)
   {
      case BaseTensor<T>::typeID::SIMPLE:
      {
         // Allocate and construct SimpleTensor of all zeros.
         mTensor = BaseTensorPtr<T>(new SimpleTensor<T>(arDims, C_0));
         break;
      }
      case BaseTensor<T>::typeID::CANONICAL:
      {
         // Allocate and construct CanonicalTensor of rank 0.
         mTensor = BaseTensorPtr<T>(new CanonicalTensor<T>(arDims, I_0));
         break;
      }
      case BaseTensor<T>::typeID::SCALAR:
      {
         // Allocate and construct Scalar of value 0.
         // ... but assert that arDims is empty, otherwise Scalar doesn't make
         // sense.
         MidasAssert(arDims.size() == 0, "Contradiction: Constructing Scalar with non-empty dimensions.");
         mTensor = BaseTensorPtr<T>(new Scalar<T>(static_cast<T>(C_0)));
         break;
      }
      case BaseTensor<T>::typeID::ZERO:
      {
         // Allocate and construct ZeroTensor.
         mTensor = BaseTensorPtr<T>(new ZeroTensor<T>(arDims));
         break;
      }
      case BaseTensor<T>::typeID::SUM:
      {
         // Allocate and construct empty TensorSum
         mTensor = BaseTensorPtr<T>(new TensorSum<T>(arDims));
         break;
      }
      default:
      {
         // The std::to_string(aType) will result in an integer.  Check enum
         // definition in BaseTensor<T> class for correspondence with tensor
         // types.
         MIDASERROR("The NiceTensor(..., BaseTensor<T>::typeID) constructor is not implemented for BaseTensor<T>::typeID number '"+std::to_string(static_cast<In>(aType))+"'.");
      }
   }
}

/*! Constructs a unit NiceTensor holding appropriate BaseTensor type, with a 1 at the index.
 *
 **/
template <class T>
NiceTensor<T>::NiceTensor
   ( const std::vector<unsigned int>& arDims
   , const std::vector<unsigned int>& arIndex
   , typename BaseTensor<T>::typeID aType
   )
   : mTensor(nullptr)
{
   switch(aType)
   {
      case BaseTensor<T>::typeID::SIMPLE:
      {
         // Allocate and construct SimpleTensor of all zeros.
         mTensor = BaseTensorPtr<T>(new SimpleTensor<T>(arDims, arIndex));
         break;
      }
      case BaseTensor<T>::typeID::CANONICAL:
      {
         // Allocate and construct CanonicalTensor of rank 0.
         mTensor = BaseTensorPtr<T>(new CanonicalTensor<T>(arDims, arIndex));
         break;
      }
      case BaseTensor<T>::typeID::SCALAR:
      {
         // Allocate and construct Scalar of value 1.
         // ... but assert that arDims and arIndex are empty, otherwise Scalar
         // doesn't make sense.
         MidasAssert(arDims.size() == 0, "Contradiction: Constructing Scalar with non-empty dimensions.");
         MidasAssert(arIndex.size() == 0, "Contradiction: Non-empty index for construction of Scalar.");
         mTensor = BaseTensorPtr<T>(new Scalar<T>(static_cast<T>(C_1)));
         break;
      }
      default:
      {
         // The std::to_string(aType) will result in an integer.  Check enum
         // definition in BaseTensor<T> class for correspondence with tensor
         // types.
         MIDASERROR("The NiceTensor(..., BaseTensor<T>::typeID) constructor is not implemented for BaseTensor<T>::typeID number '"+std::to_string(static_cast<In>(aType))+"'.");
      }
   }
}

/*!
 *
 **/
template <class T>
NiceTensor<T>::NiceTensor
   ( BaseTensorPtr<T>&& aTensor
   )
   : mTensor(std::move(aTensor))
{
}

/*! Copy c-tor, check for existence of other, then copy or initialize to nullptr
 *
 **/
template <class T>
NiceTensor<T>::NiceTensor
   ( const NiceTensor<T>& other
   )
   : mTensor(other.mTensor ? other.mTensor->Clone() : nullptr)
{
}

/*! Move c-tor, initialize to nullptr then swap
 *
 **/
template <class T>
NiceTensor<T>::NiceTensor
   (  NiceTensor<T>&& other
   )
   :  mTensor
         (  other.mTensor  ?  std::move(other.mTensor)
                           :  nullptr
         )
{
}

/*! Reads tag (see BaseTensor::tag_t) and initializes #mTensor
 *  accordingly from the input stream.
 */
template <class T>
NiceTensor<T>::NiceTensor
   (  std::istream& input
   )
   :  NiceTensor()
{
   typename BaseTensor<T>::tag_t tag;
   read_binary(tag, input);
   switch(tag)
   {
      case ( BaseTensor<T>::TAG_SIMPLE ):
         mTensor = BaseTensorPtr<T>(new SimpleTensor<T>(input));
         break;

      case ( BaseTensor<T>::TAG_CANONICAL ):
         mTensor = BaseTensorPtr<T>(new CanonicalTensor<T>(input));
         break;

      case ( BaseTensor<T>::TAG_SCALAR ):
         mTensor = BaseTensorPtr<T>(new Scalar<T>(input));
         break;

      case ( BaseTensor<T>::TAG_CANON_IM ):
         mTensor = BaseTensorPtr<T>(new CanonicalIntermediate<T>(input));
         break;

      default:
         MIDASERROR("Bad tag in tensor.");
   }
}

/**
 *
 **/
template <class T>
std::ostream& NiceTensor<T>::Write
   ( std::ostream& output
   ) const
{
   typename BaseTensor<T>::tag_t tag;
   switch(mTensor->Type())
   {
      case ( BaseTensor<T>::typeID::SIMPLE ):
         tag=BaseTensor<T>::TAG_SIMPLE;
         break;

      case ( BaseTensor<T>::typeID::CANONICAL ):
         tag=BaseTensor<T>::TAG_CANONICAL;
         break;

      case ( BaseTensor<T>::typeID::SCALAR ):
         tag=BaseTensor<T>::TAG_SCALAR;
         break;

      case ( BaseTensor<T>::typeID::CANON_IM ):
         tag=BaseTensor<T>::TAG_SCALAR;
         break;

      default:
         MIDASERROR("Cannot Write this type of tensor: <"+mTensor->ShowType()+">");
   }
   write_binary(tag, output);
   return this->mTensor->Write(output);
}

/**
 *
 **/
template
   <  class T
   >
void NiceTensor<T>::WriteToDisc
   (  const std::string& aName
   ,  midas::mpi::OFileStream::StreamType aStreamType
   )  const
{
   midas::mpi::OFileStream out( aName, aStreamType, std::ios::out | std::ios::binary );
   this->Write(out);
   out.close();
}

/**
 *
 **/
template
   <  class T
   >
bool NiceTensor<T>::ReadFromDisc
   (  const std::string& aName
   ,  bool aHardErr
   )
{
   if (  InquireFile(aName)
      )
   {
      std::ifstream input( aName, std::ios::in | std::ios::binary );
      *this = NiceTensor<T>(input);
      input.close();
      return true;
   }
   else if  (  aHardErr
            )
   {
      MIDASERROR("NiceTensor::Read: Filename '" + aName + "' not found in directory '" + midas::os::Getcwd() + "!");
   }

   return false;
}

/**
 *
 **/
template <class T>
NiceTensor<T>::~NiceTensor
   (
   )
{
}

/**
 * 
 **/
template <class T>
NiceTensor<T>& NiceTensor<T>::operator=
   ( const NiceTensor<T>& other
   )
{
   if(mTensor)
   {
      if(mTensor->Assign(other.GetTensor()))
      {
         return *this; // if assignment was successfull we return
      }
   }
   mTensor = other.mTensor ? std::unique_ptr<BaseTensor<T> >(other.mTensor->Clone()) 
                           : nullptr; // if assignment was unsuccesful we clone the other tensor and assign
   return *this;
}

/**
 *
 **/
template <class T>
NiceTensor<T> NiceTensor<T>::Slice
   ( const std::vector<std::pair<unsigned int, unsigned int> >& limits
   ) const
{
   return NiceTensor<T>(this->mTensor->Slice(limits));
}

/**
 * Clear the tensor by setting it to nullptr.
 *
 * @return  Reference to NiceTensor
 **/
template <class T>
NiceTensor<T>& NiceTensor<T>::Clear
   (
   )
{
   this->mTensor = nullptr;

   return *this;
}

/**
 * Swap tensors with another NiceTensor
 *
 * @param aOther  The other tensor
 **/
template
   <  class T
   >
void NiceTensor<T>::SwapTensors
   (  NiceTensor<T>& aOther
   )
{
   std::swap(this->mTensor, aOther.mTensor);
}

/**
 *
 **/
template <class T>
NiceTensor<T>& NiceTensor<T>::operator=
   (  NiceTensor<T>&& other
   )
{
   std::swap(mTensor, other.mTensor);
   return *this;
}

/**
 * Get the number of dimensions (order) of the tensor.
 **/
template <class T>
unsigned int NiceTensor<T>::NDim
   (
   ) const
{
   if(this->mTensor)
   {
      return mTensor->NDim();
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return 0;
   }
}

/**
 * Get the total size, i.e. the number of multi-indices that the vector can take.
 **/
template <class T>
std::size_t NiceTensor<T>::TotalSize
   (
   ) const
{
   if(this->mTensor)
   {
      return mTensor->TotalSize();
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return 0;
   }
}

/**
 * Get the max extent
 **/
template <class T>
unsigned NiceTensor<T>::MaxExtent
   (
   ) const
{
   if(this->mTensor)
   {
      return mTensor->MaxExtent();
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return 0;
   }
}

/**
 * Get the max extent
 **/
template <class T>
unsigned NiceTensor<T>::MinExtent
   (
   ) const
{
   if(this->mTensor)
   {
      return mTensor->MinExtent();
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return 0;
   }
}

/**
 * Get extent along dimension
 *
 * @param i       The dimension
 * @return
 *    Dims[i]
 **/
template
   <  class T
   >
unsigned NiceTensor<T>::Extent
   (  unsigned i
   )  const
{
   if (  this->mTensor
      )
   {
      return this->mTensor->Extent(i);
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor!");
      return 0;
   }
}

/**
 * Get the shape (dimensions) of the vector.
 **/
template <class T>
const std::vector<unsigned int>& NiceTensor<T>::GetDims
   (
   ) const
{
   if(this->mTensor)
   {
      return mTensor->GetDims();
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return mTensor->GetDims(); // Will never be called!
   }
}

/**
 *
 **/
template <class T>
std::string NiceTensor<T>::ShowType() const
{
   if(this->mTensor)
   {
      return mTensor->ShowType();
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return mTensor->ShowType();
   }
}

/**
 *
 **/
template <class T>
std::string NiceTensor<T>::ShowDims() const
{
   if (  !this->mTensor )
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
   }

   return this->mTensor->ShowDims();
}

/**
 *
 **/
template<class T>
typename BaseTensor<T>::typeID NiceTensor<T>::Type() const
{
   if(this->mTensor)
   {
      return mTensor->Type();
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return mTensor->Type();
   }
}

/**
 * Get the underlying tensor
 **/
template <class T>
BaseTensor<T>* NiceTensor<T>::GetTensor() const
{
   if(this->mTensor)
   {
      return mTensor.get();
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return nullptr;
   }
}

/**
 * Release the underlying tensor
 **/
template <class T>
BaseTensor<T>* NiceTensor<T>::Release()
{
   if(this->mTensor)
   {
      return mTensor.release();
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return nullptr;
   }
}

/**
 * Checks if mTensor points to nullptr.
 **/
template <class T>
bool NiceTensor<T>::IsNullPtr() const
{
   return mTensor ? false : true;
}

/**
 *
 **/
template<class T>
bool NiceTensor<T>::SanityCheck() const
{
   return this->mTensor->SanityCheck();
}

/**
 *
 **/
template
   <  class T
   >
void NiceTensor<T>::Conjugate
   (
   )
{
   if (  this->mTensor
      )
   {
      mTensor->Conjugate();
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
   }
}

/**
 *
 **/
template <class T>
NiceTensor<T> NiceTensor<T>::ContractDown
   ( const unsigned int idx
   , const NiceTensor<T>& vect_in
   ) const
{
   //LOGCALL("begin");
   if(this->mTensor && vect_in.mTensor)
   {
      return NiceTensor<T>( mTensor->ContractDown(idx, vect_in.mTensor.get()) );
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      NiceTensor<T> result;
      return result;
   }
}

/**
 *
 **/
template <class T>
NiceTensor<T> NiceTensor<T>::ContractForward
   ( const unsigned int idx
   , const NiceTensor<T>& vect_in
   ) const
{
   //LOGCALL("begin");
   if(this->mTensor && vect_in.mTensor)
   {
      return NiceTensor<T>( mTensor->ContractForward(idx, vect_in.mTensor.get()) );
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      NiceTensor<T> result;
      return result;
   }
}

/**
 *
 **/
template <class T>
NiceTensor<T> NiceTensor<T>::ContractUp
   ( const unsigned int idx
   , const NiceTensor<T>& vect_in
   ) const
{
   //LOGCALL("begin");
   if(this->mTensor && vect_in.mTensor)
   {
      return NiceTensor<T>( mTensor->ContractUp(idx, vect_in.mTensor.get()) );
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      NiceTensor<T> result;
      return result;
   }
}

/**
 *
 **/
template <class T>
NiceTensor<T> NiceTensor<T>::ContractInternal
 ( const std::vector<unsigned int>& indices
 ) const
{
   return NiceTensor<T>(mTensor->ContractInternal(indices));
}

/**
 *
 **/
template <class T>
NiceTensor<T>& NiceTensor<T>::operator*=
   ( const T& k
   )
{
   if(this->mTensor)
   {
      *(this->mTensor) *= k;
      return *this;
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return *this;
   }
}

/**
 *
 **/
template <class T>
NiceTensor<T> NiceTensor<T>::operator*(const T& k) const
{
   NiceTensor<T> result(*this);
   result*=k;
   return result;
}

/**
 *
 **/
template <class T>
NiceTensor<T> operator*(const T& k, const NiceTensor<T>& this_)
{
   return this_*k;
}

/**
 *
 **/
template <class T>
NiceTensor<T>& NiceTensor<T>::operator+=(const NiceTensor& right)
{
   if (  this->mTensor
      && right.mTensor
      )
   {
      if (  this->mTensor->Type() == BaseTensor<T>::typeID::ZERO
         )
      {
         *this = right;
         return *this;
      }
      else
      {
         *(this->mTensor) += *(right.mTensor);
         return *this;
      }
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return *this;
   }
}

/**
 *
 **/
template <class T>
NiceTensor<T> NiceTensor<T>::operator+ (const NiceTensor& right) const
{
   if(this->mTensor && right.mTensor)
   {
      //return NiceTensor(*(this->mTensor) + *(right.mTensor));
      auto copy_this = *this;
      return copy_this += right;
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return *this;
   }
}

/**
 *
 **/
template <class T>
NiceTensor<T>& NiceTensor<T>::operator-=(const NiceTensor& right)
{
   if (  this->mTensor
      && right.mTensor
      )
   {
      if (  this->mTensor->Type() == BaseTensor<T>::typeID::ZERO
         )
      {
         *this = right;
         this->Scale(-1.0);
         return *this;
      }
      else
      {
         *(this->mTensor) -= *(right.mTensor);
         return *this;
      }
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return *this;
   }
}

/**
 *
 **/
template <class T>
NiceTensor<T> NiceTensor<T>::operator- (const NiceTensor& right) const
{
   if(this->mTensor && right.mTensor)
   {
      auto copy_this = *this;
      return copy_this -= right;
      //return NiceTensor(*(this->mTensor) - *(right.mTensor));
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return *this;
   }
}

/**
 *
 **/
template <class T>
NiceTensor<T>& NiceTensor<T>::operator*=(const NiceTensor& right)
{
   if(this->mTensor)
   {
      *(this->mTensor) *= *(right.mTensor);
      return *this;
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return *this;
   }
}

/**
 *
 **/
template <class T>
NiceTensor<T> NiceTensor<T>::operator* (const NiceTensor& right) const
{
   auto result = *this;
   result *= right;
   return result;
}

/**
 *
 **/
template <class T>
NiceTensor<T>& NiceTensor<T>::operator/=(const NiceTensor& right)
{
   if(this->mTensor)
   {
      *(this->mTensor) /= *(right.mTensor);
      return *this;
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return *this;
   }
}

/**
 * `Y = aX + Y`; add a scalar times a tensor to the calling NiceTensor object,
 * in-place. Calls corresponding Axpy for contained BaseTensor object.
 * @param arX     The (other) NiceTensor, to be multiplied and added.
 * @param arA     The scalar with which to multiply arX.
 **/
template<class T>
void NiceTensor<T>::Axpy(const NiceTensor& arX, const T& arA)
{
   if (  this->GetTensor()
      && arX.GetTensor()
      )
   {
      if (  this->mTensor->Type() == BaseTensor<T>::typeID::ZERO
         )
      {
         *this = arX;
         this->Scale(arA);
      }
      else
      {
         this->GetTensor()->Axpy(*(arX.GetTensor()), arA);
      }
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
   }
}

/*! 
 * Wrapper for BaseTensor::dot_product().
 */
template <class T>
T dot_product
   ( const NiceTensor<T>& left
   , const NiceTensor<T>& right
   )
{
   if(left.GetTensor() && right.GetTensor())
   {
      return left.GetTensor()->Dot(right.GetTensor());
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return static_cast<T>(0.);
   }
}

///*! Wrapper for BaseTensor::safe_dot_product().
// */
//template <class T>
//T safe_dot_product(const NiceTensor<T>& left, const NiceTensor<T>& right)
//{
//   if(left.GetTensor() && right.GetTensor())
//   {
//      auto left_simple = left.GetTensor()->ToSimpleTensor();
//      auto right_simple = right.GetTensor()->ToSimpleTensor();
//      return left_simple->Dot(right_simple);
//   }
//   else
//   {
//      MIDASERROR("Uninitialized NiceTensor.mTensor");
//   }
//}

/**
 * make a NiceTensor holding a SimpleTensor
 * @param aDims
 *    dimensions of tensor
 * @param data
 *    data to be held (SimpleTensor will take ownership)
 **/
template<class T>
NiceTensor<T> make_nice_simple( const std::vector<unsigned>& aDims
                              , T* data
                              )
{
   return {new SimpleTensor<T>(aDims, data)};
}

/**
 *
 **/
template<class T>
NiceTensor<T> make_nice_random_simple( const std::vector<unsigned>& aDims
                                     )
{
   return NiceTensor<T>(random_simple_tensor<T>(aDims));
}

/**
 *
 **/
template <class T>
typename NiceTensor<T>::real_t NiceTensor<T>::Norm() const
{
   if(this->mTensor)
   {
      return this->mTensor->Norm();
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return static_cast<real_t>(0.);
   }
}

/**
 *
 **/
template <class T>
typename NiceTensor<T>::real_t NiceTensor<T>::Norm2() const
{
   if(this->mTensor)
   {
      return this->mTensor->Norm2();
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      return static_cast<real_t>(0.);
   }
}

/**
 *
 **/
template<class T>
void NiceTensor<T>::Zero()
{
   if(this->mTensor)
   {
      this->mTensor->Zero();
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
   }
}

/**
 *
 **/
template<class T>
void NiceTensor<T>::ToZeroTensor()
{
   if(this->mTensor)
   {
      MIDASERROR("MBH:NiceTensor<T>::ToZeroTensor not impl. yet.");
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
   }
}

/**
 *
 **/
template<class T>
void NiceTensor<T>::Scale(const T& aScalar)
{
   if(this->mTensor)
   {
      this->mTensor->Scale(aScalar);
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
   }
}

/**
 *
 **/
template
   <  class T
   >
void NiceTensor<T>::Abs
   (
   )
{
   if (  this->mTensor
      )
   {
      this->mTensor->Abs();
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
   }
}

/**
 *
 **/
template<class T>
void NiceTensor<T>::ElementwiseScalarAddition(const T& aScalar)
{
   if(this->mTensor)
   {
      this->mTensor->ElementwiseScalarAddition(aScalar);
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
   }
}

/**
 *
 **/
template<class T>
void NiceTensor<T>::ElementwiseScalarSubtraction(const T& aScalar)
{
   if(this->mTensor)
   {
      this->mTensor->ElementwiseScalarSubtraction(aScalar);
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
   }
}

/*!
 *
 */
template <class T>
NiceTensor<T> contract
   (  const NiceTensor<T>& tensor1
   ,  const std::vector<unsigned int>& indices1
   ,  const NiceTensor<T>& tensor2
   ,  const std::vector<unsigned int>& indices2
   ,  bool conjugate_left
   ,  bool conjugate_right
   )
{
   return NiceTensor<T>(contract(*tensor1.GetTensor(), indices1,
                                 *tensor2.GetTensor(), indices2, conjugate_left, conjugate_right));
}

/*!
 *
 */
template <class T>
NiceContractor<T> NiceTensor<T>::operator[] (const ContractionIndices& indices) const
{
   return NiceContractor<T>(this, indices);
}

/*!
 *
 */
template <class T>
NiceContractor<T> NiceTensor<T>::operator[] (const ContractionIndex& index) const
{
   return (*this)[ContractionIndices(std::vector<ContractionIndex>{index})];
}

/*! Converting a contractor to NiceTensor means performing all internal contractions
 *  and reordering. E.g.
 *
 *       NiceTensor<T> a=b[i,i,b,a]
 *
 *  Now \f$a_{ab} = \sum_i b_{iiba}\f$
 */
template <class T>
NiceTensor<T>::NiceTensor
   ( const NiceContractor<T>& contractor
   )
   : NiceTensor<T>
        ( contractor.GetTensor()->GetTensor()->ContractInternal(contractor.GetIndices().ToUIntVector())
        )
{
}

#ifdef VAR_MPI
/**
 * Send NiceTensor using MPI
 *
 * @param aRecievingRank   The rank that will recieve the NiceTensor.
 **/
template<class T>
void NiceTensor<T>::MpiSend
   ( In aRecievingRank
   ) const
{
   // if we do not have a tensor we throw an error (for now 17/10-16).
   if(!mTensor) 
   {
      MIDASERROR("Trying to send null tensor");
   }

   //auto comm = midas::mpi_util::get_mpi_comm_manager().GetCommunicator();

   // send type of underlying tensor
   int type = static_cast<int>(mTensor->Type());
   midas::mpi::detail::WRAP_Send(&type, 1, MPI_INT, aRecievingRank, 0, MPI_COMM_WORLD);
   
   // send data
   mTensor->MpiSend(aRecievingRank);
}

/**
 * Recieve a NiceTensor using MPI
 *
 * @param aSendingRank   The rank that will send the NiceTensor.
 **/
template<class T>
void NiceTensor<T>::MpiRecv
   ( In aSendingRank
   ) 
{
   //auto comm = midas::mpi_util::get_mpi_comm_manager().GetCommunicator();
   MPI_Status status;

   // recieve tensor type
   int type;
   midas::mpi::detail::WRAP_Recv(&type, 1, MPI_INT, aSendingRank, 0, MPI_COMM_WORLD, &status);

   // recieve data
   mTensor = BaseTensor<T>::ConstructFromType(type);
   mTensor->MpiRecv(aSendingRank);
}

/**
 * Bcast a NiceTensor using MPI
 *
 * @param aRoot      Root rank that sends to all.
 **/
template
   <  class T
   >
void NiceTensor<T>::MpiBcast
   (  In aRoot
   )
{
   if constexpr (MPI_DEBUG)
   {
      midas::mpi::WriteToLog("Broadcasting Nicetensor type.");
   }

   // Get type
   int type = static_cast<int>(this->mTensor->Type());
   midas::mpi::detail::WRAP_Bcast(&type, 1, MPI_INT, aRoot, MPI_COMM_WORLD);
   
   if constexpr (MPI_DEBUG)
   {
      midas::mpi::WriteToLog("Broadcasting tensor of type : " + std::to_string(type) + ".");
   }

   // If this is not root, we need to re-allocate the tensor
   if (  midas::mpi::GlobalRank() != aRoot
      )
   {
      this->mTensor = BaseTensor<T>::ConstructFromType(type);
   }
   else
   {
      if  (  !this->mTensor
          )
      {
         MIDASERROR("Trying to Bcast nullptr!");
      }
   }

   // Bcast tensor
   this->mTensor->MpiBcast(aRoot);

   if constexpr(MPI_DEBUG) 
   {
      midas::mpi::WriteToLog("Finished broadcasting Nicetensor.");
   }
}

#endif /* VAR_MPI */

/*!
 *
 */
template <class T>
NiceTensor<T> linear_combination(const std::vector<T>& coeffs,
                                 const std::vector<NiceTensor<T>>& tensors)
{
   if(std::all_of(tensors.begin(), tensors.end(), [](const NiceTensor<T>& t){return t.mTensor;}))
   {
      std::vector<BaseTensor<T>*> ptr_vector;
      for(auto it=tensors.begin();it<tensors.end();++it)
      {
         ptr_vector.push_back(it->mTensor);
      }
      return NiceTensor<T>(linear_combination(coeffs, ptr_vector));
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
      NiceTensor<T> fake_result;
      return fake_result;
   }
}

/*!
 *
 */
template <class T>
std::ostream& operator<<
   ( std::ostream& os
   , const NiceTensor<T>& self
   )
{
   //os << *(self.ToSimpleTensor().mTensor);
   if(!self.mTensor) MIDASERROR("Uninitialized NiceTensor.mTensor");
   os << self.mTensor->Prettify();
   return os;
}

/*!
 *
 */
template <class T>
NiceTensor<T> NiceTensor<T>::ToSimpleTensor() const
{
   switch(this->mTensor->Type())
   {
      case BaseTensor<T>::typeID::SIMPLE:
      {
         return *this;
      }
      case BaseTensor<T>::typeID::SCALAR:
      {
         return *this;
      }
      case BaseTensor<T>::typeID::CANONICAL:
      {
         midas::tensor::BaseTensorAllocator<T> alloc;
         auto ptr = std::unique_ptr<T[]>(new T[this->mTensor->TotalSize()]);
         mTensor->DumpInto(ptr.get());
         return NiceTensor<T>(new SimpleTensor<T>(mTensor->GetDims(), ptr.release()));
      }
      case BaseTensor<T>::typeID::DIRPROD:
      {
         midas::tensor::BaseTensorAllocator<T> alloc;
         auto ptr = std::unique_ptr<T[]>(new T[this->mTensor->TotalSize()]);
         mTensor->DumpInto(ptr.get());
         return NiceTensor<T>(new SimpleTensor<T>(mTensor->GetDims(), ptr.release()));
      }
      case BaseTensor<T>::typeID::SUM:
      {
         midas::tensor::BaseTensorAllocator<T> alloc;
         auto ptr = std::unique_ptr<T[]>(new T[this->mTensor->TotalSize()]);
         mTensor->DumpInto(ptr.get());
         return NiceTensor<T>(new SimpleTensor<T>(mTensor->GetDims(), ptr.release()));
      }
      case BaseTensor<T>::typeID::VIEW:
      {
         return NiceTensor<T>(this->mTensor->Clone()).ToSimpleTensor() ;
      }
      default:
      {
         MIDASERROR ( "Missing case in NiceTensor::ToSimpleTensor" );
         return *this;
      }
   }
}

/*!
 *
 */
template
   <  class T
   >
NiceTensor<T> NiceTensor<T>::ToCanonicalTensor(const T& threshold) const
{
   switch(this->mTensor->Type())
   {
      case BaseTensor<T>::typeID::SCALAR:
      {
         return *this;
      }
      case BaseTensor<T>::typeID::VIEW:
      {
         return NiceTensor<T>(this->mTensor->Clone()).ToCanonicalTensor(threshold) ;
      }
      case BaseTensor<T>::typeID::SIMPLE:
      case BaseTensor<T>::typeID::CANONICAL:
      {
         if constexpr   (  !midas::type_traits::IsComplexV<T>
                        )
         {
            return NiceTensor<T>(new CanonicalTensor<T>( compress_to_canonical(*(this->mTensor), threshold) ));
         }
      }
      default:
      {
         MIDASERROR ( "Missing case in NiceTensor::ToCanonicalTensor" );
         return *this;
      }
   }
}

/**
 * Perform rank-1 cross approximation on NiceTensor (wrapper function)
 *
 * @param aPivot
 *    The pivot to start from.
 * @param aMaxSteps
 *    Maximum steps in pivot search.
 * @return
 *    Rank-1 cross approximation in NiceTensor format.
 **/
template <class T>
NiceTensor<T> NiceTensor<T>::CrossApproximation
   (  const std::vector<unsigned>& aPivot
   ,  const unsigned aMaxSteps
   )  const
{
   return NiceTensor<T>( cross_approximation<T>( this->mTensor.get(), aPivot, aMaxSteps ) );
}


/**
 * Calculate successive cross approximation (SCA) of underlying tensor
 * Ref. [Espig, Hackbusch: Regularized Newton CP decomp](https://link.springer.com/article/10.1007/s00211-012-0465-9)
 *
 * @param aRank
 *    Rank of the result
 * @param aMaxSteps
 *    Maximum number of steps for pivot search
 * @return
 *    SCA approximation to mTensor
 **/
template <class T>
NiceTensor<T> NiceTensor<T>::SuccessiveCrossApproximation
   (  const unsigned aRank
   ,  const unsigned aMaxSteps
   )  const
{
   // Determine rank
   In input_rank = 0;
   switch(this->Type())
   {
      case BaseTensor<T>::typeID::CANONICAL:
      {
         input_rank = this->StaticCast<CanonicalTensor<T>>().GetRank();
         break;
      }
      case BaseTensor<T>::typeID::DIRPROD:
      {
         input_rank = this->StaticCast<TensorDirectProduct<T>>().Rank();
         break;
      }
      case BaseTensor<T>::typeID::SUM:
      {
         input_rank = this->StaticCast<TensorSum<T>>().Rank();
         break;
      }
      case BaseTensor<T>::typeID::SIMPLE:
      {
         input_rank = -1;
         break;
      }
      default:
      {
         MIDASERROR("SuccessiveCrossApproximation: No match for tensor type: "+this->ShowType());
      }
   }

   // Return this if rank is smaller than requested rank
   if (  input_rank < aRank
      && input_rank >= 0
      )
   {
      return *this;
   }

   // Initialize tensors
   NiceTensor<T> result( new CanonicalTensor<T>( this->GetDims(), 0 ) );
   NiceTensor<T> incr( new CanonicalTensor<T>( this->GetDims(), 1 ) );
   NiceTensor<T> diff( new TensorSum<T>( this->GetDims() ) );
   diff += *this;

   // Initialize pivot and other variables
   T scaling = static_cast<T>(1.);
   std::vector<unsigned> pivot(this->NDim(), 0);
   auto ndim = this->NDim();

   // Loop over ranks
   for( unsigned r=1; r<=aRank; ++r )
   {
      // Calculate rank-1 cross approximation
      incr = diff.CrossApproximation( pivot, aMaxSteps );

      // Scale the rank-1 tensor
      scaling = dot_product( diff, incr ) / incr.Norm2();
      incr.Scale( scaling );

      // Update result and diff
      result += incr;
      if (  r != aRank  )
      {
         diff -= incr;
      }
   }

   return result;
}

/*!
 *
 */
template <class T>
T NiceTensor<T>::GetScalar() const
{
   switch(this->mTensor->Type())
   {
      case BaseTensor<T>::typeID::SCALAR:
      {
         return dynamic_cast<Scalar<T>*>(this->mTensor.get())->GetValue();
      }
      default:
      {
         MIDASERROR("NiceTensor::GetScalar() used on a non-scalar.");
         return static_cast<T>(0.);
      }
   }
}

/*!
 *
 */
template <class T>
bool NiceTensor<T>::IsScalar() const
{
   return this->IsType(BaseTensor<T>::typeID::SCALAR);
}

/*!
 *
 */
template<class T>
void NiceTensor<T>::DumpInto
   ( T* ptr
   ) const
{
   if(this->mTensor)
   {
      this->mTensor->DumpInto(ptr);
   }
   else
   {
      MIDASERROR("Uninitialized NiceTensor.mTensor");
   }
}

/*!
 *
 */
template <class T>
NiceTensor<T> NiceTensor<T>::Reorder(const std::vector<unsigned int>& neworder) const
{
   return NiceTensor<T>(this->mTensor->Reorder(neworder));
}

/*!
 *
 */
template<class T>
NiceTensor<T> random_canonical_tensor
   ( const std::vector<unsigned int>& aDims
   , const unsigned int aRank
   )
{
   return NiceTensor<T>(random_canonical_tensor_explicit<T>(aDims, aRank));
}

/**
 * Norm for ODE
 **/
template
   <  typename PARAM_T
   ,  typename STEP_T
   >
typename NiceTensor<PARAM_T>::real_t OdeNorm2
   (  const NiceTensor<PARAM_T>& aDeltaY
   ,  STEP_T aAbsTol
   ,  STEP_T aRelTol
   ,  const NiceTensor<PARAM_T>& aYOld
   ,  const NiceTensor<PARAM_T>& aYNew
   )
{
   auto size = aDeltaY.TotalSize();
   assert(size == aYOld.TotalSize());
   assert(size == aYNew.TotalSize());
   auto type = aDeltaY.Type();
   assert(type == aYOld.Type());
   assert(type == aYNew.Type());

   using result_t = typename NiceTensor<PARAM_T>::real_t;

   result_t result(0.);
   result_t sc_i(0.);
   PARAM_T val(0.);

   switch   (  type
            )
   {
      case BaseTensor<PARAM_T>::typeID::SIMPLE:
      {
         auto* dy_data     = static_cast<SimpleTensor<PARAM_T>*>(aDeltaY.GetTensor())->GetData();
         auto* yold_data   = static_cast<SimpleTensor<PARAM_T>*>(aYOld.GetTensor())->GetData();
         auto* ynew_data   = static_cast<SimpleTensor<PARAM_T>*>(aYNew.GetTensor())->GetData();

         for(In i=I_0; i<size; ++i)
         {
            sc_i = aAbsTol + std::max(std::abs(yold_data[i]), std::abs(ynew_data[i]))*aRelTol;

            val = dy_data[i]/sc_i;
         
            result += std::real(midas::math::Conj(val) * val);
         }
         break;
      }
      case BaseTensor<PARAM_T>::typeID::SCALAR:
      {
         sc_i = aAbsTol + std::max(std::abs(aYOld.GetScalar()), std::abs(aYNew.GetScalar()))*aRelTol;

         val = aDeltaY.GetScalar() / sc_i;

         result = std::real(midas::math::Conj(val) * val);

         break;
      }
      default:
      {
         MIDASERROR("OdeNorm2 not implemented for type: " + aDeltaY.ShowType());
      }
   }

   return result;
}

//! Mean Norm2 for ODE
template
   <  typename PARAM_T
   ,  typename STEP_T
   >
typename NiceTensor<PARAM_T>::real_t OdeMeanNorm2
   (  const NiceTensor<PARAM_T>& aDeltaY
   ,  STEP_T aAbsTol
   ,  STEP_T aRelTol
   ,  const NiceTensor<PARAM_T>& aYOld
   ,  const NiceTensor<PARAM_T>& aYNew
   )
{
   return OdeNorm2(aDeltaY, aAbsTol, aRelTol, aYOld, aYNew) / aDeltaY.TotalSize();
}

//! Max Norm2 for ODE
template
   <  typename PARAM_T
   ,  typename STEP_T
   >
typename NiceTensor<PARAM_T>::real_t OdeMaxNorm2
   (  const NiceTensor<PARAM_T>& aDeltaY
   ,  STEP_T aAbsTol
   ,  STEP_T aRelTol
   ,  const NiceTensor<PARAM_T>& aYOld
   ,  const NiceTensor<PARAM_T>& aYNew
   )
{
   auto size = aDeltaY.TotalSize();
   assert(size == aYOld.TotalSize());
   assert(size == aYNew.TotalSize());
   auto type = aDeltaY.Type();
   assert(type == aYOld.Type());
   assert(type == aYNew.Type());

   using result_t = typename NiceTensor<PARAM_T>::real_t;

   result_t result(0.);
   result_t sc_i(0.);
   PARAM_T val(0.);

   switch   (  type
            )
   {
      case BaseTensor<PARAM_T>::typeID::SIMPLE:
      {
         auto* dy_data     = static_cast<SimpleTensor<PARAM_T>*>(aDeltaY.GetTensor())->GetData();
         auto* yold_data   = static_cast<SimpleTensor<PARAM_T>*>(aYOld.GetTensor())->GetData();
         auto* ynew_data   = static_cast<SimpleTensor<PARAM_T>*>(aYNew.GetTensor())->GetData();

         for(In i=I_0; i<size; ++i)
         {
            sc_i = aAbsTol + std::max(std::abs(yold_data[i]), std::abs(ynew_data[i]))*aRelTol;

            val = dy_data[i]/sc_i;
         
            result = std::max(result, std::real(midas::math::Conj(val) * val));
         }

         break;
      }
      case BaseTensor<PARAM_T>::typeID::SCALAR:
      {
         sc_i = aAbsTol + std::max(std::abs(aYOld.GetScalar()), std::abs(aYNew.GetScalar()))*aRelTol;

         val = aDeltaY.GetScalar() / sc_i;

         result = std::real(midas::math::Conj(val) * val);

         break;
      }
      default:
      {
         MIDASERROR("OdeMaxNorm2 not implemented for type: " + aDeltaY.ShowType());
      }
   }

   return result;
}

#endif /* NICETENSOR_IMPL_H_INCLUDED */
