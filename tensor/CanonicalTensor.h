#ifndef CANONICALTENSORINTERFACE_H_INCLUDED
#define CANONICALTENSORINTERFACE_H_INCLUDED

// include declaration
#include"tensor/CanonicalTensor_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
// include implementation
#include"tensor/CanonicalTensor_Impl.h"
#include"tensor/CanonicalTensor_Fits.h"
#endif /* DISABLE_PRECOMPILED_TEMPLATES */

//#include"tensor/CanonicalTensor_SolverWrapper.h"

#endif /* CANONICALTENSORINTERFACE_H_INCLUDED */
