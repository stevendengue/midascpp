/**
 ************************************************************************
 * 
 * @file                BaseTensor.h
 *
 * Created:             02.10.2014
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * \brief               BaseTensor: Base class for tensor representations.
 * 
 * Last modified: Wed Oct 22, 2014  18:44
 * 
 * Detailed Description: BaseTensor and derived types are not supposed
 *                       to be used directly, except when directly dealing
 *                       with the particular tensor representation. Use
 *                       NiceTensor.
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef BASETENSOR_DECL_H_INCLUDED
#define BASETENSOR_DECL_H_INCLUDED

// std headers
#include<vector>
#include<cstddef>
#include<cmath>
#include<iostream>
#include<string>
#include<utility>
#include<cassert>
#include<memory>

#include "util/Io.h"
#include "util/Memalloc.h"
#include "tensor/BaseTensorAllocator.h"
#include "util/type_traits/Complex.h"

/* ------- Declarations for friend classes and functions ------------ */
template <class T>
class BaseTensor;

template <class T>
BaseTensor<T>* contract
   (  const BaseTensor<T>&
   ,  const std::vector<unsigned int>&
   ,  const BaseTensor<T>&
   ,  const std::vector<unsigned int>&
   ,  bool = false
   ,  bool = false
   );

template <class T>
std::ostream& operator<<(std::ostream&, const BaseTensor<T>&);
/* ------- End of declarations for friend classes and functions ------------ */

//! Base class for representing tensors.
/*! A tensor is a collection of values ordered by some multi-index:
 *  \f$  t_\mathbf{i} \f$,
 *  where \f$\mathbf{i}=\{i_1, i_2, \dots i_D\} = \{i_j: 1\le j \le D, 1\le i_j \le N_j \}\f$.
 *  \f$D\f$ is the number of dimensions. The *shape* of the tensor
 *  is the set of sizes, or set of values each index can take, i.e.
 *  \f$\{N_j |  1\le j \le D \}\f$.
**/
template <class T>
class BaseTensor
   :  protected midas::tensor::BaseTensorAllocator<T >
   ,  protected midas::tensor::BaseTensorAllocator<T*>
{
   public:
      using Allocator_Tp  = midas::tensor::BaseTensorAllocator<T >;
      using Allocator_Tpp = midas::tensor::BaseTensorAllocator<T*>;

      using UniqueArray = typename Allocator_Tp::UniqueArray;
      using RawPointer = typename Allocator_Tp::RawPointer;

   protected:
      std::vector<unsigned int> mDims; ///< Number of dimensions.

   public:
      using dot_t = typename DotEvaluator<T>::dot_t;
      using real_t = typename midas::type_traits::RealTypeT<T>;

      //! Tags for NiceTensor IO (see #Write and e.g. SimpleTensor(std::istream &))
      using tag_t = unsigned int;
      static const tag_t TAG_SIMPLE   =0; ///< Tag for SimpleTensor
      static const tag_t TAG_CANONICAL=1; ///< Tag for CanonicalTensor

      static const tag_t TAG_SCALAR   =2; ///< Tag for Scalar
      static const tag_t TAG_CANON_IM =3; ///< Tag for CanonicalIntermediate
      static const tag_t TAG_GRAPH    =4; ///< Tag for TensorGraph
      static const tag_t TAG_DIRPROD  =5; ///< Tag for TensorDirectProduct
      static const tag_t TAG_SUM      =6; ///< Tag for TensorSum

      //! Identifiers for dynamic casts. \see Type()
      enum class typeID: int
      {
        SIMPLE = 0   ///< Identifier for SimpleTensor
      , CANONICAL    ///< Identifier for CanonicalTensor
      , ZERO         ///< Identifier for ZeroTensor
      , SCALAR       ///< Identifier for Scalar
      , CANON_IM     ///< Identifier for CanonicalIntermediate
      , GRAPH        ///< Identifier for TensorGraph
      , VIEW         ///< Identifier for TensorView
      , DIRPROD      ///< Identifier for TensorDirectProduct
      , SUM          ///< Identifier for TensorSum
      , ENERGYHOLDER ///< Identifier for EnergyHolderTensorBase
      };

   public:
      /*! Usage example:
       * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
       * switch(base_tensor_instance.Type())
       * {
       *    case BaseTensor<T>::typeID::SIMPLE:
       *    {
       *       const SimpleTensor<T>& simple_tensor_instance=dynamic_cast<const SimpleTensor<T>&>(base_tensor_instance);
       *       ...
       *    }
       * }
       * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       */
      virtual typeID          Type() const = 0; ///< Returns the relevant #typeID.
      virtual std::string ShowType() const = 0; ///< Returns a string representing the type of the tensor.
   private:
      /* -------------- Implementations -------------
       * These private pure virtual methods take an awful lot of arguments
       * that are figured out by their respective public functions.
       */
      //! Representation-specific implementation of general tensor contractions. Called by contract().
      /*! 
       *
       *  @param indicespos_inner_left    Locations of the internal indices of the left tensor,
       *                                  ordered from the right.
       *  @param indicespos_outer_left    Locations of the outer indices of the left tensor,
       *                                  ordered from the right.
       *  @param right                    Right tensor.
       *  @param indicespos_inner_right   Locations of the internal indices of the right tensor,
       *                                  matching the order in \p indicespos_inner_left.
       *  @param indicespos_outer_right   Locations of the outer indices of the left tensor,
       *                                  from the right.
       *  @param dims_result              Dimensions of the tensor resulting from the contraction.
       *  @param indicespos_result_left   Positions that the dimensions at the positions in
       *                                  indicespos_outer_left will occupy in the resulting tensor,
       *                                  as determined by the lexicographic value of the index
       *                                  for that dimension.
       *  @param indicespos_result_right  Positions that the dimensions at the positions in
       *                                  indicespos_outer_right will occupy in the resulting tensor,
       *                                  as determined by the lexicographic value of the index
       *                                  for that dimension.
       *  @param conjugate_left           Complex conjugate the elements of the left tensor
       *  @param conjugate_right          Complex conjugate the elements of the right tensor
       *
       *  Example
       *  -------
       *
       *  Consider the following contraction:
       */
      virtual BaseTensor* Contract_impl
         (  const std::vector<unsigned int>& indicespos_inner_left
         ,  const std::vector<unsigned int>& indicespos_outer_left
         ,  const BaseTensor<T>&             right
         ,  const std::vector<unsigned int>& indicespos_inner_right
         ,  const std::vector<unsigned int>& indicespos_outer_right
         ,  const std::vector<unsigned int>& dims_result
         ,  const std::vector<unsigned int>& indicespos_result_left
         ,  const std::vector<unsigned int>& indicespos_result_right
         ,  bool conjugate_left = false
         ,  bool conjugate_right= false
         )  const = 0;

      //! Called by ContractInternal().
      virtual BaseTensor* ContractInternal_impl(
            const std::vector<std::pair<unsigned int, unsigned int>>&,
            const std::vector<unsigned int>&) const=0;

      //! Called by Reorder(), who parses its input and calculates \p neworder and \p newdims.
      /*! @param neworder New position assigned to a given dimension, e.g. {1,2,0}
       *                  means that the first dimension becomes the second one,
       *                  the second dimension becomes the third one, and the third
       *                  dimensions becomes the first one.
       *  @param newdims  Dimensions of the output tensor.
       *
       */
      virtual BaseTensor* Reorder_impl(const std::vector<unsigned int>& neworder,
                                       const std::vector<unsigned int>& newdims) const=0;

      //! Called by Slice().
      virtual BaseTensor* Slice_impl(const std::vector<std::pair<unsigned int,unsigned int>>&) const=0;

      //! Complex conjugate (in-place)
      virtual void ConjugateImpl
         (
         )
      {
         MIDASERROR("Not implemented for tensor type: " + this->ShowType());
      };

   public:
//*=======CONSTRUCTORS, DESTRUCTOR, ASSIGNMENTS ==============*/
      //! Default constructor (no elements)
      BaseTensor();
      //! Basic constructor to set up #mDims, to be used by all other derived classes.
      BaseTensor(const std::vector<unsigned int>&);
      
      //! Basic constructor to set up #mDims, to be used by all other derived classes.
      BaseTensor(std::vector<unsigned int>&&);

      //! Does nothing, required by pure virtual destructor...
      virtual ~BaseTensor() = 0;

      //! Create a hard copy of the tensor.
      virtual BaseTensor* Clone() const=0;

      //!
      virtual bool Assign(BaseTensor const * const) { return false; }


/*============== QUERY ==============*/
      //! Number of dimensions (AKA order) of the tensor ( #mDims .size() ).
      unsigned int NDim()                 const;
      //! Total number of elements ( \f$\prod_i N_i \f$ ).
      std::size_t  TotalSize()            const;
      //! 
      unsigned Extent(unsigned i) const;

      //! Get maximum extent
      unsigned MaxExtent() const;

      //! Get minimum extent
      unsigned MinExtent() const;

      //! Get the dimensions (shape) of the tensor.
      const std::vector<unsigned int>& GetDims() const;
            std::vector<unsigned int>& GetDims(); // <<--- this is not good design :C

      //! Get dimensions as string
      std::string ShowDims() const;

/*============== CONTRACTIONS ==============*/
      //! Contract one dimension away with a vector. See BaseTensor::ContractDown.
      virtual BaseTensor* ContractDown   (const unsigned int, const BaseTensor*) const=0;
      //! Transform a dimension with a matrix. See BaseTensor::ContractForward.
      virtual BaseTensor* ContractForward(const unsigned int, const BaseTensor*) const=0;
      //! Insert a dimension with a vector. See BaseTensor::ContractUp.
      virtual BaseTensor* ContractUp     (const unsigned int, const BaseTensor*) const=0;

   protected:
      size_t NBefore(const unsigned int)  const;
      size_t NAfter (const unsigned int)  const;

   public:
/*============ OTHER OPERATIONS ============*/
      //! Scaling (in place).
      virtual BaseTensor* operator*= (const T)       =0;
      //! Scaling. 
      virtual BaseTensor* operator*  (const T) const;

      //! Add another tensor (in place).
      virtual BaseTensor* operator+=(const BaseTensor&) = 0;
      //! Subtract another tensor (in place).
      virtual BaseTensor* operator-=(const BaseTensor&);
      //! Element-wise multiplication (in-place).
      virtual BaseTensor* operator*=(const BaseTensor&) =0;
      //! Element-wise division (in-place)
      virtual BaseTensor* operator/=(const BaseTensor&) =0;

      //! Axpy (Y = aX + Y); add a scalar times a tensor to this tensor, in-place.
      virtual void Axpy(const BaseTensor&, const T&) {MIDASERROR("Axpy not implemented for '"+this->ShowType()+"'."); }

      //! Norm of the tensor.
      real_t Norm() const;
      
      //! Norm2 of the tensor.
      virtual real_t Norm2() const;

      //! Safe norm2
      real_t SafeNorm2() const;

      //! Sanity check
      virtual bool SanityCheck
         (
         )  const
      {
         MIDASERROR("NOT IMPLEMENTED FOR TYPE: " + this->ShowType());
         return false;
      }

      //! Value of element
      virtual T Element
         (  const std::vector<unsigned>&
         )  const
      {
         MIDASERROR("NOT IMPLEMENTED FOR TYPE: " + this->ShowType());
         return C_0;
      }

      //! Optimize pivot (find largest possible element of tensor)
      virtual real_t OptimizePivot
         (  std::vector<unsigned>& arPivot
         ,  const unsigned aMaxSteps=0
         )  const
      {
         MidasWarning("OptimizePivot not implemented for " + this->ShowType());
         return std::abs<real_t>(this->Element(arPivot));
      }

      //! Set tensor to absolute value
      virtual void Abs() { MIDASERROR("NOT IMPLEMENTED FOR TYPE: " + this->ShowType()); }

      //! Complex conjugate (in-place)
      void Conjugate
         (
         );

      //! Zero the tensor
      virtual void Zero() { MIDASERROR("NOT IMPLEMENTED FOR TYPE: " + this->ShowType()); }
      
      //! Scale the tensor
      virtual void Scale(const T&) { MIDASERROR("NOT IMPLEMENTED FOR TYPE: " + this->ShowType()); }
      
      //! Do elementwise stuff
      virtual void ElementwiseScalarAddition(const T&) { MIDASERROR("NOT IMPLEMENTED FOR TYPE: " + this->ShowType()); }
      virtual void ElementwiseScalarSubtraction(const T&) { MIDASERROR("NOT IMPLEMENTED FOR TYPE: " + this->ShowType()); }

      //! Dump a vector to canonical format
      std::unique_ptr<BaseTensor<T>> DumpVectorToCanonical
         (
         )  const;

      //! Reduce the rank of a 2nd-order tensor (matrix) using SVD
      std::unique_ptr<BaseTensor<T>> SvdRankReduction
         (  real_t
         ,  In=-I_1
         )  const;

      //! General tensor contraction. Calls BaseTensor::Contract_impl()
      friend BaseTensor* contract<>
         (  const BaseTensor<T>&
         ,  const std::vector<unsigned int>&
         ,  const BaseTensor<T>&
         ,  const std::vector<unsigned int>&
         ,  bool
         ,  bool
         );

      BaseTensor* ContractInternal(const std::vector<unsigned int>&) const;


      //! Return a new tensor by reordering dimensions.
      virtual BaseTensor* Reorder(const std::vector<unsigned int>&) const;

      //! Return a sub-section of the tensor.
      BaseTensor* Slice(const std::vector<std::pair<unsigned int,unsigned int>>&) const;
      
      //! Dump tensor into pointer
      virtual void DumpInto(T*) const;

/*============ PRETTY PRINT ============*/
      //! Push the output of Prettify() to std::ostream.
      friend std::ostream& operator<< <>(std::ostream&, const BaseTensor&);
      //! Show tensor representation in human readable format.
      virtual std::string Prettify() const;

/*============  IO ============*/
              std::ostream& Write     (std::ostream& output) const;
      virtual std::ostream& write_impl(std::ostream& output) const=0;

   protected:
      std::istream& ReadDims(std::istream& input);
   
   public:
      /**************************** Dot product ****************************/
      // Interface
      virtual dot_t Dot(const BaseTensor<T>* const) const = 0; // pure virtual, children must implement

      // Dispatches
      virtual dot_t DotDispatch(const BaseTensor<T>* const) const;
      virtual dot_t DotDispatch(const SimpleTensor<T>* const) const;
      virtual dot_t DotDispatch(const CanonicalTensor<T>* const) const;
      virtual dot_t DotDispatch(const Scalar<T>* const) const;
      virtual dot_t DotDispatch(const ZeroTensor<T>* const) const;
      virtual dot_t DotDispatch(const TensorDirectProduct<T>* const) const;
      virtual dot_t DotDispatch(const TensorSum<T>* const) const;
      /**************************** Dot product end ************************/

      Allocator_Tp GetAllocatorTp() const { return static_cast<const Allocator_Tp&>(*this); }
      Allocator_Tpp GetAllocatorTpp() const { return static_cast<const Allocator_Tpp&>(*this); }
      
#ifdef VAR_MPI
      /**************************** MPI INTERFACE ****************************/
   public:
      virtual void MpiSend(In aRecievingRank) const;
      virtual void MpiRecv(In aSendingRank);
      virtual void MpiBcast(In aRoot);
      static std::unique_ptr<BaseTensor<T> > ConstructFromType(int type);
   protected:
      void MpiSendDimensions(In aRecievingRank) const;
      void MpiRecvDimensions(In aSendingRank);
      void MpiBcastDimensions(In aRoot);
#endif /* VAR_MPI */
};

template <class T>
BaseTensor<T>* operator*(const T, const BaseTensor<T>&);

template <class T>
bool have_same_shape  (const BaseTensor<T>&, const BaseTensor<T>&);

template <class T>
void assert_same_shape(const BaseTensor<T>&, const BaseTensor<T>&);
template <class T>
void assert_same_shape(const BaseTensor<T>* const, const BaseTensor<T>* const);

template <class T>
typename BaseTensor<T>::real_t diff_norm(const BaseTensor<T>&, const BaseTensor<T>&);

template <class T>
typename BaseTensor<T>::real_t diff_norm_new(const BaseTensor<T>&, const BaseTensor<T>&);

template <class T>
typename BaseTensor<T>::real_t diff_norm_new
   (  const BaseTensor<T>* const
   ,  const typename BaseTensor<T>::real_t
   ,  const BaseTensor<T>* const
   ,  const typename BaseTensor<T>::real_t
   ,  T = static_cast<T>(0.0)
   ,  const size_t=1000
   );

template <class T>
typename BaseTensor<T>::real_t diff_norm2_new
   (  const BaseTensor<T>* const
   ,  const typename BaseTensor<T>::real_t
   ,  const BaseTensor<T>* const
   ,  const typename BaseTensor<T>::real_t
   ,  T = static_cast<T>(0.0)
   ,  const size_t=1000
   );

template <class T>
typename BaseTensor<T>::real_t safe_diff_norm2
   (  const BaseTensor<T>* const
   ,  const BaseTensor<T>* const
   );

#endif /* BASETENSOR_DECL_H_INCLUDED */
