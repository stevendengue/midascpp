/**
************************************************************************
* 
* @file    CpAsdDecomposer.h
*
* @date    25-01-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Tensor decomposer for fitting a tensor to a fixed rank using 
*     alternating steepest descent algorithms.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef CPASDDECOMPOSER_H_INCLUDED
#define CPASDDECOMPOSER_H_INCLUDED

#include "tensor/TensorDecomposer.h"
#include "tensor/TensorDecompInfo.h"

namespace midas
{
namespace tensor
{

/**
 * Decomposer class using ASD algorithm
 **/
class CpAsdDecomposer
   :  public detail::ConcreteCpDecomposerBase
{
   private:
      //! Use pivotised ASD
      bool mPivotised;

      //! Threshold for CP-ASD algorithm (gradient norm)
      Nb mCpAsdThreshold;

      //! Relative threshold for CP-ASD algorithm
      Nb mCpAsdRelativeThreshold;

      //! Relative threshold for checkiter in FitASD
      Nb mCpAsdRelativeCheckThresh;

      //! Reference to TensorDecompInfo
      const TensorDecompInfo& mInfo; 


      //! Implementation of CheckDecomposition
      bool CheckDecompositionImpl
         (  const NiceTensor<Nb>&
         )  const override;
      
      //! Implementation of Decompose with argument guess.
      FitReport<Nb> DecomposeImpl
         (  const NiceTensor<Nb>&
         ,  Nb
         ,  NiceTensor<Nb>&
         ,  Nb
         )  const override;

      //! Implementation of Type
      std::string TypeImpl
         (
         )  const override;

   public:
      //! c-tor from TensorDecompInfo
      CpAsdDecomposer
         (  const TensorDecompInfo&
         );
};

} /* namespace tensor */
} /* namespace midas */
#endif /* CPASDDECOMPOSER_H_INCLUDED */
