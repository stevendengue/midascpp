#ifndef TENSORDIRECTPRODUCT_IMPL_H_INCLUDED
#define TENSORDIRECTPRODUCT_IMPL_H_INCLUDED

#include "tensor/TensorDirectProduct.h"
#include "tensor/CanonicalTensor.h"
#include "util/CallStatisticsHandler.h"
#include "util/stream/ScopedManipulators.h"
#include "libmda/numeric/float_eq.h"
#include "util/Io.h"
#include "util/Math.h"

/**
 * Constructor from indices.
 **/
template<class T>
TensorDirectProduct<T>::TensorDirectProduct
   (  const std::vector<unsigned>& aDims
   ,  T aCoef
   ,  std::vector<std::unique_ptr<BaseTensor<T> > >&& aTensors
   ,  connection_t&& aConnection
   )
   :  BaseTensor<T>(aDims)
   ,  mCoef(aCoef)
   ,  mTensors(std::move(aTensors))
   ,  mIndexMap(std::move(aConnection))
{
   mReverseConnection.resize(mTensors.size());
   for(int i = 0; i < mReverseConnection.size(); ++i)
   {
      mReverseConnection.at(i).resize(mTensors.at(i)->NDim());
      for(int j = 0; j < mReverseConnection.at(i).size(); ++j)
      {
         for(int k = 0; k < mIndexMap.size(); ++k)
         {
            if(mIndexMap.at(k).first == i && mIndexMap.at(k).second == j)
            {
               mReverseConnection.at(i).at(j) = k;
               break;
            }
         }
      }
   }
//   std::cout << " Indexmap          : " << mIndexMap << std::endl;
//   std::cout << " ReverseConnection : " << mReverseConnection << std::endl;
}

/**
 * Get type ID
 * @return           DIRPROD type ID
 **/
template<class T>
typename BaseTensor<T>::typeID TensorDirectProduct<T>::Type
   (
   )  const
{
   return BaseTensor<T>::typeID::DIRPROD;
}

/**
 * Get type string
 * @return               String "TensorDirectProduct".
 **/
template<class T>
std::string TensorDirectProduct<T>::ShowType
   (
   )  const
{
   return "TensorDirectProduct";
}

/**
 * Implementation of contract 
 **/
template<class T>
BaseTensor<T>* TensorDirectProduct<T>::Contract_impl
   (  const std::vector<unsigned int>& indicespos_inner_left
   ,  const std::vector<unsigned int>& indicespos_outer_left
   ,  const BaseTensor<T>&             right
   ,  const std::vector<unsigned int>& indicespos_inner_right
   ,  const std::vector<unsigned int>& indicespos_outer_right
   ,  const std::vector<unsigned int>& dims_result
   ,  const std::vector<unsigned int>& indicespos_result_left
   ,  const std::vector<unsigned int>& indicespos_result_right
   ,  bool conjugate_left
   ,  bool conjugate_right
   )  const
{
   MIDASERROR("Not Implemented");
   return nullptr;
}

/**
 * Implementation of internal contraction
 **/
template<class T>
BaseTensor<T>* TensorDirectProduct<T>::ContractInternal_impl
   (  const std::vector<std::pair<unsigned int, unsigned int> >&
   ,  const std::vector<unsigned int>&
   )  const
{
   MIDASERROR("Not Implemented");
   return nullptr;
}

/**
 * Implementation of reordering
 **/
template<class T>
BaseTensor<T>* TensorDirectProduct<T>::Reorder_impl
   (  const std::vector<unsigned int>& neworder
   ,  const std::vector<unsigned int>& newdims
   )  const
{
   MIDASERROR("Not Implemented");
   return nullptr;
}

/**
 * 
 **/
template<class T>
BaseTensor<T>* TensorDirectProduct<T>::Slice_impl
   (  const std::vector<std::pair<unsigned int,unsigned int> >&
   )  const
{
   MIDASERROR("Not Implemented");
   return nullptr;
}

/**
 *
 **/
template
   <  class T
   >
void TensorDirectProduct<T>::ConjugateImpl
   (
   )
{
   mCoef = midas::math::Conj(mCoef);

   for(auto& tens : mTensors)
   {
      tens->Conjugate();
   }
}

/**
 * 
 **/
template<class T>
BaseTensor<T>* TensorDirectProduct<T>::Clone
   (
   )  const
{
   // clone all tensors
   std::vector<std::unique_ptr<BaseTensor<T> > > cloned_tensors(mTensors.size());
   for(int itens = 0; itens < mTensors.size(); ++itens)
   {
      cloned_tensors[itens] = std::unique_ptr<BaseTensor<T> >(mTensors[itens]->Clone());
   }
   // clone connection
   auto cloned_connection = mIndexMap;
   
   // return cloned dirprod
   return new TensorDirectProduct<T>(this->GetDims(), mCoef, std::move(cloned_tensors), std::move(cloned_connection));
}

/**
 * Convert direct product to CanonicalTensor
 *
 * @param balance    Balance the mode vectors before return
 *
 * @return
 *    Result as BaseTensorPtr
 **/
template <class T>
BaseTensorPtr<T> TensorDirectProduct<T>::ConstructModeMatrices
   (  bool balance
   )  const
{
   //LOGCALL("calls");

   // Assert all canonical
   assert(  this->AllCanonical() );

   // Some declarations
   auto ntens = this->mTensors.size();
   auto rank = this->Rank();
   auto resid_rank = rank;
   unsigned repititions = 1;

   // Initialize result tensor
   auto result = std::unique_ptr<BaseTensor<T>>(new CanonicalTensor<T>(this->mDims, rank));

   // Result ptr
   auto result_tensor_ptr = static_cast<CanonicalTensor<T>*>(result.get());

   // Loop over tensors in TensorDirectProduct
   for(size_t itens=0; itens<ntens; ++itens)
   {
      // Get stuff
      const auto& tensor = *static_cast<CanonicalTensor<T>*>(this->mTensors[itens].get());
      const auto& irank = tensor.GetRank();
      
      // Resid rank determines the number of successive repetitions of each vector in the mode matrices of itens.
      resid_rank /= irank;

      // Get index numbers represented by itens
      const auto& index_numbers = this->mReverseConnection[itens];

      // Loop over mode matrices (dimensions)
      size_t idim_tensor = 0;
      for(const auto& idim_result : index_numbers)
      {
         // Get pointer to mode matrix of result
         T* result_mat_ptr = result_tensor_ptr->GetModeMatrices()[idim_result];

         // Get extent of current mode matrix
         auto iextent = tensor.Extent(idim_tensor);

         // Loop over repititions of the overall pattern
         for(size_t irep2=0; irep2<repititions; ++irep2)
         {
            // Loop over mode vectors
            for(size_t ivec=0; ivec<irank; ++ivec)
            {
               // Get pointer to mode vector of itens
               T* vec_ptr = tensor.GetModeMatrices()[idim_tensor] + ivec*iextent;

               // Loop over successive repititions of a single mode vector
               for(size_t irep1=0; irep1<resid_rank; ++irep1)
               {
                  // Copy data
                  for(size_t i=0; i<iextent; ++i)
                  {
                     *(result_mat_ptr++) = vec_ptr[i];
                  }
               }
            }
         }
         ++idim_tensor;
      }

      // Repititions determines the number of times we repeat the combination of mode vectors
      repititions *= irank;
   }

   // Scale result by mCoef
   result->Scale(this->mCoef);

   // Balance the mode vectors if requested
   if (  balance  )
   {
      result_tensor_ptr->BalanceModeVectors();
   }

   // Debug check
   if (  gDebug
      )
   {
      auto err = std::sqrt(safe_diff_norm2(this, result.get()));
      if (  !libmda::numeric::float_numeq_zero(err, 1., 2)
         )
      {
         midas::stream::ScopedPrecision(15, Mout);
         std::cout   << "Error in TensorDirectProduct::ConstructModeMatrices!\n"
                     << "\terr   = " << err << std::endl;
         exit(1);
      }
   }

   // Return result as BaseTensorPtr
   return result;
}

/**
 * 
 **/
template<class T>
typename BaseTensor<T>::real_t TensorDirectProduct<T>::Norm2
   (
   )  const
{
   real_t norm2 = std::real(midas::math::Conj(mCoef)*mCoef);
   for(int i = 0; i < mTensors.size(); ++i)
   {
      norm2 *= mTensors[i]->Norm2();
   }
   return norm2;
}

/**
 * Value of element
 *
 * @param aIndices   The indices
 * @return
 *    Value of element
 **/
template <class T>
T TensorDirectProduct<T>::Element
   (  const std::vector<unsigned>& aIndices
   )  const
{
   T result = mCoef;

   const auto& n_tensors = this->mTensors.size();

   std::vector<unsigned> indices_i;
   indices_i.reserve( aIndices.size() );

   for(size_t i=0; i<n_tensors; ++i)
   {
      const auto& index_numbers = mReverseConnection[i];
      const auto& n_indices = index_numbers.size();
      indices_i.resize( n_indices );
      for(size_t j=0; j<n_indices; ++j)
      {
         indices_i[j] = aIndices[index_numbers[j]];
      }

      result *= this->mTensors[i]->Element(indices_i);
   }

   return result;
}

/**
 * 
 **/
template<class T>
unsigned TensorDirectProduct<T>::Rank
   (
   )  const
{
   unsigned rank = 0;
   if (  mTensors.size()
      )
   {
      rank = 1;
      for(int i = 0; i < mTensors.size(); ++i)
      {
         if(mTensors[i]->Type() == BaseTensor<T>::typeID::CANONICAL)
         {
            rank *= static_cast<const CanonicalTensor<T>* const>(mTensors[i].get())->GetRank();
         }
         else if  (  mTensors[i]->Type() == BaseTensor<T>::typeID::SCALAR
                  )
         {
            // Do nothing
         }
         else
         {
            MidasWarning("TensorDirectProduct: Returning rank = 0 because the product contains " + mTensors[i]->ShowType());
            rank = 0;
            break;
         }
      }
   }
   return rank;
}

/**
 * 
 **/
template<class T>
void TensorDirectProduct<T>::NormalizeTensors
   (
   )
{
   for(int itens = 0; itens < mTensors.size(); ++itens)
   {
      auto norm = mTensors[itens]->Norm();
      mTensors[itens]->Scale(static_cast<T>(1.0)/norm);
      mCoef *= norm;
   }
}

/**
 * 
 **/
template<class T>
void TensorDirectProduct<T>::NormalizeTensors
   (  const std::vector<real_t>& aNorm2s
   )
{
   auto size = mTensors.size();
   real_t norm(0.);

   for(int itens = 0; itens < size; ++itens)
   {
      norm = std::sqrt( aNorm2s[itens] );
      mTensors[itens]->Scale(static_cast<T>(1.0)/norm);
      mCoef *= norm;
   }
}

/**
 * 
 **/
template<class T>
BaseTensor<T>* TensorDirectProduct<T>::ContractDown
   (  const unsigned int
   ,  const BaseTensor<T>*
   )  const
{
   MIDASERROR("Not implemented");
   return nullptr;
}

/**
 * 
 **/
template<class T>
BaseTensor<T>* TensorDirectProduct<T>::ContractForward
   (  const unsigned int
   ,  const BaseTensor<T>*
   )  const
{
   MIDASERROR("Not implemented");
   return nullptr;
}

/**
 * 
 **/
template<class T>
BaseTensor<T>* TensorDirectProduct<T>::ContractUp
   (  const unsigned int
   ,  const BaseTensor<T>*
   )  const
{
   MIDASERROR("Not implemented");
   return nullptr;
}

/**
 *
 **/
template<class T>
bool TensorDirectProduct<T>::AllCanonical
   (
   )  const
{
   bool allcanonical = true; // assume all are canonical
   for(int i = 0; i < mTensors.size(); ++i)
   {
      //Mout << " TYPE " << i << " : " << mTensors[i]->ShowType() << std::endl;
      if(mTensors[i]->Type() != BaseTensor<T>::typeID::CANONICAL)
      {
         allcanonical = false;
         break; // break for
      }
   }
   return allcanonical;
}

/**
 *
 **/
template<class T>
bool TensorDirectProduct<T>::SanityCheck() const
{
   bool sane = true;

   for(size_t i = 0; i < mTensors.size(); ++i)
   {
      sane = sane && mTensors[i]->SanityCheck();
   }

   return sane;
}

/**
 *
 **/
template<class T>
std::string TensorDirectProduct<T>::Prettify
   (
   )  const
{
   std::stringstream out;

   out << "TensorDirectProduct : " << this->NDim() << "   [ " ;
   if(this->NDim()>0)
   {
      out << this->mDims[0];
      for (unsigned int i=1;i<this->NDim();i++)
      {
         out << ", " << this->mDims[i];
      }
   }
   out << " ]   IndexMap: " << mIndexMap << std::endl;;
   out << "Rank: " << this->Rank() << std::endl;
   
   for(int i = 0; i < mTensors.size(); ++i)
   {
      out << " Tensor " << i << " : " << mReverseConnection[i] << "\n" << mTensors[i]->Prettify() << "\n";
   }
   
   return out.str();
}

/**
 *
 **/
template<class T>
void TensorDirectProduct<T>::DumpInto
   (  T* ptr
   )  const
{
   auto total_size = this->TotalSize();
   // zero the incoming ptr
   for(int i = 0; i < total_size; ++i)
   {
      ptr[i] = static_cast<T>(0.0);
   }

   std::vector<std::unique_ptr<T[]> > ptrs(mTensors.size());

   // dump each tensor
   for(unsigned itens = 0; itens < mTensors.size(); ++itens)
   {
      auto size = mTensors[itens]->TotalSize();
      ptrs[itens] = std::make_unique<T[]>(size);
      mTensors[itens]->DumpInto(ptrs[itens].get());
   }

   // then do direct product
   std::vector<unsigned> ivec;
   ivec.resize(this->GetDims().size());
   for(int i = 0; i < ivec.size(); ++i)
   {
      ivec[i] = static_cast<unsigned>(0);
   }
   
   auto IncIvec = [&ivec, this]()
   {
      for (int i = ivec.size() - 1; i >= 0; --i)
      {
         if (++ivec[i] == this->GetDims()[i])
         {
            ivec[i] = I_0;
         }
         else
         {
            break;
         }
      }

   };

   unsigned i_res = I_0;
   while(true)
   {
      T res = C_1;
      for (unsigned i = I_0; i < ptrs.size(); ++i)
      {
         unsigned addr = ivec[mReverseConnection[i][I_0]];
         for (unsigned k=I_1; k<mReverseConnection[i].size(); ++k)
         {
            addr = addr*this->GetDims()[mReverseConnection[i][k]] + ivec[mReverseConnection[i][k]];
         }
         res *= ptrs[i][addr];
      }
      ptr[i_res] += mCoef * res;

      if (++i_res < total_size)
      {
         IncIvec();
      }
      else
      {
         break;
      }
   }
}

/**
 *
 **/
template<class T>
typename DotEvaluator<T>::dot_t TensorDirectProduct<T>::Dot
   (  const BaseTensor<T>* const other
   )  const
{
   return midas::math::Conj(other->DotDispatch(this));
}

/**
 *
 **/
template<class T>
typename DotEvaluator<T>::dot_t TensorDirectProduct<T>::DotDispatch
   (  const CanonicalTensor<T>* const other
   )  const
{
   return midas::math::Conj(DotEvaluator<T>().Dot(other, this)); // arguments swapped
}

/**
 *
 **/
template<class T>
typename DotEvaluator<T>::dot_t TensorDirectProduct<T>::DotDispatch
   (  const TensorDirectProduct<T>* const other
   )  const
{
   return DotEvaluator<T>().Dot(this, other);
}

#endif /* TENSORDIRECTPRODUCT_IMPL_H_INCLUDED */
