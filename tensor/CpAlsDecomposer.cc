#include "tensor/CpAlsDecomposer.h"
#include "tensor/FitALS.h"
#include "tensor/TensorFits.h"

#include <cassert>
#include <limits>

namespace midas
{
namespace tensor
{

///> register CpAlsDecomposer with ConcreteCpDecomposerFactory
detail::ConcreteCpDecomposerRegistration<CpAlsDecomposer> registerCpAlsDecomposer("ALS");

/** 
 * Check if tensor is decomposed by this decomposer.
 * @param aTensor                    Tensor to check.
 * @return                           Returns whether tensor will be decomposed.
 **/
bool CpAlsDecomposer::CheckDecompositionImpl
   ( const NiceTensor<Nb>& aTensor
   ) const
{
   bool type_ok  = ( (aTensor.Type() == BaseTensor<Nb>::typeID::SIMPLE) 
                  || (aTensor.Type() == BaseTensor<Nb>::typeID::CANONICAL)
                  || (aTensor.Type() == BaseTensor<Nb>::typeID::SUM)
                  || (aTensor.Type() == BaseTensor<Nb>::typeID::DIRPROD)
                   );
   return type_ok;
}


/**
 * Decompose NiceTensor using CPALS algorithm.
 * @param aTensor        Tensor to decompose.
 * @param aTargetNorm2   Norm2 of aTensor
 * @param aFitTensor     The fitting tensor
 * @param aThresh        The requested accurcy
 * @return               FitReport of the decomposition
 **/
FitReport<Nb> CpAlsDecomposer::DecomposeImpl
   (  const NiceTensor<Nb>& aTensor
   ,  Nb aTargetNorm2
   ,  NiceTensor<Nb>& aFitTensor
   ,  Nb aThresh
   )  const
{
   // Set threshold if the driver does not require a specific accuracy
   Nb thresh = aThresh > C_0 ? aThresh : this->mCpAlsThreshold;
   Nb check_thresh = thresh * this->mCpAlsRelativeCheckThresh;
   if (  this->mCpAlsRelativeThreshold > C_0 )
   {
      thresh *= this->mCpAlsRelativeThreshold;
   }

   CpAlsInput<Nb> cpals_input(mInfo);
   cpals_input.SetMaxerr(thresh);
   cpals_input.SetCheckThresh(check_thresh);

   // Distance conv check only makes sence if we know the absolute error, which is not the case if no target norm is provided.
   if (  aTargetNorm2 < C_0
      && cpals_input.mDistanceConvCheck
      )
   {
      MidasWarning("CpAlsDecomposer: Disable distance conv check for aTargetNorm2 = " + std::to_string(aTargetNorm2));
      cpals_input.mDistanceConvCheck = false;
   }

   // Allow extra iterations for fit to max rank.
   if (  this->mMaxRankMaxIter > I_0   )
   {
      auto& dims = aTensor.GetDims();
      In max_rank = aTensor.TotalSize() / *std::max_element(dims.begin(), dims.end());
      auto rank = aFitTensor.StaticCast<CanonicalTensor<Nb>>().GetRank();

      if (  rank == max_rank  )
      {
         cpals_input.SetMaxiter( std::max(this->mCpAlsMaxIter, this->mMaxRankMaxIter) );
      }
   }

   // Check relative accuracy compared to machine precision
   auto eps = std::numeric_limits<Nb>::epsilon();
   if (  aThresh > C_0
      && aThresh < std::sqrt(aTargetNorm2*eps)
      && cpals_input.mIoLevel > I_5
      )
   {
      Mout  << " Requesting CP-ALS fit of " << aTensor.ShowDims() << " " << aTensor.ShowType() << " to high relative accuracy!\n"
            << "    Threshold:   " << aThresh << "\n"
            << "    Norm:        " << std::sqrt(aTargetNorm2) << "\n"
            << std::flush;
      MidasWarning("Requesting CP-ALS fit to high relative accuracy!");
   }

   auto fit_report   =  this->mPivotised
                     ?  FitCPPALS( aTensor, aTargetNorm2, aFitTensor, cpals_input )
                     :  FitCPALS ( aTensor, aTargetNorm2, aFitTensor, cpals_input );

   return fit_report;
}

/**
 * Get type of decomposer as a std::string.
 * @return          String with type.
 **/
std::string CpAlsDecomposer::TypeImpl
   (
   ) const
{
   return std::string("AlsDecomposer");
}

/**
 * Constructor from TensorDecompInfo.
 * @param aInfo                TensorDecompInfo to construct from.
 **/
CpAlsDecomposer::CpAlsDecomposer
   ( const TensorDecompInfo& aInfo
   )
   :  mCpAlsMaxIter              (const_cast<TensorDecompInfo&>(aInfo)["CPALSMAXITER"].get<In>())
   ,  mMaxRankMaxIter            (const_cast<TensorDecompInfo&>(aInfo)["MAXRANKMAXITER"].get<In>())
   ,  mCpAlsThreshold            (const_cast<TensorDecompInfo&>(aInfo)["CPALSTHRESHOLD"].get<Nb>())
   ,  mCpAlsRelativeThreshold    (const_cast<TensorDecompInfo&>(aInfo)["CPALSRELATIVETHRESHOLD"].get<Nb>())
   ,  mCpAlsRelativeCheckThresh  (const_cast<TensorDecompInfo&>(aInfo)["CHECKTHRESH"].get<Nb>())
   ,  mPivotised                 (const_cast<TensorDecompInfo&>(aInfo)["PIVOTISEDCPALS"].get<bool>())
   ,  mInfo                      (aInfo)
{
}

} /* namespace tensor */
} /* namespace midas */
