/**
 ************************************************************************
 * 
 * @file                CanonicalTensor_SolverWrapper_Impl.h
 *
 * Created:             02.10.2014
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * Short Description:   Wraps CanonicalTensor to call Ian's solvers.
 * 
 * Last modified: Tue Dec 16, 2014  15:02
 * 
 * Detailed Description: Provides interfaces to +, *, and the target 
 *                       function which implements () and first_derivative
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

//! Construct by copying an existing tensor.
template <class T>
CanonicalTensor_SolverWrapper<T>::CanonicalTensor_SolverWrapper(const CanonicalTensor<T>& orig):
   mTensor(orig)
{
}

//! Copy constructor.
template <class T>
CanonicalTensor_SolverWrapper<T>::CanonicalTensor_SolverWrapper(const CanonicalTensor_SolverWrapper<T>& orig):
   mTensor(orig.mTensor)
{
}

//! Directly add the mode matrices of two CanonicalTensor's.
template <class T>
CanonicalTensor_SolverWrapper<T> CanonicalTensor_SolverWrapper<T>::operator+(const CanonicalTensor_SolverWrapper<T>& right) const
{
   return CanonicalTensor_SolverWrapper<T>(mTensor.DirectIncrease(
            right.mTensor, static_cast<T>(1.0)));
}

//! Directly subtract the mode matrices of two CanonicalTensor's.
template <class T>
CanonicalTensor_SolverWrapper<T> CanonicalTensor_SolverWrapper<T>::operator-(const CanonicalTensor_SolverWrapper<T>& right) const
{
   return CanonicalTensor_SolverWrapper<T>(mTensor.DirectIncrease(
            right.mTensor, static_cast<T>(-1.0)));
}

//! Multiply all the elements of all mode matrices by a constant factor, in place.
template <class T>
CanonicalTensor_SolverWrapper<T>& CanonicalTensor_SolverWrapper<T>::operator*=(const T factor)
{
   std::vector<unsigned int> dims = this->mTensor.GetDims();
   unsigned int ndim =              this->mTensor.NDim();
   unsigned int rank =              this->mTensor.GetRank();
   T** modematrices  =              this->mTensor.GetModeMatrices();

   for(unsigned int idim=0;idim<ndim;idim++)
   {
      for(size_t i=0;i<rank*dims[idim];i++)
      {
         modematrices[idim][i] *= factor;
      }
   }
   return *this;
}

//! Multiply all the elements of all mode matrices by a constant factor (uses operator*=() ).
template <class T>
CanonicalTensor_SolverWrapper<T> CanonicalTensor_SolverWrapper<T>::operator*(const T factor) const
{
   CanonicalTensor_SolverWrapper<T> result = *this;

   result *= factor;
   return result;
}

//! Like operator*(), but factor is on the left.
template <class T>
CanonicalTensor_SolverWrapper<T> operator*(const T factor,
                                           const CanonicalTensor_SolverWrapper<T>& self)
{
   return self * factor;
}

//! Flip signs of all elements of all mode matrices.
template <class T>
CanonicalTensor_SolverWrapper<T> CanonicalTensor_SolverWrapper<T>::operator-() const
{
   return static_cast<T>(-1.0)*(*this);
}

//! Copy assignment.
template <class T>
CanonicalTensor_SolverWrapper<T>&
   CanonicalTensor_SolverWrapper<T>::operator=(const CanonicalTensor_SolverWrapper& other)
{
  this->mTensor=other.mTensor;
  return *this;
}

//! Sum the products of the corresponding mode-matrix elements of two CP tensors: \f$\sum_r \sum_i \sum_j a^{(i)}_{i_jr} b^{(i)}_{i_jr}\f$
template <class T>
T dot(const CanonicalTensor_SolverWrapper<T>& left
     ,const CanonicalTensor_SolverWrapper<T>& right)
{
   assert_same_shape(left.mTensor, right.mTensor);

   T sum=0.0;

   std::vector<unsigned int> dims = left.mTensor.GetDims();
   unsigned int ndim =              left.mTensor.NDim();
   unsigned int rank =              left.mTensor.GetRank();
   T**  left_modematrices =  left.mTensor.GetModeMatrices();
   T** right_modematrices = right.mTensor.GetModeMatrices();

   for(unsigned int idim=0;idim<ndim;idim++)
   {
      for(size_t i=0;i<rank*dims[idim];i++)
      {
         sum+= (left_modematrices[idim][i])*(right_modematrices[idim][i]);
      }
   }
   return sum;
}

//! `norm(x) = sqrt(dot(x,x)))`.
template <class T>
T norm(const CanonicalTensor_SolverWrapper<T>& x)
{
   return sqrt(dot(x,x));
}

//! True if `norm(x) <= acc` (x is an error vector whose elements are 0 when converged).
template <class T>
bool converged(const CanonicalTensor_SolverWrapper<T>& x,
               const T acc)
{
   return norm(x) <= acc;
}

//!
template<class T>
std::ostream& operator<<(std::ostream& os, const CanonicalTensor_SolverWrapper<T>& tensor)
{
   os << tensor.mTensor << std::endl;
   return os;
}
