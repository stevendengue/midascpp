/**
 ************************************************************************
 * 
 * @file                TGMatrix.h
 *
 * Created:             19.08.2015
 *
 * Author:              Sergio Losilla (sergio@chem.au.dk)
 *
 * \brief               TGMatrix: Simple wrapper for matrices for TensorGraph.
 * 
 * Detailed Description: Just matrices and products. Nothing special.
 *
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef TGMATRIX_H_INCLUDED
#define TGMATRIX_H_INCLUDED

#include<vector>
#include<memory>
#include<iostream>

template <class T>
class TGMatrix;

// Matrix multiplication
template <class T>
TGMatrix<T> matmul(const TGMatrix<T>& left,  const char left_mode,
                   const TGMatrix<T>& right, const char right_mode);

//template <class T>
//std::ostream& operator<<(std::ostream&, const TGMatrix<T>&);

template <class T>
class TGMatrix
{
   private:
      unsigned int mNRow;
      unsigned int mNCol;
      std::unique_ptr<const T[]> mMatrix;

   public:
      TGMatrix(const unsigned int aNRow, const unsigned int aNCol);
      TGMatrix(const unsigned int aNRow, const unsigned int aNCol, const T* aMatrix);

      unsigned int GetNRow() const {return mNRow;};
      unsigned int GetNCol() const {return mNCol;};
      const std::unique_ptr<const T[]>& GetMatrix() const{return mMatrix;};

      std::string Prettify() const;

      friend TGMatrix<T> matmul<>(const TGMatrix<T>& left,  const char left_mode,
                                  const TGMatrix<T>& right, const char right_mode);

};

#include"TGMatrix_Impl.h"

#endif /* TGMATRIX_H_INCLUDED */
