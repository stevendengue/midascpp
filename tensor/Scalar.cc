#ifndef DISABLE_PRECOMPILED_TEMPLATES
#include "Scalar.h"
#include "Scalar_Impl.h"

// define
#define INSTANTIATE_SCALAR(T) \
template class Scalar<T>;

// concrete instantiations
INSTANTIATE_SCALAR(float)
INSTANTIATE_SCALAR(double)
INSTANTIATE_SCALAR(std::complex<float>)
INSTANTIATE_SCALAR(std::complex<double>)

#undef INSTANTIATE_SCALAR

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
