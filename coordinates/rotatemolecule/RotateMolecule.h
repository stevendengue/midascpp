/*
************************************************************************
*  
* @file                RotateMolecule.h
*
* Created:             2016-09-23
*
* Author:              Diana Madsen (diana@chem.au.dk)
*
* Short Description:   Rotating the coordinates
*                      
*                      
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef ROTATEMOLECULE_H
#define ROTATEMOLECULE_H

#include "inc_gen/TypeDefs.h"
#include "mmv/MidasMatrix.h"
 
//! Rotate a molecule an angle around an axis.
void DoRotateMolecule(const ModSysCalcDef& arCalcDef, const System& arSystem);

#endif //ROTCOORD_H
