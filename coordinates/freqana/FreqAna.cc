/*
************************************************************************
*
* @file                FreqAna.cc
*
* Created:             21-04-2015
*
* Author:              Mads Boettger Hansen (mb.hansen@chem.au.dk)
*
* Short Description:   Frequency analysis by diagonalization of the
*                      Hessian matrix obtained using the 3-point
*                      central difference formula.
*
* Rev. Date            Comments:
*
* Detailed  Description:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

// std headers
#include<algorithm>
#include<iomanip>
#include<sstream>
#include<memory>  //for std::unique_ptr
#include<vector>

// midas headers
#include"input/Input.h"
#include"util/MidasStream.h"
#include"util/Error.h"
#include"mmv/MidasMatrix.h"
#include"lapack_interface/LapackInterface.h"
#include"pes/molecule/MoleculeInfo.h"
#include"nuclei/Nuclei.h"
#include"pes/singlepoint/SinglePoint.h"
#include"pes/singlepoint/AsyncSinglePointDriver.h"
#include"coordinates/freqana/FreqAna.h"
#include"inc_gen/Const.h"
#include"it_solver/EigenvalueContainer.h"
#include"mmv/DataCont.h"
#include "pes/PropertyInfo.h"
#include "mpi/FileToString.h"

/* *
* Constructor
**/
FreqAna::FreqAna(molecule::MoleculeInfo* const apMolInfo, const FreqAnaCalcDef* const apCalcDef)
   :  mpMolInfo(apMolInfo)
   ,  mpCalcDef(apCalcDef)
   ,  mInitCoords(apMolInfo->NormalCoord())
{
}

/* *
* Constructor without calcdef
**/
FreqAna::FreqAna(molecule::MoleculeInfo* const apMolInfo)
   :  mpMolInfo(apMolInfo)
   ,  mpCalcDef(nullptr)
   ,  mInitCoords(apMolInfo->NormalCoord())
{
   MidasWarning("FreqAna initialized without calcdef");
}

/* *
* Regular frequency analysis
**/
void FreqAna::Preparations() const
{
   std::ios_base::fmtflags Mout_old_flags = Mout.flags(); //For later restoring them.

   Out72Char(Mout,'-','-','-');
   OneArgOut72(Mout," Settings for Frequency Analysis: ",' ');
   Out72Char(Mout,'-','-','-');
   Mout << std::boolalpha;
   Mout << "General settings:" << std::endl;
   Mout << "   Displacement factor:           " << mpCalcDef->GetDispFactor() << std::endl;
   Mout << "   Use non-symmetric Hessian:     " << mpCalcDef->GetNonSymmetricHessian() << std::endl;
   Mout << "   Compare sym./non-sym. Hessian: " << mpCalcDef->GetCompareSymAndNonSym() << std::endl;
   Mout << "   SinglePoint name:              " << mpCalcDef->GetSinglePointName() << std::endl;
   Mout << "   #Nodes:                        " << mpCalcDef->GetNumNodes() << std::endl;
   Mout << "   #Processes per node:           " << mpCalcDef->GetProcPerNode() << std::endl;
   Mout << "   Dump interval:                 " << mpCalcDef->GetDumpInterval() << std::endl;
   Mout << "   Rotation threshold:            " << mpCalcDef->GetRotationThr() << std::endl;
   Mout << "   MoleculeInfo output:           " << mpCalcDef->GetMolInfoOutName() << std::endl;
   Mout << std::endl;
   Mout << "Targeting settings (only relevant if 'Using targeting' is 'true'):" << std::endl;
   Mout << "   Using targeting:               " << mpCalcDef->GetTargeting() << std::endl;
   Mout << "   Iter. eq. energy threshold:    " << mpCalcDef->GetItEqEnerThr() << std::endl;
   Mout << "   Iter. eq. residual threshold:  " << mpCalcDef->GetItEqResThr() << std::endl;
   Mout << "   Iter. eq. break dimension:     " << mpCalcDef->GetItEqBreakDim() << std::endl;
   Mout << "   Iter. eq. max. iterations:     " << mpCalcDef->GetItEqMaxIt() << std::endl;
   Mout << "   Overlap sum min:               " << mpCalcDef->GetOverlapSumMin() << std::endl;
   Mout << "   Overlap N_max:                 " << mpCalcDef->GetOverlapNMax() << std::endl;
   Mout << "   Targets already mass-weighted: " << mpCalcDef->GetAlreadyMassWeighted() << std::endl;
   Mout << std::endl;
   Out72Char(Mout,'-','-','-');
   Mout << std::endl;
   Mout << std::endl;

   //Restore formatting:
   Mout.flags(Mout_old_flags);
}

void FreqAna::DoFreqAna()
{
   //MBH-NB! Check if hessian is already there?
   Mout << "Frequency analysis; calculating Hessian." << std::endl;
   MidasMatrix hessian = CalcHessian();

   //Eigenvalue structures (declared here so both can be used for comparison):
   Eigenvalue_struct<Nb> eig_sol;
   Eigenvalue_ext_struct<Nb> eig_sol_ext;

   //Symmetrize Hessian or not?
   //Use Eigenvalue_struct or Eigenvalue_ext_struct accordingly.
   //If CompareSymAndNonSym == true, both get calculated, however only one of them
   //gets written to MoleculeInfo depending on whether NonSymmetricHessian == true/false.
   if(mpCalcDef->GetNonSymmetricHessian() || mpCalcDef->GetCompareSymAndNonSym())
   {
      Mout << "Frequency analysis; diagonalizing non-symmetrized Hessian." << std::endl;

      //Diagonalize as general matrix:
      eig_sol_ext = GEEV(hessian);

      //Sort eigenvalues according to real part (true: ascending), then write to MolInfo:
      sort_eigenvalues(eig_sol_ext, true);
   }

   if(!mpCalcDef->GetNonSymmetricHessian() || mpCalcDef->GetCompareSymAndNonSym())
   {
      Mout << "Frequency analysis; diagonalizing symmetrized Hessian." << std::endl;

      //Symmetrize, then diagonalize as symmetric matrix:
      hessian.Symmetrize();
      eig_sol = SYEVD(hessian);

      //Sort eigenvalues according to real part (true: ascending), then write to MolInfo:
      sort_eigenvalues(eig_sol, true);
   }

   //Do comparison if asked to:
   if(mpCalcDef->GetCompareSymAndNonSym())
   {
      Mout << "Frequency analysis; comparing non-sym./sym. Hessian eigendata.\n" << std::endl;
      CompareEigenData(eig_sol, eig_sol_ext);
   }

   //Write data to MoleculeInfo, then to file:
   if(mpCalcDef->GetNonSymmetricHessian())
   {
      Mout << "Frequency analysis; writing results from non-sym. diag. to MoleculeInfo file." << std::endl;
      WriteEigenDataToMolInfo(eig_sol_ext);
   }
   else
   {
      Mout << "Frequency analysis; writing results from sym. diag. to MoleculeInfo file." << std::endl;
      WriteEigenDataToMolInfo(eig_sol);
   }

   WriteMoleculeFile(mpCalcDef->GetMolInfoOutName(), "MIDAS");
   WriteMoleculeFile(mpCalcDef->GetMolInfoOutName()+".molden", "MOLDEN");
   WriteMoleculeFile(mpCalcDef->GetMolInfoOutName()+".minfo", "SINDO");

   //If we did comparison, then also output a seperate MoleculeInfo for both the sym.
   //and non-sym. diagonalization.
   if(mpCalcDef->GetCompareSymAndNonSym())
   {
      Mout << "Frequency analysis; did comparison of sym. and non-sym. diagonalization\n"
           << "so writing results of both to the MoleculeInfo files\n"
           << "   MoleculeInfo_FreqAna_sym.mmol\n"
           << "   MoleculeInfo_FreqAna_nonsym.mmol\n"
           << std::endl;
      //Could check which one is already the one stored, but won't safe us that much effort:
      //Non-sym:
      WriteEigenDataToMolInfo(eig_sol_ext);
      WriteMoleculeFile("MoleculeInfo_FreqAna_nonsym.mmol", "MIDAS");
      //Sym:
      WriteEigenDataToMolInfo(eig_sol);
      WriteMoleculeFile("MoleculeInfo_FreqAna_sym.mmol", "MIDAS");
   }

   Mout << "Frequency analysis; end."  << std::endl;
   Mout << std::endl;
   Mout << std::endl;
}


/* *
* Frequency analysis with given Hessian matrix
**/
void FreqAna::DoFreqAna(const MidasMatrix& arHessian, const std::string& arOutFileName)
{
   //Check if hessian is already there?
   MidasAssert(arHessian.Nrows()==arHessian.Ncols(),"Hessian Matrix not square");
   MidasAssert(arHessian.Nrows()==mpMolInfo->GetNoOfVibs(),"Dimensions of Hessian does not fit to number of vibrations");
   Eigenvalue_struct<Nb> eig_sol = SYEVD(arHessian);
   WriteEigenDataToMolInfo(eig_sol);
   mpMolInfo->WriteMoleculeFile(molecule::MoleculeFileInfo(arOutFileName+".mol", "MIDAS"));
   mpMolInfo->WriteMoleculeFile(molecule::MoleculeFileInfo(arOutFileName+".input", "MOLDEN"));
   mpMolInfo->WriteMoleculeFile(molecule::MoleculeFileInfo(arOutFileName+".minfo", "SINDO"));
}

/* *
* Targeting
**/
std::vector<DataCont> FreqAna::TargetsFromMolInfo() const
{
   //Just a const ref:
   const MidasMatrix& targets = mInitCoords;

   //Put into vector of data containers:
   std::vector<DataCont> vec_dc;
   for(In i=I_0; i<targets.Nrows(); ++i)
   {
      //Extract from MoleculeInfo:
      MidasVector vec_target(targets.Ncols());
      targets.GetRow(vec_target, i);

      //Mass-weight if not already done:
      //(As of now the it.eq.solver needs the targets to be mass-weighted on input,
      //i.e. <target|target> = 1, as opposed to <target|M|target> = 1 which is the case
      //for the modes stored in the MoleculeInfo. M: mass matrix.)
      if(!mpCalcDef->GetAlreadyMassWeighted())
      {
         for(In j=I_0; j<vec_target.Size(); ++j)
         {
            vec_target[j] *= sqrt(mpMolInfo->GetNuclMassi(j/I_3));
         }
      }

      //Ensure normalization:
      Nb norm = vec_target.Norm();
      if(norm != C_0)   //Normalize, make DataCont and add the latter to std::vector:
      {
         vec_target.Scale(C_1/norm);
         DataCont dc(vec_target);
         vec_dc.push_back(dc);
      }
      else  //Warning (and discard) in case user provided a zero vector as target:
      {
         MidasWarning("Target "
               +std::to_string(i)
               +" neglected; zero norm. Consider removing it from input.");
      }
   }
   return vec_dc;
}


/* *
* Utility functions
**/
MidasMatrix FreqAna::CalcHessian() const
{
   MidasMatrix sigma = CalcSigma();
   return mInitCoords * sigma; //Project with basis vecs.
}

MidasMatrix FreqAna::CalcSigma() const
{
   return HessianTransformation(mInitCoords);
}

MidasMatrix FreqAna::HessianTransformation(const MidasMatrix& arB) const
{
   MidasAssert(arB.Ncols() == VectorDimension(), "Size mismatch in HessianTransformation.");

   //Containers to be used/filled in in the following:
   MidasMatrix sigma(VectorDimension(), arB.Nrows());
   MidasVector disp_vec(VectorDimension());
   const std::vector<Nuclei> config_ref = mpMolInfo->GetCoord();
   MidasVector disp_factor_eff(sigma.Ncols());

   //List of SP pointers to give to AsyncSP class (parallel):
   std::list<SinglePoint> sp_list;

   auto setup_directory = gMainDir + "/setup";
   auto target_directory = setup_directory + "/";
   std::string midasifc_propinfo_path = target_directory + "midasifc.propinfo";
   std::string midasifc_propinfo = midas::mpi::FileToString(midasifc_propinfo_path);

   midas::pes::PropertyInfo property_info;
   property_info.Update(false, midasifc_propinfo, mpMolInfo->IsLinear());

   //Make displacements, set up the SP calcs list:
   for(In j=0, count=0; j<sigma.Ncols(); ++j)
   {
      //Configs:
      arB.GetRow(disp_vec, j);
      disp_factor_eff[j] = mpCalcDef->GetDispFactor() / disp_vec.Norm(); //Accounts for norm of disp_vec.
      const std::vector<Nuclei> config_plus = DispConfigWithVector(config_ref, disp_vec, disp_factor_eff[j]);
      const std::vector<Nuclei> config_minus = DispConfigWithVector(config_ref, disp_vec, -disp_factor_eff[j]);

      //SPs:
      //One SP for both configs;
      //    +: has even   'count' = 2j
      //    -: has uneven 'count' = 2j+1
      //MIDASERROR("PROPERTYINFO NOT READ IN CORRECTLY! FIX IT!");
      sp_list.emplace_back(SinglePointCalc(mpCalcDef->GetSinglePointName()), property_info, config_plus, count++);
      sp_list.emplace_back(SinglePointCalc(mpCalcDef->GetSinglePointName()), property_info, config_minus, count++);
   }

   // SP list having been set up, run the calcs:
   AsyncSinglePointDriver sp_driver(0);
   sp_driver.EvaluateList(sp_list);

   //Calculate elements of Sigma:
   auto it=sp_list.begin();
   for(In j=0; j<sigma.Ncols(); ++j)
   {
      const MidasVector gradient_plus  = (*(it++)).Properties().GetFirstDerivatives(0);
      const MidasVector gradient_minus = (*(it++)).Properties().GetFirstDerivatives(0);

      MidasAssert(gradient_plus.Size() != I_0 && gradient_minus.Size() != I_0, "FreqAna::HessianTransformation: Gradient vectors are empty. Were they calculated/read?");

      for(In i=0; i<sigma.Nrows(); ++i)
      {
         sigma[i][j] = (gradient_plus[i] - gradient_minus[i])/(2*disp_factor_eff[j]);
      }
   }

   return sigma;
}

std::vector<Nuclei> FreqAna::DispConfigWithVector(std::vector<Nuclei> aConfig, const MidasVector& arVector, Nb aDispFactor) const
{
   MidasAssert(3*aConfig.size() == arVector.Size(), "Size of displacement vector != 3*number of atoms, in FreqAna::DispConfigWithVector.");
   for(int n=0; n<aConfig.size(); ++n) //Loop over nuclei.
   {
      //Chop a 3D vec corresponding to one of the nuclei from the arVector.
      Vector3D nuclear_shift(
         arVector[3*n+0],
         arVector[3*n+1],
         arVector[3*n+2]
      );
      nuclear_shift = aDispFactor * nuclear_shift; //Scale with (effective) disp. factor.
      aConfig[n].Shift(nuclear_shift);   //Shift nucleus.
   }
   return aConfig;
}

std::vector<std::vector<Nb> > FreqAna::TransEigVecsWithBasis(const Eigenvalue_struct<Nb>& arEigStruct) const
{
   Mout << " arEigStruct._n " << arEigStruct._n << endl;
   Mout << " mInitCoords.Nrows() " << mInitCoords.Nrows() << endl;
   MidasAssert(arEigStruct._n == mInitCoords.Nrows(), "Size of eigenvectors != number of coordinate vectors, in FreqAna::TransEigVecsWithBasis.");
   std::vector<std::vector<Nb> > eig_vecs(arEigStruct._num_eigval);
   for(In i=0; i<eig_vecs.size(); ++i) //Loop over (transformed) eigvecs.
   {
      eig_vecs[i].resize(VectorDimension());
      for(In j=0; j<eig_vecs[i].size(); ++j)
      {
         eig_vecs[i][j] = 0.;
         for(In k=0; k<arEigStruct._n; ++k)
         {
            eig_vecs[i][j] += arEigStruct._eigenvectors[i][k] * mInitCoords[k][j];
         }
      }
   }
   return eig_vecs;
}

std::vector<std::vector<Nb> > FreqAna::TransEigVecsWithBasis(const Eigenvalue_ext_struct<Nb>& arEigStruct) const
{
   MidasAssert(arEigStruct.n_ == mInitCoords.Nrows(), "Size of eigenvectors != number of coordinate vectors, in FreqAna::TransEigVecsWithBasis.");
   std::vector<std::vector<Nb> > eig_vecs(arEigStruct.num_eigval_);
   for(In i=0; i<eig_vecs.size(); ++i) //Loop over (transformed) eigvecs.
   {
      eig_vecs[i].resize(VectorDimension());
      for(In j=0; j<eig_vecs[i].size(); ++j)
      {
         eig_vecs[i][j] = 0.;
         for(In k=0; k<arEigStruct.n_; ++k)
         {
            eig_vecs[i][j] += arEigStruct.rhs_eigenvectors_[i][k] * mInitCoords[k][j];
         }
      }
   }
   return eig_vecs;
}

void FreqAna::CompareEigenData(const Eigenvalue_struct<Nb>& arEigSol, const Eigenvalue_ext_struct<Nb>& arEigSolExt, std::ostream& arOS) const
{
   MidasAssert(arEigSol._num_eigval == arEigSolExt.num_eigval_, "Unequal number of eig.vals.");
   //Some formatting:
   std::ios_base::fmtflags arOS_old_flags = arOS.flags(); //For later restoring them.
   In w0 = I_5;
   In w1 = arOS.precision() + I_10;
   In w2 = I_8;
   In w3 = I_25;
   arOS  << std::boolalpha;

   arOS  << "Comparison of eigenvalues/vectors from non-symmetrized and symmetrized\n"
         << "mass-weighted Hessian diagonalization. ONLY comparison of REAL parts.\n"
         << "All numbers in atomic units.\n"
         << "s:  symmetric\n"
         << "ns: non-symmetric\n"
         << "diff_val              = val_s - Re(val_ns)\n"
         << "rel_diff_val          = diff/Re(val_ns)\n"
         << "norm_ip               = <vec_s|Re(vec_ns)> / (||vec_s|| * ||Re(vec_ns)||)\n"
         << "proximity_to_parallel = |1 - |norm_ip||\n"
         << std::endl;
   //Eigval. comparison header:
   arOS  << std::left;
   arOS  << "Frequency analysis; sym/non-sym eigenvalue comparison:\n";
   arOS  << ' '   << std::setw(w0)  << "i";
   arOS  << ' '   << std::setw(w1)  << "val_s";
   arOS  << ' '   << std::setw(w1)  << "Re(val_ns)";
   arOS  << ' '   << std::setw(w1)  << "Im(val_ns)";
   arOS  << ' '   << std::setw(w1)  << "diff_val";
   arOS  << ' '   << std::setw(w1)  << "rel_diff_val";
   arOS  << std::endl;

   //Eigval. statistics:
   Nb eigval_dev_mean      = C_0;
   Nb eigval_dev_rms       = C_0;
   Nb eigval_rel_dev_mean  = C_0;
   Nb eigval_rel_dev_rms   = C_0;
   Nb eigval_max_dev       = C_0;
   Nb eigval_max_rel_dev   = C_0;
   Nb eigval_min_dev       = C_NB_MAX;
   Nb eigval_min_rel_dev   = C_NB_MAX;
   arOS  << std::right;
   for(In i=I_0; i<arEigSol._num_eigval; ++i)
   {
      Nb diff     = arEigSol._eigenvalues[i] - arEigSolExt.re_eigenvalues_[i];
      Nb rel_diff = diff/arEigSolExt.re_eigenvalues_[i];
      eigval_dev_mean      += fabs(diff);
      eigval_dev_rms       += diff*diff;
      eigval_rel_dev_mean  += fabs(rel_diff);
      eigval_rel_dev_rms   += rel_diff*rel_diff;
      eigval_max_dev       = std::max(eigval_max_dev, fabs(diff));
      eigval_max_rel_dev   = std::max(eigval_max_rel_dev, fabs(rel_diff));
      eigval_min_dev       = std::min(eigval_min_dev, fabs(diff));
      eigval_min_rel_dev   = std::min(eigval_min_rel_dev, fabs(rel_diff));
      arOS  << ' '   << std::setw(w0)  << i;
      arOS  << ' '   << std::setw(w1)  << arEigSol._eigenvalues[i];
      arOS  << ' '   << std::setw(w1)  << arEigSolExt.re_eigenvalues_[i];
      arOS  << ' '   << std::setw(w1)  << arEigSolExt.im_eigenvalues_[i];
      arOS  << ' '   << std::setw(w1)  << diff;
      arOS  << ' '   << std::setw(w1)  << rel_diff;
      arOS  << std::endl;
   }
   arOS  << std::endl;

   //Eigvec. comparison header:
   arOS  << "Frequency analysis; sym/non-sym eigenvector comparison:\n";
   arOS  << std::left;
   arOS  << ' '   << std::setw(w0)  << "i";
   arOS  << ' '   << std::setw(w1)  << "norm_ip(rhs)";
   arOS  << ' '   << std::setw(w1)  << "prox_to_par(rhs)";
   arOS  << ' '   << std::setw(w1)  << "norm_ip(lhs)";
   arOS  << ' '   << std::setw(w1)  << "prox_to_par(lhs)";
   arOS  << ' '   << std::setw(w2)  << "complex";
   arOS  << std::endl;

   //Eigvec. statistics:
   Nb eigvec_norm_ip_rhs_mean = C_0;
   Nb eigvec_norm_ip_lhs_mean = C_0;
   Nb eigvec_norm_ip_rhs_rms  = C_0;
   Nb eigvec_norm_ip_lhs_rms  = C_0;
   Nb eigvec_norm_ip_rhs_max  = C_0;
   Nb eigvec_norm_ip_lhs_max  = C_0;
   Nb eigvec_norm_ip_rhs_min  = C_NB_MAX;
   Nb eigvec_norm_ip_lhs_min  = C_NB_MAX;
   arOS  << std::right;
   for(In i=I_0; i<arEigSol._num_eigval; ++i)
   {
      bool is_complex = (arEigSolExt.im_eigenvalues_[i] != C_0);
      MidasVector sym(arEigSol._n, arEigSol._eigenvectors[i]);
      MidasVector nsymr(arEigSolExt.n_, arEigSolExt.rhs_eigenvectors_[i]);
      MidasVector nsyml(arEigSolExt.n_, arEigSolExt.lhs_eigenvectors_[i]);
      Nb norm_ip_rhs = Dot(sym, nsymr)/(sym.Norm() * nsymr.Norm());
      Nb norm_ip_lhs = Dot(sym, nsyml)/(sym.Norm() * nsyml.Norm());
      Nb prox_rhs    = fabs(C_1 - fabs(norm_ip_rhs));
      Nb prox_lhs    = fabs(C_1 - fabs(norm_ip_lhs));
      eigvec_norm_ip_rhs_mean += fabs(norm_ip_rhs);
      eigvec_norm_ip_lhs_mean += fabs(norm_ip_lhs);
      eigvec_norm_ip_rhs_rms  += norm_ip_rhs*norm_ip_rhs;
      eigvec_norm_ip_lhs_rms  += norm_ip_lhs*norm_ip_lhs;
      eigvec_norm_ip_rhs_max  = std::max(eigvec_norm_ip_rhs_max, fabs(norm_ip_rhs));
      eigvec_norm_ip_lhs_max  = std::max(eigvec_norm_ip_lhs_max, fabs(norm_ip_lhs));
      eigvec_norm_ip_rhs_min  = std::min(eigvec_norm_ip_rhs_min, fabs(norm_ip_rhs));
      eigvec_norm_ip_lhs_min  = std::min(eigvec_norm_ip_lhs_min, fabs(norm_ip_lhs));
      arOS  << ' '   << std::setw(w0)  << i;
      arOS  << ' '   << std::setw(w1)  << norm_ip_rhs;
      arOS  << ' '   << std::setw(w1)  << prox_rhs;
      arOS  << ' '   << std::setw(w1)  << norm_ip_lhs;
      arOS  << ' '   << std::setw(w1)  << prox_lhs;
      arOS  << ' '   << std::setw(w2)  << is_complex;
      arOS  << std::endl;

      if(is_complex) //Then repeat with sym-vec. but same (real) part of non-sym. vec.
      {
         ++i;
         MidasVector sym(arEigSol._n, arEigSol._eigenvectors[i]);
         Nb norm_ip_rhs = Dot(sym, nsymr)/(sym.Norm() * nsymr.Norm());
         Nb norm_ip_lhs = Dot(sym, nsyml)/(sym.Norm() * nsyml.Norm());
         Nb prox_rhs    = fabs(C_1 - fabs(norm_ip_rhs));
         Nb prox_lhs    = fabs(C_1 - fabs(norm_ip_lhs));
         eigvec_norm_ip_rhs_mean += fabs(norm_ip_rhs);
         eigvec_norm_ip_lhs_mean += fabs(norm_ip_lhs);
         eigvec_norm_ip_rhs_rms  += norm_ip_rhs*norm_ip_rhs;
         eigvec_norm_ip_lhs_rms  += norm_ip_lhs*norm_ip_lhs;
         eigvec_norm_ip_rhs_max  = std::max(eigvec_norm_ip_rhs_max, fabs(norm_ip_rhs));
         eigvec_norm_ip_lhs_max  = std::max(eigvec_norm_ip_lhs_max, fabs(norm_ip_lhs));
         eigvec_norm_ip_rhs_min  = std::min(eigvec_norm_ip_rhs_min, fabs(norm_ip_rhs));
         eigvec_norm_ip_lhs_min  = std::min(eigvec_norm_ip_lhs_min, fabs(norm_ip_lhs));
         arOS  << ' '   << std::setw(w0)  << i;
         arOS  << ' '   << std::setw(w1)  << norm_ip_rhs;
         arOS  << ' '   << std::setw(w1)  << prox_rhs;
         arOS  << ' '   << std::setw(w1)  << norm_ip_lhs;
         arOS  << ' '   << std::setw(w1)  << prox_lhs;
         arOS  << ' '   << std::setw(w2)  << is_complex;
         arOS  << std::endl;
      }
   }

   //Statistics:
   arOS  << std::endl;
   arOS  << "Frequency analysis; sym/non-sym comparison statistics:\n";
   arOS  << std::left << std::setw(w3) << "Number of eigvals/vecs"   << " = " << std::right << std::setw(w1) << arEigSol._num_eigval << '\n';
   arOS  << "Eigenvalues:\n";
   eigval_dev_mean      /= arEigSol._num_eigval;
   eigval_dev_rms        = sqrt(eigval_dev_rms/arEigSol._num_eigval);
   eigval_rel_dev_mean  /= arEigSol._num_eigval;
   eigval_rel_dev_rms    = sqrt(eigval_rel_dev_rms/arEigSol._num_eigval);
   arOS  << std::left << std::setw(w3) << "|diff|_mean"     << " = " << std::right  << std::setw(w1)  << eigval_dev_mean      << '\n';
   arOS  << std::left << std::setw(w3) << "|diff|_max"      << " = " << std::right  << std::setw(w1)  << eigval_max_dev       << '\n';
   arOS  << std::left << std::setw(w3) << "|diff|_min"      << " = " << std::right  << std::setw(w1)  << eigval_min_dev       << '\n';
   arOS  << std::left << std::setw(w3) << "diff_rms"        << " = " << std::right  << std::setw(w1)  << eigval_dev_rms       << '\n';
   arOS  << std::left << std::setw(w3) << "|rel_diff|_mean" << " = " << std::right  << std::setw(w1)  << eigval_rel_dev_mean  << '\n';
   arOS  << std::left << std::setw(w3) << "|rel_diff|_max"  << " = " << std::right  << std::setw(w1)  << eigval_max_rel_dev   << '\n';
   arOS  << std::left << std::setw(w3) << "|rel_diff|_min"  << " = " << std::right  << std::setw(w1)  << eigval_min_rel_dev   << '\n';
   arOS  << std::left << std::setw(w3) << "rel_diff_rms"    << " = " << std::right  << std::setw(w1)  << eigval_rel_dev_rms   << '\n';
   arOS  << "Eigenvectors:\n";
   eigvec_norm_ip_rhs_mean /= arEigSol._num_eigval;
   eigvec_norm_ip_lhs_mean /= arEigSol._num_eigval;
   eigvec_norm_ip_rhs_rms   = sqrt(eigvec_norm_ip_rhs_rms/arEigSol._num_eigval);
   eigvec_norm_ip_lhs_rms   = sqrt(eigvec_norm_ip_lhs_rms/arEigSol._num_eigval);
   arOS  << "Right-hand-side:\n";
   arOS  << std::left << std::setw(w3) << "|norm_ip(rhs)|_mean"      << " = " << std::right  << std::setw(w1)  << eigvec_norm_ip_rhs_mean  << '\n';
   arOS  << std::left << std::setw(w3) << "|norm_ip(rhs)|_max"       << " = " << std::right  << std::setw(w1)  << eigvec_norm_ip_rhs_max   << '\n';
   arOS  << std::left << std::setw(w3) << "|norm_ip(rhs)|_min"       << " = " << std::right  << std::setw(w1)  << eigvec_norm_ip_rhs_min   << '\n';
   arOS  << std::left << std::setw(w3) << "norm_ip(rhs)_rms"         << " = " << std::right  << std::setw(w1)  << eigvec_norm_ip_rhs_rms   << '\n';
   arOS  << std::left << std::setw(w3) << "|1-|norm_ip(rhs)|_mean|"  << " = " << std::right  << std::setw(w1)  << fabs(C_1 - fabs(eigvec_norm_ip_rhs_mean))  << '\n';
   arOS  << std::left << std::setw(w3) << "|1-|norm_ip(rhs)|_max|"   << " = " << std::right  << std::setw(w1)  << fabs(C_1 - eigvec_norm_ip_rhs_max)         << '\n';
   arOS  << std::left << std::setw(w3) << "|1-|norm_ip(rhs)|_min|"   << " = " << std::right  << std::setw(w1)  << fabs(C_1 - eigvec_norm_ip_rhs_min)         << '\n';
   arOS  << std::left << std::setw(w3) << "|1-norm_ip(rhs)_rms|"     << " = " << std::right  << std::setw(w1)  << fabs(C_1 - eigvec_norm_ip_rhs_rms)         << '\n';
   arOS  << "Left-hand-side:\n";
   arOS  << std::left << std::setw(w3) << "|norm_ip(lhs)|_mean"      << " = " << std::right  << std::setw(w1)  << eigvec_norm_ip_lhs_mean  << '\n';
   arOS  << std::left << std::setw(w3) << "|norm_ip(lhs)|_max"       << " = " << std::right  << std::setw(w1)  << eigvec_norm_ip_lhs_max   << '\n';
   arOS  << std::left << std::setw(w3) << "|norm_ip(lhs)|_min"       << " = " << std::right  << std::setw(w1)  << eigvec_norm_ip_lhs_min   << '\n';
   arOS  << std::left << std::setw(w3) << "norm_ip(lhs)_rms"         << " = " << std::right  << std::setw(w1)  << eigvec_norm_ip_lhs_rms   << '\n';
   arOS  << std::left << std::setw(w3) << "|1-|norm_ip(lhs)_mean||"  << " = " << std::right  << std::setw(w1)  << fabs(C_1 - fabs(eigvec_norm_ip_lhs_mean))  << '\n';
   arOS  << std::left << std::setw(w3) << "|1-|norm_ip(lhs)|_max|"   << " = " << std::right  << std::setw(w1)  << fabs(C_1 - eigvec_norm_ip_lhs_max)        << '\n';
   arOS  << std::left << std::setw(w3) << "|1-|norm_ip(lhs)|_min|"   << " = " << std::right  << std::setw(w1)  << fabs(C_1 - eigvec_norm_ip_lhs_min)        << '\n';
   arOS  << std::left << std::setw(w3) << "|1-norm_ip(lhs)_rms|"     << " = " << std::right  << std::setw(w1)  << fabs(C_1 - eigvec_norm_ip_lhs_rms)         << '\n';

   arOS  << std::endl;
   arOS  << std::endl;

   //Restore formatting:
   arOS.flags(arOS_old_flags);
}

void FreqAna::WriteEigenDataToMolInfo(const Eigenvalue_struct<Nb>& arEigStruct, bool aTransWithBasis, bool aInvertedMassWeight)
{
   //Frequencies
   std::vector<Nb> frequencies(arEigStruct._num_eigval); //Extract from Eigenvalue_struct.
   for(In i=0; i<frequencies.size(); ++i)
   {
      if(arEigStruct._eigenvalues[i] < 0) //Imaginary freqs are given negative values.
      {
         frequencies[i] = -sqrt(-arEigStruct._eigenvalues[i]/C_FAMU);
      }
      else
      {
         frequencies[i] =  sqrt( arEigStruct._eigenvalues[i]/C_FAMU);
      } //Freqs are in atomic units.
   }

   FrequencyData frequency_data(FrequencyType::Key());
   frequency_data.Initialize(origin_type::CALCULATED, std::move(frequencies), "AU");
   mpMolInfo->ReInitFrequencies(frequency_data);

   //Eigenvectors
   //If we've been doing targeting (it.eq.sol., then the eigen vectors have already been
   //transformed with the basis vectors by the solver).
   std::vector<std::vector<Nb> > eig_vecs;
   if(aTransWithBasis)
   {
      eig_vecs = TransEigVecsWithBasis(arEigStruct);
   }
   else //Just put the eigenvectors into a vector<vector<Nb>>.
   {
      MidasAssert(arEigStruct._n == VectorDimension(), "Wrong dimension of eig.vecs. from targeting procedure.");
      for(In i=I_0; i<arEigStruct._num_eigval; ++i)
      {
         const Nb* const it_begin = arEigStruct._eigenvectors[i];
         const Nb* const it_end   = it_begin + arEigStruct._n;
         eig_vecs.emplace_back(it_begin, it_end);
      }
   }

   if(aInvertedMassWeight) //Necessary e.g. for solutions coming from it.eq.solver.
   {
      InvertedMassWeight(eig_vecs);
   }

   VibrationalCoordinateData vib_coord_data(VibrationalCoordinateType::Key());
   vib_coord_data.Initialize(origin_type::CALCULATED, std::move(eig_vecs), "AU", "NORMALCOORD");
   mpMolInfo->ReInitVibrationalCoordinates(vib_coord_data);
}

void FreqAna::WriteEigenDataToMolInfo(const Eigenvalue_ext_struct<Nb>& arEigStruct, bool aTransWithBasis, bool aInvertedMassWeight)
{
   //Frequencies
   std::vector<Nb> frequencies(arEigStruct.num_eigval_); //Extract from Eigenvalue_ext_struct.
   bool found_complex_eigval = false;
   for(In i=0; i<frequencies.size(); ++i)
   {
      if(arEigStruct.re_eigenvalues_[i] < 0) //Imaginary freqs are given negative values.
      {
         frequencies[i] = -sqrt(-arEigStruct.re_eigenvalues_[i]/C_FAMU);
      }
      else
      {
         frequencies[i] =  sqrt( arEigStruct.re_eigenvalues_[i]/C_FAMU);
      } //Freqs are in atomic units.

      //Check for imaginary part:
      if(arEigStruct.im_eigenvalues_[i] != C_0)
      {
         //Register that we found complex pair for printing warning at the end:
         found_complex_eigval = true;
         //std::stringstream is for formatting:
         std::stringstream str_freq;
         std::stringstream str_re;
         std::stringstream str_im;
         str_freq << std::scientific   << frequencies[i]*C_AUTKAYS;
         str_re   << std::scientific   << arEigStruct.re_eigenvalues_[i]/C_FAMU;
         str_im   << std::scientific   << arEigStruct.im_eigenvalues_[i]/C_FAMU;
         MidasWarning("Hessian eig.val. " + std::to_string(i)
               + " (~ freq. "
               + str_freq.str()
               + " cm-1) is complex: ("
               + str_re.str()
               + " + i*("
               + str_im.str()
               + ")) a.u.");
      }
   }
   if(found_complex_eigval)
   {
      MidasWarning("Some of the Hessian eig.vals. had non-zero imag. parts.");
      MidasWarning("For eig.vals.: only real parts written to MoleculeInfo.");
      MidasWarning("For eig.vecs.: real/imag. parts written in two consecutive vectors.");
   }

   FrequencyData frequency_data(FrequencyType::Key());
   frequency_data.Initialize(origin_type::CALCULATED, std::move(frequencies), "AU");
   mpMolInfo->ReInitFrequencies(frequency_data);

   //Eigenvectors
   //If we've been doing targeting (it.eq.sol., then the eigen vectors have already been
   //transformed with the basis vectors by the solver).
   std::vector<std::vector<Nb> > eig_vecs;
   if(aTransWithBasis)
   {
      eig_vecs = TransEigVecsWithBasis(arEigStruct);
   }
   else //Just put the eigenvectors into a vector<vector<Nb>> (Right-hand-side only):
   {
      MidasAssert(arEigStruct.n_ == VectorDimension(), "Wrong dimension of eig.vecs. from targeting procedure.");
      for(In i=I_0; i<arEigStruct.num_eigval_; ++i)
      {
         const Nb* const it_begin = arEigStruct.rhs_eigenvectors_[i];
         const Nb* const it_end   = it_begin + arEigStruct.n_;
         eig_vecs.emplace_back(it_begin, it_end);
      }
   }

   if(aInvertedMassWeight) //Necessary e.g. for solutions coming from it.eq.solver.
   {
      InvertedMassWeight(eig_vecs);
   }

   VibrationalCoordinateData vib_coord_data(VibrationalCoordinateType::Key());
   vib_coord_data.Initialize(origin_type::CALCULATED, std::move(eig_vecs), "AU", "NORMALCOORD");
   mpMolInfo->ReInitVibrationalCoordinates(vib_coord_data);
}

void FreqAna::WriteEigenDataToMolInfo(const EigenvalueContainer<MidasVector, MidasVector>& arValCont, std::vector<DataCont>& arVecCont, bool aTransWithBasis, bool aInvertedMassWeight)
{
   //Put it into an Eigenvalue_ext_struct then write it using the function for that.
   //Creates (slightly unnecessary) temporary object, but less error-prone.
   MidasAssert(arValCont.Re().Size() == arValCont.Im().Size(), "Unequal numbers of re/im. eig.vals.");
   MidasAssert(arValCont.Re().Size() == arVecCont.size(), "Unequal numbers of eig.vals./vecs.");

   //Okay, load it all into Eigenvalue_ext_struct...
   //Seems very error-prone this way, so it would be nicer with some setting functions
   //for the struct (or a similar class!), but we'll do with this for now.
   //This resembles the way it's done in lapack_interface/GEEV.h.
   Eigenvalue_ext_struct<Nb> eig_struct;  //Initializes all pointers to nullptr.
   eig_struct.info_              = I_0;   //info_ is probably ret. stat., 0: succ. Not used here anyway.
   eig_struct.num_eigval_        = arValCont.Re().Size();
   eig_struct.lhs_eigenvectors_  = nullptr;  //Only working with rhs.
   if(eig_struct.num_eigval_ > I_0)
   {
      //Eigenvalues:
      eig_struct.re_eigenvalues_ = new Nb[eig_struct.num_eigval_];
      eig_struct.im_eigenvalues_ = new Nb[eig_struct.num_eigval_];
      for(In i=I_0; i<eig_struct.num_eigval_; ++i)
      {
         eig_struct.re_eigenvalues_[i] = arValCont.Re()[i];
         eig_struct.im_eigenvalues_[i] = arValCont.Im()[i];
      }
      //Eigenvectors (temp. pointer/array to store them all in column major format):
      eig_struct.n_ = arVecCont.front().Size();
      std::unique_ptr<Nb[]> ptr(new Nb[eig_struct.num_eigval_ * eig_struct.n_]);
      eig_struct.rhs_eigenvectors_ = new Nb*[eig_struct.num_eigval_];
      In index = I_0;
      for(In i=I_0; i<eig_struct.num_eigval_; ++i)
      {
         MidasAssert(arVecCont[i].Size() == eig_struct.n_, "Wrong size of DataCont "+std::to_string(i));
         for(In j=I_0; j<eig_struct.n_; ++j)
         {
            arVecCont[i].DataIo(IO_GET, j, ptr[index]);
            ++index;
         }
         eig_struct.rhs_eigenvectors_[i] = ptr.get() + i*eig_struct.n_;
      }
      MidasAssert(index == eig_struct.num_eigval_ * eig_struct.n_, "Something wrong with ptr/index.");
         //^Just to be sure.
      ptr.release(); //Release ownership (will be deleted by ~Eigenvalue_ext_struct).
   }
   else  //Set all sizes to zero and pointers to null:
   {
      eig_struct.n_                 = I_0;
      eig_struct.re_eigenvalues_    = nullptr;
      eig_struct.im_eigenvalues_    = nullptr;
      eig_struct.rhs_eigenvectors_  = nullptr;
   }

   //At last, write it:
   WriteEigenDataToMolInfo(eig_struct, aTransWithBasis, aInvertedMassWeight);
}

void FreqAna::WriteHessianToMolInfo(const MidasMatrix& arHessian)
{
   HessianData hessian_data(HessianType::Key());
   hessian_data.Initialize(origin_type::CALCULATED, arHessian);
   mpMolInfo->ReInitHessian(hessian_data);
}

void FreqAna::WriteHessianToMolInfo(MidasMatrix&& arHessian)
{
   HessianData hessian_data(HessianType::Key());
   hessian_data.Initialize(origin_type::CALCULATED, std::move(arHessian));
   mpMolInfo->ReInitHessian(hessian_data);
}

void FreqAna::WriteMoleculeFile(const std::string& arName, const std::string& arType) const
{
   mpMolInfo->WriteMoleculeFile(molecule::MoleculeFileInfo(arName, arType));
}

void FreqAna::InvertedMassWeight(std::vector<std::vector<Nb> >& arCoords)
{
   //Do inverted mass-weight, i.e. coord = M^(-1/2) coord.
   for(In i=I_0; i<arCoords.size(); ++i)
   {
      for(In j=I_0; j<arCoords[i].size(); ++j)
      {
         arCoords[i][j] /= sqrt(mpMolInfo->GetNuclMassi(j/I_3));
      }
   }
}
