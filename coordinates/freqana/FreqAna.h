/*
************************************************************************
*  
* @file                FreqAna.h
*
* Created:             21-04-2015
*
* Author:              Mads Boettger Hansen (mb.hansen@chem.au.dk)
*
* Short Description:   Frequency analysis by diagonalization of the
*                      Hessian matrix obtained using the 3-point
*                      central difference formula.
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef FREQANA_H_INCLUDED
#define FREQANA_H_INCLUDED

#include<memory>
#include<vector>
#include"input/Input.h"
#include"input/FreqAnaCalcDef.h"
#include"util/MidasStream.h"
#include"mmv/MidasMatrix.h"
#include"lapack_interface/LapackInterface.h"
#include"pes/molecule/MoleculeInfo.h"
#include"nuclei/Nuclei.h"
#include"pes/singlepoint/SinglePoint.h"
#include"it_solver/EigenvalueContainer.h"
extern MidasStream Mout;

using namespace midas;

class FreqAna
{
   private:
      //Data members:
      molecule::MoleculeInfo* const mpMolInfo;
      const FreqAnaCalcDef* const mpCalcDef;
      const MidasMatrix mInitCoords;   //Needed for transformation with basis.

      //"Utility" functions:
      std::vector<Nuclei> DispConfigWithVector(std::vector<Nuclei> arConfig, const MidasVector& arVector, Nb aDispFactor) const;

   public:
      //Constructor:
      FreqAna(molecule::MoleculeInfo* const apMolInfo, const FreqAnaCalcDef* const apFreqAnaCalcDef);
      FreqAna(molecule::MoleculeInfo* const apMolInfo);

      //Getting:
      const molecule::MoleculeInfo&   MolInfo() const {return *mpMolInfo;}
      const FreqAnaCalcDef& CalcDef() const {return *mpCalcDef;}
      const MidasMatrix&    InitCoords() const {return mInitCoords;}

      //Functions used by FreqAnaDrv:
      //Regular frequency analysis:
      void Preparations() const;
      void DoFreqAna();      
      void DoFreqAna(const MidasMatrix&, const std::string& arOutFileName="FreqAna.mol"); //<do frequency analysis with precalculated Hessian

      //Targeting:
      std::vector<DataCont> TargetsFromMolInfo() const;

      //"Utility" functions:
      MidasMatrix CalcHessian() const; //Calcs. (reduced Hess.); B^T Sigma (B: mat. of basis vectors).
      MidasMatrix CalcSigma() const;   //Calcs. Sigma = H B. (Using basis from MoleculeInfo.)
      MidasMatrix HessianTransformation(const MidasMatrix& arB) const;
         //^The actual transformation used by CalcSigma(), and other transformers.
         //NB: The vectors to be transformed by the Hessian, as in sigma_i = H*b_i, should be stored
         //as ROWS in arB. (Yes, counter-intuitive compared to the column-major notation used above.
         //It's because the vibrational modes are stored as rows in MoleculeInfo::mNormalCoord.)
         //The MidasMatrix output matrix has the transformed vectors stored as COLUMNS though.

      std::vector<std::vector<Nb> > TransEigVecsWithBasis(const Eigenvalue_struct<Nb>& arEigStruct) const;
      std::vector<std::vector<Nb> > TransEigVecsWithBasis(const Eigenvalue_ext_struct<Nb>& arEigStruct) const;
      In VectorDimension() const {return 3*mpMolInfo->GetNumberOfNuclei();}
      void CompareEigenData(const Eigenvalue_struct<Nb>& arEigSol, const Eigenvalue_ext_struct<Nb>& arEigSolExt, std::ostream& arOS = Mout) const;
      void WriteEigenDataToMolInfo(const Eigenvalue_struct<Nb>& arEigSol, bool aTransWithBasis = true, bool aInvertedMassWeight = false);
      void WriteEigenDataToMolInfo(const Eigenvalue_ext_struct<Nb>& arEigSol, bool aTransWithBasis = true, bool aInvertedMassWeight = false);
      void WriteEigenDataToMolInfo(const EigenvalueContainer<MidasVector, MidasVector>& arValCont, std::vector<DataCont>& arVecCont, bool aTransWithBasis = false, bool aInvertedMassWeight = true);
      void WriteHessianToMolInfo(const MidasMatrix& arHessian);
      void WriteHessianToMolInfo(MidasMatrix&& arHessian);
      void WriteMoleculeFile(const std::string& arName, const std::string& arType = "MIDAS") const;
      void InvertedMassWeight(std::vector<std::vector<Nb> >& arCoords);
};

#endif //FREQANA_H_INCLUDED
