/*
************************************************************************
*  
* @file                RotCoordDrv.h
*
* Created:             16-01-2020
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Declaration of RotCoordDrv
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ROTCOORDDRV_H_INCLUDED
#define ROTCOORDDRV_H_INCLUDED

// Forward decl
class RotCoordCalcDef;

namespace midas::molecule
{
class MoleculeInfo;
} /* namespace midas::molecule */

/**
 * Run coordinate rotations
 **/
void RotCoordDrv
   (  const RotCoordCalcDef* const apRotCoordCalcDef
   ,  midas::molecule::MoleculeInfo* arMolInfo
   );

#endif /* ROTCOORDDRV_H_INCLUDED */
