/*
************************************************************************
*  
* @file                BaseMeasureFunction.h
*
* Created:             07-12-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Handles the type of function for finding 
*                      the angles and convergence for coordinate rotation
*                      Based on VscfOptCoord by Bo Thomsen
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "coordinates/rotcoord/BaseMeasureFunction.h"
#include "coordinates/rotcoord/GridMeasureFunction.h"

/**
 *
**/
BaseMeasureFunction::BaseMeasureFunction
   (  const RotCoordCalcDef* const apCalcDef
   ,  midas::molecule::MoleculeInfo* arMolInfo
   )
   :  mOptMeasure
      (  BaseOptMeasure::Factory
         (  apCalcDef
         ,  arMolInfo
         )
      )
   ,  mName("")
   ,  mConvCrit("")
   ,  mInitValue(C_0)
   ,  mCurrentValue(C_0)
{
}

/**
 * Implementation of d-tor. Thus, BaseMeasureFunction is abstract, but the derived classes do not need to implement a destructor.
 **/
BaseMeasureFunction::~BaseMeasureFunction
   (
   )
{
}

/**
 * Factory 
**/
typename BaseMeasureFunction::func_ptr_t BaseMeasureFunction::Factory
   (  const RotCoordCalcDef* const apCalcDef
   ,  midas::molecule::MoleculeInfo* apMolInfo
   )
{ 
   return GridMeasureFunction::Factory(apCalcDef, apMolInfo);
}

/**
 * Initialization 
**/
void BaseMeasureFunction::Init()
{
  mOptMeasure->InitMeasure();
  mInitValue = mOptMeasure->GetCurrentValue();
  mCurrentValue = mInitValue;
}

