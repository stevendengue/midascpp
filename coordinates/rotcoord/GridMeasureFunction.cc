/*
************************************************************************
*  
* @file                GridMeasureFunction.cc
*
* Created:             07-12-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Handles the type of function for finding 
*                      the angles and convergence for coordinate rotation
*                      Based on VscfOptCoord by Bo Thomsen
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <complex>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <random>

// midas headers
#include "mmv/MidasMatrix.h"
#include "coordinates/rotcoord/GridMeasureFunction.h"
#include "coordinates/rotcoord/SerGridMeasureFunction.h"
#include "coordinates/rotcoord/ParGridMeasureFunction.h"

/**
 * Constructor
**/
GridMeasureFunction::GridMeasureFunction
   (  const RotCoordCalcDef* const apCalcDef
   ,  midas::molecule::MoleculeInfo* arMolInfo
   )
   :  BaseMeasureFunction
      (  apCalcDef
      ,  arMolInfo
      )
   ,  mpGridCalcDef(&apCalcDef->GetGridCalcDef())
   ,  mFourierPts
      (  apCalcDef->GetGridCalcDef().GetFourierPt()
      )
{
   mDoGridFreqScreen = apCalcDef->GetmDoFreqScreen(); 
   mGridFreqScreenMaxFreq = apCalcDef->GetmFreqScreenMaxFreq()/C_AUTKAYS;
   
   mName = "Grid based";
   switch(mpGridCalcDef->GetOptParam())
   {
      case RotCoordParam::VALUE :
      {
         mConvCrit="Change in value";
         break;
      }
      case RotCoordParam::GRADIENT :
      {
         mConvCrit="Gradient";
         break;
      }
      case RotCoordParam::GRADIENTNORM :
      {
         mConvCrit="Gradient norm";
         break;
      }
      case RotCoordParam::GRADHESS :
      {
         mConvCrit="Gradient checking also the Hessian";
         break;
      }
      default :
      {
         MIDASERROR("Unknown convergence criteria");
         break;
      }
   } 
}

/**
 * Factory
**/
typename GridMeasureFunction::func_ptr_t GridMeasureFunction::Factory
   (  const RotCoordCalcDef* const apCalcDef
   ,  midas::molecule::MoleculeInfo* apMolInfo
   )
{
   if (apCalcDef->GetSetDoRotsAtSweepEnd())
   {
      return std::make_unique<ParGridMeasureFunction>(apCalcDef, apMolInfo);
   }
   else
   {
      return std::make_unique<SerGridMeasureFunction>(apCalcDef, apMolInfo);
   }
}

/**
 * Function for quickly finding a point near global minima using a Fourier expansion
**/
Nb GridMeasureFunction::GenGridCalcMin
   (  In aQFirst
   ,  In aQSecond
   )  const
{
   In nr_of_points = I_2*mFourierPts + I_1;
   MidasVector angle_points(nr_of_points);
   MidasVector energy_points(nr_of_points);
   Nb min_ang = C_0;
   
   for (In i = -mFourierPts; i <= mFourierPts; ++i)
   {
      angle_points[i + mFourierPts] = C_PI*Nb(i)/(C_2*Nb(nr_of_points));
      std::pair<Nb, bool> calc_result = mOptMeasure->RelEval(aQFirst, aQSecond, angle_points[i + mFourierPts]);

      if (!calc_result.second)
      {
         // If high enough IoLevel setting we can explain why the rotation angle is set to zero for a particular mode-pair
         if (gDebug || gIoLevel > I_5)
         {
            Mout << "  Fourier scheme failed for modes (" << aQFirst << "," << aQSecond << ") with angle " << angle_points[i + mFourierPts] << std::endl;
         
            MidasWarning("Fourier scheme failed, please investigate", true);
         }
         
         return C_0;
      }
      else if (calc_result.first < C_0)
      {
         MidasWarning("Fourier scheme recieved negative measure value, please investigate", true);
      }
     
      //
      energy_points[i + mFourierPts] = std::fabs(calc_result.first);
   }
   
   std::vector<std::complex<Nb> > fourier_coeff;
   GenerateFourierCoeff(fourier_coeff, angle_points, energy_points);
   Nb curr_min = C_100;       //Emil, (14/02/2019): Weird Initialization for control value?
   In nr_fine_pt = mFourierPts * I_10 * I_2 + I_1;
   MidasVector points(nr_fine_pt);
   MidasVector energies(nr_fine_pt);
   
   for (In i = -mFourierPts*I_10; i <= mFourierPts*I_10; ++i)
   {
      Nb pt = C_PI*Nb(i)/(C_2*Nb(nr_fine_pt));
      Nb en = CalculateFourierPt(fourier_coeff, pt);
      if (curr_min > en)
      {
         curr_min = en;
         min_ang = pt;
      }
   }
   return min_ang;
}

/**
 * Calculates the approximate energy using fourier expansion
**/
Nb GridMeasureFunction::CalculateFourierPt
   (  const std::vector<std::complex<Nb> >& arCoeffs
   ,  Nb point
   )  const
{
   std::complex<Nb> res(C_0, C_0);   
   for (In i = I_1; i <= mFourierPts; ++i)
   {
      res += arCoeffs[i - I_1]*exp(std::complex<Nb>(C_0, C_1)*C_4*Nb(i)*point);
   }
   return res.real();
}

/**
 * Generates the fourier expansion coefficients for the fourier search..
**/
void GridMeasureFunction::GenerateFourierCoeff
   (  std::vector<std::complex<Nb> >& arCoeffs
   ,  const MidasVector& arAngles
   ,  const MidasVector& arEnergies
   )  const
{
   In nr_of_points = I_2*mFourierPts + I_1;
   for (In i = I_1; i <= mFourierPts; ++i)
   {
      arCoeffs.emplace_back(C_0, C_0);
      for (In j = -mFourierPts; j <= mFourierPts; ++j)
      {
         arCoeffs[i - I_1] += arEnergies[j + mFourierPts]*std::exp(-std::complex<Nb>(C_0, C_1)*C_4*Nb(i)*arAngles[j + mFourierPts]);
      }
      arCoeffs[i - I_1] *= (C_1/Nb(nr_of_points));
   }
}

/**
 * This function finds the minima of the energy as a function of the angle between the modes aQFirst and aQSecond
**/
Nb GridMeasureFunction::NewtonStepper
   (  Nb aStartPt
   ,  In aQFirst
   ,  In aQSecond
   )  const
{
   In iter_count = I_0;
   Nb grad = C_100;
   Nb hess = C_0;
   
   while (iter_count < mpGridCalcDef->GetMaxNewtIter() && std::fabs(grad) > mpGridCalcDef->GetNewtConv())
   {
      if (!CalcGradHess(aStartPt, grad, hess, aQFirst, aQSecond))
      {
         // If high enough IoLevel setting we can explain why the rotation angle is set to zero for a particular mode-pair
         if (gDebug || gIoLevel > I_5)
         {
            Mout << "  Newton scheme failed for modes (" << aQFirst << "," << aQSecond << ") with angle " << aStartPt << ", due to negative VSCF energy " << std::endl;
         
            MidasWarning("VSCF calculation failed in Newton minimization scheme, please investigate", true);
         }

         return C_0;
      }

      if (hess < C_0)
      {
         // If high enough IoLevel setting we can explain why the rotation angle is set to zero for a particular mode-pair
         if (gDebug || gIoLevel > I_5)
         {
            Mout << "  Newton scheme failed for modes (" << aQFirst << "," << aQSecond << ") with angle " << aStartPt << ", due to negative Hessian: " << hess << std::endl;

            MidasWarning("Negative Hessian detected in Newton minimization scheme, please investigate", true);
         }

         return C_0;
      }

      aStartPt = aStartPt - grad/hess;
      ++iter_count;
   }
   
   return aStartPt;
}

/**
 * Calculates the Element of the Gradient 
**/
Nb GridMeasureFunction::CalculateGradElement
   (  In aQFirst
   ,  In aQSecond
   ) const
{
   Nb grad, hess;
   if (CalcGradHess(C_0, grad, hess, aQFirst, aQSecond))
   {  
      return grad;
   }
   else
   {
      MIDASERROR("Gradient calculation did not converge");
   }
   return C_0; //Never happens
}

/**
 * Calculates the numerical gradient and hessian
**/
bool GridMeasureFunction::CalcGradHess
   (  Nb aPt
   ,  Nb& aGrad
   ,  Nb& aHess
   ,  In aQFirst
   ,  In aQSecond
   )  const
{
   // Finite difference scheme to calculate gradient and hessian via calls to Vscf
   std::vector<Nb> energies;
   energies.reserve(I_3);
   Nb small = mpGridCalcDef->GetNumDiffDelta()*C_PI/C_180;
   for (In i = -I_1; i <= I_1; ++i)
   {
      std::pair<Nb, bool> calc_result = mOptMeasure->RelEval(aQFirst, aQSecond, aPt + Nb(i)*small);

      if (!calc_result.second)
      {
         return false;
      }
      else if (calc_result.first < C_0)
      {
         MidasWarning("Calculation of numerical Gradient and Hessian recieved negative measure value, please investigate", true);
      }
     
      //
      energies.push_back(std::fabs(calc_result.first));
   }
   aGrad = (energies[I_2] - energies[I_0])/(C_2*small);
   aHess = (energies[I_0] + energies[I_2] - energies[I_1]*C_2)/(small*small);
   
   return true;
}

/**
 * Return current value of the convergence criterion and updates the current value
**/
std::pair<bool, Nb> GridMeasureFunction::ConvCritOnGrid
   (  In aIter
   ) 
{
   if (aIter > I_0 && (mpGridCalcDef->GetPlotPoints() > I_0))
   {
      CreatePlotFiles(aIter - I_1);
   }

   Nb old_value = mCurrentValue;
   mCurrentValue = mOptMeasure->GetCurrentValue();
 
   In n_modes = mOptMeasure->GetNrOfModes();
   In space_size = In(Nb(n_modes*(n_modes - I_1))/C_2);

   switch (mpGridCalcDef->GetOptParam())
   {
      case RotCoordParam::VALUE :
      {
         Nb en_diff = std::fabs(mCurrentValue - old_value);
         
         if (gIoLevel > I_5)
         {
            Mout << " Largest criterion value: " << en_diff << std::endl;
         }

         return std::make_pair(true, en_diff);
         break;
      }
      case RotCoordParam::GRADIENT :
      {
         MidasVector gradient(space_size);
         CalculateGradient(gradient);
         Nb max_ele = C_0;
         for (In i = I_0; i < space_size; ++i)
         {
            max_ele = std::max(std::fabs(gradient[i]), max_ele);
         }
         
         if (gIoLevel > I_5)
         {
            Mout << " Largest gradient element: " << max_ele  << std::endl;
         }

         return std::make_pair(true, max_ele);
         break;
      }
      case RotCoordParam::GRADIENTNORM :
      {
         MidasVector gradient(space_size);
         CalculateGradient(gradient);
         Nb grad_norm = gradient.Norm();
         
         if (gIoLevel > I_5)
         {
            Mout << " Largest gradient norm: " << grad_norm  << std::endl;
         }

         return std::make_pair(true, grad_norm);
         break;
      }
      case RotCoordParam::GRADHESS :
      {
         Nb max_ele = C_0;
         bool hess_check = true;
         for (In j = I_0; j < mOptMeasure->GetNrOfModes(); ++j)
         {
            for (In k = j + I_1; k < mOptMeasure->GetNrOfModes(); ++k)
            {
               Nb grad, hess;

               if (  mOptMeasure->KnowsFrequencies()
                  )
               {
                  std::vector<Nb> curr_freqs = mOptMeasure->GetCurrentFreqs();
                  if (  mDoGridFreqScreen
                     && std::abs(curr_freqs[j] - curr_freqs[k]) > mGridFreqScreenMaxFreq 
                     )
                  {
                     continue;
                  }
               }

               if (CalcGradHess(C_0, grad, hess, j, k))
               {  
                  if (hess < C_0)
                  {
                     Mout << " Negative Hessian element found skip the rest of the gradient calculation" << std::endl;
                     hess_check = false;
                     break;
                  }
                  else
                  {
                     max_ele = std::max(std::fabs(grad), max_ele);
                  }
               }
               else
               {
                  MIDASERROR("Gradient calculation did not converge");
               }
            }
            if (hess_check == false)
            {
               break;
            }
         }
         
         if (gIoLevel > I_5)
         {
            Mout << " Largest gradient element: " << max_ele  << std::endl;
         }

         return std::make_pair(hess_check, max_ele);
         break;
      }
      default :
      {
         MIDASERROR("Unknown convergence criteria");
         break;
      }
   }
   return std::make_pair(true, C_NB_MAX); //NEVER HAPPENS
}

/** 
 * Calculates the rotation angle on a grid 
**/
Nb GridMeasureFunction::GetAngleOnGrid
   (  In aQFirst
   ,  In aQSecond
   ,  In aIter
   ) const
{
   if (mpGridCalcDef->GetPlotPoints() > I_0)
   {
      PlotRotation(aQFirst, aQSecond, aIter);
   } 
   
   Nb grid_min = GenGridCalcMin(aQFirst, aQSecond);

   if (gDebug || gIoLevel > I_7)
   {
      Mout << "  Found Fourier series minimum at angle: " << grid_min << " for modes " << aQFirst << "," << aQSecond << std::endl;
   }
   
   Nb newt_min = NewtonStepper(grid_min, aQFirst, aQSecond);

   if (gDebug || gIoLevel > I_7)
   {
      Mout << "  Found Newton minimization minimum at angle: " << grid_min << " for modes " << aQFirst << "," << aQSecond << std::endl;
   }
   
   return newt_min;
}

/**
 * Plot all rotations
**/
void GridMeasureFunction::PlotAllRotations() const
{
   Nb nmodes = mOptMeasure->GetNrOfModes();
   for (In i = I_0; i < nmodes; ++i)
   {
      for (In j = i + I_1; j < nmodes; ++j)
      {
         PlotRotation(i, j, I_0);
      }
   }
   CreatePlotFiles(I_0);
}

/**
 * Create gnu plot files for plotting rotations
**/
void GridMeasureFunction::CreatePlotFiles(In aSweepNr) const
{
   std::stringstream file_name;
   file_name << "Plots_For_" << mOptMeasure->GetName() << "_Sweep_" << aSweepNr;
   std::ofstream plot_file(file_name.str());
   plot_file << "set terminal pdf enhanced" << std::endl;
   plot_file << "set ylabel \" "+mOptMeasure->GetName()+" \" "<< std::endl;
   plot_file << "set xtics 15" << std::endl;
   plot_file << "unset key" << std::endl;
   plot_file << "set multiplot layout 2, 2" << std::endl;

   In counter = I_0;
   Nb nmodes=mOptMeasure->GetNrOfModes();

   for(In i = I_0; i < nmodes; ++i)
   {
      for(In j = i+I_1; j < nmodes; ++j)
      {
         std::stringstream file_name;
         if(I_0 == counter % I_4 && I_0 != counter)
            plot_file << "unset multiplot" << std::endl << "set multiplot layout 2, 2" << std::endl;
         plot_file << "set xlabel \"{/Symbol q}_" << i << "_" << j << "({/Symbol \\260})\"" << std::endl;
         plot_file << "plot \"" << mOptMeasure->GetName() << "_" << i << "_" << j << "_SWEEP" << aSweepNr << "\"" << std::endl;
         ++counter;
      }
   }
}

/**
* Plot one rotation
**/
void GridMeasureFunction::PlotRotation(In aQFirst, In aQSecond, In aSweepNr) const
{
   std::stringstream file_name;
   file_name << mOptMeasure->GetName() << "_" << aQFirst << "_" << aQSecond << "_SWEEP" << aSweepNr; 
   std::ofstream data(file_name.str());
   if(data.is_open())
   {
      In nr_of_points = I_2*mpGridCalcDef->GetPlotPoints()+I_1;
      for(In i = -(mpGridCalcDef->GetPlotPoints()); i <= mpGridCalcDef->GetPlotPoints(); ++i)
      {
         std::pair<Nb, bool> calc_result = mOptMeasure->RelEval(aQFirst, aQSecond, C_PI*Nb(i)/(C_2*Nb(nr_of_points)));
         if(calc_result.second)
         {
            data << Nb(90*i)/(Nb(nr_of_points)) << " "<< calc_result.first - mInitValue << std::endl;
         }
      }   
   }
}

