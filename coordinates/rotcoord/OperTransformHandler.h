/**
************************************************************************
*  
* @file                OperTransformHandler.h
*
* Created:             03-12-2015
*
* Author:              Carolin Koenig, based on Bo Thomsen OperRotHandler
*
* Short Description:   Handles the transformation of polynomial potential operator
* 
* Rev. Date            
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef OPERTRANSFORMHANDLER_H
#define OPERTRANSFORMHANDLER_H

#include <vector>
using std::vector;
#include <map>
using std::map;
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
//Forward declaring since we are not really using here
class OpDef;

class OperTransformHandler{
   protected:
      OpDef*                           mOpDef;
      In                                 mOrder;
      bool                              mScaled;
      In                               mMaxCoupLevel;
      bool                             mKeepMaxCoupLevel;
      map<vector<GlobalModeNr>, Nb>    mOrigOper;
      map<vector<GlobalModeNr>, Nb>      mCurrOper;
      MidasVector                        mFrequencies;
      MidasVector                        mScalFactors;

      std::vector<std::string>          mModeNames;
      std::vector<GlobalModeNr>         mModeNumbers;

      OperTransformHandler() {;}

      std::vector<GlobalModeNr> GetModeNrVector(const std::vector<string>& arModeNames) const;
      In GetOper(map<vector<GlobalModeNr>, Nb>&, std::vector<GlobalModeNr>&);
      void GiveOper(const map<vector<GlobalModeNr>, Nb>&,OpDef&) const;
      bool IncludeTerm(const vector<GlobalModeNr>& arModeVector) const;
   public:
      std::vector<std::string> GetModeNames() const {return mModeNames;}
      In GetNrOfModes() const;
      OperTransformHandler(OpDef*, bool arKeepMaxCoupLevel = false);
      void DumpPotential(const string& aSuffix);
      void DumpPotentialNew(const string& aSuffix);
      MidasVector GetScaleFrequencies() const
      {
         return mFrequencies;
      }
      std::vector<Nb> GetQuasiFrequencies() const;
      void SetFreq(const std::string arModeName, const Nb& arFreq);
};
#endif //OPERTRANSFORMHANDLER_H
