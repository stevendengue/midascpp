/**
************************************************************************
* 
* @file                SaVscfOptMeasure.h
*
* Created:             12-12-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk) 
*
* Short Description:   class calculating the state average VSCF energy for  
*                      Based in StateAverageCalculationForOpt by Bo Thomsen
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef SAVSCFOPTMEASURE_H
#define SAVSCFOPTMEASURE_H
#include <utility>
#include "inc_gen/TypeDefs.h"
#include "coordinates/rotcoord/VscfOptMeasure.h"

class SaVscfOptMeasure
   :  public VscfOptMeasure
{
   private:
      std::pair<Nb, bool> CalcSaVscfEnergy(In aQFirst, In aQSecond, Nb aAngle);

   public:
      SaVscfOptMeasure
         (  const RotCoordVscfCalcDef* const apCalcDef
         )
         :  VscfOptMeasure(apCalcDef)
      {
         mMeasureType="State averaged "+mMeasureType;
      }
      virtual ~SaVscfOptMeasure() = default;

      std::pair<Nb, bool> Eval(In aQFirst, In aQSecond, Nb aAngle)
      {
         return CalcSaVscfEnergy(aQFirst,aQSecond,aAngle);
      }

};
#endif //STATEAVCALCULATIONFOROPT_H
