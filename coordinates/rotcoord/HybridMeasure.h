/*
************************************************************************
*  
* @file                HybridMeasure.h
*
* Created:             12-03-2015
*
* Author:              Emil Lund Klinting (Klint@chem.au.dk)
*                      Niels Kristian Madsen (nielskm@chem.au.dk), extension to multiple VSCF measures
*
* Short Description:   Contains the functions needed for utilizing the hybrid
*                      measure which combines localization and optimization
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef HYBRIDMEASURE_H_INCLUDED
#define HYBRIDMEASURE_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "coordinates/rotcoord/BaseOptMeasure.h"

// Forward decl
class RotCoordCalcDef;
namespace midas::molecule
{
class MoleculeInfo;
} /* namespace midas::molecule */

/**
 * Wrapper class for combining multiple measures
 **/
class HybridMeasure
   :  public BaseOptMeasure
{
   public:
      //! Alias
      using measure_ptr_t = std::unique_ptr<BaseOptMeasure>;      ///< Pointer to measure
      using wmeasure_t = std::pair<Nb, measure_ptr_t>;            ///< Weighted measure
      using ret_t = std::pair<Nb, bool>;                          ///< Return type of Eval functions

      //! c-tor
      HybridMeasure
         (  const RotCoordCalcDef* const apCalcDef
         ,  midas::molecule::MoleculeInfo* apMolInfo
         );

      //! Eval
      ret_t Eval
         (  In aQFirst=-I_1
         ,  In aQSecond=-I_1
         ,  Nb aAngle=C_0
         );
      ret_t RelEval
         (  In aQFirst=-I_1
         ,  In aQSecond=-I_1
         ,  Nb aAngle=C_0
         );

      //! Frequencies
      bool KnowsFrequencies
         (
         )  const override;
      std::vector<Nb> GetCurrentFreqs
         (
         )  const override;

      //! Update data for all measures
      void UpdateData
         (  In aQFirst
         ,  In aQSecond
         ,  Nb aAngle
         )  override;

      // Finalize
      void Finalize
         (  const std::string& aPostFix
         )  override;

      //! Get number of modes
      In GetNrOfModes
         (
         )  const override;

   private:
      //! Vector of optimization measures
      std::vector<wmeasure_t> mOptMeasures;

      //! Localization measure (if needed)
      wmeasure_t mLocMeasure = {C_0, nullptr};
};

#endif /* HYBRIDMEASURE_H_INCLUDED */
