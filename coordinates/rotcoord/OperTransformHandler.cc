/*
************************************************************************
*
* @file                OperTransformHandler.cc
*
* Created:             03-12-2015
*
* Author:              Carolin Koenig, based on Bo Thomsen OperRotHandler
*
* Short Description:   Handles the transformation of polynomian potential operator
*
* Rev. Date
*
* Detailed  Description:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/
#include<vector>
#include<map>
#include<algorithm>
#include<set>
#include <fstream>
#include "input/OpDef.h"
#include "input/MidasOperatorWriter.h"
#include "mmv/MidasMatrix.h"
#include "coordinates/rotcoord/OperTransformHandler.h"
#include "input/GlobalOperatorDefinitions.h"
#include "input/Input.h"
#include "operator/OneModeOper.h"
#include "util/Path.h"

void SortTermForOperatorWriter
   (  std::vector<string>& arModes
   ,  std::vector<string>& arFunctions
   );

OperTransformHandler::OperTransformHandler
   (  OpDef* arPotential
   ,  bool arKeepMaxCoupLevel
   )
   : mOpDef(arPotential)
   , mScaled(true)
   , mMaxCoupLevel(arPotential->McLevel())
   , mKeepMaxCoupLevel(arKeepMaxCoupLevel)
{
   mOrder = GetOper(mOrigOper,mModeNumbers);
   mCurrOper = mOrigOper;

   if ((mMaxCoupLevel < GetNrOfModes()) &&  (mOrder != mMaxCoupLevel))
   {
      if (gDebug || gIoLevel > I_14)
      {
         Mout << "mMaxCoupLevel: " << mMaxCoupLevel << endl;
         Mout << "NrOfModes: " << GetNrOfModes() << endl;
         Mout << "mOrder: " << mOrder << endl;
         Mout << "mMaxCoupLevel: " << mMaxCoupLevel << endl;
      }

      MidasWarning("The operator given for rotation is not invariant w.r.t. rotation", true);
   }

   mModeNames.reserve(GetNrOfModes());
   for (In i = I_0; i < GetNrOfModes(); ++i)
   {
      mModeNames.push_back("Q"+std::to_string(i));
   }
}

In OperTransformHandler::GetOper
   (  std::map<std::vector<GlobalModeNr>, Nb>& arOperStruct
   ,  std::vector<GlobalModeNr>& arModeNumbers
   )
{
   std::vector<GlobalModeNr> mode_no_set;

   In max_order=I_0;
   mScalFactors.SetNewSize(mOpDef->mScalFactors.size());
   mFrequencies.SetNewSize(mOpDef->mScalFactors.size());

   for(In i = I_0; i < mOpDef->mScalFactors.size(); ++i)
   {
      mScalFactors[i] = mOpDef->mScalFactors[i];
      mFrequencies[i] = mScalFactors[i]*mScalFactors[i];
   }
   if(mFrequencies.Size() == I_0)
      mScaled = false;

   for(In i = I_0; i < mOpDef->Nterms(); ++i)
   {
      In order = I_0;
      bool ignore = false;
      std::vector<GlobalModeNr> oper_term;
      Nb scaling = C_1;

      for(In j = I_0; j < mOpDef->NfactorsInTerm(i); ++j)
      {
         LocalModeNr local_mode_nr = mOpDef->ModeForFactor(i,j);
         LocalOperNr local_oper_nr = mOpDef->OperForFactor(i,j);
         GlobalOperNr global_oper_nr = mOpDef->GetOneModeOperGlobalNr(local_mode_nr,local_oper_nr);
         GlobalModeNr global_mode_nr = mOpDef->GetGlobalModeNr(local_mode_nr);
         const std::unique_ptr<OneModeOperBase<Nb>>& oper = gOperatorDefs.GetOneModeOper(global_oper_nr);

         // add only unique elements in the order they appear (no sorting)
         if (std::find(mode_no_set.begin(), mode_no_set.end(), global_mode_nr) == mode_no_set.end())
         {
            mode_no_set.push_back(global_mode_nr);
         }
         
         if (  !oper->GetmIsCoriolisTerm()
            || (  oper->GetRDer() == I_0
               && oper->GetPow() != I_0
               )
            )
         {
            if (oper->GetLDer() || oper->GetRDer() != I_0)
            {
               ignore = true;
            }

            for (In k = I_0; k < oper->GetPow(); ++k)
            {
               oper_term.push_back(global_mode_nr);
            }
            order += oper->GetPow();

            if (mScaled)
            {
               scaling *= std::pow(mOpDef->ScaleFactorForMode(local_mode_nr), Nb(oper->GetPow()));
            }
         }
         else
         {
            MIDASERROR("Operator is not simple, cannot do rotations in OptVscf");
         }
      }
      //If the operator does not contain a derivative
      //Since the operator has already been cleaned we need not worry about doublicate terms here
      if (!ignore)
      {
         std::sort(oper_term.begin(), oper_term.end());
         //mOrigOper.insert(std::make_pair(oper_term,mOpDef->Coef(i)));
         mOrigOper.insert(std::make_pair(oper_term,scaling*mOpDef->Coef(i)));
      }
      max_order = std::max(max_order,order);
   }

   arModeNumbers = mode_no_set;

   return max_order;
}


In OperTransformHandler::GetNrOfModes() const
{
   return mOpDef->NmodesInOp();
}

/**
 *
**/
std::vector<GlobalModeNr> OperTransformHandler::GetModeNrVector
   (  const std::vector<std::string>& arModeNames
   )  const
{
   std::vector<GlobalModeNr> mode_no_vec;
   mode_no_vec.reserve(arModeNames.size());
   for (In i=I_0; i < arModeNames.size(); ++i)
   {
      auto it=std::find(mModeNames.begin(),mModeNames.end(),arModeNames.at(i));
      MidasAssert(it != mModeNames.end(), "Asking for a non-existing mode "+arModeNames.at(i));
      In index = std::distance(mModeNames.begin(),it);
      mode_no_vec.push_back(mModeNumbers.at(index));
   }

   return mode_no_vec;
}

/**
 *
**/
void OperTransformHandler::GiveOper
   (  const std::map<std::vector<GlobalModeNr>, Nb>& arOper
   ,  OpDef& arOpDef
   )  const
{
   //Give the operator to arOpDef
   arOpDef.mScalFactors.SetNewSize(mOpDef->mScalFactors.size());
   std::vector<GlobalModeNr> vec;
   if (mScaled)
   {
      for(In i = I_0; i < mOpDef->mScalFactors.size(); ++i)
      {
         vec.push_back(i);
         vec.push_back(i);
         std::map<std::vector<GlobalModeNr>, Nb>::const_iterator it = arOper.find(vec);
         if (it!= arOper.end())
         {
            arOpDef.mScalFactors[i] = std::sqrt(std::sqrt(C_2 * std::fabs(it->second)));
         }
         else
         {
            arOpDef.mScalFactors[i] = C_1;
         }
         vec.clear();
      }
   }

   for (std::map<std::vector<GlobalModeNr>, Nb>::const_iterator it = arOper.begin(); it != arOper.end(); ++it)
   {
      set<GlobalModeNr> uniq;
      Nb scale_fact = C_1;
      for(In i = 0; i < it->first.size(); ++i)
      {
         uniq.insert(it->first[i]);
      }
      for(set<GlobalModeNr>::iterator mode_it = uniq.begin(); mode_it != uniq.end(); ++mode_it)
      {
         In power = std::count(it->first.begin(), it->first.end(), *mode_it);
         if(mScaled)
         {
            scale_fact *= pow(arOpDef.mScalFactors[*mode_it],power);
         }
         arOpDef.AddSimpleOneModeOperProd(arOpDef.mNterms, gOperatorDefs.GetModeLabel(*mode_it), power, 0, false, (mode_it == uniq.begin()));
      }
      arOpDef.mNterms++;
      arOpDef.AddCterm(it->second/scale_fact);
   }
   arOpDef.AddSimpleKe();
   arOpDef.SetName("Rotated oper");
   arOpDef.SetType("SOP-Q^I-ONLY");
   arOpDef.SetIgnQiOnly(true); //This will always be the case with our current scheme
   arOpDef.Reorganize(true);
   arOpDef.InitOpRange();
   arOpDef.CleanOper();
   arOpDef.MCRReorder();
   arOpDef.SetScaledPot(mScaled);
}

/**
 *
**/
void OperTransformHandler::DumpPotential(const std::string& aSuffix)
{
   ofstream pot_out(mOpDef->GetOpFile()+aSuffix);
   pot_out.setf(ios::scientific);
   pot_out.setf(ios::uppercase);
   pot_out.precision(I_22);
   pot_out << "SCALING FREQUENCIES N_FRQS=" << mFrequencies.Size()<<endl;
   for(In i = I_0; i < mFrequencies.Size(); ++i)
   {
      pot_out << mFrequencies[i] << endl;
   }
   pot_out << "DALTON_FOR_MIDAS" << endl;
   for(map<vector<GlobalModeNr>, Nb>::const_iterator it = mCurrOper.begin(); it != mCurrOper.end(); ++it)
   {
      set<GlobalModeNr> uniq;
      Nb scale_fact = C_1;
      for(In i = 0; i < it->first.size(); ++i)
         uniq.insert(it->first[i]);
      for(set<GlobalModeNr>::iterator mode_it = uniq.begin(); mode_it != uniq.end(); ++mode_it)
      {
         In power = std::count(it->first.begin(), it->first.end(), *mode_it);
         if (mScaled)
         {
            scale_fact *= std::pow(std::sqrt(std::fabs(mFrequencies[*mode_it])), power);
         }
      }
      pot_out << it->second/scale_fact << "    ";
      for(In i = I_0; i < it->first.size(); ++i)
         pot_out << it->first[i]+1 << "    ";
      pot_out << endl;
   }
}

/**
 *
**/
void OperTransformHandler::DumpPotentialNew(const std::string& aSuffix)
{
   std::string file = midas::path::IsRelPath(mOpDef->GetOpFile()) ? gMainDir + "/" + mOpDef->GetOpFile() : mOpDef->GetOpFile();
   file += aSuffix;
   MidasOperatorWriter op_writer(file);
   op_writer.AddModeNames(mModeNames);
   if (mScaled)
      op_writer.AddFrequencies(mFrequencies);

   for(map<vector<GlobalModeNr>, Nb>::const_iterator it = mCurrOper.begin(); it != mCurrOper.end(); ++it)
   {
      std::set<GlobalModeNr> mode_set;
      std::vector<In> powers;
      powers.reserve(it->first.size());
      std::vector<std::string> mode_vec;
      mode_vec.reserve(it->first.size());
      In total_power=I_0;

      // get mode names and powers
      for(In i = 0; i < it->first.size(); ++i)
      {
         auto empl=mode_set.emplace(it->first[i]);
         if (empl.second)
         {
            auto it_mode_no=std::find(mModeNumbers.begin(),mModeNumbers.end(),it->first[i]);
            In index = std::distance(mModeNumbers.begin(),it_mode_no);
            mode_vec.push_back(mModeNames.at(index));
            In power = std::count(it->first.begin(), it->first.end(), it->first[i]);
            powers.push_back(power);
            total_power+=power;
         }
      }
      MidasAssert(total_power==it->first.size(),"Something went wrong with the total power in OperTransformHandler::DumpPotentialNew");

      // set up the functions
      std::vector<string> function_vec(powers.size()," ");
      for(In i = 0; i < powers.size(); ++i)
      {
         function_vec[i]="Q^"+std::to_string(powers.at(i));
      }

      // actual scaling
      Nb scale_fact = C_1;
      for(set<GlobalModeNr>::iterator mode_it = mode_set.begin(); mode_it != mode_set.end(); ++mode_it)
      {
         In power = std::count(it->first.begin(), it->first.end(), *mode_it);
         if (mScaled)
         {
            In index = std::distance(mModeNumbers.begin(),std::find(mModeNumbers.begin(),mModeNumbers.end(),*mode_it));
            scale_fact *= std::pow(std::sqrt(std::fabs(mFrequencies[index])), power);
         }
      }
      // and add the term to the writer
      SortTermForOperatorWriter(mode_vec,function_vec);
      op_writer.AddOperatorTerm(it->second/scale_fact,function_vec,mode_vec);
   }
}

/**
 *
**/
bool OperTransformHandler::IncludeTerm(const std::vector<GlobalModeNr>& arModeVector) const
{
  bool include =!mKeepMaxCoupLevel;
  if (!include)
  {
     set<GlobalModeNr> mode_set(arModeVector.begin(),arModeVector.end());
     include = (mode_set.size() <= mMaxCoupLevel );
  }
  return include;
}

/**
 *
**/
std::vector<Nb> OperTransformHandler::GetQuasiFrequencies() const
{
   std::vector<Nb> quasi_frequencies;

   quasi_frequencies.reserve(GetNrOfModes());
   
   std::vector<GlobalModeNr> vec;
   for (In i = I_0; i < GetNrOfModes(); ++i)
   {
      vec.push_back(i);
      vec.push_back(i);
      std::map<std::vector<GlobalModeNr>, Nb>::const_iterator it = mCurrOper.find(vec);
      if (it!= mCurrOper.end())
      {
         if (it->second < C_0)
         {
            MidasWarning("Found a negative quasi-frequency: " + std::to_string(it->second) + ", will set this to a positive value before using it ", true);
         }
         quasi_frequencies.push_back(std::sqrt(C_2 * std::fabs(it->second)));
      }
      else
      {
         quasi_frequencies.push_back(C_0);
      }
      vec.clear();
   }
   return quasi_frequencies;
}

/**
 *
**/
void OperTransformHandler::SetFreq(const std::string arModeName, const Nb& arFreq)
{
   MidasAssert(arFreq>I_0,"Cannot handle negative frequencies");
   auto it = std::find(mModeNames.begin(),mModeNames.end(),arModeName);
   MidasAssert(it!=mModeNames.end(),"Cannot assign scaling for non-existing mode "+ arModeName);
   In index=std::distance(mModeNames.begin(),it);
   bool inserted=false;
   if (mScalFactors.size()>I_0)
   {
      mScalFactors[index] = std::sqrt(std::fabs(arFreq));
      inserted=true;
   }
   if (mFrequencies.size()>I_0)
   {
      mFrequencies[index]=arFreq;
      inserted=true;
   }
   //MidasAssert(inserted,"Could not insert frequencies, as there are none beforehand..");
   if (!inserted) MidasWarning("Could not insert frequencies, as there are none beforehand..");
}
