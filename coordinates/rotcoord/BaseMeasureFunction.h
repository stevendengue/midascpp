/*
************************************************************************
*  
* @file                BaseMeasureFunction.h
*
* Created:             07-12-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Handles the type of function for finding 
*                      the angles and convergence for coordinate rotation
*                      Based on VscfOptCoord by Bo Thomsen
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef BASEMEASUREFUNCTION_H
#define BASEMEASUREFUNCTION_H

#include<memory>
#include<string>
#include<iostream>

#include "inc_gen/TypeDefs.h"
#include "coordinates/rotcoord/BaseOptMeasure.h"


/**
 *
 **/
class BaseMeasureFunction
{
   public:
      using func_ptr_t = std::unique_ptr<BaseMeasureFunction>;
      using measure_ptr_t = std::unique_ptr<BaseOptMeasure>;

      //! Default c-tor
      BaseMeasureFunction() = default;

      //! c-tor
      BaseMeasureFunction(const RotCoordCalcDef* const apCalcDef, midas::molecule::MoleculeInfo* arMolInfo);

      //! Purely virtual d-tor (to make the class abstract - implementation is also provided)
      virtual ~BaseMeasureFunction() = 0;

      //! Factory
      static func_ptr_t Factory(const RotCoordCalcDef* const apCalcDef, midas::molecule::MoleculeInfo* apMolInfo);

      void Init();
      void Finalize(const std::string& aPostFix) {mOptMeasure->Finalize(aPostFix);}
      virtual void PlotAllRotations() const = 0;
      virtual Nb GetAngle(In, In,In aIter=I_0) const = 0;
      virtual std::pair<bool,Nb> ConvCrit(In aIter) = 0;
      bool KnowsFrequencies() const {return mOptMeasure->KnowsFrequencies();}
      std::vector<Nb> GetCurrentFreqs() const {return mOptMeasure->GetCurrentFreqs();}

      const auto& GetName() const {return mName;}
      const auto& GetConvCrit() const {return mConvCrit;}
      In GetNrOfModes() {return mOptMeasure->GetNrOfModes();}
      void Update(In QFirst, In QSecond , Nb aAngle) {mOptMeasure->Update(QFirst, QSecond, aAngle);}
      Nb GetCurrentValue() { return mCurrentValue;}
      const auto& GetMeasureName() const {return mOptMeasure->GetName();}
      const auto& GetMeasureUnit() const {return mOptMeasure->GetUnit();}

   protected:
      measure_ptr_t mOptMeasure;    ///< Handles the actual measure
      std::string   mName;          ///< Name for measure function
      std::string   mConvCrit;      ///< Name of Convergence criterion
      Nb            mInitValue;     ///< Initial value of the measure
      Nb            mCurrentValue;  ///< Current value of the measure
};
#endif //BASEMEASUREFUNCTION_H
