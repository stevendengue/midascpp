/*
************************************************************************
*  
* @file                SimpeHoOptMeasure.cc
*
* Created:             21-08-2015
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   Vscf Measures 
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <utility>

#include "coordinates/rotcoord/SimpleHoVscfOptMeasure.h"
#include "input/Input.h"
In FindVScfCalcNumber(const string&);

SimpleHoVscfOptMeasure::SimpleHoVscfOptMeasure
   (  const RotCoordVscfCalcDef* const apCalcDef
   )
   :  VscfBaseOptMeasure(apCalcDef)
{
   if (apCalcDef->GetOperFile()!="NONE")
   {
      gOperatorDefs.ReSet(); 
      gOperatorDefs.AddOperator();
      gOperatorDefs[0].ReadInAll(apCalcDef->GetOperFile());
      mOperRotHandler.reset(new OperRotHandler(&gOperatorDefs[0],apCalcDef->KeepMaxCoupLevel()));
   }
   else 
   {
      mCalcNo=FindVScfCalcNumber(apCalcDef->GetName()); 
      mCalcDef= &gVscfCalcDef[mCalcNo];
      mOperNo=gOperatorDefs.GetOperatorNr(mCalcDef->Oper());
      if (mOperNo == -1) MIDASERROR("Operator not found in VscfOptMeasure");
      mOperRotHandler.reset(new OperRotHandler(&gOperatorDefs[mOperNo],apCalcDef->KeepMaxCoupLevel()));
      if (apCalcDef -> RotZeroFirst())
      {
         for (In i=0; i < GetNrOfModes(); ++i)
            for (In j=0; j < GetNrOfModes(); ++j)
               mOperRotHandler->TransformCurrOper(i, j,C_0);
      }
   }
   mMeasureType+=" (Simple HO)";
}
/**
* Set up and do the actual calculation   
**/
std::pair<Nb, bool> SimpleHoVscfOptMeasure::Eval(In aQFirst, In aQSecond, Nb aAngle)
{
   if (!gDebug)
   {
      Mout.Mute();
   }
   
   // Generate the operator
   OperRotHandler rot_op=*mOperRotHandler;
   rot_op.TransformCurrOper(aQFirst,aQSecond,aAngle);
   
   //
   Mout.Unmute();
   
   std::vector<Nb> freqs_rot = rot_op.GetQuasiFrequencies();
 
   bool rotate=true;
   if (aQFirst==-I_1||aQFirst==-I_1)
     rotate=false; 

   std::vector<Nb> freqs = mOperRotHandler->GetQuasiFrequencies();
   
   Nb zpe=I_0;
   bool success=true;
   for (In i=I_0; i < freqs.size(); ++i)
   {
      if (rotate && (i==aQFirst || i==aQSecond)){continue;} // do not include those to rotated here.
      if (freqs.at(i)<I_0) 
         success=false;
      zpe+=freqs.at(i);
   }
   zpe/=C_2;
   if (rotate)
   {
      std::pair<Nb, bool> changed_en=RelEval(aQFirst,aQSecond,aAngle);
      zpe+=changed_en.first;
      success = success && changed_en.second;
   }
   return std::make_pair(zpe,success);
}
std::pair<Nb, bool> SimpleHoVscfOptMeasure::RelEval(In aQFirst, In aQSecond, Nb aAngle)
{
   return mOperRotHandler->TwoModeSimpleHoEnergyAfterRot(aQFirst,aQSecond,aAngle);
}
