/*
************************************************************************
*  
* @file                LocMeasure.h
*
* Created:             26-11-2014
*
* Author:              Emil Lund Klinting (Klint@chem.au.dk)
*
* Short Description:   Contains the functions needed for the localization
*                      measure, which includes  evaluation of the localization
*                      criterion
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef LOCMEASURE_H
#define LOCMEASURE_H
#include "inc_gen/TypeDefs.h"
#include "pes/molecule/MoleculeInfo.h"
#include "coordinates/rotcoord/BaseOptMeasure.h"

using namespace midas;

/**
 *
 **/
class LocMeasure
   :  public BaseOptMeasure
{
   protected:
      In                        mNrOfModes;
      string                    mCurrentMoleculeName;
      molecule::MoleculeInfo*   mMoleculeInfo;

      std::vector<Nb> AtomicContri_i(In aQFirst) const;
      std::vector<Nb> AtomicContri_ij(In aQFirst, In aQSecond) const;
      Nb CalculateA(In aQFirst, In aQSecond) const;
      Nb CalculateB(In aQFirst, In aQSecond) const;
 
    public:
      LocMeasure(midas::molecule::MoleculeInfo*);
      Nb AnglePM(In aQFirst, In aQSecond) const;      
      Nb kiPM() const;
      Nb kiPM_rot(In aQFirst, In aQSecond, Nb aGamma) const; 

      std::vector<std::vector<Nb>> CentersOfVib() const;          // Trial      
      void PrintPairDistances() const;                            // Trial
      Nb PairDistance(const In& aMode1, const In& aMode2) const;  // Trial
      void DistanceFourthMC() const;                              // Trial
      In GetNrOfModes() const override {return mNrOfModes;}
      std::pair<Nb, bool> Eval(In aQFirst=-I_1, In aQSecond=-I_1, Nb aAngle=C_0);
      std::pair<Nb, bool> RelEval(In aQFirst=-I_1, In aQSecond=-I_1, Nb aAngle=C_0) {return Eval(aQFirst,aQSecond,aAngle);}
      void UpdateData(In QFirst, In QSecond, Nb aAngle) override;
      void Finalize(const std::string&) override;
      bool KnowsFrequencies() const override {return false;}
      std::vector<Nb> GetCurrentFreqs() const override
      {
         MIDASERROR("No update of frequencies possible in pure localization scheme -- I should never be here!!"); 
         std::vector<Nb> dummy;
         return dummy;
      }
};

#endif /* LOCMEASURE_H */
