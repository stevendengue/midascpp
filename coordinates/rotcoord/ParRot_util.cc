/*
************************************************************************
*  
* @file                ParRot_utile.cc
*
* Created:             11-12-2014
*
* Author:              Carolin Koenig (ckonig@chem.au.dk)
*
* Short Description:   
*                      Based on VscfParOptCoord by Bo Thomsen. 
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <utility>

// midas headers
#include "inc_gen/TypeDefs.h"

std::pair<In, In> GetPairForTaskNr(In aTaskNr, In aNrOfModes) 
{
   In first = 0;
   In second = aTaskNr;
   In second_corr = 0;
   while(second - (aNrOfModes-first-1) >= 0)
   {
      second = second - (aNrOfModes-first-1);
      ++first;
      ++second_corr;
   }
   second += second_corr + 1;
   return std::make_pair(first, second);
}

In GetTaskNrForPair(std::pair<In, In> aPair, In aNrOfModes)
{
   In place = 0;
   for(In j = aPair.first; j != 0; --j)
   {
      place += aNrOfModes - j - 1;
   }
   place += aPair.second - 1;
   return place;
}
