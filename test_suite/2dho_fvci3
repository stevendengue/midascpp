#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         2-dim ho. test 
   Wave Function:    FVCI / 8 8 HO basis 
   Test Purpose:     Check FVCI energy of direct calculated lowest states
   Reference:        Thompson & Truhlar, CPL 75, 87 (1980),
                     There are for the higher states up to 5 difference
                     on the last digit. They used only a 60 conf. as 
                     "accurate" which was apparently not conv. to 4 decimals
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 2dho_fvci3.minp <<%EOF%

#0 MIDAS Input     // This indicates start of Midas input from here

/*
Now comes the real input 
*/

// This the general input 

#1 general
#2 IoLevel
    5
#2 BufSIZE
 120
#2 FileSize
 500
#2 Time
!#2 Debug


// Now comes the section of the vibrational input
#1 Vib
#2 IoLevel
  5

#2 Operator    // This is operator input
#3 IoLevel
14
#3 Name
 testoper
#3operinput
#4operatorterms
-0.5 DDQ^2(a)
-0.5 DDQ^2(b)
0.98 Q^2(a)
0.245 Q^2(b)
0.0064 Q^3(a)
-0.08 Q^1(a) Q^2(b)
#3 KineticEnergy
 USER_DEFINED

#2 Basis     // This is basis input
#3 Name
 basis
#3 Define
 a HO n_high=8,omeg=1.4
 b HO n_high=8,omeg=0.7
#3 IoLevel
   9

#2 Vscf      // This a the VSCF input
#3 IoLevel
11

#2 Vcc       // This a the VCC  input
#3 Name
   1vcc
#3 IoLevel
11
#3 ExpHvci
 1 // Single exci only, ref state = vscf energy IF MODAL BASIS IS USED
#3 PrimitiveBasis


#2 Vcc       // This a the VCC  input
#3 Name
   2vcc
#3 IoLevel
11
#3 ExpHvci
 2 // Double = fvci
#3 PrimitiveBasis

/*
*/

/*
Input is over - just a little bit of more confusing comments.
*/

#0 Midas Input End              // End comment

Now it is no more input.
And I can write a long story here without effect.

%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do Vscf:" $log | wc -l`
CRIT2=`$GREP "Do Vcc:" $log | wc -l`
CRIT3=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`
CRIT4=`$GREP "n_high=8,omeg=1.4" $log | wc -l`
CRIT5=`$GREP "n_high=8,omeg=0.7" $log | wc -l`
TEST[1]=`expr $CRIT1 \
\+ $CRIT2 \
\+ $CRIT3 \
\+ $CRIT4 \
\+ $CRIT5`
echo $CRIT1
echo $CRIT2
echo $CRIT3
echo $CRIT4
echo $CRIT5
CTRL[1]=7
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT"

# SCF energy:
CRIT1=`$GREP "Occ: \[0,0\]" $log | $GREP "E =  1\.0493492614924[0-9][0-9]" | wc -l`
TEST[2]=`expr $CRIT1`
CTRL[2]=1
ERROR[2]="VSCF ENERGIES NOT CORRECT"

# FVCI energy:
CRIT01=`$GREP "ExpHvci_0 * 1.048497616454" $log | wc -l`
CRIT02=`$GREP "ExpHvci_1 * 1.740364190814" $log | wc -l`
CRIT03=`$GREP "ExpHvci_2 * 2.385864231554" $log | wc -l`
CRIT04=`$GREP "ExpHvci_3 * 2.486317079043" $log | wc -l`
CRIT05=`$GREP "ExpHvci_4 * 3.031618970389" $log | wc -l`
CRIT06=`$GREP "ExpHvci_5 * 3.206981569672" $log | wc -l`
CRIT07=`$GREP "ExpHvci_6 * 3.658459047775" $log | wc -l`
CRIT08=`$GREP "ExpHvci_7 * 3.827481565704" $log | wc -l`
CRIT09=`$GREP "ExpHvci_8 * 3.949552482154" $log | wc -l`
CRIT010=`$GREP "ExpHvci_9 * 4.273675544030" $log | wc -l`
CRIT011=`$GREP "ExpHvci_10 * 4.496525239302" $log | wc -l`
CRIT012=`$GREP "ExpHvci_11 * 4.688672820231" $log | wc -l`

TEST[3]=`expr \
$CRIT01 \
\+ $CRIT02 \
\+ $CRIT03 \
\+ $CRIT04 \
\+ $CRIT05 \
\+ $CRIT06 \
\+ $CRIT07 \
\+ $CRIT08 \
\+ $CRIT09 \
\+ $CRIT010 \
\+ $CRIT011 \
\+ $CRIT012 \
`
CTRL[3]=12
ERROR[3]="VCI ENERGIES NOT CORRECT"
echo $CRIT01
echo $CRIT02
echo $CRIT03
echo $CRIT04
echo $CRIT05
echo $CRIT06
echo $CRIT07
echo $CRIT08
echo $CRIT09
echo $CRIT010
echo $CRIT011
echo $CRIT012

PASSED=1
for i in 1 3 
do 
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > 2dho_fvci3.check
#######################################################################
