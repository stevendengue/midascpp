#!/bin/sh
#
# This is the script for generating files for a specific Midas test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
TESTNAME="mctdh_pulse"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         Water
   Wave Function:    MCTDH with time-dependent pulse
   Test Purpose:     Check energy and <Q0>
   Reference:        
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > ${TESTNAME}.minp <<%EOF%

#0 MIDAS Input     // This indicates start of Midas input from here

#1 General

#1 Vib
#2 IOLevel
 5

// Hamiltonian
#2 Operator
#3 Name
   h_mctdh
#3 OperFile
   ${TESTNAME}.mop
#3 SetInfo
   type=energy

// Q0 operator
#2 Operator
#3 Name
   q0
#3 OperInput
#4 ModeNames
 Q0 Q1 Q2
#4 OperatorTerms
 1.0  Q^1(Q0)

#2 Basis
#3 Name
   bsplines
#3 BsplineBasis
   10
#3 NPrimBasisFunctions
   50
#3 TurningPoint
   10

// Do a VCI calculation on h_ref
#2 Vcc
#3 Method
   VCI[3]
#3 LimitModalBasis
   [3*4]
#3 Oper
   h_mctdh
#3 Basis
   bsplines
#3 Name
   vci_ref

// Do MCTDH on VCI
#2 McTdH
#3 Method
   MCTDH
#3 ActiveSpace
   [3*4]
#3 Name
   mctdh_pulse
#3 Basis
   bsplines
#3 Oper
   h_mctdh
#3 InitialWf
   VCI vci_ref
#3 GaussianPulse
   q0
   0.01 1.e2 1.e1 7.1927213582064185E-03 0.0
#3 Integrator
   #4 Scheme
      VMF
   #4 OdeInfo
      #5 Type
         MIDAS DOPR853
      #5 TimeInterval
         0. 2.e2
      #5 OutputPoints
         1001
      #5 Tolerance
         1.e-10 1.e-10
      #5 InitialStepSize
         1.e-2
#3 Properties           // Properties to calculate
   energy
   halftimeautocorr
#3 ExpectationValues
   q0

#0 Midas Input End

%EOF%

#######################################################################
#  OPERATORS
#######################################################################
cat > ${TESTNAME}.mop << %EOF%
#0MIDASMOPINPUT
#1CONSTANTS
#1FUNCTIONS
#1OPERATORTERMS
 2.8537812113427207805216E-08  Q^1(Q0)
 2.8274241032022473518737E-05  Q^2(Q0)
 1.2482867361995886312798E-07  Q^3(Q0)
-5.3553605994238751009107E-10  Q^4(Q0)
-2.5371934953000163659453E-07  Q^1(Q1)
 1.5560698335548295290209E-04  Q^2(Q1)
-3.2124065114658151287585E-06  Q^3(Q1)
 4.4404202981240814551711E-08  Q^4(Q1)
-2.8421709430404007434845E-14  Q^1(Q2)
 1.6550190184716484509408E-04  Q^2(Q2)
 1.1368683772161602973938E-13  Q^3(Q2)
 4.7011212700454052537680E-08  Q^4(Q2)
 1.0503526937100104987621E-08  Q^1(Q0) Q^1(Q1)
-3.0864907785144168883562E-07  Q^1(Q0) Q^2(Q1)
 1.0755059065559180453420E-08  Q^1(Q0) Q^3(Q1)
 7.1887268404680071398616E-07  Q^2(Q0) Q^1(Q1)
-4.5559545469586737453938E-08  Q^2(Q0) Q^2(Q1)
-1.0265523542329901829362E-08  Q^3(Q0) Q^1(Q1)
 2.8421709430404007434845E-14  Q^1(Q0) Q^1(Q2)
-9.7773227025754749774933E-07  Q^1(Q0) Q^2(Q2)
-5.6843418860808014869690E-14  Q^1(Q0) Q^3(Q2)
-5.6843418860808014869690E-14  Q^2(Q0) Q^1(Q2)
-5.6332964959437958896160E-08  Q^2(Q0) Q^2(Q2)
-2.8421709430404007434845E-14  Q^3(Q0) Q^1(Q2)
 5.6843418860808014869690E-14  Q^1(Q1) Q^1(Q2)
-9.8837517725769430398941E-06  Q^1(Q1) Q^2(Q2)
-1.7053025658242404460907E-13  Q^1(Q1) Q^3(Q2)
 4.5474735088646411895752E-13  Q^2(Q1) Q^1(Q2)
 2.7564192350837402045727E-07  Q^2(Q1) Q^2(Q2)
-2.2737367544323205947876E-13  Q^3(Q1) Q^1(Q2)
 5.6843418860808014869690E-14  Q^1(Q0) Q^1(Q1) Q^1(Q2)
 5.9629428506013937294483E-08  Q^1(Q0) Q^1(Q1) Q^2(Q2)
-4.5474735088646411895752E-13  Q^1(Q0) Q^2(Q1) Q^1(Q2)
-5.6843418860808014869690E-13  Q^2(Q0) Q^1(Q1) Q^1(Q2)
#0MIDASMOPINPUTEND
%EOF%

cat > ${TESTNAME}_ref.mop << %EOF%
#0MIDASMOPINPUT
#1CONSTANTS
#1FUNCTIONS
#1OPERATORTERMS
 2.8537812113427207805216E-08  Q^1(Q0)
 2.8274241032022473518737E-05  Q^2(Q0)
 1.2482867361995886312798E-07  Q^3(Q0)
-5.3553605994238751009107E-10  Q^4(Q0)
-2.5371934953000163659453E-07  Q^1(Q1)
 1.5560698335548295290209E-04  Q^2(Q1)
-3.2124065114658151287585E-06  Q^3(Q1)
 4.4404202981240814551711E-08  Q^4(Q1)
-2.8421709430404007434845E-14  Q^1(Q2)
 1.6550190184716484509408E-04  Q^2(Q2)
 1.1368683772161602973938E-13  Q^3(Q2)
 4.7011212700454052537680E-08  Q^4(Q2)
#0MIDASMOPINPUTEND
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")

# ------------------------------------------------------------------------------
# Check energy at t_beg and t_end
# ------------------------------------------------------------------------------
CRIT_ITER_E[0]=`$GREP -A2 "ENERGY \(Re\)" $log | grep "\- At t_beg:                2\.14311776796631..E-02" |  wc -l`
CRIT_ITER_E[1]=`$GREP -A2 "ENERGY \(Re\)" $log | grep "\- At t_end:                5\.252423156153....E-02" |  wc -l`
for i in $(seq 0 1)
do
   TEST+=(${CRIT_ITER_E[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''energy'\'', CRIT_ITER_E[$i].")
done

# ------------------------------------------------------------------------------
# Check autocorrelation function at t_beg and t_end and std deviation
# ------------------------------------------------------------------------------
CRIT_ITER_S[0]=`$GREP -A2 "HALFTIMEAUTOCORR \(abs\)" $log | grep "\- At t_beg:                1\.000000000000000.E+00" |  wc -l`
CRIT_ITER_S[1]=`$GREP -A2 "HALFTIMEAUTOCORR \(abs\)" $log | grep "\- At t_end:                7\.726749680.......E-03" |  wc -l`
for i in $(seq 0 1)
do
   TEST+=(${CRIT_ITER_S[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''auto-correlation function'\'', CRIT_ITER_S[$i].")
done

# ------------------------------------------------------------------------------
# Check Re[<q0>] at t_beg and t_end and std deviation
# ------------------------------------------------------------------------------
CRIT_ITER_Q[0]=`$GREP -A2 "Re\[<q0>\]" $log | grep "\- At t_beg:                1\.8223990372286...E-01" |  wc -l`
CRIT_ITER_Q[1]=`$GREP -A2 "Re\[<q0>\]" $log | grep "\- At t_end:                \-2\.2813470369740...E+01" |  wc -l`
CRIT_ITER_Q[2]=`$GREP -A8 "Re\[<q0>\]" $log | grep "\- Std. dev.                7\.59..............E+00" |  wc -l`
for i in $(seq 0 2)
do
   TEST+=(${CRIT_ITER_Q[$i]})
   CTRL+=(1)
   ERROR+=("Wrong '\''<Q0> expectation value'\'', CRIT_ITER_Q[$i].")
done

# ------------------------------------------------------------------------------
# Check against controls.
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do 
   if test ${TEST[$i]} -ne ${CTRL[$i]}
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > ${TESTNAME}.check
#######################################################################
