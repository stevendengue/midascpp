#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         2-dim ho. test 
   Wave Function:    VCC / HO basis 
   Test Purpose:     Check FVCC energy 
   Reference:        Henon Heiles potential, test dates back to Bowman 78,
                     and Norris et al. JCM 1996
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > vcc_2db.minp <<%EOF%

#0 MIDAS Input

#1 general
#2 IoLevel
  1
#2 BufSIZE
 4
#2 FileSize
 30
#2 Time


#1 Vib
#2 IoLevel
   3

#2 Operator
#3 Name
 2d_bowman78
#3operinput
#4operatorterms
-0.5 DDQ^2(a)
-0.5 DDQ^2(b)
.146875 Q^2(a)
1.062905 Q^2(b)
-0.009390024 Q^3(a)
-0.1116 Q^1(a) Q^2(b)
#3 KineticEnergy
 USER_DEFINED

#2 Basis
#3 Name
 basis
#3 Define
 a HO n_high=3,omeg=0.541987084716970
 b HO n_high=3,omeg=1.458015774948954
#3 IoLevel
   1

#2 Vscf
#3 IoLevel
 1
#3 Occup
 0 0 
#3 OccuMax
 2 1 

#2 Vcc
#3 LimitModalBasis
 4 4 
#3 Occup
 0 0 
#3 IoLevel
 5
#3 Vci
 7
#3 TestTransformer
#3 RepVciAsVcc
#3 MaxExci
 2
#3 Name
VciCalc
#3 ITEQRESIDTHR
 1.0e-12



#2 Vcc
#3 LimitModalBasis
 4 4 
#3 Occup
 0 0 
#3 OccuMax
 2 1 
#3 IoLevel
 5
#3 Vcc
#3 Transformer
   trf=orig outofspace=true t1transh=false
#3 MaxExci
 2
#3 ITEQRESIDTHR
 1.0e-12
#3 ITEQMAXIT
 30  


#0 Midas Input End              // End comment

%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

CRIT01=`$GREP "Final Vcc" $log | grep "9.916709323589[6,7]" | wc -l`
CRIT02=`$GREP "Final Vcc" $log | grep "1.518038830925[0,1]" | wc -l`
CRIT03=`$GREP "Final Vcc" $log | grep "2.0446245193639" | wc -l`
CRIT04=`$GREP "Final Vcc" $log | grep "2.4192278672014" | wc -l`
CRIT05=`$GREP "Final Vcc" $log | grep "3.4704430421324" | wc -l`
CRIT06=`$GREP "Final Vcc" $log | grep "2.9294039229536" | wc -l`

TEST[1]=`expr \
$CRIT01 \
\+ $CRIT02 \
\+ $CRIT03 \
\+ $CRIT04 \
\+ $CRIT05 \
\+ $CRIT06 \
`
CTRL[1]=6
ERROR[1]="VCC ENERGIES NOT CORRECT"
echo $CRIT01
echo $CRIT02
echo $CRIT03
echo $CRIT04
echo $CRIT05
echo $CRIT06

CRIT11=`$GREP "Vci_Root" $log | grep "9.916709323589[6,7]" | wc -l`
CRIT12=`$GREP "Vci_Root" $log | grep "1.5180388309250" | wc -l`
CRIT13=`$GREP "Vci_Root" $log | grep "2.0446245193639" | wc -l`
CRIT14=`$GREP "Vci_Root" $log | grep "2.4192278672014" | wc -l`
CRIT15=`$GREP "Vci_Root" $log | grep "3.4704430421324" | wc -l`
CRIT16=`$GREP "Vci_Root" $log | grep "2.9294039229536" | wc -l`
TEST[2]=`expr \
$CRIT11 \
\+ $CRIT12 \
\+ $CRIT13 \
\+ $CRIT14 \
\+ $CRIT15 \
\+ $CRIT16 \
`
CTRL[2]=6
ERROR[2]="VCI ENERGIES NOT CORRECT"
echo $CRIT11
echo $CRIT12
echo $CRIT13
echo $CRIT14
echo $CRIT15
echo $CRIT16

CRIT31=`$GREP "VCC" $log | grep "5.263678985661" | wc -l`
CRIT32=`$GREP "VCC" $log | grep "1.052953587004" | wc -l`
CRIT33=`$GREP "VCC" $log | grep "1.427556934842" | wc -l`
CRIT34=`$GREP "VCC" $log | grep "1.937732990594" | wc -l`
CRIT35=`$GREP "VCC" $log | grep "2.478772109773" | wc -l`
TEST[3]=`expr \
$CRIT31 \
\+ $CRIT32 \
\+ $CRIT33 \
\+ $CRIT34 \
\+ $CRIT35 \
`
CTRL[3]=5
ERROR[3]="VCC EXCITATION ENERGIES NOT CORRECT"
echo $CRIT31
echo $CRIT32
echo $CRIT33
echo $CRIT34
echo $CRIT35

PASSED=1
for i in 1 2 3
do 
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > vcc_2db.check
#######################################################################
