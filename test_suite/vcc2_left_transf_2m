#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         H2O as in ir_h2o_vci. test 
   Wave Function:    ??? 
   Test Purpose:     Check VCC[2] response properties 
   Additional:       Properties are calculated using Dalton, DFT/B3LYP
                     and the aug-cc-pVTZ basis.
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > vcc2_left_transf_2m.minp <<%EOF%

#0 MIDAS Input 

// General input 

#1 general
#2 IoLevel
    5
#2 Time

// Vibrational input
#1 Vib

// Operator input
#2 Operator
   #3 Name
      h0
   #3 OperFile
      ir_h2o_vci_h0.mop
   #3 SetInfo
      type=energy

// Dipole operators
#2 Operator    // This is operator input
   #3 Name
       dipole_x
   #3 OperFile
       ir_h2o_vci_x.mop
   #3 SetInfo
       type=dipole_x
#2 Operator    // This is operator input
   #3 Name
       dipole_y
   #3 OperFile
       ir_h2o_vci_y.mop
   #3 SetInfo
       type=dipole_y
#2 Operator    // This is operator input
   #3 Name
       dipole_z
   #3 OperFile
       ir_h2o_vci_z.mop
   #3 SetInfo
       type=dipole_z

#2 Basis     // This is basis input
   #3 Name
   basis_gauss
   #3 PrimBasisDensity
   0.8
   #3 GaussianBasis
   0.9
   #3 IoLevel
   6
   #3ScalBounds
   1.5 40

#2 Vscf      // This a the VSCF input
    #3 Restart
    #3 IoLevel
        1
    #3 Name 
        scf0
    #3 Oper
        h0
    #3 Basis
        basis_gauss
    #3 OccGroundState
    #3 Threshold
        1.0e-15
    #3 MaxIter
        40

#2 Vcc
    #3 Restart
    #3 Vcc
    #3 Transformer
      trf=2m
    #3 VscfThreshold
      1.0e-15
    #3 LimitModalBasis
      [3*3]
    #3 MaxExci
      2
    #3 ItEqResidThr
      1.0e-15
    #3 Oper
      h0
    #3 Basis 
      basis_gauss 
    #3 VecStorage 
      0
    #3 ItEqMaxDim
      200
    #3 ItEqMaxIt
      150
    #3 ItEqBreakDim
      200
    #3 TimeIt
    #3 IoLevel
        11
    #3 Rsp
        #4 TimeIt
        #4 DiagMeth
         LAPACK
        #4 IoLevel
            3
        #4 Eigen val
            11
        #4 ItEqResidThr
            1.0e-15
        #4 LambdaThreshold
            1.0e-15
        #4 ItEqMaxIt
            100
        #4 ItEqMaxDim
            200
        #4 ItEqBreakDim
            150
        #4 RspFunc
             1 h0
             1 dipole_x
             1 dipole_x NO2NP2
             1 dipole_y
             1 dipole_y NO2NP2
             1 dipole_z
             1 dipole_z NO2NP2

#0 Midas Input End
%EOF%

# Now we define all the operators...

cat > ir_h2o_vci_h0.mop <<%EOF%
DALTON_FOR_MIDAS
-1.3983481039758772000000E-11    1     
 1.5756874643102492000000E-04    1     1     
-1.3022827261011116000000E-10    1     1     1     
 4.6707384626643034000000E-08    1     1     1     1     
 4.5483926669476205000000E-07    2     
 1.4937195226139011000000E-04    2     2     
 3.0827675345790340000000E-06    2     2     2     
 4.3788077164208516000000E-08    2     2     2     2     
 3.7033530020380567000000E-08    3     
 2.7465351735145305000000E-05    3     3     
 1.4056156771857786000000E-07    3     3     3     
-5.4826898576720851000000E-10    3     3     3     3     
 2.2424728740588762000000E-10    1     2     
 1.3244516594568267000000E-10    1     2     2     
 1.0004441719502211000000E-11    1     2     2     2     
 9.5331590728164883000000E-06    1     1     2     
 2.7097757993033156000000E-07    1     1     2     2     
-4.0927261579781771000000E-12    1     1     1     2     
 1.6018475434975699000000E-10    1     3     
 1.0061285138363019000000E-11    1     3     3     
-1.2647660696529783000000E-12    1     3     3     3     
-9.1266440449544461000000E-07    1     1     3     
-5.4972815632936545000000E-08    1     1     3     3     
-7.3896444519050419000000E-13    1     1     1     3     
-2.8100544113840442000000E-10    2     3     
-7.2744757062537246000000E-07    2     3     3     
 9.8503392109705601000000E-09    2     3     3     3     
-2.2016263301338768000000E-07    2     2     3     
-4.5356273403740488000000E-08    2     2     3     3     
-9.0807930064329412000000E-09    2     2     2     3     
%EOF%

cat > ir_h2o_vci_x.mop <<%EOF%
DALTON_FOR_MIDAS
 5.9466261933083017000000E-03    1     
-6.9084653969708754000000E-10    1     1     
 1.6358854797345057000000E-07    1     1     1     
-3.8139283398130885000000E-12    1     1     1     1     
 8.1519183905138090000000E-08    2     
 7.6635677157709771000000E-10    2     2     
-1.1814304487513459000000E-11    2     2     2     
 1.6793121081384116000000E-12    2     2     2     2     
 3.6421274585127040000000E-09    3     
-7.4582181456856300000000E-11    3     3     
 9.6729966832230374000000E-13    3     3     3     
-6.4918922320864864000000E-13    3     3     3     3     
 5.5929864454224050000000E-05    1     2     
-9.6813214170085593000000E-07    1     2     2     
-1.2677817016679138000000E-08    1     2     2     2     
 7.2171140591548166000000E-11    1     1     2     
-8.8380136542554055000000E-12    1     1     2     2     
-9.2691439454450908000000E-09    1     1     1     2     
-1.2251871231805595000000E-04    1     3     
 1.1458751519493118000000E-06    1     3     3     
 3.6132391740065217000000E-10    1     3     3     3     
 1.2171142566019810000000E-11    1     1     3     
-9.7285929312462827000000E-12    1     1     3     3     
 7.7715807799555447000000E-09    1     1     1     3     
-1.6425522658677930000000E-09    2     3     
 1.5017041558612033000000E-11    2     3     3     
-1.3801961458477968000000E-12    2     3     3     3     
-9.8233102244450509000000E-12    2     2     3     
 1.3196157239395726000000E-13    2     2     3     3     
-1.6974224867611291000000E-12    2     2     2     3     
%EOF%

cat > ir_h2o_vci_y.mop <<%EOF%
DALTON_FOR_MIDAS
 8.8094570600241383000000E-15    1     
 2.0579515215545181000000E-14    1     1     
-3.9730892762765628000000E-14    1     1     1     
-6.7385565528687044000000E-14    1     1     1     1     
 3.1999408614138801000000E-16    2     
 3.8403586889375180000000E-14    2     2     
-3.2574694064581650000000E-15    2     2     2     
-1.0560113345207904000000E-13    2     2     2     2     
-1.5814514173919275000000E-15    3     
 1.7735807639239755000000E-14    3     3     
 2.2478608095957593000000E-15    3     3     3     
-1.8324910770300230000000E-14    3     3     3     3     
 2.1536863258180361000000E-15    1     2     
-7.3421903241223336000000E-14    1     2     2     
 1.1465895712826050000000E-14    1     2     2     2     
-1.4158683383691875000000E-14    1     1     2     
-4.6517856051405650000000E-13    1     1     2     2     
 2.0717942875917686000000E-14    1     1     1     2     
 2.2390977737264117000000E-15    1     3     
-5.0789583579486514000000E-14    1     3     3     
-2.4611069414207930000000E-15    1     3     3     3     
 2.2172343451965871000000E-14    1     1     3     
-2.3422196280492047000000E-13    1     1     3     3     
-5.5815443638190166000000E-14    1     1     1     3     
 2.0483972203442721000000E-14    2     3     
 3.0939562954175324000000E-14    2     3     3     
-3.2169454769875466000000E-14    2     3     3     3     
 1.3051627728937089000000E-14    2     2     3     
-1.2593085685454386000000E-13    2     2     3     3     
-9.8224815858746958000000E-14    2     2     2     3     
%EOF%

cat > ir_h2o_vci_z.mop <<%EOF%
DALTON_FOR_MIDAS
 1.8027356940564232000000E-08    1     
 1.6368545080247543000000E-05    1     1     
-5.4192206278003141000000E-12    1     1     1     
 2.8840849708444694000000E-09    1     1     1     1     
-1.6066597706836383000000E-03    2     
-2.4899345155482422000000E-05    2     2     
 1.6070990582761624000000E-07    2     2     2     
 6.6033383205876817000000E-09    2     2     2     2     
 6.5273750308395551000000E-03    3     
-3.3700807905656660000000E-05    3     3     
 2.6208677517125523000000E-07    3     3     3     
-1.0468585998069102000000E-09    3     3     3     3     
 1.1107390562870023000000E-09    1     2     
 1.3325340830760979000000E-11    1     2     2     
 2.4709123636057484000000E-12    1     2     2     2     
 5.3277693101705381000000E-07    1     1     2     
 1.5315734458454244000000E-08    1     1     2     2     
 2.1556090246122039000000E-12    1     1     1     2     
-3.9093883685836772000000E-10    1     3     
 6.1719518384961702000000E-12    1     3     3     
-5.3135273958559992000000E-13    1     3     3     3     
-3.8460020412856011000000E-07    1     1     3     
 3.0578206633435911000000E-10    1     1     3     3     
 1.0329515021112456000000E-12    1     1     1     3     
 3.3004144652126755000000E-05    2     3     
-4.5190854036647465000000E-07    2     3     3     
 1.4223994870832257000000E-08    2     3     3     3     
 5.2844697506770899000000E-07    2     2     3     
-9.6636032509422876000000E-09    2     2     3     3     
-4.1491672320148609000000E-09    2     2     2     3     
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep "
fi

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")

# ------------------------------------------------------------------------------
# Check energy for each iteration.
# ------------------------------------------------------------------------------
# grep "Iter: [[:digit:]]* Total energy" $log
CRIT_ITER_E[0]=$($GREP -c "Iter: 1 Total energy : *2.098463386579....E-02" $log)
CRIT_ITER_E[1]=$($GREP -c "Iter: 2 Total energy : *2.098866787530....E-02" $log)
CRIT_ITER_E[2]=$($GREP -c "Iter: 3 Total energy : *2.098644360925....E-02" $log)
CRIT_ITER_E[3]=$($GREP -c "Iter: 4 Total energy : *2.098672307923....E-02" $log)
CRIT_ITER_E[4]=$($GREP -c "Iter: 5 Total energy : *2.098662715620....E-02" $log)
CRIT_ITER_E[5]=$($GREP -c "Iter: 6 Total energy : *2.098664296579....E-02" $log)
CRIT_ITER_E[6]=$($GREP -c "Iter: 7 Total energy : *2.098663853241....E-02" $log)
CRIT_ITER_E[7]=$($GREP -c "Iter: 8 Total energy : *2.098663936753....E-02" $log)
CRIT_ITER_E[8]=$($GREP -c "Iter: 9 Total energy : *2.098663915617....E-02" $log)
CRIT_ITER_E[9]=$($GREP -c "Iter: 10 Total energy : *2.098663919897....E-02" $log)
CRIT_ITER_E[10]=$($GREP -c "Iter: 11 Total energy : *2.098663918873....E-02" $log)
CRIT_ITER_E[11]=$($GREP -c "Iter: 12 Total energy : *2.098663919089....E-02" $log)
CRIT_ITER_E[12]=$($GREP -c "Iter: 13 Total energy : *2.098663919039....E-02" $log)
CRIT_ITER_E[13]=$($GREP -c "Iter: 14 Total energy : *2.098663919050....E-02" $log)
CRIT_ITER_E[14]=$($GREP -c "Iter: 15 Total energy : *2.098663919047....E-02" $log)
CRIT_ITER_E[15]=$($GREP -c "Iter: 16 Total energy : *2.098663919048....E-02" $log)
CRIT_ITER_E[16]=$($GREP -c "Iter: 17 Total energy : *2.098663919048....E-02" $log)
CRIT_ITER_E[17]=$($GREP -c "Iter: 18 Total energy : *2.098663919048....E-02" $log)
CRIT_ITER_E[18]=$($GREP -c "Iter: 19 Total energy : *2.098663919048....E-02" $log)

for i in $(seq 0 18)
do
   TEST+=(${CRIT_ITER_E[$i]})
   CTRL+=(1)
   iter=$(expr $i + 1)
   ERROR+=("Wrong '\''Iter: $iter Total energy'\'', CRIT_ITER_E[$i].")
done


# ------------------------------------------------------------------------------
# Check convergence and final VCC energy.
# ------------------------------------------------------------------------------
TEST+=($($GREP "Nonlinear equations are converged in [[:digit:]]* iterations" $log | awk '\''{print $6}'\''))
CTRL+=(19)
ERROR+=("Wrong number of VCC iterations (or failed to converge).")

TEST+=($($GREP -c "Final Vcc energy *2\.098663919048....E-02 *a\.u\." $log))
CTRL+=(1)
ERROR+=("VCC energy not correct")

# ------------------------------------------------------------------------------
# Check convergence and VCC rsp. energies
# ------------------------------------------------------------------------------
TEST+=($($GREP "Equations are converged in [[:digit:]]* iterations" $log | awk '\''{print $5}'\''))
CTRL+=(2)
ERROR+=("Wrong number of (right-hand) response iterations.")

CRIT_RSP_E[0]=$($GREP -c "Rsp_Root_0 EigenValue *7\.08539926438.....E-03 au" $log)
CRIT_RSP_E[1]=$($GREP -c "Rsp_Root_1 EigenValue *1\.40170250083.....E-02 au" $log)
CRIT_RSP_E[2]=$($GREP -c "Rsp_Root_2 EigenValue *1\.67360848058.....E-02 au" $log)
CRIT_RSP_E[3]=$($GREP -c "Rsp_Root_3 EigenValue *1\.72802479177.....E-02 au" $log)
CRIT_RSP_E[4]=$($GREP -c "Rsp_Root_4 EigenValue *2\.36719572031.....E-02 au" $log)
CRIT_RSP_E[5]=$($GREP -c "Rsp_Root_5 EigenValue *2\.44003109883.....E-02 au" $log)
CRIT_RSP_E[6]=$($GREP -c "Rsp_Root_6 EigenValue *3\.03968900365.....E-02 au" $log)
CRIT_RSP_E[7]=$($GREP -c "Rsp_Root_7 EigenValue *3\.10611133936.....E-02 au" $log)
CRIT_RSP_E[8]=$($GREP -c "Rsp_Root_8 EigenValue *3\.34945698772.....E-02 au" $log)
CRIT_RSP_E[9]=$($GREP -c "Rsp_Root_9 EigenValue *3\.44830110886.....E-02 au" $log)
CRIT_RSP_E[10]=$($GREP -c "Rsp_Root_10 EigenValue *3\.44947222683.....E-02 au" $log)

for i in $(seq 0 10)
do
   TEST+=($(expr ${CRIT_RSP_E[$i]}))
   CTRL+=(1)
   ERROR+=("Wrong '\''Rsp_Root_$i EigenValue'\'', CRIT_RSP_E[$i].")
done

# ------------------------------------------------------------------------------
# Dipole expectation values
# ------------------------------------------------------------------------------
CRIT_DIPEXP_A[0]=$($GREP -c "Order: 1  <dipole_x> = *-1.9092529.........E-08 *$" $log)
CRIT_DIPEXP_A[1]=$($GREP -c "Order: 1  <dipole_y> = *-1.998839..........E-09 *$" $log)
CRIT_DIPEXP_A[2]=$($GREP -c "Order: 1  <dipole_z> = *4.5996960296......E-04 *$" $log)

for i in $(seq 0 2)
do
   TEST+=($(expr ${CRIT_DIPEXP_A[$i]}))
   CTRL+=(1)
   ERROR+=("Wrong expectation value (with 2n+2), CRIT_DIPEXP_A[$i].")
done

CRIT_DIPEXP_B[0]=$($GREP -c "Order: 1  <dipole_x> = *-1.9092529.........E-08 *No 2n\+2 *$" $log)
CRIT_DIPEXP_B[1]=$($GREP -c "Order: 1  <dipole_y> = *-1.998839..........E-09 *No 2n\+2 *$" $log)
CRIT_DIPEXP_B[2]=$($GREP -c "Order: 1  <dipole_z> = *4.5996960296......E-04 *No 2n\+2 *$" $log)

for i in $(seq 0 2)
do
   TEST+=($(expr ${CRIT_DIPEXP_B[$i]}))
   CTRL+=(1)
   ERROR+=("Wrong expectation value (no 2n+2), CRIT_DIPEXP_B[$i].")
done

# ------------------------------------------------------------------------------
# Check transformer calls
# ------------------------------------------------------------------------------

# 1 pre.iter., 19 iters., 4 operators in response.
TEST+=($($GREP -c "Entering Vcc2TransReg<Nb>::Transform" $log))
CTRL+=(24)
ERROR+=("Wrong number of '\''Entering Vcc2TransReg<Nb>::Transform'\''.")

# Seems like the R-hand Jacobian eig.prob. is solved twice, first for finding
# eigenvalues, then when doing response. :S 18 iters. for each. -MBH, Apr 2019. 
TEST+=($($GREP -c "Entering Vcc2TransRJac<Nb>::Transform" $log))
CTRL+=(36)
ERROR+=("Wrong number of '\''Entering Vcc2TransRJac<Nb>::Transform'\''.")

TEST+=($($GREP -c "Entering Vcc2TransEta<Nb>::Transform" $log))
CTRL+=(1)
ERROR+=("Wrong number of '\''Entering Vcc2TransEta<Nb>::Transform'\''.")

TEST+=($($GREP -c "Entering Vcc2TransLJac<Nb>::Transform" $log))
CTRL+=(10)
ERROR+=("Wrong number of '\''Entering Vcc2TransLJac<Nb>::Transform'\''.")

# Done each time constructing R- or L-Jac. transformer.
TEST+=($($GREP -c "Vcc2TransFixedAmps<Nb>; T1-transforming integrals" $log))
CTRL+=(3)
ERROR+=("Wrong number of '\''Vcc2TransFixedAmps<Nb>; T1-transforming integrals'\''.")

# Done each time constructing R-Jac. transformer (which is twice, see remark above).
TEST+=($($GREP -c "Vcc2TransRJac<Nb>; computing T-based intermediates" $log))
CTRL+=(2)
ERROR+=("Wrong number of '\''Vcc2TransRJac<Nb>; computing T-based intermediates'\''.")

# Done once when constructing L-Jac. transformer.
TEST+=($($GREP -c "Vcc2TransLJac<Nb>; computing T-based intermediates" $log))
CTRL+=(1)
ERROR+=("Wrong number of '\''Vcc2TransLJac<Nb>; computing T-based intermediates'\''.")

# Done at every L-Jac. iteration.
TEST+=($($GREP -c "Vcc2TransLJac<Nb>; computing L-based intermediates" $log))
CTRL+=(10)
ERROR+=("Wrong number of '\''Vcc2TransLJac<Nb>; computing L-based intermediates'\''.")

# ------------------------------------------------------------------------------
# Check against controls.
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do 
   if test ${TEST[$i]} -ne ${CTRL[$i]}
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi
' > vcc2_left_transf_2m.check
#######################################################################
