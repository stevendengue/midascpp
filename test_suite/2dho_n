#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         2-dim ho. test 
   Wave Function:    VSCF / 8 8 HO basis 
   Test Purpose:     Check VSCF energy of direct calculated lowest states
   Reference:        Thompson & Truhlar, CPL 75, 87 (1980)
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 2dho_n.minp <<%EOF%

#0 MIDAS Input     // This indicates start of Midas input from here

/*
Now comes the real input 
*/

// This the general input 

#1 general
#2 IoLevel
    5
!#2 Debug


// Now comes the section of the vibrational input
#1 Vib
#2 IoLevel
  11

#2 Operator    // This is operator input
#3 Name
 testoper
#3operinput
#4operatorterms
-0.5 DDQ^2(a)
-0.5 DDQ^2(b)
0.98 Q^2(a)
0.245 Q^2(b)
0.0064 Q^3(a)
-0.08 Q^1(a) Q^2(b)
#3 KineticEnergy
 USER_DEFINED

#2 Basis     // This is basis input
#3 Name
 basis
#3 Define
 a HO n_high=8,omeg=1.4
 b HO n_high=8,omeg=0.7
#3 IoLevel
   1

#2 Vscf      // This a the VSCF input
#3 IoLevel
 10
!#3 NMODES
! 2
#3 OccGroundState 
!#3 Fundamentals 
! 0-1
!#3 OccALLFund
#3 OccGenCombi
 0^0
 0^1
 0^2
 0^3
 0^4
 0^5
 1^1
 1^2
 1^3
 1^4
 1^5
#3 OccGenCombi
 0^1 1^1
 0^1 1^2
 0^1 1^3
 0^1 1^4
 0^1 1^5
 0^2 1^1
 0^2 1^2
 0^2 1^3
 0^2 1^4
 0^2 1^5

!#3 OccALLFirstover
!#3 OccFirstover
! 0-1
!#3 Occup
! 0 0 
!#3 OCCUmax
! 2 5 
!#3 OccMaxSumN
! 5
!#3 OccMaxExci
! 2
#3 Threshold
 1.0e-12
#3 MaxIter
 20
!#3 OnDisc


/*
Input is over - just a little bit of more confusing comments.
*/

#0 Midas Input End              // End comment

Now it is no more input.
And I can write a long story here without effect.

%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Basic logic: integrals for two modes, 15 Vscf calcs,omeg input in each basis.
CRIT1=`$GREP "Do Vscf:" $log | wc -l`
CRIT2=`$GREP "Harmonic oscillator integrals calculated for mode " $log | wc -l`
CRIT3=`$GREP "n_high=8,omeg=1.4" $log | wc -l`
CRIT4=`$GREP "n_high=8,omeg=0.7" $log | wc -l`
TEST[1]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4`
CTRL[1]=18
ERROR[1]="INPUT/INTEGRAL PROCESSING NOT CORRECT"

# SCF energy:
CRIT1=`$GREP "Occ: \[0,0\]" $log | $GREP "E =  1\.0493492614924[0-9][0-9]" | wc -l`
CRIT2=`$GREP "Occ: \[0,1\]" $log | $GREP "E =  1\.7429904052909[0-9][0-9]" | wc -l`
CRIT3=`$GREP "Occ: \[0,2\]" $log | $GREP "E =  2\.4296836007176[0-9][0-9]" | wc -l`
CRIT4=`$GREP "Occ: \[1,0\]" $log | $GREP "E =  2\.4496704954869[0-9][0-9]" | wc -l`
CRIT5=`$GREP "Occ: \[0,3\]" $log | $GREP "E =  3\.1092154093604[0-9][0-9]" | wc -l`
CRIT6=`$GREP "Occ: \[1,1\]" $log | $GREP "E =  3\.1441249282984[0-9][0-9]" | wc -l`
CRIT7=`$GREP "Occ: \[0,4\]" $log | $GREP "E =  3\.7813535099924[0-9][0-9]" | wc -l`
CRIT8=`$GREP "Occ: \[1,2\]" $log | $GREP "E =  3\.8316468350879[0-9][0-9]" | wc -l`
CRIT9=`$GREP "Occ: \[2,0\]" $log | $GREP "E =  3\.8499113594583[0-9][0-9]" | wc -l`
CRIT10=`$GREP "Occ: \[0,5\]" $log | $GREP "E =  4\.4459300371806[0-9][0-9]" | wc -l`
CRIT11=`$GREP "Occ: \[1,3\]" $log | $GREP "E =  4\.5120240417778[0-9][0-9]" | wc -l`
CRIT12=`$GREP "Occ: \[2,1\]" $log | $GREP "E =  4\.5451782900198[0-9][0-9]" | wc -l`
CRIT13=`$GREP "Occ: \[1,4\]" $log | $GREP "E =  5\.1850256725133[0-9][0-9]" | wc -l`
CRIT14=`$GREP "Occ: \[2,2\]" $log | $GREP "E =  5\.2335280439966[0-9][0-9]" | wc -l`
CRIT15=`$GREP "Occ: \[2,3\]" $log | $GREP "E =  5\.9147497010704[0-9][0-9]" | wc -l`


TEST[2]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ $CRIT7 \+ $CRIT8 \+ $CRIT9 \+ $CRIT10 \+ $CRIT11 \+ $CRIT12 \+ $CRIT13 \+ $CRIT14 \+ $CRIT15`
CTRL[2]=15
ERROR[2]="VSCF ENERGIES NOT CORRECT"

PASSED=1
for i in 2 
do 
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > 2dho_n.check
#######################################################################
