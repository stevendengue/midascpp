#!/bin/bash
#


NAT=`head -1 midasifc.xyz_input`
echo " number of atoms" $NAT

cp  midasifc.xyz_input midasifc.cartrot_xyz
cat midasifc.xyz_input | tail -$NAT > midasmop.inp

potx -u aa midasmop.inp > pot.out

energy=`awk '{print $3}' pot.out` 
echo "GROUND_STATE_ENERGY     " $energy > midasifc.prop_general

