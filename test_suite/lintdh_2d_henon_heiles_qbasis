#!/bin/sh
#
# This is the script for generating files for a specific Midas test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   energy_direct
   -------------
   Molecule:         2D Henon-Heiles potential
   Wave Function:    L-TDH
   Test Purpose:     Propagate VSCF wave function, calculate properties, and check energy.
                     Check that transformation to q-operator spectral basis works.
   Reference:        
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > lintdh_2d_henon_heiles_qbasis.minp <<%EOF%

#0 MIDAS Input     // This indicates start of Midas input from here

#1 General

#1 Vib
#2 IOLevel
 5

#2 Operator
#3 IoLevel
14
#3 Name
 vscfoper
#3 Operinput
#4 Operatorterms
 -0.5 DDQ^2(Q0)
 0.5 Q^2(Q0)
 -1.0 Q^1(Q0)
 -0.5 DDQ^2(Q1)
 0.5 Q^2(Q1)
 -1.0 Q^1(Q1)
#3 KineticEnergy
   USER_DEFINED
#3 SetInfo
   type=energy

#2 Operator
#3 Name
 tdhoper
#3 Operinput
#4 Operatorterms
 -0.5 DDQ^2(Q0)
 0.5 Q^2(Q0)
 -0.5 DDQ^2(Q1)
 0.5 Q^2(Q1)
 0.111803 Q^2(Q0) Q^1(Q1)
 -0.0372676666667 Q^3(Q1)
#3 KineticEnergy
   USER_DEFINED

// q operator
#2 Operator
#3 Name
   q
#3 OperInput
#4 OperatorTerms
 1.0 Q^1(Q0)
 1.0 Q^1(Q1)

// i*p operator
#2 Operator
#3 Name
   ip
#3 OperInput
#4 OperatorTerms
 1.0 DDQ^1(Q0)
 1.0 DDQ^1(Q1)

#2 Basis
#3 Name
 testbasis
#3 HoBasis
   30

#2 vscf
#3 Name
 VSCF-testoper
#3 Basis
 testbasis
#3 oper
 vscfoper

// Do TDH
#2 TdH
#3 IoLevel
   5
#3 LinearTdH       // Linear parametrization (L-TDH)
#3 SpectralBasis
   q
#3 Name
 TDH-testoper
#3 Basis
 testbasis
#3 Oper
 tdhoper
#3 Integrator
   #4 IoLevel
      1
   #4 Type
      MIDAS DOPR853
   #4 TimeInterval
      0. 1.e2
   #4 OutputPoints
      1000
   #4 Tolerance
      1.e-7 1.e-7
   #4 MaxSteps
      -1
#3 Properties           // Properties to calculate
   energy
   phase
   halftimeautocorr
   kappanorm
#3 Spectra
   halftimeautocorr
#3 SpectrumEnergyShift
   E0
#3 SpectrumOutputScreening
   1.e-5
#3 ExpectationValues    // Expectation values of operators
   q ip

#0 Midas Input End

%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy:
CRIT1=`$GREP "t = 0\.0000000000000000E\+00" $log | $GREP "E = \(2\.074535" | wc -l`
CRIT2=`$GREP "t = 1\.0000000000000000E\+02" $log | $GREP "E = \(2\.074535" | wc -l`

#echo $CRIT1
#echo $CRIT2

TEST[1]=`expr $CRIT1 \+ $CRIT2`
CTRL[1]=2
ERROR[1]="TDH ENERGY NOT CORRECT! "

PASSED=1
for i in 1
do 
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > lintdh_2d_henon_heiles_qbasis.check
#######################################################################
