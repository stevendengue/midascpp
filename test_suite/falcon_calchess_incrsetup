#!/bin/sh
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > falcon_calchess_incrsetup.info <<%EOF%
   -------------
   Molecule:         glycine
   Wave Function:     
   Test Purpose:     Calculate semi-numerical Hessian using ORCA
                     and generate fragment combinations with
                     Falcon algorithm  
   -------------
%EOF%

#######################################################################
#  Setup directories
#######################################################################
export MAINDIR=$PWD/Dir_falcon_calchess_incrsetup
export SETUPDIR=$MAINDIR/setup
export INTERFACEDIR=$MAINDIR/InterfaceFiles
rm -rf $MAINDIR

mkdir $MAINDIR
mkdir $INTERFACEDIR

#######################################################################
#  Define interface file names
#######################################################################

ESPINPUT=InputCreatorScript.sh

ESPRUN=RunScript.sh

PROPINFO=midasifc.propinfo

#######################################################################
#  Midascpp Input
#######################################################################
cat > falcon_calchess_incrsetup.minp <<%EOF%
#0 MidasInput

#1 General
#2 IoLevel
5
#2 MainDir
$MAINDIR

#1 Singlepoint
#2 Name
sp
#2 Type
SP_GENERIC
#2 InputCreatorScript
$ESPINPUT
#2 RunScript
$ESPRUN
#2 SetupDir
$SETUPDIR
#2 PropertyInfo
$PROPINFO

#1 System
#2 SysDef
#3 Moleculefile
 Midas
 Molecule.mmol
#3 Name
 glycine
#3 CalcModes
 Cart
#2 ModSys
#3 Sysnames
 1
 glycine
#3 ModVibCoord
 Falcon
#4 COUPLING
 Dist
#4 MODEOPTTYPE
 HESSIAN
#4 HESSTYPE
 CALC
#5 SinglePointName
sp
#4 Degeneracy
 0.001e0
#4 MAXSUBSYSFORRELAX
 1
#4 WriteIncrInput
 2  1e9 4e0

#0 MidasInputEnd
%EOF%

#######################################################################
#  Molecule input file                                               #
#######################################################################
cat > falcon_calchess_incrsetup.mmol <<%EOF%
#0MOLECULEINPUT

#1XYZ
10 AU
xyz coordinates output from Midas
N  1.500142198175E-03  1.138791900482E-01  3.019520867263E-01 ISO=14 SUB=11 TREAT=ACTIVE
C  2.761752081897E+00  1.040331142213E-02  1.229261114081E-01 ISO=12 SUB=12 TREAT=ACTIVE
C  3.938357030040E+00  9.723966754994E-02  2.870462643369E+00 ISO=12 SUB=13 TREAT=ACTIVE
O  6.120472538015E+00 -3.447167931476E-01  3.328822015877E+00 ISO=16 SUB=13 TREAT=ACTIVE
H  3.490215453738E+00 -1.664519880583E+00 -8.405059347128E-01 ISO=1 SUB=12 TREAT=ACTIVE
H  3.473855600869E+00  1.658676642036E+00 -8.945750949981E-01 ISO=1 SUB=12 TREAT=ACTIVE
H -8.295411741031E-01 -1.609368775573E+00  2.734406970263E-01 ISO=1 SUB=11 TREAT=ACTIVE
H -8.150873696459E-01  1.274034728385E+00 -9.794592024720E-01 ISO=1 SUB=11 TREAT=ACTIVE
O  2.222656787839E+00  7.572680860355E-01  4.672172151293E+00 ISO=16 SUB=13 TREAT=ACTIVE
H  5.774752172548E-01  9.556452359233E-01  3.815886567512E+00 ISO=1 SUB=13 TREAT=ACTIVE

#1FREQ
0 cm-1

#1VIBCOORD
AU

#0MOLECULEINPUTEND
%EOF%

#######################################################################
#  ORCA input creator script
#######################################################################
cat > $INTERFACEDIR/$ESPINPUT << %EOF%
#!/bin/bash
#######################################################################
#                                                                     #
#  Interface script between MidasCpp and ORCA                         #
#                                                                     #
#  Electronic structure model: HF-3c                                  #
#                                                                     #
#  Purpose: Create an ORCA input file                                 #
#                                                                     #
#######################################################################

#!/bin/bash
#
NAT=\`head -1 midasifc.xyz_input\`
echo " number of atoms" \$NAT

echo "! HF-3c TightSCF ENGRAD" > orca.inp
echo "%SCF" >> orca.inp
echo "MAXITER 500" >> orca.inp
echo "end" >> orca.inp
echo "* xyz 0 1" >> orca.inp
cat midasifc.xyz_input | tail -\$NAT >> orca.inp
echo "END" >> orca.inp
%EOF%

#######################################################################
#  ORCA input run script
#######################################################################
cat > $INTERFACEDIR/$ESPRUN << %EOF%
#!/bin/bash
#######################################################################
#                                                                     #
#  Interface script between MidasCpp and ORCA                         #
#                                                                     #
#  Electronic structure model: HF-3c                                  #
#                                                                     #
#  Purpose: Run an ORCA input file and extract the final results      #
#                                                                     #
#######################################################################

# Define which properties should be extrated from the electronic structure calculation
create_property_file() 
{
   # Extract the energy
   energy=\`grep "FINAL SINGLE POINT ENERGY     " orca.out | awk '{print \$5}'\`

   NAT=\`grep -2 "\# Number of atoms" orca.engrad | tail -1 \`

   # Store the energy for MidasCpp to read
   echo "GROUND_STATE_ENERGY     " \$energy > midasifc.prop_general
   echo \$NAT > midasifc.my_deriva 
   
   G=\$(expr \$NAT + \$NAT + \$NAT)  
   H=\$(expr \$G + 1) 
   grep -A \$H "# The current gradient in Eh/bohr"  orca.engrad | tail -\$G  | awk '{print \$1}'>> midasifc.my_deriva

   echo \$NAT > midasifc.cartrot_xyz 
   E=\$(expr \$NAT + 2) 
   echo " returned coordinates " >> midasifc.cartrot_xyz
   N=\$(expr \$NAT)
   grep -A \$E "CARTESIAN COORDINATES (A.U.)"  orca.out | tail -\$N  | \
   awk 'BEGIN {bohr=.5291772108; }
        {printf ("%2s      %20.15f  %20.15f  %20.15f \n",\
                        \$2,\$6*bohr,\$7*bohr,\$8*bohr)}
         END { }' >> midasifc.cartrot_xyz 
}

# Change directory to the scratch directory
cd \$1
orca orca.inp > orca.out
create_property_file
%EOF%

#######################################################################
#  MidasCpp property infomation file
#######################################################################
cat > $INTERFACEDIR/$PROPINFO << %EOF%
tens_order=(0),descriptor=(GROUND_STATE_ENERGY),der_file=(midasifc.my_deriva),type=(energy)
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Did midas end correctly?
CRIT0=`$GREP "Midas ended at" $log | wc -l`
TEST[0]=`expr $CRIT0`
CTRL[0]=1
ERROR[0]="MIDAS NOT ENDED CORRECTLY"

# Did we obtain the correct incr_input file?
CRIT0=`$GREP "FC_0 12,     12    3" '"$MAINDIR/FC_savedir/incr_input"' | wc -l` # 1
CRIT1=`$GREP "FC_1 13,     12    3" '"$MAINDIR/FC_savedir/incr_input"' | wc -l` # 1
CRIT2=`$GREP "FC_2 14,     6    3" '"$MAINDIR/FC_savedir/incr_input"' | wc -l` # 1
CRIT3=`$GREP "FC_3 12,13,     6    3" '"$MAINDIR/FC_savedir/incr_input"' | wc -l` # 1
CRIT4=`$GREP "FC_4 12,14,     12    9" '"$MAINDIR/FC_savedir/incr_input"' | wc -l` # 1
CRIT5=`$GREP "FC_5 13,14,     12    9" '"$MAINDIR/FC_savedir/incr_input"' | wc -l` # 1
TEST[1]=`expr $CRIT0 \+ $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5`
CTRL[1]=6
ERROR[1]="DID NOT OBTAIN THE CORRECT INCR_INPUT FILE"


# Did we obtain correct frequencies for generated FCs? Lets check some of them. 
CRIT0=`$GREP "1\.85..........E\+02 A Q6" '"$MAINDIR/FC_savedir/FC_0.mmol"' | wc -l` # 1
CRIT1=`$GREP "3\.62..........E\+02 A Q7" '"$MAINDIR/FC_savedir/FC_0.mmol"' | wc -l` # 1
CRIT2=`$GREP "5\.26..........E\+02 A Q8" '"$MAINDIR/FC_savedir/FC_0.mmol"' | wc -l` # 1
CRIT3=`$GREP "1\.850.........E\+03 A Q23" '"$MAINDIR/FC_savedir/FC_0.mmol"' | wc -l` # 1
CRIT4=`$GREP "3\.543.........E\+03 A Q25" '"$MAINDIR/FC_savedir/FC_1.mmol"' | wc -l` # 1
CRIT5=`$GREP "1\.318.........E\+03 A Q18" '"$MAINDIR/FC_savedir/FC_2.mmol"' | wc -l` # 1
TEST[2]=`expr $CRIT0 \+ $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5`
CTRL[2]=6
ERROR[2]="DID NOT OBTAIN CORRECT FREQUENCIES FOR IC and INTRA COORDINATES"


# Are frequencies for auxiliary coordinates also correct? 
CRIT0=`$GREP "3\.29..........E\+02 A AUX33" '"$MAINDIR/FC_savedir/FC_0_AUX_CAPPED.mmol"' | wc -l` # 1
CRIT1=`$GREP "1\.35..........E\+03 A AUX34" '"$MAINDIR/FC_savedir/FC_1_AUX_CAPPED.mmol"' | wc -l` # 1
CRIT2=`$GREP "7\.71..........E\+02 A AUX35" '"$MAINDIR/FC_savedir/FC_2_AUX_CAPPED.mmol"' | wc -l` # 1
CRIT3=`$GREP "8\.21..........E\+02 A AUX35" '"$MAINDIR/FC_savedir/FC_3_AUX_CAPPED.mmol"' | wc -l` # 1
CRIT4=`$GREP "3\.49..........E\+02 A AUX40" '"$MAINDIR/FC_savedir/FC_4_AUX_CAPPED.mmol"' | wc -l` # 1
CRIT5=`$GREP "5\.87..........E\+02 A AUX41" '"$MAINDIR/FC_savedir/FC_5_AUX_CAPPED.mmol"' | wc -l` # 1
TEST[3]=`expr $CRIT0 \+ $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5`
CTRL[3]=6
ERROR[3]="DID NOT OBTAIN CORRECT FREQUENCIES FOR AUX COORDINATES"


PASSED=1
for i in 0 1 2 3 
do 
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > falcon_calchess_incrsetup.check
