#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   Testing B-spline integrals of delta function using the double delta potential.
   -------------
   Molecule:         One-dimensional system of two delta functions.
   Wave Function:    VSCF
   Test Purpose:     Check energies of double delta function (implicitly B-spline integrals of delta function).
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > delta_function_bsplines.minp <<%EOF%
#0 MIDAS Input
#1 general
   #2 IoLevel
      2 
   #2 Time

#1 Vib
   #2 IoLevel 
      1

#2 Operator
   #3 Name
      delta
   #3 IoLevel
      3 
   #3 OperInput
   #4 OperatorTerms
   -0.5 DDQ^2(Q0)
   -1.0 DELTA(0.0)(Q0)
   #3 KineticEnergy
   USER_DEFINED

#2 Basis 
   #3 Name
      basis
   #3 Define
      Q0 Bsplines leftb=-12.0, rightb=12.0, nbas=999, iord=2
   #3 IoLevel
      7

#2 Vscf
#3 IoLevel
   5
#3 Oper
   delta
#3 Name
   vscf_delta

#0 Midas Input End
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ------------------------------------------------------------------------------
# Has midas ended?
# ------------------------------------------------------------------------------
# The TEST, CTRL, ERROR arrays for doing the checking. Note that they are
# initialized here by filling in the zeroth element, while later on elements
# are appended to them using +=. Empty initialization then appending, i.e.
# array=(); array+=(hello), does not work consistently across ksh, bash and sh.
TEST=($($GREP "Midas ended at" $log | wc -l))
CTRL=(1)
ERROR=("MIDAS NOT ENDED")

## ------------------------------------------------------------------------------
## Check final VSCF energy.
## NB: The exact energy should be -0.5, but that requires a very large B-spline basis 
## as the wave function is of the form exp(-|x|).
## ------------------------------------------------------------------------------
TEST+=(`$GREP "VSCF: vscf_delta_0\.0           E = \-4\.999760022282....E-01" $log |  wc -l`) # 1
CTRL+=(1)
ERROR+=("VSCF ENERGY NOT CORRECT")

# ------------------------------------------------------------------------------
# Check against controls.
# ------------------------------------------------------------------------------
PASSED=1
for i in $(seq 0 $(expr ${#TEST[*]} - 1))
do 
   if test ${TEST[$i]} -ne ${CTRL[$i]}
   then
      echo "${ERROR[$i]} Expected ${CTRL[$i]}, got ${TEST[$i]}. (ERROR[$i])"
      PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > delta_function_bsplines.check
#######################################################################
