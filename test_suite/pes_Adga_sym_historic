#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   Molecule:         water  
   Wave Function:    Partridge potential 
   Test Purpose:     Check Adga procedure with symmetry
%EOF%

#######################################################################
#  SETUP DIRECTORIES
#######################################################################

MAINDIR=$PWD/Dir_pes_Adga_sym_historic
export MAINDIR
rm -rf  $MAINDIR

SAVEDIR=$MAINDIR/savedir
export SAVEDIR

INTERFACEDIR=$MAINDIR/InterfaceFiles
export INTERFACEDIR

mkdir $MAINDIR
mkdir $INTERFACEDIR

#######################################################################
#  MIDAS INPUT
#######################################################################
cat > pes_Adga_sym_historic.minp <<%EOF%
#0 MIDAS Input
#1 general
#2 IoLevel
2
#2 MainDir
$MAINDIR

#1 singlepoint // here i will give the input for a model singlepoint (and test that i can write a comment)
#2 Name
partridge_water_pot
#2 tYpe
SP_MODEL
#2 PartridgePotential

#1 System

#2 SysDef
#3 MoleculeFile
Midas
Molecule.mmol

#1 Pes
#2 historicAdgaPes
#2 historicPESNumSource
#2FITBAS
#3NAME
poly_bas
#3 FitFunctionsMaxOrder
12
#3 FitBasisMaxOrder
12

#2USEFITBASIS
poly_bas
#2 NThreads
4
#2 IoLevel
12 
#2 Multilevelpes
partridge_water_pot 2
#2 AnalyzeStates
[3*3]
#2 AdgaGridInitialDim
2
#2 PesCalcMuTens
#2 AdgaInfo
2
#2 Symmetry
C2v
#2 SCREENOPERCOEF
OFF
1e-2

#2 AdgaRelScalFact
0.5 2
#2 ItVdensThr
1.0e-2
#2 ItResEnThr
1.0e-6
#2POTVALSFROMOPFILE

#1 Vib
#2 Operator    // This is operator input
#3 Name
h0
#3 ScreenZeroTerm
1.0e-24
#3 OperFile
$SAVEDIR/prop_no_1.mop

#2 Basis     // This is basis input
#3 Name
basis_gauss
#3 PrimBasisDensity
0.8
#3 GaussianBasis
1.00
#3 ReadBoundsFromFile
#3 ScalBounds
1.5 20.0

#2 Vscf      // This the Vscf input
#3 IoLevel
2
#3 Name
vscf00
#3 Oper
h0
#3 Basis
basis_gauss
#3 OccGroundState
#3 ScreenZero
0.1E-20
#3 MaxIter
50

#2 Vscf      // This the Vscf input
#3 IoLevel
2
#3 Name
vscf00_splines
#3 Oper
h0
#3 Basis
basis_gauss
#3 OccGroundState
#3 ScreenZero
0.1E-20
#3 MaxIter
50

#2 Vcc      // This the VFCI input
#3 IoLevel
2
#3 Name
vfci00
#3 Oper
h0
#3 Basis
basis_gauss
#3 VscfScreenZero
0.1E-16
#3 LimitModalBasis
[3*8]
#3 Vci
10
#3 MaxExci
3
#3 Reference
#3 AllFundamentals
#3 AllFirstOvertones
//#3 EigValSeq
//5
#3 ItEqResidThr                // Convergence Threshold for the VCC error vector norm 
1.0e-08 // 
#3 ItEqEnerThr                 // Convergence Threshold for VCC energy between two iterations 
1.0e-10 // 
#3 ItEqMaxIt                 // maximum allowed number of interations 
100
#3 ItEqMaxDim                 //maximum allowed dimension of the reduced space
600
#1 Analysis
#0 Midas Input End
%EOF%
#######################################################################
#  NUMDER INPUT 
#######################################################################
cat > pes_Adga_sym_historic.mmol <<%EOF%
#0 MOLECULE INPUT
#1 XYZ
3 Aa
comment
O#1    0.000000000000    0.000000000000      0.0
H#1    0.757390973309    0.586352753497      0.0
H#2   -0.757390973309    0.586352753497      0.0

#1 FREQ
3 cm-1
 1.6494968413962467E+03 A1
 3.8337828001194184E+03 A1
 3.9453552233505602E+03 B2

#1 VIBCOORD
au
#2 COORDINATE
 -2.1856071206933261E-17   -6.8064280142906802E-02   0.0000000000000000E+00
 -4.0955238123917437E-01    5.4025995916333314E-01   0.0000000000000000E+00
  4.0955238123917476E-01    5.4025995916333380E-01   0.0000000000000000E+00
#2 COORDINATE
 -8.8974954529188906E-17    4.8625096195909628E-02   0.0000000000000000E+00
 -5.7328278772467722E-01   -3.8596084656763274E-01   0.0000000000000000E+00
  5.7328278772467856E-01   -3.8596084656763396E-01   0.0000000000000000E+00
#2 COORDINATE
 -6.7575790584075535E-02   -5.4225158422113038E-17   0.0000000000000000E+00
  5.3638134704449603E-01    4.1525288027304541E-01   0.0000000000000000E+00
  5.3638134704449458E-01   -4.1525288027304452E-01   0.0000000000000000E+00
#0 MOLECULE INPUT END
%EOF%

#######################################################################
#  MidasCpp property infomation file
#######################################################################
cat > $INTERFACEDIR/midasifc.propinfo << %EOF%
tens_order=(0),descriptor=(GROUND_STATE_ENERGY)
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Is midas ended?
CRIT0=`$GREP "Midas ended at" $log | wc -l`
TEST[0]=`expr $CRIT0`
CTRL[0]=1
ERROR[0]="MIDAS NOT ENDED"

# Basic logic: basic setup in numder.
CRIT1=`$GREP "Mode Combination Range from default input is 2" $log | wc -l` # 2
CRIT2=`$GREP "Grid initialization for mode comb" $log | wc -l`              # 6
CRIT3=`$GREP "The maximum nr. of iterations is 10" $log | wc -l`            # 1
TEST[1]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[1]=9
ERROR[1]="BASIC SETUP IN NUMDER NOT CORRECT"

# "Grid expansion factor: 4" is 1
# "Mode Combination Range from default input is 2" is 1
# "Grid initialization for mode comb" is 3
# "The maximum nr. of iterations is 10" is 1

# iterations
CRIT4=`$GREP "Extend to the right" $log | wc -l`                                 # 5
CRIT5=`$GREP "Extend to the left" $log | wc -l`                                  # 7
TEST[2]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5`
CTRL[2]=12
ERROR[2]="NUMBER OF CALCULATIONS/ITERATIONS/EXTENSION WRONG"

# Harmonic freqs.
CRIT1=`$GREP "1\.6494064" $log | wc -l`
CRIT2=`$GREP "3\.833783" $log | wc -l`
CRIT3=`$GREP "3\.94535347" $log | wc -l`
TEST[3]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[3]=18
ERROR[3]="CALCULATED HARMONIC FREQS WRONG"

#  vci energies
CRIT1=`$GREP "1\.5793757" $log  | grep re | wc -l`
CRIT2=`$GREP "3\.6478732" $log  | grep re | wc -l`
CRIT3=`$GREP "3\.72188148" $log  | grep re | wc -l`
CRIT4=`$GREP "3\.126283[89]" $log  | grep re | wc -l`
CRIT5=`$GREP "7\.15026600" $log  | grep re | wc -l`
CRIT6=`$GREP "7\.3744990" $log  | grep re | wc -l`
TEST[4]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6`
CTRL[4]=6
ERROR[4]="VCI ENERGIED WRONG"

PASSED=1
for i in 0 1 2 3 4 
do 
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

' > pes_Adga_sym_historic.check
#######################################################################
