#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   Molecule:         Formaldehyd 
   Wave Function:    MOLPRO/MP2 - cc-pVDZ
   Test Purpose:     Check generic framework
%EOF%

#######################################################################
#  Check for cfour existence
#######################################################################
molpro_bin=`grep "^molpro_bin" ES_PROGRAMS | cut -d '=' -f2`
molpro_bin=`echo $molpro_bin | cut -d '"' -f2`

if [ ! -x $molpro_bin/bin/molpro ]; then
   echo $molpro_bin
   echo "molpro_bin path in ES_PROGRAMS MUST be right for this test to run"
   # 132 is TEST script error code!
   exit 132
fi

#######################################################################
#  End of check
#######################################################################

MAINDIR=$PWD/Dir_molpro_mp2_quad_transdichlorethane_sym
export MAINDIR
rm -rf  $MAINDIR

SETUPDIR=$MAINDIR/level_1/setup
export SETUPDIR

SAVEDIR=$MAINDIR/level_1/savedir
export SAVEDIR

ANALYSISDIR=$MAINDIR/analysis
export ANALYSISDIR

TEMPLATEDIR=$PWD/generic/generic_molpro_mp2_quad_transdichlorethane_sym

#######################################################################
#  MOLECULE INPUT
#######################################################################
export MYSAVE=`echo $MAINDIR | sed -e 's/\//\\\\\//g'`
echo $MYSAVE
mkdir $MAINDIR
mkdir $MAINDIR/level_1
mkdir $SAVEDIR
mkdir $SETUPDIR
eval sed -e 's/PWD/$MYSAVE/g' $TEMPLATEDIR/pes.minp > pes_generic_molpro_mp2_quad_transdichlorethane_sym.minp

#######################################################################
#  REQUIRED FILES TO SETUPDIR
#######################################################################
cp $TEMPLATEDIR/midasifc.propinfo $SETUPDIR/.
MOLPRO_BIN=`echo $molpro_bin | sed -e 's/\//\\\\\//g'`
#cfour_basis=`echo $cfour_basis | sed -e 's/\//\\\\\//g'` Python....
#eval sed -e 's/cfour_basis_path/$cfour_basis/g' $TEMPLATEDIR/RunScript.sh > generic/tmpfile #cfour stuff
cp $TEMPLATEDIR/InputCreatorScript.sh $SETUPDIR/.
eval sed -e 's/molpro_dir_path/$MOLPRO_BIN/g' $TEMPLATEDIR/RunScript.sh > $SETUPDIR/RunScript.sh
chmod +x $SETUPDIR/RunScript.sh
chmod +x $SETUPDIR/InputCreatorScript.sh
#######################################################################
#  NUMDER INPUT 
#######################################################################
cp $TEMPLATEDIR/NumDer.mmol pes_generic_molpro_mp2_quad_transdichlorethane_sym.mmol
#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL"
log=\$1
dir=$MAINDIR/savedir/

if [ \`uname\` = Linux ]; then
   GREP=\"egrep -a\"
else
   GREP=\"egrep\"
fi

# Is midas ended?
CRIT0=\`\$GREP \" Midas ended at \" \$log | wc -l\`
TEST[0]=\`expr \$CRIT0\`
CTRL[0]=1
ERROR[0]=\"MIDAS NOT ENDED\"

# Check for correct number of Adga iterations
CRIT1=\`\$GREP \" Adga Iteration: 7  Mode coup.: 1 \" \$log | wc -l\`
TEST[1]=\`expr \$CRIT1\`
CTRL[1]=1
ERROR[1]=\"No. of ADGA iterations not correct\"

# Check for correct number of single points 
CRIT2=\`\$GREP \" Total : 740 \" \$log | wc -l\`
TEST[2]=\`expr \$CRIT2\`
CTRL[2]=1
ERROR[2]=\"No. of single point calculations not correct\"

# Check coefficients in potential
CRIT3=\`\$GREP \"1\.6063178453............E-05 Q\^4\(Q3\)\" \$dir/prop_no_1.mop | wc -l\` 
CRIT4=\`\$GREP \"9\.0202999567............E-03 Q\^2\(Q10\)\" \$dir/prop_no_2.mop | wc -l\` 
CRIT5=\`\$GREP \"4\.5440859686............E-04 Q\^3\(Q0\)\" \$dir/prop_no_3.mop | wc -l\` 
CRIT6=\`\$GREP \"2\.8439470108............E-01 Q\^1\(Q8\)\" \$dir/prop_no_4.mop | wc -l\` 
CRIT7=\`\$GREP \"\-5\.0685725788............E-03 Q\^2\(Q7\)\" \$dir/prop_no_5.mop | wc -l\`
CRIT8=\`\$GREP \"\-3\.2052807803............E-03 Q\^2\(Q4\)\" \$dir/prop_no_10.mop | wc -l\`
TEST[3]=\`expr \$CRIT3 \+ \$CRIT4 \+ \$CRIT5 \+ \$CRIT6 \+ \$CRIT7 \+ \$CRIT8\`
CTRL[3]=6
ERROR[3]=\"SPECIFIC COEFFICIENTS NOT CORRECT\"

PASSED=1
for i in 0 1 2 3
do 
   if [ \${TEST[\$i]} -ne \${CTRL[\$i]} ]; then
     echo \${ERROR[\$i]}\"! \"\${TEST[\$i]}\" should be: \"\${CTRL[\$i]}
     PASSED=0
   fi
done 

if [ \$PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

" > pes_generic_molpro_mp2_quad_transdichlorethane_sym.check
#######################################################################
