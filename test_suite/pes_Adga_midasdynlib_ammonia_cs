#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
elif [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
else
   CHECK_SHELL='#!/bin/sh'
fi
echo "I am using SHELL = $CHECK_SHELL"
#######################################################################
#  Input
#######################################################################
cat > test.info <<%EOF%
   Molecule:             Ammonia
   Electronic Structure: MidasPot (2-mode) from RI-MP2/aug-cc-pVTZ
   Test Purpose:         Check reproducibility of property surfaces,
                         correct handling of symmetry.
%EOF%

CALCNAME=pes_Adga_midasdynlib_ammonia_cs
TEMPLATEDIR=$(pwd)/generic/generic_${CALCNAME}
MIDASIFCPROPINFO=midasifc.propinfo

MIDASDYNLIB=$(pwd)/midasdynlib/ammonia_rimp2_augccpvtz_dynlib.so.1.0
NATOMS=4

export MAINDIR=$(pwd)/Dir_${CALCNAME}
export SAVEDIR=$MAINDIR/savedir
export INTERFACEDIR=$MAINDIR/InterfaceFiles
export SAVEDIR1=$MAINDIR/FinalSurfaces/savedir

#######################################################################
#  DIRECTORIES
#######################################################################
rm -rf  $MAINDIR
mkdir $MAINDIR
mkdir $INTERFACEDIR

#######################################################################
#  REQUIRED FILES TO SAVEDIR
#######################################################################
for file in $MIDASIFCPROPINFO
do
   cp $TEMPLATEDIR/$file $INTERFACEDIR/
done

#######################################################################
#  MOLECULE INFO
#######################################################################
cp $TEMPLATEDIR/${CALCNAME}.mmol ${CALCNAME}.mmol

#######################################################################
#  MIDAS INPUT
#######################################################################
cat > ${CALCNAME}.minp <<%EOF%
#0 MidasInput
#1 General
#2 IoLevel
2
#2 MainDir
$MAINDIR

#1 Singlepoint
#2 Name
midaspot
#2 Type
SP_MODEL
#2 DYNLIBPOT
#2 DYNLIBFILE
$MIDASDYNLIB

#1 System

#2 SysDef
#3 MoleculeFile
Midas
Molecule.mmol

#1 Pes
#2 IoLevel
8

#2 FitBasis
#3 Name
fitbasis
#3 FitFunctionsMaxOrder
10
#3 FitBasisMaxOrder
10 10 10 10
#3 AddFitFuncsConserv // Always uses pol.deg. less than num. SPs.
#3 FitMethod
SVD
#3 SVDFitCutOffThr
1.0e-13

#2 UseFitBasis
fitbasis
#2 AdgaInfo
2
#2 AnalyzeStates
[6*4]
#2 MultiLevelPes
midaspot 2
#2 AdgaGridInitialDim
2
#2 NIterMax
20
#2 AdgaRelScalFact
1.0 3.0 15.0
#2 ItVdensThr
1.0e-2
#2 ItResEnThr
1.0e-6
#2 ItResDensThr
1.0e-3
#2 DynamicAdgaExt
#2 NThreads
0
#2 Symmetry
cs

#1 Vib
#2 Operator
#3 Name
h0
#3 ScreenZeroTerm
1.0e-24
#3 OperFile
prop_no_1.mop
#3 SetInfo
type=energy

#2 Basis
#3 Name
basis_bspline
#3 ReadBoundsFromFile
#3 BsplineBasis
10
#3 PrimBasisDensity
0.8
#3 ScalBounds
1.5 20.0

#2 Vscf
#3 IoLevel
2
#3 Name
vscf_adga
#3 Oper
h0
#3 Basis
basis_bspline
#3 OccGroundState
#3 ScreenZero
1.0e-21
#3 Threshold
1.0e-15
#3 MaxIter
500

#1 Analysis

#0 MidasInputEnd
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Has midas ended?
CRIT0=$($GREP "Midas ended at" $log | wc -l)
TEST[0]=$(expr $CRIT0)
CTRL[0]=1
ERROR[0]="MIDAS NOT ENDED"

# Same number of single point calculations in each iteration as when the
# calculation was originally performed. (I.e. does ADGA run in the same way?)
# Without accounting for symmetry.
# Grepping (first for "Evaulating...", then for "New :" shall yield something
# like
#  |      New : 13                                                        |
#  |      New : 12                                                        |
#  |      New : 32                                                        |
#  ... etc.
# Then use awk to turn the numbers into a string like
#  13 12 32 ... etc.
n_points_test=($($GREP -A3 "Evaluating list of singlepoints" $log | $GREP "New :" | awk '\''{printf("%i ",$4)}'\''))
n_points_ctrl=(13 12 32 56 25 2 60 182 734 1569)
# Accounting for use of symmetry.
# Grepping (first for "Evaulating...", then for "Unique :" shall yield something
# like
#  |   Unique : 11                                                        |
#  |   Unique : 10                                                        |
#  |   Unique : 28                                                        |
#  ... etc.
# Then use awk to turn the numbers into a string like
#  11 10 28 ... etc.
n_points_uniq_test=($($GREP -A3 "Evaluating list of singlepoints" $log | $GREP "Unique :" | awk '\''{printf("%i ",$4)}'\''))
n_points_uniq_ctrl=(11 10 28 50 24 2 42 127 515 1233)

# Then do checks.
TEST[1]=1 # Without symmetry.
TEST[2]=1 # With symmetry.
CTRL[1]=1
CTRL[2]=1
ERROR[1]="SPs in ADGA its. (no sym); expected ${n_points_ctrl[@]}, got ${n_points_test[@]}."
ERROR[2]="SPs in ADGA its. (sym); expected ${n_points_uniq_ctrl[@]}, got ${n_points_uniq_test[@]}."
if test ${#n_points_test[@]} -eq ${#n_points_ctrl[@]}
then
   for((i=0; i < ${#n_points_ctrl[@]}; i++))
   do
      if test ${n_points_test[$i]} -ne ${n_points_ctrl[$i]}
      then
         TEST[1]=0
      fi
   done
else
   TEST[1]=0
fi
if test ${#n_points_uniq_test[@]} -eq ${#n_points_uniq_ctrl[@]}
then
   for((i=0; i < ${#n_points_uniq_ctrl[@]}; i++))
   do
      if test ${n_points_uniq_test[$i]} -ne ${n_points_uniq_ctrl[$i]}
      then
         TEST[2]=0
      fi
   done
else
   TEST[2]=0
fi

# Check distributions of singlepoints over different mode combinations.
# Grepping yields something like:
#  | Adga Iteration: 3  Mode coup.: 2                                     |
#  |    Num. calcs. for MC   (0):  36                                     |
#  |    Num. calcs. for MC   (1):  35                                     |
#       ... etc.
#  |    Num. calcs. for MC (4,5):  64                                     |
# Then awk extracts the numbers from every line but the first one.
n_points_per_mc_test=($($GREP -A21 "Adga Iteration: 3  Mode coup.: 2" $log | awk -F[:\|] '\''(NR!=1){printf("%i ", $3)}'\''))
n_points_per_mc_ctrl=(36 35 28 39 26 32 225 196 208 176 256 64 225 140 182 168 120 240 64 160 64 )
TEST[3]=1
CTRL[3]=1
ERROR[3]="SPs per MC wrong; expected ${n_points_per_mc_ctrl[@]}, got ${n_points_per_mc_test[@]}."
if test ${#n_points_per_mc_test[@]} -eq ${#n_points_per_mc_ctrl[@]}
then
   for((i=0; i < ${#n_points_per_mc_ctrl[@]}; i++))
   do
      if test ${n_points_per_mc_test[$i]} -ne ${n_points_per_mc_ctrl[$i]}
      then
         TEST[3]=0
      fi
   done
else
   TEST[3]=0
fi

# Check VSCF energy.
CRIT40=$($GREP -c "VSCF: vscf_adga_0              E =  3\.41282220180.....E-02 .* au" $log)
TEST[4]=$((CRIT40))
CTRL[4]=1
ERROR[4]="Incorrect final VSCF energy."

FILE_PROP1='"$SAVEDIR1/prop_no_1.mop"' # energy
FILE_PROP2='"$SAVEDIR1/prop_no_2.mop"' # dip_x
FILE_PROP3='"$SAVEDIR1/prop_no_3.mop"' # dip_y
FILE_PROP4='"$SAVEDIR1/prop_no_4.mop"' # dip_z
# Check dominant terms (> 1.0e-3) and some random ones in property fits.
# NB! The accuracy was determined by comparing the results from running the
# test against the operator file that the MidasPot was _actually_ built from -
# yes, it differs, even though it should in principle (?) give the same. It is
# due to numerical instabilities in the polynomial fitting, I guess. -MBH
#
# Energy 
if test -f $FILE_PROP1
then
   # 1-mode > 1.0e-3
   CRIT5[0]=$($GREP -c -- " 2\.363507346[78]............E-03 Q\^2\(Q6\)"  $FILE_PROP1)
   CRIT5[1]=$($GREP -c -- " 3\.8029607637............E-03 Q\^2\(Q7\)"  $FILE_PROP1)
   CRIT5[2]=$($GREP -c -- " 3\.8022143143............E-03 Q\^2\(Q8\)"  $FILE_PROP1)
   CRIT5[3]=$($GREP -c -- " 7\.979364642.............E-03 Q\^2\(Q9\)"  $FILE_PROP1)
   CRIT5[4]=$($GREP -c -- " 8\.314522180[12]............E-03 Q\^2\(Q10\)" $FILE_PROP1)
   CRIT5[5]=$($GREP -c -- " 8\.313748223.............E-03 Q\^2\(Q11\)" $FILE_PROP1)
   # 2-mode > 1.0e-3
   CRIT5[6]=$($GREP -c -- "-1\.228505585[34]............E-03 Q\^2\(Q6\) Q\^1\(Q9\)"   $FILE_PROP1)
   CRIT5[7]=$($GREP -c -- " 2\.958762443[56]............E-03 Q\^1\(Q9\) Q\^2\(Q10\)"  $FILE_PROP1)
   CRIT5[8]=$($GREP -c -- " 2\.950884109[56]............E-03 Q\^1\(Q9\) Q\^2\(Q11\)"  $FILE_PROP1)
   CRIT5[9]=$($GREP -c -- " 2\.1306414999............E-03 Q\^2\(Q10\) Q\^1\(Q11\)" $FILE_PROP1)
   # random
   CRIT5[10]=$($GREP -c -- "-1\.73148278[23].............E-07 Q\^5\(Q6\) Q\^3\(Q11\)" $FILE_PROP1)
   CRIT5[11]=$($GREP -c -- " 2\.870268432.............E-08 Q\^4\(Q6\) Q\^4\(Q7\)"  $FILE_PROP1)
   CRIT5[12]=$($GREP -c -- " 3\.5433826[34]..............E-07 Q\^2\(Q7\) Q\^4\(Q8\)"  $FILE_PROP1)

   TEST[5]=0
   for((i=0; i < ${#CRIT5[@]}; i++))
   do 
      TEST[5]=$((${TEST[5]} + ${CRIT5[$i]}))
   done
   CTRL[5]=${#CRIT5[@]}
   ERROR[5]="Wrong coefficients in energy operator file (prop_no_1.mop)."
else
   TEST[5]=0
   CTRL[5]=1
   ERROR[5]="File missing: $FILE_PROP1"
fi

# x-dipole 
if test -f $FILE_PROP2
then
   # 1-mode > 1.0e-3
   CRIT6[0]=$($GREP -c -- "-3\.25557360071...........E-02 Q\^1\(Q7\)"  $FILE_PROP2)
   CRIT6[1]=$($GREP -c -- " 1\.3110699139............E-03 Q\^2\(Q7\)"  $FILE_PROP2)
   CRIT6[2]=$($GREP -c -- "-1\.31085202087...........E-03 Q\^2\(Q8\)"  $FILE_PROP2)
   CRIT6[3]=$($GREP -c -- " 1\.68569879708...........E-02 Q\^1\(Q11\)" $FILE_PROP2)
   # 2-mode > 1.0e-3
   CRIT6[4]=$($GREP -c -- " 8\.0449652857............E-03 Q\^1\(Q6\) Q\^1\(Q11\)" $FILE_PROP2)
   CRIT6[5]=$($GREP -c -- "-1\.8559370710............E-03 Q\^1\(Q7\) Q\^1\(Q9\)" $FILE_PROP2)
   CRIT6[6]=$($GREP -c -- " 6\.4690804242............E-03 Q\^1\(Q7\) Q\^1\(Q11\)" $FILE_PROP2)
   CRIT6[7]=$($GREP -c -- "-6\.4649073019............E-03 Q\^1\(Q8\) Q\^1\(Q10\)" $FILE_PROP2)
   CRIT6[8]=$($GREP -c -- " 2\.7949464117............E-03 Q\^1\(Q9\) Q\^1\(Q11\)" $FILE_PROP2)
   # random
   CRIT6[9]=$($GREP -c -- " 1\.228723594.............E-05 Q\^2\(Q9\) Q\^2\(Q11\)" $FILE_PROP2)
   CRIT6[10]=$($GREP -c -- "-3\.5047593...............E-10 Q\^2\(Q8\) Q\^7\(Q9\)"  $FILE_PROP2)
   CRIT6[11]=$($GREP -c -- "-5\.2118692...............E-10 Q\^7\(Q7\) Q\^3\(Q9\)"  $FILE_PROP2)

   TEST[6]=0
   for((i=0; i < ${#CRIT6[@]}; i++))
   do 
      TEST[6]=$((${TEST[6]} + ${CRIT6[$i]}))
   done
   CTRL[6]=${#CRIT6[@]}
   ERROR[6]="Wrong coefficients in x-dipole operator file (prop_no_2.mop)."
else
   TEST[6]=0
   CTRL[6]=1
   ERROR[6]="File missing: $FILE_PROP2"
fi


# y-dipole 
if test -f $FILE_PROP3
then
   # 1-mode > 1.0e-3
   CRIT7[0]=$($GREP -c -- " 1\.2873653681[3-5]...........E-01 Q\^1\(Q6\)" $FILE_PROP3)
   CRIT7[1]=$($GREP -c -- " 1\.644020519[89]............E-02 Q\^2\(Q6\)" $FILE_PROP3)
   CRIT7[2]=$($GREP -c -- " 2\.0876276699............E-03 Q\^2\(Q7\)" $FILE_PROP3)
   CRIT7[3]=$($GREP -c -- " 2\.08754957101...........E-03 Q\^2\(Q8\)" $FILE_PROP3)
   CRIT7[4]=$($GREP -c -- "-1\.093877305076..........E-02 Q\^1\(Q9\)" $FILE_PROP3)
   CRIT7[5]=$($GREP -c -- " 1\.0767445361............E-03 Q\^2\(Q9\)" $FILE_PROP3)
   # 2-mode > 1.0e-3
   CRIT7[6]=$($GREP -c -- " 1\.1161211075[2-4]...........E-02 Q\^1\(Q6\) Q\^1\(Q9\)" $FILE_PROP3)
   CRIT7[7]=$($GREP -c -- " 1\.6517921394............E-03 Q\^2\(Q6\) Q\^1\(Q9\)" $FILE_PROP3)
   # random
   CRIT7[8]=$($GREP -c -- " 1\.28598462..............E-10 Q\^10\(Q11\)"         $FILE_PROP3)
   CRIT7[9]=$($GREP -c -- " 5\.5797778...............E-11 Q\^3\(Q8\) Q\^7\(Q10\)"  $FILE_PROP3)
   CRIT7[10]=$($GREP -c -- " 7\.8978003...............E-11 Q\^7\(Q9\) Q\^2\(Q11\)" $FILE_PROP3)

   TEST[7]=0
   for((i=0; i < ${#CRIT7[@]}; i++))
   do 
      TEST[7]=$((${TEST[7]} + ${CRIT7[$i]}))
   done
   CTRL[7]=${#CRIT7[@]}
   ERROR[7]="Wrong coefficients in y-dipole operator file (prop_no_3.mop)."
else
   TEST[7]=0
   CTRL[7]=1
   ERROR[7]="File missing: $FILE_PROP3"
fi


# z-dipole 
if test -f $FILE_PROP4
then
   # 1-mode > 1.0e-3
   CRIT8[0]=$($GREP -c -- "-3\.25503390639...........E-02 Q\^1\(Q8\)"  $FILE_PROP4)
   CRIT8[1]=$($GREP -c -- " 1\.685916526424..........E-02 Q\^1\(Q10\)" $FILE_PROP4)
   # 2-mode > 1.0e-3
   CRIT8[2]=$($GREP -c -- " 8\.0493567294............E-03 Q\^1\(Q6\) Q\^1\(Q10\)"  $FILE_PROP4)
   CRIT8[3]=$($GREP -c -- "-2\.6214351050............E-03 Q\^1\(Q7\) Q\^1\(Q8\)"   $FILE_PROP4)
   CRIT8[4]=$($GREP -c -- "-6\.4653132360............E-03 Q\^1\(Q7\) Q\^1\(Q10\)"  $FILE_PROP4)
   CRIT8[5]=$($GREP -c -- "-1\.87823463477...........E-03 Q\^1\(Q8\) Q\^1\(Q9\)"   $FILE_PROP4)
   CRIT8[6]=$($GREP -c -- "-6\.4617396826............E-03 Q\^1\(Q8\) Q\^1\(Q11\)"  $FILE_PROP4)
   CRIT8[7]=$($GREP -c -- " 2\.79900064347...........E-03 Q\^1\(Q9\) Q\^1\(Q10\)"  $FILE_PROP4)
   CRIT8[8]=$($GREP -c -- " 1\.39544106336[5-7]..........E-03 Q\^1\(Q10\) Q\^1\(Q11\)" $FILE_PROP4)
   # random
   CRIT8[9]=$($GREP -c -- "-2\.7700037[5-7]..............E-07 Q\^1\(Q7\) Q\^5\(Q8\)"    $FILE_PROP4)
   CRIT8[10]=$($GREP -c -- " 2\.951177[56]...............E-09 Q\^3\(Q10\) Q\^4\(Q11\)" $FILE_PROP4)
   CRIT8[11]=$($GREP -c -- "-2\.4253000[34]..............E-07 Q\^5\(Q10\)"          $FILE_PROP4)

   TEST[8]=0
   for((i=0; i < ${#CRIT8[@]}; i++))
   do 
      TEST[8]=$((${TEST[8]} + ${CRIT8[$i]}))
   done
   CTRL[8]=${#CRIT8[@]}
   ERROR[8]="Wrong coefficients in z-dipole operator file (prop_no_4.mop)."
else
   TEST[8]=0
   CTRL[8]=1
   ERROR[8]="File missing: $FILE_PROP4"
fi


PASSED=1
# ! NB ! NB ! NB !
# NOTE: WE CURRENTLY TEST ONLY ENERGY OPERATOR AS DIPOLES ARE NOT CREATED BY DYNLIB. 
# THIS FEATURE WILL BE IMPLEMTED AT SOME POINT AT WHICH POINT THESE TESTS SHOULD BE RUN AS WELL.
# ! NB ! NB ! NB !
for i in {0..8}
do 
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi
' > ${CALCNAME}.check
#######################################################################
