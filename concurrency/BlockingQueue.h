/**
************************************************************************
*
* @file                 BlockingQueue.h
*
* Created:              05-06-2018
*
* Authors:              ? 
*
* Short Description:    ?
*
* Last modified:        ?
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

#ifndef BLOCKING_QUEUE_H_INCLUDED
#define BLOCKING_QUEUE_H_INCLUDED

#include <mutex>
#include <condition_variable>
#include <deque>

template<class T>
class BlockingQueue
{
   private:

      mutable std::mutex mMutex;
      std::condition_variable mCondition;
      std::deque<T> mQueue;
               
   public:

      /**  @function void QueuePush(const T& t) 
       *  @brief Push a value onto the queue.
       *
       *  @param t The value to be pushed onto queue
      **/
      void QueuePush(const T& t)
      {
         {
            std::lock_guard<std::mutex> queue_lock(mMutex);
            mQueue.emplace_back(t);
         }
         mCondition.notify_one();
      }
 
      /**
       *
      **/
      bool PopTryWait(T& t)
      {
         // Get the event
         std::unique_lock<std::mutex> lock(mMutex);
         auto wait_success = mCondition.wait_for( lock
                                                 , std::chrono::milliseconds(100)
                                                 , [this](){ return !this->mQueue.empty(); }
                                                 );
      
         // We successfully popped an event
         if (wait_success)
         { 
            t = std::move(mQueue.front());
            mQueue.pop_front();
         }
         
         return wait_success;
      }
 
      /**
       *
      **/
      bool PopForceWait(T& t)
      {
         // Get the event
         std::unique_lock<std::mutex> lock(mMutex);
         mCondition.wait( lock
                         , [this](){ return !this->mQueue.empty(); }
                         );
      
         t = std::move(mQueue.front());
         mQueue.pop_front();
         
         return true;
      }
      
      /**
       *
      **/
      bool Empty() const
      {
         std::lock_guard<std::mutex> lock(mMutex);
         
         return mQueue.empty();
      }
};
   
#endif /* BLOCKING_QUEUE_H_INCLUDED */
