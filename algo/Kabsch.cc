#include "Kabsch.h"

#include "lapack_interface/GESDD.h"
#include "lapack_interface/GETRF.h"

namespace midas
{
namespace algo
{

/**
 * Find centroid of a set of k points P.
 *
 * /[f
 *    C_{\alpha} = \frac{ \sum_i^k x_{i,\alpha} }{ k }
 * /]f
 * 
 * @param aP   P set of points, given as rows in a matrix.
 *
 * @return  Returns centroid in a MidasVector.
 **/
MidasVector FindCentroid
   (  const MidasMatrix& aP
   )
{
   MidasVector centroid{aP.Ncols(), C_0};

   for(int i = 0; i < aP.Nrows(); ++i)
   {
      for(int j = 0; j < aP.Ncols(); ++j)
      {
         centroid[j] += aP[i][j];
      }
   }
   
   // Divide by k
   auto inv_k = 1.0 / aP.Nrows();

   for(int j = 0; j < centroid.Size(); ++j)
   {
      centroid[j] *= inv_k;
   }

   return centroid;
}

/**
 * Implements Kabsch's algorithm to find the rotation 
 * that rotates P set of points onto Q set of points.
 *
 * Given:
 * /[f
 *    \bold{H} = \bold{P}^T \bold{Q}  
 * /]f
 * and the SVD:
 * /[f
 *    \bold{H} = \bold{U} \bold{S} \bold{V}
 * /]f
 * 
 * /[f
 *    d = det \left( \bold{V} \bold{U}^T \right) 
 * /]f
 *
 * /[f
 *   \bold{R} =   
 * /]f
 *
 *
 * @param aP   P set of points, given as rows in a matrix.
 * @param aQ   Q set of points, given as rows in a matrix.
 *
 * @return   Returns rotation matrix that rotates P onto Q.
 **/
MidasMatrix Kabsch
   (  MidasMatrix& aP
   ,  MidasMatrix& aQ
   )
{
   assert(aP.Nrows() == aQ.Nrows());

   // Calculate H = P^T Q
   MidasMatrix h_mat{aP.Ncols(), aQ.Ncols(), C_0};

   for(int i = 0 ; i < h_mat.Nrows(); ++i)
   {
      for(int j = 0; j < h_mat.Ncols(); ++j)
      {
         for(int k = 0; k < aP.Nrows(); ++k)
         {
            h_mat[i][j] += aP[k][i] * aQ[k][j];
         }
      }
   }

   // Calculate SVD of H
   auto svd = GESDD(h_mat);

   MidasMatrix u;
   MidasMatrix vt;
   LoadU (svd, u);
   LoadVt(svd, vt);

   // Calcutate d = det(V U^T)
   MidasMatrix vut{vt.Ncols(), u.Nrows(), C_0};

   for(int i = 0; i < vut.Nrows(); ++i)
   {
      for(int j = 0; j < vut.Ncols(); ++j)
      {
         for(int k = 0; k < vt.Nrows(); ++k)
         {
            vut[i][j] += vt[k][i] * u[j][k];
         }
      }
   }

   auto lu = GETRF(vut); /* We do a LU decomposition to find the determinant */
   auto d  = lu.Determinant();
   
   /* Calculate optimal rotation matrix
    * 
    *        / 1 0 . 0 \
    * R = V  | 0 1 . 0 | U^T
    *        | . . . . | 
    *        \ 0 0 . d /
    *
    */
   MidasMatrix rot_matrix{aP.Ncols(), aQ.Ncols(), C_0};

   for(int i = 0; i < rot_matrix.Nrows(); ++i)
   {
      for(int j = 0; j < rot_matrix.Ncols(); ++j)
      {
         for(int k = 0; k < vt.Nrows(); ++k)
         {
            rot_matrix[i][j] += vt[k][i] * u[j][k] * (k == rot_matrix.Ncols() - 1? d : 1.0);
         }
      }
   }
   
   // Return result
   return rot_matrix;
}

} /* namespace algo */
} /* namespace midas */
