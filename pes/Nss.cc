/**
************************************************************************
*
* @file                Nss.cc
*
* Created:             25-02-2008
*
* Author:              Eduard Matito
*
* Short Description:   Defines class Nss (Nested Summation Symbol)
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

/*------------------------- Nested Summation Symbol -----------------------
// Program based on Besalu & Carbo one in [J. Math. Chem. 18, 37-72 (1995)]
// using the algorithm by Carbo & Besalu
// Carbo R, Besalu E, J. Math. Chem. 13, 331-342 (1993)
// Carbo R, Besalu E, Computers & Chemistry 18 (2) 117-126 (1994)
---------------------------------------------------------------------------*/

#include "inc_gen/TypeDefs.h"
#include "pes/Nss.h"
#include <stdarg.h>
#include <iostream>


// Generalized Nested Do Loop (GNDL) or NSS algorithm
Uin Nss::RunAll() // Generates next leader vector form
{
   if (mPos>=0) *(mDummy+mPos)+=*(mStep+mPos);

   while (mPos>=0)
      if ( (*(mDummy+mPos)-*(mEnd+mPos))*(*(mStep+mPos))>0 ) // Limit exceeded: index out of range
      {
         *(mDummy+mPos)=*(mIni+mPos);
         mPos--;
         if (mPos>=0) *(mDummy+mPos)+=*(mStep+mPos);
      }
      else
      {
          mPos=mN;
          return 1; // Another vector VectorIndex form is achieved
      }
   Nss::Initialize();  // Initialize NSS for further use
   return 0;           // Ends NSS vector generation
}

// Generalized Nested Do Loop (GNDL) or NSS algorithm - ORDERED VERSION
Uin Nss::RunOrder() // Generates next leader vector form - ordered
{
   if (mPos>=0) *(mDummy+mPos)+=*(mStep+mPos);
   if (mFirst) // set the initial value
   {
      for (In m=1;m<=mN;m++)
         while ( *(mDummy+m) <= *(mDummy+(m-1)))
               *(mDummy+m)+=*(mStep+m);
      mFirst=false;
   }

   while (mPos>=0)
      if  ( (*(mDummy+mPos)-*(mLog+mPos))*(*(mStep+mPos))>0) // Limit exceeded: index out of range ADDED OR TO FIX MEM LEAK
      {
         mPos--;
         if (mPos>=0) *(mDummy+mPos)+=*(mStep+mPos);
         for (int m=mPos+1;m<=mN;m++)
         {
            *(mDummy+m)=*(mIni+m);
            if(m > 0) //Added this to fix mem leak BT
            while ( *(mDummy+m) <= *(mDummy+(m-1) ) )
               *(mDummy+m)+=*(mStep+m);
         }
      }
      else
      {
          mPos=mN;
          return 1; // Another vector VectorIndex form is achieved
      }
   Nss::Initialize();  // Initialize NSS for further use
   return 0;           // Ends NSS vector generation
}
//--------------------------------------------------------------------------
In Nss::Dim(void) { return mDim; } // Returns the NSS dimension
//--------------------------------------------------------------------------
// Returns the NSS vector index m value (indices numbered from 1 to dimension)
In Nss::VectorIndex(In aPos) 
{ 
   return *(mDummy+aPos-1); 
}
//--------------------------------------------------------------------------
// Nss definition
Nss::Nss(const In aDim, ...) // Constructor
{
   va_list index;
   va_start(index,aDim);
  
   mDummy=new In[aDim]; // Allocate needed vectors
   mIni=new In[aDim];
   mEnd=new In[aDim];
   mStep=new In[aDim];
   mLog=new In[aDim];
   mDim=aDim;  // Store auxiliar variables
   mN=mDim-1;
  
   // Initial, Final and Step vectors (variable number of arguments)
  
   for (In m=0;m<mDim;m++) 
      *(mIni+m)=va_arg(index,In);
   for (In m=0;m<mDim;m++) 
      *(mEnd+m)=va_arg(index,In);
   for (In m=0;m<mDim;m++) 
      *(mStep+m)=va_arg(index,In);
   
   va_end(index);  
   
   Nss::Initialize(); //initialize
}


//--------------------------------------------------------------------------
void Nss::Initialize(void) //initializes leader vector mDummy of the nss 
{                   
   for (In m=0;m<=mN;m++)
       *(mLog+m)=*(mEnd+m);

   for (In ia=mN-1;ia>=0;ia--)
      while (*(mLog+ia) >= *(mLog+(ia+1)))
         *(mLog+ia)-=*(mStep+ia);

   for (In m=0;m<mN ; m++) 
      *(mDummy+m)=*(mIni+m); 
   *(mDummy+mN)=*(mIni+mN)-*(mStep+mN); //last index decremented 
   mPos=mN;
   mFirst=true;
}

//--------------------------------------------------------------------------
Nss::~Nss(void) // destructor
{ 
   mFirst=false;
   mDim=mPos=mN=0;
   delete[] mDummy;
   delete[] mIni;
   delete[] mEnd;
   delete[] mStep;
   delete[] mLog;  
   //deallocates vectors
} 

// NSS alternative definition in terms of arrays
Nss::Nss(const In aDim, In* aIni, In* aEnd, In* aStep) // Constructor
{
   mDummy=new In[aDim]; // Allocate needed vectors
   mIni=new In[aDim];
   mEnd=new In[aDim];
   mStep=new In[aDim];
   mLog=new In[aDim];
   mDim=aDim;  // Store auxiliar variables
   mN=mDim-1;
  
   // Initial, Final and Step vectors (variable number of arguments)
  
   for (In m=0;m<mDim;m++) 
      *(mIni+m)=*(aIni+m);
   for (In m=0;m<mDim;m++) 
      *(mEnd+m)=*(aEnd+m);
   for (In m=0;m<mDim;m++) 
      *(mStep+m)=*(aStep+m);
   
   Nss::Initialize(); //initialize
}
