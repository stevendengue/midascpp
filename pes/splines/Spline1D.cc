/**
************************************************************************
 *
 * @file                Spline1D.cc
 *
 * Created:             09-10-2006
 *
 * Author:              Daniele Toffoli & Ove Christiansen
 *
 * Short Description:   Define class spline for cubic spline inerpolation
 *
 * Last modified:
 *
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission
 * of the author or in accordance with the terms and conditions under
 * which the program was supplied.  The code is provided "as is"
 * without any expressed or implied warranty.
 *
 ************************************************************************
 */

// std headers
#include <vector>
#include <string>
#include <string>
#include <iostream>
#include <fstream>
#include <cctype>
#include <map>
#include <algorithm>

// midas headers
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "pes/splines/Spline1D.h"
#include "pes/PesFuncs.h"

// using declarations
using std::vector;
using std::map;
using std::string;

/**
* Local Declarations
 * */
typedef string::size_type Sst;


/**
* Constructor 
* Member and friends Definitions of class Spline1D:
* MidasVector& arXig;      // 1-index grid 
* MidasVector& arYig;      // Values at grid points stored as vector 
* MidasVector& mYigDerivs;
**/


Spline1D::Spline1D(SPLINEINTTYPE arSplineType
                 , const MidasVector& arXig
                 , const MidasVector& arYig
                 , const Nb& arFirstDerivLeft
                 , const Nb& arFirstDerivRight): 
                     mYigDerivs()
                   , mFirstDerivLeft(arFirstDerivLeft)
                   , mFirstDerivRight(arFirstDerivRight)
{
   if (arXig.Size() != arYig.Size())
   {  
      Mout << " Mismatch in the # of grid points and functional values  " << endl;
      MIDASERROR(" Stop in spline1D::spline1D due to mismatch in arXig and arYig shapes");
   }
   
   Mout.setf(ios::scientific);
   Mout.setf(ios::uppercase);
   midas::stream::ScopedPrecision(22, Mout);
        
   In n = arXig.Size();
   mYigDerivs.SetNewSize(n, false);
        
   MidasVector temp_vect(n, C_0);
   Nb sig, p, qn, un;
   
   mSplineType = arSplineType;

   // ***   then calculate the second derivatives array
   switch (arSplineType)
   {
      case SPLINEINTTYPE::NATURAL:
      {
         mYigDerivs[I_0] = C_0;
         temp_vect[I_0] = C_0;

         break;
      }
      case SPLINEINTTYPE::GENERAL:
      {
         mYigDerivs[I_0] = -C_I_2;
         temp_vect[I_0]=(C_3/(arXig[I_1]-arXig[I_0])) * ((arYig[I_1]-arYig[I_0]) / 
                (arXig[I_1]-arXig[I_0]) - arFirstDerivLeft);
         break;
      }
      case SPLINEINTTYPE::ERROR:
      default:
      {
         // below error message does not give any useable information :S
         MIDASERROR(" Keyword "+ SPLINEINTTYPE_MAP::EnumToString(arSplineType) +" does not represent a valid spline type ");
      }
   }
        
// ***   then construct and solve the tridiagonal system 
// ***   (from numerical recipes in c++, spline.cpp, pag. 118)
   for (In i = I_1; i < n - I_1; i++) 
   {
      sig = (arXig[i]-arXig[i-I_1]) / (arXig[i+I_1]-arXig[i-I_1]);
      p   = sig * mYigDerivs[i-I_1] + C_2;
      mYigDerivs[i] = (sig - C_1) / p;
        temp_vect[i]=(arYig[i+I_1]-arYig[i])/(arXig[i+I_1]-arXig[i]) 
           - (arYig[i]-arYig[i-I_1])/(arXig[i]-arXig[i-I_1]);
        temp_vect[i]=(C_6 * temp_vect[i]/(arXig[i+I_1]-arXig[i-I_1])-sig * temp_vect[i-I_1])/p;
   }

   if (mSplineType == SPLINEINTTYPE::NATURAL)
   {
      qn = C_0;
      un = C_0;
   }
   else 
   {
      qn = C_I_2;
      un = (C_3/(arXig[n - I_1] - 
      arXig[n - I_2])) * (arFirstDerivRight - (arYig[n - I_1]
      -arYig[n - I_2])/(arXig[n - I_1] - arXig[n - I_2]));
   } 
           
   mYigDerivs[n - I_1] = (un - qn*temp_vect[n-I_2]) / (qn*mYigDerivs[n-I_2]+C_1);
        
   for (In k = n-I_2; k >= I_0; k--)
   {
         mYigDerivs[k]=mYigDerivs[k] * mYigDerivs[k+I_1]+ temp_vect[k];
   }
}

/**
* Update: update temporary storage for type Spline1D 
**/ 

void Spline1D::Update(const MidasVector& arXigNew, const MidasVector& arYigNew)                  // refresh spline set on new grids
{
        
   if (arXigNew.Size() != arYigNew.Size())
   {
      Mout << " Mismatch in the # of grid points and functional values  " << endl;
      MIDASERROR(" Stop in spline1D::Update due to mismatch in arXig and arYig shapes");
   }
        
   In n = arXigNew.Size();
   mYigDerivs.SetNewSize(n, false);
        
   ///enum spline {NATURAL, GENERAL};
        
   MidasVector temp_vect(n, C_0);
   Nb sig, p, qn, un;
        
   switch (mSplineType)
   {
      case SPLINEINTTYPE::NATURAL:
      {
         mYigDerivs[I_0] = C_0;
         temp_vect[I_0] = C_0;
                        
         break;
      }
      case SPLINEINTTYPE::GENERAL:
      {
         mYigDerivs[I_0] = -C_I_2;
         temp_vect[I_0]=(C_3/(arXigNew[I_1]-arXigNew[I_0])) * ((arYigNew[I_1]-arYigNew[I_0]) /
         (arXigNew[I_1]-arXigNew[I_0]) - mFirstDerivLeft);
         break;
      }
      case SPLINEINTTYPE::ERROR:
      default:
      {
         MIDASERROR("ERROR IN SPLINE SWITCH");
      }
   }
        
   for (In i = I_1; i < n - I_1; i++)
   {
      sig = (arXigNew[i]-arXigNew[i-I_1]) / (arXigNew[i+I_1]-arXigNew[i-I_1]);
      p   = sig * mYigDerivs[i-I_1] + C_2;
      mYigDerivs[i] = (sig - C_1) / p;
      temp_vect[i]=(arYigNew[i+I_1]-arYigNew[i])/(arXigNew[i+I_1]-arXigNew[i])
        - (arYigNew[i]-arYigNew[i-I_1])/(arXigNew[i]-arXigNew[i-I_1]);
      temp_vect[i]=(C_6 * temp_vect[i]/(arXigNew[i+I_1]-arXigNew[i-I_1])-sig * temp_vect[i-I_1])/p;
   }
        
   if (mSplineType == SPLINEINTTYPE::NATURAL)
   {
      qn = C_0;
      un = C_0;
   }
   else
   {
      qn = C_I_2;
      un = (C_3/(arXigNew[n - I_1] -
           arXigNew[n - I_2])) * (mFirstDerivRight - (arYigNew[n - I_1]
           -arYigNew[n - I_2])/(arXigNew[n - I_1] - arXigNew[n - I_2]));
   }
        
   mYigDerivs[n - I_1] = (un- qn * temp_vect[n - I_2]) /
                         (qn * mYigDerivs[n - I_2] + C_1);
        
   for (In k = n-I_2; k >= I_0; k--)
       mYigDerivs[k]=mYigDerivs[k] * mYigDerivs[k+I_1]+ temp_vect[k];
}

/**
* return the arrays of second derivatives on the CG
**/
void Spline1D::GetYigDerivs(MidasVector& arYigDerivs) const
{
   if (arYigDerivs.Size() != mYigDerivs.Size())
   {
      MIDASERROR(" Mismatch in the shapes of arYigDerivs and mYigDerivs \n Stop in spline1D::GetYigDerivs due to mismatch in arXig and arYig shapes");
   }
   arYigDerivs=mYigDerivs;
}

/**
 * return the spline approx. at the point at a given output grid (og) arXog, in the
 * arYog array 
**/
void GetYog
   (  MidasVector& arYog
   ,  const MidasVector& arXog
   ,  const MidasVector& arXig
   ,  const MidasVector& arYig
   ,  const MidasVector& arYigDerivs
   ,  const In& arstart
   ,  const In& arstride
   ) 
{
        
//   Mout << " *** entered in Spline1D::GetYog " << endl;
//   Mout << " given arstart and arstride values are :" << arstart << " " << arstride << endl;
   In n = arXig.Size();
   In n_out = arXog.Size();
   In k_low=I_0;
   In k_high=n-I_1;
   Nb a_factor, b_factor, h; 
        
   for (In i = I_0; i<n_out; i++)
   {
//      Mout << " current i index is : " << i << endl;
//      Mout << " current index in arYog array is : " << i*arstride+arstart << endl;
      PesFuncs::hunt(arXig, arXog[i], k_low);
      k_high=k_low+I_1;
      h = arXig[k_high] - arXig[k_low];
      if (h == C_0) 
      {
         MIDASERROR("Bad evaluation points in Spline1D::GetYog");
      }
      a_factor = (arXig[k_high] - arXog[i]) / h;
      b_factor = (arXog[i] - arXig[k_low])  / h;
      arYog[i*arstride+arstart] =  a_factor * arYig[k_low] + b_factor * arYig[k_high]+
         ((a_factor * a_factor * a_factor - a_factor) * arYigDerivs[k_low]
         + (b_factor * b_factor * b_factor - b_factor) *arYigDerivs[k_high])*(h * h)/C_6;
   }
   //Mout << " filled arYog array is :" << endl << arYog << endl;
   return;
}

/**
* return the spline approx. at a given  output grid (og) points arXog
**/

void GetYog(Nb& arYog, const Nb& arXog, const MidasVector& arXig, const MidasVector& arYig, const MidasVector& arYigDerivs)

{
        
   In k;
   In n = arXig.Size();
   In k_low;
   In k_high;
   Nb a_factor, b_factor, h;
        
   k_low  = I_0;
   k_high = n-I_1;
        
   while (k_high - k_low > I_1)
   {
      k = (k_high + k_low) >> I_1;               // halves the interval
      if (arXig[k] > arXog) k_high = k;
      else k_low = k;
   }
   h = arXig[k_high] - arXig[k_low];
   if (h == C_0)
   { 
      MIDASERROR("Bad evaluation points in Spline1D::GetYog");
   }
   a_factor = (arXig[k_high] - arXog) / h;
   b_factor = (arXog - arXig[k_low])  / h;
   arYog = a_factor * arYig[k_low] + b_factor * arYig[k_high]+
   ((a_factor * a_factor * a_factor - a_factor) * arYigDerivs[k_low]
   + (b_factor * b_factor * b_factor - b_factor) *arYigDerivs[k_high])*(h*h)/C_6;
}

