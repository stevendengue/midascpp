/**
************************************************************************
*
* @file                Shepard.h
*
* Created:             23-06-2008
*
* Author:              Daniele Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Declares class for Shepard interpolation
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef SHEPARD_H
#define SHEPARD_H

#include <vector>

#include "inc_gen/TypeDefs.h"

/**
 * Class to handle Shepard interpolation.
 **/
class Shepard
{
   private:

      //! Order of the Shepard interpolation
      In mIord;            
   
      //! The actual mode combination to be interpolated
      std::vector<In> mModeCombi;  

   public:
   
      //! Constructor.
      Shepard();

      //! Constructor from order and mode combination.
      Shepard(In aIord, std::vector<In>& arModeCombi);
      
      //! Set the order of the interpolation
      void SetIord(In aIord) { mIord=aIord; }                        
      
      //! Get the Shepard order
      In GetIord() { return mIord; }
      
      //! Set the mode combi vector
      void SetModeCombi(std::vector<In>& arModeCombi) {mModeCombi=arModeCombi;}  
      
      //! Get the mode combi
      std::vector<In> GetModeCombi() {return mModeCombi;}
      
      //! Get a vector with the interpolated data
      void GetYogNonDp
         (  MidasVector& arYog
         ,  const std::vector<MidasVector>& arXog
         ,  MidasVector& arSig
         ,  const std::vector<MidasVector>& arXig
         ,  const MidasVector& arYig
         ,  const std::vector<MidasVector>& arFuncDerivs
         ,  const MidasVector& arMinDis
         ,  In aNpoints
         ); 
};

#endif /* SHEPARD_H */
