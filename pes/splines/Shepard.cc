/**
************************************************************************
*
* @file                Shepard.cc
*
* Created:             23-06-2008
*
* Author:              Daniele Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Member declarations and definitions
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "util/Io.h"
#include "pes/splines/Shepard.h"
#include "input/PesCalcDef.h"

/**
 * Default constructor
**/
Shepard::Shepard()
{
   mModeCombi.clear();
   mIord = I_0;
}

/**
 * Constructor
**/
Shepard::Shepard(In aIord, std::vector<In>& aModeCombi)
{
   mIord = aIord;
   mModeCombi = aModeCombi;

   if (gPesIoLevel > I_10)
   {
      if (aIord == I_0)
      {
         Mout << "  Function values will be used, (order 0) for mode combination " << aModeCombi << std::endl;
      }
      else if (aIord == I_1)
      {
         Mout << "  Function and Gradient data will be used, (order 1) for mode combination " << aModeCombi << std::endl;
      }
      else if (aIord == I_2)
      {
         Mout << "  Function, Gradient and Hessian data will be used, (order 2) for mode combination " << aModeCombi << std::endl;
      }
      else
      {
         MIDASERROR("Order not implemented for Shepard interpolation");
      }
   }
}
/**
 * Perform the actual interpolation for the given mode combi with an explicit grid of point
 * */
void Shepard::GetYogNonDp
   (  MidasVector& arYog
   ,  const vector<MidasVector>& arXog
   ,  MidasVector& arSig
   ,  const vector<MidasVector>& arXig
   ,  const MidasVector& arYig
   ,  const vector<MidasVector>& arFuncDerivs
   ,  const MidasVector& arMinDis
   ,  In aNpoints
   )
{
   if (gDebug || gPesIoLevel > I_14)
   {
      Mout << " ARYOG \n" << arYog << std::endl;
      Mout << " ARXOG \n" << arXog << std::endl;
      Mout << " ARSIG \n" << arSig << std::endl;
      Mout << " ARXIG \n" << arXig << std::endl;
      Mout << " ARYIG \n" << arYig << std::endl;
      Mout << " FUNCDERIVS \n" << arFuncDerivs << std::endl;
      Mout << " arMinDis \n" << arMinDis << std::endl;
      Mout << " aNpoints \n" << aNpoints << std::endl;
   }

   In n_derivs=arFuncDerivs.size();
   In n_ord=GetIord();
   vector<In> mc_vec=GetModeCombi();
   //Mout << " mc_vec: " << mc_vec << " n_ord: " << n_ord << " n_derivs: " << n_derivs << endl;  
   //do some checking now
   if (n_ord==I_1 && n_derivs!=mc_vec.size())
   {
      In n_n_d=mc_vec.size()+ mc_vec.size()*(mc_vec.size()+I_1)/I_2;
      if (n_n_d == n_derivs)
      {
         if (gPesIoLevel > I_7)
         {
            Mout << "Hessian was avaible but it is not going to be used" << std::endl;
         }
      }  
      else
      {
         MIDASERROR(" Mismatch in the number of derivatives in Shepard::GetYog ");
      }
   }
   else if (n_ord==I_2)
   {
      In n_n_d=mc_vec.size()+ mc_vec.size()*(mc_vec.size()+I_1)/I_2;
      if (n_n_d!=n_derivs)
         MIDASERROR(" Mismatch in the number of derivatives in Shepard::GetYog ");
   }
   In n_dim=arXog.size();
   if (n_dim!=mc_vec.size()) MIDASERROR(" Mismatch in the dimension of the MC surface ");

   //Mout << "dimension of arXig " << arXig.size() <<endl;
   if (n_dim!=arXig.size()) MIDASERROR(" Mismatch in the dimension of arXig and arXog arrays ");

   In n_og_points=I_1;
   for (In i_dim=I_0; i_dim<n_dim; i_dim++)
      n_og_points*=arXog[i_dim].Size();
   if (n_og_points != arYog.Size())
      MIDASERROR(" Stop in Shepard::GetYog due to mismatch in arXig's and arYig points ");

   In n_ig_points=arYig.Size();
   for (In i_dim=I_0; i_dim<n_dim; i_dim++)
      if (arXig[i_dim].Size() != arYig.Size())
         MIDASERROR(" Stop in Shepard::GetYog due to mismatch in arXig's and arYig shapes");

   MidasVector q_grad(I_0);
   if (n_ord>I_0) q_grad.SetNewSize(mc_vec.size());
   MidasMatrix q_hess(I_0);
   if (n_ord==I_2) q_hess.SetNewSize(mc_vec.size(),false,true);
   //run over the points to be interpolated
   for (In o_g=I_0; o_g<n_og_points; o_g++)
   {
      In curr_dim=I_1;
      In curr_index;
      MidasVector x_og(n_dim,C_0);
      for(In i_dim=n_dim-I_1; i_dim>=I_0; i_dim--)
      {
         curr_index = (o_g%(curr_dim*arXog[i_dim].Size()))/curr_dim;
         x_og[i_dim]=arXog[i_dim][curr_index];
         curr_dim = curr_dim*arXog[i_dim].Size();
      }
      if (gDebug)
      {
         Mout << " Point to be interpolated: ";
         for (In i=I_0; i<n_dim; i++) Mout << "  " << x_og[i];
         Mout << endl << endl;
      }
      Nb aVal=C_0;
      Nb den=C_0;
      Nb decay=C_0;
      Nb lambda=gPesCalcDef.GetmPesMsiLambda();
      bool calculated = false;
      //run over the calculated points
      //Mout << " Run over the calculated points " << endl;
      for (In i_g=I_0; i_g<n_ig_points; i_g++)
      {
         MidasVector x_ig(n_dim,C_0);
         q_grad.Zero();
         q_hess.Zero();
         for(In i_dim=I_0; i_dim<n_dim;i_dim++)
            x_ig[i_dim]=arXig[i_dim][i_g];

         if (gDebug)
         {
            Mout << " Point # : " << i_g << "   ";
            for (In i=I_0; i<n_dim; i++) Mout << x_ig[i] << " ";
            Mout << endl;
            Mout << " Function value: " << arYig[i_g] << endl;
         }
         //retrieve the point and derivative infos.
         Nb f_val=arYig[i_g];
         In n_pos=I_0;
         if (n_ord>I_0)
         {
            for (In i=I_0; i<mc_vec.size(); i++) 
            {
               q_grad[i]=arFuncDerivs[i][i_g];
               n_pos++;
            }
            if (n_ord==I_2)
            {
               for (In i=I_0; i<mc_vec.size(); i++) 
                  for (In j=i; j<mc_vec.size(); j++)
                  {
                     q_hess[i][j]=arFuncDerivs[n_pos][i_g];
                     n_pos++;
                  }
               //symmetrize
               for (In i=I_0; i<q_hess.Nrows(); i++)
               {
                  for (In j=i+I_1; j<q_hess.Ncols(); j++)
                     q_hess[j][i]=q_hess[i][j];
               }
            }
         }
         if (gDebug)
         {
            Mout << " Gradient: " << endl;
            Mout << q_grad;
            if (n_ord>I_1)
            {
               Mout << " Hessian: " << endl;
               Mout << q_hess;
            }
         }
         MidasVector q_vec(n_dim,C_0);
         q_vec.Zero();
         q_vec=x_og-x_ig;
         Nb distance = q_vec.Norm();
         Nb w_i = C_1 / std::pow((distance + C_NB_EPSILON), Nb(gPesCalcDef.GetmShepardExp()));
         // Mout << " for q_vec: " << q_vec << " weigth: " << w_i << endl;
         den+=w_i;
         Nb t_series=f_val;
         if (n_ord>I_0) 
         {
            t_series+=Dot(q_grad,q_vec);
            if (n_ord==I_2) {
               MidasVector tmp_vec=C_I_2*q_hess*q_vec;
               t_series+=Dot(tmp_vec,q_vec);
            }
         }
         if (gDebug) Mout << " Partial value:  " << t_series << " * " << w_i << " = " << t_series*w_i << endl  << endl;
         aVal+=t_series*w_i;

         if (distance<=C_I_10_8)
         {
            calculated = true;
         }
         else
         {
            if (i_g < aNpoints) 
            {
               Nb term =C_1;
               //if (i_g < aNpoints) decay+=exp(-1.0*lambda*distance/aMinDis);
               for (In j=I_0; j<n_dim; j++)
               {
                  term*=exp(-1.0*lambda*fabs(q_vec[j]/arMinDis[j]));
               }
               decay+=term;
            }
         }
      }
      aVal/=den;
      arYog[o_g]=aVal;

      arSig[o_g]=C_1/decay;
      //arSig[o_g]=(C_1)/(pow(decay,n_dim));
      
      if(calculated || arSig[o_g]< C_1) arSig[o_g]=C_1;   //hard coded 
      

      bool check=false;
      for (In i=I_0; i<n_dim; i++)
         if (x_og[i] == C_0)  check=true;

      if (check)
      {
         if (arYog[o_g] > 1000.0*C_NB_EPSILON)
         {
            MidasWarning("Possible problem with arYog = " + std::to_string(arYog[o_g]) + " > " + std::to_string(1000.0*C_NB_EPSILON));
         }
         arYog[o_g]=C_0;   // enforce C_0 on the axes. 
      }
      
      if (gPesIoLevel > I_11) 
      {
         Mout << " Ev. pt: ";
         for (In i = I_0; i < n_dim; i++)
         {
            Mout << x_og[i] << " ";
         }
         Mout << " interp.: " << arYog[o_g] <<  "  "  << arSig[o_g] << std::endl;
      }
      
   }
   return;
}
