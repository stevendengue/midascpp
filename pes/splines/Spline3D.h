/**
************************************************************************
*
* @file                Spline3D.h
*
* Created:             11-10-2006
*
* Author:              Daniele Toffoli & Ove Christiansen
*
* Short Description:   Declares class Spline1D for cubic spline interpolation
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef SPLINE3D_H
#define SPLINE3D_H

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "mmv/MidasVector.h"
#include "pes/splines/Spline1D.h"
#include "pes/splines/Spline2D.h"
#include "pes/splines/SplineType.h"

/**
* Declarations:
* 2D interpolation of input grid (ig) to another output grid (og):
* MidasVector& arXig1;                            // 1-index input grid (ig) 
* MidasVector& arXig2;                            // 2-index input grid (ig)
* MidasVector& arXig3;                            // 3-index input grid (ig)
* MidasVector& arYig;                             // Values at grid points stored as vector 
* MidasVector& mYigDerivs;                        // Second derivatives vector mapping arYig
* */
class Spline3D 
{
   private:
      MidasVector mYigDerivs;                                                          ///< stores second derivatives on the input grid (ig)
      SPLINEINTTYPE mSplineType;                                                                  ///< define the type of splines
      Nb mFirstDerivLeft;
      Nb mFirstDerivRight;
   public:
      Spline3D(SPLINEINTTYPE arSplineType, const MidasVector& arXig1, const MidasVector& arXig2,
               const MidasVector& arXig3, const MidasVector& arYig,
               const Nb& arFirstDerivLeft, const Nb& arFirstDerivRight);
      SPLINEINTTYPE GetSplineType() const {return mSplineType;}                                         ///< return the type of spline function
      void GetYigDerivs(MidasVector& arYigDerivs) const;                                     ///< return the second derivatives on the input grid (ig)
      Nb GetYigDerivs(const In& address) const {return mYigDerivs[address];}                 ///< return the second derivatives on the input grid (ig)
};

//! Return the spline interpolation on the output arYog grid.
void GetYog(MidasVector& arYog, const MidasVector& arXog1, const MidasVector& arXog2, const MidasVector& arXog3,   
            const MidasVector& arXig1, const MidasVector& arXig2, const MidasVector& arXig3,
            const MidasVector& arYig, const MidasVector& arYigDerivs,
            SPLINEINTTYPE arSplineType, const MidasVector& arFirstDerivLeft, const MidasVector& arFirstDerivRight);

#endif // SPLINE3D_H
