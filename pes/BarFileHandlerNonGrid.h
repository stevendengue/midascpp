/**
************************************************************************
* 
* @file                BarFileHandlerNonGrid.h
*
* Created:             09-10-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Generate the list of calculations and work on the .mbar files
* 
* Last modified: Thu Oct 09, 2012  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef BARFILEHANDLERNONGRID_H
#define BARFILEHANDLERNONGRID_H

// std headers
#include <vector>
#include <map>
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "pes/BaseBarFileHandler.h"

// forward declarations
class GridType;
class ModeCombiOpRange;
class PesInfo;
class Derivatives;
class ModeCombi;

/**
 *
 **/
class BarFileHandlerNonGrid 
   : public BaseBarFileHandler
{
   private:
      using ipair = std::pair<In, In>;

      //!
      void ConstructAllKvecs
         (  const std::vector<InVector>&
         ,  const std::map<ipair,ipair>&
         ,  std::vector<InVector>&
         ,  std::map<In,ipair>&
         )  const;
      
      //! Calculates the factors for the derivates
      void ConstructAllNvecs(std::vector<InVector>&, std::map<ipair, ipair>&, In, In, GridType);
      
      //!
      Nb C_function(In&, In&); 
      
      //!
      void AddKvecsToVec(std::vector<InVector>&, InVector, const InVector& , const InVector&, In) const;
      
      //!
      void AddNvecsToVec(std::vector<InVector>&, InVector, In&, In, In&) const;
      
      //!
      void ModeDisplacements_zero(const ModeCombi&, std::vector<In>&, const GridType&, CalculationList&);
      
      //!
      void ModeDisplacements_one(const ModeCombi&, std::vector<In>&, std::vector<In>&,MidasVector& ,bool&,
                                 std::vector<Nb>&,const GridType&,Nb&,bool,std::map<InVector,Nb>&, std::string&, std::string&,const Derivatives&, std::vector<MidasVector>&);
   public:
      //! Constructor
      BarFileHandlerNonGrid(PesInfo&);

      //!
      void DoDispDer_zero(const ModeCombiOpRange&, In, const GridType&, CalculationList&);
      
      //!
      void DoDispDer_one(const ModeCombiOpRange&,In,const GridType&, const Derivatives&);
      
      //!
      void Extrapolation(const Derivatives&, const CalculationList&, const std::vector<std::string>&);
};

#endif //BARFILEHANDLERNONGRID_H
