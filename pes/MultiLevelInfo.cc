#include "pes/MultiLevelInfo.h"

#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "util/Error.h"
#include "mpi/Impi.h"

namespace midas
{
namespace pes
{

/**
 * Update PesLevelInfo for next multilevel.
 *
 * @param aPesCalcDef   Reference to the CalcDef.
 * @param aNextLevel    The next multilevel to run.
 * @param aSubDir       The level sub-directory.
 * @param aLinear       Is the molecule linear.
 * */
void MultiLevelInfo::Update
   (  const PesCalcDef& aPesCalcDef
   ,  const In& aNextLevel
   ,  const std::string& aSubDir
   ,  const bool& aLinear
   )
{
   // Update level
   mCurrentLevel = aNextLevel;

   // Find the number of threads for this multilevel
   if(mCurrentLevel == -I_1)
   {
      MIDASERROR("Number of threads for this multilevel is not set.");
   }
   else
   {
      mNthreads = FindNthreads(mCurrentLevel, aPesCalcDef);
   }

   // Update main directory
   mMainDir      = aSubDir + "/Multilevels/level_" + std::to_string(aNextLevel);

   // Update directories
   mSetupDir    = mMainDir + "/setup";
   mSaveIoDir   = mMainDir + "/savedir";
   mAnalysisDir = mMainDir + "/analysis";

   // Get the property info file name
   auto single_point_name = gPesCalcDef.GetSinglePointName(aNextLevel);
   auto sp_info = SinglePointCalc(single_point_name).SpInfo();
   auto property_info_name = sp_info.find("PROPERTYINFO")->second;
   auto propinfo_path = mSetupDir + "/" + property_info_name;

   // If the property info file exists, then update
   std::string propinfo;
   if (midas::filesystem::Exists(propinfo_path))
   {
      propinfo = midas::mpi::FileToString(propinfo_path);
   }
   else
   {
      propinfo = "";
      if (aSubDir != "FinalSurfaces")
      {
         MidasWarning("Did not find propinfo-file: '" + propinfo_path + "'.");
      }
   }
   mPropertyInfo.Update(aPesCalcDef, propinfo, aLinear);
}

/**
 * Get current level.
 *
 * @return   Return current level as integer.
 **/
In MultiLevelInfo::CurrentLevel
   (
   )  const
{
   return mCurrentLevel;
}

/**
 * Get the number of threads for the current multilevel.
 *
 * @return   Return number of threads for multilevel as unsigned integer.
 **/
Uin MultiLevelInfo::Nthreads
   (
   ) const
{
   return mNthreads;
}

/**
 * Find the number of threads for current multilevel.
 *
 * @param arCurrentLevel   The current multilevel number.
 * @param arPesCalcDef     PesCalcDef.
**/
Uin MultiLevelInfo::FindNthreads
   (  In arCurrentLevel
   ,  const PesCalcDef& arPesCalcDef
   )  const
{
   std::vector<Uin> nthreads_per_ml = arPesCalcDef.GetNThreads();
   
   // If we only give one number for the NThreads, then we extent this to all multilevels
   if (nthreads_per_ml.size() == I_1)
   {
      if (arPesCalcDef.BoundariesPreOpt())
      {
         for (In i = I_0; i < arCurrentLevel; i++)
         {
            nthreads_per_ml.push_back(nthreads_per_ml[I_0]);
         }
      }
      else
      {
         for (In i = I_0; i < arCurrentLevel - I_1; i++)
         {
            nthreads_per_ml.push_back(nthreads_per_ml[I_0]);
         }
      }
   }

   // Return the number of threads for the current multilevel 
   if (arPesCalcDef.BoundariesPreOpt())
   {
      return nthreads_per_ml[arCurrentLevel];
   }
   else
   {
      return nthreads_per_ml[arCurrentLevel - I_1];
   }
}

/**
 * Get sub directory.
 *
 * @return    Returns current sub directory as string.
 **/
const std::string& MultiLevelInfo::SubDir
   (
   )  const
{
   return mSubDir;
}

/**
 * Get path for main directory.
 *
 * @return    Returns current main directory as string.
 **/
const std::string& MultiLevelInfo::MainDir
   (
   )  const
{
   return mMainDir;
}

/**
 * Get path for setup directory.
 *
 * @return    Returns current setup directory as string.
 **/
const std::string& MultiLevelInfo::SetupDir
   (
   )  const
{
   return mSetupDir;
}

/**
 * Get path for savedir.
 *
 * @return    Return path to current savedir as string.
 **/
const std::string& MultiLevelInfo::SaveIoDir
   (
   )  const
{
   return mSaveIoDir;
}

/**
 * Get path for analysis dir.
 *
 * @return    Returns current analysis dir as string.
 **/
const std::string& MultiLevelInfo::AnalysisDir
   (
   )  const
{
   return mAnalysisDir;
}

/**
 * Get read-in prop info string.
 *
 * @return    Return prop info string for current level.
 **/
const PropertyInfo& MultiLevelInfo::PropInfo
   (
   )  const
{
   return mPropertyInfo;
}

/**
 * Get property number from property name/descriptor. 
 * NB: Used in extrap/derivative code, so might go away at some point.
 *
 * @param aDescriptor   The property descriptor.
 *
 * @return    Returns property number. If not found throws a MIDASERROR.
 **/
In MultiLevelInfo::GetPropertyNumber
   (  const std::string& aDescriptor
   )  const
{
   In number = -1;

   // Find the property.
   for( In i = 0; i < mPropertyInfo.Size(); ++i)
   {
      if( mPropertyInfo.GetPropertyInfoEntry(i).GetDescriptor() == aDescriptor)
      {
         number = i;
         break;
      }
   }

   // Check for error.
   if( number == -1 )
   {
      MIDASERROR("Operator with name : '" + aDescriptor + "' not found.");
   }
   
   // Return property number. We add 1 as properties are numbered from 1 and not from 0 (needed by extrap code).
   return number + 1;
}

/**
 * Get property descriptor from property number.
 * NB: Used in derivative/extrap code so might go away at some point.
 *
 * @param aI   Property index.
 *
 * @return  Returns property descriptor if found. Else throws a MIDASERROR.
 **/
const std::string& MultiLevelInfo::GetPropertyName
   (  In aI
   )  const
{
   if (aI < mPropertyInfo.Size())
   {
      return mPropertyInfo.GetPropertyInfoEntry(aI).GetDescriptor();
   }
   
   // If not found throw an error.
   MIDASERROR("Operator number " + std::to_string(aI) + " not found. Only " + std::to_string(mPropertyInfo.Size()) + " operators available.");

   // Will never get here
   return mPropertyInfo.GetPropertyInfoEntry(0).GetDescriptor();
}

/**
 * Get the number of properties for current level.
 *
 * @return Returns number of properties.
 **/
In MultiLevelInfo::GetNoOfProps
   (
   )  const
{
   return mPropertyInfo.Size();
}

} /* namespace pes */
} /* namespace midas */
