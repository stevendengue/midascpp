#ifndef MIDAS_PES_ADJUSTINPUT_H_INCLUDED
#define MIDAS_PES_ADJUSTINPUT_H_INCLUDED

class PesCalcDef;

namespace midas
{

// Forward declarations
namespace molecule
{
   class MoleculeInfo;
}

//  
namespace pes
{

//! Adjust molecular input based in Pes Input
void AdjustMolecularInput(const PesCalcDef& aPesCalcDef, molecule::MoleculeInfo& aMolecule);

} /* namespace pes */
} /* namespace midas */

#endif /* MIDAS_PES_ADJUSTINPUT_H_INCLUDED */
