/**
************************************************************************
* 
* @file                InvTypeMetric.cc
*
* Created:             06-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Class for collecting the terms of the metric using the
*                    inverse of the whole strategy
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <map>
#include <vector>
#include <string>
#include <sstream>

// midas headers
#include "pes/kinetic/InvTypeMetric.h"
#include "pes/kinetic/BaseMetric.h"
#include "pes/molecule/MoleculeInfo.h"
#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"
#include "inc_gen/TypeDefs.h"

// using declarations
using std::map;
using std::vector;
using std::string;
using std::stringstream;

/**
 * 
 **/
InvTypeMetric::InvTypeMetric
   (  const midas::molecule::MoleculeInfo& aMol
   ) 
   :  BaseMetric(aMol)
   ,  mInvMetric(aMol.GetNoOfVibs()+I_3)
   ,  mTraces(aMol.GetNoOfVibs())
{
   Mout << "Initing inv metric" << endl;
   mDerMetrics = new MidasMatrix[aMol.GetNoOfVibs()];
   for(In i = I_0; i < aMol.GetNoOfVibs(); ++i)
      mDerMetrics[i].SetNewSize(aMol.GetNoOfVibs()+I_3);
   Mout << "Done allocating" << endl;
}

/**
 *
 **/
InvTypeMetric::~InvTypeMetric()
{
   delete[] mDerMetrics;
}

/**
 *
 **/
void InvTypeMetric::PopulateMetric()
{
   for(In i = I_0; i < mMol.GetNoOfVibs(); ++i)
   {
      for(In j = i+I_1; j < mMol.GetNoOfVibs(); ++j)
         mInvMetric[j][i] = mInvMetric[i][j] = mS[i][j];
      mInvMetric[i][i] = mS[i][i];
   }
   for(In i = I_0; i < I_3; ++i)
      for(In j = I_0; j < mMol.GetNoOfVibs(); ++j)
         mInvMetric[mMol.GetNoOfVibs() + i][j] = mInvMetric[j][mMol.GetNoOfVibs() + i] = mC[j][i];
   for(In i = I_0; i < I_3; ++i)
   {
      for(In j = i + I_1; j < I_3; ++j)
         mInvMetric[mMol.GetNoOfVibs() + i][mMol.GetNoOfVibs() + j] = mInvMetric[mMol.GetNoOfVibs() + j][mMol.GetNoOfVibs() + i] = mI[i][j];
      mInvMetric[mMol.GetNoOfVibs() + i][mMol.GetNoOfVibs() + i] = mI[i][i];
   }
}

/**
 *
 **/
void InvTypeMetric::PopulateDerMetric()
{
   for(In h = I_0; h < mMol.GetNoOfVibs(); ++h)
   {
      for(In i = I_0; i < mMol.GetNoOfVibs(); ++i)
      {
         for(In j = i+I_1; j < mMol.GetNoOfVibs(); ++j)
            mDerMetrics[h][j][i] = mDerMetrics[h][i][j] = mDerS[h][i][j];
         mDerMetrics[h][i][i] = mDerS[h][i][i];
      }
      for(In i = I_0; i < I_3; ++i)
         for(In j = I_0; j < mMol.GetNoOfVibs(); ++j)
            mDerMetrics[h][mMol.GetNoOfVibs() + i][j] = mDerMetrics[h][j][mMol.GetNoOfVibs() + i] = mDerC[h][j][i];
      for(In i = I_0; i < I_3; ++i)
      {
         for(In j = i + I_1; j < I_3; ++j)
         {
            mDerMetrics[h][mMol.GetNoOfVibs() + i][mMol.GetNoOfVibs() + j] = 
            mDerMetrics[h][mMol.GetNoOfVibs() + j][mMol.GetNoOfVibs() + i] = mDerI[h][i][j];
         }
         mDerMetrics[h][mMol.GetNoOfVibs() + i][mMol.GetNoOfVibs() + i] = mDerI[h][i][i];
      }
   }
}

/**
 *
 **/
void InvTypeMetric::CalculateKEOTerms(map<string, Nb>& aKEOTerms, vector<Nuclei>& aNewStruct)
{
   PopulateMatrices(aNewStruct);
   PopulateMetric();
   PopulateDerMetric();
   Invert(mInvMetric);

   for(In i = I_0; i < mMol.GetNoOfVibs(); ++i)
      mTraces[i] = mInvMetric.TraceProduct(mDerMetrics[i]);

   Nb PseudoPot = C_0;
   MidasVector SingDerTerms(mMol.GetNoOfVibs(), C_0);

   for(In i = I_0; i < mMol.GetNoOfVibs(); ++i)
   {
      for(In j = I_0; j <= i; ++j)
         SingDerTerms[i] += mInvMetric[i][j]*mTraces[j];

      for(In j = i+I_1; j < mMol.GetNoOfVibs(); ++j)
      {
         PseudoPot += C_2*mInvMetric[i][j]*(mTraces[i]*mTraces[j]);
         SingDerTerms[i] += mInvMetric[i][j]*mTraces[j];
      }
      PseudoPot += mInvMetric[i][i]*(mTraces[i]*mTraces[i]);
   }



   for(In i = I_0; i < mMol.GetNoOfVibs(); i++)
      for(In j = i; j < mMol.GetNoOfVibs(); j++)
      {
         stringstream Name;
         Name << "Metric_" << mMol.GetModeLabel(i) << "_" << mMol.GetModeLabel(j);
         aKEOTerms.insert(make_pair(Name.str(), mInvMetric[i][j]));
      }
}
