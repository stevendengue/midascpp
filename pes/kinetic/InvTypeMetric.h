/**
************************************************************************
* 
* @file                InvTypeMetric.h
*
* Created:             06-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Class for collecting the terms of the metric using the
*                     inverse of the whole strategy
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef INVTYPEMETRIC_H
#define INVTYPEMETRIC_H

// std headers
#include <map>
#include <vector>

// midas headers
#include "pes/kinetic/BaseMetric.h"
#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"

// using declarations
using std::map;
using std::vector;

// forward declarations
class GenericMolecule;

class InvTypeMetric 
   :  public BaseMetric
{
   private:
      MidasMatrix*   mDerMetrics;
      MidasMatrix      mInvMetric;
      MidasVector      mTraces;
      void PopulateDerMetric();
      void PopulateMetric();
   public:
      InvTypeMetric(const midas::molecule::MoleculeInfo&);
      ~InvTypeMetric();
      void CalculateKEOTerms(map<string, Nb>& , vector<Nuclei>&);
};

#endif
