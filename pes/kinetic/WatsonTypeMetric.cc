/**
************************************************************************
* 
* @file                WatsonTypeMetric.cc
*
* Created:             06-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Class for collecting the terms of the metric using the
*                    strategy leading to the watsonian in the case of
*                    normal coordinates.
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include "pes/kinetic/WatsonTypeMetric.h"

// std headers
#include <string>
#include <map>
#include <vector>

// midas headers
#include "pes/kinetic/BaseMetric.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasMatrix.h"
#include "pes/molecule/MoleculeInfo.h"

// using declarations
using std::string;
using std::map;
using std::vector;

/**
 *
 **/
WatsonTypeMetric::WatsonTypeMetric
   (  const midas::molecule::MoleculeInfo& aMol
   )
   :  BaseMetric(aMol)
   ,  mInvS(aMol.GetNoOfVibs())
   ,  mInvI(I_3)
   ,  mTraceProds(aMol.GetNoOfVibs())
{
   mIStarDers = new MidasMatrix[aMol.GetNoOfVibs()];
   mCDerInvSC = new MidasMatrix[aMol.GetNoOfVibs()];
   for(In i = I_0; i < aMol.GetNoOfVibs(); ++i)
   {
      mIStarDers[i].SetNewSize(I_3);
      mCDerInvSC[i].SetNewSize(I_3);
   }
}

WatsonTypeMetric::~WatsonTypeMetric()
{
   delete[] mIStarDers;
   delete[] mCDerInvSC;
}

void WatsonTypeMetric::CalculateKEOTerms(map<string, Nb>& aKEOTerms, vector<Nuclei>& aNewStruct)
{
   PopulateMatrices(aNewStruct);
   mInvS = mS;
   Invert(mInvS);
   MidasMatrix temp     = mC*mInvS;
   MidasMatrix transC = Transpose(mC);
   MidasMatrix temp3  = temp*transC;
   mInvI = mI - temp3;
   Invert(mInvI);
   PopulateIStarDers();
   PopulateDerTraces();
   //Do stuff to create the real KEO
   
   /*
   *   Vibrational parts
   */
   

   /*
   *   Inertia parts
   */
   MidasMatrix middle = mInvS*transC*mInvI*mC*mInvS;
}

void WatsonTypeMetric::PopulateDerTraces()
{
   PopulateIStarDers();
   for(In i = I_0; i < mMol.GetNoOfVibs(); ++i)
   {
      mTraceProds[i] = mInvS.TraceProduct(mDerS[i]) + mInvI.TraceProduct(mIStarDers[i]);
   }
}

void WatsonTypeMetric::PopulateIStarDers()
{
   for(In i = I_0; i < mMol.GetNoOfVibs(); ++i)
   {
      for(In j = I_0; j < I_3; ++j)
      {
         for(In k = j+I_1; k < I_3; ++k)
         {
            mIStarDers[i][j][k] = mDerI[i][j][k] + mCDerInvSC[i][j][k];
            for(In l = I_0; l < mMol.GetNoOfVibs(); ++l)
            {
               for(In m = I_0; m < mMol.GetNoOfVibs(); ++m)
               {
                  mIStarDers[i][j][k] -= mDerC[i][j][l]*mInvS[l][m]*mC[m][k];
                  mIStarDers[i][j][k] -= mC[j][l]*mInvS[l][m]*mDerC[i][j][l];
               }
            }
            mIStarDers[i][k][j] = mIStarDers[i][j][k];
         }
      }
   }
}

void WatsonTypeMetric::PopulateCDerInvSC()
{
   for(In i = I_0; i < mMol.GetNoOfVibs(); ++i)
   {
      for(In j = I_0; j < I_3; ++j)
      {
         for(In k = j; k < I_3; ++k)
         {
            mCDerInvSC[i][j][k] = I_0;
            for(In l = I_0; l < mMol.GetNoOfVibs(); ++l)
            {
               for(In m = I_0; m < mMol.GetNoOfVibs(); ++m)
               {
                  for(In n = I_0; n < mMol.GetNoOfVibs(); ++n)
                  {
                     for(In o = I_0; o < mMol.GetNoOfVibs(); ++o)
                     {
                        mCDerInvSC[i][j][k] += mC[l][j]*mInvS[l][n]*mDerS[i][n][o]*mInvS[o][m]*mDerC[i][m][k];
                     }
                  }
               }
            }
            mCDerInvSC[i][k][j] = mCDerInvSC[i][j][k];
         }
      }
   }
}
