/**
************************************************************************
* 
* @file                BaseMetric.cc
*
* Created:             06-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Base class for calculating all the components of the metric
*                    Namely S, C, and I matrices and their derivates
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "pes/kinetic/BaseMetric.h"

// std headers
#include   <vector>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasMatrix.h"
#include "pes/molecule/MoleculeInfo.h"
#include "pes/kinetic/BaseCoordJacobian.h"
#include "pes/kinetic/CoordJacobian.h"
#include "pes/kinetic/LincombCoordJacobian.h"

// using declarations
using std::vector;

/**
 *
 **/
BaseMetric::BaseMetric
   (  const midas::molecule::MoleculeInfo& aMol
   )
   :  mMol(aMol)
   ,  mS(aMol.GetNoOfVibs())
   ,  mC(aMol.GetNoOfVibs(), I_3)
   ,  mI(I_3)
   ,  mMassScaledCoord(I_3, aMol.GetNumberOfNuclei())
{
   //Mout << "Initing : " << mMol << endl;
   mDerI = new MidasMatrix[mMol.GetNoOfVibs()];
   mDerS = new MidasMatrix[mMol.GetNoOfVibs()];
   mDerC = new MidasMatrix[mMol.GetNoOfVibs()];
   for(In i = I_0; i < mMol.GetNoOfVibs(); ++i)
   {
      mDerI[i].SetNewSize(I_3);
      mDerS[i].SetNewSize(mMol.GetNoOfVibs());
      mDerC[i].SetNewSize(mMol.GetNoOfVibs(), I_3);
   }
   Mout << "Done allocating" << endl;
   if(mMol.HasLinComb())
   {
      mJacobians = new LincombCoordJacobian(mMol);
   }
   else
   {
      mJacobians = new CoordJacobian(mMol);
   }
   Mout << "All done" << endl;
}

/**
 *
 **/
BaseMetric::~BaseMetric()
{
   delete    mJacobians;
   delete[]   mDerI;
   delete[] mDerS;
   delete[]   mDerC;
}

/**
 *
 **/
void BaseMetric::PopulateMatrices(vector<Nuclei>& aNewStruct)
{
   mJacobians->CalcJacobian(aNewStruct);
   PopulateMassScaledCoord(aNewStruct);
   ZeroMatrices();
   PopulateS();
   PopulateDerS();
   PopulateC();
   PopulateDerC();
   PopulateI();
   PopulateDerI();
}

void BaseMetric::PopulateMassScaledCoord(vector<Nuclei>& aNewStruct)
{
   for(In i = I_0; i < mMol.GetNumberOfNuclei(); i++)
   {
      mMassScaledCoord[I_0][i] = aNewStruct[i].X()*sqrt(aNewStruct[i].GetMass()*C_FAMU);
      mMassScaledCoord[I_1][i] = aNewStruct[i].Y()*sqrt(aNewStruct[i].GetMass()*C_FAMU);
      mMassScaledCoord[I_2][i] = aNewStruct[i].Z()*sqrt(aNewStruct[i].GetMass()*C_FAMU);
   }
}

/**
*   zeros all the matrices used for accumulating the results
**/
void BaseMetric::ZeroMatrices()
{
   mS.Zero();
   mI.Zero();
   mC.Zero();
   for(In i = I_0; i < mMol.GetNoOfVibs(); ++i)
   {
      mDerI[i].Zero();
      mDerS[i].Zero();
      mDerC[i].Zero();
   }
}

/**
*   Populate S matrix exploiting the symmetry
**/
void BaseMetric::PopulateS()
{
   for(In i = I_0; i < mMol.GetNoOfVibs(); i++)
      for(In j = i; j < mMol.GetNoOfVibs(); j++)
      {
         for(In k = I_0; k < I_3*mMol.GetNumberOfNuclei(); k++)
            mS[i][j] += mJacobians->GetCoordJacEl(k,i)*mJacobians->GetCoordJacEl(k,j);
         mS[j][i] = mS[i][j];
      }
}

/**
*   Populate the derivatives of the S matrix using symmetry
**/
void BaseMetric::PopulateDerS()
{
   for(In i = I_0; i < mMol.GetNoOfVibs(); i++)
      for(In j = i; j < mMol.GetNoOfVibs(); j++)
         for(In k = I_0; k < mMol.GetNoOfVibs(); k++)
         {
            for(In l = I_0; l < I_3*mMol.GetNumberOfNuclei(); l++)
               mDerS[k][i][j] += mJacobians->GetCoordJacEl(l,i)*mJacobians->GetCoordDerJacEl(k,l,j) 
                              + mJacobians->GetCoordJacEl(l,j)*mJacobians->GetCoordDerJacEl(k,l,i);
            mDerS[k][j][i] = mDerS[k][i][j];
         }
}

/**
*   Populate the C matrix
**/
void BaseMetric::PopulateC()
{
   for(In i = I_0; i < mMol.GetNoOfVibs(); i++)
      for(In j = I_0; j < mMol.GetNumberOfNuclei(); j++)
      {
         mC[i][I_0] += mJacobians->GetCoordJacEl(j*I_3+I_2,i)*mMassScaledCoord[I_1][j] 
                     - mJacobians->GetCoordJacEl(j*I_3+I_1,i)*mMassScaledCoord[I_2][j];
         mC[i][I_1] += mJacobians->GetCoordJacEl(j*I_3,i)*mMassScaledCoord[I_2][j] 
                     - mJacobians->GetCoordJacEl(j*I_3+I_2,i)*mMassScaledCoord[I_0][j];
         mC[i][I_2] += mJacobians->GetCoordJacEl(j*I_3+I_1,i)*mMassScaledCoord[I_0][j] 
                     - mJacobians->GetCoordJacEl(j*I_3,i)*mMassScaledCoord[I_1][j];
      }
}

/**
*   Populate the derivatives of C
**/
void BaseMetric::PopulateDerC()
{
   for(In i = I_0; i < mMol.GetNoOfVibs(); i++)
      for(In j = I_0; j < mMol.GetNoOfVibs(); j++)
         for(In k = I_0; k < mMol.GetNumberOfNuclei(); k++)
         {
            mDerC[i][j][I_0]      +=   mJacobians->GetCoordJacEl(k*I_3+I_1,i)*mJacobians->GetCoordJacEl(k*I_3+I_2,j)
                                    -mJacobians->GetCoordJacEl(k*I_3+I_1,j)*mJacobians->GetCoordJacEl(k*I_3+I_2,i)
                                    +mMassScaledCoord[I_1][k]*mJacobians->GetCoordDerJacEl(i,k*I_3+I_2,j)
                                    -mMassScaledCoord[I_2][k]*mJacobians->GetCoordDerJacEl(i,k*I_3+I_1,j);

            mDerC[i][j][I_1]       +=   mJacobians->GetCoordJacEl(k*I_3+I_2,i)*mJacobians->GetCoordJacEl(k*I_3,j)
                                    -mJacobians->GetCoordJacEl(k*I_3+I_2,j)*mJacobians->GetCoordJacEl(k*I_3,i)
                                    +mMassScaledCoord[I_2][k]*mJacobians->GetCoordDerJacEl(i,k*I_3,j)
                                    -mMassScaledCoord[I_0][k]*mJacobians->GetCoordDerJacEl(i,k*I_3+I_2,j);

              mDerC[i][j][I_2]       += mJacobians->GetCoordJacEl(k*I_3,i)*mJacobians->GetCoordJacEl(k*I_3+I_1,j)
                                    -mJacobians->GetCoordJacEl(k*I_3,j)*mJacobians->GetCoordJacEl(k*I_3+I_1,i)
                                    +mMassScaledCoord[I_0][k]*mJacobians->GetCoordDerJacEl(i,k*I_3+I_1,j)
                                    -mMassScaledCoord[I_1][k]*mJacobians->GetCoordDerJacEl(i,k*I_3,j);
         }
}

/**
*   Populate the I matrix
**/
void BaseMetric::PopulateI()
{
   for(In i = I_0; i < mMol.GetNumberOfNuclei(); i++)
   {
      mI[I_0][I_0] +=   mMassScaledCoord[I_1][i]*mMassScaledCoord[I_1][i] 
                        + mMassScaledCoord[I_2][i]*mMassScaledCoord[I_2][i];
      mI[I_1][I_1] +=   mMassScaledCoord[I_0][i]*mMassScaledCoord[I_0][i] 
                        + mMassScaledCoord[I_2][i]*mMassScaledCoord[I_2][i];
      mI[I_2][I_2] +=   mMassScaledCoord[I_1][i]*mMassScaledCoord[I_1][i] 
                        + mMassScaledCoord[I_0][i]*mMassScaledCoord[I_0][i];
   
      mI[I_1][I_0] -=    mMassScaledCoord[I_0][i]*mMassScaledCoord[I_1][i];
      mI[I_2][I_0] -=    mMassScaledCoord[I_0][i]*mMassScaledCoord[I_2][i];
      mI[I_2][I_1] -=    mMassScaledCoord[I_1][i]*mMassScaledCoord[I_2][i];
   }
   mI[I_1][I_2] = mI[I_2][I_1];
   mI[I_0][I_1] = mI[I_1][I_0];
   mI[I_0][I_2] = mI[I_2][I_0];
}
/**
*   Populate the derivatives of I
**/
void BaseMetric::PopulateDerI()
{
   for(In i = I_0; i < mMol.GetNoOfVibs(); i++)
   {
      for(In j = I_0; j < mMol.GetNumberOfNuclei(); j++)
      {
         mDerI[i][I_0][I_0] += C_2*mJacobians->GetCoordJacEl(j*I_3+I_1,i)*mMassScaledCoord[I_1][j]
                               +C_2*mJacobians->GetCoordJacEl(j*I_3+I_2,i)*mMassScaledCoord[I_2][j];
         mDerI[i][I_1][I_1] += C_2*mJacobians->GetCoordJacEl(j*I_3+I_2,i)*mMassScaledCoord[I_2][j]
                               +C_2*mJacobians->GetCoordJacEl(j*I_3,i)*mMassScaledCoord[I_0][j];
         mDerI[i][I_2][I_2] += C_2*mJacobians->GetCoordJacEl(j*I_3+I_1,i)*mMassScaledCoord[I_1][j]
                               +C_2*mJacobians->GetCoordJacEl(j*I_3,i)*mMassScaledCoord[I_0][j];

         mDerI[i][I_1][I_0] += -mJacobians->GetCoordJacEl(j*I_3+I_1,i)*mMassScaledCoord[I_0][j]
                                -mJacobians->GetCoordJacEl(j*I_3,i)*mMassScaledCoord[I_1][j];
         mDerI[i][I_2][I_0] += -mJacobians->GetCoordJacEl(j*I_3,i)*mMassScaledCoord[I_2][j]
                                -mJacobians->GetCoordJacEl(j*I_3+I_2,i)*mMassScaledCoord[I_0][j];
         mDerI[i][I_2][I_1] += -mJacobians->GetCoordJacEl(j*I_3+I_1,i)*mMassScaledCoord[I_2][j]
                              -mJacobians->GetCoordJacEl(j*I_3+I_2,i)*mMassScaledCoord[I_1][j];
      }
      mDerI[i][I_0][I_1] = mDerI[i][I_1][I_0];
      mDerI[i][I_0][I_2] = mDerI[i][I_2][I_0];
      mDerI[i][I_1][I_2] = mDerI[i][I_2][I_1];
   }
}
