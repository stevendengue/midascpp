/**
************************************************************************
* 
* @file                CoordJacobian.cc
*
* Created:             05-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:     Class for calculating an ordenary jacobian for a 
*                        set of coordinates
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include "pes/kinetic/BaseCoordJacobian.h"
#include "pes/kinetic/CoordJacobian.h"

// std headers
#include <vector>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasMatrix.h"
#include "pes/molecule/MoleculeInfo.h"
#include "nuclei/Nuclei.h"

// using declarations
using std::vector;

void CoordJacobian::CalcJacobian(vector<Nuclei>& aNewStruct)
{
   ResetSingDer();
   vector<vector< taylor<Nb, 2, 2> > > Coordinates;
   Nb AccCMX, AccCMY, AccCMZ;
   for(In i = I_0; i < mMolecule.GetNoOfVibs(); i++)
      for(In j = i+I_1; j < mMolecule.GetNoOfVibs(); j++)
      {
         Coordinates.clear();
         AccCMX = C_0;
         AccCMY = C_0;
         AccCMZ = C_0;
         for(In k = I_0; k < mMolecule.GetNumberOfNuclei(); k++)
         {
            mNucleiPossVec[k]->CalculateCoord(Coordinates, i, j);
            Coordinates.emplace_back(mNucleiPossVec[k]->GetCoord());
            mDerJacobians[j][k*I_3][i]     = Coordinates[k][I_0][I_4];
            mDerJacobians[j][k*I_3+I_1][i] = Coordinates[k][I_1][I_4];
            mDerJacobians[j][k*I_3+I_2][i] = Coordinates[k][I_2][I_4];
            mDerJacobians[i][k*I_3][j]     = Coordinates[k][I_0][I_4];
            mDerJacobians[i][k*I_3+I_1][j] = Coordinates[k][I_1][I_4];
            mDerJacobians[i][k*I_3+I_2][j] = Coordinates[k][I_2][I_4];
            AccCMX += Coordinates[k][I_0][I_4]*(aNewStruct[k].GetMass()/mTotalMass);
            AccCMY += Coordinates[k][I_1][I_4]*(aNewStruct[k].GetMass()/mTotalMass);
            AccCMZ += Coordinates[k][I_2][I_4]*(aNewStruct[k].GetMass()/mTotalMass);
         }
         for(In k = I_0; k < mMolecule.GetNumberOfNuclei(); k++)
         {
            mDerJacobians[j][k*I_3][i]     = (mDerJacobians[j][k*I_3][i]     - AccCMX)*sqrt(aNewStruct[k].GetMass());
            mDerJacobians[j][k*I_3+I_1][i] = (mDerJacobians[j][k*I_3+I_1][i] - AccCMY)*sqrt(aNewStruct[k].GetMass());
            mDerJacobians[j][k*I_3+I_2][i] = (mDerJacobians[j][k*I_3+I_2][i] - AccCMZ)*sqrt(aNewStruct[k].GetMass());
            mDerJacobians[i][k*I_3][j]     = (mDerJacobians[i][k*I_3][j]     - AccCMX)*sqrt(aNewStruct[k].GetMass());
            mDerJacobians[i][k*I_3+I_1][j] = (mDerJacobians[i][k*I_3+I_1][j] - AccCMY)*sqrt(aNewStruct[k].GetMass());
            mDerJacobians[i][k*I_3+I_2][j] = (mDerJacobians[i][k*I_3+I_2][j] - AccCMZ)*sqrt(aNewStruct[k].GetMass());
         }

         if(!mSingDerDone[i])
         {
            AccCMX = C_0;
            AccCMY = C_0;
            AccCMZ = C_0;
            for(In k = I_0; k < mMolecule.GetNumberOfNuclei(); k++)
            {
               mJacobian[k*I_3][i]     = Coordinates[k][I_0][I_1];
               mJacobian[k*I_3+I_1][i] = Coordinates[k][I_1][I_1];
               mJacobian[k*I_3+I_2][i] = Coordinates[k][I_2][I_1];
               AccCMX += Coordinates[k][I_0][I_1]*(aNewStruct[k].GetMass()/mTotalMass);
               AccCMY += Coordinates[k][I_1][I_1]*(aNewStruct[k].GetMass()/mTotalMass);
               AccCMZ += Coordinates[k][I_2][I_1]*(aNewStruct[k].GetMass()/mTotalMass);
            }
            for(In k = I_0; k < mMolecule.GetNumberOfNuclei(); k++)
            {
               mJacobian[k*I_3][i]     = (mJacobian[k*I_3][i]     - AccCMX )*sqrt(aNewStruct[k].GetMass());
               mJacobian[k*I_3+I_1][i] = (mJacobian[k*I_3+I_1][i] - AccCMY )*sqrt(aNewStruct[k].GetMass());
               mJacobian[k*I_3+I_2][i] = (mJacobian[k*I_3+I_2][i] - AccCMZ )*sqrt(aNewStruct[k].GetMass());
            }
            AccCMX = C_0;
            AccCMY = C_0;
            AccCMZ = C_0;
            for(In k = I_0; k < mMolecule.GetNumberOfNuclei(); k++)
            {
               mDerJacobians[i][k*I_3][i]     = Coordinates[k][I_0][I_3];
               mDerJacobians[i][k*I_3+I_1][i] = Coordinates[k][I_1][I_3];
               mDerJacobians[i][k*I_3+I_2][i] = Coordinates[k][I_2][I_3];
               AccCMX += Coordinates[k][I_0][I_3]*(aNewStruct[k].GetMass()/mTotalMass);
               AccCMY += Coordinates[k][I_1][I_3]*(aNewStruct[k].GetMass()/mTotalMass);
               AccCMZ += Coordinates[k][I_2][I_3]*(aNewStruct[k].GetMass()/mTotalMass);
            }
            for(In k = I_0; k < mMolecule.GetNumberOfNuclei(); k++)
            {
               mDerJacobians[i][k*I_3][i]     = C_2*(mDerJacobians[i][k*I_3][i]     - AccCMX )*sqrt(aNewStruct[k].GetMass());
               mDerJacobians[i][k*I_3+I_1][i] = C_2*(mDerJacobians[i][k*I_3+I_1][i] - AccCMY )*sqrt(aNewStruct[k].GetMass());
               mDerJacobians[i][k*I_3+I_2][i] = C_2*(mDerJacobians[i][k*I_3+I_2][i] - AccCMZ )*sqrt(aNewStruct[k].GetMass());
            }

            mSingDerDone[i] = true;
         }
      }

   //cout << "Jacobian : " << endl << Jacobian << endl;
   //for(In i = I_0; i < mNumberOfVibs; i++)
   // cout << "DerJacobian[" << i << "] : " <<endl << DerJacobians[i] << endl;
   MoveNucleiTo(Coordinates, aNewStruct);
}

void CoordJacobian::CalcFinalRows(const vector<Nuclei>& aNewStruct)
{
   vector<vector< taylor<Nb, 2, 2> > > Coordinates;
   for(In i = I_0; i < mMolecule.GetNumberOfNuclei(); i++)
   {
      mNucleiPossVec[i]->CalculateCoord(Coordinates, mMolecule.GetNoOfVibs()-I_1, -I_1);
      Coordinates.emplace_back(mNucleiPossVec[i]->GetCoord());
   }
   Nb AccCMX, AccCMY, AccCMZ;
   AccCMX = AccCMY = AccCMZ = C_0;
   for(In i = I_0; i < mMolecule.GetNumberOfNuclei(); i++)
   {
      mDerJacobians[mMolecule.GetNoOfVibs()-I_1][i*I_3][mMolecule.GetNoOfVibs()-I_1]     = Coordinates[i][I_0][I_3];
      mDerJacobians[mMolecule.GetNoOfVibs()-I_1][i*I_3+I_1][mMolecule.GetNoOfVibs()-I_1] = Coordinates[i][I_1][I_3];
      mDerJacobians[mMolecule.GetNoOfVibs()-I_1][i*I_3+I_2][mMolecule.GetNoOfVibs()-I_1] = Coordinates[i][I_2][I_3];
      AccCMX += Coordinates[i][I_0][I_3]*(aNewStruct[i].GetMass()/mTotalMass);
      AccCMY += Coordinates[i][I_1][I_3]*(aNewStruct[i].GetMass()/mTotalMass);
      AccCMZ += Coordinates[i][I_2][I_3]*(aNewStruct[i].GetMass()/mTotalMass);
   }
   for(In i = I_0; i < mMolecule.GetNumberOfNuclei(); i++)
   {
      mDerJacobians[mMolecule.GetNoOfVibs()-I_1][i*I_3][mMolecule.GetNoOfVibs()-I_1]       = 
         C_2*(mDerJacobians[mMolecule.GetNoOfVibs()-I_1][i*I_3][mMolecule.GetNoOfVibs()-I_1]       - AccCMX)*sqrt(aNewStruct[i].GetMass());
      mDerJacobians[mMolecule.GetNoOfVibs()-I_1][i*I_3+I_1][mMolecule.GetNoOfVibs()-I_1]    = 
         C_2*(mDerJacobians[mMolecule.GetNoOfVibs()-I_1][i*I_3+I_1][mMolecule.GetNoOfVibs()-I_1] -   AccCMY)*sqrt(aNewStruct[i].GetMass());
      mDerJacobians[mMolecule.GetNoOfVibs()-I_1][i*I_3+I_2][mMolecule.GetNoOfVibs()-I_1]   = 
         C_2*(mDerJacobians[mMolecule.GetNoOfVibs()-I_1][i*I_3+I_2][mMolecule.GetNoOfVibs()-I_1] -   AccCMZ)*sqrt(aNewStruct[i].GetMass());
   }
   AccCMX = AccCMY = AccCMZ = C_0;
   for(In i = I_0; i < mMolecule.GetNumberOfNuclei(); i++)
   {
      mJacobian[i*I_3][mMolecule.GetNoOfVibs()-I_1]     = Coordinates[i][I_0][I_1];
      mJacobian[i*I_3+I_1][mMolecule.GetNoOfVibs()-I_1] = Coordinates[i][I_1][I_1];
      mJacobian[i*I_3+I_2][mMolecule.GetNoOfVibs()-I_1] = Coordinates[i][I_2][I_1];
      AccCMX += Coordinates[i][I_0][I_1]*(aNewStruct[i].GetMass()/mTotalMass);
      AccCMY += Coordinates[i][I_1][I_1]*(aNewStruct[i].GetMass()/mTotalMass);
      AccCMZ += Coordinates[i][I_2][I_1]*(aNewStruct[i].GetMass()/mTotalMass);
   }
   for(In i = I_0; i < mMolecule.GetNumberOfNuclei(); i++)
   {
      mJacobian[i*I_3][mMolecule.GetNoOfVibs()-I_1]     = 
                           (mJacobian[i*I_3][mMolecule.GetNoOfVibs()-I_1]     - AccCMX)*sqrt(aNewStruct[i].GetMass());
      mJacobian[i*I_3+I_1][mMolecule.GetNoOfVibs()-I_1] = 
                           (mJacobian[i*I_3+I_1][mMolecule.GetNoOfVibs()-I_1] - AccCMY)*sqrt(aNewStruct[i].GetMass());
      mJacobian[i*I_3+I_2][mMolecule.GetNoOfVibs()-I_1] = 
                           (mJacobian[i*I_3+I_2][mMolecule.GetNoOfVibs()-I_1] - AccCMZ)*sqrt(aNewStruct[i].GetMass());
   }
}
