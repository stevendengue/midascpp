/**
************************************************************************
* 
* @file                ZmatNucleiPoss.h
*
* Created:             12-10-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Class for calculating derivatives and coordinates
*                      for Zmat coordinates
* 
* Last modified: Thu Jul 15, 2013  01:13:00
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ZMATNUCLEIPOSS_H 
#define ZMATNUCLEIPOSS_H

// std headers
#include<vector>
#include<string>

// midas headers
#include "mathlib/Taylor/taylor.hpp"
#include "inc_gen/TypeDefs.h"
#include "nuclei/Nuclei.h"
#include "pes/kinetic/BaseNucleiPoss.h"
#include "util/Io.h"
#include "pes/molecule/MoleculeInfo.h"

// using declarations
using std::vector;

// forward declarations
namespace midas
{
namespace molecule
{
class Molecule;
}
}

class ZmatNucleiPoss 
   : public BaseNucleiPoss
{
   private:
      ZmatNucleiPoss();
      ZmatNucleiPoss operator=(const ZmatNucleiPoss&);
      ZmatNucleiPoss(const ZmatNucleiPoss&);
      vector<taylor<double, 2, 2> > Crossprod(const vector<taylor<double, 2, 2> >&,const vector<taylor<double, 2, 2> >&);
   public:
      ZmatNucleiPoss(const Nuclei& aNuclei, const InVector& aDepend, In aNr, const InVector& aVarList, midas::molecule::MoleculeInfo* aMoleculeInfo) 
                     : BaseNucleiPoss(aNuclei, aDepend, aNr, aVarList, aMoleculeInfo) {};
      
      void CalculateCoord(const vector<vector<taylor<double, 2, 2> > >&, In, In);
};

#endif // ZMATNUCLEI_H
