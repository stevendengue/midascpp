/**
************************************************************************
* 
* @file                WatsonTypeMetric.h
*
* Created:             06-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Class for collecting the terms of the metric using the
*                    strategy leading to the watsonian in the case of
*                     normal coordinates.
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef WATSONTYPEMETRIC_H
#define WATSONTYPEMETRIC_H

// std headers
#include <map>
#include <vector>

// midas headers
#include "pes/kinetic/BaseMetric.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"

// using declarations
using std::map;
using std::vector;

// forward declarations
class GenericMolecule;

class WatsonTypeMetric 
   :  public BaseMetric
{
   private:
      MidasMatrix*   mCDerInvSC;
      MidasMatrix*   mIStarDers;
      MidasMatrix    mInvS;
      MidasMatrix    mInvI;
      MidasVector    mTraceProds;
      void PopulateDerTraces();
      void PopulateIStarDers();
      void PopulateCDerInvSC();
   public:
      WatsonTypeMetric(const midas::molecule::MoleculeInfo&);
      ~WatsonTypeMetric();
      void CalculateKEOTerms(map<string, Nb>& , vector<Nuclei>&);
};

#endif //WATSONTYPEMETRIC_H
