/**
************************************************************************
* 
* @file                BaseKinetic.h
*
* Created:             25-10-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Base class for calculating all the components of the metric
*                     Namely S, C, and I matrices and their derivates
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef BASEKINETIC_H
#define BASEKINETIC_H

// std headers
#include<map>
#include<string>
#include<vector>

class Molecule;

/**
 * 
 **/
class BaseKinetic
{
   private:
      Molecule*   mMolecule;

   public:
      BaseKinetic(Molecule* aMolecule) 
         :  mMolecule(aMolecule)
      {
      }

      virtual ~BaseKinetic() {}
      virtual void CalculateKEOTerms(std::map<std::string, Nb>&, std::vector<Nuclei>&) = 0;
};

#endif //BASEKINETIC_H
