/**
************************************************************************
* 
* @file                BaseNucleiPoss.h
*
* Created:             02-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Base class for nuclei poss, providing all the needed interfaces
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef BASENUCLEIPOSS_H
#define BASENUCLEIPOSS_H

// std headers
#include <vector>
#include <string>

// midas headers
#include "mathlib/Taylor/taylor.hpp"
#include "inc_gen/TypeDefs.h"
#include "nuclei/Nuclei.h"
#include "util/Io.h"

// forward declarations
namespace midas
{
namespace molecule
{
class MoleculeInfo; //Forward declaring since we don't need implementation atm.
}
}

class BaseNucleiPoss
{
   protected:
      bool                          mIsDone;
      Nb                            mMass;
      InVector                      mDepend;
      InVector                      mVarList;
      In                            mNucleiNo;
      vector<taylor<Nb, 2, 2> >     mCoord;
      midas::molecule::MoleculeInfo*  mNumDerInfo;

      void ResetmIsDone() {mIsDone = false; mCoord.clear();}
      void CheckDone() const { if(!mIsDone) MIDASERROR("Coordinates requested not calculated yet"); }

   public:
      //! constructor
      BaseNucleiPoss(const Nuclei& aNuclei, const InVector& aDepend, In aNr, const InVector& aVarList, midas::molecule::MoleculeInfo* aMoleculeInfo) 
         : mIsDone(false)
         , mMass(aNuclei.GetMass())
         , mDepend(aDepend)
         , mVarList(aVarList)
         , mNucleiNo(aNr)
         , mCoord(C_1) 
      {
      }
      
      //! ?
      const vector<taylor<Nb, 2, 2> >& GetCoord() const { CheckDone(); return mCoord; }

      //! ?
      const InVector& GetConnections() const {return mDepend;}
      
      //! virtual destructor for base class.
      virtual ~BaseNucleiPoss() {};

      //! ?
      virtual void CalculateCoord(const vector<vector<taylor<double, 2, 2> > >&, In, In) = 0;
};

#endif /* BASENUCLEIPOSS_H */
