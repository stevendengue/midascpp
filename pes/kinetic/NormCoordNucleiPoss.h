/**
************************************************************************
* 
* @file                NormCoordNucleiPoss.h
*
* Created:             02-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Class for calculating the needed derivatives of
*                       normalcoordinate defined nuclei possitions
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef NORMCOORDNUCLEIPOSS_H
#define NORMCOORDNUCLEIPOSS_H

#include <vector>
#include "mathlib/Taylor/taylor.hpp"
#include "inc_gen/TypeDefs.h"
#include "pes/kinetic/BaseNucleiPoss.h"
#include "pes/molecule/MoleculeInfo.h"

class Nuclei;

class NormCoordNucleiPoss 
   : public BaseNucleiPoss
{
   private: 

   public:
      NormCoordNucleiPoss(const Nuclei& aNuclei
                        , const InVector& aDepend
                        , In aNr
                        , const InVector& aVarList
                        , midas::molecule::MoleculeInfo* aMoleculeInfo)
                     : BaseNucleiPoss(aNuclei, aDepend, aNr, aVarList, aMoleculeInfo) 
      {
      };

      void CalculateCoord(const vector<vector<taylor<double, 2, 2> > >&, In, In);
};

#endif //NORMCOORDNUCLEIPOSS_H
