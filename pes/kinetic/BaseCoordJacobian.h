/**
************************************************************************
* 
* @file                BaseCoordJacobian.h
*
* Created:             05-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Baseclass for the jacobian of a set of coordinates
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef BASECOORDJACOBIAN_H
#define BASECOORDJACOBIAN_H

// std headers
#include <vector>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "mmv/MidasMatrix.h"
#include "pes/kinetic/BaseNucleiPoss.h"

class GenericMolecule;
class Nuclei;

class BaseCoordJacobian
{
   protected:
      In                      mNrCart;
      In                      mNrVars;
      Nb                      mTotalMass;
      const midas::molecule::MoleculeInfo&  mMolecule;
      bool*                   mSingDerDone;
      vector<BaseNucleiPoss*> mNucleiPossVec;
      MidasMatrix*            mDerJacobians;
      MidasMatrix             mJacobian;
      
      BaseCoordJacobian();
      BaseCoordJacobian(const BaseCoordJacobian&);
      
      void ResetSingDer() {for(In i = I_0; i < mNrVars; ++i) mSingDerDone[i] = false;}
      void ZeroJacs() {mJacobian.Zero(); for(In i = I_0; i < mNrVars; ++i) mDerJacobians[i].Zero();}
      void MoveNucleiTo(const vector<vector<taylor<Nb, 2, 2> > >& aCoordinates, vector<Nuclei>& aNewStruct);
   public:
      BaseCoordJacobian(const midas::molecule::MoleculeInfo& aMol);
      virtual ~BaseCoordJacobian();
      virtual void CalcJacobian(vector<Nuclei>&) = 0;
      Nb GetCoordJacEl(In aRow,In aCol) {return mJacobian[aRow][aCol];}
      Nb GetCoordDerJacEl(In aDer,In aRow,In aCol) {return mDerJacobians[aDer][aRow][aCol];}
};

#endif //BASECOORDJACOBIAN_H
