/**
************************************************************************
* 
* @file                NormCoordNucleiPoss.cc
*
* Created:             02-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Class for calculating the needed derivatives of
*                      normalcoordinate defined nuclei possitions
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "pes/kinetic/NormCoordNucleiPoss.h"

// std headers
#include <vector>

// midas headers
#include "pes/kinetic/BaseNucleiPoss.h"
//#include "pes/molecule/MoleculeInfo.h"
#include "mathlib/Taylor/taylor.hpp"
#include "inc_gen/TypeDefs.h"
#include "nuclei/Nuclei.h"
#include "util/Io.h"

void NormCoordNucleiPoss::CalculateCoord(const vector<vector<taylor<double, 2, 2> > >& aSetOfCoord, In aDer, In aSecDer)
{
   mCoord.clear();
   taylor<double, 2, 2> x;
   taylor<double, 2, 2> y;
   taylor<double, 2, 2> z;
   x[I_3] = C_0;
   x[I_4] = C_0;
   x[I_5] = C_0;
   y[I_3] = C_0;
   y[I_4] = C_0;
   y[I_5] = C_0;
   z[I_3] = C_0;
   z[I_4] = C_0;
   z[I_5] = C_0;
   
   /*
   *   Per definition of normal coordinates
   */
   x[I_0] = mNumDerInfo->GetNuci(mNucleiNo)->X();
   for(int i = I_0; i < mNumDerInfo->NormalCoord().Nrows(); ++i)
      x[I_0] += mNumDerInfo->GetCurrCoordI(i)*mNumDerInfo->NormalCoord()[i][mNucleiNo*I_3];
   x[I_1] = mNumDerInfo->NormalCoord()[aDer][mNucleiNo*I_3]; 
   x[I_2] = mNumDerInfo->NormalCoord()[aSecDer][mNucleiNo*I_3];
   y[I_0] = mNumDerInfo->GetNuci(mNucleiNo)->Y();
   for(int i = I_0; i < mNumDerInfo->NormalCoord().Nrows(); ++i)
      y[I_0] += mNumDerInfo->GetCurrCoordI(i)*mNumDerInfo->NormalCoord()[i][mNucleiNo*I_3+I_1];
   y[I_1] = mNumDerInfo->NormalCoord()[aDer][mNucleiNo*I_3+I_1]; 
   y[I_2] = mNumDerInfo->NormalCoord()[aSecDer][mNucleiNo*I_3+I_1];
   z[I_0] = mNumDerInfo->GetNuci(mNucleiNo)->Z();
   for(int i = I_0; i < mNumDerInfo->NormalCoord().Nrows(); ++i)
      z[I_0] += mNumDerInfo->GetCurrCoordI(i)*mNumDerInfo->NormalCoord()[i][mNucleiNo*I_3+I_2];
   z[I_1] = mNumDerInfo->NormalCoord()[aDer][mNucleiNo*I_3+I_2]; 
   z[I_2] = mNumDerInfo->NormalCoord()[aSecDer][mNucleiNo*I_3+I_2];
   mIsDone = true;
   mCoord.push_back(x);
   mCoord.push_back(y);
   mCoord.push_back(z);
}
