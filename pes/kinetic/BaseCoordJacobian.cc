/**
************************************************************************
* 
* @file                BaseCoordJacobian.cc
*
* Created:             05-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Baseclass for the jacobian of a set of coordinates
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include "pes/kinetic/BaseCoordJacobian.h"

// std headers
#include <vector>

// midas headers
#include "mathlib/Taylor/taylor.hpp"
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasMatrix.h"
#include "pes/molecule/MoleculeInfo.h"

// using declarations
using std::vector;

BaseCoordJacobian::BaseCoordJacobian
   (  const midas::molecule::MoleculeInfo& aMol
   ) 
   :  mNrCart(aMol.GetNumberOfNuclei()*I_3)
   ,  mNrVars(aMol.GetNoOfVibs())
   ,  mTotalMass(aMol.GetTotalMass())
   ,  mMolecule(aMol)
{
   Mout << "Initing base jac" << endl;
   mJacobian.SetNewSize(mNrCart, mNrVars);
   mDerJacobians = new MidasMatrix[mNrVars];
   mSingDerDone = new bool[mNrVars];
   for(In i = I_0; i < mNrVars; ++i)
   {
      mDerJacobians[i].SetNewSize(mNrCart, mNrVars);
      mSingDerDone[i] = false;
   }
   Mout << "Done allocating" << endl;
   /*for(In i=I_0; i < aInfo.size(); ++i)
   {
      mNucleiPossVec.push_back(new );
   }*/ //Need to create interface for making all the nuclei...
}

BaseCoordJacobian::~BaseCoordJacobian()
{
   delete[] mSingDerDone;
   delete[] mDerJacobians;
   for(In i = I_0; i < mNucleiPossVec.size(); ++i)
      delete mNucleiPossVec[i];
}

void BaseCoordJacobian::MoveNucleiTo(const vector<vector<taylor<Nb, 2, 2> > >& aCoordinates, vector<Nuclei>& aNewStruct)
{
   Nb AccCMX, AccCMY, AccCMZ;
   AccCMX = AccCMY = AccCMZ = C_0;
   for(In i = I_0; i < aNewStruct.size(); ++i)
   {
      AccCMX += aCoordinates[i][I_0][I_0]*(aNewStruct[i].GetMass()/mTotalMass);
      AccCMY += aCoordinates[i][I_1][I_0]*(aNewStruct[i].GetMass()/mTotalMass);
      AccCMZ += aCoordinates[i][I_2][I_0]*(aNewStruct[i].GetMass()/mTotalMass);
   }
   for(In i = I_0; i < aNewStruct.size(); ++i)
      aNewStruct[i].MoveTo(aCoordinates[i][I_0][I_0]-AccCMX, aCoordinates[i][I_1][I_0]-AccCMY, aCoordinates[i][I_2][I_0]-AccCMZ);
}

