/**
************************************************************************
* 
* @file                CoordJacobian.h
*
* Created:             05-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Class for calculating an ordenary jacobian for a 
*                       set of coordinates
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef COORDJACOBIAN_H
#define COORDJACOBIAN_H

// std headers
#include <vector>

// midas headers
#include "pes/kinetic/BaseCoordJacobian.h"

// forward declarations
class GenericMolecule;

class CoordJacobian 
   : public BaseCoordJacobian
{
   private:
      void CalcFinalRows(const std::vector<Nuclei>&);
   public:
      CoordJacobian(const midas::molecule::MoleculeInfo& aMol) : BaseCoordJacobian(aMol) {}
      void CalcJacobian(std::vector<Nuclei>&);
};

#endif //COORDJACOBIAN_H
