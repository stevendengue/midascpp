/**
************************************************************************
* 
* @file                BaseMetric.h
*
* Created:             06-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Base class for calculating all the components of the metric
*                     Namely S, C, and I matrices and their derivates
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef BASEMETRIC_H
#define BASEMETRIC_H

// std headers
#include <map>
#include <vector>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasMatrix.h"
#include "pes/kinetic/BaseCoordJacobian.h"
#include "pes/kinetic/CoordJacobian.h"
#include "pes/kinetic/LincombCoordJacobian.h"
#include "pes/molecule/MoleculeInfo.h"

// using declarations
using std::map;
using std::vector;

// forward declarations
class Nuclei;

class BaseMetric
{
   private:
      void PopulateS();
      void PopulateDerS();
      void PopulateC();
      void PopulateDerC();
      void PopulateI();
      void PopulateDerI();
      void PopulateMassScaledCoord(vector<Nuclei>&);
      void ZeroMatrices();
   
   protected:
      const midas::molecule::MoleculeInfo&  mMol;

      MidasMatrix*         mDerS;
      MidasMatrix*         mDerC;
      MidasMatrix*         mDerI;
      BaseCoordJacobian*   mJacobians;
      
      MidasMatrix          mS;
      MidasMatrix          mC;
      MidasMatrix          mI;
      MidasMatrix          mMassScaledCoord;

      void PopulateMatrices(vector<Nuclei>&);

      BaseMetric();
      BaseMetric(const BaseMetric&);
   
   public:
      BaseMetric(const midas::molecule::MoleculeInfo& aMolecule);
      virtual ~BaseMetric();
      virtual void CalculateKEOTerms(map<string, Nb>& , vector<Nuclei>&) = 0;
};

#endif //BASEMETRIC_H
