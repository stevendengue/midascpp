/**
************************************************************************
* 
* @file                LincombCoordJacobian.cc
*
* Created:             05-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Methods for calculating a jacobian for a set of 
*                     linearly combined coordinate displacements
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// midas headers
#include "pes/kinetic/LincombCoordJacobian.h"
#include "pes/kinetic/BaseCoordJacobian.h"
#include "pes/kinetic/CoordJacobian.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasMatrix.h"
#include "pes/molecule/MoleculeInfo.h"


LincombCoordJacobian::LincombCoordJacobian(const midas::molecule::MoleculeInfo& aMol) 
   :  BaseCoordJacobian(aMol)
   ,  mNormalJac(aMol)
   ,  mInvLinComb(aMol.GetLinComb()) 
{
   Invert(mInvLinComb);
}

void LincombCoordJacobian::CalcJacobian(vector<Nuclei>& aNewStruct)
{
   ZeroJacs();
   mNormalJac.CalcJacobian(aNewStruct); // This will also move aNewStruct to the right possition

   for(In i = I_0; i < mMolecule.GetNoOfVibs(); i++)
      for(In j = I_0; j < mMolecule.GetNoOfVibs(); j++)
         for(In k = I_0; k < mMolecule.GetNumberOfNuclei();k++)
         {
            mJacobian[k*I_3][i]     += mInvLinComb[i][j]*mNormalJac.GetCoordJacEl(k*I_3,j);
            mJacobian[k*I_3+I_1][i] += mInvLinComb[i][j]*mNormalJac.GetCoordJacEl(k*I_3+I_1,j);
            mJacobian[k*I_3+I_2][i] += mInvLinComb[i][j]*mNormalJac.GetCoordJacEl(k*I_3+I_2,j);
         }

   for(In i = I_0; i < mMolecule.GetNoOfVibs(); i++)
      for(In j = I_0; j < mMolecule.GetNoOfVibs(); j++)
         for(In k = I_0; k < mMolecule.GetNoOfVibs(); k++)
            for(In l = I_0; l < mMolecule.GetNoOfVibs(); l++)
               for(In m = I_0; m < mMolecule.GetNumberOfNuclei();m++)
               {
                  mDerJacobians[i][m*I_3][j]        += mInvLinComb[i][k]*mNormalJac.GetCoordDerJacEl(k,m*I_3,l)*mInvLinComb[j][l];
                  mDerJacobians[i][m*I_3+I_1][j]    += mInvLinComb[i][k]*mNormalJac.GetCoordDerJacEl(k,m*I_3+I_1,l)*mInvLinComb[j][l];
                  mDerJacobians[i][m*I_3+I_2][j]    += mInvLinComb[i][k]*mNormalJac.GetCoordDerJacEl(k,m*I_3+I_2,l)*mInvLinComb[j][l];
               }
}
