/**
************************************************************************
* 
* @file                ZmatNucleiPoss.cc
*
* Created:             08-07-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Class for calculating derivatives and coordinates
*                      Zmat coordinates
* 
* Last modified: Thu Jul 15, 2010  01:13:00
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// midas headers
#include "pes/kinetic/BaseNucleiPoss.h"
#include "pes/kinetic/ZmatNucleiPoss.h"

void ZmatNucleiPoss::CalculateCoord(const vector<vector<taylor<double, 2, 2> > >& aSetOfNuclei, In aDer, In aSecDer)
{
   mCoord.clear();
   taylor<double, 2, 2> newEpsilon1(0,0);
   taylor<double, 2, 2> newEpsilon2(0,1);
   taylor<double, 2, 2> x;
   taylor<double, 2, 2> y;
   taylor<double, 2, 2> z;
   if(mNucleiNo == I_0)
   {
      x = C_0;
      y = C_0;
      z = C_0;
   }
   else if(mNucleiNo == I_1)
   {
      taylor<double, 2, 2> dist;
      if(aDer == mVarList[I_0])
         dist = mNumDerInfo->GetCurrCoordI(mVarList[I_0]) + newEpsilon1;
      else if(aSecDer == mVarList[I_0])
         dist = mNumDerInfo->GetCurrCoordI(mVarList[I_0]) + newEpsilon2;
      else
         dist = mNumDerInfo->GetCurrCoordI(mVarList[I_0]);
      x = aSetOfNuclei[mDepend[0]][0];
      y = aSetOfNuclei[mDepend[0]][1];
      z = dist + aSetOfNuclei[mDepend[0]][2];

   }
   else if(mNucleiNo == I_2)
   {
      taylor<double, 2, 2> dist;
      taylor<double, 2, 2> angle;
      if(aDer == mVarList[I_0])
         dist = mNumDerInfo->GetCurrCoordI(mVarList[I_0]) + newEpsilon1;
      else if(aSecDer == mVarList[I_0])
         dist = mNumDerInfo->GetCurrCoordI(mVarList[I_0]) + newEpsilon2;
      else
         dist = mNumDerInfo->GetCurrCoordI(mVarList[I_0]);

      if(aDer == mVarList[I_1])
         angle = mNumDerInfo->GetCurrCoordI(mVarList[I_1]) + newEpsilon1;
      else if(aSecDer == mVarList[I_1])
         angle = mNumDerInfo->GetCurrCoordI(mVarList[I_1]) + newEpsilon2;
      else
         angle = mNumDerInfo->GetCurrCoordI(mVarList[I_1]);
      

      x = aSetOfNuclei[mDepend[0]][0];
      x += dist*sin(angle);

      y = aSetOfNuclei[mDepend[0]][1];
      if(mDepend[0] > mDepend[1])
      {
         z = aSetOfNuclei[mDepend[0]][2];
         z -= dist*cos(angle);
      }
      else
      {
         z = aSetOfNuclei[mDepend[0]][2];
         z += dist*cos(angle);
      }
   }
   else
   {
      taylor<double, 2, 2> dist;
      taylor<double, 2, 2> angle;
      taylor<double, 2, 2> dihedral;
      if(aDer == mVarList[I_0])
         dist = mNumDerInfo->GetCurrCoordI(mVarList[I_0]) + newEpsilon1;
      else if(aSecDer == mVarList[I_0])
         dist = mNumDerInfo->GetCurrCoordI(mVarList[I_0]) + newEpsilon2;
      else
         dist = mNumDerInfo->GetCurrCoordI(mVarList[I_0]);

      if(aDer == mVarList[I_1])
         angle = mNumDerInfo->GetCurrCoordI(mVarList[I_1]) + newEpsilon1;
      else if(aSecDer == mVarList[I_1])
         angle = mNumDerInfo->GetCurrCoordI(mVarList[I_1]) + newEpsilon2;
      else
         angle = mNumDerInfo->GetCurrCoordI(mVarList[I_1]);

      if(aDer == mVarList[I_2])
         dihedral = mNumDerInfo->GetCurrCoordI(mVarList[I_2]) + newEpsilon1;
      else if(aSecDer == mVarList[I_2])
         dihedral = mNumDerInfo->GetCurrCoordI(mVarList[I_2]) + newEpsilon2;
      else
         dihedral = mNumDerInfo->GetCurrCoordI(mVarList[I_2]);
      
      //cout << "dihedral : " << dihedral[0] << endl;
      
      vector<taylor<double, 2, 2> > FirstCoord = aSetOfNuclei[mDepend[0]];
      vector<taylor<double, 2, 2> > SecondCoord = aSetOfNuclei[mDepend[1]];
      vector<taylor<double, 2, 2> > ThirdCoord = aSetOfNuclei[mDepend[2]];

      vector<taylor<double, 2, 2> > E2;

      E2.push_back(SecondCoord[0]-FirstCoord[0]);
      E2.push_back(SecondCoord[1]-FirstCoord[1]);
      E2.push_back(SecondCoord[2]-FirstCoord[2]);

      vector<taylor<double, 2, 2> > E3;

      E3.push_back(ThirdCoord[0]-SecondCoord[0]);
      E3.push_back(ThirdCoord[1]-SecondCoord[1]);
      E3.push_back(ThirdCoord[2]-SecondCoord[2]);

      E3 = Crossprod(E3, E2);

      taylor<double, 2, 2> e2Length = sqrt( E2[0]*E2[0] + E2[1]*E2[1] + E2[2]*E2[2] );
      taylor<double, 2, 2> e3Length = sqrt( E3[0]*E3[0] + E3[1]*E3[1] + E3[2]*E3[2] );

      E2[0] /= e2Length;
      E2[1] /= e2Length;
      E2[2] /= e2Length;

      E3[0] /= e3Length;
      E3[1] /= e3Length;
      E3[2] /= e3Length;

      vector<taylor<double, 2, 2> > E1 = Crossprod(E2, E3);

      //Analytic results for simple internals for a four atomic
      /*cout << "E1 : " << E1[0][0] << " " << E1[1][0] << " " << E1[2][0] << endl;
      cout << "AE1: " << -cos(mNumDerInfo->GetCurrCoord()[I_2]) << " " << Nb(0) << " " << -sin(mNumDerInfo->GetCurrCoord()[I_2]) << endl;
      cout << "E2 : " << E2[0][0] << " " << E2[1][0] << " " << E2[2][0] << endl;
      cout << "AE2: " << -sin(mNumDerInfo->GetCurrCoord()[I_2]) << " " << Nb(0) << " " << cos(mNumDerInfo->GetCurrCoord()[I_2]) << endl;
      cout << "E3 : " << E3[0][0] << " " << E3[1][0] << " " << E3[2][0] << endl;
      cout << "AE3: " << Nb(0) << " " << Nb(1) << " " << Nb(0) << endl;*/

      x = cos(angle)*E2[0];
      x += cos(dihedral)*sin(angle)*E1[0];
      x += sin(dihedral)*sin(angle)*E3[0];
      x *= dist;
      x += FirstCoord[0];

      y = cos(angle)*E2[1];
      y += cos(dihedral)*sin(angle)*E1[1];
      y += sin(dihedral)*sin(angle)*E3[1];
      y *= dist;
      y += FirstCoord[1];

      z = cos(angle)*E2[2];
      z += cos(dihedral)*sin(angle)*E1[2];
      z += sin(dihedral)*sin(angle)*E3[2];
      z *= dist;
      z += FirstCoord[2];
   }
   mIsDone = true;
   mCoord.push_back(x);
   mCoord.push_back(y);
   mCoord.push_back(z);
}

vector<taylor<double, 2, 2> > ZmatNucleiPoss::Crossprod(const vector<taylor<double, 2, 2> >&aFirstVector, const vector<taylor<double, 2, 2> >& aSecondVector)
{
   vector<taylor<double, 2, 2> > result;
   result.emplace_back(aFirstVector[1]*aSecondVector[2]-aFirstVector[2]*aSecondVector[1]);
   result.emplace_back(aFirstVector[2]*aSecondVector[0]-aFirstVector[0]*aSecondVector[2]);
   result.emplace_back(aFirstVector[0]*aSecondVector[1]-aFirstVector[1]*aSecondVector[0]);
   return result;
}
