/**
************************************************************************
* 
* @file                LincombCoordJacobian.h
*
* Created:             05-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Methods for calculating a jacobian for a set of 
*                    linearly combined coordinate displacements
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef LINCOMBCOORDJACOBIAN_H
#define LINCOMBCOORDJACOBIAN_H

// std headers
#include <vector>

// midas headers
#include "pes/kinetic/BaseCoordJacobian.h"
#include "pes/kinetic/CoordJacobian.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasMatrix.h"

// using declarations
using std::vector;

// forward declarations
class Nuclei;
class GenericMolecule;

class LincombCoordJacobian 
   : public BaseCoordJacobian
{
   private:
      CoordJacobian    mNormalJac;
      MidasMatrix      mInvLinComb;
   public:
      LincombCoordJacobian(const midas::molecule::MoleculeInfo& aMol);
      void CalcJacobian(vector<Nuclei>&);
};

#endif //LINCOMBCOORDJACOBIAN_H
