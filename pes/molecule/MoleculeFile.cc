#include "MoleculeFile.h"

#include <string>
#include <istream>

#include "util/MidasStream.h"
#include "mpi/FileToString.h"
#include "input/Input.h"
#include "input/AbsPath.h"

extern MidasStream Mout;

namespace midas
{
namespace molecule
{


/**
 * @brief operator bitwise OR for molecule_field_t
 **/
molecule_field_t operator|(const molecule_field_t& i, const molecule_field_t& j)
{
   return molecule_field_t(int(i) | int(j));
}

/**
 *@brief operator bitwise AND for molecule_field_t
 **/
molecule_field_t operator&(const molecule_field_t& i, const molecule_field_t& j)
{
   return molecule_field_t(int(i) & int(j));
}

/**
 * Constructor for MoleculeFile object.
 *
 * @param aInfo    The molecular file information.
 **/
MoleculeFile::MoleculeFile
   (  const MoleculeFileInfo& aInfo
   )
   :  mFileStream()
   ,  mFileInfo(aInfo)
{
}


/**
 * Constructor from filename, type, and fields to read.
 **/
MoleculeFileInfo::MoleculeFileInfo
   (  const std::string& aFileName
   ,  const std::string& aType
   ,  const MoleculeFields& aFields
   )
   :  mFileName
         (  midas::path::IsRelPath(aFileName)                     // Check if relative path
         ?  gMainDir + "/" + aFileName                            // Place in main dir if relative path
         :  aFileName                                             // Absolute path given
         )
   ,  mType(aType)
   ,  mFields(aFields)
{
   if (  midas::path::IsRelPath(aFileName)
      )
   {
      Mout  << " Relative path given for MoleculeFileInfo. Will place output files in main directory: '" << mFileName << "'" << std::endl;
   }
}

///**
// *
// **/
//bool MoleculeFile::Open(std::ios_base::openmode mode)
//{ 
//   if(mFileStream.is_open()) // if open already we close
//      mFileStream.close();
//
//   mFileStream.open(mFileInfo.FileName(), mode); // try to open existing file
//   
//   if(!mFileStream.is_open()) // if it does not exist we open a new file and truncate it (using trunc)
//      mFileStream.open(mFileInfo.FileName(), mode | std::ios_base::trunc); 
//   
//   return mFileStream.good(); // return goodbit
//}
//
///**
// *
// **/
//bool MoleculeFile::Close() 
//{ 
//   mFileStream.close(); 
//   return !mFileStream.bad(); // we are content if stream is 'not bad'
//}

/**
 * Read molecular file into buffer for later processing.
 **/
bool MoleculeFile::Buffer
   (
   )
{
   if(mFileInfo.GetContents().empty())
   {
      //mFileStream = midas::mpi::FileToStringStream(mFileInfo.FileName()).str();
      
      mFileStream.clear();
      auto filename = input::AbsPathScratch(mFileInfo.FileName());
      mFileStream.str(mpi::FileToStringStream(filename).str());
   }
   else
   {
      //mFileStream = std::stringstream{ mFileInfo.GetContents() };
      
      mFileStream.clear();
      mFileStream.str(mFileInfo.GetContents());
   }
   return true;
}

/**
 *
 **/
bool MoleculeFile::Flush
   (
   )
{
   midas::mpi::StringToFile(mFileInfo.FileName(), mFileStream.str());
   return true;
}

/**
 *
 **/
bool MoleculeFile::SearchKeyword
   (  const std::string& aKeyword
   ,  std::function<std::string(const std::string&)> aParser
   ) 
{
   //MidasWarning("THIS FUNCTION SHOULD BE REVMOED !!!"); // I have commented out the warning, but the use of this function should be reconsidered anyways, i.e. reconsider implementation of MoleculeWriter
   mFileStream.clear(); // clear fail and eof bit
   
   mFileStream.seekg(0, std::ios::beg); // start at beginning of file
   std::string s;
   
   while( std::getline(mFileStream,s) && (aParser(s).find(aKeyword) == s.npos) && !mFileStream.eof() ) 
   { // read file line-by-line until keyword is found in parsed string or eof is hit
   }
   
   // if we are at end-of-file, we did not find keyword, else keyword was found
   if(mFileStream.eof()) 
   {
      mFileStream.clear(); // clear fail and eof bit
      mFileStream.seekg(0, std::ios::end);
      return false; // keyword not found
   }
   else 
   {
      return true; // keyword found
   }
}

} /* namespace molecule */
} /* namespace midas */
