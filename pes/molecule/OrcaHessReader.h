/**
************************************************************************
* 
* @file                OrcaHessReader.h
*
* Created:             24-03-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Implementation of OrcaHessReader class used to read
*                      orca hessian molecule input files. 
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ORCAHESSREADER_H_INCLUDED
#define ORCAHESSREADER_H_INCLUDED

#include "MoleculeReader.h"

namespace midas
{
namespace molecule
{

/**
 * OrcaHessReader - defines how orca hessian files should be read
 **/
class OrcaHessReader
   :  public MoleculeReader
{
   private:
      using key_t = typename MoleculeReader::key_t;
      using return_t = typename MoleculeReader::return_t;

      return_t ReadXYZ(const key_t&, MoleculeInfo&);
      return_t ReadFrequencies(const key_t&, MoleculeInfo&);
      //return_t ReadVibrationalCoord(const key_t&, MoleculeInfo&);
      return_t ReadHessian(const key_t&, MoleculeInfo&);
      
      // give type
      std::string Type() const { return "ORCAHESS_READER"; }

   public:
      // we do not allow copying or default construction
      OrcaHessReader() = delete;
      OrcaHessReader(const OrcaHessReader&) = delete;
      OrcaHessReader& operator=(const OrcaHessReader&) = delete;

      // ctor
      OrcaHessReader(MoleculeFile& aMoleculeFile)
         : MoleculeReader(aMoleculeFile) 
      {
         this->InputDelimeter([](const std::string& str) -> bool { return str == "$orca_hessian_file"; }
                            , [](const std::string& str) -> bool { return str == "$end"; }
                            );
         this->RegisterFunction("$atoms"
                               , molecule_field_t::XYZ
                               , std::bind(&OrcaHessReader::ReadXYZ,this,std::placeholders::_1,std::placeholders::_2)
                               );
         this->RegisterFunction("$vibrational_frequencies"
                               , molecule_field_t::FREQ
                               , std::bind(&OrcaHessReader::ReadFrequencies,this,std::placeholders::_1,std::placeholders::_2)
                               );
         this->RegisterFunction("$hessian"
                               , molecule_field_t::HESSIAN
                               , std::bind(&OrcaHessReader::ReadHessian,this,std::placeholders::_1,std::placeholders::_2)
                               );

      }
};

} /* namespace molecule */
} /* namespace midas */

#endif /* ORCAHESSREADER_H_INCLUDED */
