#ifndef MOLECULEDATA_H_INCLUDED
#define MOLECULEDATA_H_INCLUDED

#include "mmv/MidasMatrix.h"
#include "nuclei/Nuclei.h"

/**
 * origin_type:
 *    Has the data been read from or calculated by the program.
 *    Might be important for logging and debugging purposes/sanity checking.
 **/
enum class origin_type: int { NONE, INPUT, CALCULATED , SYSTEM};
inline origin_type MoleculeDataInput() { return origin_type::INPUT; }
inline origin_type MoleculeDataCalculated() { return origin_type::CALCULATED; }
inline origin_type MoleculeDataSystem() { return origin_type::SYSTEM; }

/**
 * @brief MoleculeData
 *        A class to hold different kinds of molecule data with a corresponding key,
 *        which keeps track of whether the data is available or not,
 *        and whether the data been read in from file, or calculated by the program.
 **/
template<class T>
class MoleculeData
{
   using value_type = T;
   using pointer_type = std::unique_ptr<value_type>; // we use unique_ptr for SBRM (scope bound resource management, also known as RAII)
   using key_type = std::string;

   private:
      pointer_type mData = nullptr; // actual data
      bool mInitialized = false;    // has data been initialized
      key_type mKey = "NONE";       // what type of data do we hold
      origin_type mOrigin = origin_type::NONE; // what is the origin of the data? an input file or calculated by midas

      const key_type& Key() const { return mKey; }
      bool Initialized() const { return mInitialized; }

   public:
      /////
      // ctors
      /////
      MoleculeData() = delete;
      MoleculeData(const MoleculeData&) = delete;
      MoleculeData(const key_type& aKey)
         : mKey(aKey)
      {
      }
      MoleculeData(MoleculeData&&) = default;
      // default dtor
      ~MoleculeData() = default;

      //////
      // initialize data in container
      /////
      template<class... Ts>
      void Initialize(origin_type origin, Ts&&... ts)
      {
         // assert that data is not already initialized
         MidasAssert(!mInitialized, Key() + " already initialized");

         // then initialize the data
         try
         {
            mData = pointer_type(new value_type(std::forward<Ts>(ts)...));
            mInitialized = true;
            mOrigin = origin;
         }
         catch(std::exception& e)
         {
            Mout << e.what() << std::endl;
            MIDASERROR(Key() + " caught exception");
         }
         catch(...)
         {
            MIDASERROR(Key() + " caught something :S");
         }
      }

      //////
      // MBH edit: ReInitialize/overwrite data in container. May need changing!
      /////
      template<class... Ts>
      void ReInitialize(origin_type aOrigin, Ts&&... arTs)
      {
         mInitialized = false;
         Initialize(aOrigin, std::forward<Ts>(arTs)...);
            //^unique_ptr (mData) auto. deletes prev. contents on reassignment
      }
      void UnInit()
      {
         mInitialized = false;
         mData.reset();
         mOrigin = origin_type::NONE;
      }

      //@{
      //! Access MoleculeData
      const pointer_type& operator->() const
      {
         MidasAssert(mInitialized, Key() + " not initialized");
         return mData;
      }
      pointer_type& operator->()
      {
         MidasAssert(mInitialized, Key() + " not initialized");
         return mData;
      }
      const value_type& Get() const
      {
         MidasAssert(mInitialized, Key() + " not initialized");
         return *mData;
      }
      value_type& Get()
      {
         MidasAssert(mInitialized, Key() + " not initialized");
         return *mData;
      }
      //@}

      //!
      operator bool() const { return Initialized(); }

      /////
      // see where data came from, input or calculation
      /////
      const origin_type& Origin() const { return mOrigin; }

      bool IsCalculated() const { return Origin() == origin_type::CALCULATED; }
      void SetCalculated() { mOrigin = MoleculeDataCalculated(); }
};

///////
// the different data types
///////
/**
 *
 **/
class XYZType
{
   private:
      //                                     LABEL       X  Y  Z  ISOTOPE SUBSYSTEM TYPE
      using cartesian_atom_type = std::tuple<std::string,Nb,Nb,Nb,In      ,In,NucTreatType>;
      using cartesian_type = std::vector<cartesian_atom_type>;

      In mNumAtom;
      std::string mUnit;
      std::string mComment;
      cartesian_type mCartesian;

   public:
      XYZType() = default;
      XYZType(const XYZType&) = delete;
      XYZType& operator=(const XYZType&) = delete;
      ~XYZType() = default;

      static const std::string Key() { return "XYZ_READ_IN"; }

      In& NumAtom() { return mNumAtom; }
      const In& NumAtom() const { return mNumAtom; }

      std::string& Unit() { return mUnit; }
      const std::string& Unit() const { return mUnit; }

      std::string& Comment() { return mComment; }
      const std::string& Comment() const { return mComment; }

      void AddAtom(const std::string& aLabel // atom label
                 , Nb aX // x coordinate
                 , Nb aY // y coordinate
                 , Nb aZ // z coordinate
                 , In aIso // isotope number
                 , In aSubSystem = 1 // subsystem number
                 , const NucTreatType& arType = NucTreatType::ACTIVE //
                 )
      {
         mCartesian.emplace_back(aLabel,aX,aY,aZ,aIso,aSubSystem,arType);
      }

      // 'getters'
      const std::string& Label(In i) const { return std::get<0>(mCartesian[i]); }
      Nb X(In i) const { return std::get<1>(mCartesian[i]); }
      Nb Y(In i) const { return std::get<2>(mCartesian[i]); }
      Nb Z(In i) const { return std::get<3>(mCartesian[i]); }
      In Isotope(In i) const { return std::get<4>(mCartesian[i]); }
      In SubSystem(In i) const { return std::get<5>(mCartesian[i]); }
      NucTreatType TreatType(In i) const {return std::get<6>(mCartesian[i]); }

      Nb ConversionToAtomicUnits() const
      {
         Nb conv = C_1;
         if(mUnit == "AA")
         {
            conv /= C_TANG;
         }
         else if(mUnit == "AU")
         { // do nothing
         }
         else
         {
            MIDASERROR("Unknown unit in XYZ input: " + mUnit);
         }
         return conv;
      }
};

using XYZData = MoleculeData<XYZType>;

/**
 *
 **/
class FrequencyType
{
   In mNumFreq;
   std::vector<Nb> mFrequencies;
   std::vector<std::string> mSymLabel;
   std::vector<std::string> mModeLabel;
   std::string mUnit;

   public:
      FrequencyType() = default;

      FrequencyType(const std::vector<Nb>& arFreqs, std::string aUnit = "CM-1")
       : mNumFreq(arFreqs.size())
       , mFrequencies(arFreqs)
       , mSymLabel(mNumFreq)
       , mModeLabel(mNumFreq)
       , mUnit(aUnit)
      {
         for(In i=0; i<mNumFreq; ++i)  mSymLabel[i] = "A"; //symmetry unknown
         for(In i=0; i<mNumFreq; ++i)  mModeLabel[i] = "Q"+std::to_string(i); //
      }

      FrequencyType(std::vector<Nb>&& arFreqs, std::string aUnit = "CM-1")
       : mNumFreq(arFreqs.size())
       , mFrequencies(std::move(arFreqs))
       , mSymLabel(mNumFreq)
       , mModeLabel(mNumFreq)
       , mUnit(aUnit)
      {
         for(In i=0; i<mNumFreq; ++i)  mSymLabel[i] = "A"; //symmetry unknown
         for(In i=0; i<mNumFreq; ++i)  mModeLabel[i] = "Q"+std::to_string(i); //
      }

      static const std::string Key() { return "FREQUENCY_READ_IN"; }

      In& NumFreq() { return mNumFreq; }
      const In& NumFreq() const { return mNumFreq; }

      std::string& Unit() { return mUnit; }
      const std::string& Unit() const { return mUnit; }

      //!
      void AddFrequency
         (  Nb freq
         ,  const std::string& sym_label
         )
      {
         AddFrequency
            (  freq,sym_label
            ,  "Q" + std::to_string(mModeLabel.size())
            );
      }

      //!
      void AddFrequency
         (  Nb freq
         ,  const std::string& sym_label
         ,  const std::string& mode_label
         )
      {
         mFrequencies.emplace_back(freq);
         mSymLabel.emplace_back(sym_label);

         MidasAssert(std::find(mModeLabel.begin(),mModeLabel.begin(),mode_label)==mModeLabel.begin(),"Trying to assign the same mode label twice");
         mModeLabel.emplace_back(mode_label);
      }
      Nb Frequency(In i) const { return mFrequencies[i]; }
      const std::string& SymLabel(In i) const { return mSymLabel[i]; }
      const std::string& ModeLabel(In i) const { return mModeLabel[i]; }

      Nb ConversionToInverseCM() const
      {
         Nb conv = C_1;
         if(mUnit == "CM-1")
         {
            conv = C_1;
         }
         else if(mUnit == "AU")
         {
            conv = C_AUTKAYS;
         }
         else
         {
            MIDASERROR("Unknown unit: " + mUnit);
         }
         return conv;
      }
};

using FrequencyData = MoleculeData<FrequencyType>;

/***
 *
 ***/
class VibrationalCoordinateType
{
   std::vector<std::vector<Nb> > mVibrationalCoordinates;
   std::string mUnit;
   std::string mType = "NORMALCOORD";

   //Some flags for editing the norm.coords. after having read in the vib.coord.data,
   //needed e.g. when reading Turbomole norm.coords.
   bool mNeedsInvSqrtMassScaling = false;
   bool mNeedsTrimOfTransRotCoords = false;

   public:

      VibrationalCoordinateType() = default;

      VibrationalCoordinateType
         (  const std::vector<std::vector<Nb> >& arVibCoord
         ,  std::string aUnit = "AU"
         ,  std::string aType = "NORMALCOORD"
         )
         :  mVibrationalCoordinates(arVibCoord)
         ,  mUnit(aUnit)
         ,  mType(aType)
      {
      }

      VibrationalCoordinateType
         (  std::vector<std::vector<Nb> >&& arVibCoord
         ,  std::string aUnit = "AU"
         ,  std::string aType = "NORMALCOORD"
         )
         :  mVibrationalCoordinates(std::move(arVibCoord))
         ,  mUnit(aUnit)
         ,  mType(aType)
      {
      }

      static const std::string Key() { return "VIBRATIONAL_COORDINATE_READ_IN"; }

      std::string& Unit() { return mUnit; }
      const std::string& Unit() const { return mUnit; }

      std::string& Type() { return mType; }
      const std::string& Type() const { return mType; }

      In NumCoords() const { return mVibrationalCoordinates.size(); }
      In NumDisplacements() const { return mVibrationalCoordinates.size() > 0 ? mVibrationalCoordinates[0].size() : 0; }

      void AddCoordinate(const std::vector<Nb>& coord) { mVibrationalCoordinates.emplace_back(coord); }

      const std::vector<Nb>& Coordinate(In i) const { return mVibrationalCoordinates[i]; }

      //Flags for editing coords. after read-in, needed for e.g. Turbomole:
      bool& NeedsInvSqrtMassScaling()   {return mNeedsInvSqrtMassScaling;}
      bool& NeedsTrimOfTransRotCoords() {return mNeedsTrimOfTransRotCoords;}
      const bool& NeedsInvSqrtMassScaling()   const {return mNeedsInvSqrtMassScaling;}
      const bool& NeedsTrimOfTransRotCoords() const {return mNeedsTrimOfTransRotCoords;}

      Nb ConversionToAtomicUnits() const
      {
         Nb conv = C_1;
         if (mUnit == "AA")
         {
            conv /= C_TANG;

            if (gIoLevel > I_8)
            {
               Mout << " Displacement coordinates readin in Aangstrom " << std::endl;
            }
         }
         else if (mUnit == "AU")
         {
            if (gIoLevel > I_8)
            {
               Mout << " Displacement coordinates readin in atomic units " << std::endl;
            }
         }
         else
         {
            MIDASERROR("Unknown unit in Vibrational Coordinate input: " + mUnit);
         }
         return conv;
      }
};

using VibrationalCoordinateData = MoleculeData<VibrationalCoordinateType>;

/**
 *
**/
class InternalCoordType
{
   private:

   //! Number of internal coordinates
   In mNoInternalCoords;

   //! Values of internal coordinates, (at the reference)
   std::vector<Nb> mInternalCoord;

   //! The type of vibrational coordinate set
   std::string mVibCoordType = "INTERNALCOORD";

   //! Unit for internal distances
   std::string mUnitDist;

   //! Unit for internal angles
   std::string mUnitAng;

   // Type of internal, i.e. distance (R), valence angle (V), dihedral angle (D)
   std::vector<std::string> mTypeLabel;

   //! Mode label, i.e. Qx
   std::vector<std::string> mModeLabel;

   //! Symmetry label, (note that this is not really used for anything with internal coordinates!)
   std::vector<std::string> mSymLabel;

   public:

      // Default constructor
      InternalCoordType() = default;

      // Constructor
      InternalCoordType
         (  const std::vector<Nb>& arInternalCoord
         ,  std::string aType = "INTERNALCOORD"
         ,  std::string aUnitDist = "AU"
         ,  std::string aUnitAng = "RAD"
         )
         :  mInternalCoord(arInternalCoord)
            ,  mVibCoordType(aType)
            ,  mUnitDist(aUnitDist)
            ,  mUnitAng(aUnitAng)
            ,  mTypeLabel(arInternalCoord.size())
            ,  mModeLabel(arInternalCoord.size())
            ,  mSymLabel(arInternalCoord.size())
      {
         for (In i = I_0; i < arInternalCoord.size(); ++i)
         {
            mTypeLabel[i] = "N"; // N for unknown mode type
            mModeLabel[i] = "Q" + std::to_string(i);
            mSymLabel[i] = "A";  // A for unknown symmetry
         }
      }

      //
      static const std::string Key() {return "INTERNAL_COORDINATE_READ_IN";}

      //
      void AddCoordinate
         (  const Nb& coord
         ,  const std::string& aTypeLabel
         )
      {
         AddCoordinate
         (  coord
         ,  aTypeLabel
         ,  "Q" + std::to_string(mModeLabel.size())
         );
      }

      //
      void AddCoordinate
         (  const Nb& coord
         ,  const std::string& aTypeLabel
         ,  const std::string& aModeLabel
         )
      {
         mInternalCoord.emplace_back(coord);
         mTypeLabel.emplace_back(aTypeLabel);
         mSymLabel.emplace_back("A");

         MidasAssert(std::find(mModeLabel.begin(), mModeLabel.begin(), aModeLabel) == mModeLabel.begin(), "Attempting to assign the same mode label twice!");
         mModeLabel.emplace_back(aModeLabel);
      }

      // Convert distances to atomic units if input in Aangström
      Nb ConversionToAtomicUnits() const
      {
         Nb conv = C_1;
         if (mUnitDist == "AA")
         {
            conv /= C_TANG;

            if (gIoLevel > I_5)
            {
               Mout << " Internal coordinates (distance) readin in Aangstrom and converted to atomic units " << std::endl;
            }
         }
         else if (mUnitDist == "AU")
         {
            if (gIoLevel > I_5)
            {
               Mout << " Internal coordinates (distance) readin in atomic units and will not be converted " << std::endl;
            }
         }
         else
         {
            MIDASERROR("Unknown unit in polyspherical molecule input: " + mUnitDist);
         }

         return conv;
      }

      // Convert angles to radians if input in degrees
      Nb ConversionToRadianUnits() const
      {
         Nb conv = C_1;
         if (mUnitAng == "DEG")
         {
            conv *= (C_PI/C_180);

            if (gIoLevel > I_5)
            {
               Mout << " Internal coordinates (angles) readin in degrees and converted to radians " << std::endl;
            }
         }
         else if (mUnitAng == "RAD")
         {
            if (gIoLevel > I_5)
            {
               Mout << " Internal coordinates (angles) readin in radians and will not be converted " << std::endl;
            }
         }
         else
         {
            MIDASERROR("Unknown unit in polyspherical molecule input: " + mUnitAng);
         }
         return conv;
      }

      //@{
      //! InternalCoordType class (const) getters
      const std::string& GetmVibCoordType() const              {return mVibCoordType;}
      const Nb& GetmInternalCoord(const In& aIn) const         {return mInternalCoord[aIn];}
      const In& GetmNoInternalCoords() const                   {return mNoInternalCoords;}
      const std::string& GetmUnitDist() const                  {return mUnitDist;}
      const std::string& GetmUnitAng() const                   {return mUnitAng;}
      const std::string& GetmTypeLabel(const In& aIn) const    {return mTypeLabel[aIn];}
      const std::string& GetmModeLabel(const In& aIn) const    {return mModeLabel[aIn];}
      const std::string& GetmSymLabel(const In& aIn) const     {return mSymLabel[aIn];}
      //@}

      //@{
      //! InternalCoordType class setters
      void SetmNoInternalCoords(const In& aIn)                 {mNoInternalCoords = aIn;}
      void SetmUnitDist(const std::string& aStr)               {mUnitDist = aStr;}
      void SetmUnitAng(const std::string& aStr)                {mUnitAng = aStr;}
      //@}
};

using InternalCoordData = MoleculeData<InternalCoordType>;

/**
 *
**/
class InactiveModesType
{ 
   private:
   
   //! Number of inactive modes
   In mNoInactiveModes;
   
   //! Inactive mode labels 
   std::vector<std::string> mInactiveModeLabels;
   
   public:

      // Default constructor
      InactiveModesType() = default;

      // Constructor
      InactiveModesType
         (  const std::vector<std::string>& arInactiveModeLabels 
         )
         :  mNoInactiveModes(arInactiveModeLabels.size())
         ,  mInactiveModeLabels(arInactiveModeLabels)
      {

      }
      
      //
      static const std::string Key() {return "INACTIVE_MODES_READ_IN";}
      
      //@{
      //! InactiveModesType class getters
      const In& GetmNoInactiveModes() const                             {return mNoInactiveModes;}
      const std::vector<std::string>& GetmInactiveModeLabels() const    {return mInactiveModeLabels;}
      const std::string& GetmInactiveModeLabel(const In& aIn) const     {return mInactiveModeLabels[aIn];}
      //@}
      
      //@{
      //! InactiveModesType class setters
      void SetmNoInactiveModes(const In& aIn)                           {mNoInactiveModes = aIn;}
      void SetmInactiveModeLabels(const std::vector<std::string>& aVec) {mInactiveModeLabels = aVec;}   
      //@}
};

using InactiveModesData = MoleculeData<InactiveModesType>;

/***
 *
 ***/
class SigmaType
{
   std::vector<std::vector<Nb> > mSigmas;
   std::string mUnit;
   std::string mType = "SIGMA";

   public:
      static const std::string Key() { return "SIGMA_READ_IN"; }

      std::string& Unit() { return mUnit; }
      const std::string& Unit() const { return mUnit; }

      std::string& Type() { return mType; }
      const std::string& Type() const { return mType; }

      In NumSigma() const { return mSigmas.size(); }
      In NumDisplacements() const { return mSigmas.size() > 0 ? mSigmas[0].size() : 0; }

      void AddCoordinate(const std::vector<Nb>& coord) { mSigmas.emplace_back(coord); }

      const std::vector<Nb>& Sigma(In i) const { return mSigmas[i]; }

      Nb ConversionToAtomicUnits() const
      {
         Nb conv = C_1;
         if (mUnit == "AA")
         {
            conv /= C_TANG;

            if (gIoLevel > I_8)
            {
               Mout << " Displacement coordinates readin in Aangstrom " << std::endl;
            }
         }
         else if (mUnit == "AU")
         {
            if (gIoLevel > I_8)
            {
               Mout << " Displacement coordinates readin in atomic units " << std::endl;
            }
         }
         else
         {
            MIDASERROR("Unknown unit in Sigma input: " + mUnit);
         }
         return conv;
      }
};

using SigmaData = MoleculeData<SigmaType>;
/***
 *
 ***/
class LinCombType
{
   std::vector<std::vector<Nb> > mLinComb;

   public:
      static const std::string Key() { return "LIN_COMB_READ_IN"; }

      void AddLinComb(const std::vector<Nb>& lin_comb) { mLinComb.emplace_back(lin_comb); }

      In NumLinComb() const { return mLinComb.size(); }
      In NumDisplacements() const { return mLinComb.size() > 0 ? mLinComb[0].size() : 0; }

      const std::vector<Nb>& LinComb(In i) const { return mLinComb[i]; }
};

using LinCombData = MoleculeData<LinCombType>;

/**
 *
 **/
class SelectVibsType
{
   std::vector<In> mSelectVibs;

   public:
      static const std::string Key() { return "SELECTED_VIBS_READ_IN"; }

      void AddSelectVib(In i) { mSelectVibs.emplace_back(i); }

      In NumSelectVibs() const { return mSelectVibs.size(); }

      In SelectVib(In i) const { return mSelectVibs[i]; }
};

using SelectVibsData = MoleculeData<SelectVibsType>;

/**
 *
 **/
class ZmatType
{
   enum class const_type: int { VARIABLE, CONSTANT }; // is a variable constant or 'variable'
   class variable_pair: public std::pair<const_type,Nb> // internal class to define a variable
   {
      public:
         variable_pair(const_type c, Nb val): std::pair<const_type,Nb>(c,val) { }

         bool is_constant() const { return this->first == const_type::CONSTANT; }
         bool is_variable() const { return this->first == const_type::VARIABLE; }
   };
   using variable_type = std::map<std::string,variable_pair>;

   // zmat_line_type: element-label, atom 1, bond-length, atom 2, bond-angle, atom 3, dihedral-angle
   using zmat_line_type = std::vector<std::string>;
   //                           zmat line       ISO SUB (ISO and SUB are not yet used for anything)
   using atom_type = std::tuple<zmat_line_type, In, In>;
   using zmat_type = std::vector<atom_type>;

   std::string mUnit;
   zmat_type mZmat;
   variable_type mVariables;
   bool mVariablesFromXYZ = false; // if true variables are calculated from XYZ data and mVariables is empty,
                                   // else all variables must be present in mVariables
                                   // If variables are calculated from XYZ all labels must be identical in ZMAT and XYZ!

   public:
      static const std::string Key() { return "ZMAT_READ_IN"; }

      std::string& Unit() { return mUnit; }
      const std::string& Unit() const { return mUnit; }

      bool& VariablesFromXYZ() { return mVariablesFromXYZ; }
      const bool& VariablesFromXYZ() const { return mVariablesFromXYZ; }

      In NumVariables() const { return mVariables.size(); }

      void AddAtom(const std::vector<std::string>& atom, In iso=0, In sub=0) { mZmat.emplace_back(atom,iso,sub); }
      void AddVariable(const std::string& var, Nb val) { mVariables.insert({var, {const_type::VARIABLE, val} }); }
      void AddConstant(const std::string& var, Nb val) { mVariables.insert({var, {const_type::CONSTANT, val} }); }

      In NumAtoms() const { return mZmat.size(); }
      //const atom_type& Atom(In i) const { return mZmat.at(i); }
      zmat_type::const_iterator BeginAtoms() { return mZmat.begin(); }
      zmat_type::const_iterator EndAtoms() { return mZmat.end(); }

      variable_pair Variable(const std::string& key)
      {
         variable_pair val = {const_type::CONSTANT, 0.0};
         auto iter = mVariables.find(key);
         //if((auto iter = mVariables.find(key)) != mVariables.end())
         if(iter != mVariables.end())
         {
            val = iter->second;
         }
         else
         {
            MIDASERROR("Did not find VAR: " + key);
         }
         return val;
      }
};

using ZmatData = MoleculeData<ZmatType>;

/**
 *
 **/
class HessianType
   : public MidasMatrix
{
   public:
      HessianType() = default;
      //HessianType(In i, In j): MidasMatrix(i,j) { }
      HessianType(const MidasMatrix& mat): MidasMatrix(mat)
      {
         MidasAssert(MidasMatrix::Nrows() == MidasMatrix::Ncols(), "hessian not square");
      }

      HessianType(MidasMatrix&& mat): MidasMatrix(std::move(mat))
      {
         MidasAssert(MidasMatrix::Nrows() == MidasMatrix::Ncols(), "hessian not square");
      }

      static const std::string Key() { return "HESSIAN_READ_IN"; }

      In NumElem() const { return MidasMatrix::Nrows(); }
};

using HessianData = MoleculeData<HessianType>;

/**
 *
 **/
class GradientType
   : public MidasVector
{
   public:
      GradientType() = default;

      GradientType(const MidasVector& vec): MidasVector(vec) {}
      GradientType(MidasVector&& vec): MidasVector(vec) {}

      static const std::string Key() { return "GRADIENT_READ_IN";}

      In NumElem() const {return MidasVector::Size();}
};

using GradientData = MoleculeData<GradientType>;

#endif /* MOLECULEDATA_H_INCLUDED */
