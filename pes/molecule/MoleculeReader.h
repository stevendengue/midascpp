/*
************************************************************************
* 
* @file                MoleculeReader.h
*
* Created:             24-03-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Interface to read molecule files.
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MOLECULEREADER_H_INCLUDED
#define MOLECULEREADER_H_INCLUDED

#include <fstream>
#include <string>
#include <memory>

#include "libmda/util/multiple_return.h"

#include "util/AbstractFactory.h"
#include "util/Error.h"

#include "pes/molecule/MoleculeFile.h"
#include "pes/molecule/MoleculeData.h"

namespace midas
{
namespace molecule
{

class MoleculeInfo; 
class MoleculeReader; // forward declare
struct MoleculeReaderTraits // define some types for MoleculeReader
{
   using key_t = std::string; // key type is an ordinary string (a line read from file)
   using return_t = libmda::util::return_type<key_t,bool>; // multiple return type. key + bool designating whether the key returned should be processed by reader loop
   using function_t = std::function<return_t(const key_t&, MoleculeInfo&)>; // registered functions must take a key and a MoleculeInfo and return a (key, bool) 'pair'
   using field_t = molecule_field_t; // field type is described in MoleculeFile.h
   using function_field_t = std::pair<field_t, function_t>; // (field, function) pair
   using function_map_t = std::map<key_t, function_field_t>; // (key, (field, function)) map of registered functions
};

/**
 * Factory to create molecule readers
 **/
using MoleculeReaderFactory = AbstractFactory<MoleculeReader*(MoleculeFile&), std::string>;

// define how to register as a molecule reader (see e.g. top of MidasReader.cc for usage example)
template<class A>
using MoleculeReaderRegistration = AbstractFactoryRegistration<MoleculeReader*(MoleculeFile&), A, std::string>;

/**
 * MoleculeReader class - interface to read molecular input files in different formats
 **/
class MoleculeReader
{
   protected:
      // some type definitions (see MoleculeReaderTraits for descriptions)
      using key_t = MoleculeReaderTraits::key_t;
      using return_t = MoleculeReaderTraits::return_t;
      using function_t = MoleculeReaderTraits::function_t;
      using field_t = MoleculeReaderTraits::field_t;
      using function_field_t = MoleculeReaderTraits::function_field_t;
      using function_map_t = MoleculeReaderTraits::function_map_t;
      using function_map_const_iterator_t = typename function_map_t::const_iterator;
      using delimeter_function_t = std::function<bool(const string&)>;
   
   private:
      function_map_t mFunctionMap; ///< registered functions
      delimeter_function_t mInputBegin = nullptr; ///< input begins after this has been read
      delimeter_function_t mInputEnd = nullptr; ///< input ends after this has been read

      bool InputBegin(const key_t& aKey) { return (mInputBegin ? mInputBegin(aKey) : true); } ///< are we at beginning of input? 
      bool InputEnd(const key_t& aKey) { return (mInputEnd ? mInputEnd(aKey) : false); } ///< are we at end of input?

      function_map_const_iterator_t FindFunction(const key_t& aKey); ///< search for function given a key

      //////
      // virtual functions
      //////
      virtual std::string Type() const = 0; ///< derived types must implement specific Type function (mostly for debug, and to force MoleculeReader to be abstract)
      virtual key_t ParseKey(const key_t& aKey) const { return aKey; } ///< function to parse keywords before searching function map (default do nothing)
      
      //////
      // Factory Interface 
      //////
      //! factory interface
      static std::unique_ptr<MoleculeReader> Factory(const std::string& aType
                                                   , MoleculeFile& aMoleculeFile
                                                   )
      {
         return MoleculeReaderFactory::create(aType, aMoleculeFile);
      }
      
      //////
      // reader driver
      //////
      bool Read(MoleculeInfo&);  ///< the driver function that controls reading the molecule file

   protected:
      MoleculeFile& mMoleculeFile; ///< molecule file: (protected: used by derived types)
      
      /////
      // ctor (protected: can only be called from derived types)
      /////
      MoleculeReader(MoleculeFile& aMoleculeFile) ///< protected ctor to be called from derived classes
         : mMoleculeFile(aMoleculeFile) 
      {
      } 
      
      /////
      // setup derived readers
      /////
      void RegisterFunction(const key_t&, const field_t&, const function_t&); ///< register (field, function) pair for keyword
      void InputDelimeter(const delimeter_function_t&, const delimeter_function_t&); ///< set input begin and end delimeters

   public: // public interface starts here!
      /////
      // deleted functions (we do not allow copying)
      /////
      MoleculeReader() = delete; ///< we do not allow default construction
      MoleculeReader(const MoleculeReader&) = delete; ///< we do not allow copy construction
      MoleculeReader& operator=(const MoleculeReader&) = delete; ///< we do not allow copy assignment
      
      /////
      // pure virtual dtor (pure virtual such that an object of type MoleculeReader cannot be created)
      /////
      virtual ~MoleculeReader() = 0; ///< pure virtual dtor to make MoleculeReader abstract
      
      
      return_t ReadKeys(std::string& s, bool already_read, MoleculeInfo& aMolecule); ///< read molecule keys
      
      ////////////////////////////////////////////////////////////////////////////////////////
      //
      // Interface (use only this function to invoke/interact with the molecule reader!)
      //
      ////////////////////////////////////////////////////////////////////////////////////////
      static bool ReadMolecule(const MoleculeFileInfo::Set& aSet, MoleculeInfo& aMolecule); ///< interface to read molecule files given a set of MoleculeFileInfo's

};

// define dtor
inline MoleculeReader::~MoleculeReader() { }

/**
 * Handle class (ensures we do not try to use a nullptr)
 **/
class MoleculeReaderHandle
{
   private:
      //! Actual reader
      std::unique_ptr<MoleculeReader> mReader;

   public:
      //! We do not allow default construction
      MoleculeReaderHandle() = delete; 
      
      //! Ctor
      MoleculeReaderHandle(std::unique_ptr<MoleculeReader> aReader): mReader(std::move(aReader))
      {
      } 
      
      //! Access manages MoleculeReader
      std::unique_ptr<MoleculeReader>& operator->() 
      {
         MidasAssert(bool(mReader),"MoleculeReader is Null");
         return mReader;
      } 
};

} /* namespace molecule */
} /* namespace midas */

#endif /* MOLECULEREADER_H_INCLUDED */
