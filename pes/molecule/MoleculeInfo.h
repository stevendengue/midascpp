/**
************************************************************************
*
* @file                MoleculeInfo.h
*
* Created:             02-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*                      Ian H. Godtliebsen (ian@chem.au.dk): did some refactorisation
*
* Short Description:   Definitions for a molecule
*
* Last modified:       09-08-2015 (carolin)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MOLECULEINFO_H_INCLUDED
#define MOLECULEINFO_H_INCLUDED

// std headers
#include <string>
#include <vector>
#include <fstream>
#include <map>

// midas headers
#include "pes/kinetic/BaseNucleiPoss.h"
#include "nuclei/Nuclei.h"
#include "mmv/MidasMatrix.h"
#include "mmv/MidasVector.h"
#include "pes/molecule/MoleculeFile.h"
#include "pes/molecule/MoleculeData.h"

class System;

namespace midas
{
namespace molecule
{
namespace detail
{

// Modify the molecular frame
void ModifyMolecularFrame(MoleculeInfo& aMolecule);

}

/**
 * @class MoleculeInfo
 *    Holds information about molecules e.g. atomic positions.
 *    Data can either be read from file, or calculated by the program if
 *    if other relevant information is present.
 **/
class MoleculeInfo
{
   private:
      //! Is the molecule linear?
      bool Linear() const;

      //! Calculate reduced mass and save
      void CalcRedMass();

      //!@{
      //! Some flags for editing the norm.coords. after having read in the vib.coord.data, needed e.g. when reading Turbomole norm.coords.
      //! Does the coordinates need scaling?
      bool mNeedsInvSqrtMassScaling = false;
      //! Removed trans/vib coordinates.
      bool mNeedsTrimOfTransRotCoords = false;
      //!@}
      
      //
      friend void detail::ModifyMolecularFrame(MoleculeInfo& aMolecule);

      // Actual zmat stuff
      MidasVector                mCoordList; // ??
      MidasVector                mCurrCoord; // ??
      vector<BaseNucleiPoss*>    mNuclei; // ??

      // wtf
      In GetNucleiNrFirstTime(const string&, In, std::map<string, In>&, std::map<string, In>&, std::map<In, In>&, bool);
      In GetNucleiNr         (const string&, const std::map<string, In>&, const std::map<In, In>&, bool);

      //!@{
      //! Stuff that can be read in or calculated
      //! XYZ molecular geometry (as of now this is required, I think)
      MoleculeData<std::vector<Nuclei> > mCoordinates    = {"XYZ"};
      //! Normal coordinates (this could be generalized)
      MoleculeData<MidasMatrix> mNormalCoord             = {"NORMCORD"};
      //! Internal (curvilinear) coordinates
      MoleculeData<MidasVector> mInternalCoord           = {"INTERNALCOORD"};
      //! Inactive mode labels
      MoleculeData<std::vector<std::string> > mInactiveModes = {"INACTIVEMODES"};
      //! Sigma vectors
      MoleculeData<MidasMatrix> mSigma                   = {"SIGMA"};
      //! Vibrational frequencies
      MoleculeData<MidasVector> mFreqs                   = {"FREQ"};
      //! Mode labels for each mode (join with freq?)
      std::vector<std::string> mModeLabels;
      //! Symmetry labels for each mode (join with freq?)
      std::vector<std::string> mSymLabels;
      //! Type (of internal) labels for each mode (join with mInternalCoord?)
      std::vector<std::string> mTypeLabels;
      //!
      std::vector<std::string> mInactiveModeLabels;
      //!
      MidasVector mInternalActiveCoords;
      //!
      std::vector<In> mInactiveModeNrs;
      //!@}

      //!@{
      //! midas.mol specific stuff
      //! Linear combination of coordinates
      MoleculeData<MidasMatrix> mLinComb                 = {"LINCOMB"};
      //! Only do selected vibrations (weird :S... maybe do in anther way... in combination with FREQ will depend on what is read in first)
      MoleculeData<std::vector<In> > mSelectVibs         = {"SELECTVIBS"};
      //! Hessian of the molecule
      MoleculeData<MidasMatrix> mHessian                 = {"HESSIAN"};
      //! Gradient of the molecule
      MoleculeData<MidasVector> mGradient                = {"GRADIENT"};
      //!@}


      //!@{
      //! Stuff that needs initialization after read-in
      //! Mass vector
      MoleculeData<std::vector<Nb> > mMassVector         = {"MASSVEC"};
      //! Total mass of molecule
      Nb mTotalMass                                      = 0;
      //! Reduced mass (for diatomic)
      Nb mRedcucedMassDiatomic                           = 0;
      //! Number of vibrations
      In mNumberOfVibs                                   = 0;
      //! Number of nuclei
      In mNumberOfNuclei                                 = 0;
      //! Is the molecule linear or not
      bool mLinear                                       = false;
      //!@}

      //! Initialize molecule info
      void Initialization();

      //! Initialization mass vector
      void InitMassVector();

      //! Sanity check, check that molecule input is acutally valid
      void SanityCheck() { };// dont ask, dont tell

      //! Rotate Normal coordinates (Used by Rotate())
      void RotateNormalCoord(const MidasMatrix& aRotation);

   public:

      //! Deleted default constructor
      MoleculeInfo() = delete;

      //! Delete copy c-tor
      MoleculeInfo(const MoleculeInfo&) = delete;

      //! Enable move c-tor
      MoleculeInfo(MoleculeInfo&&) = default;

      //! Deleted default copy assignment
      MoleculeInfo& operator=(const MoleculeInfo&) = delete;

      //! Deleted default copy assignment
      MoleculeInfo& operator=(MoleculeInfo&&) = default;

      //! Constructor from set of MoleculeFileInfo's.
      MoleculeInfo(const MoleculeFileInfo::Set&);

      //! Constructor from MoleculeFileInfo.
      MoleculeInfo(const MoleculeFileInfo&);

      //! Constructor from system.
      MoleculeInfo(const System&, const std::set<GlobalSubSysNr>&, const std::set<GlobalModeNr>&, const bool& arInclSigmas = true);

      //! Constructor from XYZ, normal coordinates, and frequencies
      MoleculeInfo(const std::vector<Nuclei>& aXYZ, const MidasMatrix& aNormCoord, const MidasVector& aFreq);

      //! Destructor
      virtual ~MoleculeInfo() = default;

      //@{
      //! Getters for MoleculeInfo class
      const std::vector<Nuclei>& GetCoord() const                 {return mCoordinates.Get();}
      const std::vector<Nb>& GetMasses() const                    {return mMassVector.Get();}
      const Nb GetNuclMassi(In aI) const                          {return mCoordinates->at(aI).GetMass();}
      const MidasMatrix& NormalCoord() const                      {return mNormalCoord.Get();}
      MidasMatrix& NormalCoord()                                  {return mNormalCoord.Get();}
      const MidasVector& GetmInternalCoord() const                {return mInternalCoord.Get();}
      const MidasVector& GetmInternalActiveCoords() const         {return mInternalActiveCoords;}
      const std::vector<In>& GetmInactiveModeNrs() const          {return mInactiveModeNrs;}
      const std::vector<std::string>& GetmTypeLabels() const      {return mTypeLabels;}
      const std::vector<In>& GetmSelectVibs() const               {return mSelectVibs.Get();}
      const In GetNoSelectVibs() const                            {return mSelectVibs->size();}
      const Nb& GetTotalMass() const                              {return mTotalMass;}
      const In& GetNumberOfNuclei() const                         {return mNumberOfNuclei;}
      const In& GetNoOfVibs() const                               {return mNumberOfVibs;}
      const Nb& GetCurrCoordI(const In& aIn) const                {return mCurrCoord[aIn];}
      const std::string& GetModeSymInfo(const In& aMode) const    {return mSymLabels[aMode];}
      const std::string& GetModeLabel(const In& aMode) const      {return mModeLabels[aMode];}
      const std::vector<std::string>& GetSymLabels() const        {return mSymLabels;}
      const std::vector<std::string>& GetModeNames() const        {return mModeLabels;}
      const Nuclei* GetNuci(const In& aI) const                   {return &(mCoordinates->at(aI));}
      const Nb& GetFreqI(const In& aIn) const
      {
         if (HasSelectVibs())
         {
            return mFreqs->at(mSelectVibs->at(aIn));
         }
         else
         {
            return mFreqs->at(aIn);
         }
      }
      //@}

      //!@{
      //! Manipulate symmetry labels
      void SetSymLabels(const std::vector<std::string>& aSymLabels) { mSymLabels = aSymLabels; }
      //!@}

      In GetModeNo(const std::string& arName) const
      {
         for ( In i = I_0; i < mModeLabels.size(); i++)
         {
            if (mModeLabels.at(i) == arName)
            {
               return i;
            }
         }
         return -I_1;
      }

      MidasMatrix NormalCoordMw() const
      {
          MidasMatrix mat(GetNoOfVibs(), I_3*GetNumberOfNuclei(), C_0);
          for (In i = I_0 ; i < GetNoOfVibs() ; ++i)
          {
             MidasVector vec(I_3*GetNumberOfNuclei(), C_0);
             GetNormalModeMw(vec, i);
             mat.AssignRow(vec, i);
          }
          return mat;
      }

      MidasMatrix GetLinComb() const {return mLinComb.Get();}

      void GetNormalMode(MidasVector& arRow, In aI) const
      {
         if (arRow.Size() != I_3 * mNumberOfNuclei)
         {
            arRow.SetNewSize(I_3 * mNumberOfNuclei);
         }

         if (HasSelectVibs())
         {
            mNormalCoord->GetRow(arRow, mSelectVibs->at(aI));
         }
         else
         {
            mNormalCoord->GetRow(arRow, aI);
         }
      }

      //!
      void GetNormalModeMw(MidasVector& arRow,In aI) const 
      {
         GetNormalMode(arRow,aI);
         In entry=I_0;
         for (In i=I_0 ; i < GetNumberOfNuclei() ; ++i)
         {
            for (In j=I_0;j<I_3;++j)
            {
               arRow[entry++]*=sqrt(GetNuclMassi(i));
            }
         }
      }

      //!
      void GetSigma(MidasVector& arRow, In aI) const 
      {
         if (arRow.Size() != I_3 * mNumberOfNuclei)
         {
            arRow.SetNewSize(I_3 * mNumberOfNuclei);
         }

         if (HasSelectVibs())
         {
            mSigma->GetRow(arRow, mSelectVibs->at(aI));
         }
         else
         {
            mSigma->GetRow(arRow, aI);
         }
      }
      
      //!
      void OutputMinfo(const string& aFileName) const;
      
      //! Is the molecule linear?
      bool IsLinear() const {return mLinear;}
      
      //! Returns the reduced mass og a diatomic, should only be used for diatomics
      Nb GiveRedMass() const {return mRedcucedMassDiatomic;}
      
      //! Returns the number of vibrations we are considering
      void SetNoOfVibs(In i) { mNumberOfVibs = i;} 
      
      //! Returns the number of Sigmas we are considering
      In GetNoOfSigmas() const 
      { 
         if (HasSelectVibs())
         { 
            In n = I_0;
            std::for_each
               (  mSelectVibs->begin()
               ,  mSelectVibs->end()
               ,  [this, &n](In selected)
                     {
                        if (selected<mSigma->Nrows())
                        {
                           ++n;
                        }
                     }
               );

            return n;
         }
         else
         {
           return mSigma->Nrows();
         }
      }

      //! Rotate normal coordinates
      void MixNormalCoordByRotation(const MidasMatrix& aRotation);

      //! Print vibrational vector "statistics"
      void DoCoordNucleiStat() const;

      //! Setup default mode name, i.e. Q0, Q1, ... etc....
      void SetupDefaultModeNames();

      /////
      // below needs refactorization!
      /////
      void FindZmat();

      /**
       * Remove these 3 functions??
      **/
      //! Prints the reference geometry
      void PrintmCoordinates() const;

      //!  Prints the frequencies
      void PrintNoOfFreq() const
      {
         Mout << " Number of frequencies: " << mNumberOfVibs << std::endl;
      }

      //! Prints the number of nuclei
      void PrintNoOfNuclei() const
      {
         Mout << " Number of nuclei:      " << mNumberOfNuclei << std::endl;
      }

      //!
      std::string AtomLabel(In i) const { return mCoordinates->at(i).AtomLabel(); }

      //////
      // Things that probably are as they should be
      //////
      //!@{
      //! Write molecular files
      void WriteMoleculeFile(const MoleculeFileInfo::Set&) const;
      void WriteMoleculeFile(const MoleculeFileInfo&) const;
      //!@}

      //!@{
      //! Check whether data is available
      bool HasXYZ() const              {return bool(mCoordinates);}
      bool HasZmat() const             {return false;}
      bool HasFreq() const             {return bool(mFreqs);}
      bool HasNormalCoord() const      {return bool(mNormalCoord);}
      bool HasInternalCoord() const    {return bool(mInternalCoord);}
      bool HasInactiveModes() const    {return bool(mInactiveModes);}
      bool HasLinComb() const          {return bool(mLinComb);}
      bool HasSelectVibs() const       {return bool(mSelectVibs);}
      bool HasGradient() const          {return bool(mGradient);}
      bool HasHessian() const          {return bool(mHessian);}
      bool HasSigma() const            {return bool(mSigma);}
      //!@}

      //!@{
      //! Set data from input file
      void InitXYZ(const XYZData&);
      void InitFrequencies(const FrequencyData&);
      void InitVibrationalCoordinates(const VibrationalCoordinateData&);
      void InitInternalCoord(const InternalCoordData&);
      void InitInactiveModes(const InactiveModesData&);
      void InitSigma(const SigmaData&);
      void InitLinComb(const LinCombData&);
      void InitSelectVibs(const SelectVibsData&);
      void InitZmat(const ZmatData&);
      void InitGradient(const GradientData&);
      void InitHessian(const HessianData&);
      //!@}

      //!@{
      //! Convert internal data to input/output structures
      //MBH edit: Stuff for reinitializing/overwriting the data. May need changing!
      void ReInitGradient(const GradientData&);
      void ReInitHessian(const HessianData&);
      void ReInitFrequencies(const FrequencyData&);
      void ReInitFreqSymm(const FrequencyData&);
      void ReInitVibrationalCoordinates(const VibrationalCoordinateData&);
      void UnInitSigma() {mSigma.UnInit();}
      void UnInitSelectVibs() {mSelectVibs.UnInit();}

      // Convert internal data to input/output structures
      XYZData GetXYZData() const;
      FrequencyData GetFrequencyData() const;
      VibrationalCoordinateData GetVibrationalCoordinateData() const;
      SigmaData GetSigmaData() const;
      ZmatData GetZmatData() const;
      GradientData GetGradientData() const;
      HessianData GetHessianData() const;
      //!@}

      //! Rotate molecule
      void Rotate(const MidasMatrix& aRotMat);

      //! Translate molecule
      void Translate(const MidasVector& aTransVec);

      //!
      void PurgeSymmetryLabels();
};

} /* namespace molecule */
} /* namespace midas */

#endif /* MOLECULEINFO_H_INCLUDED */
