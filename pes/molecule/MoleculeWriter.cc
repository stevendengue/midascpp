#include "MoleculeWriter.h"

#include "MoleculeInfo.h"

namespace midas
{
namespace molecule
{

bool MoleculeWriter::Write
   (  const MoleculeInfo& aMolecule
   )
{
   //if(!mMoleculeFile.Open(std::ios_base::out))
   //{
   //   MIDASERROR("Could not open molecule file: "+mMoleculeFile.FileName());
   //}

   // read molecule input
   bool success = true;

   if(!mWritingOrder.empty())
   {
      return Write(aMolecule, mWritingOrder);
   }
   else
   {
      return Write(aMolecule, {molecule_field_t::XYZ,
                               molecule_field_t::FREQ,
                               molecule_field_t::VIBCOORD,
                               molecule_field_t::SIGMA,
                               molecule_field_t::GRADIENT,
                               molecule_field_t::HESSIAN});
                              // This is the default output order
   }

   return success;
}

bool MoleculeWriter::Write
   (  const MoleculeInfo& aMolecule
   ,  const MoleculeFields& aMoleculeFields
   )
{
   // check that file exists and that we can correctly open it
   //if(!mMoleculeFile.Open(std::ios_base::out)) MIDASERROR("Could not open molecule file: "+mMoleculeFile.FileName());

   bool success = true;

   Initialize();

   // loop through input fields and read molecule input
   for(molecule_field_t field: aMoleculeFields)
   {
      switch(field)
      {
         case molecule_field_t::XYZ:
            if(XYZData xyz = aMolecule.GetXYZData())
            {
               WriteXYZ(xyz);
            }
            break;
         case molecule_field_t::FREQ:
            if(FrequencyData freq = aMolecule.GetFrequencyData())
            {
               WriteFrequencies(freq);
            }
            break;
         case molecule_field_t::VIBCOORD:
            if(VibrationalCoordinateData vib_coord = aMolecule.GetVibrationalCoordinateData())
            {
               WriteVibrationalCoord(vib_coord);
            }
            break;
         case molecule_field_t::SIGMA:
            if(SigmaData sigma = aMolecule.GetSigmaData())
            {
               WriteSigma(sigma);
            }
            break;
         case molecule_field_t::ZMAT:
            /*
            // Implement me! :-)
            if(ZmatData zmat = aMolecule.GetZmatData())
            {
               WriteZmat(zmat);
            }
            */
            break;
         case molecule_field_t::GRADIENT:
            if(GradientData gradient = aMolecule.GetGradientData())
            {
               WriteGradient(gradient);
            }
            break;
         case molecule_field_t::HESSIAN:
            if(HessianData hessian = aMolecule.GetHessianData())
            {
               WriteHessian(hessian);
            }
            break;
         default:
            MIDASERROR("Wrong field type in switch");
      }
   }

   Finalize();

   // close file again
   //if(!mMoleculeFile.Close()) MIDASERROR("Could not close molecule file"+mMoleculeFile.FileName());
   mMoleculeFile.Flush();

   return success;
}

/**
 *
 **/
bool MoleculeWriter::WriteMolecule
   (  const MoleculeFileInfo::Set& aSet
   ,  const MoleculeInfo& aMolecule
   )
{
   bool success = true;
   for(const MoleculeFileInfo& aInfo: aSet)
   {
      MoleculeWriterHandle writer = MoleculeWriter::Factory(aInfo);
      success = writer->Write(aMolecule) && success;
   }
   return success;
}

} /* namespace molecule */
} /* namespace midas */
