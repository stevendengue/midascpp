#include "MoldenWriter.h"

#include "MoleculeInfo.h"
#include "input/GetLine.h"

namespace midas
{
namespace molecule
{

/////
// Registration for factory
/////
MoleculeWriterRegistration<MoldenWriter> registerMoldenWriter("MOLDEN");

/**
 *
 **/
MoldenWriter::MoldenWriter(const MoleculeFileInfo& aMoleculeFileInfo)
   :  MoleculeWriter(aMoleculeFileInfo) 
{
   mMoleculeFile.FileStream().setf(std::ios::scientific);
   mMoleculeFile.FileStream().setf(std::ios::uppercase);
   mMoleculeFile.FileStream().precision(I_12);
}

/**
 *
 **/
bool MoldenWriter::Initialize()
{
   bool success = false;
   if(!mMoleculeFile.SearchKeyword("[Molden Format]"))
   {
      mMoleculeFile.FileStream() << "[Molden Format]" << "\n" 
                                 << "[Title]" << "\n" 
                                 << "\n" << std::flush;
      success = true;
   }
   else
   {
      MidasWarning("Already a [Molden Format] block in file: " + mMoleculeFile.FileName());
   }
   return success;
}

/**
 *
 **/
bool MoldenWriter::Finalize()
{
   return true;
}

/**
 *
 **/
bool MoldenWriter::WriteXYZ(const XYZData& xyz)
{
   bool success = false;
   
   if(!mMoleculeFile.SearchKeyword("[FR-COORD]"))
   {
      // output first line
      mMoleculeFile.FileStream() << "[FR-COORD] \n";

      // output each atoms
      for(In i = 0; i < xyz->NumAtom(); ++i)
      {
         mMoleculeFile.FileStream() << xyz->Label(i) << " " 
                                    << xyz->X(i) << " "
                                    << xyz->Y(i) << " "
                                    << xyz->Z(i) << "\n"; 
      }
      
      // print aestetic \n and and flush
      mMoleculeFile.FileStream() << "\n" << std::flush;
      success = true;
   }
   else
   {
      MidasWarning("Already a [FR-COORD] block in file: " + mMoleculeFile.FileName());
   }

   return success;
}

/**
 *
 **/
bool MoldenWriter::WriteVibrationalCoord(const VibrationalCoordinateData& vib_coord)
{
   bool success = false;

   MidasWarning("MoldenWriter writes non-normalized vibrational modes, may lead to unconventional amplitudes");
   
   if(!mMoleculeFile.SearchKeyword("[FR-NORM-COORD]"))
   {
      // print first line
      mMoleculeFile.FileStream() << "[FR-NORM-COORD]\n";

      // print coordinates
      for(In i = 0; i < vib_coord->NumCoords(); ++i)
      {
         mMoleculeFile.FileStream() << "vibration " << i+I_1 << "\n" ;  
         for(In j = 0; j < vib_coord->NumDisplacements(); ++j)
         {
            if( (j%3 == 0) && (j != 0) )
            {
               mMoleculeFile.FileStream() << "\n";
            }
            mMoleculeFile.FileStream() << vib_coord->Coordinate(i).at(j)*sqrt(C_FAMU) << " ";
         }
         mMoleculeFile.FileStream() << "\n";
      }
      
      // print aestetic \n and flush to file
      mMoleculeFile.FileStream() << "\n" << std::flush;
      success = true;
   }
   else
   {
      MidasWarning("Already a [FR-NORM-COORD] block in file: " + mMoleculeFile.FileName());
   }

   return success;
}

/**
 *
 **/
bool MoldenWriter::WriteFrequencies(const FrequencyData& freq)
{
   bool success = false;
   if(!mMoleculeFile.SearchKeyword("[FREQ]"))
   {
      mMoleculeFile.FileStream() << "[FREQ]\n" ;
      for(In i = 0; i < freq->NumFreq(); ++i)
      {
         mMoleculeFile.FileStream() << (freq->Frequency(i)) << "\n";
      }
      mMoleculeFile.FileStream() << "\n" << std::flush; // \n for aestetics and flush the output to file
      success = true;
   }
   else
   {
      MidasWarning("Already a [FREQ] block in file: " + mMoleculeFile.FileName());
   }
   return success;
}

} /* namespace molecule */
} /* namespace midas */
