/*
************************************************************************
*
* @file                MoleculeReader.cc
*
* Created:             24-03-2015
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Interface to read molecule files.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "MoleculeReader.h"

#include "util/Error.h"
#include "input/GetLine.h"
#include "pes/molecule/MoleculeInfo.h"

namespace midas
{
namespace molecule
{

/**
 * register function with the reader
 **/
void MoleculeReader::RegisterFunction(const key_t& aKey
                                    , const field_t& aField
                                    , const function_t& aFunction
                                    )
{
   mFunctionMap.insert({aKey,{aField,aFunction}});
}

/**
 * set input delimeters
 **/
void MoleculeReader::InputDelimeter(const delimeter_function_t& aBegin
                                  , const delimeter_function_t& aEnd
                                  )
{
   mInputBegin = aBegin;
   mInputEnd = aEnd;
}

/**
 * find function in map for given key
 **/
typename MoleculeReader::function_map_const_iterator_t MoleculeReader::FindFunction
   (  const key_t& aKey
   )
{
   key_t key = aKey;
   auto end_iter = mFunctionMap.end();
   while(!key.empty())
   {
      auto iter = mFunctionMap.find(key);
      if(iter != end_iter) return iter;
      auto pos = key.find_last_of(" ");
      if(pos == std::string::npos) break; // we didn't find any spaces, which means we will not find a match, thus we break loop
      key = key.substr(0,pos);
   }
   return end_iter;
}

/**
 *
 **/
MoleculeReader::return_t MoleculeReader::ReadKeys
   (  std::string& s
   ,  bool already_read
   ,  MoleculeInfo& aMolecule
   )
{
   std::string s_parsed;
   // Loop over lines and process molecule file keys
   while (  (already_read || std::getline(mMoleculeFile.FileStream(), s))
         && !InputEnd(s_parsed = ParseKey(s))
         )
   {

      if (gIoLevel > I_5 && !s_parsed.empty())
      {
         Mout << " Trying to identify keyword: " << s_parsed << std::endl;
      }

      already_read = false;
      // Find (field, function) pair for parsed keyword
      auto iter = FindFunction(s_parsed);

      // If a registered function is found
      if (iter != mFunctionMap.end())
      {
         // Check if we should actually read the field, if yes then call registred function
         if (mMoleculeFile.HasField(iter->second.first))
         {
            // Input the original s, as it may contain more information than s_parsed
            libmda::util::ret(s, already_read) = iter->second.second(s, aMolecule);
         }
      }
   }
   return libmda::util::ret(s, already_read);
}

/**
 * reader driver
 **/
bool MoleculeReader::Read(MoleculeInfo& aMolecule)
{
   bool success = true;

   try
   {
      if (gIoLevel > I_4)
      {
         Mout << std::endl << " Now reading the Molecule file: " << std::endl;
         Mout << "  " << mMoleculeFile.FileName() << std::endl;
      }

      std::string s;
      std::string s_parsed;

      // Find beginning of molecule file, (check only if beginning of input is given else read from the start)
      if (mInputBegin)
      {
         while (  std::getline(mMoleculeFile.FileStream(), s)
               && !InputBegin(s_parsed = ParseKey(s))
               )
         {
            Mout << " Trying to find beginning: " << s_parsed << std::endl;
         }

         // Assert that we actually found beginning
         MidasAssert(InputBegin(s_parsed), "Did not find BEGIN keyword in file '" + mMoleculeFile.FileName() + "'");
      }

      // Read input until end of input
      libmda::util::ret(s, libmda::util::_) = ReadKeys(s, false, aMolecule);

      // Assert that we found end of file
      MidasAssert(mInputEnd ? InputEnd(s_parsed = ParseKey(s)) : true, "Did not find END keyword in file '" + mMoleculeFile.FileName() +"'");
   }
   catch (std::exception& e)
   {
      MIDASERROR("Reading molecule file i caugth exception: " + std::string(e.what()));
   }
   catch (...)
   {
      MIDASERROR("Reading molecule file i caugth 'something'");
   }

   return success;
}

/**
 * molecule reader interface
 **/
bool MoleculeReader::ReadMolecule(const MoleculeFileInfo::Set& aSet, MoleculeInfo& aMolecule)
{
   bool success = true;

   // loop over molecule info's
   for(const MoleculeFileInfo& aInfo: aSet)
   {
      // check that file exists and that we can correctly open it
      MoleculeFile moleculefile(aInfo);
      //MidasAssert(moleculefile.Open(std::ios_base::in), "Could not open moleculefile file: " + moleculefile.FileName());
      MidasAssert(moleculefile.Buffer(), "Could not open moleculefile file: " + moleculefile.FileName());

      MoleculeReaderHandle reader = Factory(aInfo.Type(), moleculefile);
      success = reader->Read(aMolecule) && success;

      // close file again
      //MidasAssert(moleculefile.Close(), "Could not close moleculefile file" + moleculefile.FileName());
   }

   return success;
}

} /* namespace molecule */
} /* namespace midas */
