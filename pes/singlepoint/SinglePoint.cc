#include "pes/singlepoint/SinglePoint.h"

#include <string>

#include "libmda/util/stacktrace.h"

#include "util/Error.h"
#include "util/MidasStream.h"
#include "util/FileSystem.h"
#include "util/Os.h"
#include "util/FindInMap.h"
#include "pes/FindSymRot.h"

extern MidasStream Mout;

using namespace midas;

/* ==========================================================================
 *
 * SinglePointImpl implementation
 *
 * ========================================================================== */

/**
 * Error message printed when wrong type of point is requested (remember to update when you add a singlepoint type!).
 *
 * @return    Returns string with error message.
 **/
std::string SinglePointImpl::ErrorMessage
   (
   ) 
{
   std::string str { "Wrong singlepoint type... \n" 
                     "Currently one may choose from: \n"
                     "   Dalton  (SP_DALTON),    http://www.kjemi.uio.no/software/dalton \n"
                     "   Cfour   (SP_CFOUR),     http://cfour.de \n" 
                     "   Aces    (SP_ACES),      http://aces2.de \n" 
                     "   Tinker  (SP_TINKER),    http://dasher.wustl.edu/tinker \n" 
                     "   Generic (SP_GENERIC),   a generic setup, please see the manual \n" 
                     "   Model   (SP_MODEL),     model potential for water, and other, please see the manual \n"
                   };
   return str;
}

/**
 * Setup scratch directory (depends on mCalcNumber, i.e. it must be initialized before this is called).
 * This will create the \<scratchdir\>/scr\<calcnumber\> directory
 * to be used as scratch for the singlepoint calculation.
 * This will remove any old "scratch" directories with the same name.
 **/
void SinglePointImpl::SetupSpScratchDir
   (
   )
{
   // Get the current working directory if no indication of the scratch directory placementis is given, i.e. if the #2 SpScratchDirPreFix keyword is inactive
   if ( mSpScratchDir == ""  )
   {
      mSpScratchDir = midas::os::Getcwd();
   }

   // Create Multilevels directory, where the level_ directories will be placed
   midas::filesystem::Mkdir(mSpScratchDir + "/Multilevels");

   // Get the correct level_ information as a string
   std::string level_prefix = mSpLevelPrefix != "" ? "/" + mSpLevelPrefix : "";
   mSpScratchDir += level_prefix; 
   
   // Make the level_ scratch sub-directory, where all the single points calculations run from
   midas::filesystem::Mkdir(mSpScratchDir);
   
   // Finally we create an individual scr directory for each single point calculation
   mSpScratchDir += "/scr" + std::to_string(mCalcNumber);
 
   // Delete old scratch dir if any.
   midas::filesystem::Remove(mSpScratchDir);

   // Then make 'new' scratch dir.
   midas::filesystem::Mkdir(mSpScratchDir);;
}

/**
 * Constructor from SinglePointInfo map.
 *
 * @param aInfo         The input map to construct the singlepoint from.
 * @param aPropertyInfo Information on the properties we should collect from the singlpoint.
 **/
SinglePointImpl::SinglePointImpl
   (  const SinglePointInfo& aInfo
   ,  const pes::PropertyInfo& aPropertyInfo
   )
   :  mType(aInfo.at("PROGRAM"))
   ,  mRotationThr(midas::util::FromString<Nb>(aInfo.at("ROTATIONTHR")))
   ,  mSpScratchDir (aInfo.at("SPSCRATCHPREFIX"))
   ,  mSpLevelPrefix(aInfo.at("SPLEVELPREFIX"))
   ,  mSaveEspOut(aInfo.at("SAVEESPOUT") == "TRUE" ? true : false)
   ,  mBohr(aInfo.at("BOHR") == "TRUE" ? true : false)
   ,  mProperties(aPropertyInfo)
{
}

/**
 *  Setup rotation matrix for GeneralProp
 *  from structure and midasifc.cartrot.
 *  This is needed as the Electronic Structure (ES) program 
 *  might rotate the molecule before calculating properties.
 *  We thus need to check if the molecule has been rotated,
 *  such that Midas can rotate any properties to its own axis frame.
 *
 *  @param aRotated      The "rotated" structure coming from the singlepoint ES.
 *  @param aNucMap       The nuclei "relation map", i.e. which nuclei correspond to each other in the two structures (Midas and ES).
 *  @param aLabelVec     A label vec (? no clue...).
 *  @param aRotMatrix    On return this will hold the rotation matrix needed to rotate the properties to the Midas frame.
 **/
void SinglePointImpl::SetupRotationMatrix
   (  const std::vector<Nuclei>& aRotated
   ,  std::map<In,In>& aNucMap
   ,  std::vector< pair<string,string> >& aLabelVec
   ,  MidasMatrix& aRotMatrix
   )
{
   // Set rotation matrix to unit (needed by FindRotationMatrix).
   aRotMatrix.SetNewSize(I_3, I_3, false, true);
   aRotMatrix.Unit();

   // Perform rotation.
   if (!FindRotationMatrix 
         (  const_cast<std::vector<Nuclei>& >(mStructure)
         ,  const_cast<std::vector<Nuclei>& >(aRotated)
         ,  aNucMap
         ,  aRotMatrix
         ,  aLabelVec
         ,  false
         ,  mRotationThr
         ,  mIoLevel
         )
      )
   {
      MIDASERROR("Could not relate structures for point: " + std::to_string(mCalcNumber) + "!");
   }
}

/**
 * Gather properties from files. This is done by first reading 
 * a molecular structure output by the singlepoint.
 * Then a rotation matrix is found relating the returned structure
 * with the Midas frame. Lastly the properties are read-in and rotated to the Midas frame.
 *
 * This is not done for SP_MODEL and SP_TINKER, as these two singlepoint types "short circuit"
 * the standard SP procedure and just set the values directly in the mProperties member variable
 * when they are evaluated.
 **/
void SinglePointImpl::GatherProperties
   (
   ) 
{ 
   MidasMatrix rot_mat;
   std::map<In,In> nuc_map;
   std::vector<Nuclei> rotated;
   std::vector< pair<string,string> > label_vec;

   if(mType != "SP_MODEL" && mType != "SP_TINKER") 
   {
      this->ReadRotatedStructureImpl(nuc_map, rotated, label_vec);
      this->SetupRotationMatrix(rotated, nuc_map, label_vec, rot_mat);
      mProperties.RetrieveProperties(mSpScratchDir, rot_mat, nuc_map);
   }
}

/**
 * Initialize SinglePointImpl with calculation number.
 * This could in principle be moved to the constructor,
 * but clients shouldn't meet the SinglePointImpl interface anyways,
 * they should only interact with SinglePoint.
 **/
void SinglePointImpl::Initialize
   ( In aCalcNumber
   )
{
   mCalcNumber = aCalcNumber;
   mInitialized = true; // set init to true
}

/**
 * Set the CalcCodeString (identifier for displacement) of this SinglePoint 
 **/
void SinglePointImpl::SetCalcCodeString
   (  std::string aCalcCodeString
   )
{
   mCalcCodeString = aCalcCodeString;
}

/**
 * Factory for creating singlepoints from a SinglePointCalcDef.
 * Will call the factory from SinglePointInfo.
 *
 * @param aSpCalcDef     The calculation definition to create the singlepoint from.
 * @param aPropertyInfo  Information on the properties we should collect from the singlpoint.
 *
 * @return            Returns a unique_ptr containing the newly constructed singlepoint.
 **/
std::unique_ptr<SinglePointImpl> SinglePointImpl::Factory
   (  const SinglePointCalcDef& aSpCalcDef
   ,  const pes::PropertyInfo& aPropertyInfo
   )
{
   auto sp_info = aSpCalcDef.SpInfo();
   return SinglePointImpl::Factory(sp_info, aPropertyInfo);
}

/**
 * Factory for creating singlepoints from a SinglePointInfo.
 *
 * @param aSpInfo   The info to construct the singlepoint from.
 * @param aPropertyInfo  Information on the properties we should collect from the singlpoint.
 *
 * @return          Returns a unique_ptr containing the newly constructed singlepoint.
 **/
std::unique_ptr<SinglePointImpl> SinglePointImpl::Factory
   (  const SinglePointInfo& aSpInfo
   ,  const pes::PropertyInfo& aPropertyInfo
   )
{
   auto iter = aSpInfo.find("PROGRAM");
   if(iter == aSpInfo.end()) // check map
   {
      MIDASERROR("No PROGRAM entry in SinglePointInfo map.");
   }

   // create singlepoint
   std::unique_ptr<SinglePointImpl> sp = SinglePointFactory::create(iter->second, aSpInfo, aPropertyInfo);
   
   if(!sp) // check singlepoint
   {
      MIDASERROR("Wrong type of singlepoint: " + aSpInfo.at("PROGRAM") + "\n" + SinglePointImpl::ErrorMessage());
   }
   return sp;
}

/**
 * Create input interface. 
 *
 * Will call the virtual implementation function, 
 * which is then overloaded in the specific singlepoint classes.
 **/
void SinglePointImpl::CreateInput
   (
   ) const 
{ 
   if(!mInitialized) 
   {
      MIDASERROR("Creating input for non initialized singlepoint"); 
   }
   CreateInputImpl(); 
} 

/**
 * Submit calculation interface. 
 *
 * Will call the virtual implementation function, 
 * which is then overloaded in the specific singlepoint classes.
 **/
void SinglePointImpl::Submit
   ( bool aB
   ) const
{
   if(!mInitialized) 
   {
      MIDASERROR("Submitting non-initialized singlepoint");
   }
   SubmitImpl(aB);
}

/**
 * Validate calculation interface.
 *
 * Will call the virtual implementation function, 
 * which is then overloaded in the specific singlepoint classes.
 **/
void SinglePointImpl::ValidateCalculation
   ( bool& aHalt
   , bool& aSuspicious
   ) const
{
   ValidateCalculationImpl(aHalt, aSuspicious);
}

/**
 * Clean-up calculation interface.
 *
 * Will call the virtual implementation function, 
 * which is then overloaded in the specific singlepoint classes.
 *
 * After specific clean-up has been done, the scratch dir will be removed unless
 * it has been requested on input to keep it.
 **/
void SinglePointImpl::CleanUp
   (
   ) const
{
   CleanUpImpl();

   if (!mSaveScratch)
   {
      midas::filesystem::Rmdir(mSpScratchDir, true);
   }
}

/* ==========================================================================
 *
 * SinglePoint implementation
 *
 * ========================================================================== */

/**
 * Constructor from SinglePointInfo input map.
 *
 * @param aSpInfo          The singlepoint input map.
 * @param aPropertyInfo    Information on the properties we should collect from the singlpoint.
 * @param aStructure       The structure for the singlepoint.
 * @param aCalcNumber      The unique calculation number.
 * @param aCalcCodeString  The unique identifier for the displacement.
 **/
SinglePoint::SinglePoint
   (  const SinglePointInfo& aSpInfo
   ,  const pes::PropertyInfo& aPropertyInfo
   ,  const std::vector<Nuclei>& aStructure
   ,  const In& aCalcNumber
   ,  const std::string& aCalcCodeString
   )
   :  mImpl(SinglePointImpl::Factory(aSpInfo, aPropertyInfo))
{
   mImpl->SetStructure(aStructure);
   mImpl->Initialize(aCalcNumber);
   mImpl->SetCalcCodeString(aCalcCodeString);
}

/**
 * Constructor from SinglePointCalcDef.
 *
 * @param aSpCalcDef    The singlepoint CalcDef.
 * @param aPropertyInfo Information on the properties we should collect from the singlpoint.
 * @param aStructure    The structure for the singlepoint.
 * @param aCalcNumber   The unique calculation number.
 **/
SinglePoint::SinglePoint
   (  const SinglePointCalcDef& aSpCalcDef
   ,  const pes::PropertyInfo& aPropertyInfo
   ,  const std::vector<Nuclei>& aStructure
   ,  const In& aCalcNumber
   )
   :  mImpl(SinglePointImpl::Factory(aSpCalcDef, aPropertyInfo))
{
   mImpl->SetStructure(aStructure);
   mImpl->Initialize(aCalcNumber);
}

/**
 * Interface for running the singlepoint.
 * 
 * This function will do all the setup, running and post-processing of the singlepoint.
 *
 * After the function has run the mProperties member will hold the calculated properties from the singlepoint calculation.
 **/
void SinglePoint::RunSinglePoint
   (  
   )
{
   // Setup scratch dir and create input
   mImpl->SetupSpScratchDir();
   mImpl->CreateInput();

   // Submit/run the calculation
   mImpl->Submit(true);

   // Validate calculation
   bool halt = false;
   bool suspicious = false;
   mImpl->ValidateCalculation(halt, suspicious);
   if(halt) 
   {
      MIDASERROR  (  "VALIDATION DID NOT SUCCEED FOR CALCULATION : "
                  ,  std::to_string(CalcNumber())
                  ,  "\nCALCULATION WAS HALTED\n"
                  );
   }

   // Gather properties
   mImpl->GatherProperties();
   
   mImpl->SetDone(); 
   
   // After this, we can clean up
   mImpl->CleanUp();
}
