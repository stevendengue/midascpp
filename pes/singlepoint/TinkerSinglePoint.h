/**
************************************************************************
* 
* @file                TinkerSinglePoint.h
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   TinkerSinglePoint datatype 
* 
* Last modification:   24-11-2014 (Carolin Koenig)
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TINKER_SP_H
#define TINKER_SP_H

#include <string>

#include "inc_gen/TypeDefs.h"
#include "pes/singlepoint/SinglePoint.h"
#include "tinker_interface/TinkerMM.h"

/**
 *
 **/
class TinkerSinglePoint 
   :  public SinglePointImpl
{
   private:
      std::string mLabel;
      TinkerMM mTinkerMM;
      
      // overloads
      void CreateInputImpl() const;
      void SubmitImpl(bool) const;
      void ValidateCalculationImpl(bool&,bool&) const;
      void ReadRotatedStructureImpl(std::map<In,In>& aNucMap, std::vector<Nuclei>& aRotated, std::vector< pair<string,string> >& aLabelVec) const {};
      void CleanUpImpl() const;

   public:
      TinkerSinglePoint() = delete;
      TinkerSinglePoint(const SinglePointInfo&, const pes::PropertyInfo&);
      ~TinkerSinglePoint();
};

#endif /* TINKER_SP_H */
