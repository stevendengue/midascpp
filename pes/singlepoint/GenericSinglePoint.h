/**
************************************************************************
* 
* @file                GenericSinglePoint.h
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   GenericSinglePoint datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef GENERICSINGLEPOINT_H_INCLUDED
#define GENERICSINGLEPOINT_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "pes/singlepoint/SinglePoint.h"

/**
 *
 **/
class GenericSinglePoint 
   :  public SinglePointImpl
{
   private:
      // data members
      std::string mSetupDir;
      std::string mInputCreatorScript;
      std::string mRunScript;
      std::string mValidationScript;

      // private functions
      void CreateGenericXyzFile() const;
      //std::string CreateGenericXyzFile() const;
      
      void InputCreatorScript() const;

      bool CheckValidateString(std::stringstream& aSstr, const std::string aStr) const;
      
      // overloads   
      void CreateInputImpl() const;
      void SubmitImpl(bool) const;
      void ValidateCalculationImpl(bool&,bool&) const;
      void ReadRotatedStructureImpl(std::map<In,In>& aNucMap, std::vector<Nuclei>& aRotated, std::vector< pair<string,string> >& aLabelVec) const;
      //void CleanUpImpl() const;
   
   public:
      GenericSinglePoint() = delete;
      GenericSinglePoint(const SinglePointInfo&, const pes::PropertyInfo&);
      virtual ~GenericSinglePoint() = default;
};

#endif /* GENERICSINGLEPOINT_H_INCLUDED */
