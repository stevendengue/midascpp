/**
************************************************************************
* 
* @file                FitFunctions.h
*
* Created:             02-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Class for holding the functions needed for fitting in Midas
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef FITFUNCTIONS_H
#define FITFUNCTIONS_H

// std headers
#include <map>
#include <mutex>
#include <vector>
#include <string>
#include <memory>
#include <iostream>

// midas headers
#include "inc_gen/TypeDefs.h"

//forward declarations
class BaseFitBasisSet;
template<class T>
class MidasFunctionWrapper;
class MidasOperatorWriter;

class FitFunctions
{

   private:

      //!
      std::map<std::pair<In, std::string>, std::shared_ptr<BaseFitBasisSet> >    mFitBasisMap;

      //1
      std::vector<std::shared_ptr<BaseFitBasisSet> >                             mFitBasisSets;

      //! 
      std::map<std::string, Nb>                                                  mConstants;

      //!
      std::map<std::string, std::string>                                         mFunctions;

      //!
      std::vector<std::unique_ptr<std::mutex> >                                  mMutexBasisSets;

      //!
      std::shared_ptr<BaseFitBasisSet> FindBasis(const In, const std::string&) const;
      
   public:

      //! Delete default constructor
      FitFunctions() = delete;

      //! Constructor
      FitFunctions(const FitBasisCalcDef& aFitBasisCalcDef);

      //! Enable move constructor
      FitFunctions(FitFunctions&&) = default;
      
      //! Delete default copy assignment
      FitFunctions& operator=(const FitFunctions&) = delete;
      
      //! Enable default move assignment
      FitFunctions& operator=(FitFunctions&&) = default;
     
      //!
      void InsertBasis(const std::vector<std::string>& arPropNames, const std::vector<In>& arModes, std::shared_ptr<BaseFitBasisSet> aPtr);
      //!
      void InsertCutOff(const std::string&, const std::vector<In>&) const;
      //! 
      void AddToConstants(const std::map<std::string, Nb>&);
      //!
      void AddToFunctions(const std::map<std::string,std::string>&);
      //! 
      void OptimizeFunctionsForMode(const In& aModeNr, const std::string& aPropType, const MidasVector&, const MidasVector&) const;
      //!
      bool GetIsCutOff(const std::string&) const;
      //!
      In GetCutOff(In, const std::string&) const;
      //!
      std::string GetFunctionName(const In, const std::string&, const In) const;
      //!
      In GetNrOfBasFuncsInMode(const In aModeNr, const std::string& aPropType) const;
      //!
      void GetFunctions(const In&, const In&, const In&, const std::string&, std::vector<std::vector<MidasFunctionWrapper<Nb>* > >&) const;
      //!
      std::string GetFuncPart() const;
      //!
      void SetConstAndFunct(std::vector<MidasOperatorWriter>&) const;
      //!
      bool CheckModeNumbers(const In aMaxModeNr) const;
      //!
      friend std::ostream& operator<<(std::ostream&, const FitFunctions&);
};

// Output operator forward decl
std::ostream& operator<<(std::ostream&, const FitFunctions&);

#endif //FITFUNCTIONS_H
