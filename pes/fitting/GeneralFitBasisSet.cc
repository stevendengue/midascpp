/**
************************************************************************
* 
* @file                GeneralFitBasisSet.cc
*
* Created:             02-08-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk) 
*
* Short Description:   Class for holding the functions needed for fitting 
*                      with general functions in Midas
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <vector>
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/FunctionContainer.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"
#include "pes/fitting/GeneralFitBasisSet.h"
#include "pes/fitting/BaseFitBasisSet.h"

/**
 * Constructor
 *
 * @param arFunctions      Functions as strings to be converted to general functions and used as a fitting basis.
 * @param arFunctionCont   Function container to construct general functions from.
 **/
GeneralFitBasisSet::GeneralFitBasisSet
   (  const std::vector<std::string>& arFunctions
   ,  const FunctionContainer<Nb>& arFunctionCont
   )
{
   for(auto it = arFunctions.begin(); it != arFunctions.end(); ++it)
   {
      mFunctionStrings.emplace_back(*it);
      mFunctions.emplace_back(*it, arFunctionCont, false);
   }
}
