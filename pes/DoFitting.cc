/**
************************************************************************
*
* @file                DoFitting.cc
*
* Created:             23-03-2018
*
* Authors:             Emil Lund Klinting (klint@chem.au.dk)
*
* Short Description:   Driver for fitting routine
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

//std headers
#include <vector>
#include <string>

// midas headers
#include "pes/DoFitting.h"

/**
 *
 * @param aFitBasisCalcDef
 * @param aPesCalcDef
 * @param aPesInfo
**/
DoFitting::DoFitting
   (  const FitBasisCalcDef& aFitBasisCalcDef
   ,  const PesCalcDef& aPesCalcDef
   ,  const PesInfo& aPesInfo
   )
   :  mFitBasisCalcDef(aFitBasisCalcDef)
   ,  mPesCalcDef(aPesCalcDef)
   ,  mPesInfo(aPesInfo) 
{

}

/**
 *
**/
MidasVector DoFitting::DoFit
   (  const In& aBar
   ,  const ModeCombi& aModeCombi
   ,  const std::vector<MidasVector>& aCgCoords
   ,  const std::vector<MidasVector>& aFgCoords
   ,  const MidasVector& aFgProperty
   ,  const MidasVector& aFgSigma
   ,  const FitFunctions& aFitBasisFuncs
   ,  std::vector<InVector>& aPowvecVec
   ,  const std::string& aPropType 
   ,  const std::pair<In, In>& aInertiaProps
   ,  const bool& aDerivatives
   ,  std::vector<In>& aAutoNoFitFuncs
   )  const
{
   bool fit_inertia = false;
   if (  mPesCalcDef.GetmPesCalcMuTens()
      && (aBar >= aInertiaProps.first && aBar <= aInertiaProps.second)
      )
   {
      fit_inertia = true;
      
      if (gPesIoLevel > I_10)
      {
         Mout << std::endl << " Fitting inertia" << std::endl;
      }
   }
      
   //
   auto use_gen_fit_basis_def = false;
   if (mFitBasisCalcDef.GetmUseGenFitBasis())
   {
      use_gen_fit_basis_def = !mFitBasisCalcDef.GetmGenFitBasisModeMap()[aModeCombi.Mode(I_0)].empty();
   }

   // Non-linear fit-function parameters are only optimized for the individual one-dimensional grids and are thereafter fixed in fits to the higher dimensional grids 
   if (  mPesInfo.GetMCLevel() == I_1
      && (  use_gen_fit_basis_def
         || mFitBasisCalcDef.GetmAutomaticFitBasis()
         )
      )
   {
      aFitBasisFuncs.OptimizeFunctionsForMode(aModeCombi.Mode(I_0), aPropType, aFgCoords[I_0], aFgProperty);
   }

   // Determine the maximum possible number of functions for available in the one-mode fit-basis 
   std::vector<In> max_pow;
   std::vector<In> max_pow_ref;
   for (In i_mt = I_0; i_mt < aModeCombi.Size(); i_mt++)
   {
      if (mFitBasisCalcDef.GetmAutomaticFitFuncs() && mPesInfo.GetMCLevel() > I_1)
      {
         max_pow_ref.push_back(aAutoNoFitFuncs[aModeCombi.Mode(i_mt)]);
      }
      else
      {
         max_pow_ref.push_back(aFitBasisFuncs.GetNrOfBasFuncsInMode(aModeCombi.Mode(i_mt), aPropType));
      }
   }
   
   if (gDebug)
   {
      Mout << " Maximum fit-basis function orders: " << max_pow_ref << std::endl;
   }

   // Find out how many fit-basis functions should be constructed, i.e. do we restrict the number of functions in the fit-basis in any way
   FindPowVec(aCgCoords, max_pow_ref, max_pow, aModeCombi, fit_inertia, aDerivatives);
  
   // If requested we can attempt to find out which number of fit functions gives the most stable fit
   if (mFitBasisCalcDef.GetmAutomaticFitFuncs() && mPesInfo.GetMCLevel() == I_1 && max_pow[I_0] != I_2)
   {
      auto opt_pow = AutoMaxFitBasisOrder(aBar, aModeCombi, aCgCoords, aFgCoords, aFgProperty, aFgSigma, aFitBasisFuncs, aPropType, max_pow);

      aAutoNoFitFuncs[aModeCombi.Mode(I_0)] = opt_pow[I_0];

      max_pow = opt_pow;
   }

   // Construct all fit-basis functions
   ConstructAllBasisFunc(aBar, aPowvecVec, aModeCombi, max_pow, aPropType);
 
   //
   std::vector<In> func_list_sizes(aModeCombi.Size(), I_0);
   for (In i = I_0; i < aPowvecVec.size(); ++i)
   {
      for (In j = I_0; j < aPowvecVec[i].size(); ++j)
      {
         if (func_list_sizes[j] < aPowvecVec[i][j] + I_1)
         {
            func_list_sizes[j] = aPowvecVec[i][j] + I_1;
         }
      }
   }

   //
   std::vector<std::vector<MidasFunctionWrapper<Nb>* > > fit_functions(aModeCombi.Size(), std::vector<MidasFunctionWrapper<Nb>* >());
   for (In i_mt = I_0; i_mt < aModeCombi.Size(); ++i_mt)
   {
      aFitBasisFuncs.GetFunctions(i_mt, aModeCombi.Mode(i_mt), func_list_sizes[i_mt], aPropType, fit_functions);
   }
  
   // Prepare linear fitting routine
   MidasVector least_sqr_parms;
   least_sqr_parms.SetNewSize(aPowvecVec.size());
   PolyFit lin_fit_routine(aPowvecVec.size(), mFitBasisCalcDef.GetmSvdCutoffThr());

   // Do the linear fitting
   lin_fit_routine.MakeFit(mFitBasisCalcDef.GetmFitMethod(), aFgProperty, aFgCoords, aFgSigma, aPowvecVec, fit_functions);
   
   // Store least squares linear coefficients
   lin_fit_routine.GetLeastSqrParms(least_sqr_parms);

   // Output option for fitting related quantities, i.e. RMS deviation and chi^2
   if (gFitBasisIoLevel > I_10)
   {
      std::string property_name = mPesInfo.GetMultiLevelInfo().GetPropertyName(aBar);

      Mout << std::endl << " Linear least squares fitting for mode combination " << aModeCombi.MCVec() << " and property " << property_name <<  " resulted in " << std::endl;
      Mout << lin_fit_routine;
   }

   // Return the least squares parameters
   return least_sqr_parms;
}

/**
 * This function figures out how many functions we can use as basis for our fit in this MC. The RefPowVec is set by the input, and may/may not be possible to use?
 *
**/
void DoFitting::FindPowVec
   (  const std::vector<MidasVector>& arXig
   ,  const std::vector<In>& arRefPowVec
   ,  std::vector<In>& arPowVec
   ,  const ModeCombi& arModeCombi
   ,  const bool& aIsInertia
   ,  const bool& aDerivatives
   )  const
{
   if (!aIsInertia && !mPesCalcDef.GetmPesAdga())
   {
      for (In i = I_0; i < arRefPowVec.size(); i++)
      {
         arPowVec.push_back(arRefPowVec[i]);
      }
      return;
   }
   else if (aIsInertia)
   {
      for (In i_mt = I_0; i_mt < arModeCombi.Size(); i_mt++)
      {
         arPowVec.push_back(mFitBasisCalcDef.GetmFitFuncsMuMaxOrder()[arModeCombi.Mode(i_mt)]);
      }

      if (mPesCalcDef.GetmPesAdga())
      {
         for (In i_dim = I_0; i_dim < arModeCombi.Size(); i_dim++)
         {
            In tmp_max_pow = arXig[i_dim].Size();
            if (arModeCombi.Size() == I_1)
            {
               tmp_max_pow = tmp_max_pow - I_1;
            }
            if (arPowVec[i_dim] >= tmp_max_pow)
            {
               arPowVec[i_dim] = tmp_max_pow;
               if (gPesIoLevel > I_10)
               {
                  Mout << " Max degree of polynomial for mode (inertia) " << arModeCombi.Mode(i_dim) << " is equal to " << arPowVec[i_dim] << std::endl;
               }
            }
         }
      }
   }
   // Mess with the polynomial order in Adga calculation
   else if (mPesCalcDef.GetmPesAdga())   
   {
      for (In i = I_0; i < arRefPowVec.size(); i++)
      {
         arPowVec.push_back(arRefPowVec[i]);
      }

      for (In i_dim = I_0; i_dim < arModeCombi.Size(); i_dim++)
      {
         In tmp_max_pow = arXig[i_dim].Size() - I_1; //Nr of SPs - 1

         if (mFitBasisCalcDef.GetmAddFitFuncsConserv())
         {
            if (tmp_max_pow == I_3) //take care of case #SP = 4
            {
               tmp_max_pow++;
            }
            if (tmp_max_pow % I_2 == I_1)
            {
               tmp_max_pow--;
            }
         }
         else
         {
            if (tmp_max_pow % I_2 == I_1)
            {
               tmp_max_pow++; 
            }
         }
         // if the FG is constructed according with the new FG generation
         if (  mPesInfo.GetmUseDerivatives() 
            && !arModeCombi.IsToBeExtrap() 
            && aDerivatives 
            && tmp_max_pow == I_2
            )
         {
            tmp_max_pow += I_2;
         }
         // the possible polynomial order is increased by 2. 
         if (arModeCombi.IsToBeExtrap())
         {
            tmp_max_pow = I_2; //If we are extrapolating, set a stricter bound
         }
         if (arPowVec[i_dim] >= tmp_max_pow)
         {
            arPowVec[i_dim] = tmp_max_pow;

            if (gPesIoLevel > I_10)
            {
               Mout << " Max degree of polynomial for mode " << arModeCombi.Mode(i_dim) << " is equal to " << arPowVec[i_dim] << std::endl;
            }
         }
      }
   }
}

/**
 * Automatic determination of the number of fit-basis functions for a given mode
**/
std::vector<In> DoFitting::AutoMaxFitBasisOrder
   (  const In& aBar
   ,  const ModeCombi& aModeCombi
   ,  const std::vector<MidasVector>& aCgCoords
   ,  const std::vector<MidasVector>& aFgCoords
   ,  const MidasVector& aFgProperty
   ,  const MidasVector& aFgSigma
   ,  const FitFunctions& aFitBasisFuncs
   ,  const std::string& aPropType 
   ,  const std::vector<In>& aMaxIterOrder
   )  const
{
   std::vector<Nb> rmsd_values;
   std::vector<In> max_fit_orders;
   
   // Modify the maximum order and determine the effect this have on the fitting
   for (In mod_order = I_0; mod_order < aMaxIterOrder[I_0]; ++mod_order)
   {
      auto tmp_order = aMaxIterOrder;

      if (tmp_order[I_0] < I_3)
      {
         break;
      }
      else
      {
         tmp_order[I_0] += -I_1 * mod_order;
      }
   
      // Construct all fit-basis functions
      std::vector<InVector> fit_func_orders;
      ConstructAllBasisFunc(aBar, fit_func_orders, aModeCombi, tmp_order, aPropType);
   
      //
      std::vector<In> func_list_sizes(aModeCombi.Size(), I_0);
      for (In i = I_0; i < fit_func_orders.size(); ++i)
      {
         for (In j = I_0; j < fit_func_orders[i].size(); ++j)
         {
            if (func_list_sizes[j] < fit_func_orders[i][j] + I_1)
            {
               func_list_sizes[j] = fit_func_orders[i][j] + I_1;
            }
         }
      }
   
      std::vector<std::vector<MidasFunctionWrapper<Nb>* > > fit_functions(aModeCombi.Size(), std::vector<MidasFunctionWrapper<Nb>* >());
      //
      for (In i_mt = I_0; i_mt < aModeCombi.Size(); ++i_mt)
      {
         aFitBasisFuncs.GetFunctions(i_mt, aModeCombi.Mode(i_mt), func_list_sizes[i_mt], aPropType, fit_functions);
      }
            
      // Do the linear fitting
      PolyFit lin_fit_routine(fit_func_orders.size(), mFitBasisCalcDef.GetmSvdCutoffThr());
      lin_fit_routine.MakeFit(mFitBasisCalcDef.GetmFitMethod(), aFgProperty, aFgCoords, aFgSigma, fit_func_orders, fit_functions);
   
      rmsd_values.push_back(lin_fit_routine.GetRmsd());
      max_fit_orders.push_back(tmp_order[I_0]);
   
      // Check for the is fitting result is sane (or going to be)
      if (rmsd_values[mod_order] > C_I_10_6)
      {
         // If the first result is bad, we need to check if this can be improved before terminating the procedure
         if (rmsd_values.size() == I_1)
         {
            continue;
         }
         // If the current RMSD is worse than the previous we are no longer improving the fit and should terminate the procedure
         else if (rmsd_values[mod_order] > rmsd_values[mod_order - I_1])
         {
            break;
         }
      }
   }

   // Now we should have all the fitting information and we just need to find the smallet RMSD in order to obtain the optimal order in the fit-basis functions 
   auto min_element = std::distance(rmsd_values.begin(), std::min_element(std::begin(rmsd_values), std::end(rmsd_values)));
   auto opt_order = {max_fit_orders[min_element]};
   
   if (gFitBasisIoLevel > I_10)
   {
      Mout << std::endl << " The lowest RMSD for mode Q" << aModeCombi.MCVec()[I_0] << " was found to be " << rmsd_values[min_element] << ", which corresponds to the use of a fit-basis with maximum order in the fit functions to be " << max_fit_orders[min_element] << std::endl;
   }

   return opt_order;
}

/**
 * Construct vector of all basis functions
 *
 * @param aIprop         property #. Used to retrieve the symmetry of the property
 * @param arPowvecVec    container for the whole polynomial basis powers for the given mode combination
 * @param arModeCombi    Actual coupled modes
 * @param arMxPows       Max polynomial degree for each dimension
 * @param aPropType      The property type.
 **/
void DoFitting::ConstructAllBasisFunc
   (  const In& aIprop
   ,  std::vector<InVector>& arPowvecVec
   ,  const ModeCombi& arModeCombi
   ,  const InVector& arMxPows
   ,  const std::string& aPropType
   )  const
{
   arPowvecVec.clear();
   In n_mc = arModeCombi.Size();
   std::vector<In> mode_combi_vec = arModeCombi.MCVec();
   InVector i_cur_vec(n_mc);
   for (In i = I_0; i < n_mc; i++)
   {
      i_cur_vec[i] = I_0;
   }
   In level = I_0;
   AddPowvecsToVec(arPowvecVec, i_cur_vec, n_mc, level, arMxPows);

   // Determines if a maximum order should be enforced upon the fit-basis
   In fit_basis_cutoff = I_0;
   // If the property type is ENERGY, DIPOLE, POLARIZABILITY etc. we use the same maximum order
   if (aPropType != "EFFINERTIAINV")
   {
      fit_basis_cutoff = mFitBasisCalcDef.GetmFitBasisMaxOrder()[n_mc - I_1];
   }
   // Do something special for fitting effective inertia moment inverse tensor
   else if (aPropType == "EFFINERTIAINV")
   {
      fit_basis_cutoff = mFitBasisCalcDef.GetmFitBasisMuMaxOrder()[n_mc - I_1];
   }
   else
   {
      MIDASERROR("Something has gone wrong with the FitBasisMaxOrder settings!");
   }
   // Throw away terms higher than the fixed cut off
   std::vector<InVector> ivect_tmp;
   ivect_tmp.assign(arPowvecVec.begin(), arPowvecVec.end());
   arPowvecVec.clear();
   bool print_sym = false;
   for (In i = I_0; i < ivect_tmp.size(); i++)
   {
      In curr_deg = I_0;
      for (In ind = I_0; ind < ivect_tmp[i].size(); ind++)
      {
         curr_deg += ivect_tmp[i][ind] + I_1;
      }
      if (curr_deg <= fit_basis_cutoff)
      {
         if (  !mPesCalcDef.GetmUseSym() 
            || aIprop != I_0 
            || !mPesCalcDef.GetmPesScreenSymPotTerms()
            )
         {
            // If symmetry is not used then we do nothing
            arPowvecVec.push_back(ivect_tmp[i]);
         }
         else
         {
            if (!mFitBasisCalcDef.GetmAutomaticFitBasis() && mFitBasisCalcDef.GetmGenFitBasisModeMap()[arModeCombi.Mode(I_0)].empty())
            {
               // If symmetry is used, then check for the total symmetric representation, (will always be the first). Not necessarily meaningful for general fit-bases.
               if (!print_sym)
               {
                  if (gPesIoLevel > I_10)
                  {
                     Mout << " Checking symmetry of fit-basis functions " << std::endl;
                  }

                  print_sym = true;
               }

               if (IsTermSymmetric(ivect_tmp[i], mode_combi_vec))
               {
                  arPowvecVec.push_back(ivect_tmp[i]);
               }
            }
            else
            {
               arPowvecVec.push_back(ivect_tmp[i]);
            }
         }
      }
   }

   if (gPesIoLevel > I_11)
   {
      Mout << " Modes coupled:  ";
      for (In i = I_0; i < n_mc; ++i)
      {
         Mout << "  " << arModeCombi.Mode(i);
      }
      Mout << std::endl << std::endl;
      Mout << " Set of basis functions constructed: " << std::endl << std::endl;
      for (std::vector<InVector>::const_iterator ci = arPowvecVec.begin(); ci != arPowvecVec.end(); ++ci)
      {
         Mout << " " << *ci  << std::endl;
      }
      Mout << std::endl;
   }
}

/**
 * Recursive addition to vector of arPowvectors
**/
void DoFitting::AddPowvecsToVec
   (  std::vector<InVector>& arPowvecVec
   ,  InVector& aInVec
   ,  const In& arNmc
   ,  const In& aCoordNr
   ,  const InVector& arMxPows
   )  const
{
   for (In i_t = I_0; i_t < arMxPows[aCoordNr]; ++i_t)
   {
      aInVec[aCoordNr] = i_t;
      if (aCoordNr == (arNmc - I_1) ) // we have reached end, add to list
      {
         arPowvecVec.push_back(aInVec);
      }
      else
      {
         AddPowvecsToVec(arPowvecVec, aInVec, arNmc, aCoordNr + I_1, arMxPows);
      }
   }
}

/**
 * Determine if a given potential energy term has the symmetry specified by aSymLab
**/
bool DoFitting::IsTermSymmetric
   (  const InVector& arIvec
   ,  const std::vector<In>& arModeCombi
   )  const
{
   bool is_sym = true;
   In n_op = mPesInfo.NopSym();
   std::string sym_lab = mPesInfo.GetIrLab(I_0);
   std::vector<In> mode_combi = arModeCombi;
   
   if (gPesIoLevel > I_11)
   {
      Mout << " Checking the fit function of number " << arIvec << " for being of symmetry " << sym_lab << std::endl;
   }

   In n_modes = mode_combi.size();
   std::vector<In> prod_sym_no;
   In ir_sym = mPesInfo.GetIrrepNr(sym_lab);
   for (In i = I_0; i < n_modes; i++)
   {
      In i_mode = mode_combi[i];
      string mode_sym_lab = mPesInfo.GetMolecule().GetModeSymInfo(i_mode);

      if (gPesIoLevel > I_11)
      {
         Mout << " The mode Q" << i_mode << " is of symmetry " << mode_sym_lab << std::endl;
      }

      //WARNING: This assumes that the fit-basis set is ordered consecutively!!!
      In pow = arIvec[i] + I_1;
      if (pow%I_2 == I_0)
      {
         // Then it is totally symmetric
         prod_sym_no.push_back(I_0);
      }
      else
      {
         In mode_sym_no = mPesInfo.GetIrrepNr(mode_sym_lab);
         prod_sym_no.push_back(mode_sym_no);
      }
   }
   // Get the character of the direct product reps.
   for (In i_op = I_0; i_op < n_op; i_op++)
   {
      In i_char = I_1;
      for (In i = I_0; i < n_modes; i++)
      {
         In i_rep = prod_sym_no[i];
         i_char *= In(mPesInfo.GetCharacter(i_rep, i_op));
      }
      if (i_char != In(mPesInfo.GetCharacter(ir_sym, i_op)))
      {
         is_sym = false;
         return is_sym;
      }
   }
   return is_sym;
}

