/**
************************************************************************
*
* @file                CalculationList.cc
*
* Created:             01-06-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) M. Sparta (msparta@chem.au.dk) (original implementation)
*                      Ian Heide Godtliebsen (ian@chem.au.dk) (almost complete rewrite)
*
* Short Description:   stores informations about the List of calculations that have to be 
*                      be computed
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#include "pes/CalculationList.h"

// std headers
#include <vector>
#include <string>
#include <list>
#include <map>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <memory>
#include <cstdlib>
#include <thread>
#include <dirent.h>
#include <regex>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "util/MidasSystemCaller.h"
#include "util/FileSystem.h"
#include "input/PesCalcDef.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "pes/PesFuncs.h"
#include "pes/CalcCode.h"
#include "pes/PesInfo.h"
#include "pes/SimpleRational.h"
#include "pes/FindSymRot.h"
#include "pes/PesSinglePoint.h"
#include "pes/singlepoint/AsyncSinglePointDriver.h"

#include "mpi/Info.h"

// using declarations
using std::make_pair;


/**
 * Parse the List of calculations and identify all unique points using symmetry arguments.
 * Will set parent point for the non-unique calculations.
 *
 * @return Returns a CalculationMap of all unique points in current batch.
 **/
CalculationList::CalculationMap CalculationList::IdentifyUniqueCalculations
   (
   )
{
   // Get the symmetry information
   In nclass = mPesInfo.Nclass();
   
   // Map of unique points and their symmetry related points
   CalculationMap unique_calculations;

   Mout << std::endl << " Building a list of unique calculations: " << std::flush;

   // Run over the list of new points and process points that are symmetry related
   for (auto iter = mNewCodes.begin(); iter != mNewCodes.end(); ++iter)
   {
      const auto& calccode = iter->first;
      auto& calculationproperty = iter->second;

      // if we have already found a symmetry relation, then we just continue.
      if(calculationproperty->mSymmetry.IsSymmetryRelated())
      {
         continue;
      }

      std::map<CalcCode, In> calcset;

      // First finds the symmetry equivalent calccodes.
      for (In iclass = I_0; iclass < nclass; iclass++)
      {
         std::vector<SimpleRational> new_disp_vec = calccode.GetDispVec();
         for (In imode = I_0; imode < calccode.Size(); imode++)
         {
            In mode = calccode.GetModeI(imode);
            std::string sym_type = mPesInfo.GetMolecule().GetModeSymInfo(mode);
            In sym_no = mPesInfo.GetIrrepNr(sym_type);
            In i_char = In(mPesInfo.GetCharacter(sym_no, iclass));
            new_disp_vec[imode] = new_disp_vec[imode] * i_char;
         }

         // add the symmetry related calculation code to the set of symmetry related calculation codes... ... :D
         CalcCode calccode_new(calccode.GetModeVec(), new_disp_vec);
         calcset.emplace(calccode_new, iclass);
      }

      // Check already calculated points
      bool stop = false;
      for(const auto& calccode_pair : calcset)
      {
         auto iter_calculated = mCalculated.find(calccode_pair.first);
         if(iter_calculated != mCalculated.end())
         {
            calculationproperty->mSymmetry.DefineRelation
               (  &(*iter_calculated->second)
               ,  mPesInfo.GetInverseRotation(calccode_pair.second)
               ,  mPesInfo.GetInverseRelation(calccode_pair.second)
               );
            stop = true;
         }
      }
      
      // If a symmetry relation was found amongst already calculated points we stop here
      if (stop)
      {
         continue;
      }
      
      // Point is not symmetry related to a previously calculated point,
      // so we define it as "unique", and identify all calulations in current batch
      // that are related to it and set their symmetry relation.
      auto iter2 = iter;
      ++iter2;
      for(; iter2 != mNewCodes.end(); ++iter2)
      {
         for(const auto& calccode_pair : calcset)
         {
            if(iter2->first == calccode_pair.first)
            {
               iter2->second->mSymmetry.DefineRelation
                  (  &(*calculationproperty)
                  ,  mPesInfo.GetRotation(calccode_pair.second)
                  ,  mPesInfo.GetRelation(calccode_pair.second)
                  );
            }
         }
      }

      unique_calculations.Insert(calculationproperty);
   }
   
   // We now have a list of unique points which we return
   Mout << " Done " << std::endl;
   return unique_calculations;
}

/**
 * Run new singlepoints. Setup list of SinglePointRunners 
 * and subsequently run them in parallel in a threaded fashion.
 *
 * @param aSinglePointName    The name of the type of singlepoints.
 * @param aPesInfo            The pes info.
 * @param aDisplacementGenerator  The generator for molecular displacements.
 * @param aNewCalculationMap  The list of calculated points.
 **/
void CalculationList::RunSinglePoints
   (  const std::string& aSinglePointName
   ,  const PesInfo& aPesInfo
   ,  const DisplacementGenerator& aDisplacementGenerator
   ,  CalculationMap& aNewCalculationMap
   )
{
   // Create list of singlepoints
   std::list<PesSinglePoint> pes_sp_list;
   for(const auto& calccode : aNewCalculationMap)
   {
      auto& calcptr = calccode.second;
      pes_sp_list.emplace_back(aDisplacementGenerator, calcptr->mCalcCode, calcptr->mCalculationNumber);
   }

   bool doMLPes = aPesInfo.GetMLPes();

   // Create list of single point jobs
   const auto& multi_level_info = aPesInfo.GetMultiLevelInfo();
   std::list<SinglePointRunner> submit_list;
   for(auto& pes_sp : pes_sp_list)
   {

      // Add SinglePointRunner with Setup and Teardown functions to the list
      submit_list.emplace_back
         (  pes_sp.GetCalcCode()
            // Setup
         ,  [&pes_sp, aSinglePointName, &multi_level_info, &doMLPes]() 
            { 
               pes_sp.ConstructStructure();

               auto sp_info = SinglePointCalc(aSinglePointName).SpInfo();
               sp_info["SETUPDIR"] = multi_level_info.SetupDir();
               sp_info["SAVEDIR"]  = multi_level_info.SaveIoDir();
               sp_info["SPSCRATCHPREFIX"] = gPesCalcDef.GetSpScratchDirPrefix();
               sp_info["SPLEVELPREFIX"]   = multi_level_info.SubDir();
               if (doMLPes) sp_info["MLPES"] = "TRUE";
               SinglePoint sp(sp_info, multi_level_info.PropInfo(), pes_sp.GetStructure(), pes_sp.GetCalculationNumber(), pes_sp.GetCalcCode());
               return sp;
            }
            // Teardown
         ,  [&pes_sp, &aNewCalculationMap](const GeneralProp& aProp) 
            {
               if (gPesCalcDef.GetmPesCalcMuTens() || gPesCalcDef.GetmZmatInPes())
               {
                  pes_sp.AddCoriolisTermsToProperties(const_cast<GeneralProp&>(aProp));
               }
               auto& prop = aNewCalculationMap.GetCalculationProperty(pes_sp.GetCalculationNumber());
               prop.mProperties = aProp;
            }
         );
   }

   // Calculate all singlepoints on the list.
   AsyncSinglePointDriver sp_driver(multi_level_info.Nthreads(), multi_level_info.SaveIoDir(), gPesCalcDef.GetmDumpSpInterval());
   sp_driver.EvaluateList(submit_list);
}

/**
 * Constructor
 *
 * @param aMolecule  The molecule.
 * @param aPesInfo   A reference to the abominable PESINFO!!! WAARGH ! 3:-O
 **/
CalculationList::CalculationList
   (  const molecule::MoleculeInfo& aMolecule
   ,  const PesInfo& aPesInfo
   ) 
   :  mMolecule(aMolecule)
   ,  mPesInfo(aPesInfo)
   ,  mDisplacementGenerator(gPesCalcDef, aPesInfo, aMolecule)
   ,  mPropertyNames()
{ 
}

/**
 * Destructor. 
 **/
CalculationList::~CalculationList
   (
   )
{ 
}

/**
 * Add an entry into the calculation list.
 *
 * @param arCalcCode   The calculation code to add.
 *
 * @return             Returns true if the calculation was added and false if it was already there.
 **/
bool CalculationList::AddEntry
   (  const CalcCode& arCalcCode
   )
{
   auto iter = mCalculated.find(arCalcCode);
   auto new_iter = mNewCodes.find(arCalcCode);
   if((iter == mCalculated.end()) && (new_iter == mNewCodes.end()))
   {
      std::shared_ptr<CalculationProperty> prop(new CalculationProperty(arCalcCode, mCalculationNumber++, mPesInfo.GetMultiLevelInfo().PropInfo()));
      mNewCodes.Insert(prop);
      return true;
   }
   return false;
}

/**
 * EvaluateList - evaluate a list of singlepoints (in parallel or 'serial').
 * Will first identify all symmetry relations bewteen newly added pionts 
 * and create a list of unique calculations. 
 * If MPI all unique points will be distributed among the nodes and calculated in parallel,
 * and if not all points will be calculated on "master".
 * Afterwards all symmtry related point will be "calculated" using the symmetry relations found earlier.
 * All new points will be added to the set of all points and the set of new points will be cleared for next batch.
 * In the end property files will be dumped on disc for the pes module to load, and for restartabliy reasons.
 **/
void CalculationList::EvaluateList
   (
   )
{
   // Identify all unique singlepoints.
   auto unique_calculations = IdentifyUniqueCalculations();
   
   // Do some output
   std::vector<std::string> v_str(I_3);
   v_str[I_0] = std::to_string(mCalculated.size() + mNewCodes.size());
   v_str[I_1] = std::to_string(mNewCodes.size());
   v_str[I_2] = std::to_string(unique_calculations.size());
   Uin width = I_0;
   for(auto&& str: v_str)
   {
      width = std::max(static_cast<Uin>(str.size()), width);
   }
   for(auto&& str: v_str)
   {
      str.insert(I_0, width - str.size(), ' ');
   }
   Out72Char(Mout, '+', '-', '+');
   OneArgOut72(Mout, " Evaluating list of singlepoints", '|');
   OneArgOut72(Mout, "    Total : " + v_str[I_0]);
   OneArgOut72(Mout, "      New : " + v_str[I_1]);
   OneArgOut72(Mout, "   Unique : " + v_str[I_2]);
   Out72Char(Mout, '+', '-', '+');

   // Run singlepoints (either MPI or not).
   if (unique_calculations.size())
   {
#ifdef VAR_MPI
      RunSinglePointsMaster(mpi::CommunicatorWorld(), mPesInfo.GetSinglePointName(), mPesInfo, mDisplacementGenerator, unique_calculations);
#else
      RunSinglePoints(mPesInfo.GetSinglePointName(), mPesInfo, mDisplacementGenerator, unique_calculations);
#endif /* VAR_MPI */
   }
   
   // Apply symmetry relations.
   ApplySymmetryRelations();
   
   // Add all newly calculated points to full list of points and clear mNewCodes.
   for (const auto& prop : mNewCodes)
   {
      mCalculated.Insert(prop.second);
   }
   mNewCodes.Clear();
   
   // Lastly dump properties to disk
   DumpPropFiles();
}

/**
 * For each symmetry related point, we apply the symmetry relation found earlier and
 * calculate the properties of the point from the parent point.
 **/
void CalculationList::ApplySymmetryRelations
   ( 
   )
{
   // loop over new points
   for(auto& calculation : mNewCodes)
   {
      auto& calculationproperty = calculation.second;
      
      // if symmetry related apply the symmetry relation.
      if(calculationproperty->mSymmetry.IsSymmetryRelated())
      {
         calculationproperty->mProperties = calculationproperty->mSymmetry.ApplyRelation();
      }
   }
}

/**
 * Check for already calculated points.
 *
 * Will go through all new points and see whether they have already been calculated before.
 **/
void CalculationList::CheckForCalcsDone
   (
   )
{
   struct dirent *pDirent;
   DIR *pDir;
   
   // Try to open savedir
   pDir = opendir(mPesInfo.GetMultiLevelInfo().SaveIoDir().c_str());
   if (pDir == NULL) 
   {
      Mout << " Cannot open directory " << mPesInfo.GetMultiLevelInfo().SaveIoDir() << "\n";
      return;
   }
   
   // Loop through files in savedir
   while ((pDirent = readdir(pDir)) != NULL) 
   {
      // Match all files named 'sp_restart_N.mrestart', where N is an integer rank (can be multiple digits)
      std::regex fileregex("sp_restart_[0-9]+\\.mrestart");
      std::string filename(pDirent->d_name);
      if (std::regex_match(filename, fileregex))
      {
         if (gPesIoLevel > I_8)
         {
            Mout << " Found single point restart file: " << std::endl 
                 << "  " << mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + filename << std::endl;
         }
         
         std::ifstream restart_file(mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + filename);
         std::string calccode;

         // Loop over all singlepoints in file and add to map of calculated points
         while (restart_file >> calccode)
         {
            std::shared_ptr<CalculationProperty> prop(new CalculationProperty(calccode, mCalculationNumber++, mPesInfo.GetMultiLevelInfo().PropInfo()));
            prop->mProperties.ReadFromStream(restart_file);
            mCalculated.Insert(prop);
         }
      }
   }
   
   // Close directory again
   closedir (pDir);
}

/**
 * Dump the prop_no_x.mpoints and prop_no_x_bar.mpoints files.
 **/
void CalculationList::DumpPropFiles
   (
   )
{
   // Get reference calculation
   const GeneralProp& reference_prop = mCalculated.GetCalculationProperty(CalcCode("*#0*")).mProperties;

   // get properties names
   std::vector<string> names = reference_prop.GetDescriptors();
   mPropertyNames = names;

   // get properties values
   std::vector<Nb> ref_values = reference_prop.GetValues();
   
   In arNoOfProps = -1;
   if (names.size() == ref_values.size()) 
   {
      arNoOfProps = names.size();
   }
   else 
   {
      MIDASERROR("we have a problem with the number of properties");
   }
   
   // we start by Dumping "prop_files.mpesinfo"
   // ----------------------------------------------------------------------
   std::string OutLabel = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "prop_files.mpesinfo";
   if (InquireFile(OutLabel))
   {
      RmFile(OutLabel);
   }
   std::ofstream Out(OutLabel.c_str(), ios_base::app);
   Out << "# Electronic State  Property File  Property Label" << std::endl;

   for (In i = I_0; i < arNoOfProps; i++) 
   {
      Out << gPesCalcDef.GetmPesExcState()
          << "  " << "prop_no_" << i + I_1 << ".mop" 
          << "  " << names[i]
          << std::endl;
   }
   
   Out.close();
   
   // we dump now the "Reference values"
   // ----------------------------------------------------------------------
   std::string kkk = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "prop_ref_values.mpesinfo";
   if (InquireFile(kkk)) 
   {
      RmFile(kkk);
   }
   std::ofstream Reference(kkk.c_str(),ios_base::out);
   Reference.setf(ios::scientific);
   Reference.setf(ios::uppercase);
   Reference.precision(I_22);

   for (In i = I_0; i < arNoOfProps; i++)
   {
      Reference << i + I_1 << "   " << ref_values[i] << std::endl;
   }

   Reference.close();
   
   // Dump properties
   // ----------------------------------------------------------------------
   // Open the prop_no_x.mpoints files
   std::unique_ptr<ofstream[]> File(new ofstream [arNoOfProps]);
   for (In i = I_0; i < arNoOfProps; i++)
   {
      std::string PropFileName = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "prop_no_" + std::to_string(i + I_1) + ".mpoints";
      File[i].open(PropFileName.c_str());
      File[i].setf(ios::scientific);
      File[i].setf(ios::uppercase);
      File[i].precision(I_22);
   }

   // Open the prop_no_X_bar.mpoints files
   std::unique_ptr<ofstream[]> File_reo(new ofstream [arNoOfProps]);
   for (In i = I_0; i < arNoOfProps; i++)
   {
      std::string PropFileName_reo = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/" + "prop_no_" + std::to_string(i + I_1) + "_bar.mpoints";
      File_reo[i].open(PropFileName_reo.c_str());
      File_reo[i].setf(ios::scientific);
      File_reo[i].setf(ios::uppercase);
      File_reo[i].precision(I_22);
   }
   
   // Parse the calculation List and print properties
   for (const auto& point : mCalculated.CalcNumberMap())
   {
      // get the calculation number
      const CalcCode& calccode = point.second->mCalcCode;
      const GeneralProp& properties = point.second->mProperties;
      const auto calc_number = point.second->mCalculationNumber;

      // get the vector with the numbers
      std::vector<Nb>  values = properties.GetValues();

      for (In i = I_0; i < arNoOfProps; i++)
      {
         File[i]     << calc_number <<  "   " << calccode <<  "   " << values[i] << std::endl;
         File_reo[i] << calc_number <<  "   " << calccode <<  "   " << values[i] - ref_values[i] << std::endl;
      }
   }
   
   // Dump derivatives
   // ----------------------------------------------------------------------
   for (const auto& point : mCalculated.CalcNumberMap())
   {
      const CalcCode& calccode = point.second->mCalcCode;
      GeneralProp properties = point.second->mProperties;
      const auto calc_number = point.second->mCalculationNumber;

      // This should not be done for some model potentials, but no model potential have derivatives as of this writing.
      properties.TransDerByNormCoord(mMolecule);

      for (In i = I_0; i < arNoOfProps; i++) 
      {
         std::string prop_no_s = std::to_string(i + 1);
         
         const MidasVector& first_der  = properties.GetFirstDerivatives (i);
         const MidasMatrix& second_der = properties.GetSecondDerivatives(i);

         if(first_der.Size() == 0)
            continue;

         // setup Data container
         DataCont derivs;
         derivs.SaveUponDecon(true);
         map<In,In>::iterator off_it = mDerivOffsets.find(i + 1);
         In offset = I_0;
         if(off_it == mDerivOffsets.end()) 
         {
            mDerivOffsets.insert(std::make_pair(i + 1, offset));
            off_it = mDerivOffsets.find(i + 1);
         }
         else 
         {
            offset = off_it->second;
         }
         In n_derivs = mPesInfo.GetMolecule().GetNoOfVibs();
         
         // update offset
         off_it->second += n_derivs;
         std::string label = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/prop_no_" + prop_no_s + "_derivatives.mbinary";
         if(offset != I_0) 
         {
            derivs.GetFromExistingOnDisc(offset, label);
            if (! derivs.ChangeStorageTo("InMem",true)) 
            {
               MIDASERROR("Expected to find derivatives!! ("+std::to_string(offset)+")");
            }
         }
         derivs.NewLabel(label);
         In first_offset = derivs.Size();
         derivs.SetNewSize(first_offset + n_derivs);
         
         // NOW we may write
         derivs.DataIo(IO_PUT, const_cast<MidasVector&>(first_der), first_der.Size(), first_offset);
         n_derivs = second_der.Ncols() * second_der.Ncols();
         off_it->second += n_derivs;
         In second_offset = derivs.Size();
         if(n_derivs!=I_0) 
         {
            derivs.SetNewSize(second_offset + n_derivs);
            derivs.DataIo(IO_PUT, const_cast<MidasMatrix&>(second_der), n_derivs, second_der.Ncols(), second_der.Ncols(), second_offset);
         }
         
         // done, just write info file
         std::string filename = mPesInfo.GetMultiLevelInfo().SaveIoDir() + "/prop_no_" + prop_no_s + "_derivatives_navigation.mpesinfo";

         std::ofstream ofs(filename.c_str(), ios::app);
         ofs << point.second->mCalculationNumber << "   " << calccode << "   " << first_offset;
         if(n_derivs != I_0)
         {
            ofs << "   " << second_offset;
         }
         ofs << std::endl;
         ofs.close();
      }
   }

   // Do some clean-up
   // ----------------------------------------------------------------------
   for (In i = I_0; i < arNoOfProps; i++)
   {
      File[i].close();
      File_reo[i].close();
   }
}

/**
 * THIS FUNCTION SHOULD GO AWAY!
 **/
std::string CalculationList::GetCalcCodeFromCalcNumber
   (  In aCalcNumber
   )  const
{
   const auto& calculation_property = mNewCodes.GetCalculationProperty(aCalcNumber);
   return calculation_property.mCalcCode.GetString();
}

/**
 * THIS FUNCTION SHOULD GO AWAY!
 **/
In CalculationList::GetCalcNrFromCode
   (  const std::string& aCalcCode
   )  const
{
   //if(aCalcCode == "*#0*") return -I_1;

   std::string calccode = aCalcCode;
   
   // Remove any "#0#"
   while(calccode.find("#0#") != calccode.npos)
   {
      calccode.erase(calccode.find("#0#"), I_2);
   }
   
   // Bo's hack.... He will fix it after he hands in :D (this was 3 years ago btw)... 
   std::string check = "0/"+std::to_string(In(pow(I_2,gPesCalcDef.GetmPesIterMax()+In(gPesCalcDef.GetmPesItGridExpScalFact()))));
   if(calccode.find(check) != calccode.npos)
   {
      return -I_1;
   }

   const auto& calculation_property = mCalculated.GetCalculationProperty(calccode);
   return calculation_property.mCalculationNumber;
}


/**
 * Overload of ouput operators for CalculationList.
 *
 * @param aOs    The stream to print to.
 * @param aCl    The list to print.
 *
 * @return   Return stream for chaining of '<<'.
 **/
ostream& operator<<
   (  ostream& aOs
   ,  const CalculationList& aCl
   )
{
   aOs << " NO PRINT OUT FOR CALCULATION LIST YET. " << std::endl;
   return aOs;
}
