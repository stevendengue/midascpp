/**
************************************************************************
* 
* @file                PesProperty.h
*
* Created:             24-02-2010
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   Property datatype 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDAS_PES_PES_PROPERTY_H_INCLUDED
#define MIDAS_PES_PES_PROPERTY_H_INCLUDED

// std headers
#include <string>
#include <vector>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "util/Io.h"
#include "pes/PropertyInfo.h"

#ifdef VAR_MPI
#include "mpi/Blob.h"
#endif /* VAR_MPI */

namespace midas
{
namespace pes 
{

/**
 * Holds the information on a single property for a singlepoint, 
 * this could e.g be GROUND_STATE_ENERGY, X_DIPOLE, etc.
 **/
class PesProperty
{
   private:
      //! Order of the property
      In mOrder;
      //! Rotation group of the property
      In mRotGroup; 
      //! Element index in property tensor.
      std::vector<In> mElement; 
      //! Property descriptor, e.g. GROUND_STATE_ENERGY, X_DIPOLE, etc.
      std::string mDescriptor;
      //! Derivative file
      std::string mDerFile; 
      //! holds value of property
      Nb mValue; 
      //! holds first derivatives of property
      MidasVector mFirstDer; 
      //! holds second derivative of property
      MidasMatrix mSecondDer; 

   public:
      //! Default constructor
      PesProperty() = default;

      //! Default destructor
      ~PesProperty() = default;

      //! Constructor from PropertyInfoEntry.
      PesProperty(const PropertyInfoEntry& aPropertyInfoEntry);

      //! Deleted copy constructor.
      PesProperty(const PesProperty&) = default;

      //! Default move constructor.
      PesProperty(PesProperty&&) = default;

      //!
      PesProperty& operator=(const PesProperty& aOtherInfo);
      
      //! Correct nuclear order of derivatives.
      void CorrectNucOrder(const std::vector<In>& aNucMap);

      //!
      bool HasDerivatives() const;

      //!@{
      //! Setters
      void SetValue(Nb aN) { mValue = aN; }
      void SetDerFile(const std::string& aS) { mDerFile = aS; }
      void SetFirstDerivatives(const MidasVector& aV);
      void SetSecondDerivatives(const MidasMatrix& aM);
      void SetDescription(const std::string& aS) { mDescriptor = aS; }
      //!@}
      
      //!@{
      //! Getters
      Nb GetValue()              const { return mValue; }
      In GetRotGroup()           const { return mRotGroup; }
      std::vector<In> GetElement()    const { return mElement; }
      In GetOrder()              const { return mOrder; }
      std::string GetDerFile()   const { return mDerFile; }
      const MidasVector& GetFirstDerivatives() const {return mFirstDer;}
      MidasVector& GetFirstDerivatives() {return mFirstDer;}
      const MidasMatrix& GetSecondDerivatives() const {return mSecondDer;}
      MidasMatrix& GetSecondDerivatives() {return mSecondDer;}
      bool GetDerInfo() const { return mDerFile.size() > I_0; }
      const std::string& GetDescriptor() const {return mDescriptor;}
      //!@}
      
      //! Dump to file for restart.
      void DumpToStream(std::ostream& aOs) const;
      
      //! Dump to file for restart.
      void ReadFromStream(std::istream& aOs);
      
      //! Output operator overload
      friend ostream& operator<<(ostream& aOS, const PesProperty& aPropInfo);
      
      // Mpi stuff
      // ----------------------------------------------------------------------
#ifdef VAR_MPI
      //! Constructor from mpi::Blob
      PesProperty(mpi::Blob& aBlob);

      //! Create an mpi::Blob
      void LoadIntoMpiBlob(mpi::Blob& aBlob) const;
#endif /* VAR_MPI */
};

} /* namespace pes */
} /* namespace midas */

#endif /* MIDAS_PES_PES_PROPERTY_H_INCLUDED */
