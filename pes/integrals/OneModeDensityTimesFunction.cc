/**
 *******************************************************************************
 * 
 * @file    OneModeDensityTimesFunction.cc
 * @date    26-04-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include "pes/integrals/OneModeDensityTimesFunction.h"
#include "pes/integrals/OneModeDensityInterpolator.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"
#include "util/Error.h"

namespace midas{
namespace pes{
namespace integrals{

/***************************************************************************//**
 * Stores the const-refs to the argument density and function.
 ******************************************************************************/
OneModeDensityTimesFunction::OneModeDensityTimesFunction
   (  const dens_t& arDens
   ,  const func_t& arFunc
   )
   :  mDens(arDens)
   ,  mFunc(arFunc)
{
}

/***************************************************************************//**
 * 
 ******************************************************************************/
Nb OneModeDensityTimesFunction::operator()
   (  Nb aInputValue
   )  const
{
   return mDens(aInputValue) * mFunc(aInputValue);
}

/***************************************************************************//**
 *
 ******************************************************************************/
bool OneModeDensityTimesFunction::operator==
   (  const OneModeDensityTimesFunction& arOther
   )  const
{
   return mFunc.Compare(arOther.mFunc) && mDens == arOther.mDens;
}

/***************************************************************************//**
 * @note
 *    Throws MIDASERROR in case of size mismatch.
 * @warning
 *    Probably leaves dangling references (OneModeDensityTimesFunction::mDens
 *    and mFunc) if calling this function with rvalue arguments.
 *
 * @param[in] arDensities
 *    One density functor per mode.
 * @param[in] arFunctions
 *    One function per mode.
 * @return
 *    A vector with one OneModeDensityTimesFunction per mode.
 ******************************************************************************/
std::vector<OneModeDensityTimesFunction> CombineDensitiesAndFunctionOneTerm
   (  const std::vector<OneModeDensityTimesFunction::dens_t>& arDensities
   ,  const std::vector<OneModeDensityTimesFunction::func_t>& arFunctions
   )
{
   if (arDensities.size() != arFunctions.size())
   {
      MIDASERROR( "Size mismatch; arDensities.size() = "+std::to_string(arDensities.size())+
                  ", but arFunctions.size() = "+std::to_string(arFunctions.size())+
                  ".");
   }
   std::vector<OneModeDensityTimesFunction> v;
   v.reserve(arFunctions.size());
   for(Uin m = I_0; m < arFunctions.size(); ++m)
   {
      v.emplace_back(arDensities[m], arFunctions[m]);
   }
   return v;
}

/***************************************************************************//**
 * @note
 *    Throws MIDASERROR in case of size mismatch.
 * @warning
 *    Probably leaves dangling references (OneModeDensityTimesFunction::mDens
 *    and mFunc) if calling this function with rvalue arguments.
 *
 * @param[in] arDensities
 *    One density functor per mode.
 * @param[in] arFunctions
 *    Vector of terms with one function per mode.
 * @return
 *    A vector of vectors; each of the latter with one
 *    OneModeDensityTimesFunction per mode.
 ******************************************************************************/
std::vector<std::vector<OneModeDensityTimesFunction>> CombineDensitiesAndFunctions
   (  const std::vector<OneModeDensityTimesFunction::dens_t>& arDensities
   ,  const std::vector<std::vector<OneModeDensityTimesFunction::func_t>>& arFunctions
   )
{
   std::vector<std::vector<OneModeDensityTimesFunction>> v;
   v.reserve(arFunctions.size());
   for(const auto& v_funcs: arFunctions)
   {
      v.push_back(CombineDensitiesAndFunctionOneTerm(arDensities, v_funcs));
   }
   return v;
}

} /*namespace integrals*/
} /*namespace pes*/
} /*namespace midas*/
