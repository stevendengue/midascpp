/**
 *******************************************************************************
 * 
 * @file    MidasPesIntegrals_Fwd.h
 * @date    26-04-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    Forward declares the midas::pes::integrals namespace and related
 *    entities. Include in other header files if only declarations are
 *    necessary.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MIDASPESINTEGRALS_FWD_H_INCLUDED
#define MIDASPESINTEGRALS_FWD_H_INCLUDED

namespace midas{
namespace pes{
namespace integrals{

   template<typename F> class ModeCombiIntegrals;
   template<typename F> class OneModeIntegrals;
   template<typename F> class SumOverProductIndexer;

   class OneModeDensityInterpolator;
   class OneModeDensityTimesFunction;

} /*namespace integrals*/
} /*namespace pes*/
} /*namespace midas*/

#endif/*MIDASPESINTEGRALS_FWD_H_INCLUDED*/
