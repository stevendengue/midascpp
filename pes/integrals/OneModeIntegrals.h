/**
 *******************************************************************************
 * 
 * @file    OneModeIntegrals.h
 * @date    19-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef ONEMODEINTEGRALS_H_INCLUDED
#define ONEMODEINTEGRALS_H_INCLUDED

#include <functional>
#include <string>
#include <vector>
#include <utility>

#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"

#include "quadrature_wrappers/integration/GaussLegendre.h"


namespace midas{
namespace pes{
namespace integrals{

/**
 * @brief
 *    Integrals over one-mode functions on various intervals.
 *
 * The template parameter shall be a function/functor of one variable, i.e.
 * have an overload of `double operator()(double)`.
 *
 * Class to calculate and hold integrals for some given one-mode functions,
 * over some given intervals, and potentially with a non-unit scaling factor.
 * For use in e.g. ADGA density/potential integrals.  Integrals are calculated
 * as simple n-point Gauss-Legendre quadratures, the number of points n being
 * given as input to constructor.
 *
 * I.e. with the \f$ i \f$'th interval being \f$ [a_i ; b_i] \f$, the
 * \f$ j \f$'th function being \f$ f_j (x) \f$, and the scaling factor being
 * \f$ \alpha \f$, calculates
 * \f[
 *    I_{ij} = \int_{a_i}^{b_i} f(\alpha x) dx
 * \f]
 *
 * Integrals are evaluated at time of construction, thereafter accessible via
 * member function Integral(Uin, Uin).
 *
 * After construction, intervals and functions are only referred to by index;
 * therefore bookkeeping of such should be done externally.
 **/
template<typename F>
class OneModeIntegrals
{
   public:
      //! The type of the one-mode functions is given as template argument.
      using function_t = F;

      //! The type/class used to evaluate integrals.
      using quadrature_t = midas::quadrature_wrappers::GaussLegendre;

      //! Construct from num. quad. points, 1-mode functions, intervals, scaling factor.
      OneModeIntegrals(Uin, std::vector<function_t>, std::vector<std::pair<Nb,Nb>>, Nb = C_1);

      //@{
      //! Copies deleted (shouldn't be necessary, but change at will), moves are default.
      OneModeIntegrals() = delete;
      OneModeIntegrals(const OneModeIntegrals<F>&) = delete;
      OneModeIntegrals<F>& operator=(const OneModeIntegrals<F>&) = delete;
      OneModeIntegrals(OneModeIntegrals<F>&&) = default;
      OneModeIntegrals<F>& operator=(OneModeIntegrals<F>&&) = default;
      ~OneModeIntegrals() = default;
      //@}


      //@{
      //! Numbers of intervals/functions.
      Uin NumIntervals() const;
      Uin NumFunctions() const;
      //@}

      //@{
      //! Beginning/end of the i'th interval.
      Nb IntervalBegin(Uin) const;
      Nb IntervalEnd(Uin) const;
      //@}

      //! Integral value for specific interval and function.
      Nb Integral(Uin aInterval, Uin aFunction) const;

   private:
      //! The object responsible for evaluating integrals.
      quadrature_t mQuadRule;
      
      //! The one-mode functions.
      std::vector<function_t> mFunctions;
      
      //! The intervals.
      std::vector<std::pair<Nb,Nb>> mIntervals;

      //! Stores the integrals, when calculated.
      std::vector<std::vector<Nb>> mIntegrals;

      //! Scaling factor, if relevant (can just be 1).
      Nb mScalingFactor;

      //! Calculate all integrals.
      void CalculateIntegrals();

      //! Calculate specific integral using the quadrature rule.
      Nb Quadrature(const function_t&, Nb, Nb, Nb) const;
};

} /*namespace integrals*/
} /*namespace pes*/
} /*namespace midas*/

#include "pes/integrals/OneModeIntegrals_Impl.h"

#endif/*ONEMODEINTEGRALS_H_INCLUDED*/
