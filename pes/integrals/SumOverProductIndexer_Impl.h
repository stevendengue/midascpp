/**
 *******************************************************************************
 * 
 * @file    SumOverProductIndexer_Impl.h
 * @date    19-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @brief
 *    SumOverProductIndexer and related stuff.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include <exception>
#include <utility>

#include "pes/integrals/SumOverProductIndexer.h"
#include "util/Error.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"

namespace midas{
namespace pes{
namespace integrals{

/***************************************************************************//**
 * Takes a vector of coefficients, a vector of factors in the
 * sum-over-product, and an equality comparison function.
 * Function indexing is done during construction, meaning the object is _ready
 * to use_ directly after construction.
 *
 * @note
 *    Throws MIDASERROR if the number of terms are inconsistent between
 *    aCoefficients and arFunctionsForEachTerm.
 *
 * @note
 *    Throws MIDASERROR if the number of modes (num. factors/functions) are not
 *    identical for all terms.
 *
 * @param[in] aCoefficients
 *    Vector of coefficients, one for each term.
 * @param[in] arFunctionsForEachTerm
 *    Vector with elements for each term, each element being a vector of
 *    functions, one function for each mode.
 * @param[in] aFunctionEquality
 *    An equality comparison, telling the object how to find out that two
 *    functions are equal. Should be functor taking two function_t arguments
 *    and returning bool.
 ******************************************************************************/
template<typename F>
SumOverProductIndexer<F>::SumOverProductIndexer
   (  std::vector<Nb> aCoefficients
   ,  const std::vector<std::vector<function_t>>& arFunctionsForEachTerm
   ,  equality_t aFunctionEquality
   )
   :  mNumModes(AssertSameNumModesAndTerms(aCoefficients, arFunctionsForEachTerm))
   ,  mFunctionEquality(std::move(aFunctionEquality))
   ,  mCoefficients(std::move(aCoefficients))
   ,  mFunctionIndices(I_0)
   ,  mFunctions(NumModes())
{
   mFunctionIndices.reserve(NumTerms());
   for(const auto& f: arFunctionsForEachTerm)
   {
      mFunctionIndices.push_back(AssignIndex(f));
   }
}


/***************************************************************************//**
 * @return
 *    The number of modes/factors of each term in the sum (must be the same for
 *    every term).
 ******************************************************************************/
template<typename F>
Uin SumOverProductIndexer<F>::NumModes
   (
   )  const
{
   return mNumModes;
}

/***************************************************************************//**
 * @return
 *    The number of terms in the sum.
 ******************************************************************************/
template<typename F>
Uin SumOverProductIndexer<F>::NumTerms
   (
   )  const
{
   return mCoefficients.size();
}

/***************************************************************************//**
 * @note
 *    Throws MIDASERROR if the given mode number is out of range.
 *
 * @param[in] aMode
 *    The index of the mode in question. (0,...,NumModes() - 1)
 * @return
 *    The number of unique functions (as determined by the provided equality
 *    comparison) for that mode.
 ******************************************************************************/
template<typename F>
Uin SumOverProductIndexer<F>::NumFunctions
   (  Uin aMode
   )  const
{
   try
   {
      return mFunctions.at(aMode).size();
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Out-of-range: " + std::string(oor.what()));
      return 0;
   }
}

/***************************************************************************//**
 * @note
 *    Throws MIDASERROR if the given term index is out of range.
 *
 * @param[in] aTerm
 *    The index of the term in question.
 * @return
 *    The linear expansion coefficient for that term.
 ******************************************************************************/
template<typename F>
Nb SumOverProductIndexer<F>::Coefficient
   (  Uin aTerm
   )  const
{
   try
   {
      return mCoefficients.at(aTerm);
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Out-of-range: " + std::string(oor.what()));
      return 0;
   }
}

/***************************************************************************//**
 * E.g. if the one-mode functions have been found to be:
 *     mode 0: f_0, f_1, f_2,...
 *     mode 1: g_0, g_1, g_2,...
 *     mode 2: h_0, h_1, h_2,...
 * and the term `i` is `c_i f_0 * g_2 * h_1`, then this function will return
 * `{0, 2, 1}`.
 *
 * @note
 *    Throws MIDASERROR if the given term index is out of range.
 *
 * @param[in] aTerm
 *    The index of the term in question.
 * @return
 *    The indices of the functions of that term.
 ******************************************************************************/
template<typename F>
const std::vector<Uin>& SumOverProductIndexer<F>::FunctionIndices
   (  Uin aTerm
   )  const
{
   try
   {
      return mFunctionIndices.at(aTerm);
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Out-of-range: " + std::string(oor.what()));
      return mFunctionIndices.front();
   }
}

/***************************************************************************//**
 * @param[in] aMode
 *    The mode in question.
 * @return
 *    Vector of unique one-mode functions of the given mode.
 ******************************************************************************/
template<typename F>
const std::vector<typename SumOverProductIndexer<F>::function_t>& SumOverProductIndexer<F>::Functions
   (  Uin aMode
   )  const
{
   try
   {
      return mFunctions.at(aMode);
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Out-of-range: " + std::string(oor.what()));
      return mFunctions.front();
   }
}

/***************************************************************************//**
 * Since only an equality comparison is provided, has to search _linearly_ in
 * the vector of current functions. This is probably okay for around 10, maybe
 * more functions. But beware if using for substantially more one-mode
 * functions; at some point an ordered set approach or something _could_ be
 * better (but don't know what's the breaking point). Need a less-than
 * comparison for functions then, though.
 *
 * @param[in] aMode
 *    The mode in question.
 * @param[in] arFunction
 *    The function to find the index for.
 * @return
 *    The index the given function has in the current vector of functions. If
 *    not already contained, returns current size of that vector.
 ******************************************************************************/
template<typename F>
Uin SumOverProductIndexer<F>::FindFunctionIndex
   (  Uin aMode
   ,  const function_t& arFunction
   )  const
{
   const auto& funcs = Functions(aMode);
   const auto iter_found = std::find_if
      (  funcs.begin()
      ,  funcs.end()
      ,  [this, &arFunction](const function_t& f)->bool {return mFunctionEquality(arFunction,f);}
      );
   return std::distance(funcs.begin(), iter_found);
}

/***************************************************************************//**
 * If a function is not already contained in the current vector of functions of
 * the object, it is added, so that valid indices can be returned.
 *
 * @param[in] arVecFuncs
 *    A vector of functions, corresponding to one term in the sum over
 *    products.
 * @return
 *    A vector of indices for the given functions, giving their
 *    positions/indices in the stored unique one-mode-functions.
 ******************************************************************************/
template<typename F>
std::vector<Uin> SumOverProductIndexer<F>::AssignIndex
   (  const std::vector<function_t>& arVecFuncs
   )
{
   std::vector<Uin> v_index;
   v_index.reserve(arVecFuncs.size());
   for(Uin m = I_0; m < arVecFuncs.size(); ++m)
   {
      Uin i = FindFunctionIndex(m, arVecFuncs[m]);
      if (i == NumFunctions(m))
      {
         Functions(m).push_back(arVecFuncs[m]);
      }
      v_index.push_back(i);
   }
   return v_index;
}

/***************************************************************************//**
 * @param[in] aMode
 *    The mode in question.
 * @return
 *    Vector of unique one-mode functions of the given mode.
 ******************************************************************************/
template<typename F>
std::vector<typename SumOverProductIndexer<F>::function_t>& SumOverProductIndexer<F>::Functions
   (  Uin aMode
   )
{
   try
   {
      return mFunctions.at(aMode);
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Out-of-range: " + std::string(oor.what()));
      return mFunctions.front();
   }
}

/***************************************************************************//**
 * @note
 *    Throws MIDASERROR if the sizes of the coefficient vector and the function
 *    vector differ.
 *
 * @note
 *    Throws MIDASERROR if the same number of one-mode factors is not found for
 *    every term.
 *
 * @param[in] arCoefs
 *    The coefficients of the terms.
 * @param[in] arFuncs
 *    The product functions of the terms.
 * @return
 *    The number of modes per term, must be common for all terms.
 ******************************************************************************/
template<typename F>
Uin SumOverProductIndexer<F>::AssertSameNumModesAndTerms
   (  const std::vector<Nb>& arCoefs
   ,  const std::vector<std::vector<function_t>>& arFuncs
   )  const
{
   if (arCoefs.size() != arFuncs.size())
   {
      MIDASERROR  (  "Number of coefficients (which is "+std::to_string(arCoefs.size())+") "+
                     "differs from number of product functions "+
                     "(which is "+std::to_string(arFuncs.size())+")."
                  );
   }

   if (arFuncs.empty())
   {
      return 0;
   }
   else
   {
      Uin num_modes = arFuncs.front().size();
      for(Uin i = I_0; i < arFuncs.size(); ++i)
      {
         if (num_modes != arFuncs[i].size())
         {
            MIDASERROR  (  "Number of functions for term "+std::to_string(i)+" "+
                           "(which is "+std::to_string(arFuncs[i].size())+") "+
                           "differs from that of the first term "+
                           "(which is "+std::to_string(num_modes)+")."
                        );
         }
      }
      return num_modes;
   }
}


} /*namespace integrals*/
} /*namespace pes*/
} /*namespace midas*/

