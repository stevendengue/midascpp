/**
 *******************************************************************************
 * 
 * @file    ModeCombiIntegrals_Impl.h
 * @date    19-03-2018
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MODECOMBIINTEGRALS_IMPL_H_INCLUDED
#define MODECOMBIINTEGRALS_IMPL_H_INCLUDED

#include <sstream>

namespace midas{
namespace pes{
namespace integrals{

/***************************************************************************//**
 * @note
 *    Throws a MIDASERROR if the number of modes in (i.e. if the size of)
 *    either aIntervalsPerMode or arScalingFactors is not equal to the number
 *    of modes for the terms found by the SumOverProductIndexer.
 *
 * @param[in] aSopIndexer
 *    The object containing the sum-over-product function.
 * @param[in] aIntervalsPerMode
 *    One std::vector<std::pair<Nb,Nb>> per mode, one std::pair<Nb,Nb> per
 *    subinterval, where `first` is beginning of interval, `second` is end of
 *    interval.
 * @param[in] aNumQuadPoints
 *    The number of quadrature points used for the underlying Gauss-Legendre
 *    routine.
 * @param[in] arScalingFactors
 *    Scaling factors, one for each mode.
 ******************************************************************************/
template<typename F>
ModeCombiIntegrals<F>::ModeCombiIntegrals
   (  SumOverProductIndexer<function_t> aSopIndexer
   ,  std::vector<std::vector<std::pair<Nb,Nb>>> aIntervalsPerMode
   ,  Uin aNumQuadPoints
   ,  const std::vector<Nb>& arScalingFactors
   )
   :  mSopIndexer(std::move(aSopIndexer))
   ,  mVecOneModeIntegrals
      (  CalculatedIntegralsForAllModes
         (  mSopIndexer
         ,  std::move(aIntervalsPerMode)
         ,  aNumQuadPoints
         ,  arScalingFactors
         )
      )
{
}

/***************************************************************************//**
 * @return
 *    The number of modes (must be identical for all terms).
 ******************************************************************************/
template<typename F>
Uin ModeCombiIntegrals<F>::NumModes
   (
   )  const
{
   return mSopIndexer.NumModes();
}

/***************************************************************************//**
 * @note
 *    Throws a MIDASERROR if the mode number is out of range.
 *
 * @param[in] aMode
 *    The mode of interest.
 * @return
 *    The number of intervals for that mode.
 ******************************************************************************/
template<typename F>
Uin ModeCombiIntegrals<F>::NumIntervals
   (  Uin aMode
   )  const
{
   try
   {
      return mVecOneModeIntegrals.at(aMode).NumIntervals();
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Out-of-range: " + std::string(oor.what()));
      return I_0;
   }
}

/***************************************************************************//**
 * @note
 *    Throws MIDASERROR if the given index vector has wrong size or if any of
 *    its indices are out-of-range w.r.t. the number of intervals for that
 *    mode.
 *
 * @note
 *    Uses a regular summation over all terms, which potentially can cause
 *    significant numerical round-off errors. Consider a stable summation like
 *    pairwise summation or Kahan summation if this becomes an issue.
 *    Okay, this would probably not be the only place we have this issue.
 *
 * @param[in] arIntervalIndices
 *    A vector of indices (one for each mode) for the subintervals of interest.
 * @return
 *    The integral of the total sum-over-product function on the specified
 *    subvolume/grid box.
 ******************************************************************************/
template<typename F>
Nb ModeCombiIntegrals<F>::Integral
   (  const std::vector<Uin>& arIntervalIndices
   )  const
{
   AssertIntervalIndices(arIntervalIndices);
   Nb sum = C_0;
   for(Uin t = I_0; t < mSopIndexer.NumTerms(); ++t)
   {
      sum += Coefficient(t) * IntegralForTerm(arIntervalIndices, t);
   }
   return sum;
}


/***************************************************************************//**
 * @param[in] aTerm
 *    The sum-over-product term of interest.
 * @return
 *    The coefficient of that term.
 ******************************************************************************/
template<typename F>
Nb ModeCombiIntegrals<F>::Coefficient
   (  Uin aTerm
   )  const
{
   return mSopIndexer.Coefficient(aTerm);
}

/***************************************************************************//**
 * The ModeCombiIntegrals::mSopIndexer takes care of which 1-mode function
 * integrals goes into the product for this term.
 *
 * @param[in] arIntervalIndices
 *    Index vector for the integral of interest.
 * @param aTerm
 *    The term of interest.
 * @return
 *    The product of the 1-mode integrals. 1 if the object has 0 modes.
 ******************************************************************************/
template<typename F>
Nb ModeCombiIntegrals<F>::IntegralForTerm
   (  const std::vector<Uin>& arIntervalIndices
   ,  Uin aTerm
   )  const
{
   const auto& f_indices = mSopIndexer.FunctionIndices(aTerm);
   Nb prod = C_1;
   for(Uin m = I_0; m < NumModes(); ++m)
   {
      prod *= mVecOneModeIntegrals[m].Integral(arIntervalIndices[m], f_indices[m]);
   }
   return prod;
}

/***************************************************************************//**
 * @note
 *    Throws a MIDASERROR if the number of modes in (i.e. if the size of)
 *    either aIntervalsPerMode or arScalingFactors is not equal to the number
 *    of modes for the terms found by the SumOverProductIndexer.
 *
 * @param[in] arSopIndexer
 *    Contains the unique 1-mode functions to be integrated for each mode.
 * @param[in] aIntervalsPerMode
 *    The intervals of integration for each mode.
 * @param[in] aNumQuadPoints
 *    The number of quadrature points to be used.
 * @param[in] arScalingFactors
 *    Scaling factors for each mode.
 * @return
 *    Calculated OneModeIntegrals for each mode.
 ******************************************************************************/
template<typename F>
std::vector<OneModeIntegrals<typename ModeCombiIntegrals<F>::function_t>>
ModeCombiIntegrals<F>::CalculatedIntegralsForAllModes
   (  const SumOverProductIndexer<function_t>& arSopIndexer
   ,  std::vector<std::vector<std::pair<Nb,Nb>>> aIntervalsPerMode
   ,  Uin aNumQuadPoints
   ,  const std::vector<Nb>& arScalingFactors
   )  const
{
   if (arSopIndexer.NumModes() != aIntervalsPerMode.size())
   {
      std::stringstream err;
      err   << "Wrong number of modes. "
            << "arSopIndexer.NumModes() (which is " << arSopIndexer.NumModes()
            << ") not equal to aIntervalsPerMode.size() (which is " << aIntervalsPerMode.size()
            << ")."
            ;
      MIDASERROR(err.str());
   }
   if (arSopIndexer.NumModes() != arScalingFactors.size())
   {
      std::stringstream err;
      err   << "Wrong number of modes. "
            << "arSopIndexer.NumModes() (which is " << arSopIndexer.NumModes()
            << ") not equal to arScalingFactors.size() (which is " << arScalingFactors.size()
            << ")."
            ;
      MIDASERROR(err.str());
   }

   std::vector<OneModeIntegrals<function_t>> v_omi;
   v_omi.reserve(arSopIndexer.NumModes());
   for(Uin m = I_0; m < arSopIndexer.NumModes(); ++m)
   {
      v_omi.emplace_back
         (  aNumQuadPoints
         ,  arSopIndexer.Functions(m)
         ,  std::move(aIntervalsPerMode[m])
         ,  arScalingFactors[m]
         );
   }
   return v_omi;
}

/***************************************************************************//**
 * @note
 *    Throws MIDASERROR if the given index vector has wrong size or if any of
 *    its indices are out-of-range w.r.t. the number of intervals for that
 *    mode.
 *
 * @param[in] arIntervalIndices
 *    The index vector.
 ******************************************************************************/
template<typename F>
void ModeCombiIntegrals<F>::AssertIntervalIndices
   (  const std::vector<Uin>& arIntervalIndices
   )  const
{
   if (arIntervalIndices.size() != NumModes())
   {
      std::stringstream err;
      err   << "Wrong number of interval indices. "
            << "arIntervalIndices.size() (which is " << arIntervalIndices.size()
            << ") not equal to NumModes() (which is " << NumModes()
            << ")."
            ;
      MIDASERROR(err.str());
   }
   for(Uin m = I_0; m < arIntervalIndices.size(); ++m)
   {
      const auto& i = arIntervalIndices[m];
      if (i >= mVecOneModeIntegrals[m].NumIntervals())
      {
         std::stringstream err;
         err   << "Bad interval index. "
               << "arIntervalIndices[" << m << "] (which is " << i
               << ") >= mVecOneModeIntegrals[" << m << "].NumIntervals() (which is "
               << mVecOneModeIntegrals[m].NumIntervals()
               << ")."
               ;
         MIDASERROR(err.str());
      }
   }
}


} /*namespace integrals*/
} /*namespace pes*/
} /*namespace midas*/

#endif/*MODECOMBIINTEGRALS_IMPL_H_INCLUDED*/
