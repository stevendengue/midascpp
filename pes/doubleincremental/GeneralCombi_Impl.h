/**
************************************************************************
* 
* @file                GeneralCombi_Impl.h
*
* Created:             13-07-2015
*
* Author:              Carolin Konig (ckonig@chem.au.dk)
*
* Short Description:   Implement General combin range class members 
* 
* Last modified:                 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

template<class T>
bool GeneralCombi<T>::Contains(const GeneralCombi<T>& arCombi) const
{
   return std::includes(mEntries.begin(), mEntries.end(),
          arCombi.mEntries.begin(), arCombi.mEntries.end());
}
