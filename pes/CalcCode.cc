#include "CalcCode.h"

using Sst = string::size_type;

/**
 * Constructor from modevector and grid vector.
 *
 * @param aModeVec  The mode vector.
 * @param aGridVec  The grid vector.
 **/
CalcCode::CalcCode
   (  const std::vector<In>& aModeVec
   ,  const std::vector<SimpleRational> aGridVec
   )
   :  mModes(aModeVec)
   ,  mGridPoints(aGridVec) 
{
   if(mModes.size() != mGridPoints.size()) 
   {
      MIDASERROR("Size of mode vector and GP vector should be equal");
   }
}

/**
 * Constructor from two vectors of ints, 
 * making things a bit simpler when the grid is defined as ... 1/1 0/1 -1/1 ...
 *
 * @param aModeVec  The mode vector.
 * @param aGridVec  Grid vector as vector of integers.
 **/
CalcCode::CalcCode(const std::vector<In>& aModeVec, const std::vector<In> aGridVec) 
   : mModes(aModeVec)
   , mGridPoints(aGridVec.begin(), aGridVec.end())
{
   if(mModes.size() != mGridPoints.size()) 
   {
      MIDASERROR("Size of mode vector and GP vector should be equal");
   }
}

/**
 * Constructor from calculation code string.
 * 
 * @param aCalcCodeStr   String defining calculation code.
 **/
CalcCode::CalcCode
   (  const string& aCalcCodeStr
   )
{
   if(aCalcCodeStr != "*#0*" && aCalcCodeStr != "XXX")
   {
      std::vector<string> temp_store;
      typedef string::size_type Sst;
      //Mout << " string in input to GetInfoFromString: " << aCalcCode << endl;
      string s_inf = aCalcCodeStr;
      //erase *
      Sst sfind = s_inf.find("*");
      s_inf.erase(sfind, I_1);
      sfind = s_inf.find("*");
      s_inf.erase(sfind,I_1);
      //special case for reference structure
      while (s_inf.size()>0)
      {
         string piece;
         Sst sfind=s_inf.find("#");
         Sst efind=s_inf.find("#",sfind+1);
         //if (sfind==s_inf.npos) 
            //Mout << " end reached " << endl;
         piece=s_inf.substr(sfind, efind-sfind);
         s_inf.erase(sfind,efind);
         //Mout << " piece: " << piece << endl;
         if (piece.find("#")!=piece.npos) //always
         {
            Sst cfind=piece.find("_");
            string mode=piece.substr(piece.find("#"),cfind);
            mode.erase(I_0,I_1);
            string k_val=piece.substr(cfind);
            k_val.erase(I_0,I_1);
            //Mout << " mode: " << mode << " k_val: " << k_val << endl;
            mModes.push_back(InFromString(mode)-1);
            temp_store.push_back(k_val);
         }
      }
      for(In i=I_0; i < temp_store.size(); ++i)
         mGridPoints.emplace_back(temp_store[i]);
      if(mGridPoints.size() != mModes.size())
         MIDASERROR("Size of mode vector and GP vector should be equal");
   }
}

/**
 *
 **/
CalcCode& CalcCode::operator=
   (  const CalcCode& aRHS
   )
{
   if(this != &aRHS){
      mModes = aRHS.mModes;
      mGridPoints = aRHS.mGridPoints;
   }
   return *this;
}

/**
 *
 **/
std::string CalcCode::GetString
   (
   )  const
{
   std::stringstream ret;
   ret << "*";
   
   if (Size() == I_0)
   {
      ret << "#0";
   }
   else
   {
      for (In i = I_0; i < mModes.size(); ++i)
      {
         ret << "#" << mModes[i] + I_1 << "_" << mGridPoints[i];
      }
   }
   ret << "*";
   
   return ret.str();
}

/**
 *
 **/
std::vector<SimpleRational> CalcCode::GetDispVec
   (
   ) const 
{
   return mGridPoints;
}

/**
 *
 **/
std::vector<In> CalcCode::GetModeVec
   (
   ) const 
{
   return mModes;
}

/**
 *
 **/
In CalcCode::Size
   (
   ) const 
{
   return mModes.size();
}

/**
 *
 **/
In CalcCode::GetModeI
   (  In aIn
   )  const 
{
   return mModes[aIn];
}

/**
 *
 **/
Nb CalcCode::GetDispI
   (  In aIn
   )  const 
{
   return mGridPoints[aIn];
}

/**
 *
 **/
std::ostream& operator<<
   (  std::ostream& as
   ,  const CalcCode& ar
   )
{
   as << ar.GetString();
   return as;
}

/**
 *
 **/
bool operator==
   (  const CalcCode& lhs
   ,  const CalcCode& rhs
   )
{
   if(lhs.Size() != rhs.Size())
      return false;
   if(lhs < rhs)
      return false;
   if(rhs < lhs)
      return false;
   return true;
}

/**
 *
 **/
bool operator!=
   (  const CalcCode& lhs
   ,  const CalcCode& rhs
   )
{
   return !(lhs==rhs);
}

/**
 *
 **/
bool operator<
   (  const CalcCode& arLHS
   ,  const CalcCode& arRHS
   )
{
   //Ordering 1. Low MCs before high MCs
   if(arLHS.Size() < arRHS.Size())
      return true;
   else if(arLHS.Size() > arRHS.Size())
      return false;
   //Ordering 2. Low MC# before high MC#
   for(int i = 0; i < arLHS.Size();++i)
      if(arLHS.mModes[i] < arRHS.mModes[i])
         return true;
      else if(arLHS.mModes[i] > arRHS.mModes[i])
         return false;
   //Ordering 3. order after displacements in MC order
   //Note different order in the two function calls, checking arLHS < arRHS and then arRHS < arLHS
   for(In i = 0; i < arLHS.Size();++i)
      if(arLHS.mGridPoints[i] < arRHS.mGridPoints[i])
         return true;
      else if(arLHS.mGridPoints[i] > arRHS.mGridPoints[i])
         return false;
   //If nothing has happened so far we are just going to return false
   //This should ensure that if two CalcCodes are identical they will
   //fullfil arLHS < arRHS and arLHS > arRHS which is what std::set/std::map(keys) 
   //uses to determine equality between two elements.
   return false;   
}

/**
 * Set code from string.
 **/
void SetCodeFromString
   (  std::string& aCalcCode
   ,  const std::string& aFullCode
   )
{
   //Mout << " string in input to SetCalcCode: " << aFullCode << endl;
   string s_inf=aFullCode;
   string code="";
   //erase *
   Sst sfind=s_inf.find("*");
   s_inf.erase(sfind,I_1);
   sfind=s_inf.find("*");
   s_inf.erase(sfind,I_1);
   vector<string> m_vec_s;
   vector<string> k_vec_s;
   //special case for reference structure
   while (s_inf.size()>0)
   {
      string piece;
      Sst sfind=s_inf.find("#");
      Sst efind=s_inf.find("#",sfind+1);
      //if (sfind==s_inf.npos) 
         //Mout << " end reached " << endl;
      piece=s_inf.substr(sfind, efind-sfind);
      s_inf.erase(sfind,efind);
      //Mout << " piece: " << piece << endl;
      if (piece.find("#")!=piece.npos) //always
      {
         Sst cfind=piece.find("_");
         string mode=piece.substr(piece.find("#"),cfind);
         mode.erase(I_0,I_1);
         m_vec_s.push_back(mode);
         string k_val=piece.substr(cfind);
         k_val.erase(I_0,I_1);
         //Mout << " mode: " << mode << " k_val: " << k_val << endl;
         k_vec_s.push_back(k_val);
      }
   }
   //Mout << " m_vec_s: " << endl;
   //for (vector<string>::iterator Vsi=m_vec_s.begin(); Vsi!=m_vec_s.end(); Vsi++)
      //Mout << *Vsi << "  ";
   //Mout << endl;
   //Mout << " k_vec_s: " << endl;
   //for (vector<string>::iterator Vsi=k_vec_s.begin(); Vsi!=k_vec_s.end(); Vsi++)
      //Mout << *Vsi << "  ";
   //Mout << endl;
   if (m_vec_s.size()!=k_vec_s.size()) MIDASERROR(" Size mismatch ");
   for (In i=I_0; i<m_vec_s.size(); i++)
   {
      Sst bfind=k_vec_s[i].find("/");
      In nb=InFromString(k_vec_s[i].substr(I_0,bfind));
      if (nb!=I_0)
         code+="#"+m_vec_s[i]+"_"+k_vec_s[i];
   }
   if (code=="") code="#0";
   aCalcCode="*"+code+"*";
   //Mout << " Stripped code: " << aCalcCode << endl;
   //Mout << " infos from string: " << endl;
   //Mout << " mode_vec: " << arModeCombi << endl;
   //Mout << " k vec:    " << arKvec << endl;
}

/**
* Get info from string 
* */
void GetInfoFromString
   (  const std::string& aCalcCode
   ,  std::vector<In>& arModeCombi
   ,  std::vector<std::string>& arKvec
   )
{
   if(aCalcCode == "*#0*")
      return;
   typedef string::size_type Sst;
   string s_inf=aCalcCode;
   //erase *
   Sst sfind=s_inf.find("*");
   s_inf.erase(sfind,I_1);
   sfind=s_inf.find("*");
   s_inf.erase(sfind,I_1);
   //special case for reference structure
   while (s_inf.size()>0)
   {
      string piece;
      Sst sfind=s_inf.find("#");
      Sst efind=s_inf.find("#",sfind+1);
      //if (sfind==s_inf.npos) 
         //Mout << " end reached " << endl;
      piece=s_inf.substr(sfind, efind-sfind);
      s_inf.erase(sfind,efind);
      //Mout << " piece: " << piece << endl;
      if (piece.find("#")!=piece.npos) //always
      {
         Sst cfind=piece.find("_");
         string mode=piece.substr(piece.find("#"),cfind);
         mode.erase(I_0,I_1);
         string k_val=piece.substr(cfind);
         k_val.erase(I_0,I_1);
         //Mout << " mode: " << mode << " k_val: " << k_val << endl;
         arModeCombi.push_back(InFromString(mode)-1);
         arKvec.push_back(k_val);
      }
   }
   //Mout << " infos from string: " << endl;
   //Mout << " mode_vec: " << arModeCombi << endl;
   //Mout << " k vec:    " << arKvec << endl;
}
