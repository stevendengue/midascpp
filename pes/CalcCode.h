/**
************************************************************************
*
*  @file                CalcCode.h
* 
*  Created:             11/06/2013
* 
*  Author:              B. Thomsen(bothomsen@chem.au.dk)
* 
*  Short Description:   Declares and implements CalcCode, which is a wrapper
*                        for storing the coordinate information for a given point 
* 
*  Last modified: Tue 11th Jun 2013
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

#ifndef CALCEXTRAVAGANZACODE_H_INCLUDED
#define CALCEXTRAVAGANZACODE_H_INCLUDED

// std headers
#include <string>
#include <vector>
#include <ostream>
#include <sstream>
#include <math.h>

// midas headers
#include "pes/SimpleRational.h"
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"

/**
 * Calculation code for a singlepoint.
 **/
class CalcCode
{
   private:

      //! Mode to be displaced for singlepoint
      std::vector<In>                mModes;

      //! The displacement for each mode
      std::vector<SimpleRational>    mGridPoints;

      //! We do not allow default construction
      CalcCode() = delete;

   public:

      //! Constructor
      CalcCode(const std::vector<In>& aModeVec, const std::vector<SimpleRational> aGridVec);
      
      //! Constructor from two vectors of ints, making things a bit simpler when the grid is defined as ... 1/1 0/1 -1/1 ...
      CalcCode(const std::vector<In>& aModeVec, const std::vector<In> aGridVec);
      
      //! Ctor from string
      CalcCode(const string& aCalcCodeStr);

      //! Copy constructor
      CalcCode& operator=(const CalcCode& aRHS);
         
      //! Get calculation code as string.
      std::string GetString() const;
      
      //! Get displacements
      std::vector<SimpleRational> GetDispVec() const;

      //! Get Modes
      std::vector<In> GetModeVec() const;
      
      //! Get size of mode combi
      In Size() const;
      
      //! Get specific mode
      In GetModeI(In aIn) const;
      
      //! Get specific displacement
      Nb GetDispI(In aIn) const;

      //! Ouput operator
      friend std::ostream& operator<<(std::ostream& aOs, const CalcCode& arCode);
   
      //! Equal operator
      friend bool operator==(const CalcCode& arLhs, const CalcCode& arRhs);
      
      //! Not equal operator
      friend bool operator!=(const CalcCode& arLhs, const CalcCode& arRhs);
   
      //! Less than operator
      friend bool operator< (const CalcCode& arLhs, const CalcCode& arRhs);
};

//!
void SetCodeFromString
   (  std::string& aCalcCode
   ,  const std::string& aFullCode
   );

//!
void GetInfoFromString
   (  const std::string& aCalcCode
   ,  std::vector<In>& arModeCombi
   ,  std::vector<std::string>& arKvec
   );

#endif /* CALCEXTRAVAGANZACODE_H_INCLUDED */
