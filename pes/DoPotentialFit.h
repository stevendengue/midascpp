/**
************************************************************************
*
* @file                DoPotentialFit.h
*
* Created:             11-10-2012
*
* Authors:             Bo Thomsen (bothomsen@chem.au.dk), original implementation
*                      Emil Lund Klinting (klint@chem.au.dk), separation of functionalities
*
* Short Description:   Driver for fitting routines and interpolation methods, resulting in the creation of .mop files
*
* Last modified: Thu Jul 16, 2018
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

#ifndef DOPOTENTIALFIT_H_INCLUDED
#define DOPOTENTIALFIT_H_INCLUDED

// std headers
#include <map> 
#include <vector>
#include <string>
#include <algorithm>

// midas headers
#include "concurrency/Info.h"
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "pes/PesInfo.h"
#include "pes/DoFitting.h"
#include "pes/Derivatives.h"
#include "pes/DumpPotential.h"
#include "pes/ItGridHandler.h"
#include "pes/CalculationList.h"
#include "pes/DoInterpolation.h"
#include "util/Io.h"
#include "util/FileHandler.h"
#include "util/BarInterface.h"
#include "input/Input.h"
#include "input/PesCalcDef.h"
#include "input/FitBasisCalcDef.h"
#include "input/ModeCombiOpRange.h"
#include "pes/fitting/BaseFitBasisSet.h"
#include "pes/fitting/PolyFitBasisSet.h"
#include "pes/fitting/GeneralFitBasisSet.h"
#include "pes/fitting/GeneralOptFitBasisSet.h"

// using declarations
using namespace BarHandles;

/**
 *
 **/
class DoPotentialFit
{
   private:

      //! FitBasisCalcDef reference 
      const FitBasisCalcDef&  mFitBasisCalcDef;

      //! PesCalcDef reference 
      const PesCalcDef&       mPesCalcDef;

      //! PesInfo
      const PesInfo&          mPesInfo;
      
      //! Container for the fit-basis for all modes
      FitFunctions            mFitFunctions;
      
      //! Controls whether or not a fit-basis should be constructed
      bool                    mDoConstructFitBasis;
      
      //! Keep track of fit functions orders in the automatic procedure
      mutable std::vector<In> mAutoGenNoFuncs;

      //! Map of operators for all calculated properties as obtained in the fitting routine. Format is a key consisiting of 'property number, mode combination address, basis function order(s)' and the property term consisting of 'property term coefficient, operator(s), mode(s)' 
      std::map<std::tuple<In, In, std::vector<In> >, std::tuple<Nb, std::vector<std::string>, std::vector<std::string> > > mOperatorTerms;

      //! Investigated if inertia moments are meant to be interpolated or fitted 
      std::pair<In, In> GetInertiaProps() const;

      //! Plot the raw single point data 
      void PlotNonFittedPoints(const DumpPotential& aDumpPotential, const BarFileInterface& aBarFileInterface, const ModeCombiOpRange& aModeCombiRange, const std::vector<std::vector<Nb>>& aOneModeGridBounds, const bool& aUseDer, const Derivatives& aDerivatives) const;

      //! Construct the fit-basis for all modes
      void ConstructFitBasis(const ModeCombiOpRange& arModeCombiRange, const BarFileInterface& aBarFileInterface);

      //! Automatic one-mode fit-basis determination
      void AutoOneModeFitBasis(const BarFileInterface& aBarFileInterface, const ModeCombi& aModeCombi, const FunctionContainer<Nb>& aFuncCont, const FunctionContainer<taylor<Nb,1,1> >& aDerFuncCont);

      //! Function which takes care of interpolation and/or fitting for individual mode combinations and properties
      void DoInterpFit(DumpPotential& aDumpPotential, const BarFileInterface& aBarFileInterface, const CalculationList& aCalculationList, const ItGridHandler* aIterGrid, const ModeCombi& aModeCombi, const In& aBarNumber, const GridType& aGridPointFractions, const std::string aPropertyName, const std::pair<In, In>& aInertiaProps, std::map<std::tuple<In, In, std::vector<In> >, std::tuple<Nb, std::vector<std::string>, std::vector<std::string> > >& aOperatorTerms, const Derivatives& aDerivatives, const bool& aAvailableDerivatives) const;

      //! Used to find out if a fine grid should be constructed or not..
      bool GridGen(const ModeCombi&, bool) const; 

      //! Generates the fine grids used for interpolation in the NonAdga case
      std::vector<MidasVector> GenNonAdgaFG(const ModeCombi&, const std::vector<MidasVector>&, std::vector<MidasVector>&) const;

      //! Generates the fine grids used for Shepard interpolation in the Adga case
      std::vector<MidasVector> GenAdgaFG(const ModeCombi&, const std::vector<MidasVector>&, std::vector<MidasVector>&) const;

      //!
      void OutputXiGCoord(const ModeCombi&, const std::vector<MidasVector>&) const;
      
      //! Do we only do fitting or are we also doing other things?   
      bool OnlyFit(const ModeCombi&, bool) const; 

   public:
      
      //! Constructor
      DoPotentialFit(const FitBasisCalcDef& aFitBasisCalcDef, const PesCalcDef& aPesCalcDef, const PesInfo& aPesInfo);
      
      // Delete default constructor
      DoPotentialFit() = delete;
      
      // Delete copy constructor
      DoPotentialFit(const DoPotentialFit&) = delete;
      
      // Enable move constructor
      DoPotentialFit(DoPotentialFit&&) = default;
      
      //! Delete default copy assignment
      DoPotentialFit& operator=(const DoPotentialFit&) = delete;
      
      //! Enable default move assignment
      DoPotentialFit& operator=(DoPotentialFit&&) = default;

      //! Main function, runs over mode-combinations and properties to perform interpolation and/or fitting
      void GetAnalyticalSurfaces(const ModeCombiOpRange&, const GridType&, const Derivatives&, const bool&, const std::vector<std::vector<Nb>>&, const ItGridHandler*, const CalculationList&, const In&, const std::vector<bool>&);
      
      //! Clean out the map of operators 
      void CleanOperatorMap() 
      {
         mOperatorTerms.clear();
      }
};

#endif /* DOPOTENTIALFIT_H_INCLUDED */
