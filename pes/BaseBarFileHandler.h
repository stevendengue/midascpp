/**
************************************************************************
* 
* @file                BaseBarFileHandler.h
*
* Created:             09-10-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   
* 
* Last modified: Thu Oct 09, 2012  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef BASEBARFILEHANDLER_H
#define BASEBARFILEHANDLER_H

//Forward declaring everything since this class is just a wrapper
// std headers
#include <vector>

// midas headers
#include "util/Io.h"
#include "inc_gen/TypeDefs.h"
#include "pes/CalculationList.h"

// using declarations
using std::vector;

// forward decalrations
class ModeCombiOpRange;
class GridType;
class Derivatives;
class ItGridHandler;
class PesInfo;

/**
 *
 **/
class BaseBarFileHandler
{
   protected:
      //! Reference to pes info for calculation
      PesInfo& mPesInfo; 
      
      //! Constructor
      BaseBarFileHandler(PesInfo& arPesInfo) 
         : mPesInfo(arPesInfo) 
      {
      }

   public:
      //! Virtual destructor
      virtual ~BaseBarFileHandler() 
      {
      }
      
      /**
       * virtual interface
       **/
      virtual void DoDispDer_zero(const ModeCombiOpRange&,In,const GridType&, CalculationList&) = 0;
      virtual void DoDispDer_one(const ModeCombiOpRange&,In,const GridType&, const Derivatives&) = 0;
      virtual void SetmIterGrid(ItGridHandler* aIterGrid) { }
      virtual void Extrapolation(const Derivatives&, const CalculationList&, const std::vector<std::string>&) = 0;
};

#endif /* BASEBARFILEHANDLER_H */
