/**
************************************************************************
* 
* @file                Surface.h
*
* Created:             10-08-2005
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Facotry for the individual surface classes(Adga/NonAdga)
* 
* Last modified: Thu Jul 15, 2010  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef SURFACE_H_INCLUDED
#define SURFACE_H_INCLUDED

#include <vector>
#include <string>
#include <set>

#include "inc_gen/TypeDefs.h"
#include "util/conversions/TupleFromString.h"
#include "util/conversions/VectorFromString.h"
#include "pes/DoPotentialFit.h"
#include "pes/molecule/MoleculeUtility.h"

class PesInfo;
class Molecule;
class Derivatives;
class GridType;
class BaseBarFileHandler;

namespace midas
{
namespace pes
{

/**
 * Base class for a potential energy or molecular property surface.
 **/
class Surface
{
   protected:

      //! PesInfo
      std::vector<PesInfo>&                        mPesInfos;

      //! Path to the main directory for calculation using pes-module
      const std::string&                           mPesMainDir;

      //! CalculationList, responsible for calculating singlepoints.
      std::vector<CalculationList>                 mCalculationLists;

      //! Determines how the bar-files should be treated
      std::vector<BaseBarFileHandler*>             mBarFiles;

      //! Controls the fitting routine
      std::vector<DoPotentialFit>                  mDoPotentialFits;

      //! The number of subsystem for which surfaces should be constructed
      const In                                     mNoSubsystems = I_0;
      
      //!
      void GridSetup(const PesInfo&, const In&, ModeCombiOpRange&, In&, GridType&);
      
      //!
      void MakeCutAna(const PesInfo& aPesInfo) const;
      
      //! Finalize the surface calculation, e.g. by merging operator files
      void FinalizeMultilevelSurface() const;
      
      //!
      void MergeListofPlots(const PesInfo& aPesInfo) const;
      
      //!
      void FillDerContainer(Derivatives&, const CalculationList&, const PesInfo&) const;
      
      //! Checks that the potential boundaries are the same for all multilevel calculations
      void CheckMultiLevelPesBounds(const PesInfo& aPesInfo) const;

      //! Merge the prop_ref_values.mpesinfo file(s) and copy this to the FinalSurfaces directory
      void MergeReferenceValues(const In& aNoMultilevels, const PesInfo& aPesInfo) const;

      //! Calculate Coriolis coupling matrices and output to file
      void CalcCoriolisMatrices() const;
      
      //! Calculate moment of inertia tensor and output to file
      void CalcMomentOfInertiaTensor() const;

   public:

      //! Constructor
      Surface(std::vector<PesInfo>&, const std::string& aPesMainDir);

      //! Virtual destructor.
      virtual ~Surface() = 0;
      
      //! Surface factory interface
      static Surface* Factory
         (  const bool& aIsAdga
         ,  std::vector<PesInfo>& aPesInfos
         ,  const std::string& aPesMainDir
         );
      
      //! Run the surface
      virtual void RunSurface() 
      { 
         MIDASERROR("Runs the wrong RunSurface function"); 
      }
};

} /* namespace pes */
} /* namespace midas */

#endif /* SURFACE_H_INCLUDED */
