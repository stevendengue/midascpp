/**
************************************************************************
* 
* @file                DisplacementGenerator.h
*
* Created:             06-06-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: 
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDAS_PES_DISPLACEMENTGENERATOR_H_INCLUDED
#define MIDAS_PES_DISPLACEMENTGENERATOR_H_INCLUDED

// std headers
#include <map>
#include <vector>
#include <string>

// midas headers
#include "util/Io.h"
#include "util/Os.h"
#include "util/paral/run_process.h"
#include "util/MidasSystemCaller.h"
#include "input/PesCalcDef.h"
#include "pes/molecule/MoleculeInfo.h"
#include "pes/molecule/MoleculeFile.h"

// forward declarations
class PesInfo;
class CalcCode;

namespace midas
{
namespace pes
{

/**
 * Helper class for generating displaced molecular structures from calculation codes.
 **/
class DisplacementGenerator
{
   private:

      //! Reference to PesCalcDef
      const PesCalcDef& mPesCalcDef;

      //! Reference to PesInfo
      const PesInfo& mPesInfo;

      //! Reference to molecule
      const molecule::MoleculeInfo& mMolecule;
      
   public:

      //! Constructor
      DisplacementGenerator(const PesCalcDef& aPesCalcDef, const PesInfo& aPesInfo, const molecule::MoleculeInfo& aMolecule);
      
      //!@{
      //! Deleted default c-tor, defaulted move and copy c-tors
      DisplacementGenerator() = delete;
      DisplacementGenerator(const DisplacementGenerator&) = default;
      DisplacementGenerator(DisplacementGenerator&&) = default;
      //!@}
      
      //! Destructor
      ~DisplacementGenerator() = default; 

      //! Create a displaced single point structure
      void GenerateDisplacedStructure(const In&, const CalcCode&, std::vector<Nuclei>&, std::map<std::string, Nb>&) const;

};

} /* namespace pes */
} /* namespace midas */

#endif /* MIDAS_PES_DISPLACEMENTGENERATOR_H_INCLUDED */
