/**
************************************************************************
*
* @file                Derivatives.h
*
* Created:             17-07-2009
*
* Author:              Manuel Sparta (mspartachem.au.dk)
*
* Short Description:   Declares class for handling the derivative informations
*                      
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef DERIVATIVES_H_INCLUDED
#define DERIVATIVES_H_INCLUDED

// Standard Headers
#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

// My Headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "util/Io.h"

/**
 * Struct to handle storage of derivatives.
 **/
class Derivatives
{
   private:

      //! Vector of the properties that have available derivatives info
      std::vector<std::string>         mProp;

      //! Vector with the order of the derivatives
      std::vector<In>                  mOrder;

      //! Vector of the vector containg the derivatives  
      std::vector<std::vector<Nb> >    mVecDerCont;   

      //! Vector of the maps containg the offset to handle the derivative vectors
      std::vector<std::map<In, In> >   mVecDerContHandler;

   public:

      //! Default constructor
      Derivatives();
      
      //! Enable move constructor
      Derivatives(Derivatives&&) = default;
      
      //! Delete default copy assignment
      Derivatives& operator=(const Derivatives&) = delete;
      
      //! Enable default move assignment
      Derivatives& operator=(Derivatives&&) = default;
      
      //! check if a prop has available derivatives
      bool AvailableDer(const std::string& aProp) const;     
      
      //! Save the derivatives for a property
      void SetDerivatives(const std::string& aProp, const In& aOrder, const std::vector<Nb>& aDerCont, const std::map<In, In>& aDerContHand);
      
      //! For a given property return the derivatives container and handler
      void GetDerivatives(const std::string& aProp, std::vector<Nb>& aDerCont, std::map<In, In>& aDerContHand) const;
      
      //! return the order of the available derivatives for a property
      In OrderDer(const std::string& aProp) const;

      //! Output operator for debug
      friend std::ostream& operator<<(std::ostream& aOs, const Derivatives& arDerivatives);
};

#endif /* DERIVATIVES_H_INCLUDED */

