/**
************************************************************************
*
*  @file                SimpleRational.h
* 
*  Created:             11/06/2013
* 
*  Author:              B. Thomsen(bothomsen@chem.au.dk)
* 
*  Short Description:   Declares and implements SimpleRational,
*                        which is just a very simple fraction class
*                        containing only the shortened form of the fraction
* 
*  Last modified: Tue 11th Jun 2013
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

#ifndef SIMPLERATIONAL_H_INCLUDED
#define SIMPLERATIONAL_H_INCLUDED

// std headers
#include <iostream>
#include <string>

// midas headers
#include "util/Io.h"
#include "inc_gen/TypeDefs.h"
#include "util/conversions/FromString.h"

class SimpleRational
{
   private:
      In mTop;
      In mBot;

      SimpleRational() = delete;
      
      In GCD(In m,In n)
      {
         if(n==0)
            return m;
         else if(n>m)
            return GCD(n,m);
         else
            return GCD(n,m%n);
      }
   public:
      SimpleRational(const std::string& aString)
      {
         std::string::size_type divide_poss = aString.find("/");
         if(divide_poss != string::npos)
         {
            mBot = midas::util::FromString<In>(aString.substr(divide_poss+1));
            mTop = midas::util::FromString<In>(aString.substr(I_0,divide_poss));
            if(mBot == 0) 
               MIDASERROR("You can't have a fraction with zero denominator");
         }
         else
         {
            mTop = midas::util::FromString<In>(aString);
            mBot = I_1;
         }
         In gcd = GCD(abs(mTop), abs(mBot));
         mTop = mTop/gcd;
         mBot = mBot/gcd;
      }
   
      SimpleRational(In aTop, In aBot = I_1)
      {
         if(aBot == 0) 
            MIDASERROR("You can't have a fraction with zero denominator");
         In gcd = GCD(abs(aTop), abs(aBot));
         mTop = aTop/gcd;
         mBot = aBot/gcd;
      }
      
      SimpleRational(const SimpleRational& aRat) = default;
      SimpleRational& operator=(const SimpleRational& arRHS) = default;
      
      operator double() const 
      {
         return double(mTop)/double(mBot);
      }
      
      friend bool operator==(const SimpleRational& aLHS, const SimpleRational& aRHS)
      {
         return (aLHS.mTop==aRHS.mTop && aLHS.mBot == aRHS.mBot);
      }

      friend bool operator!=(const SimpleRational& aLHS, const SimpleRational& aRHS)
      {
         return !(aLHS == aRHS);
      }

      friend bool operator<(const SimpleRational& aLHS, const SimpleRational& aRHS)
      {
         return (aLHS.mTop*aRHS.mBot < aRHS.mTop*aLHS.mBot);
      }
      
      friend bool operator>(const SimpleRational& aLHS, const SimpleRational& aRHS)
      {
         return (aLHS.mTop*aRHS.mBot > aRHS.mTop*aLHS.mBot);
      }
      
      friend bool operator<=(const SimpleRational& aLHS, const SimpleRational& aRHS)
      {
         return !(aLHS > aRHS);
      }
      
      friend bool operator>=(const SimpleRational& aLHS, const SimpleRational& aRHS)
      {
         return !(aLHS < aRHS);
      }

      friend std::ostream& operator<<(std::ostream& aOut, const SimpleRational& aSR)
      {
         aOut << aSR.mTop << "/" << aSR.mBot;
         return aOut;
      }

      friend SimpleRational operator*(const SimpleRational& arLHS, const SimpleRational& arRHS)
      {
         return SimpleRational(arLHS.mTop*arRHS.mTop, arLHS.mBot*arRHS.mBot);
      }

      friend SimpleRational operator*(const SimpleRational& arRat, const In& aIn)
      {
         return SimpleRational(arRat.mTop*aIn, arRat.mBot);
      }

      friend SimpleRational operator*(const In& aIn, const SimpleRational& arRat)
      {
         return SimpleRational(arRat.mTop*aIn, arRat.mBot);
      }
};

#endif /* SIMPLERATIONAL_H_INCLUDED */
