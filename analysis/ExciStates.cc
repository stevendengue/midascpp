/**
************************************************************************
* 
* @file                ExciStates.cc
*
* Created:             15-03-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   ExciStates info holder
* 
* Last modified: man mar 21, 2005  10:56
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <vector>
#include <set>
#include <map>
#include <algorithm>
#include <utility>

// My headers:
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "analysis/Analysis.h"
#include "analysis/ExciStates.h"
#include "util/Io.h"
#include "util/Property.h"

// using declarations
using std::vector;
using std::set;
using std::map;
using std::sort;
using std::remove_if;
using std::pair;

/**
* Construct from infor on a property
* */
ExciStates::ExciStates(Property& arP)
{
   mSym = I_0;
   mSpin= I_0;
   mNumber = I_0;
   mModel = "";

   if (arP.Nord() <= I_0) 
   {
      mSym = arP.ExSym();
      mSpin = arP.ExSp();
      mNumber = arP.ExNr();
      mModel = arP.PropertyModel();
   }
}
/**
* overload for <
* */
bool operator<(const ExciStates& ar1,const ExciStates& ar2)
{
   if (ar1.mSym != ar2.mSym ) 
      return (ar1.mSym < ar2.mSym);
   else if (ar1.mSpin != ar2.mSpin ) 
      return (ar1.mSpin < ar2.mSpin);
   else if (ar1.mNumber != ar2.mNumber ) 
      return (ar1.mNumber < ar2.mNumber);
   else 
      return false;
}
/**
* overload for ==
* */
bool operator==(const ExciStates& ar1,const ExciStates& ar2)
{
   if (ar1.mSym == ar2.mSym && ar1.mSpin == ar2.mSpin  && ar1.mNumber == ar2.mNumber ) 
      return true;
   else return false;
}
