#include "VccRestartInfo.h"

#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"

/**
 *
 **/
VccRestartInfo::VccRestartInfo(std::ifstream& input)
{
   std::string str;
   while(std::getline(input, str))
   {
      if(str == "Number of modes:")
      {
         std::getline(input, str);
         mNumModes = midas::util::FromString<In>(str);
      }
      else if(str == "Number of modals per mode:")
      {
         std::getline(input, str);
         auto str_vec = midas::util::StringVectorFromString(str);
         for(auto& s : str_vec)
         {
            mModalsPerMode.emplace_back(midas::util::FromString<In>(s));
         }
      }
      else if(str == "Mode coupling level in VCC:")
      {
         std::getline(input, str);
         mExciLevel = midas::util::FromString<In>(str);
      }
      else if(str == "Size of vectors in VCC:")
      {
         std::getline(input, str);
         mSizeOfVector = midas::util::FromString<In>(str);
      }
   }
}
