/**
************************************************************************
* 
* @file                VscfDrv.cc
*
* Created:             15-03-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Driver for MidasCPP Vscf calculations
 
* Last modified: Mon Jan 22, 2007  08:57AM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef STAT_MECH_H
#define STAT_MECH_H

// Standard headers:
#include <vector>
using std::vector;
#include <string>
using std::string;
#include <algorithm>

// links to headers:
#include "inc_gen/math_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"
#include "util/Io.h"
#include "inc_gen/Warnings.h"
#include "nuclei/Nuclei.h"

class StatMechanics {
   
   private:
      Nb mVibPartFunc;
      Nb mRotPartFunc;
      Nb mTrsPartFunc;
      Nb mTemp;
      vector<Nuclei> mCoordinates;

   public:
      StatMechanics(Nb aT=C_0);
      void RotPartFunc();
      void VibPartFunc();
      void TrsPartFunc();
      Nb GetTrsPartFunc() {return mTrsPartFunc;}
      Nb GetRotPartFunc() {return mRotPartFunc;}
      Nb GetVibPartFunc() {return mVibPartFunc;}
      friend ostream& operator<<(ostream&, StatMechanics&);
};

#endif
