/**
************************************************************************
* 
* @file                PotAnaPlot.h
*
* Created:             27-05-2007
*
* Author:              Daniele Toffoli
*
* Short Description:   Generating Potential Plot and analysis
* 
* Last modified: ???
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef PLOTANA_H
#define PLOTANA_H

#include "inc_gen/TypeDefs.h"

/**
* Main function for generating spectra.
* */
void PotentialAnalysis();

#endif // PLOTANA_H

