/**
************************************************************************
* 
* @file                IRData.h 
*
* Created:             20-9-2006
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   IRData class definition and funtion
*                      declarations.
* 
* ???? Last modified: Mon Oct 30, 2006  09:25AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef IRDATA_H
#define IRDATA_H

#include <string>
using std::string;
#include <iostream>

#include "inc_gen/TypeDefs.h"
#include "operator/PropertyType.h"

//! Class for holding all data needed for generating an IR intensity.
/** All members are in atomic units. */
class IRData
{
   protected:
      In mInitState;        ///< Initial state.
      In mFinalState;       ///< Final state.
      Nb mOmegafi;          ///< E(final) - E(initial)
      
      Nb mMuX;              ///< Dipole moment matrix elements.
      Nb mMuY;
      Nb mMuZ;
      
   public:
      IRData(): mMuX(C_0), mMuY(C_0), mMuZ(C_0) { };
      void SetInitState(const In ai);
      void SetFinalState(const In af);
      void SetOmegafi(const Nb afi);
      void SetMu(const oper::PropertyType aIndex, const Nb aValue);

      In GetInitState() const {return mInitState;}
      In GetFinalState() const {return mFinalState;}
      Nb GetOmegafi() const {return mOmegafi;}
     
      Nb GetMu2() const;            ///< |mu|^2 
      Nb GetOscStrength() const;    ///< Nice dimensionless oscillator strength.
      
      friend std::ostream& operator<<(std::ostream&, const IRData&);
      
};

#endif // IRDATA_H

