/**
************************************************************************
* 
* @file                VscfTemp.cc
*
* Created:             01-05-2009
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Temperatur Vscf funcs 
* 
* Last modified: Thu Jun 11, 2009  05:56PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include<iostream>
#include<set>
#include<string>
using std::set;

// Link to standard headers:
#include "inc_gen/math_link.h" 

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/VscfCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "vscf/Vscf.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "mmv/ItEqSol.h"
#include "ni/OneModeInt.h"
#include "input/RspVecInf.h"
#include "input/TotalRspFunc.h"
#include "util/MultiIndex.h"
#include "VscfRspTransformer.h"
#include "util/Plot.h"
#include "util/MidasStream.h"
#include "vscf/IRData.h"
#include "vscf/RamanData.h"

/**
* Construct population transformed integrals for
* OneState temp. ave.
**/
void Vscf::ConstructTempAve(OneModeInt* aOneModeInt) 
{

   // first 
   In col=(mpVscfCalcDef->GetOneStateMatrix()).Ncols();
   mpVscfCalcDef->OneStateMatrixExtend(I_1);
   for(In i=I_0;i<(mpVscfCalcDef->GetOneStateMatrix()).Nrows();i++) 
   {
      Nb average=C_0;
      for(In j=I_0;j<aOneModeInt->GetmpOpDef()->Nterms();j++) 
      {
         Nb tot_trans_int=C_1;
         for(In k=I_0;k<aOneModeInt->GetmpOpDef()->NfactorsInTerm(j);k++) 
         {
            // calculate a^(m,t)
            Nb trans_int=CalculateTransInt(aOneModeInt,i,j,k);
            // Add value to total
            tot_trans_int*=trans_int;
         }
         tot_trans_int*=aOneModeInt->GetmpOpDef()->Coef(j);
         average+=tot_trans_int;
      }
      if(gDebug || mpVscfCalcDef->IoLevel() > 10)
         Mout << "Average = " << average << endl;
      mpVscfCalcDef->SetOneStateMatrix(i,col,average);
   }
}
/**
* Calculate the a^{m,t} terms
**/
Nb Vscf::CalculateTransInt(OneModeInt* aO, In aTempRow, In aTerm, In aFacInTerm) 
{
   Nb result=C_0;
   LocalModeNr mode=aO->GetmpOpDef()->ModeForFactor(aTerm,aFacInTerm);
   LocalOperNr oper=aO->GetmpOpDef()->OperForFactor(aTerm,aFacInTerm);
   if(gDebug)
      Mout << "Mode = " << mode << " Oper = " << oper << endl;
   // Get the number of basis functions for mode: mode
   In n_bas=mEigVal.Size()-mOccModalOffSet[mpOpDef->NmodesInOp()-mode-I_1]-mOccModalOffSet[mode];
   Nb p_sum=C_0;
   for(In i=I_0;i<n_bas;i++) 
   {                                             // loop over r^m
      In p_col=mOccModalOffSet[mode]+I_1+i;
      Nb p_m_rm=mpVscfCalcDef->GetOccupancy(aTempRow,p_col);
      p_sum+=p_m_rm;
      In offset=ModalAddress(mode,i);
      for(In j=I_0;j<n_bas;j++) 
      {                                     // loop over prim. modals
         Nb coef_j=C_0;
         mModals.DataIo(IO_GET,offset+j,coef_j);
         for(In k=I_0;k<n_bas;k++) 
         {                                     // loop over prim. modals
            Nb coef_k=C_0;
            mModals.DataIo(IO_GET,offset+k,coef_k);
            Nb int_j_k=C_0;
            aO->GetInt(mode,oper,j,k,int_j_k);
            Nb cont=p_m_rm*coef_j*coef_k*int_j_k;
            result+=cont;
         }
      }
   }
   return result;
}
/**
* Construct population transformed integrals for
* OneState temp. ave.
**/
void Vscf::NewConstructTempAve(OneModeInt* aOneModeInt) 
{
   In n_modes=aOneModeInt->GetmpOpDef()->NmodesInOp();
   // we have all property integrals, loop over temp. and make 
   // temp. ave.
   In col=(mpVscfCalcDef->GetOneStateMatrix()).Ncols();
   mpVscfCalcDef->OneStateMatrixExtend(I_1);
   bool virt_expt_first=true;
   for(In i=I_0;i<gTempSteps;i++) 
   {
      // first make population transformed
      // density matrices (one for each mode)
      Nb one_mode_acc=C_0;
      Nb unnorm_acc=C_0;
      vector<Nb> all_a_m_t;
      In int_off=I_0;
      Timer pop_time;
      for (LocalModeNr j = I_0; j < n_modes; j++) 
      {
         In n_bas = I_0;
         if (mpBasDef->GetmUseHoBasis())
         {
            n_bas = mpBasDef->GetmHoQnrs()[j] + I_1;
         }
         MidasMatrix coef_mode_m(n_bas,n_bas,C_0);
         GetTransformedDensity(coef_mode_m,n_bas,j,i);
         // now make \hat{a}^{m,t} terms
         for(LocalOperNr k=I_0;k<aOneModeInt->GetmpOpDef()->NrOneModeOpers(j);k++) 
         {
            MidasMatrix prop_int(n_bas,n_bas);
            aOneModeInt->GetInt(j,k,prop_int);
            Nb a_m_t=coef_mode_m.TraceProduct(prop_int);
            all_a_m_t.push_back(a_m_t);
            int_off++;
         }
      }
      if (mpVscfCalcDef->IoLevel() > I_5) pop_time.CpuOut(Mout,"Time generating hat{a}^{m,t}: ");
      // at this point, sum over terms
      Timer ave_time;
      Nb average=C_0;
      vector<In> one_mode_int_off=aOneModeInt->GetOneModeIntOccOff();
      for(In j=I_0;j<aOneModeInt->GetmpOpDef()->Nterms();j++) 
      {
         Nb cont=C_1;
         for(In k=I_0;k<aOneModeInt->GetmpOpDef()->NfactorsInTerm(j);k++) 
         {
            LocalModeNr mode=aOneModeInt->GetmpOpDef()->ModeForFactor(j,k);
            LocalOperNr oper=aOneModeInt->GetmpOpDef()->OperForFactor(j,k);
            // We have the mode number and oper nr, get \hat{a}^{m,t}
            In i_term=I_0;
            /* commented out by sugg. of Ove
            for(In l=I_0;l<mode;l++)
               i_term+=aOneModeInt->pOpDef()->NrOneModeOpers(l);
            i_term+=oper;
            */
            i_term=one_mode_int_off[mode]+oper;
            cont*=all_a_m_t[i_term];
         }
         average+=cont*aOneModeInt->GetmpOpDef()->Coef(j);
      }
      if (mpVscfCalcDef->IoLevel() > I_5) ave_time.CpuOut(Mout,"SUMMING UP: ");
      mpVscfCalcDef->SetOneStateMatrix(i,col,average);
      // If a calc. of all Virt. Expt. Vals have been requested,
      // do them here
      bool first_run_done=false;
      if((mpVscfCalcDef->AllVirtExptValues() && virt_expt_first) || gDebug) 
      {
         virt_expt_first=false; // only do it once unless DEBUG...
         // make multiindex object...
         vector<In> occmax_vec,occ_vec;
         vector<Nb> all_a_m_t;
         occmax_vec.clear();
         for (In j = I_0; j < n_modes; j++) 
         {
            In n_bas = I_0;
            if (mpBasDef->GetmUseHoBasis())
            {
               n_bas = mpBasDef->GetmHoQnrs()[j] + I_1;
            }
            occmax_vec.push_back(n_bas - I_1);
            occ_vec.push_back(I_0);
         }

         ModeCombiOpRange range(n_modes,occ_vec.size());
         In n_mode_combi = range.Size();
         for (In i_mode_combi=1;i_mode_combi<n_mode_combi;i_mode_combi++)
         {
            const ModeCombi& modes_excited = range.GetModeCombi(i_mode_combi);
            In n_excited = modes_excited.Size();

            vector<In> occ(n_excited);
            vector<In> occmax(n_excited);

            for (In m=0;m<n_excited;m++)
            {
               In mode   = modes_excited.Mode(m);
               occ[m]    = occ_vec[mode];
               occmax[m] = occmax_vec[mode];
            }

            MultiIndex mi_for_modecombi(occ,occmax,"LOWHIG",first_run_done);
            first_run_done=true;

            vector<In> occ2(occ);          // The active part.
            vector<In> occ_vec_new(occ_vec);  // The full occupation vector.

            In n_vscf = mi_for_modecombi.Size();
            for (In i_vscf=0;i_vscf<n_vscf;i_vscf++)
            {
               mi_for_modecombi.IvecForIn(occ2,i_vscf);
               for (In m=0;m<n_excited;m++)
               {
                  In mode   = modes_excited.Mode(m);
                  occ_vec_new[mode] = occ2[m];
               }
               all_a_m_t.clear();
               In int_off=I_0;
               for (LocalModeNr j = I_0; j < n_modes; j++) 
               {
                  In n_bas = I_0;
                  if (mpBasDef->GetmUseHoBasis())
                  {
                     n_bas = mpBasDef->GetmHoQnrs()[j] + I_1;
                  }
                  MidasMatrix coef_mode_m(n_bas,n_bas,C_0);
                  GetTransformedDensity(coef_mode_m,n_bas,j,I_0,true,occ_vec_new[j]);
                  // now make \hat{a}^{m,t} terms
                  for(LocalOperNr k=I_0;k<aOneModeInt->GetmpOpDef()->NrOneModeOpers(j);k++) 
                  {
                     MidasMatrix prop_int(n_bas,n_bas);
                     aOneModeInt->GetInt(j,k,prop_int);
                     Nb a_m_t=coef_mode_m.TraceProduct(prop_int);
                     // old code: Nb a_m_t=coef_mode_m.TraceProduct(all_prop_int[int_off]);
                     all_a_m_t.push_back(a_m_t);
                     int_off++;
                  }
               }
               // at this point, sum over terms
               Nb expt_val=C_0;
               for(In j=I_0;j<aOneModeInt->GetmpOpDef()->Nterms();j++) 
               {
                  Nb cont=C_1;
                  for(In k=I_0;k<aOneModeInt->GetmpOpDef()->NfactorsInTerm(j);k++) 
                  {
                     LocalModeNr mode=aOneModeInt->GetmpOpDef()->ModeForFactor(j,k);
                     LocalOperNr oper=aOneModeInt->GetmpOpDef()->OperForFactor(j,k);
                     // We have the mode number and oper nr, get \hat{a}^{m,t}
                     In i_term=I_0;
                     for(LocalModeNr l=I_0;l<mode;l++)
                        i_term+=aOneModeInt->GetmpOpDef()->NrOneModeOpers(l);
                     i_term+=oper;
                     cont*=all_a_m_t[i_term];
                  }
                  expt_val+=cont*aOneModeInt->GetmpOpDef()->Coef(j);
               }
               /// Always print the expectation value. This is what has been requested at this point.
               Mout << "<" << occ_vec_new << "|" << aOneModeInt->GetmpOpDef()->Name() << "|"
                    << occ_vec_new << "> = " << expt_val << endl;
               // if this is a debug run, produce a file
               // in the analysis dir. similar to the one for ss-VSCF
               if(gDebug) 
               {
                  string prop_no=std::to_string(col-n_modes-I_2);
                  std::string one_mode_file = mpVscfCalcDef->GetmVscfAnalysisDir() + "/one_mode_prop_" + prop_no + "_conv_T_";
                  Nb temp=mpVscfCalcDef->GetOneStateMatrix(i,I_0);
                  one_mode_file+=StringForNb(temp)+".data";
                  // open file
                  midas::mpi::OFileStream one_mode(one_mode_file, midas::mpi::OFileStream::StreamType::MPI_MASTER, std::ios::app);

                  /// \f$ <A>^T=\sum_r A_r\prod_m p^m_rm, get prod of p^m_rm \f$
                  Nb p_m_rm=C_1;
                  for(In l=I_0;l<n_modes;l++) 
                  {
                     In p_col=mOccModalOffSet[l]+I_1+occ_vec_new[l];
                     p_m_rm*=mpVscfCalcDef->GetOccupancy(i,p_col);
                  }
                  Nb contrib=expt_val*p_m_rm;
                  one_mode_acc+=contrib;
                  unnorm_acc+=(contrib*mpVscfCalcDef->GetOneStateMatrix(i,I_1+n_modes));
                  one_mode.setf(ios_base::scientific);
                  midas::stream::ScopedPrecision(8, one_mode);
                  one_mode << occ_vec_new << "  " << expt_val << "  "  << contrib << "  "; 
                  midas::stream::ScopedPrecision(16, one_mode);
                  one_mode << unnorm_acc << "  "
                           << one_mode_acc << endl;
               }
            }
         }
      }
      // end of virt. expt vals.
   }
}
/**
*  compute one-mode density to be used with the iterative grid 
*  calculation.
**/
void Vscf::ThermalDensity(Nb aTemp) 
{
   // first we must construct the Occupancy matrix.
   pVscfCalcDef()->OccupancyResize(1,GetEigVal().Size()+I_1,false);
   if (pVscfCalcDef()->ModalLimitsInput()) 
      MidasWarning("Calculate One Mode Partitioning Function  in Thermal Density only up to the given modal limit");
   vector<Nb> junk=CalcOneModePartFunc(aTemp,0,pVscfCalcDef()->ModalLimitsInput());
   /*
   1) loop over the number of modes
      1.1) read in boundaries and set the number of eval. points
   2) Compute temperature transformed density matrix
   3) contract with primitive modals
   4) write eval. points to file: Iter_MeanDensT_q[n].data, n=0,...,M-1
   */
   In n_modes=mpVscfCalcDef->GetNmodesInOcc();
   // 1)
   for(LocalModeNr i=I_0;i<n_modes;i++) 
   {
      // 1.1)
      MidasVector grid(I_0,C_0);
      if (mpVscfCalcDef->GetmAdgaDensAnalysis())
      {
         //Obs: Defaulted aPropNo to 1
         ReadPotentialFromFile(std::to_string(i), grid, 1);
         MidasWarning("Thermal density not implemented for Multi-state ADGA. Currently only for one ADGA surface.");
      }
      else 
      {
         In n_pts = 2000;
         In n_max=10;
         Nb scale=C_2;
         Mout << " Vscf:ThermalDensity  So far so good 1" << endl; 
         Nb r_max=1; //sqrt(C_2*(Nb(n_max)+C_I_2)/(mpBasDef->GetOmeg(i)))*scale;
         Mout << " Vscf:ThermalDensity  So far so good 2" << endl; 
         string swwar = "Vscf::CalcDensities -  Defining the grid for ADGA-VSCF communication in ugle hard-coded way - I hope you dont see this message ";
         MidasWarning(swwar);

         grid.SetNewSize(n_pts);
         Nb step=C_2*r_max/Nb(n_pts-1);
         for (In i_p=0; i_p<n_pts; i_p++) 
            grid[i_p]=-r_max + step*i_p;
      }
      // no. of eval. points hardwired to 201, i.e. 200 intervals
      // 2)
      GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i);
      LocalModeNr i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
      In nbas       = mpBasDef->Nbas(i_bas_mode);
      MidasMatrix dens_mat(nbas,nbas,C_0);
      GetTransformedDensity(dens_mat,nbas,i);
      // we have transformed density matrix, evaluate
      // the density at the gridpoints
      MidasVector dens_mean(grid.Size(),C_0);
      MidasVector dens_max(grid.Size(),C_0);
      // new
      MidasMatrix bas_vals(grid.Size(),nbas,C_0);
      for(In k=0;k<nbas;k++)
      {
         MidasVector bas_vals_k(grid.Size(),C_0);
         mpBasDef->EvalOneModeBasisFunc(i_bas_mode,k,grid,bas_vals_k);
         bas_vals.AssignCol(bas_vals_k,k);
      }
      for(In k=0;k<nbas;k++) 
      {
         //MidasVector bas_vals_k(grid.Size(),C_0);
         //mpBasDef->EvalOneModeBasisFunc(i_bas_mode,k,grid,bas_vals_k);
         for(In l=0;l<nbas;l++) 
         {
            //MidasVector bas_vals_l(grid.Size(),C_0);
            //mpBasDef->EvalOneModeBasisFunc(i_bas_mode,l,grid,bas_vals_l);
            for(In j=0;j<grid.Size();j++) 
            {
               dens_mean[j]+=bas_vals[j][k]*bas_vals[j][l]*dens_mat[k][l];
            }
         }
      }
      if (mpVscfCalcDef->GetmAdgaDensAnalysis())
      {
         DensPrepareIterative(std::to_string(i), grid, dens_mean, dens_max);
      }
   }
}
/**
*  Make probability transformed density matrix for mode m
**/
void Vscf::GetTransformedDensity(MidasMatrix& aM, In aBas, In aMode, In aTempRow, bool aUseOcc, In aOcc) 
{
   for(In i=I_0;i<aBas;i++) 
   {                                             // loop over r^m
      In p_col=mOccModalOffSet[aMode]+I_1+i;
      Nb p_m_rm=C_0;
      if(aUseOcc) 
      {
         if(i == aOcc)
            p_m_rm=C_1;
      }
      else 
      {
         p_m_rm=mpVscfCalcDef->GetOccupancy(aTempRow,p_col);
      }
      In offset=ModalAddress(aMode,i);
      for(In j=I_0;j<aBas;j++) 
      {                                     // loop over prim. modals
         Nb c_mu=C_0;
         mModals.DataIo(IO_GET,offset+j,c_mu);
         for(In k=I_0;k<aBas;k++) 
         {                                     // loop over prim. modals
            Nb c_nu=C_0;
            mModals.DataIo(IO_GET,offset+k,c_nu);
            aM[j][k]+=p_m_rm*c_mu*c_nu;
         }
      }
   }
   Mout << " In GetTransformedDensity -2 Norm aM " << aM.Norm() << endl; 
}
/**
*  Make probability transformed density matrix for mode m
**/
void Vscf::GetTransformedDensity(MidasMatrix& aM, In aBas, In aMode) 
{
   In thermal_offset=I_0;
   if(mpVscfCalcDef->GetThermalDensityOffsetVec().size()!=I_0) {
      thermal_offset=mpVscfCalcDef->GetThermalDensityOffset(aMode);
   }
   Nb temp=mpVscfCalcDef->GetThermalDensityTemp();
   Nb beta=-C_AUTK/temp;
   // get the offsetted reference energy
   Nb e_low = GetEigVal(aMode,thermal_offset); 

   Nb norm_weight=C_0;
   for(In i=I_0;i<aBas;i++) 
   {                                             // loop over r^m
      // Now get the weigths => get offset vector
      Nb p_m_rm=C_0;
      if(i<thermal_offset) 
      {
         p_m_rm=C_1;
      }
      else 
      {
         Nb e_high = GetEigVal(aMode,i); 
         //if(gDebug)
            Mout << "High = " << e_high << " Low = " << e_low << " high-low = " << e_high-e_low << endl;
         p_m_rm=exp(beta*(e_high-e_low));
      }
      if(gDebug)
         Mout << "For (mode,i) = (" << aMode << "," << i << ")" 
              << ", v = " << p_m_rm << endl;
      norm_weight+=p_m_rm;
      In offset=ModalAddress(aMode,i);
      for(In j=I_0;j<aBas;j++) 
      {                                     // loop over prim. modals
         Nb c_mu=C_0;
         mModals.DataIo(IO_GET,offset+j,c_mu);
         for(In k=I_0;k<aBas;k++) 
         {                                     // loop over prim. modals
            Nb c_nu=C_0;
            mModals.DataIo(IO_GET,offset+k,c_nu);
            aM[j][k]+=p_m_rm*c_mu*c_nu;
         }
      }
   }
   if (gDebug) Mout << "Now I normalize the thermal matrix with: " << C_1/norm_weight << endl;
   aM.Scale(C_1/norm_weight);
   Mout << " In GetTransformedDensity -1 Norm aM " << aM.Norm() << endl; 

}
/**
*  Find max level necessary in thermal calculations 
**/
void Vscf::ThermalLimit(Nb aTemp) 
{
   Mout << "mpVscfCalcDef->ModalLimitsInput() " << mpVscfCalcDef->ModalLimitsInput() << endl;
   MidasAssert((!mpVscfCalcDef->ModalLimitsInput()),"ModalLimits given on input cannot be combined with Thermal limit - I quit."); 
   In n_modes=mpVscfCalcDef->GetNmodesInOcc();
   vector<In> zero_vec(n_modes,I_0); 
   mpVscfCalcDef->SetModalLimits(zero_vec); 
   mpVscfCalcDef->OccupancyZero(); 

   for(LocalModeNr i=I_0;i<n_modes;i++) 
   {
      GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i);
      LocalModeNr i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
      In nbas       = mpBasDef->Nbas(i_bas_mode);
      ThermalLimitForMode(nbas,i_bas_mode,aTemp);  
   } 
   vector<Nb> junk=CalcOneModePartFunc(aTemp,0,true);
}
/**
*  Find max level necessary in thermal calculations for a specific mode 
**/
void Vscf::ThermalLimitForMode(In aBas, In aMode,Nb aTemp) 
{
   In thermal_offset=I_0;
   if(mpVscfCalcDef->GetThermalDensityOffsetVec().size()!=I_0) 
   {
      thermal_offset=mpVscfCalcDef->GetThermalDensityOffset(aMode);
   }
   Nb beta=-C_AUTK/aTemp; 
   // get the offsetted reference energy
   Nb e_low = GetEigVal(aMode,thermal_offset); 
   MidasVector w(aBas); 
   Nb norm_weight=C_0;
   for(In i=I_0;i<aBas;i++) 
   {                                             // loop over r^m
      // Now get the weigths => get offset vector
      Nb p_m_rm=C_0;
      if(i<thermal_offset) 
      {
         p_m_rm=C_1;
      }
      else 
      {
         Nb e_high = GetEigVal(aMode,i); 
         if (gDebug)
            Mout << "High = " << e_high << " Low = " << e_low << " high-low = " << e_high-e_low << endl;
         p_m_rm=exp(beta*(e_high-e_low));
      }
     if(gDebug) 
         Mout << "For (mode,i) = (" << aMode << "," << i << ")" 
               << ", v = " << p_m_rm << endl;
      norm_weight+=p_m_rm;
      w[i]=p_m_rm; 
   }

   Nb thr= mpVscfCalcDef->ThermalOccupThr(); 
   if (thr < C_0 || thr > C_1) 
   {
      Mout << " strange thermal occup threshold reset to zero " << endl; 
      thr = C_0; 
   }
   Mout << " scale factor " << C_1/norm_weight << endl; 
   for (In i=I_0;i<aBas;i++) w[i]/=norm_weight; 
   for (In i=I_0;i<aBas;i++) Mout << "For (mode,i) = (" << aMode << "," << i << ")" 
               << ", w = " << w[i] << endl;

   In n_upper=I_0; 
   Nb summone =-C_1+w[0]; 
   Mout << " upper " << n_upper << " " << summone << endl; 
   while (fabs(summone) > thr && n_upper< aBas)  // include state from below until remaining occupation is < thr. 
   {
      n_upper++;
      summone+=w[n_upper];
      Mout << " upper " << n_upper << " " << summone << endl; 
   } 
   Mout << " Threshold for w : " << thr << " actual remainder " << summone << " upper state " << n_upper;
   ++n_upper; 
   Mout  << " #modals " << n_upper << endl; 
   if (n_upper > aBas) Error ( " found to large n_upper in search for ModalLimit "); 
   mpVscfCalcDef->SetModalLimit(aMode,n_upper); 
} 
