/**
************************************************************************
* 
* @file                RamanData.h 
*
* Created:             11-9-2006
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   RamanData class definition and funtion
*                      declarations.
* 
* ???? Last modified: Mon Oct 30, 2006  09:25AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef RAMANDATA_H
#define RAMANDATA_H

#include <string>
#include <iostream>

#include "inc_gen/TypeDefs.h"
#include "operator/PropertyType.h"

//! Class for holding all data needed for generating Raman data.
/** All members are in atomic units. */
class RamanData
{
   protected:
      In mInitState;        ///< Initial state.
      In mFinalState;       ///< Final state.
      Nb mOmegaExt;         ///< External (exciting) frequency.
      Nb mOmegafi;          ///< E(final) - E(initial)
      
      Nb mAlphaXX;          ///< Matrix elements of the polarizability tensor.
      Nb mAlphaYY;
      Nb mAlphaZZ;
      Nb mAlphaXY;
      Nb mAlphaXZ;
      Nb mAlphaYZ;

   public:
      void SetInitState   (const In ai);
      void SetFinalState  (const In af);
      void SetOmegaExt    (const Nb aFreq);
      void SetOmegafi     (const Nb afi);
      void SetAlphaElement(const oper::PropertyType& aIndex, const Nb aValue);

      In GetInitState()  const {return mInitState;}
      In GetFinalState() const {return mFinalState;}
      Nb GetOmegaExt()   const {return mOmegaExt;}
      Nb GetOmegafi()    const {return mOmegafi;}
      
      void GetInvariants(Nb& a2, Nb& gamma2) const; // Get tensor invariants a^2 and gamma^2.
      Nb GetRamanActivity() const; ///< Returns Raman activity (au).
      Nb GetDepolRatio() const;    ///< Returns rho(pi/2, |_i) = I(pi/2, ||, |_) / I(pi/2, |_, |_).
      Nb GetCrossSect() const;     ///< Returns differential cross sect. (pi/2, |_+||, |_) (au).

      friend std::ostream& operator<<(std::ostream&, const RamanData&);
      
//     bool operator==(const RamanData& aCmp) const;
//     bool operator!=(const RamanData& aCmp) const;
};

#endif // RAMANDATA_H

