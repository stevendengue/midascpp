/**
************************************************************************
*
* @file                Vscf.cc
*
* Created:             04-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing Vscf class methods.
*
* Last modified:       08-12-2014 (Carolin Koenig)
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

// Standard headers:
#include<iostream>
#include<complex>

// Link to standard headers:
#include "inc_gen/math_link.h"

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/VscfCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "util/MidasStream.h"
#include "util/UnderlyingType.h"
#include "vscf/Vscf.h"
#include "mmv/Diag.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "ni/OneModeInt.h"
#include "vcc/ModalIntegrals.h"
#include "vcc/ModalIntegralsFuncs.h"
#include "input/GlobalOperatorDefinitions.h"
#include "mpi/Impi.h"
#include "mmv/mmv_omp_pragmas.h"
#include "util/CallStatisticsHandler.h"
#include "lapack_interface/GGEV.h"
#include "lapack_interface/SYGVD.h"

/**
* Default "zero" Constructor
**/
Vscf::Vscf
   (  VscfCalcDef* apCalcDef
   ,  OpDef* apOpDef
   ,  const BasDef* const apBasDef
   ,  OneModeInt* apOneModeInt
   )
   :  mModals()
   ,  mOneModeModalBasisInt(new ModalIntegrals<Nb>())
{
   mConverged                   = false;
   mpOneModeInt                 = apOneModeInt;
   mpVscfCalcDef                = apCalcDef;
   mpOpDef                      = apOpDef;
   //mbh
   mpOpDefOrig                  = apOpDef;
   mpBasDef                     = apBasDef;

   mNiterLocal                  = 0;
   mNiterGlobal                 = 0;
   mConverged                   = false;
   mEthisIt                     = C_0;

   mXmatrix.clear();
   mYmatrix.clear();

   mOccModalOffSet.clear();
   mModalOffSet.clear();
   mOneModeIntOccOff.clear();
   mOccModalOffSet.resize(mpVscfCalcDef->GetNmodesInOcc());
   mModalOffSet.resize(mpVscfCalcDef->GetNmodesInOcc());
   mOneModeIntOccOff.resize(mpVscfCalcDef->GetNmodesInOcc());

   In n_modal_coef              = 0;
   In n_modal_coef_occ          = 0;
   In n_opers                   = 0;
   //Mout << "N modes in occ = " << mpVscfCalcDef->GetNmodesInOcc() << endl;
   for (LocalModeNr i_op_mode=0;i_op_mode<mpVscfCalcDef->GetNmodesInOcc();i_op_mode++)
   {
      mOccModalOffSet[i_op_mode]        = n_modal_coef_occ;
      mModalOffSet[i_op_mode]           = n_modal_coef;
      mOneModeIntOccOff[i_op_mode]      = n_opers;
      auto nbas                         = this->NBas(i_op_mode);
      n_modal_coef_occ                  += nbas;
      n_modal_coef                      += nbas*nbas;
      n_opers                           += mpOpDef->NrOneModeOpers(i_op_mode);
   }
   mNmodalCoef          = n_modal_coef;
   mNoccModalCoef       = n_modal_coef_occ;

   // Set the appropriate sizes for the vectors & init.

   mOccEigVal.SetNewSize(mpVscfCalcDef->GetNmodesInOcc(),false);
   mOccEigVal.Zero();
   mOccModals.SetNewSize(n_modal_coef_occ,false);
   mOccModals.Zero();
   //check if comb. operators are to be calculated "on-the-fly"
   mOneModeIntOcc.SetNewSize(mpOneModeInt->GetmTotNoOneModeOpers());

   mOneModeOperNorms.SetNewSize(mpOneModeInt->GetmTotNoOneModeOpers());
   mFockNorms.SetNewSize(mpVscfCalcDef->GetNmodesInOcc());
   mFockNorms.PutToNb(C_1,mpVscfCalcDef->GetNmodesInOcc());

   // Initialize the datacontainers names
   string name_base = Name();
   mEigVal.NewLabel(name_base+"_EigVal");
   mModals.NewLabel(name_base+"_Modals");

   // First be sure the datacontainers have size zero, so no data is written out on the files
   mEigVal.SetNewSize(I_0);
   mModals.SetNewSize(I_0);

   // when we now ChangeStorage to be on disc.
   // Seidler Oct. 17 2008: Changed to InMem.
   string storage = "InMem";
   mModals.ChangeStorageTo(storage);
   mEigVal.ChangeStorageTo(storage);
   // The old datacontainer is available if there was any,
   // and a new one can be dumped.
   // We wait with setting the appropriate size since the container is
   // not really needed yet.

   mNrspParHalf = I_0;
   mNrspPar     = I_0;

   mOldModalsErr = false;
}

Vscf::~Vscf()
{}

/**
 * Start guess for the solution to the vscf equations
**/
void Vscf::StartGuess()
{
   LOGCALL("calls");
   if (mpVscfCalcDef->IoLevel() > I_4)
   {
      Mout << std::endl << " Creating a start guess for the Vscf equations: " << std::endl;
   }

   // Are we doing a restart VSCF calculation or not?
   bool modals_readin = mpVscfCalcDef->Restart();

   // If doing a restart calculation, then read the previous results
   if (modals_readin)
   {
      std::string name_base = Name(true);
      std::string filename = name_base + "_OccModals";
      Mout << "The filename is " << filename << std::endl;
      modals_readin = GetOccModalsFromFile(filename);
      BackupOldModals();
   }

   std::string diag_method;
   if (mpVscfCalcDef->DiagMeth() == "LAPACK" )
   {
      diag_method = "DSYEVD";
   }
   else if (mpVscfCalcDef->DiagMeth() == "MIDAS")
   {
      diag_method = "MIDAS_JACOBI";
   }
   else if (mpVscfCalcDef->DiagMeth() == "DSYEVD" || mpVscfCalcDef->DiagMeth() == "MIDAS_JACOBI")
   {
      diag_method = mpVscfCalcDef->DiagMeth();
   }
   else
   {
      MidasWarning("Only DSYEVD (LAPACK), or MIDAS_JACOBI (MIDAS) allowed DiagMeth. Override! ");
      diag_method = "DSYEVD";
   }

   // If not doing a restart calculation, then create unit vectors based upon unit occupations of basis functions according to start guess.
   if (!modals_readin)
   {
      // If the primitive basis is a set of harmonic oscillator functions (or we use only state-transfer operators).
      if (  !mpBasDef
         || (  mpBasDef
            && mpBasDef->GetmBasSetType() == "PROD-ALLHO"
            )
         )
      {
         if (!mpVscfCalcDef->AnharmGuess())
         {
            if (mpVscfCalcDef->IoLevel() > I_4)
            {
               Mout << " Harmonic oscillator basis set: Do not use anharmonic Vscf start guess " << std::endl;
            }

            for (In i_op_mode = I_0; i_op_mode < mpVscfCalcDef->GetNmodesInOcc(); i_op_mode++)
            {
               In i_add = OccModalOffSet(i_op_mode) + mpVscfCalcDef->GetOccMode(i_op_mode);
               mOccModals[i_add] = C_1;
            }
         }
         else
         {
            // Diagonalize one-mode hamiltonian
            if (mpVscfCalcDef->IoLevel() > I_4)
            {
               Mout << " Harmonic oscillator basis set: Diagonalize one-mode Hamiltonian to obtain Vscf start guess " << std::endl;
            }

            for (LocalModeNr i_op_mode = I_0; i_op_mode < mpOneModeInt->GetmNoModesInInt(); i_op_mode++)
            {
               auto nbas = this->NBas(i_op_mode);
               MidasMatrix h_m_0(nbas);
               h_m_0.Zero();

               // Build the one mode hamiltonian
               for (In i_act_term = I_0; i_act_term < mpOpDef->NactiveTerms(i_op_mode); i_act_term++)
               {
                  In i_term = mpOpDef->TermActive(i_op_mode,i_act_term);
                  Nb fact = mpOpDef->Coef(i_term);
                  if (mpOpDef->NfactorsInTerm(i_term) > I_1)
                  {
                     continue;
                  }
                  if (std::fabs(fact) < mpVscfCalcDef->ScreenZeroCoef())
                  {
                     continue;
                  }
                  In i_fac = I_0;
                  LocalOperNr i_oper = mpOpDef->OperForFactor(i_term, i_fac);
                  GlobalOperNr g_op_nr = mpOpDef->GetOneModeOperGlobalNr(i_op_mode, i_oper);

                  // Keep just simple (DDQ)^2 operators
                  if (  gOperatorDefs.GetOneModeOper(g_op_nr)->GetmIsCoriolisTerm()
                     || (  gOperatorDefs.GetOneModeOper(g_op_nr)->GetRDer() != I_2
                        && gOperatorDefs.GetOneModeOper(g_op_nr)->GetPow() == I_0
                        )
                     )
                  {
                     continue;
                  }

                  if (mpVscfCalcDef->IoLevel() > I_7)
                  {
                     Mout << "  One-mode operator: " << gOperatorDefs.GetOneModeOperDescription(mpOpDef->GetOneModeOperGlobalNr(i_op_mode, i_oper)) << " has coefficient: " << fact << std::endl;
                  }

                  // Get the integrals and build the one-mode Hamiltonian
                  mpOneModeInt->AddIntTo(h_m_0, i_op_mode, i_oper, fact);
               }
               MidasMatrix eig_vec(nbas);
               MidasVector eig_val(nbas);
               Diag(h_m_0, eig_vec, eig_val, diag_method, true);
               //retrieve the final modals
               //Mout << " 1-mode guess energies " << endl;
               //for (In ie=0; ie<nbas; ie++)
                  //Mout << " e_0(" << ie << ")= " << eig_val[ie] << endl;
               //Mout << endl;
               In i_col = mpVscfCalcDef->GetOccMode(i_op_mode);
               MidasVector c_vec(nbas);
               c_vec.Zero();
               eig_vec.GetCol(c_vec, i_col);
               In i_off = OccModalOffSet(i_op_mode);
               for (In k = I_0; k < nbas; k++)
               {
                  mOccModals[i_off + k] = c_vec[k];
               }
            }
         }
      }
      // If the primitive basis is a set of ditributed Gaussian or B-spline functions, then diagonalize the 1-mode Hamiltonian
      else
      {
         if (mpVscfCalcDef->IoLevel() > I_4)
         {
            Mout << " Non-orthogonal basis set: Diagonalize one-mode Hamiltonian to obtain Vscf start guess " << std::endl;
         }

         for (LocalModeNr i_op_mode = I_0; i_op_mode < mpOneModeInt->GetmNoModesInInt(); ++i_op_mode)
         {
            auto nbas = this->NBas(i_op_mode);
            MidasMatrix h_m_0(nbas);
            MidasMatrix s_m(nbas);
            h_m_0.Zero();
            s_m.Zero();

            if (mpVscfCalcDef->IoLevel() > I_7)
            {
               Mout << " The mode Q" << i_op_mode << " has " << mpOpDef->NactiveTerms(i_op_mode) << " active terms but only the following will be used to construct the VSCF start guess: " << std::endl;
            }

            // Check that the terms is supposed to be active in the current calculation and add the associated integrals to the one-mode Hamiltonian if this is the case
            for (In i_act_term = I_0; i_act_term < mpOpDef->NactiveTerms(i_op_mode); i_act_term++)
            {
               In i_term = mpOpDef->TermActive(i_op_mode, i_act_term);
               Nb fact = mpOpDef->Coef(i_term);

               In i_fac = I_0;
               LocalOperNr i_oper = mpOpDef->OperForFactor(i_term, i_fac);
               GlobalOperNr g_op_nr = mpOpDef->GetOneModeOperGlobalNr(i_op_mode, i_oper);

               // We want to construct the one-mode Hamiltonian and must therefore discard any operator terms where modes are coupled
               if (mpOpDef->NfactorsInTerm(i_term) > I_1)
               {
                  {
                     continue;
                  }
               }

               // Skip the term if the associated coefficient is very small
               if (std::fabs(fact) < mpVscfCalcDef->ScreenZeroCoef())
               {
                  continue;
               }

               // If the one-mode operator belong to the potential energy operator or the (simple/rectilinar) kinetic energy operator then
               if (!gOperatorDefs.GetOneModeOper(g_op_nr)->GetmIsPscKeTerm())
               {
                  // Keep only the (DDQ)^2 operators
                  if (  gOperatorDefs.GetOneModeOper(g_op_nr)->GetmIsCoriolisTerm()
                     || (  gOperatorDefs.GetOneModeOper(g_op_nr)->GetRDer() != I_2
                        && gOperatorDefs.GetOneModeOper(g_op_nr)->GetPow() == I_0
                        )
                     )
                  {
                     continue;
                  }
               }
               // If the one-mode operator belongs to the kinetic energy operator based on polyspherical coordinates
               else
               {
                  if (gOperatorDefs.GetOneModeOper(g_op_nr)->GetmIsPscKeTerm())
                  {
                     continue;
                  }
               }

               if (mpVscfCalcDef->IoLevel() > I_7)
               {
                  Mout << "  One-mode operator: " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " has coefficient: " << fact << std::endl;
               }

               // Get the integrals and build the one-mode Hamiltonian
               mpOneModeInt->AddIntTo(h_m_0, i_op_mode, i_oper, fact);
            }

            // Build the overlap matrix, retrieve needed integrals from unit operator integrals
            LocalOperNr i_op_n = mpOpDef->GetUnitOpForMode(i_op_mode);
            mpOneModeInt->GetInt(i_op_mode, i_op_n, s_m);

            // Diagonalize the problem
            {
               LOGCALL("sygvd");
               // Solve symmetric problem
               auto eig = SYGVD(h_m_0, s_m);

               if (mpVscfCalcDef->IoLevel() > I_14)
               {
                  Mout << " VSCF overlap matrix elements for mode Q" << i_op_mode << ": " << std::endl;
                  for (In i = I_0; i < I_5; ++i)
                  {
                     for (In j = I_0; j < I_5; ++j)
                     {
                        Mout << "  Overlap between basis function nr. " << i << " and basis function nr. " << j << ": " << s_m[i][j] << std::endl;
                     }
                  }
               }

               // Set results
               In i_off = OccModalOffSet(i_op_mode);

               if (mpVscfCalcDef->IoLevel() > I_14)
               {
                  Mout << " VSCF primitive basis function energies for mode Q" << i_op_mode << ": " << std::endl;
               }

               for (In k = I_0; k < nbas; ++k)
               {
                  if (  k < I_5
                     && mpVscfCalcDef->IoLevel() > I_14
                     )
                  {
                     Mout << "  Basis function nr. " << k << ": " << eig._eigenvectors[mpVscfCalcDef->GetOccMode(i_op_mode)][k] << std::endl;
                  }

                  mOccModals[i_off + k] = eig._eigenvectors[mpVscfCalcDef->GetOccMode(i_op_mode)][k];
               }
            }
         }
      }
   }
}

/**
 * Backup the old full modal file in case of restart
**/
void Vscf::BackupOldModals()
{
   Mout << "In BackupOldModals()" << endl;
   //string storage = "InMem";
   //mModals.ChangeStorageTo(storage);
   mModals.SaveUponDecon(true);
   mModals.GetFromExistingOnDisc(mNmodalCoef,mpVscfCalcDef->Name(true) + "_Modals");
   if (!mModals.ChangeStorageTo("InMem", true))
   {
      Mout << "Old modals not found in file " <<  mpVscfCalcDef->Name(true) + "_Modals" << endl;
   //   MidasWarning(" Copying of old modals for restart failed - cannot check the phases of the new modals, may result in errors.  ");
   //   SetOldModalsErr(true);
      return;
   }
   else Mout << "Old modals found in file " <<  mpVscfCalcDef->Name(true) << endl;
   //else SetOldModalsErr(false);

   DataCont old_modals(mModals);
   old_modals.NewLabel(Name()+"_OldModals");
   old_modals.SaveUponDecon(true);
//   old_modals.ChangeStorageTo("InMem", true);

   Mout << "The Old Modals have been saved" << endl;
}

void Vscf::SetOldModalsErr(bool aModalsErr)
{mOldModalsErr = aModalsErr;}

/**
* Return the off set for OccModal for mode aOperMode
* */
In Vscf::ModalAddress(const LocalModeNr aOperMode,const In& aModalNr) const
{
   In add =  mModalOffSet[aOperMode];
   auto nbas = this->NBas(aOperMode);
   add += nbas*aModalNr;
   return add;
}
/**
 * Solve vscf equations
**/
void Vscf::Solve()
{
   LOGCALL("calls");

   if (mpVscfCalcDef->IoLevel() > I_10)
   {
      Mout << "Vscf::Solve: Solve vscf equations " << std::endl;
   }

   Timer time_all;
   this->AllOccOneModeInt();
   if (mpVscfCalcDef->TimeIt())
   {
      time_all.CpuOut(Mout,"\n CPU time used in AllOccOneModeInt: ");
   }
   this->AllOneModeOperNorms();
   if (mpVscfCalcDef->TimeIt())
   {
      time_all.CpuOut(Mout,"\n CPU time used in AllOneModeOperNorms: ");
   }

   this->Energy();  // Calculate energy at starting point

   In n_sc_1 = I_0;
   In n_sc_2 = I_0;
   In n_nsc = I_0;
   while (  !mConverged
         && mNiterGlobal < mpVscfCalcDef->GetMaxIter()
         )
   {
      LOGCALL("outer iteration");

      Timer time_it;
      for (LocalModeNr i_op_mode = I_0; i_op_mode < mpOneModeInt->GetmNoModesInInt(); i_op_mode++)
      {
         LOGCALL("one-mode iteration");

         auto nbas = this->NBas(i_op_mode);
         MidasMatrix fock(nbas);
         this->ConstructOneModeFock(i_op_mode, fock, mpOpDef->UseActiveTermsAlgo(), I_0, &n_sc_1, &n_sc_2, &n_nsc);
         this->DiagonalizeOneModeFock(i_op_mode, fock, false, false);
         this->UpdateOccOneModeInt(i_op_mode);
      }
      ++mNiterGlobal;
      this->Energy();  // Calculates and outputs energetics and test convergence
      if (  mpVscfCalcDef->Save()
         )
      {
         this->SaveOccModals();
      }
      if (  mpVscfCalcDef->TimeIt()
         )
      {
         time_it.CpuOut(Mout, " CPU time used in Midas Vscf iteration: ");
         time_it.WallOut(Mout," Wall time used in Midas Vscf iteration: ");
      }
   }

   if (  gTime
      )
   {
      time_all.CpuOut(Mout," CPU time used in Vscf::Solve():  ");
      time_all.WallOut(Mout," Wall time used in Vscf::Solve(): ");
   }

   In n_terms = n_nsc + n_sc_1 + n_sc_2;

   if (mpVscfCalcDef->IoLevel() > I_7)
   {
      Mout << " Screening statistics for whole sequence: " << std::endl;
      Mout << " Nr. of calculated terms  = " << setw(8) << n_nsc  << " percentage = " << (n_nsc*C_10_2)/n_terms << std::endl;
      Mout << " Nr. of screened terms 1  = " << setw(8) << n_sc_1 << " percentage = " << (n_sc_1*C_10_2)/n_terms << std::endl;
      Mout << " Nr. of screened terms 2  = " << setw(8) << n_sc_2 << " percentage = " << (n_sc_2*C_10_2)/n_terms << std::endl;
   }
}

/**
 * Final vscf moves - transforms and analysis.
**/
void Vscf::Finalize
   (  bool aRecalculateF
   )
{
   if (  mpVscfCalcDef->IoLevel() > I_10
      || gDebug
      )
   {
      Mout << "Vscf::Finalize: analysis and transforms " << std::endl;
   }

   bool final_it = (mpVscfCalcDef->IoLevel() > I_1); // Niels: This seems a bit fishy...
   bool store_all = mpVscfCalcDef->Save() || mpVscfCalcDef->DoRsp();
   bool calc_dens = mpVscfCalcDef->Calc1pDensities();
   bool int_anal = mpVscfCalcDef->GetIntAnalysis();
   //Mout << " Final " << final_it << " true " << true << std::endl;
   //Mout << " store_all " << store_all << " false " << false << std::endl;
   if (  store_all
      )
   {
      // change now to correct size, Have no effect for the file, check datacont.
      mEigVal.SetNewSize(mNoccModalCoef, false);
      mModals.SetNewSize(mNmodalCoef, false);
   }
   if (  final_it
      || store_all
      || calc_dens
      )
   {
      In n_sc_1 = I_0;
      In n_sc_2 = I_0;
      In n_nsc = I_0;
      if (  !mpVscfCalcDef->ModalLimitsInput()
         )
      {
         mpVscfCalcDef->SetModalLimitSize(mpOneModeInt->GetmNoModesInInt());
      }
      if (aRecalculateF)
      {
         for (LocalModeNr i_op_mode = I_0; i_op_mode < mpOneModeInt->GetmNoModesInInt(); ++i_op_mode)
         {
            auto nbas = this->NBas(i_op_mode);
            MidasMatrix fock(nbas);

            //
            ConstructOneModeFock
               (  i_op_mode
               ,  fock
               ,  mpOpDef->UseActiveTermsAlgo()
               ,  I_0
               ,  &n_sc_1
               ,  &n_sc_2
               ,  &n_nsc
               );

            //
            DiagonalizeOneModeFock
               (  i_op_mode
               ,  fock
               ,  final_it
               ,  store_all
               );

            UpdateOccOneModeInt(i_op_mode);
            // Update also the occupied modals so they correspond to the C coef. written out.
            if (!mpVscfCalcDef->ModalLimitsInput())
            {
               mpVscfCalcDef->SetModalLimit(i_op_mode, nbas);
            }
         }
      }
      // Calculates and outputs energetics and test convergence
      Energy();

      // Calculate state average energy.
      if (mpVscfCalcDef->DoStateAve())
      {
         if (mpVscfCalcDef->IoLevel() > I_2 || gDebug)
         {
            Mout << std::endl << "  **** Calculate state-average virtual VSCF energy **** " << std::endl;
         }

         VirtVscfStateAve();
      }

      // mbh: only calculate densities if !multilevel done!
      // Niels: The flag is now set in VscfCalcDef by AdgaSurface. But why are we actually doing this?
      if (  !mpVscfCalcDef->GetAdgaMultiLevelDone()
         )
      {
         if (mpVscfCalcDef->Calc1pDensities() && !mpVscfCalcDef->GetUseThermalDensity())
         {
            if (mpVscfCalcDef->IoLevel() > I_2 || gDebug)
            {
               Mout << std::endl << "  **** Calculate standard Vscf densities **** " << std::endl;
            }

            CalcDensities();
         }
         else if (mpVscfCalcDef->Calc1pDensities() && mpVscfCalcDef->GetUseThermalDensity())
         {
            if (mpVscfCalcDef->IoLevel() > I_2 || gDebug)
            {
               Mout << std::endl << "  **** Calculate thermal Vscf densities **** " << std::endl;
            }

            ThermalDensity(mpVscfCalcDef->GetThermalDensityTemp());
         }
      }
      if (!mpVscfCalcDef->Calc1pDensities() && mpVscfCalcDef->Plot1pDensities())
      {
         MidasWarning("You seem to have requested plots of the 1-particle modals but not the calculation of 1-particle densities, please correct!");
      }
      if (mpVscfCalcDef->Save())
      {
         SaveOccModals();
      }
   }
   // Save the data on disc.
   if (store_all)
   {
      // Seidler Oct. 17 2008: Changed to InMem.
      mEigVal.ChangeStorageTo("InMem");
      mModals.ChangeStorageTo("InMem");
      mEigVal.SaveUponDecon(true);
      mModals.SaveUponDecon(true);

      // Niels) March 26, 2018: We need to save the modals now and not after destructing the Vscf object.
      // Otherwise, we have no modals to restart Vcc calculations from, since Vcc is a Vscf.
      std::string name_base = Name();
      std::string modals_filename = name_base + "_Modals";
      DataCont modals = mModals;
      modals.NewLabel(modals_filename);
      modals.SaveUponDecon(true);
      std::string eigval_filename = name_base + "_EigVal";
      DataCont eigval = mEigVal;
      eigval.NewLabel(eigval_filename);
      eigval.SaveUponDecon(true);
   }
   if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
   {
      Mout << " Vscf::Finalize Modals & Eigvals are now on Disc " << std::endl;
   }
   if (int_anal)
   {
      IntAnalysis();
   }
}

/**
 * Carry out integral analysis for later screening.
**/
void Vscf::IntAnalysis()
{
   In n_modals=mpOpDef->NmodesInOp();
   vector<string> types;
   types.push_back("all");
   types.push_back("pi");

   vector<MidasVector> hamiltonian_measures;
   hamiltonian_measures.reserve(types.size());
   for (In i=I_0; i<types.size(); i++)
   {
      Nb** max_int=MaxInt(types[i]);
      MidasVector measure(GenScreenEstimates(max_int,types[i]));

      hamiltonian_measures.push_back(measure);

      for (In j=I_0 ; j < n_modals; j++) delete[] max_int[j];
      delete[] max_int;
   }
   vector<Nb> rec_min_eps = MinRecEigValPerMc();
}

/**
* Calculate maximum value per one-mode integral and operator later screening.
* */
Nb** Vscf::MaxInt(const string& arType)
{
   std::string file_name = mpVscfCalcDef->GetmVscfAnalysisDir() + "/MaxInts_" + arType;
   midas::mpi::OFileStream out_file(file_name);
   out_file.setf(ios::scientific);
   out_file.setf(ios::uppercase);
   out_file.precision(I_22);

   In n_modes=mpOpDef->NmodesInOp();
   In n_one_mode_op_tot=mpOneModeInt->GetmTotNoOneModeOpers();

   PutOperatorToWorkingOperator(mpOpDef,mpOneModeInt);

   Nb** max_int;
   max_int = new Nb*[n_modes];

   out_file << n_modes << endl;

   for(In i=I_0;i<n_modes;++i)
   {
      In i_g_mode   = mpOpDef->GetGlobalModeNr(i);
      In n_modals   = mpVscfCalcDef->Nmodals(i);
      In n_oper     = mpOpDef->NrOneModeOpers(i_g_mode);
      max_int[i] = new Nb[n_oper];

      out_file << i << " " << n_oper << endl;

      for (In j=I_0;j< n_oper;j++)
      {
         MidasVector integrals(n_modals);
         if (arType=="all") integrals.SetNewSize(n_modals*n_modals, false);

         if (arType=="ip")
         { 
            pIntegrals()->GetIntegrals(i, j).GetOffsetRow(integrals, I_0, I_0);
         }
         else if (arType =="pi")
         {
            pIntegrals()->GetIntegrals(i, j).GetOffsetCol(integrals, I_0, I_0);
         }
         else if (arType == "all") 
         {
            integrals.MatrixRowByRow(pIntegrals()->GetIntegrals(i, j));
         }
         else
         {
            MIDASERROR("Don't understand arType option - not equal ip,pi,all");
         }

         if (arType=="all" && mpVscfCalcDef->GetIntAnalysisLimit()< n_modals)
            integrals.SetNewSize(mpVscfCalcDef->GetIntAnalysisLimit()*n_modals,true);
         else if (arType=="all" && mpVscfCalcDef->GetIntAnalysisLimit() > n_modals)
            MidasWarning ("IntAnalysisLimit set larger than Modal limit - take modal limit instead");
         max_int[i][j] = integrals.FindMaxAbsValue();
         out_file << j << " " << max_int[i][j] << endl;
      }
   }
   return max_int;
}
/**
* Calculate maximum value per one-mode integral and operator later screening.
* */
vector<Nb> Vscf::GenScreenEstimates(Nb** appMaxInt, const string& arType)
{
   std::string screen_file_all = mpVscfCalcDef->GetmVscfAnalysisDir() + "/ScreenEstimates_" + arType;
   midas::mpi::OFileStream out_file(screen_file_all);
   out_file.setf(ios::scientific);
   out_file.setf(ios::uppercase);
   out_file.precision(I_22);

   std::string screen_file_mc = mpVscfCalcDef->GetmVscfAnalysisDir() + "/ScreenEstimatesMcs_" + arType;
   midas::mpi::OFileStream out_file_mc(screen_file_mc);
   out_file_mc.setf(ios::scientific);
   out_file_mc.setf(ios::uppercase);
   out_file_mc.precision(I_22);

   out_file << "Estimates for screening of terms and mode combinations" << endl << endl;

   ModeCombiOpRange mcr=mpOpDef->GetModeCombiOpRange();

   In n_mc=mcr.Size();
   Nb** max_prod;
   max_prod= new Nb*[n_mc];
   vector<Nb>  max_sum_mc(n_mc);
   for (In i_mc=I_0; i_mc < n_mc; i_mc++)
   {
      ModeCombi mc=mcr.GetModeCombi(i_mc);
      In n_modes=mc.Size();
      vector<In> mc_vec=mc.MCVec();
      out_file << "#1 MODECOMBINATION " << endl;
      out_file << "   " <<  i_mc << " " <<mc_vec << endl;

      vector<GlobalOperNr> op_for_mc=mpOpDef->OpersForMc(mc);
      In n_op = op_for_mc.size();

      max_sum_mc[i_mc]=C_0;
      max_prod[i_mc]= new Nb[n_op];
      for (In i_op=I_0; i_op < n_op; i_op++)
      {
         Nb coef=mpOpDef->Coef(op_for_mc[i_op]);
         Nb prod=fabs(coef);
         for (In i_modes=I_0; i_modes < n_modes; i_modes++)
         {
            In mode=mc_vec[i_modes];
            In op= mpOpDef->OperForOperMode(op_for_mc[i_op],mode);
            Nb fac = appMaxInt[mode][op];
            prod *= fac ;
         }
         max_prod[i_mc][i_op]=prod;
         max_sum_mc[i_mc] += prod;
      }
      out_file << "   #2 SUM" << endl;
      out_file << "      " <<  max_sum_mc[i_mc] << endl;
      for (In i=I_0; i< mc_vec.size()-I_1; i++)
         out_file_mc  << mc_vec[i] << ",";
      out_file_mc<< mc_vec[mc_vec.size()-I_1]<< " "  <<  max_sum_mc[i_mc] << endl;
      out_file << "   #2 OPERATOR CONTRIBUTIONS" << endl;
      for (In i_op=I_0; i_op < n_op; i_op++)
         out_file << "      " << max_prod[i_mc][i_op] << " " << op_for_mc[i_op] <<  " " <<  mpOpDef->GetOpers(op_for_mc[i_op]) << endl;
   }

   out_file.close();

   for (In i=I_0 ; i < n_mc; i++) delete[] max_prod[i];
   delete[] max_prod;

   return max_sum_mc;

}
/**
* Determines minimal epsilons for a mode combination range for later screening.
* So far only for the ground state !!!
* */
vector<Nb> Vscf::MinRecEigValPerMc()
{
   std::string screen_file_mc = mpVscfCalcDef->GetmVscfAnalysisDir() + "/MinEps";
   midas::mpi::OFileStream out_file_mc(screen_file_mc);
   out_file_mc.setf(ios::scientific);
   out_file_mc.setf(ios::uppercase);
   out_file_mc.precision(I_22);

   std::string screen_file_eps = mpVscfCalcDef->GetmVscfAnalysisDir() + "/Eps";
   midas::mpi::OFileStream out_file_eps(screen_file_eps);
   out_file_eps.setf(ios::scientific);
   out_file_eps.setf(ios::uppercase);
   out_file_eps.precision(I_22);

   ModeCombiOpRange mcr=mpOpDef->GetModeCombiOpRange();
   In n_modals=mpOpDef->NmodesInOp();
   vector<Nb> one_mode_first_ex;
   one_mode_first_ex.reserve(n_modals);

   for (In i=I_0; i< n_modals; i++)
   {
      if (mpVscfCalcDef->GetOccMode(i)!=I_0)
         MIDASERROR("Minimal epsilons for screening are only available for the ground state");
      Nb e_diff= GetEigVal(i,I_1) - GetEigVal(i,I_0);
      out_file_eps << i << " " << e_diff << endl;
      one_mode_first_ex.push_back(e_diff);
   }

   In n_mc=mcr.Size();
   vector<Nb> min_rec_ex;
   min_rec_ex.reserve(n_mc);

   for (In i_mc=I_0; i_mc < n_mc; i_mc++)
   {
      ModeCombi mc=mcr.GetModeCombi(i_mc);
      In n_modes=mc.Size();
      vector<In> mc_vec=mc.MCVec();
      Nb eps_min=C_0;
      for (In n=I_0 ; n < n_modes ; n++)
      {
        In mode=mc_vec[n];
        eps_min += one_mode_first_ex[mode];
      }
      min_rec_ex.push_back(C_1/eps_min);
      for (In i=I_0; i< mc_vec.size()-I_1; i++)
         out_file_mc  << mc_vec[i] << ",";
      out_file_mc<< mc_vec[mc_vec.size()-I_1]<< " "  <<  min_rec_ex[i_mc] << endl;
  }

  return min_rec_ex;
}

/**
 * Save OccModals on disc.
**/
void Vscf::SaveOccModals()
{
   std::string name_base = Name();
   std::string filename = name_base + "_OccModals";
   DataCont occ_modals(mOccModals, "ONDISC", filename, true);
   if (gDebug)
   {
      Mout << " Occ modals saved to disc: " << std::endl << mOccModals << std::endl;
   }
}

/**
 * Get OccModals from file
**/
bool Vscf::GetOccModalsFromFile(const string& arFileName)
{
   DataCont occ_modals;
   occ_modals.NewLabel(arFileName);
   occ_modals.ChangeStorageTo("OnDisc");
   occ_modals.SetNewSize(mNoccModalCoef);
   occ_modals.SaveUponDecon(true);
   auto io_stat = occ_modals.DataIo(IO_GET | IO_ROBUST, mOccModals, mNoccModalCoef);

   if (io_stat == decltype(occ_modals)::Io::SUCCESS)
   {
      Mout << " Occupied modals has been read in from file " << arFileName << std::endl;
      if (gDebug)
      {
         Mout << " Occ modals readin: " << endl << mOccModals << std::endl;
      }
      return true;
   }
   else
   {
      Mout << " Reading of Occupied modals from file " << arFileName << " failed !!! " << std::endl;
      MidasWarning(" Reading of Occ modals for restart failed - I will survive but use standard start guess ");
      return false;
   }
}

/**
 * Diagonalize one mode Fock operator and get new modals.
**/
void Vscf::DiagonalizeOneModeFock
   (  LocalModeNr aOperMode
   ,  MidasMatrix& aFock
   ,  bool aFinal
   ,  bool aStoreAll
   )
{
   LOGCALL("calls");

   if (mpVscfCalcDef->IoLevel() > I_10)
   {
      Mout << "Vscf::DiagonalizeOneModeFock: for mode " << aOperMode << std::endl;
   }
   LocalModeNr i_op_mode = aOperMode;
   auto nbas = this->NBas(i_op_mode);
   if (nbas != aFock.Ncols())
   {
      MIDASERROR( "Nbas , Fock size mismatxh in Vscf Diagonalize" );
   }
   MidasMatrix eig_vec(nbas);
   MidasVector eig_val(nbas);

   // Check symmetry of Fock matrix
   bool is_symmetric = aFock.IsSymmetric();

   if (  !is_symmetric
      )
   {
      MidasWarning("Non-symmetric Fock matrix detected! This cannot be handled in the current implementation!");

      if (gDebug)
      {
         Mout  << " Non-symmetric Fock matrix:\n" << aFock << std::endl;
      }
   }

   // Set diagonalization method.
   std::string diag_method;
   if (  mpVscfCalcDef->DiagMeth()=="LAPACK"
      )
   {
      diag_method =  "DSYEVD";
   }
   else if  (  mpVscfCalcDef->DiagMeth()=="MIDAS"
            )
   {
      diag_method="MIDAS_JACOBI";
   }
   else if  (  mpVscfCalcDef->DiagMeth()=="DSYEVD"
            || mpVscfCalcDef->DiagMeth()=="MIDAS_JACOBI"
            )
   {
      diag_method=mpVscfCalcDef->DiagMeth();
   }
   else
   {
      MidasWarning("Only DSYEVD/DGEEV (LAPACK), or MIDAS_JACOBI (MIDAS) allowed DiagMeth. Override!");
      diag_method= "DSYEVD";
   }

   if (  !mpBasDef
      || mpOpDef->StateTransferOnly(aOperMode)           // Also do symmetric for state-transfer operators
      || (  mpBasDef
         && mpBasDef->OneModeType(aOperMode) == "HO"
         )
      )
   {
      LOGCALL("diag symmetric fock matrix");

      // Orthogobal HO basis is simple.
      Diag(aFock, eig_vec, eig_val, diag_method, true);
   }
   else if  (  mpBasDef->OneModeType(aOperMode) == "Gauss"
            || mpBasDef->OneModeType(aOperMode) == "Bsplines"
            )
   {
      // Take extra care of non-orthogobal basis sets.
      MidasMatrix s_m(nbas);
      LocalOperNr i_add = mpOpDef->GetUnitOpForMode(i_op_mode);
      s_m.Zero();
      mpOneModeInt->GetInt(i_op_mode, i_add, s_m);

      // Niels: It was a bad idea not to use SYGVD before...
      {
         LOGCALL("sygvd");
         // Solve symmetric problem
         auto eig = SYGVD(aFock, s_m);

         // Set results
         SetMidasMatrixFromColumnMajorPtr(eig_vec, eig._eigenvectors, eig._n, eig._num_eigval);
         eig_val.SetData(eig._eigenvalues, eig._num_eigval, eig._num_eigval);
      }
   }
   else
   {
      MIDASERROR("Unknown basis type for Vscf::DiagonalizeOneModeFock");
   }

   this->FixModalPhases(eig_vec); // Fix phase of each modal for consistency.
   if (  mpVscfCalcDef->Restart()
      && !OldModalsErr()
      )
   {
      if (  mpVscfCalcDef->IoLevel() > I_10
         )
      {
         Mout << "Trying to compare the phases with the old modals" << std::endl;
      }
      this->FixModalPhasesRestartCheck(eig_vec,i_op_mode);
   }

   // Store info, ok they should already be occupied, but it is much preferred that
   // they OccModals etc are in synch with the modals on disc, else strange (but very small)
   // differences may occur between occupied integrals in vector and
   // full integrals on disc.
   MidasVector modal(nbas);
   eig_vec.GetCol(modal,mpVscfCalcDef->GetOccMode(i_op_mode));
   mOccModals.PieceIo(IO_PUT,modal,nbas,OccModalOffSet(i_op_mode));
   mOccEigVal[i_op_mode] = eig_val[mpVscfCalcDef->GetOccMode(i_op_mode)];

   if (  aFinal
      )
   {
      if (mpVscfCalcDef->IoLevel() > I_7)
      {
         Nb ave_eig_val = eig_val.SumEle();
         ave_eig_val /= eig_val.Size();
         ave_eig_val -= mOccEigVal[i_op_mode];
         Mout << " Average modal excitation energies for mode " << i_op_mode << " " << ave_eig_val << std::endl;
      }
      In nextra = min(I_2, static_cast<In>(nbas - mpVscfCalcDef->GetOccMode(i_op_mode)));
      if (mpVscfCalcDef->IoLevel() > I_11)
      {
         nextra = nbas - mpVscfCalcDef->GetOccMode(i_op_mode) - I_1;

         Mout << right << std::endl;
         GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i_op_mode);
         Mout << " Modals for mode: " << gOperatorDefs.GetModeLabel(i_g_mode)
              << " (up to occupied +" << setw(3) << nextra << " extra) " << std::endl;

         In atatime = I_1;
         In iadd = I_0;
         for (In nleft = mpVscfCalcDef->GetOccMode(i_op_mode) + I_1 + nextra; nleft > I_0; nleft -= atatime)
         {
            In thistime = min(nleft,atatime);

            Mout << "--------------------------------------------------------------------------";
            Mout << std::endl;
            In iadd2 = iadd;
            for (In j = I_0; j < thistime; j++)
            {
               if (j - I_1 == mpVscfCalcDef->GetOccMode(i_op_mode))
               {
                  Mout << "* "<< setw(23) << iadd2;
               }
               else if (j != I_0)
               {
                  Mout << "  "<< setw(23) << iadd2;
               }
               else if (j == I_0)
               {
                  Mout << "modal:" << setw(19) << iadd2;
               }
               iadd2++;
            }
            Mout << std::endl;
            Mout << "--------------------------------------------------------------------------";
            Mout << std::endl;
            iadd2 = iadd;
            for (In j = I_0; j < thistime; j++)
            {
               if (j == I_0)
               {
                  Mout << "w:";
               }
               if (j != I_0)
               {
                  Mout << "  ";
               }
               Mout << setw(23) << eig_val[iadd2];
               Mout << " " << setw(23) << eig_val[iadd2] - mOccEigVal[i_op_mode];
               iadd2++;
            }

            Mout << std::endl;
            Mout << "--------------------------------------------------------------------------";
            Mout << std::endl;

            for (In k = I_0; k < nbas; k++)
            {
               iadd2 = iadd;
               for (In j = I_0; j < thistime; j++)
               {
                  Mout << "  " << setw(23) << eig_vec[k][iadd2];
                  iadd2++;
               }
               Mout << std::endl;
            }
            iadd += thistime;
         }
      }
   }
   if (  aStoreAll
      )
   {
      // For convenience in use store only the difference relative to the occupied modals
      // and store with the occupied modal at 0,
      // ORIGINALLY: The ref modal energy is still stored however. (Not zero)
      // LATER: THIS WAS FOUND CONFUSIING::::
      //Mout << " eig_val before " << eig_val << endl;
      In modalnr = mpVscfCalcDef->GetOccMode(i_op_mode);
      Nb ref = eig_val[modalnr];
      for (In icol=modalnr;icol>0;icol--) eig_val[icol] = (eig_val[icol-1]-ref);
      for (In icol=modalnr+1;icol<eig_val.Size();icol++) eig_val[icol] = (eig_val[icol]-ref);
      eig_val[I_0] = C_0; // ref;
      mEigVal.DataIo(IO_PUT,eig_val,nbas,OccModalOffSet(i_op_mode));
      //Mout << " eig_val after " << eig_val << endl;

      // For convenience in Io store the eigen vector matrix transposed,
      // and with the occupied modals at 0
      eig_vec.Transpose();
      eig_vec.ShiftRowIndexToZero(mpVscfCalcDef->GetOccMode(i_op_mode));

      mModals.DataIo(IO_PUT,eig_vec,nbas*nbas,nbas,nbas,ModalOffSet(i_op_mode));
   }
}

/**
 * Fix phase of each modal such that largest coefficient is positive.
 * Assumes modal coefficients are given as the columns of aModals.
 **/
void Vscf::FixModalPhases
   (  MidasMatrix& aModals
   )
{
   for(In i=I_0; i<aModals.Ncols(); ++i)
   {
      Nb maxabs = C_0;
      for (In k=I_0; k<aModals.Nrows(); ++k)
      {
         if (  std::abs(aModals[k][i]) > std::abs(maxabs)
            )
         {
            maxabs = aModals[k][i];
         }
      }

      if (  maxabs < C_0
         )
      {
         for(In j=I_0; j<aModals.Nrows(); ++j)
         {
            aModals[j][i] = -aModals[j][i];
         }
      }
   }
}
/**
 * Fix phase of each modal such that largest coefficient is positive.
 * Assumes modal coefficients are given as the columns of aModals.
 **/
void Vscf::FixModalPhasesRestartCheck(MidasMatrix& aModals,LocalModeNr aOperMode)
{

   DataCont old_modals_dc;
   string file_name = mpVscfCalcDef->Name(true) + "_OldModals";
   //Mout << "Alberto : In FixModalPhasesRestartCheck(), file_name: " << file_name << endl;
   old_modals_dc.GetFromExistingOnDisc(mNmodalCoef,file_name);
   old_modals_dc.SaveUponDecon();

   // Old modals should now be available for checking
   LocalModeNr i_op_mode  = aOperMode;
   auto nbas = this->NBas(i_op_mode);
   MidasVector old_modal(nbas);
   MidasVector new_modal(nbas);
   MidasVector s_times_modal(nbas);
   // old modals are stored with occupied modal as nr one!
   // So order 2 0 1 3 4   ... if 2 is the  occupied
   // this is not the case with the new yet....
   // order 0 1 2 3 4
   In occ_modal_indx = mpVscfCalcDef->GetOccMode(i_op_mode);
   // Take extra care of non-orthogobal basis sets.
   MidasMatrix s_m(nbas);
   s_m.Unit();
   bool non_orto_bas = mpBasDef ? (!mpOpDef->StateTransferOnly(i_op_mode) || mpBasDef->OneModeType(aOperMode) == "Gauss" || mpBasDef->OneModeType(aOperMode) == "Bsplines") : false;
   if (non_orto_bas)
   {
      LocalOperNr i_add = mpOpDef->GetUnitOpForMode(i_op_mode);
      s_m.Zero();
      mpOneModeInt->GetInt(i_op_mode,i_add,s_m);
   }

   for (In k=0; k<nbas; k++)
   {
      In j=k;
      if (k==occ_modal_indx) j=0;
      if (k<occ_modal_indx) j=k+1;

      // Alberto:new version, should exit when the file is not found.

      auto io_stat = old_modals_dc.DataIo(IO_GET | IO_ROBUST, old_modal,nbas,ModalAddress(i_op_mode,j));
      if (io_stat == decltype(old_modals_dc)::Io::SUCCESS)
      {
         if (mpVscfCalcDef->IoLevel() > I_10)
         {
            Mout << " Old modal have been read in from file " << file_name << ". Now proceeding to phase check." << std::endl;
         }
      }
      else
      {
         if ( !OldModalsErr() )
         {
            if (mpVscfCalcDef->IoLevel() > I_10)
            {
               Mout << "Reading of old modals from file " << file_name << " failed !!! " << std::endl;
            }
            MidasWarning(" Reading of old modals for restart failed - cannot check the phases of the new modals, may result in errors.  ");
            SetOldModalsErr(true);
            return;
         }
      }

      //old_modals_dc.DataIo(IO_GET,old_modal,nbas,ModalAddress(i_op_mode,j));
      aModals.GetCol(new_modal,k);
      //Mout << " old modal " << old_modal << endl;
      //Mout << " new modal " << new_modal << endl;
      s_times_modal=s_m*new_modal;
      Nb dot = Dot(s_times_modal,old_modal);
      if (mpVscfCalcDef->IoLevel() > I_11)
      {
         Mout << " Old vs new modal overlap = " << dot << std::endl;
      }
      if (dot < -C_I_2)
      {
         aModals.ScaleCol(C_M_1,k);
         if (mpVscfCalcDef->IoLevel() > I_10)
         {
            Mout << " Phase of new modals changed to match that of the modals given as restart " << std::endl;
         }
      }
   }
}

/**
* Get modal energy for mode and modal nr.
* */
Nb Vscf::GetEigVal(const In aOperMode, const In aEigValNr)
{
   Nb val;
   mEigVal.DataIo(IO_GET,OccModalOffSet(aOperMode)+aEigValNr,val);
   return val;
}
/**
 * Calculate all occupied one mode integrals h^(m,p)_(i_mi_m)
**/
void Vscf::AllOccOneModeInt()
{
   LOGCALL("calls");

   if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
   {
      Mout << "Vscf::AllOccOneModeInt Calculate all occupied modal integrals initially " << std::endl;
   }
   for (In i_op_mode = I_0; i_op_mode < mpOneModeInt->GetmNoModesInInt(); i_op_mode++)
   {
      LOGCALL("update one-mode ints");
      UpdateOccOneModeInt(i_op_mode);
   }
}
/**
* Update occupied one mode integrals h^(m,p)_(i_mi_m) for mode m
* */
void Vscf::UpdateOccOneModeInt(LocalModeNr aOperMode)
{
   LOGCALL("calls");

   if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
   {
      Mout << "Vscf::UpdateOccOneModeInt for OperMode " << aOperMode << std::endl;
   }
   LocalModeNr i_op_mode = aOperMode;
   auto nbas = this->NBas(i_op_mode);

   In n_op = mpOpDef->NrOneModeOpers(i_op_mode);
   //bool on_fly=(mpOpDef->OpTermsRegrouped() && !mpOneModeInt.StoreComOpInt());
   //if (on_fly) Mout << " Integrals over comb. operators are calculated now " << endl;
   //In n_op_com=0;
   //if (on_fly)
   //{
      //n_op_com = mpOpDef->NrComOneModeOpers(i_op_mode);
      //n_op-=n_op_com;
   //}
   //Mout << " Nr. of integrals calculated on the fly: " << n_op_com << endl;
   //Mout << " Nr. of integrals stored and calculated: " << n_op << endl;
   MidasVector modal(nbas);
   if (gDebug)
   {
      Mout << " OffSet for modal: " << OccModalOffSet(i_op_mode) << " for i_op_mode" << i_op_mode << std::endl;
   }
   mOccModals.PieceIo(IO_GET, modal, nbas, OccModalOffSet(i_op_mode));
   if (gDebug)
   {
      Mout << " i_op_mode, modal norm, sumele: " << i_op_mode << " " << modal.Norm() << " " << modal.SumEle() << std::endl;
   }
   In add = mOneModeIntOccOff[aOperMode];

   for (LocalOperNr i_op = I_0; i_op < n_op; i_op++)
   {
      LOGCALL("one-mode oper");

      if (gDebug)
      {
         Mout << "UpdateOccOneModeInt: " << setw(10) << left << gOperatorDefs.GetOneModeOperDescription(mpOpDef->GetOneModeOperGlobalNr(i_op_mode, i_op)) << "i_op " << i_op << " mode " << i_op_mode << std::endl;
      }
      Nb trans_int;
      // Get the expectation value for an operator and wave function in bra and ket state
      mpOneModeInt->IntBracket(modal, i_op_mode, i_op, trans_int);

      if (gDebug)
      {
         GlobalOperNr g_op_nr = mpOpDef->GetOneModeOperGlobalNr(i_op_mode, i_op);
         Mout << " Expectation value for one-mode operator " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << "(Q" << i_op_mode << ") is: " << trans_int << std::endl;
      }

      if (mpOpDef->GetIsElementaryOperator())
      {
         if (mpOpDef->GetElemMode() == i_op_mode && mpOpDef->GetElemP() == I_0 && mpOpDef->GetElemQ() == I_0)
         {
            trans_int = C_1;
         }
         else
         {
            trans_int = C_0;
         }

         if (gDebug)
         {
            Mout << " UpdateOccOneModeInt for mode " << i_op_mode << " Elementary: trans_int " << trans_int << std::endl;
         }
      }

      mOneModeIntOcc[add + i_op] = trans_int;
   }
}

/**
 * Calculate the 1-p densities over the modals
**/
void Vscf::CalcDensities()
{
   MidasAssert(this->mpBasDef, "We need a BasDef to calculate densities.");
   if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
   {
       Mout << "Vscf::1pDensities() " << std::endl;
   }
   if (mpVscfCalcDef->IoLevel() > I_2 || gDebug)
   {
      Mout << " Calculate one-mode Vscf densities";

      if (mpVscfCalcDef->AnalizeMaxDensity())
      {
         Mout << " and analyze the maximum density" << std::endl;
      }
      else if (mpVscfCalcDef->AnalizeMeanDensity())
      {
         Mout << " and analyze the mean density" << std::endl;
      }
   }
   if (mpVscfCalcDef->IoLevel() > I_2 && mpVscfCalcDef->Plot1pDensities())
   {
      Mout << " Using one-mode Vscf densities to plot wave functions and modals for individual modes " << std::endl;
   }
   In n_modes = mpVscfCalcDef->GetNmodesInOcc();
   if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
   {
      Mout << " No. of modes in Vscf::1pDensities() = " << n_modes << std::endl;
   }
   // Calculate the 1-particle densities on the monodimensional cuts. The grid is hardcoded now
   for (LocalModeNr i_op_mode = I_0; i_op_mode < mpVscfCalcDef->GetNmodesInOcc(); i_op_mode++)
   {
      GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr i_bas_mode = mpBasDef ? In(mpBasDef->GetLocalModeNr(i_g_mode)) : -I_1;

      auto nbas = this->NBas(i_op_mode);
      if (mpVscfCalcDef->IoLevel() > I_11)
      {
         Mout << " For Mode " << i_op_mode <<  " # of basis functions: " << nbas << std::endl;
      }
      // This is the default if no iterative pes has been given
      MidasVector grid(I_0, C_0); //store the grid of points
      MidasVector dens_max(I_0, C_0); //store max density
      MidasVector dens_mean(I_0, C_0); //store mean density
      MidasVector func_vals(I_0, C_0); //store wavefunc.
      MidasVector dens_vals(I_0, C_0); //store density
      // Determine the type of primitive basis functions used for representing the wave functions
      // Niels: Is it not very wrong to take it from the global calcdef vector? I changed it now...
      std::string basis_name = this->mpVscfCalcDef->Basis(); //gVscfCalcDef[I_0].Basis();
      In i_basis = -I_1;
      for (In i = I_0; i < gBasis.size(); ++i)
      {
         if (gBasis[i].GetmBasName() == basis_name)
         {
            i_basis = i;
         }
      }
      if (i_basis == -I_1)
      {
         MIDASERROR("Basis not found in VscfDrv");
      }

      // Retrieve information about grid and potential values from file, (used when primitive basis functions for representing the wave function is Gaussians or harmonic oscillator functions)
      if (  !(gBasis[i_basis].OneModeType(i_bas_mode) == "Bsplines")
         && (mpVscfCalcDef->GetmAdgaDensAnalysis() || mpVscfCalcDef->Plot1pDensities())
         )
      {
         // Read the potential(s)
         for (size_t i = 0; i < gPesCalcDef.GetmPesAdaptiveProps().size(); ++i)
         {
            ReadPotentialFromFile(gOperatorDefs.GetModeLabel(i_g_mode), grid, i);
         }
      }
      // Modals with B-spline functions as primitive basis are treated in a special way
      // This is partly to ensure that the dynamic extension for Adga works unhindered
      else if (   gBasis[i_basis].OneModeType(i_bas_mode) == "Bsplines"
              &&  (  mpVscfCalcDef->GetmAdgaDensAnalysis()
                  || mpVscfCalcDef->Plot1pDensities()
                  )
              )
      {
         In grid_plot_points = I_150;
         grid.SetNewSize(I_2 * grid_plot_points + I_1);
         std::pair<Nb, Nb> basis_bounds = gBasis[i_basis].GetBasDefForGlobalMode(i_op_mode).GetBasisGridBounds();

         for (In i_q = I_0; i_q < grid_plot_points + I_1; i_q++)
         {
            grid[i_q] = basis_bounds.first * Nb(Nb(grid_plot_points - i_q) / grid_plot_points);
         }

         for (In i_q = I_1; i_q < grid_plot_points + I_1; i_q++)
         {
            grid[i_q + grid_plot_points] = basis_bounds.second * Nb(Nb(i_q) / grid_plot_points);
         }
      }
      // Hard-coded grid of points
      else
      {
         if (  i_bas_mode == -I_1
            )
         {
            MIDASERROR("Cannot construct default grid if no basis is specified for this mode: GlobalModeNr = " + std::to_string(i_g_mode));
         }
         // Define the grid of points in a Midasvector.
         MidasWarning("Vscf::CalcDensities -  Defining the grid for ADGA-VSCF communication in ugly hard-coded way - I hope you dont see this message ");
         In n_pts = 2000;
         In n_max = 10;
         Nb scale = C_2;
         Nb r_max = std::sqrt(C_2*(Nb(n_max) + C_I_2)/(mpBasDef->GetOmeg(i_bas_mode)))*scale;
         grid.SetNewSize(n_pts);
         Nb step = C_2*r_max/Nb(n_pts - I_1);
         //Mout << " For i_op_mode " << i_op_mode << " omeg: " << mpBasDef->GetOmeg(i_bas_mode) << " r_max: " << r_max << " step: " << step << endl;
         for (In i_p = I_0; i_p < n_pts; i_p++)
         {
            grid[i_p] = -r_max + step*i_p;
         }
         //Mout << " Grid constructed: " << endl;
         //Mout << grid << endl;
      }

      if (mpVscfCalcDef->GetmAdgaDensAnalysis())
      {
         In n_st = mpVscfCalcDef->GetNrStatesForAnalysis(i_op_mode);

         if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
         {
            Mout << " Number of states per mode include in the Adga mean density: " << n_st << std::endl;
         }

         func_vals.SetNewSize(grid.Size());
         dens_vals.SetNewSize(grid.Size());
         dens_max.SetNewSize(grid.Size());
         dens_mean.SetNewSize(grid.Size());
         dens_max.Zero();
         dens_mean.Zero();

         for (In i_state = I_0; i_state < n_st; i_state++)
         {
            MidasVector modal(nbas);
            func_vals.Zero(); // storage for wave func.
            dens_vals.Zero(); // storage for density
            mModals.DataIo(IO_GET, modal, nbas, ModalAddress(i_op_mode, i_state));

            if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
            {
               Mout << " For mode " << i_op_mode << ", modal coefficients: " << std::endl;
               Mout << modal << std::endl;
            }

            In n_pts = grid.Size();
            for (In i_p = I_0; i_p < n_pts; i_p++)
            {
               // Calculate wave function as a sum of primitive basis functions
               for (In i_bas = I_0; i_bas < nbas; i_bas++)
               {
                  func_vals[i_p] += modal[i_bas] * mpBasDef->EvalOneModeBasisFunc(i_bas_mode, i_bas, grid[i_p]);
               }

               // Calculate the density by multiplying wave function with itself, (real modals are assumed)
               dens_vals[i_p] = std::pow(func_vals[i_p], C_2);

               if (mpVscfCalcDef->IoLevel() > I_14 || gDebug)
               {
                  Mout << " For point: " << grid[i_p] << " dens_val: " << dens_vals[i_p] << " func_val: " << func_vals[i_p] << std::endl;
               }

               // If the mean density is required, then the individual 1-mode densities are summed and divided by the number of modals
               if (mpVscfCalcDef->GetmAdgaDensAnalysis())
               {
                  if (dens_vals[i_p] > dens_max[i_p])
                  {
                     dens_max[i_p] = dens_vals[i_p];
                  }

                  dens_mean[i_p] += (dens_vals[i_p])/(n_st);
               }
            }
         }
            
         // Write the mean density to file for processing by the Adga
         DensPrepareIterative(gOperatorDefs.GetModeLabel(i_g_mode), grid, dens_mean, dens_max);
      }
      
      // If indicated, plots of the individual modals can also be carried out
      if (mpVscfCalcDef->Plot1pDensities())
      {
         MidasVector wf_plot_vals(grid.Size(), C_0); // storage for wave func.
         MidasVector dens_plot_vals(grid.Size(), C_0); // storage for density
         for (In i_state = I_0; i_state < mpVscfCalcDef->GetPlotNmodals(); i_state++)
         {
            wf_plot_vals.Zero();
            dens_plot_vals.Zero();
            MidasVector modal(nbas);
            mModals.DataIo(IO_GET, modal, nbas, ModalAddress(i_op_mode, i_state));

            if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
            {
               Mout << " For mode " << i_op_mode << ", modal coefficients: " << std::endl;
               Mout << modal << std::endl;
            }

            In n_pts = grid.Size();
            for (In i_p = I_0; i_p < n_pts; i_p++)
            {
               // Calculate wave function as a sum of primitive basis functions
               for (In i_bas = I_0; i_bas < nbas; i_bas++)
               {
                  wf_plot_vals[i_p] += modal[i_bas] * mpBasDef->EvalOneModeBasisFunc(i_bas_mode, i_bas, grid[i_p]);
               }
               // Calculate the density by multiplying wave function with itself, (real modals are assumed)
               dens_plot_vals[i_p] = std::pow(wf_plot_vals[i_p], C_2);
            }
            // Output the wave function and density plots of modals
            DensPrepareAnalysis(gOperatorDefs.GetModeLabel(i_g_mode), i_state, grid, wf_plot_vals, dens_plot_vals);
         }
      }
   }
}

/**
 * Prepare files specific for iterative pes construction
**/
void Vscf::DensPrepareIterative
   (  const std::string& arModeLabel
   ,  MidasVector& arGridVals
   ,  MidasVector& arMeanVals
   ,  MidasVector& arMaxVals
   )  const
{
   In n_pts = arGridVals.Size();
   if ("" == mpVscfCalcDef->GetmVscfAnalysisDir())
   {
      Mout << " Directory for the iterative pes procedure is not set. No data file will be generated." << std::endl;
      return;
   }

   // Plot the max density
   if (mpVscfCalcDef->AnalizeMaxDensity())
   {
      if (arMaxVals.Size() != n_pts)
      {
         MIDASERROR(" Mismatch in the nr. of function values in Vscf::DensPrepareIterative");
      }

      std::string oper_file_name = mpOpDef->GetOpFile(I_0);

      auto beginning_of_number = oper_file_name.find_last_of("_");
      auto end_of_number = oper_file_name.find_last_of("1234567890");

      std::string file_name = mpVscfCalcDef->GetmVscfAnalysisDir() + "/" +  "MaxDens_Prop" + oper_file_name.substr(beginning_of_number + 1, end_of_number-beginning_of_number) + "_" + arModeLabel + ".mplot";

      if (mpVscfCalcDef->IoLevel() > I_14)
      {
         Mout << " Generating data file \"" << file_name << "\" for iterative pes procedure " << std::endl;
      }
      midas::mpi::OFileStream odat(file_name);
      odat.setf(ios_base::scientific);
      odat.setf(ios_base::uppercase);
      odat.precision(22);
      for (In i_p = I_0; i_p < n_pts; i_p++)
      {
         odat << arGridVals[i_p] << '\t' << arMaxVals[i_p] << std::endl;
      }
      // Append filename to list of files read by analysis program.
      std::string file_name_list = mpVscfCalcDef->GetmVscfAnalysisDir() + "/VscfDensFiles";
      midas::mpi::OFileStream ms(file_name_list, midas::mpi::OFileStream::StreamType::MPI_MASTER, std::ios_base::app);
      ms << file_name << std::endl;
   }
   // Plot the mean density
   else if (mpVscfCalcDef->AnalizeMeanDensity())
   {
      if (arMeanVals.Size() != n_pts)
      {
         MIDASERROR(" Mismatch in the nr. of density values in Vscf::DensPrepareIterative");
      }

      std::string oper_file_name = mpOpDef->GetOpFile(I_0);

      auto beginning_of_number = oper_file_name.find_last_of("_");
      auto end_of_number = oper_file_name.find_last_of("1234567890");

      std::string file_name = mpVscfCalcDef->GetmVscfAnalysisDir() + "/" +  "MeanDens_Prop" + oper_file_name.substr(beginning_of_number + 1, end_of_number-beginning_of_number) + "_" + arModeLabel + ".mplot";

      if (mpVscfCalcDef->IoLevel() > I_14)
      {
         Mout << " Generating data file \"" << file_name << "\" for iterative pes procedure " << std::endl;
      }
      midas::mpi::OFileStream odat(file_name);
      odat.setf(ios_base::scientific);
      odat.setf(ios_base::uppercase);
      odat.precision(22);
      for (In i_p = I_0; i_p < n_pts; i_p++)
      {
         odat << arGridVals[i_p] << '\t' << arMeanVals[i_p] << std::endl;
      }

      // Append filename to list of files read by analysis program.
      std::string file_name_list = mpVscfCalcDef->GetmVscfAnalysisDir() + "/VscfDensFiles";
      midas::mpi::OFileStream ms(file_name_list, midas::mpi::OFileStream::StreamType::MPI_MASTER, std::ios::app);
      ms << file_name << std::endl;
   }
}

/**
 * Prepare stuff for plotting occ. modals and 1-particle densities
**/
void Vscf::DensPrepareAnalysis
   (  const std::string& arModeLabel
   ,  const In& arState
   ,  MidasVector& arGridVals
   ,  MidasVector& arFuncVals
   ,  MidasVector& arDensVals
   )  const
{
   if ("" == mpVscfCalcDef->GetmVscfAnalysisDir())
   {
      Mout << " Analysis directory not set. No data file will be generated." << std::endl;
      return;
   }
   // Plot the wave functions
   In n_pts = arGridVals.Size();
   if (arFuncVals.Size() != n_pts)
   {
      MIDASERROR(" Mismatch in the nr. of function values in Vscf::1pDensPrepareAnalysis");
   }

   std::string file_name = mpVscfCalcDef->GetmVscfAnalysisDir() + "/" +  mpVscfCalcDef->Name() + "_wf_mode_" + arModeLabel + "_modal_" + std::to_string(arState);

   if (mpVscfCalcDef->IoLevel() > I_14)
   {
      Mout << " Generating data file \"" << file_name << "\" for analysis program" << std::endl;
   }

   midas::mpi::OFileStream odat(file_name);
   odat.setf(ios_base::scientific, ios_base::floatfield);
   odat.precision(16);
   for (In i_p = I_0; i_p < n_pts; i_p++)
   {
      odat << arGridVals[i_p] << '\t' << arFuncVals[i_p] << std::endl;
   }

   // Append filename to list of files read by analysis program.
   std::string file_name_list = mpVscfCalcDef->GetmVscfAnalysisDir() + "/VscfWfFiles";
   midas::mpi::OFileStream odat2(file_name_list, midas::mpi::OFileStream::StreamType::MPI_MASTER, std::ios_base::app);
   odat2 << file_name << endl;

   // Plot the density functions
   if (arDensVals.Size() != n_pts)
   {
      MIDASERROR(" Mismatch in the nr. of density values in Vscf::1pDensPrepareAnalysis");
   }

   file_name = mpVscfCalcDef->GetmVscfAnalysisDir() + "/" + mpVscfCalcDef->Name() + "_dens_mode_" + arModeLabel + "_modal_" + std::to_string(arState);

   if (mpVscfCalcDef->IoLevel() > I_14)
   {
      Mout << " Generating data file \"" << file_name << "\" for analysis program" << std::endl;
   }
   midas::mpi::OFileStream odat3(file_name);
   odat3.setf(ios_base::scientific, ios_base::floatfield);
   odat3.precision(16);
   for (In i_p = I_0; i_p < n_pts; i_p++)
   {
      odat3 << arGridVals[i_p] << '\t' << arDensVals[i_p] << std::endl;
   }

   // Append filename to list of files read by analysis program.
   file_name_list = mpVscfCalcDef->GetmVscfAnalysisDir() + "/Vscf1pDensFiles";
   midas::mpi::OFileStream odat4(file_name_list, midas::mpi::OFileStream::StreamType::MPI_MASTER, std::ios::app);
   odat4 << file_name << std::endl;
}

/**
 * Read the potential information from the files for the iterative procedure
**/
void Vscf::ReadPotentialFromFile
   (  const std::string& arModeLabels
   ,  MidasVector& arGridVals
   ,  const Uin& arPropNo
   )  const
{
   std::string data_file_name = mpVscfCalcDef->GetmVscfAnalysisDir() + "/" + "Potential_Prop" + std::to_string(arPropNo + I_1) + "_" + arModeLabels + ".mplot";
   
   if (mpVscfCalcDef->IoLevel() > I_10)
   {
      Mout << " For mode " << arModeLabels << " The potential values are read from file: " << data_file_name << std::endl;
   }

   ifstream data_file;
   data_file.open(data_file_name.c_str(), ios_base::in);
   if (data_file.fail())
   {
      MIDASERROR(" File with potential data not found!");
   }
   data_file.setf(ios::scientific);
   data_file.setf(ios::uppercase);
   data_file.precision(22);

   std::vector<Nb> pot_val;
   std::vector<Nb> grid_val;
   while (!data_file.eof())
   {
      std::string s;
      while (getline(data_file,s))
      {
         istringstream input_s(s);
         Nb x_val;
         Nb p_val;
         input_s >> x_val >> p_val;
         grid_val.push_back(x_val);
         pot_val.push_back(p_val);
      }
   }
   data_file.close();
   In n_pts = pot_val.size();
   arGridVals.SetNewSize(n_pts);
   for (In i = I_0; i < n_pts; i++)
   {
      arGridVals[i] = grid_val[i];
   }

   if (mpVscfCalcDef->IoLevel() > I_14 || gDebug)
   {
      Mout << " Data read from file: " << data_file_name << std::endl << std::endl;
      for (In i = I_0; i < n_pts; i++)
      {
         Mout.setf(ios::scientific);
         Mout.setf(ios::uppercase);
         Mout.precision(22);
         Mout << grid_val[i] << "\t" << pot_val[i] << std::endl;
      }
   }
   return;
}

/**
 * Calculate norm of all one mode operators h^(m,t)
**/
void Vscf::AllOneModeOperNorms()
{
   LOGCALL("calls");

   if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
   {
      Mout << "Vscf::AllOneModeOperNorms Calculate all one mode operator norms " << std::endl;
   }
   for (LocalModeNr i_op_mode = I_0; i_op_mode < mpOneModeInt->GetmNoModesInInt(); i_op_mode++)
   {
      LOGCALL("mode loop");
      auto nbas = this->NBas(i_op_mode);
      In n_op = mpOpDef->NrOneModeOpers(i_op_mode);
      In add = mOneModeIntOccOff[i_op_mode];
      //if calculated on the fly then compute also remaining integrals
      //bool on_fly=(mpOpDef->OpTermsRegrouped() && !mpOneModeInt.StoreComOpInt());
      //if (on_fly) Mout << " Norms for integrals over comb. operators are calculated now " << endl;
      //In n_op_com=0;
      //if (on_fly)
      //{
         //n_op_com = mpOpDef->NrComOneModeOpers(i_op_mode);
         //n_op-=n_op_com;
      //}
      //Mout << " Nr. of integrals calculated on the fly: " << n_op_com << endl;
      //Mout << " Nr. of integrals stored and calculated: " << n_op << endl;

      for (LocalOperNr i_op = I_0; i_op < n_op; i_op++)
      {
         LOGCALL("oper loop");
         MidasMatrix h(nbas);
         mpOneModeInt->GetInt(i_op_mode, i_op,h);
         Nb norm = h.Norm();
         mOneModeOperNorms[add+i_op] = norm;

         if (mpVscfCalcDef->IoLevel() > I_11 || gDebug)
         {
            Mout << " For mode " << i_op_mode << " operator " << i_op << " has norm " << norm << std::endl;
         }
         if (mpVscfCalcDef->IoLevel() > I_12 || gDebug)
         {
            Mout << " The h^(m,t) matrix, as used in the VSCF equations is: \n " << h << std::endl;
         }
      }
      //add=mOneModeIntOccOff[aOperMode]+n_op;
      //then calculate the remaining integrals on the fly if needed
      //if (on_fly)
      //{
         //for (In i_com_op=0; i_com_op<mpOpDef->NrComOneModeOpers(i_op_mode); i_com_op++)
         //{
            //Mout << " linear combination nr. " << i_com_op << endl;
            //MidasMatrix dum_int(n_bas);
            //dum_int.Zero();
            //for (In i_t=0; i_t<mpOpDef->NtermsInComOper(i_com_op); i_t++)
            //{
               //In i_op=mpOpDef->OperNrInComOper(i_op_mode,i_t,i_com_op);
               //Nb coef=mpOpDef->OperComInComOper(i_op_mode,i_t,i_com_op);
               //string s_op=mpOpDef->GetOneModeOperDescription(i_op_mode,i_op);
               //Mout << " for term: " << i_t << " op label: " << s_op << " op. nr.: " << i_op << endl;
               //AddIntTo(dum_int,i_op_mode,i_op,coef);
            //}
            //Nb norm = dum_int.Norm();
            //mOneModeOperNorms[add+i_com_op] = norm;
            //Mout << " i_op_mode " << i_op_mode << " i_op " << i_op << " norm " << norm << endl;
            //Mout << " TEST matrix \n " << dum_int << endl;
         //}
      //}
   }
}

/**
* Construct one mode Fock matrix.
* aOperMode:    The mode.
* aFock:        The Fock matrix:
* aActiveTerms: Use only terms in H including aOperMode. Affects only diagonal.
* aHlevel:      Use only terms in H with this coupling level. Set to 0 to include all terms.
* arSc1:        Screening statistics.
* arSc2:        -
* arNsc:        -
* */
void Vscf::ConstructOneModeFock
   (  LocalModeNr aOperMode
   ,  MidasMatrix& aFock
   ,  bool aActiveTerms
   ,  In aHlevel
   ,  In* arSc1
   ,  In* arSc2
   ,  In* arNsc
   )
{
   LOGCALL("calls");

   if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
   {
      Mout << "Vscf::ConstructOneModeFock() for mode: " << aOperMode << std::endl;
   }

   Nb expt_size = mFockNorms[aOperMode];
   aFock.Zero();

   Nb factor_for_unit = I_0;
   auto nbas = this->NBas(aOperMode);

   if (  aActiveTerms
      )
   {
      In n_sc_1 = I_0;
      In n_sc_2 = I_0;
      In n_nsc = I_0;

      In n_act_terms = this->mpOpDef->NactiveTerms(aOperMode);

      if (mpVscfCalcDef->IoLevel() > I_7)
      {
         Mout << " The mode Q" << aOperMode << " has " << n_act_terms << " active terms and the following will be used to construct the VSCF one-mode Fock operator: " << std::endl;
      }

      #pragma omp parallel
      {
         In i_term = I_0;
         In nfacs = I_0;
         Nb fact = C_0;
         In i_fac_thismode = -I_0;
         Nb cont_size_est = C_0;

         #pragma omp for schedule(dynamic) reduction(MidasMatrix_Add : aFock), reduction(+ : n_sc_1), reduction(+ : n_sc_2), reduction(+ : n_nsc)
         for (In i_act_term = I_0; i_act_term < n_act_terms; i_act_term++)
         {
            i_term = mpOpDef->TermActive(aOperMode, i_act_term);

            nfacs = mpOpDef->NfactorsInTerm(i_term);
            if (  aHlevel != I_0
               && aHlevel != nfacs
               )
            {
               continue;
            }

            fact = mpOpDef->Coef(i_term);
            if (  std::abs(fact) < mpVscfCalcDef->ScreenZeroCoef()
               )
            {
               ++n_sc_1;
               continue;
            }

            i_fac_thismode = -I_1;
            for (In i_fac = I_0; i_fac < nfacs; ++i_fac)
            {
               LocalModeNr i_op_mode = this->mpOpDef->ModeForFactor(i_term, i_fac);
               if (  i_op_mode == aOperMode
                  )
               {
                  i_fac_thismode = i_fac;
               }
               else
               {
                  LocalOperNr i_oper = this->mpOpDef->OperForFactor(i_term, i_fac);
                  fact *= this->mOneModeIntOcc[this->mOneModeIntOccOff[i_op_mode] + i_oper];
               }
            }

            if (  i_fac_thismode > -I_1
               )
            {
               LocalOperNr i_oper = this->mpOpDef->OperForFactor(i_term, i_fac_thismode);
               cont_size_est = std::abs(fact)*this->mOneModeOperNorms[this->mOneModeIntOccOff[aOperMode] + i_oper];
               if (  (cont_size_est/expt_size) < this->mpVscfCalcDef->ScreenZero()
                  )
               {
                  ++n_sc_2;
                  continue;
               }
               ++n_nsc;
               this->mpOneModeInt->AddIntTo(aFock, aOperMode, i_oper, fact);

               if (mpVscfCalcDef->IoLevel() > I_7)
               {
                  GlobalOperNr g_op_nr = mpOpDef->GetOneModeOperGlobalNr(aOperMode, i_oper);

                  Mout << "  One-mode operator: " << gOperatorDefs.GetOneModeOperDescription(g_op_nr) << " has coefficient: " << mpOpDef->Coef(i_term) << " and combined coefficient/integral value: " << fact << std::endl;
               }
            }
            else if (i_fac_thismode == -I_1)
            {
               MIDASERROR("Vscf::ConstructOneModeFock(): Factor not found for active mode. This cannot happen unless active-terms algorithm is buggy!.");
            }
         }
      }

      // Add to screening statistics
      if (  arSc1
         )
      {
         (*arSc1) += n_sc_1;
      }
      if (  arSc2
         )
      {
         (*arSc2) += n_sc_2;
      }
      if (  arNsc
         )
      {
         (*arNsc) += n_nsc;
      }
   }
   else
   {
      for (In i_term = I_0; i_term < mpOpDef->Nterms(); i_term++)
      {
         In nfacs = mpOpDef->NfactorsInTerm(i_term);
         if (aHlevel != I_0 && aHlevel != nfacs)
         {
            continue;
         }

         Nb fact = mpOpDef->Coef(i_term);
         if (fabs(fact) < mpVscfCalcDef->ScreenZeroCoef())
         {
            continue;
         }

         In i_fac_thismode = -I_1;
         for (In i_fac = I_0; i_fac < mpOpDef->NfactorsInTerm(i_term); i_fac++)
         {
            LocalModeNr i_op_mode = mpOpDef->ModeForFactor(i_term, i_fac);
            if (i_op_mode == aOperMode)
            {
               i_fac_thismode = i_fac;
            }
            else
            {
               LocalOperNr i_oper = mpOpDef->OperForFactor(i_term, i_fac);
               fact *= mOneModeIntOcc[mOneModeIntOccOff[i_op_mode] + i_oper];
            }
         }

         if ((fabs(fact)/expt_size) < mpVscfCalcDef->ScreenZero())
         {
            continue;
         }

         if (i_fac_thismode > -I_1)
         {
            LocalOperNr i_oper = mpOpDef->OperForFactor(i_term, i_fac_thismode);
            mpOneModeInt->AddIntTo(aFock, aOperMode, i_oper, fact);
         }
         else if (i_fac_thismode == -I_1)
         {
            factor_for_unit += fact;
         }
      }

      for (In i = I_0; i < nbas; i++)
      {
         aFock[i][i] += factor_for_unit;
      }
   }

   // NB: The following assumes an orthonormal primitive modal basis - which we have right now.
   Nb norm_fock = aFock.Norm();
   mFockNorms[aOperMode] = norm_fock;

   if (mpVscfCalcDef->IoLevel() > I_9)
   {
      Mout << " Norm of VSCF Fock operator = " << norm_fock << std::endl;
   }
}

/**
 * Calculate Fock matrix for mode aMode using only terms in the Hamiltonian with mode coupling
 * level aHlevel. If aHlevel==I_0, all terms are included.
 * Screening is not applied.
 **/
void Vscf::ConstructPartialOneModeFock
   (  LocalModeNr aMode
   ,  MidasMatrix& aFock
   ,  In aHlevel
   )
{
   if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
   {
      Mout << "Vscf::ConstructPartialOneModeFock() for mode: " << aMode << std::endl;
   }

   aFock.Zero();

   Nb factor_for_unit = 0;
   auto nbas = this->NBas(aMode);
   MidasMatrix h_m_p(nbas);

   // Loop over all terms in Hamiltonian but only process those where number of factors
   // is equal to aHlevel.
   for (In i_term=0;i_term<mpOpDef->Nterms();i_term++)
   {
      In nfacs = mpOpDef->NfactorsInTerm(i_term);
      if (aHlevel!=I_0 && aHlevel!=nfacs)
         continue;

      Nb fact = mpOpDef->Coef(i_term);
      In i_fac_thismode = -I_1;
      for (In i_fac=I_0; i_fac<nfacs; i_fac++)
      {
         LocalModeNr i_op_mode  = mpOpDef->ModeForFactor(i_term,i_fac);
         if (i_op_mode == aMode)
         {
            i_fac_thismode = i_fac;
         }
         else
         {
            LocalOperNr i_oper     = mpOpDef->OperForFactor(i_term,i_fac);
            fact *= mOneModeIntOcc[mOneModeIntOccOff[i_op_mode]+i_oper];
         }
      }

      if (i_fac_thismode >-1)
      {
         LocalOperNr i_oper     = mpOpDef->OperForFactor(i_term,i_fac_thismode);
         mpOneModeInt->AddIntTo(aFock,aMode,i_oper,fact);
      }
      else // This term has no operator in the product.
         factor_for_unit += fact;
   }

   for (In i=0;i<nbas;i++)
      aFock[i][i] += factor_for_unit;
}

/**
* Calculate Vscf energy
* */
void Vscf::Energy()
{
   LOGCALL("calls");

   if (mpVscfCalcDef->IoLevel() > I_7)
   {
      if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
      {
         Mout << "Vscf::Energy: calculate Vscf energy " << std::endl;
      }

      Mout << " The modal energies in iteration " << mNiterGlobal << " are (mode/occ-level/modal-energy): " << std::endl;

      Nb sum_over_modal_energy = C_0;
      for (In i_op_mode = I_0; i_op_mode < mpOneModeInt->GetmNoModesInInt(); i_op_mode++)
      {
         Mout << "  " << i_op_mode << " " << mpVscfCalcDef->GetOccMode(i_op_mode) << " "
              << mOccEigVal[i_op_mode] << std::endl;
         sum_over_modal_energy += mOccEigVal[i_op_mode];
      }

      Mout << " The sum over modal energy is: " << sum_over_modal_energy << std::endl;
   }

   mEprevIt = mEthisIt;
   mEthisIt = I_0;
   for (In i_term = I_0; i_term < mpOpDef->Nterms(); i_term++)
   {
      Nb fact = C_1;
      for (In i_fac = I_0; i_fac < mpOpDef->NfactorsInTerm(i_term); i_fac++)
      {
         LocalModeNr i_op_mode  = mpOpDef->ModeForFactor(i_term,i_fac);
         LocalOperNr i_oper     = mpOpDef->OperForFactor(i_term,i_fac);
         fact *= mOneModeIntOcc[mOneModeIntOccOff[i_op_mode]+i_oper];

         if (gDebug)
         {
            Mout << " i_term " << i_term << "i_op_mode " << i_op_mode << " fact " << fact << " this cont " << mOneModeIntOcc[mOneModeIntOccOff[i_op_mode]+i_oper] << std::endl;
         }
      }

      if (mpVscfCalcDef->IoLevel() > I_13)
      {
         Mout << " i_term " << i_term << " Evscf contribution (wo prefac): " << fact << std::endl;
      }

      fact *= mpOpDef->Coef(i_term);

      if (mpVscfCalcDef->IoLevel() > I_13)
      {
         Mout << " i_term " << i_term << " Evscf contribution: " << fact << std::endl;
      }

      mEthisIt += fact;
   }

   Nb diff = -mEthisIt  + mEprevIt;

   // Write status of VSCF iteration to output
   if (mpVscfCalcDef->IoLevel() > I_2)
   {
      Mout << " Iter. " << mNiterGlobal << " E_vscf = " << mEthisIt;
   }

   Nb reldiff;
   if (fabs(mEthisIt) <= C_NB_EPSILON)
   {
      reldiff = diff;
   }
   else
   {
      reldiff = diff/mEthisIt;
   }

   if (mNiterGlobal > I_0)
   {
      if (mpVscfCalcDef->IoLevel() > I_2)
      {
         Mout << " (decrease:  " << diff << ",rel=" << setw(I_5) << reldiff << ") ";
      }
   }

   if (mpVscfCalcDef->IoLevel() > I_2)
   {
      Mout << std::endl;
   }

   if (fabs(reldiff) < mpVscfCalcDef->GetEnerThr())
   {
      mConverged = true;
   }
}

/**
*
* Calculate state average transformed integrals.
* \f$ \tilde{h}^{m,o}=\sum_{r_m}  {h}^{m,o}_{r_mr_m} w_{r_m}^m \f$
*
* */
void Vscf::CalculateStateAveTransformedInt
   (  vector<MidasVector>& arVecOfTransformedInts
   ,  const vector<MidasVector>& arVecOfWeightsVec
   )
{
   if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
   {
      Mout << " In Vscf::CalculateStateAveTransformedInt " << std::endl;
   }

   arVecOfTransformedInts.resize(mpVscfCalcDef->GetNmodesInOcc());
   //Mout << " size of arVecOfTransformedInts " << arVecOfTransformedInts.size() << endl;

   for (In i_op_mode=0;i_op_mode<mpVscfCalcDef->GetNmodesInOcc();i_op_mode++)
   {
      auto nbas = this->NBas(i_op_mode);

      In n_op = mpOpDef->NrOneModeOpers(i_op_mode);
      arVecOfTransformedInts[i_op_mode].SetNewSize(n_op);
      In n_sa_occ_modals=arVecOfWeightsVec[i_op_mode].Size();
      //Mout << "mode " << i_op_mode << " n_op " << n_op << " n_sa_occ_modals " << n_sa_occ_modals << endl;

      for (In i_op=I_0; i_op<n_op;i_op++)
      {
         MidasMatrix h(nbas);
         mpOneModeInt->GetInt(i_op_mode,i_op,h);
         TransInt(h,i_op_mode);   // Unnecessary to transform all integrals, but then....

//       \tilde{h}^{m,o}=\sum_{r_m}  {h}^{m,o}_{r_mr_m} w_{r_m}^m
         Nb x=C_0;
         for (In r_m = I_0; r_m<n_sa_occ_modals;r_m++)
         {
            x += h[r_m][r_m]*(arVecOfWeightsVec[i_op_mode])[r_m];
            //Mout << " x " << x << endl;
         }
         (arVecOfTransformedInts[i_op_mode])[i_op]=x;
      }
   }
}
/**
*
* Calculate state average weights
* \f$ w^m_{r_m} = \frac{1}{N_m} \f$
*
* */
void Vscf::CalculateStateAveWeights(vector<MidasVector>& arVecOfWeightsVec)
{
   if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
   {
      Mout << " In Vscf::CalculateStateAveWeights" << std::endl;
   }
   // loop modes
   arVecOfWeightsVec.resize(mpVscfCalcDef->GetNmodesInOcc());
   for (In i_op_mode=0;i_op_mode<mpVscfCalcDef->GetNmodesInOcc();i_op_mode++)
   {
      In n_occ=mpVscfCalcDef->GetStateAveNr(i_op_mode);
      auto nbas = this->NBas(i_op_mode);
      if (n_occ > nbas) MIDASERROR( " State average number beyound basis size ");
      if (n_occ < I_1) MIDASERROR( " State average number cannot be zero or negative ");

      Nb w = C_1/n_occ;
      arVecOfWeightsVec[i_op_mode].SetNewSize(n_occ,C_0);
      for (In i=I_0;i<n_occ;i++) arVecOfWeightsVec[i_op_mode][i]=w;
      //Mout << " weight vect this mode " << arVecOfWeightsVec[i_op_mode] << endl;
   }
}
/**
* Calculate virtual Vscf state average energy
* */
void Vscf::VirtVscfStateAve()
{
   if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
   {
      Mout << " In Vscf::VirtVscfStateAve" << std::endl;
   }

   Nb e_ave = C_0;
   if (mpVscfCalcDef->DoFundOnlyStateAve())
   {
      if (mpVscfCalcDef->IoLevel() > I_3)
      {
         Mout << " State-average energy will contain contributions from: " << std::endl;
         Mout << "  Ground state vibrational energy " << std::endl;
         Mout << "  Fundamental vibrational excitation energies " << std::endl;
      }

      e_ave = VirtVscfStateAveFundOnly();
   }
   else
   {
      if (mpVscfCalcDef->IoLevel() > I_3)
      {
         Mout << " State-average energy will contain contributions from: " << std::endl;
         Mout << "  Ground state vibrational energy " << std::endl;
         Mout << "  Fundamental vibrational excitation energies " << std::endl;
         Mout << "  Overtone vibrational excitation energies " << std::endl;
         Mout << "  Combination band vibrational excitation energies " << std::endl;
      }

      e_ave = VirtVscfStateAveFactWeig();
   }

   Mout << std::endl << " sa-vVSCF energy: " << e_ave << std::endl;

   mpVscfCalcDef->SetDoneStateAve(e_ave);
}

/**
 * Calculate virtual Vscf state average energy
**/
Nb Vscf::VirtVscfStateAveFundOnly()
{
   Nb vscf_ener = GetEtot();
   In n_modes = mpOpDef->NmodesInOp();
   Nb e_ave = vscf_ener;

   for (In i = I_0; i < n_modes; i++)
   {
      Nb ref_val = GetEigVal(i,I_0);
      e_ave += vscf_ener + (GetEigVal(i, I_1) - ref_val);  // Get fundamentals, modal=1
   }

   e_ave = e_ave / (n_modes + I_1);
   return e_ave;
}

/**
 * Calculate virtual Vscf state average energy
**/
Nb Vscf::VirtVscfStateAveFactWeig()
{
   if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
   {
      Mout << " In Vscf::VirtVscfStateAveFactWeig" << std::endl;
   }
//
// Prepare weights:
// 1. Staight average per mode:  w^m_{r_m} = \frac{1}{N_m}
//
   vector<MidasVector> a_vec_of_vec_of_weights;
   CalculateStateAveWeights(a_vec_of_vec_of_weights);

//
// Prepare transformed integrals:
//  \tilde{h}^{m,o}=\sum_{r_m}  {h}^{m,o}_{r_mr_m} w_{r_m}^m
//
   vector<MidasVector> a_vec_of_trans_int;
   CalculateStateAveTransformedInt(a_vec_of_trans_int,a_vec_of_vec_of_weights);

//
// Calculated average
//  e_ave = \sum_t c_t \prod_m \tilde{h}^{m,t}

   Nb e_ave=C_0;
   for (In i_term=0;i_term<mpOpDef->Nterms();i_term++)
   {
      Nb fact = C_1;
      for (In i_fac=0;i_fac<mpOpDef->NfactorsInTerm(i_term);i_fac++)
      {
         LocalModeNr i_op_mode  = mpOpDef->ModeForFactor(i_term,i_fac);
         LocalOperNr i_oper     = mpOpDef->OperForFactor(i_term,i_fac);
         fact *= a_vec_of_trans_int[i_op_mode][i_oper];
         if (mpVscfCalcDef->IoLevel() > I_14 || gDebug)
         {
            Mout << " i_term " << i_term
                 << " i_op_mode " << i_op_mode
                 << " fact " << fact
                 << " this cont " << a_vec_of_trans_int[i_op_mode][i_oper] << std::endl;
         }
      }
      if (mpVscfCalcDef->IoLevel() > I_14 || gDebug)
      {
         Mout << " i_term " << i_term << " sa-Evscf contribution (wo prefac): " << fact << std::endl;
      }
      fact *= mpOpDef->Coef(i_term);
      e_ave += fact;
      if (mpVscfCalcDef->IoLevel() > I_14 || gDebug)
      {
         Mout << " i_term " << i_term << " sa-Evscf contribution: " << fact << " accumulated " << e_ave << std::endl;
      }
   }
   return e_ave;

}
/**
* Transform integrals in arM from primitive to modal basis.
* */
void Vscf::TransInt(MidasMatrix& arM,LocalModeNr arOperMode)
{
   if (mpVscfCalcDef->IoLevel() > I_10 || gDebug)
   {
      Mout << "Vscf::TransInt for OperMode " << arOperMode << std::endl;
   }
   In i_op_mode  = arOperMode;
   auto nbas = this->NBas(i_op_mode);

   //Mout << " Matrix in primitive basis \n" << arM << endl;
   if (nbas != arM.Nrows())
   {
      MIDASERROR(" TransInt: Integral matrix assumed to fit dimension");
   }
   MidasMatrix prim_int(arM);
   MidasMatrix eig_vec(nbas);
   mModals.DataIo(IO_GET,eig_vec,nbas*nbas,nbas,nbas,ModalOffSet(i_op_mode));
   MidasMatrix tmp(nbas);
   tmp = eig_vec*prim_int;
   eig_vec.Transpose();
   arM = tmp*eig_vec;
   //Mout << " Matrix in transformed basis \n" << arM << endl;
}

/**
* Reset the occupied integrals according to the present operator
* */
void Vscf::ResetOccInt()
{
   mOneModeIntOcc.SetNewSize(mpOneModeInt->GetmTotNoOneModeOpers());
   In n_opers                   = 0;
   for (LocalModeNr i_op_mode=0;i_op_mode<mpVscfCalcDef->GetNmodesInOcc();i_op_mode++)
   {
      mOneModeIntOccOff[i_op_mode]      = n_opers;
      n_opers                           += mpOpDef->NrOneModeOpers(i_op_mode);
      UpdateOccOneModeInt(i_op_mode);
   }
}

/**
* Reset the occupied integrals according to the present operator
* */
void Vscf::Erase()
{
   mEigVal.SaveUponDecon(false);
   mModals.SaveUponDecon(false);
}

vector<Nb> Vscf::CalcOneModePartFunc(Nb aTemp, In aTempRow,bool aUseLimit)
{
   vector<Nb> result;
   result.clear();
   Nb beta=C_0;
   if(aTemp==C_0)
      beta=-C_NB_MAX;
   else
      beta=-C_AUTK/aTemp;
   for(In i=I_0;i<mpOpDef->NmodesInOp();i++)
   {
      MidasVector weights(max(I_1,mpVscfCalcDef->Nmodals(i)),C_0);
      weights[I_0]=C_1;
      Nb one_mode_part_func=C_1;
      Nb ref_val= GetEigVal(i,I_0);
      // Mout << " ref_val " << ref_val << endl;
      // remember that the eigenvalues are listed relative to the 'GS'
      In end=I_0,start=I_1;
      start=mOccModalOffSet[i]+I_1;
      if(i==(mpOpDef->NmodesInOp()-I_1))
         end=mEigVal.Size();
      else
         end=mOccModalOffSet[i+I_1];
      if (aUseLimit) end=start+mpVscfCalcDef->Nmodals(i)-I_1;
      Mout << " output Nmodals: " << mpVscfCalcDef->Nmodals(i);
      Mout << " start:  " << start << " end:  " << end << endl;
      for(In j=start;j<end;j++)
      {
         Nb val=GetEigVal(i,j-mOccModalOffSet[i]);
         // Mout << " val " << val << endl;
         // Add contribution
         // Nb cont=exp(beta*val);
         Nb cont2=exp(beta*(val-ref_val));
         one_mode_part_func+=cont2;
         weights[j-start+I_1]=cont2;
         Mout << " j " << j << " val " << val << " cont2 " << cont2 << endl;
      }
      weights=weights*(C_1/one_mode_part_func);
      //for(In i_occ=I_0;i_occ<weights.Size();i_occ++)
      for(In i_occ=I_0;i_occ<mpVscfCalcDef->Nmodals(i);i_occ++)
      {
         In i_col=mOccModalOffSet[i]+I_1+i_occ;
         mpVscfCalcDef->SetOccupancy(aTempRow,i_col,weights[i_occ]);
      }
      Mout << " Calculate one_mode_part_func weights " << weights << endl;
      result.push_back(one_mode_part_func);
   }
   Mout << " one mode part funcs for temp " << aTemp << " is: \n " << result << endl;
   return result;
}

/**
*  Calculate the one-mode temperature derivative for partition functions
*  i.e. dlnZ/dT
**/
Nb Vscf::CalcOneStateTempDeriv(Nb aTemp, In aTempRow)
{
   Nb vscf_ener=mpVscfCalcDef->GetEfinal();
   Mout << "Final VSCF energy = " << vscf_ener << endl;
   // C_AUTK == 1/k_b
   Nb beta=-C_AUTK/aTemp;
   Nb result=vscf_ener*C_AUTK/(aTemp*aTemp);
   // that was the prefactor.
   In n_modes=mpOpDef->NmodesInOp();
   // now do the double summation
   // loop over modes
   for(In i=I_0;i<n_modes;i++) {
      In start=mOccModalOffSet[i]+I_1;
      In end=I_0;
      Nb contribution=C_0;
      if(i==(mpOpDef->NmodesInOp()-I_1))
         end=mEigVal.Size();
      else
         end=mOccModalOffSet[i+I_1];
      // loop over the number of modals for mode i
      for(In j=start;j<end;j++) {
         Nb val;
         mEigVal.DataIo(IO_GET,j,val);
         contribution+=(val*C_AUTK*exp(beta*val)/(aTemp*aTemp));
      }
      Nb curr_one_mode_pf=mpVscfCalcDef->GetOneStateMatrix(aTempRow,I_1+i);
      contribution/=curr_one_mode_pf;
      result+=contribution;
   }
   return result;
}

/**
*  Calculate the one-mode 2nd temperature derivative for partition functions
*  i.e. d2lnZ/dT2
**/
Nb Vscf::CalcOneStateTemp2Deriv(Nb aTemp, In aTempRow, Nb aDlnZdT) {
   Nb result=aDlnZdT*(-C_2/aTemp);
   // that was first term
   In n_modes=mpOpDef->NmodesInOp();
   // now do the double summation
   // loop over modes
   for(In i=I_0;i<n_modes;i++) {
      In start=mOccModalOffSet[i]+I_1;
      In end=I_0;
      if(i==(mpOpDef->NmodesInOp()-I_1))
         end=mEigVal.Size();
      else
         end=mOccModalOffSet[i+I_1];
      // loop over the number of modals for mode i
      // first: \sum_s^m E/kT^2*exp(-E/kT)
      Nb cont1=C_0;
      for(In j=start;j<end;j++) {
         Nb val;
         mEigVal.DataIo(IO_GET,j,val);
         cont1+=(val/(C_KB*aTemp*aTemp))*exp(-val/(C_KB*aTemp));
      }
      // z_m is in the onestatematrix at i+I_1, as osm[k,0] == T
      Nb z_m=mpVscfCalcDef->GetOneStateMatrix(aTempRow,I_1+i);
      for(In j=start;j<end;j++) {
         Nb val;
         mEigVal.DataIo(IO_GET,j,val);
         Nb fac=(C_1/z_m)*(val/(C_KB*aTemp*aTemp))*exp(-val/(C_KB*aTemp));
         Nb a=val/(C_KB*aTemp*aTemp);
         Nb b=(C_1/z_m)*cont1;
         result+=(fac*(a-b));
      }
      //cout << "contrib for mode " << i << " = " << mode_result << endl;
   }
   return result;
}

/**
  * Put a certain operator to be the working operator for calculation
  * of transformations. That is calc. necessary integrals in the modal basis and so on.
  * */
void Vscf::PutOperatorToWorkingOperator(OpDef* apOpDef,OneModeInt* apOneModeInt)
{
   //pOpDef()->DeleteOpRange(); // Delete the existing operator range.
   if(gDebug)
   {
      Mout << " Prepare Operator '" << apOpDef->Name() << "' to be the working operator in transformations. " << std::endl;
   }
   SetOpDef(apOpDef);
   SetOneModeInt(apOneModeInt);
   ResetOccInt(); // Reset h_ii
   UpdateIntegrals();
   // Initialize the operator range of the operator
   // Just in case it has never been used before
   if (!apOpDef->IsOpRangeInitialized()) apOpDef->InitOpRange();
}

/**
  * Restore the original operator.
  * */
void Vscf::RestoreOrigOper()
{
   //pOpDef()->DeleteOpRange(); // Delete the existing operator range.
   RestoreOpDef();  // Set to point to the original operator.
   RestoreOneModeInt();  // Set to the point to the original one mode integrals
   // This does not calculate the actual integrals. Do so now:
   ResetOccInt(); // Reset h_ii
   // Necessary:
   // Prepare general modal integrals offsets.
   UpdateIntegrals(); // and the integrals
   if (!pOpDef()->IsOpRangeInitialized())
   {
      MIDASERROR(" OpRange of reference operator should have been initialized");
   }
      //mpOpDer-InitOpRange(); // Initializing the operator range of the operator
      //should be unnecessary as this is the reference operator which has
      // already by used.
}

//!
const ModalIntegrals<Nb>* Vscf::pIntegrals() const
{
   return mOneModeModalBasisInt.get();
}
ModalIntegrals<Nb>* Vscf::pIntegrals()
{
   return mOneModeModalBasisInt.get();
}

//! Update modal integrals.
void Vscf::UpdateIntegrals()
{
   *mOneModeModalBasisInt = midas::vcc::modalintegrals::InitFromVscf<Nb>(*this, Name()+"_OneModeInt_ModalBasis");
}

/**
 * @param aMode      Local mode number of OpDef
 * @return
 *    Number of basis functions for this mode
 **/
In Vscf::NBas
   (  LocalModeNr aMode
   )  const
{
   GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(aMode);
   LocalModeNr i_bas_mode = mpBasDef ? In(mpBasDef->GetLocalModeNr(i_g_mode)) : -I_1;
   In nbas = (i_bas_mode != -I_1) ? mpBasDef->Nbas(i_bas_mode) : mpOpDef->StateTransferIJMax(aMode) + I_1;

   return nbas;
}

/**
 *
 **/
void Vscf::DumpModalsForMatRep
   (
   )  const
{
   const std::string label = this->pVscfCalcDef()->DumpModalsForMatRepFileLabel();
   if (this->pVscfCalcDef()->IoLevel() > 5)
   {
      Mout << "Vscf::DumpModalsForMatRep; Dumping modals to DataCont, label = "
         << label << std::endl;
   }
   MidasVector v(this->pModals()->Size());
   this->pModals()->DataIo(IO_GET, v, v.Size());
   DataCont dc(v, "OnDisc", label, true);
}
