/**
 ************************************************************************
 * 
 * @file                VscfRspTransformer.cc
 *
 * Created:             6-2-2008
 *
 * Author:              Ove Christiansen (ove@chem.au.dk)
 *
 * Short Description:   VSCF response transformer.
 * 
 * Last modified: Sun Jan 11, 2009  05:11PM
 * 
 * Detailed  Description: 
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */


#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "input/VscfCalcDef.h"
#include "input/OpDef.h"
#include "input/Input.h"
#include "util/Timer.h"
#include "vscf/Vscf.h"
#include "ni/OneModeInt.h"
#include "VscfRspTransformer.h"
#include "input/GlobalOperatorDefinitions.h"
#include "mmv/DataCont.h"

VscfRspTransformer::VscfRspTransformer(Vscf* apVscf, VscfCalcDef* apVscfCalcDef, OpDef* apOpDef):
   mpVscf(apVscf),
   mpVscfCalcDef(apVscfCalcDef),
   mpOpDef(apOpDef)
{}

/**
* Vscf Reponse transformer 
* * AC for NoPaired,
* (A B -B -A) for VSCF! Meaning that transform is actually S^(-1)E!!!!!
* Construct new operator, use integrals of existing. 
* */
void VscfRspTransformer::Transform(DataCont& arDcIn, DataCont& arDcOut, In aI, In aJ, Nb& aNb)
{
   if (aI==I_0) 
   {
      RspTransH0(arDcIn,arDcOut,aJ,aNb);
      return;
   }
   if (mpVscfCalcDef->RspIoLevel()>I_10)
      Mout << " In Vscf::RspTrans - begin transformation." << endl;

   Timer time_all;     
   //
   // First part:
   // Build up the transformed operator, [H,C]
   //
   string s_trans_lab= "$RspT_";
   In add=I_0;
   bool also_de_ex = !mpVscfCalcDef->DoNoPaired();
   OpDef new_op;
   new_op.SetType(mpOpDef->Type());
   new_op.SetIgnQiOnly(mpOpDef->IgnQiOnly());
   new_op.SetActTermAlg(true); 
   new_op.CopyOneModeOpers(*mpOpDef); 
   for (LocalModeNr i_modes= I_0;i_modes<mpOpDef->NmodesInOp();++i_modes) 
      new_op.AddMode(mpOpDef->GetGlobalModeNr(i_modes)); 
   new_op.PrepareActiveTermsVec(mpOpDef->NmodesInOp()); 

   if (!mpOpDef->UseActiveTermsAlgo()) 
       MIDASERROR("Rsp only implemented for act. term algo"); 
   mpVscf->AllOneModeOperNorms(); // Prepare for screening part
   Nb expt_size = C_1;

   In i_new_term=I_0;
   In i_modes_screened=I_0; 
   In i_term_screened  =I_0; 
   for (LocalModeNr i_op_mode=I_0;i_op_mode<mpVscf->mpOneModeInt->GetmNoModesInInt();i_op_mode++)
   {
      GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr i_bas_mode = mpVscf->mpBasDef->GetLocalModeNr(i_g_mode);
      In nbas       = mpVscf->mpBasDef->Nbas(i_bas_mode);
      In npar       = nbas-I_1;
      MidasVector c_ai(npar);
      MidasVector c_ia(npar);
      arDcIn.DataIo(IO_GET,c_ai,npar,add);
      if (also_de_ex) arDcIn.DataIo(IO_GET,c_ia,npar,add+mpVscf->NrspPar()/I_2);
      add+=npar;
      Nb c_ai_norm = c_ai.Norm();
      Nb c_ia_norm = C_0;
      if (also_de_ex) c_ia_norm = c_ia.Norm();
      if (gDebug || mpVscfCalcDef->RspIoLevel()>20)
      {
         Mout << "\n Trans Vec mode i_op_mode " << i_op_mode << endl;
         Mout << " c_ai norm =  " << c_ai_norm << endl;
         Mout << " c_ia norm =  " << c_ia_norm << endl;
      }
      // Loop over active terms for this mode combi.
      // Potential screening here, on c,h^m,the rest. 
      // Screen on possible size for each term.
      // prod. of Ccoef * c*h-active *norm of the rest. 
      //
      // Screen level 1:
      if (c_ai_norm < mpVscfCalcDef->ScreenZeroRspC() && 
          (!also_de_ex || c_ia_norm < mpVscfCalcDef->ScreenZeroRspC())) 
      {
         i_modes_screened++;
         continue;
      }
      //
      MidasMatrix h_in(nbas);
      MidasMatrix h_out(nbas);
      for (In i_act_term=I_0;i_act_term<mpOpDef->NactiveTerms(i_op_mode);i_act_term++)
      {
         In i_term = mpOpDef->TermActive(i_op_mode,i_act_term);
         Nb coef   = mpOpDef->Coef(i_term);
         if (fabs(coef) < mpVscfCalcDef->ScreenZeroCoef()) continue;
         In n_fac  = mpOpDef->NfactorsInTerm(i_term);

         // |Coef|*Product ||h||*|c| < ScreenZero. 
         Nb fac_screen = coef;
         for (In i_fac=I_0;i_fac<n_fac;i_fac++)
         {
            LocalModeNr i_op_mode2  = mpOpDef->ModeForFactor(i_term,i_fac);
            LocalOperNr i_oper      = mpOpDef->OperForFactor(i_term,i_fac);
            fac_screen *= mpVscf->mOneModeOperNorms[mpVscf->mOneModeIntOccOff[i_op_mode2]+i_oper];
         }
         fac_screen *= sqrt(c_ai_norm*c_ai_norm + c_ia_norm*c_ia_norm);
         //Mout << " fac_screen " << fac_screen << endl;
         if (fabs(fac_screen) < mpVscfCalcDef->ScreenZeroRsp()) 
         {
            i_term_screened++;
            continue;
         }


         In i_fac_for_mode = I_0;
         In i_fac_not_trans=I_0;
         for (In i_fac=0;i_fac<n_fac;i_fac++)
         {
            LocalModeNr i_op_mode2  = mpOpDef->ModeForFactor(i_term,i_fac);
            //Mout << " i_op_mode2 " << i_op_mode2 
            //     << " i_fac " << i_fac << endl;
            if (i_op_mode2 != i_op_mode) 
            {
               LocalOperNr i_oper     = mpOpDef->OperForFactor(i_term,i_fac);
               //Mout << " i_oper " << i_oper<< " i_fac " << i_fac << endl;
               new_op.AddModeOperProdToTerm(i_new_term,
                        gOperatorDefs.GetModeLabel(mpOpDef->GetGlobalModeNr(i_op_mode2)),i_oper,
                        (i_fac_not_trans==0));
               i_fac_not_trans++;
            }
            else i_fac_for_mode = i_fac;
            if (i_fac == I_0) new_op.SetNterms(i_new_term+I_1);
         }
         //for (In i=I_0;i<gOperLabels.size();i++)
            //Mout << i << " " << gOperLabels[i] << endl;
         LocalOperNr i_oper     = mpOpDef->OperForFactor(i_term,i_fac_for_mode);
         string s_op= s_trans_lab + std::to_string(i_oper);
         //Mout << " sop " << s_op << " " << i_oper << endl;
         //s_op = s_op + gOperLabels[i_oper];
         //new_op.AddToOneModeOpers(i_op_mode,s_op);
         //Mout << " i_oper for trans mode = " << i_oper;
         i_oper = new_op.AddRspOperToken(i_op_mode,i_oper);
         //i_oper = new_op.FindOrAddToOneModeOpers(i_op_mode,s_op);
         //Mout << " " << i_oper << endl;
         new_op.AddModeOperProdToTerm(i_new_term,
                  gOperatorDefs.GetModeLabel(mpOpDef->GetGlobalModeNr(i_op_mode)),i_oper,
                  (n_fac==I_1));
         if (n_fac == I_1) new_op.SetNterms(i_new_term+I_1);
         new_op.AddCoef(mpOpDef->Coef(i_term));
         i_new_term++;
      }
   }
   if (gDebug || mpVscfCalcDef->RspIoLevel()>I_3) 
   {
      Mout << " The number of modes screened =  " << i_modes_screened << endl;
      Mout << " The number of terms screened =  " << i_term_screened << endl;
      Mout << " The number of non-zero terms =  " << i_new_term << endl;
   }
   // CHECK THIS, ARE ACTIVE TERMS ALGO OK THEN????
   //new_op.Reorganize(); // Avoid reorg, if OneModeOpers are copied

   //
   // Output the transformed operator 
   //
   if (mpVscfCalcDef->RspIoLevel()>I_15)
   {
      Mout << " The transformed operator " << endl;
      Out72Char(Mout,'+','=','+');
      Mout << " Mode m | m Label    | Oper. nr.  | Operator description " << endl;
      Out72Char(Mout,'+','=','+');
      for (LocalModeNr i_op_mode=0;i_op_mode<new_op.NmodesInOp();i_op_mode++)
      {
         GlobalModeNr i_mode = new_op.GetGlobalModeNr(i_op_mode);
         for (In i_op=0;i_op<new_op.NrOneModeOpers(i_op_mode);i_op++)
         {
             Mout.width(7);
             Mout << i_mode << " | ";
             Mout.width(10);
             Mout << gOperatorDefs.GetModeLabel(i_mode) << " | ";
             Mout.width(10);
             Mout << i_op << " | ";
             Mout << gOperatorDefs.GetOperLabel(new_op.GetOneModeOperGlobalNr(i_op_mode,i_op)) << endl;
         }
         if (i_op_mode != new_op.NmodesInOp()-1) Out72Char(Mout,'-','-','-');
      }
      Out72Char(Mout,'+','=','+');
      Mout << " The number of terms is: " << new_op.Nterms() << endl;
      //if (gVibIoLevel > 5)
      {
         for (In i_term=0;i_term<new_op.Nterms();i_term++)
         {
             Mout << new_op.Coef(i_term); 
             Mout << "*" << new_op.GetOperProd(i_term) << endl;
         }
      }
   }
   if (mpVscfCalcDef->TimeIt()) time_all.CpuOut(Mout,"\n CPU  time used in Creating OIT H: ");

   //
   // Use the transformed operator to make transformation 
   //
   add=I_0;
   //
   In n_sc_0 = I_0; 
   In n_sc_1 = I_0; 
   In n_sc_2 = I_0; 
   In n_sc_3 = I_0; 
   In n_ca_1 = I_0; 
   In n_ca_2 = I_0; 
   In n_ca_0 = I_0; 
   for (LocalModeNr i_op_mode=0;i_op_mode<mpVscf->mpOneModeInt->GetmNoModesInInt();i_op_mode++)
   {
      GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr i_bas_mode = mpVscf->mpBasDef->GetLocalModeNr(i_g_mode);
      In nbas       = mpVscf->mpBasDef->Nbas(i_bas_mode);
      In npar       = nbas-I_1;
      MidasVector res_ai(npar,C_0);
      MidasVector res_ia(npar,C_0);
      MidasMatrix fock(nbas);
      fock.Zero();

      //Mout << " Mode under way " << i_op_mode << endl;
      //Mout << " Nr of active terms " << new_op.NactiveTerms(i_op_mode) << endl;
      for (In i_act_term=0;i_act_term<new_op.NactiveTerms(i_op_mode);i_act_term++)
      {
         In i_term = new_op.TermActive(i_op_mode,i_act_term);
         {
            Nb fact = new_op.Coef(i_term);
            if (fabs(fact) < mpVscfCalcDef->ScreenZeroCoef()) 
            {
               n_sc_1++;
               continue;
            }
      
            In i_fac_thismode = -1;
            //if (gDebug) Mout << " factors " 
               //<< new_op.NfactorsInTerm(i_term) << " in term " << i_term << endl; 
            In i_fac_trans=-I_1;
            for (In i_fac=0;i_fac<new_op.NfactorsInTerm(i_term);i_fac++)
            {
               LocalModeNr i_op_mode2  = new_op.ModeForFactor(i_term,i_fac);
               if (i_op_mode2 == i_op_mode) 
               {
                  i_fac_thismode = i_fac;
               }
               else
               {
                  LocalOperNr i_oper      = new_op.OperForFactor(i_term,i_fac);
                  //string s_op    = gOperatorDefs.GetOperLabel(new_op.GetOneModeOperGlobalNr(i_op_mode2,i_oper));
                  //bool trans_op = (s_op.find(s_trans_lab)!=s_op.npos);
                  //if (!trans_op)
                  if(-I_1 == gOperatorDefs.GetRspOperToken(new_op.GetOneModeOperGlobalNr(i_op_mode2,i_oper)))
                  {
                     fact *= mpVscf->mOneModeIntOcc[mpVscf->mOneModeIntOccOff[i_op_mode2]+i_oper];
                  }
                  else
                  {
                     i_fac_trans=i_fac;
                  }
               }
            }
   
            //Found transformed operator - now do it actually now. 
            if (i_fac_trans!=-I_1)
            {
               LocalModeNr i_op_mode2  = new_op.ModeForFactor(i_term,i_fac_trans);
               if (!(i_op_mode2 == i_op_mode))
               {
                  LocalOperNr i_oper      = new_op.OperForFactor(i_term,i_fac_trans);
                  //string s_op    = gOperatorDefs.GetOperLabel(new_op.GetOneModeOperGlobalNr(i_op_mode2,i_oper));
                  //s_op.erase(I_0,s_trans_lab.size());
                  //i_oper = InFromString(s_op);
                  i_oper = gOperatorDefs.GetRspOperToken(new_op.GetOneModeOperGlobalNr(i_op_mode2,i_oper));
                  // Mout << " new s_op " << s_op 
                       // << " i_oper " << i_oper << endl;
                  GlobalModeNr i_g_mode2  = mpOpDef->GetGlobalModeNr(i_op_mode2);
                  LocalModeNr i_bas_mode2 = mpVscf->mpBasDef->GetLocalModeNr(i_g_mode2);
                  In nbas2       = mpVscf->mpBasDef->Nbas(i_bas_mode2);
                  In npar2       = nbas2-I_1;
                  MidasVector c_ai(npar2);
                  MidasVector c_ia(npar2);
                  In add2=I_0;
                  LocalModeNr i_op_mode3=I_0;
                  while(i_op_mode3<mpVscf->mpOneModeInt->GetmNoModesInInt()
                      &&i_op_mode3<i_op_mode2)
                  {
                     GlobalModeNr i_g_mode_3   = mpOpDef->GetGlobalModeNr(i_op_mode3);
                     LocalModeNr i_bas_mode_3 = mpVscf->mpBasDef->GetLocalModeNr(i_g_mode_3);
                     In nparty     = (mpVscf->mpBasDef->Nbas(i_bas_mode_3)-I_1);
                     i_op_mode3++;
                     add2+=nparty;
                  }
                  //Mout << " i_op_mode2 = " << i_op_mode2 << " add2 " << add2 <<endl;
                  arDcIn.DataIo(IO_GET,c_ai,npar2,add2);
                  if (also_de_ex) arDcIn.DataIo(IO_GET,c_ia,npar2,add2+mpVscf->NrspPar()/I_2);

                  Nb cont_size_est = fabs(fact)*mpVscf->mOneModeOperNorms[
                                                        mpVscf->mOneModeIntOccOff[i_op_mode2]
                                                        +i_oper];
                  Nb c_ai_norm = c_ai.Norm();
                  Nb c_ia_norm = C_0;
                  if (also_de_ex) c_ia_norm = c_ia.Norm();
                  cont_size_est *= sqrt(c_ai_norm*c_ai_norm + c_ia_norm*c_ia_norm);
                  LocalOperNr i_oper_thismode     = new_op.OperForFactor(i_term,i_fac_thismode);
                  cont_size_est *= mpVscf->mOneModeOperNorms[mpVscf->mOneModeIntOccOff[i_op_mode]
                                                             +i_oper_thismode];
                  //Mout << " fact " << fact << " cont_size_est " << cont_size_est << endl;
                  if ((cont_size_est/expt_size) < mpVscfCalcDef->ScreenZeroRsp()) 
                  {
                     //Mout << " Screened something " << endl;
                     n_sc_0++;
                     //continue;
                  }
                  else
                  {
                     MidasMatrix hint(nbas2);
                     mpVscf->mpOneModeInt->GetInt(i_op_mode2,i_oper,hint);
                     //Mout << " Norm before " << hint.Norm() << endl;
                     mpVscf->TransInt(hint,i_op_mode2);
                     //Mout << " Norm after " << hint.Norm() << endl;
                     Nb h_tilde_ii=C_0;
                     for (In b=I_0;b<npar2;b++)
                     {
                        h_tilde_ii += hint[I_0][b+I_1]*c_ai[b];
                        if (also_de_ex) 
                           h_tilde_ii -= hint[b+I_1][I_0]*c_ia[b];
                     }
                     fact *= h_tilde_ii;
                     n_ca_0++;
                  }
               }
            }
            //Mout << " i_op_mode " << i_op_mode << " i_term " << i_term << " fact " << fact << endl;


            if (i_fac_thismode >-1) 
            {
               //Mout << " fock before " << endl << fock << endl;
               LocalOperNr i_oper     = new_op.OperForFactor(i_term,i_fac_thismode);
               //string s_op   = gOperatorDefs.GetOperLabel(new_op.GetOneModeOperGlobalNr(i_op_mode,i_oper));
               //bool trans_op = (s_op.find(s_trans_lab)!=s_op.npos);
               if (-I_1 == gOperatorDefs.GetRspOperToken(new_op.GetOneModeOperGlobalNr(i_op_mode,i_oper)))
               {
                  Nb cont_size_est = fabs(fact)* mpVscf->mOneModeOperNorms[
                                                         mpVscf->mOneModeIntOccOff[i_op_mode]
                                                         +i_oper];
                  //Mout << " fact " << fact << " cont_size_est " << cont_size_est << endl;
                  if ((cont_size_est/expt_size) < mpVscfCalcDef->ScreenZeroRsp()) 
                  {
                     //Mout << " Screened something " << endl;
                     n_sc_2++;
                     continue;
                  }
                  n_ca_1++;
                  
                  // += Fact-tilde * h
                  mpVscf->mpOneModeInt->AddIntTo(fock,i_op_mode,i_oper,fact); 
                  //if (gDebug) 
                     // Mout << "Vscf::RspTrans: " << setw(10) << s_op
                     // << "i_op " << i_oper << " i_term " << i_term << " fact: " << fact 
                     // << " mode " << i_op_mode << endl;
                     // Mout << " fock now \n" << fock << endl;
               }
               else
               {
                  // +=  h-tilde * fact
                  MidasVector c_ai(npar);
                  MidasVector c_ia(npar);
                  arDcIn.DataIo(IO_GET,c_ai,npar,add);
                  if (also_de_ex) arDcIn.DataIo(IO_GET,c_ia,npar,add+mpVscf->NrspPar()/I_2);
                  //s_op.erase(I_0,s_trans_lab.size());
                  //i_oper = InFromString(s_op);
                  i_oper = gOperatorDefs.GetRspOperToken(new_op.GetOneModeOperGlobalNr(i_op_mode,i_oper));
                  // Another screening...
                  bool screen=true;
                  if (screen)
                  {
                     Nb cont_size_est = fabs(fact)*mpVscf->mOneModeOperNorms[
                                                           mpVscf->mOneModeIntOccOff[i_op_mode]
                                                           +i_oper];
                     Nb c_ai_norm = c_ai.Norm();
                     Nb c_ia_norm = C_0;
                     if (also_de_ex) c_ia_norm = c_ia.Norm();
                     cont_size_est *= sqrt(c_ai_norm*c_ai_norm + c_ia_norm*c_ia_norm);
                     if ((cont_size_est/expt_size) < mpVscfCalcDef->ScreenZeroRsp()) 
                     {
                        n_sc_3++;
                        continue;
                     }
                  }
                  n_ca_2++;
                  // Mout << " new s_op " << s_op << " i_oper " << i_oper << endl;
                  MidasMatrix hint(nbas);
                  mpVscf->mpOneModeInt->GetInt(i_op_mode,i_oper,hint);
                  mpVscf->TransInt(hint,i_op_mode);
                  //Mout << " hint\n" << hint  << endl;
                  for (In a=I_0;a<npar;a++)
                  {
                     for (In b=I_0;b<npar;b++)
                        res_ai[a] += hint[a+I_1][b+I_1]*c_ai[b]*fact;
                     //Mout << " res_ai 1 "  << res_ai << endl;
                     res_ai[a] -= hint[I_0][I_0]*c_ai[a]*fact;
                     //Mout << " res_ai 2 "  << res_ai << endl;
                     if (also_de_ex)
                     {
                        for (In b=I_0;b<npar;b++)
                           res_ia[a] -= hint[b+I_1][a+I_1]*c_ia[b]*fact;
                        res_ia[a] += hint[I_0][I_0]*c_ia[a]*fact;
                           // res_ia[a] += hint[b+I_1][a+I_1]*c_ia[b]*fact;
                        // res_ia[a] -= hint[I_0][I_0]*c_ia[a]*fact;
                        //Mout << " also_de_ex res_ai 3 "  << res_ai << endl;
                     }
                  }
               }
            }
         }
      }
      //Mout << " Norm of fock bef trans " << fock.Norm() << endl;
      mpVscf->TransInt(fock,i_op_mode);
      //Mout << "fock\n" << fock << endl;
      //Mout << " Norm of fock aft trans " << fock.Norm() << endl;
      //if (gDebug || mpVscfCalcDef->RspIoLevel()>10)
      //{
         //Mout << " res_ai-1 norm =  " << res_ai.Norm() << endl;
         //Mout << " res_ia-1 norm =  " << res_ia.Norm() << endl;
         //Mout << " res_ai-1 \n " << res_ai << endl;
         //Mout << " res_ia-1 \n " << res_ia << endl;
      //}
      //for (In a=I_0;a<npar;a++) res_ai[a] += fock[I_0][a+I_1];
      //if (also_de_ex) for (In a=I_0;a<npar;a++) res_ia[a] += fock[a+I_1][I_0];
      for (In a=I_0;a<npar;a++) res_ai[a] += fock[a+I_1][I_0];
      //if (also_de_ex) for (In a=I_0;a<npar;a++) res_ia[a] -= fock[I_0][a+I_1];
      if (also_de_ex) for (In a=I_0;a<npar;a++) res_ia[a] += fock[I_0][a+I_1];
      //Mout << " Norm of res_ai " << res_ai.Norm() << endl;
      //Mout << " Norm of res_ia " << res_ia.Norm() << endl;
      arDcOut.DataIo(IO_PUT,res_ai,npar,add);
      if (also_de_ex) arDcOut.DataIo(IO_PUT,res_ia,npar,add+mpVscf->NrspPar()/I_2);
      add+=npar;
      if (gDebug || mpVscfCalcDef->RspIoLevel()>10)
      {
         Mout << "\n Trans Vec mode i_op_mode " << i_op_mode;
         Mout << " res_ai norm =  " << res_ai.Norm() << endl;
         Mout << " res_ia norm =  " << res_ia.Norm() << endl;
         //Mout << " res_ai \n " << res_ai << endl;
         //Mout << " res_ia \n " << res_ia << endl;
      }
   }
   if (mpVscfCalcDef->RspIoLevel()>I_10) Mout 
       << " In Vscf::RspTrans - end transformation " << endl;
   if (mpVscfCalcDef->TimeIt()) time_all.CpuOut(Mout,"\n CPU  time used in transforming OIT H: ");
   if (gDebug || mpVscfCalcDef->RspIoLevel()>=I_3) 
   {
      Mout << " 2 part The number of terms screened 0 =  " << n_sc_0 << endl;
      Mout << " 2 part The number of terms screened 1 =  " << n_sc_1 << endl;
      Mout << " 2 part The number of terms screened 2 =  " << n_sc_2 << endl;
      Mout << " 2 part The number of terms screened 3 =  " << n_sc_3 << endl;
      Mout << " 2 part The number of terms calc. 0    =  " << n_ca_0 << endl;
      Mout << " 2 part The number of terms calc. 1    =  " << n_ca_1 << endl;
      Mout << " 2 part The number of terms calc. 2    =  " << n_ca_2 << endl;
   }
}


void VscfRspTransformer::PreparePreDiag()
{ }

void VscfRspTransformer::PreDiagTransform(DataCont& aIn, DataCont& aOut, In aI, In aJ, Nb& aNb)
{ }

void VscfRspTransformer::RestorePreDiag()
{ }

/**
* Vscf Reponse H0 transformer 
* */
void VscfRspTransformer::RspTransH0(DataCont& arDcIn,DataCont& arDcOut, In aJ, Nb& aNb)
{
   if (mpVscfCalcDef->RspIoLevel()>I_10) Mout << " In Vscf::RspTransH0 - begin transformation " << endl;
   if (gDebug || mpVscfCalcDef->IoLevel() > I_10) 
   {
      if (aJ > I_0 ) Mout << " transform by -H0 " << endl;
      if (aJ < I_0 && -I_2 < aJ) Mout << " transform by -(H0-E^0)-1 " << endl;
      if (aJ == I_1|| aJ == -I_1) Mout << " Result is assigned to Trans vec " << endl;
      // Not imp. if (aJ == I_2|| aJ == -I_2) Mout << " Result is subtracted from Trans vec " << endl;
      if (aJ < -I_2 ) Mout << " transform by -(H0-E)^-1, E = " << aNb << endl;
   }
   bool also_de_ex = !mpVscfCalcDef->DoNoPaired();
   In add=I_0;
   for (LocalModeNr i_op_mode=0;i_op_mode<mpVscf->mpOneModeInt->GetmNoModesInInt();i_op_mode++)
   {
      GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr i_bas_mode = mpVscf->mpBasDef->GetLocalModeNr(i_g_mode);
      In nbas       = mpVscf->mpBasDef->Nbas(i_bas_mode);
      In npar       = nbas-I_1;
      MidasVector c_ai(npar);
      MidasVector c_ia(npar);
      arDcIn.DataIo(IO_GET,c_ai,npar,add);
      if (also_de_ex) arDcIn.DataIo(IO_GET,c_ia,npar,add+mpVscf->NrspPar()/I_2);

      MidasVector res_ai(npar);
      MidasVector res_ia(npar);

      MidasVector eig_val_ai(npar); 
      MidasVector eig_val_ia(npar); 
      if (npar>I_0) mpVscf->mEigVal.DataIo(IO_GET,eig_val_ai,npar,
                                           mpVscf->OccModalOffSet(i_op_mode)+I_1);
      //Mout << " subtract aNb = " << aNb << endl;
      for (In i=I_0;i<npar;i++) eig_val_ia[i] = eig_val_ai[i]+aNb;
      for (In i=I_0;i<npar;i++) eig_val_ai[i] = -eig_val_ai[i]+aNb;
      // 0 followed by npar eigenvalues. 

      if (aJ==I_1)
      {
         for (In i=I_0;i<npar;i++) res_ai[i] = c_ai[i]*eig_val_ai[i];
         if (also_de_ex) for (In i=I_0;i<npar;i++) res_ia[i] = c_ia[i]*eig_val_ia[i];
      }
      else if (aJ==-I_1)
      {
         for (In i=I_0;i<npar;i++) res_ai[i] = c_ai[i]/eig_val_ai[i];
         if (also_de_ex) for (In i=I_0;i<npar;i++) res_ia[i] = c_ia[i]/eig_val_ia[i];
      }


      arDcOut.DataIo(IO_PUT,res_ai,npar,add);
      if (also_de_ex) arDcOut.DataIo(IO_PUT,res_ia,npar,add+mpVscf->NrspPar()/I_2);
      add+=npar;
   }
}
/**
* Vscf Reponse H0 transformer 
* */
void VscfRspTransformer::CheckPurifies(DataCont& arDcIn)
{ 
   Mout << "Purifications not implmented for VscfRspTransformer" << endl; 
}

/**
 * 
 **/
In VscfRspTransformer::VectorDimension() const
{
   return mpVscf->NrspPar();
}

/**
 * 
 **/
Transformer* VscfRspTransformer::Clone() const
{
   return new VscfRspTransformer(this);
}

/**
 * 
 **/
Transformer* VscfRspTransformer::CloneImprovedPrecon(In aLevel) const
{
   MIDASERROR("CloneImprovedPrecon(): not implemented for VscrRspTransformer. \n If you want to use this function, implemented it!");
   return new VscfRspTransformer(nullptr,nullptr,nullptr); /* will never reach here */
}
