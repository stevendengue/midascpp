/**
************************************************************************
* 
* @file                VscfRspDrv.cc
*
* Created:             04-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing Vscf Rsp Drive methods.
* 
* Last modified: Sun May 16, 2010  03:12PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<iostream>
#include<set>
#include<string>

// midas headers
#include "inc_gen/math_link.h" 
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/RspCalcDef.h"
#include "input/VscfCalcDef.h"
#include "input/OpDef.h"
#include "input/BasDef.h"
#include "util/Io.h"
#include "util/Timer.h"
#include "Vscf.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "mmv/DataCont.h"
#include "mmv/ItEqSol.h"
#include "ni/OneModeInt.h"
#include "input/RspVecInf.h"
#include "input/TotalRspFunc.h"
#include "util/MultiIndex.h"
#include "VscfRspTransformer.h"
#include "util/Plot.h"
#include "IRData.h"
#include "RamanData.h"
#include "vcc/LanczosRspFunc.h"
#include "input/Contribution.h"
#include "input/GlobalOperatorDefinitions.h"
#include "it_solver/LinearEquationInterface.h"
#include "it_solver/ComplexVector.h"
#include "analysis/ResponseAnalysisInterface.h"

#include "libmda/numeric/float_eq.h"

// using declarations
using std::set;

/**
* Drive calculation of response propertes
* */
void Vscf::RspDrv()
{
   Mout << "\n\n";
   Out72Char(Mout,'+','-','+');
   Out72Char(Mout,'|',' ','|');
   OneArgOut72(Mout," Response calculations ",'|');
   Out72Char(Mout,'|',' ','|');
   Out72Char(Mout,'+','-','+');
   Mout << "\n\n";

   // Set up number of params etc.
   PrepareNrspPar(); 
   In nvecsize = NrspPar();
   Mout << " Reference state name " << mpVscfCalcDef->Name() << endl
        << " Dimension of Response space " << nvecsize << endl;

   // If method is non-variational and transition moments are requested,
   // calculate left-hand eigenvectors and eigenvalues.
   bool lefteig = mpVscfCalcDef->RspLeftEig();
   if (!lefteig && !mpVscfCalcDef->Variational())
   {
      for (In i=I_0; i<mpVscfCalcDef->NrspFuncs(); ++i)
      {
         if ( (mpVscfCalcDef->GetRspFunc(i)).GetNorder() == -I_1)
         {
            mpVscfCalcDef->SetRspLeftEig(true);
         }
      }
   }
   
   if (mpVscfCalcDef->DoRspEig())
   {
      switch(mpVscfCalcDef->RspEigType())
      {
         case RSPEIGTYPE::STD:
         {
            if(mpVscfCalcDef->RspLeftEig())
            {
               RspEigDrv();
               RspEigDrv(false);
            }
            else
            {
               RspEigDrv();
            }
            break;
         }
         case RSPEIGTYPE::TENSOR:
         {
            this->TensorRspEigDrv(true);

            if (  mpVscfCalcDef->RspLeftEig()
               )
            {
               this->TensorRspEigDrv(false);
            }

            break;
         }
         default:
         {
            MIDASERROR("WRONG RSPEIGTYPE");
         }
      }
   }
   if (  mpVscfCalcDef->DoRspFuncs()
      || mpVscfCalcDef->DoDensityMatrixAnalysis()
      )
   {
      switch   (  mpVscfCalcDef->RspEigType()
               )
      {
         case RSPEIGTYPE::STD:
         {
            this->RspFuncDrv<DataCont>(); 
            break;
         }
         case RSPEIGTYPE::TENSOR:
         {
            this->RspFuncDrv<TensorDataCont>();
            break;
         }
         default:
         {
            MIDASERROR("WRONG RSPEIGTYPE");
         }
      }
   }
   if (  mpVscfCalcDef->NlanczosRspFuncs() != I_0
      )
   {
      this->LanczosChainRspDrv();
   }

   if(mpVscfCalcDef->GetDoTotalResponse()) 
   {
      Mout << "I will do total response for " << mpVscfCalcDef->GetDoTotalResponse()
           << " response functions." << endl;
      // compile list of all the response information we have
      // present
      // 1) "Normal" response functions
      set<Contribution> total_set;
      for (In i=I_0; i<mpVscfCalcDef->NrspFuncs(); ++i)
      {
         if(mpVscfCalcDef->GetRspFunc(i).GetNorder() < I_0)
            continue;
         total_set.insert((mpVscfCalcDef->GetRspFunc(i)).ToContribution());
         if(gIoLevel>I_5)
         {
            Mout << "Inserted: " << mpVscfCalcDef->GetRspFunc(i) << endl;
            Mout << "And it is: " << endl << (mpVscfCalcDef->GetRspFunc(i)).ToContribution() << endl;
         }
      }
      // 2) Lanczos response functions
      for (In i=I_0; i<mpVscfCalcDef->NlanczosRspFuncs(); ++i) 
      {
         vector<RspFunc> rsp=mpVscfCalcDef->GetLanczosRspFunc(i)->ConvertToRspFunc();
         for(In j=0;j<rsp.size();j++) 
         {
            total_set.insert(rsp[j].ToContribution());
         }
      }
      if(gIoLevel>I_5) 
      {
         Mout << "TOTAL SET IS: " << endl;
         for(set<Contribution>::iterator it=total_set.begin();it!=total_set.end();it++)
            Mout << *it << endl;
      }
      // now evaluate the total response functions
      for(In i=0;i<mpVscfCalcDef->GetDoTotalResponse();i++) 
      {
         if(gIoLevel>I_5) {
            Mout << "Will do total response function: " << endl << endl
                 << *(mpVscfCalcDef->GetTotalResponseDrv(i)) << endl;
         }
         mpVscfCalcDef->GetTotalResponseDrv(i)->SetBasicSet(total_set);
         mpVscfCalcDef->GetTotalResponseDrv(i)->Evaluate();
      }
   }
   

   Out72Char(Mout,'+','-','+');
   Out72Char(Mout,'|',' ','|');
   OneArgOut72(Mout," All response calculations are done ",'|');
   Out72Char(Mout,'|',' ','|');
   Out72Char(Mout,'+','-','+');
}

/**
* The number of VSCF response parameters. 
* */
void Vscf::PrepareNrspPar()
{
   In n_rsp_par_mode = I_0;
   for (LocalModeNr i_op_mode=0;i_op_mode<mpOneModeInt->GetmNoModesInInt();i_op_mode++)
   {
      GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
      In nbas       = mpBasDef->Nbas(i_bas_mode);
      n_rsp_par_mode += nbas-I_1;
   }
   mNrspParHalf = n_rsp_par_mode;
   if (!mpVscfCalcDef->DoNoPaired()) n_rsp_par_mode *=I_2;
   mNrspPar     = n_rsp_par_mode;
}

/**
* Add the response equations to solve for this particular
* response function.
* */
void Vscf::AddRspVecToSet(set<RspVecInf>& arRspVecInfSet,const RspFunc& arRspFunc)
{
   In n_order = arRspFunc.GetNorder();
   bool var = mpVscfCalcDef->Variational();
   bool qrf_first_time=true;
   if (!var)
   {
      RspVecInf newrsp(I_0,false);
      arRspVecInfSet.insert(newrsp);
   }
   if (n_order > I_1)
   {
      for (In i_ord=I_0;i_ord<n_order;i_ord++)
      {  
         // Unless no2np2 vcc rsp.
         if ((!(var && n_order == I_2 && i_ord == I_0))
               && (!(!var && n_order==I_2 && arRspFunc.NoTwoNPlusTwo()&&i_ord==I_0))) 
         {
            if (mpVscfCalcDef->RspIoLevel()>I_10)
               Mout << " Add first order parameters for rsp func " << arRspFunc << endl;
            RspVecInf newrsp(I_1,true);
            newrsp.SetOp(arRspFunc.GetRspOp(i_ord),I_0);
            newrsp.SetFrq(arRspFunc.GetRspFrq(i_ord),I_0);
            newrsp.SetGamma(libmda::numeric::float_neg(arRspFunc.GetRspFrq(i_ord)) ? -fabs(arRspFunc.Gamma()) : fabs(arRspFunc.Gamma()));
            //newrsp.SetGamma(arRspFunc.Gamma());
            //Mout << newrsp << endl;
            //std::cout << newrsp << endl;
            arRspVecInfSet.insert(newrsp);
            if (mpVscfCalcDef->RspIoLevel()>I_10) Mout << " Add " << newrsp << endl;
            if (mpVscfCalcDef->DoNoPaired())
            {
               // insert negative frequency vector
               //Mout << " Inserting negative " << std::endl;
               RspVecInf newrsp(I_1,true);
               newrsp.SetOp(arRspFunc.GetRspOp(i_ord),I_0);
               newrsp.SetFrq(-arRspFunc.GetRspFrq(i_ord),I_0);
               //newrsp.SetGamma(-arRspFunc.Gamma());
               newrsp.SetGamma(libmda::numeric::float_neg(-arRspFunc.GetRspFrq(i_ord)) ? -fabs(arRspFunc.Gamma()) : fabs(arRspFunc.Gamma()));
               //Mout << newrsp << endl;
               //std::cout << newrsp << endl;
               arRspVecInfSet.insert(newrsp);
               if (mpVscfCalcDef->RspIoLevel()>I_10) Mout << " Add " << newrsp << endl;
            }
         }
         //mbh: in case of order = 3 => QRF. if variational then we
         //     need to solve some 1st order response equations, add
         //     them to the set
         if (var && n_order == I_3 && qrf_first_time) 
         {
            qrf_first_time=false;
            // first add set of sigma vectors
            for(In i_sigma=I_0;i_sigma<I_3;i_sigma++) 
            {
               RspVecInf newrsp(I_2,true);
               newrsp.SetOp(arRspFunc.GetRspOp(i_sigma),I_0);
               if (i_sigma==I_0) 
               {
                  newrsp.SetOp(arRspFunc.GetRspOp(I_1),I_1);
                  newrsp.SetFrq(arRspFunc.GetRspFrq(I_1),I_1);
                  newrsp.SetGamma(arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  newrsp.SetOp(arRspFunc.GetRspOp(I_2),I_1);
                  newrsp.SetFrq(arRspFunc.GetRspFrq(I_2),I_1);
                  newrsp.SetGamma(arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  // Also negative frq.
                  newrsp.SetOp(arRspFunc.GetRspOp(I_1),I_1);
                  newrsp.SetFrq(-arRspFunc.GetRspFrq(I_1),I_1);
                  newrsp.SetGamma(-arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  newrsp.SetOp(arRspFunc.GetRspOp(I_2),I_1);
                  newrsp.SetFrq(-arRspFunc.GetRspFrq(I_2),I_1);
                  newrsp.SetGamma(-arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
               }
               else if (i_sigma==I_1) 
               {
                  newrsp.SetOp(arRspFunc.GetRspOp(I_0),I_1);
                  newrsp.SetFrq(arRspFunc.GetRspFrq(I_0),I_1);
                  newrsp.SetGamma(arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  newrsp.SetOp(arRspFunc.GetRspOp(I_2),I_1);
                  newrsp.SetFrq(arRspFunc.GetRspFrq(I_2),I_1);
                  newrsp.SetGamma(arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  // Also negative frq.
                  newrsp.SetOp(arRspFunc.GetRspOp(I_0),I_1);
                  newrsp.SetFrq(-arRspFunc.GetRspFrq(I_0),I_1);
                  newrsp.SetGamma(-arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  newrsp.SetOp(arRspFunc.GetRspOp(I_2),I_1);
                  newrsp.SetFrq(-arRspFunc.GetRspFrq(I_2),I_1);
                  newrsp.SetGamma(-arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
               }
               if (i_sigma==I_2) 
               {
                  newrsp.SetOp(arRspFunc.GetRspOp(I_0),I_1);
                  newrsp.SetFrq(arRspFunc.GetRspFrq(I_0),I_1);
                  newrsp.SetGamma(arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  newrsp.SetOp(arRspFunc.GetRspOp(I_1),I_1);
                  newrsp.SetFrq(arRspFunc.GetRspFrq(I_1),I_1);
                  newrsp.SetGamma(arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  // Also negative frq.
                  newrsp.SetOp(arRspFunc.GetRspOp(I_0),I_1);
                  newrsp.SetFrq(-arRspFunc.GetRspFrq(I_0),I_1);
                  newrsp.SetGamma(-arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
                  newrsp.SetOp(arRspFunc.GetRspOp(I_1),I_1);
                  newrsp.SetFrq(-arRspFunc.GetRspFrq(I_1),I_1);
                  newrsp.SetGamma(-arRspFunc.Gamma());
                  arRspVecInfSet.insert(newrsp);
               }
            }
            // now add a number of 1st order RF. No need to check
            // for SHG as the set will only add unique entries
            for(In i_qrf=I_0;i_qrf<I_3;i_qrf++) 
            {
               RspVecInf newrsp(I_1,true);
               newrsp.SetOp(arRspFunc.GetRspOp(i_qrf),I_0);
               newrsp.SetFrq(arRspFunc.GetRspFrq(i_qrf),I_0);
               newrsp.SetGamma(arRspFunc.Gamma());
               arRspVecInfSet.insert(newrsp);
               // also neq. frq.
               newrsp.SetOp(arRspFunc.GetRspOp(i_qrf),I_0);
               newrsp.SetFrq(-arRspFunc.GetRspFrq(i_qrf),I_0);
               newrsp.SetGamma(-arRspFunc.Gamma());
               arRspVecInfSet.insert(newrsp);
            }
         }
         if (var && n_order==I_4) {
            // cubic response, add second order rsp vectors
            RspVecInf newrsp(I_2,true,true);
            vector<pair<string,Nb> > oper_frq_vec;
            oper_frq_vec.push_back(make_pair(arRspFunc.GetRspOp(I_0),arRspFunc.GetRspFrq(I_0)));
            oper_frq_vec.push_back(make_pair(arRspFunc.GetRspOp(I_1),arRspFunc.GetRspFrq(I_1)));
            oper_frq_vec.push_back(make_pair(arRspFunc.GetRspOp(I_2),arRspFunc.GetRspFrq(I_2)));
            oper_frq_vec.push_back(make_pair(arRspFunc.GetRspOp(I_3),arRspFunc.GetRspFrq(I_3)));
            sort(oper_frq_vec.begin(),oper_frq_vec.end());
            do {
               newrsp.SetOp(oper_frq_vec[0].first,I_0);
               newrsp.SetOp(oper_frq_vec[1].first,I_1);
               newrsp.SetFrq(oper_frq_vec[0].second,I_0);
               newrsp.SetFrq(oper_frq_vec[1].second,I_1);
               newrsp.SetGamma(arRspFunc.Gamma());
               newrsp.SetNonStandardType("SECOND_ORDER_RSP_VEC");
               arRspVecInfSet.insert(newrsp);
               // also negative frq. (~ to de-ex. part)
               newrsp.SetFrq(-oper_frq_vec[0].second,I_0);
               newrsp.SetFrq(-oper_frq_vec[1].second,I_1);
               newrsp.SetGamma(-arRspFunc.Gamma());
               arRspVecInfSet.insert(newrsp);
            } while(next_permutation(oper_frq_vec.begin(),oper_frq_vec.end()));
            // Now add all linear equations
            RspVecInf linvecinf(I_1,true);
            for(In icr=I_0;icr<4;icr++) {
               linvecinf.SetOp(oper_frq_vec[icr].first,I_0);
               linvecinf.SetFrq(oper_frq_vec[icr].second,I_0);
               arRspVecInfSet.insert(linvecinf);
               // also de-ex.
               linvecinf.SetFrq(-oper_frq_vec[icr].second,I_0);
               arRspVecInfSet.insert(linvecinf);
            }
         }
         if (!var && (n_order > I_2 || (n_order>I_1 && arRspFunc.NoTwoNPlusTwo())))
         {
            if (mpVscfCalcDef->RspIoLevel()>I_10)
               Mout << " Add first order multipliers for rsp func " << arRspFunc << endl;
            RspVecInf newrsp(I_1,true);
            newrsp.SetOp(arRspFunc.GetRspOp(i_ord),I_0);
            newrsp.SetFrq(arRspFunc.GetRspFrq(i_ord),I_0);
            newrsp.SetGamma(arRspFunc.Gamma());
            newrsp.SetRight(false); 
            arRspVecInfSet.insert(newrsp);
            if (mpVscfCalcDef->RspIoLevel()>I_10)
               Mout << " Add " << newrsp << endl;
         }
         // Actually there will be more equations for non-linear response but
         // this is neglected for the moment....
      }
   }
   if (n_order == -I_11) 
   {
      RspVecInf newrsp(n_order,true);
      // Need to set some things in order to add it 
      // to the set due to the compare function
      // OP and L state should do the trick
      newrsp.SetOp(arRspFunc.GetRspOp(I_0),I_0);
      newrsp.SetLeftState(arRspFunc.LeftState());
      arRspVecInfSet.insert(newrsp);
   }
   if (!var && n_order==I_1 && arRspFunc.NoTwoNPlusTwo())
   {
      RspVecInf newrsp(I_1, true);
      newrsp.SetOp(arRspFunc.GetRspOp(I_0),I_0);
      newrsp.SetFrq(C_0,I_0);
      newrsp.SetGamma(arRspFunc.Gamma());
      arRspVecInfSet.insert(newrsp);
   }
   if (!var && n_order==-I_1)
   {
      // Make sure that first order responses for frequencies corresponding to
      // excitation energies are calculated.

      // Get final state and corresponding energy.
      In nroots = mpVscfCalcDef->GetRspNeig();
      In state = arRspFunc.RightState();
      const string& oper_name = arRspFunc.GetRspOp(I_0);
      Nb eigval = C_0;
      if (state >= nroots)
      {
         Mout << " Transition matrix element for gs -> state " << state
              << " with operator '" << oper_name << "':" << endl
              << " Residual for state is outside calculated space - skipped." << endl;
         return;
      }

      // Check if left-hand solution actually exists or if it is just a dummy.
      //std::string dummy_name = mpVscfCalcDef->Name() + "_rsp_eigvec_left_" + std::to_string(arRspFunc.RightState());
      //if(!InquireFile(dummy_name))
      //   return;

      DataCont left_eig;
      left_eig.GetFromExistingOnDisc(NrspPar(), mpVscfCalcDef->Name()
                                     + "_rsp_eigvec_left_" + std::to_string(arRspFunc.RightState()));
      left_eig.SaveUponDecon();
      if (left_eig.Norm() == C_0)
         return;
      
      DataCont eigval_dc;
      eigval_dc.GetFromExistingOnDisc(nroots, mpVscfCalcDef->Name() + "_rsp_eigval");
      eigval_dc.SaveUponDecon();
      eigval_dc.DataIo(IO_GET,state,eigval);

      // Add response equation.
      if (arRspFunc.UseMvec())
      {
         RspVecInf newrsp(I_1,false,true);
         newrsp.SetNonStandardType("M");
         newrsp.SetFrq(eigval,I_0);
         newrsp.SetGamma(arRspFunc.Gamma());
         newrsp.SetRightState(arRspFunc.RightState());
         arRspVecInfSet.insert(newrsp);
      }
      else
      {
         RspVecInf newrsp(I_1,true);
         newrsp.SetOp(oper_name,I_0);
         newrsp.SetFrq(-eigval,I_0);
         newrsp.SetGamma(-arRspFunc.Gamma());
         arRspVecInfSet.insert(newrsp);
      }
    }
}
/**
*   Find the response equations to solve 
* */
void Vscf::FindRspEqToSolve()
{
   std::set<RspVecInf> rsp_vec_inf_set;
   for (In i_rsp=I_0; i_rsp<mpVscfCalcDef->NrspFuncs(); ++i_rsp)
   {
      AddRspVecToSet(rsp_vec_inf_set, mpVscfCalcDef->GetRspFunc(i_rsp));
   }
   
   mpVscfCalcDef->ClearRspVecContainer(); 
   for(const auto& inf : rsp_vec_inf_set)
   {
      mpVscfCalcDef->AddRspVec(inf);
   }
   
   mpVscfCalcDef->SortRspVecs(); // sort rvi vector according to order
   
   if (  rsp_vec_inf_set.size() > I_0
      ) 
   {
      if(mpVscfCalcDef->IoLevel() > 4)
      {
         Mout << "\n\n Response vectors to solve for:" << std::endl;
         In ic = I_0;
         for(const auto& inf : rsp_vec_inf_set)
         {
            Mout << setw(I_3) << ic++ << ": " << inf << std::endl;
         }
      }
         
      Mout  << " A total of " << mpVscfCalcDef->NrspVecs()
            << " response equations is to be solved.\n " << std::endl;
   }
}

/**
*   Construct residue of quadratic response function 
* */
void Vscf::ResidContractXtoX(In& arIrsp)
{
   if (mpVscfCalcDef->RspIoLevel()>I_5)
      Mout << " Calculate one-photon transition matrix elements " << arIrsp << endl;

   if (!mpVscfCalcDef->Variational())
   {
      Mout << " Residues not implemented yet for non-variational methods " << endl; 
      return; 
   }

   // Get the correct sigma vector

   for (In i_sigma=I_0;i_sigma<mpVscfCalcDef->NrspVecs();i_sigma++) 
   {
      if ((mpVscfCalcDef->GetRspVecInf(i_sigma)).LeftState() == (mpVscfCalcDef->GetRspFunc(arIrsp)).LeftState() &&
         (mpVscfCalcDef->GetRspVecInf(i_sigma)).GetRspOp(I_0) == (mpVscfCalcDef->GetRspFunc(arIrsp)).GetRspOp(I_0)) 
      {
         if(gDebug) 
            Mout << "In XtoX, doing RF = " << mpVscfCalcDef->GetRspFunc(arIrsp) << endl;

         // need a help vector to make sure that the sigma vector does not get overwritten
         string s = mpVscfCalcDef->Name() + "_sigma_vec_" + std::to_string(i_sigma);
         DataCont help_vec;
         help_vec.GetFromExistingOnDisc(NrspPar(),s);
         help_vec.SaveUponDecon();
         DataCont sigma_vec(help_vec);
         if (gDebug) 
            Mout << " Norm of Sigma vec " << sigma_vec.Norm() << endl;

         // Get eigenvector 
         In right_state=(mpVscfCalcDef->GetRspFunc(arIrsp)).RightState();
         DataCont eig_vec;
         string s2 = mpVscfCalcDef->Name() + "_rsp_eigvec_"+std::to_string(right_state);
         eig_vec.GetFromExistingOnDisc(NrspPar(),s2);
         eig_vec.SaveUponDecon();   
         // if i = j, i.e. a diagonal element, add expt. val * evec
         Nb exp_val=mpVscfCalcDef->GetRspOpExpVal(mpVscfCalcDef->GetRspFunc(arIrsp).GetRspOp(I_0));
         if((mpVscfCalcDef->GetRspVecInf(i_sigma)).LeftState() == (mpVscfCalcDef->GetRspFunc(arIrsp)).RightState()) 
         {
            sigma_vec.Axpy(eig_vec,exp_val);
         }
         if (gDebug) 
            Mout << " Norm of eig vec " << eig_vec.Norm() << endl;

         // contract 
         Nb value = C_0;
         bool also_de_ex = !mpVscfCalcDef->DoNoPaired();
         if (also_de_ex) 
         {
            In half = NrspPar()/I_2;
            Nb value_e = Dot(sigma_vec,eig_vec,I_0,half);
            Nb value_d = Dot(sigma_vec,eig_vec,half,half);
            if (gDebug) Mout << " Value e = " << value_e << " Value d = " << value_d << endl;
            value = value_d + value_e;
         }
         else 
         {
            //Mout << "sigma_vec " << sigma_vec << endl;
            //Mout << "eig_vec " << eig_vec << endl;
            //value += C_2*Dot(sigma_vec,rsp_vec);  
            value = Dot(sigma_vec,eig_vec);  
         }
         (mpVscfCalcDef->GetRspFunc(arIrsp)).SetValue(value);
         (mpVscfCalcDef->GetRspFunc(arIrsp)).SetHasBeenEval(true);
         return;
      } // end if
   } // end for
}
/**
*   Construct residue of linear response function 
* */
void Vscf::ResidContractGtoX(In& arIrsp)
{
   RspFunc& rsp_fct = mpVscfCalcDef->GetRspFunc(arIrsp);
   In i_state = rsp_fct.RightState();
   string eta_op = rsp_fct.GetRspOp(I_0);
   
   if (mpVscfCalcDef->RspIoLevel()>I_5)
      Mout << " Calculate one-photon transition matrix elements for rsp. function No. "
           << arIrsp << "." << endl
           << "    State:    " << i_state<< endl
           << "    Operator: " << eta_op << endl;

   if (i_state >= mpVscfCalcDef->GetRspNeig()) 
   {
      Mout << " Residual requested for state outside calculated space - skipped." << endl;
      rsp_fct.SetValue(C_NB_MAX);
      rsp_fct.SetHasBeenEval(false);
      return;
   }
  
   // Get eta vector 
   string s = mpVscfCalcDef->Name() + "_eta_vec_" + eta_op; 
   DataCont eta_vec;
   eta_vec.GetFromExistingOnDisc(NrspPar(),s); 
   eta_vec.SaveUponDecon();
   if (gDebug)
      Mout << " Norm of eta vec " << eta_vec.Norm() << endl;

   // Get eigenvector 
   DataCont eig_vec;
   string s2 = mpVscfCalcDef->Name() + "_rsp_eigvec_"+std::to_string(i_state);
   eig_vec.GetFromExistingOnDisc(NrspPar(),s2);
   eig_vec.SaveUponDecon();
   if (gDebug)
      Mout << " Norm of right eig vec " << eig_vec.Norm() << endl;

   // contract 
   Nb value = C_0;
   bool also_de_ex = !mpVscfCalcDef->DoNoPaired();
   if (also_de_ex) 
   {
      In half = NrspPar()/I_2;
      Nb value_e = Dot(eta_vec,eig_vec,I_0,half);
      Nb value_d = Dot(eta_vec,eig_vec,half,half);
      if (gDebug)
         Mout << " Value e = " << value_e << " Value d = " << value_d << endl;
      value = value_d + value_e;
   }
   else 
      value = Dot(eta_vec,eig_vec);  

   if (! mpVscfCalcDef->Variational())
      value = ResidContractGtoXnonVar(arIrsp, eig_vec, value);

   rsp_fct.SetValue(value);
   rsp_fct.SetHasBeenEval(true);
}

/**
 *   Construct linear response function 
 * */
void Vscf::RspExptValue(In& arIrsp)
{
   if (mpVscfCalcDef->RspIoLevel()>I_5) 
      Mout << " Expectation value read from rsp op expectation value list." << endl;

   Nb value = mpVscfCalcDef->GetRspOpExpVal(mpVscfCalcDef->GetRspFunc(arIrsp).GetRspOp(I_0));

   if (!mpVscfCalcDef->Variational())
   {
      Mout << " Add non-variational contribution." << endl; 
      if (! mpVscfCalcDef->GetRspFunc(arIrsp).NoTwoNPlusTwo())
      {
         // Use 2n+2 rule, that is add t-bar * xi^X , THE SMART WAY
         Mout << " RspExptValue, 2n+2 eval" << endl;

         RspVecInf rsp_vec_inf(I_0,false);

         // Find equation number.
         In i_eq = mpVscfCalcDef->FindRspEq(rsp_vec_inf); 
         if (!mpVscfCalcDef->GetRspVecInf(i_eq).HasBeenEval())
            MIDASERROR("Multipliers was requested but has not been solved for yet");

         DataCont mul0_vec;
         string vecname=mpVscfCalcDef->Name()+"_mul0_vec_"+std::to_string(i_eq);
         mul0_vec.GetFromExistingOnDisc(NrspPar(),vecname);
         mul0_vec.SaveUponDecon(true);

         string oper_name = (mpVscfCalcDef->GetRspFunc(arIrsp)).GetRspOp(I_0);
         string s_xi = mpVscfCalcDef->Name() + "_xi_vec_" + oper_name;
         DataCont xi_vec; 
         xi_vec.GetFromExistingOnDisc(NrspPar(),s_xi); 
         xi_vec.SaveUponDecon(true);

         Nb add_value = Dot(mul0_vec,xi_vec);
         value += add_value; 
         Mout << " non-variational correction " << add_value << " accumulated " << value << endl; 
      }
      else
      {
         // Do not use 2n+2 rule, that is add eta^0 * t^X, THE STUPID WAY
         Mout << " RspExptValue, No 2n+2 eval" << endl;
         
         // Get the response vector
         DataCont rsp_vec;
         GetFirstOrderRsp((mpVscfCalcDef->GetRspFunc(arIrsp)).GetRspOp(I_0), C_0, rsp_vec);
         rsp_vec.SaveUponDecon(true);
           
         // Get eta0 vector vector.
         string s = mpVscfCalcDef->Name() + "_eta0_vec"; 
         DataCont eta_vec;
         eta_vec.GetFromExistingOnDisc(NrspPar(),s);
         eta_vec.SaveUponDecon(true);

         Nb add_value = Dot(rsp_vec,eta_vec);
         value += add_value; 
         Mout << " non-variational correction " << add_value << " accumulated " << value << endl; 
      }
   }

   (mpVscfCalcDef->GetRspFunc(arIrsp)).SetValue(value);
   (mpVscfCalcDef->GetRspFunc(arIrsp)).SetHasBeenEval(true);
}

/**
 * Get first order response parameters for operator 'aOper' and frequency 'aFrq'
 * and store them a aDc.
 **/
void Vscf::GetFirstOrderRsp
   (  const std::string& aOper
   ,  Nb aFrq
   ,  DataCont& aDc
   )
{
   // Get number of vectors with order different from 1.
   // May for instance be zeroth order amplitudes.
   In eqs_bef=I_0;
   for(eqs_bef=I_0; eqs_bef<mpVscfCalcDef->NrspVecs(); ++eqs_bef)
   {
      const RspVecInf vecinf = mpVscfCalcDef->GetRspVecInf(eqs_bef);
      if (vecinf.GetNorder() == I_1 && !vecinf.IsNonStandardType())
         break;
   }

   // The response vector we are searching for.
   RspVecInf vecinf(I_1);
   vecinf.SetOp(aOper,I_0);
   vecinf.SetFrq(aFrq,I_0);
   In eq = mpVscfCalcDef->FindRspEq(vecinf); 
   if (mpVscfCalcDef->RspIoLevel()>I_10)
      Mout << " Searched for and found first order response with oper = '" << aOper
           << "', freq = " << aFrq << ", and rsp. vec. no. " << eq << endl;

   if (!mpVscfCalcDef->GetRspVecInf(eq).HasBeenEval())
         MIDASERROR(" Vscf::GetFirstOrderRsp(): A Response vector was requested "
               "that has not been solved for yet.");

   // Get from disc.
   aDc.GetFromExistingOnDisc(NrspPar(),mpVscfCalcDef->Name()
                                       +"_p1rsp_vec_"+std::to_string(eq-eqs_bef));
   aDc.SaveUponDecon(true);
}

/**
 * Get first order response complex case
 **/
void Vscf::GetFirstOrderRsp
   (  const std::string& aOper
   ,  Nb aFrq
   ,  Nb aGamma
   ,  ComplexVector<DataCont, DataCont>& aDc
   )
{
   // Get number of vectors with order different from 1.
   // May for instance be zeroth order amplitudes.
   In eqs_bef=I_0;
   for(eqs_bef=I_0; eqs_bef<mpVscfCalcDef->NrspVecs(); ++eqs_bef)
   {
      const RspVecInf vecinf = mpVscfCalcDef->GetRspVecInf(eqs_bef);
      if (vecinf.GetNorder() == I_1 && !vecinf.IsNonStandardType())
         break;
   }

   // The response vector we are searching for.
   RspVecInf vecinf(I_1);
   vecinf.SetOp(aOper,I_0);
   vecinf.SetFrq(aFrq,I_0);
   vecinf.SetGamma(aGamma);
   In eq = mpVscfCalcDef->FindRspEq(vecinf); 
   if (mpVscfCalcDef->RspIoLevel()>I_10)
   {
      Mout << " Searched for and found first order response with oper = '" << aOper
           << "', freq = " << aFrq << ", and rsp. vec. no. " << eq << endl;
   }

   if (!mpVscfCalcDef->GetRspVecInf(eq).HasBeenEval())
   {
         MIDASERROR(" Vscf::GetFirstOrderRsp(): A Response vector was requested that has not been solved for yet.");
   }

   // Get from disc.
   aDc.Re().GetFromExistingOnDisc(NrspPar(),analysis::rsp().analysis_dir + "/" + mpVscfCalcDef->Name()
                                       +"_p1rsp_vec_re_"+std::to_string(eq-eqs_bef));
   aDc.Re().SaveUponDecon(true);
   aDc.Im().GetFromExistingOnDisc(NrspPar(),analysis::rsp().analysis_dir + "/" + mpVscfCalcDef->Name()
                                       +"_p1rsp_vec_im_"+std::to_string(eq-eqs_bef));
   aDc.Im().SaveUponDecon(true);
}

void Vscf::GetMvec(In aRightState, Nb aFrq, DataCont& aDc)
{
   In eqs_bef = I_0;
   for (eqs_bef=I_0; eqs_bef<mpVscfCalcDef->NrspVecs(); ++eqs_bef)
   {
      const RspVecInf& vecinf = mpVscfCalcDef->GetRspVecInf(eqs_bef);
      if (vecinf.GetNorder() == I_1 && vecinf.GetNonStandardType() == "M")
         break;
   }
   
   RspVecInf vecinf(I_1);
   vecinf.SetNonStandardType("M");
   vecinf.SetFrq(aFrq,I_0);
   vecinf.SetRightState(aRightState);
   In eq = mpVscfCalcDef->FindRspEq(vecinf);

   if (gDebug)
      Mout << " Searched for M vec with freq. " << aFrq << ": Found with vec. no. " << eq << endl;
   
   if (!mpVscfCalcDef->GetRspVecInf(eq).HasBeenEval())
      MIDASERROR(" Vscf::GetMvec(): Vector requested not yet evaluated.");

   aDc.GetFromExistingOnDisc(NrspPar(),
                             mpVscfCalcDef->Name()+"_rsp_vcc_m_"+std::to_string(eq-eqs_bef));
   aDc.SaveUponDecon();
}

/**
* Construct linear response function 
* */
void Vscf::LrfContract(In& arIrsp)
{
   if (mpVscfCalcDef->RspIoLevel()>I_5)
   {
      Mout << " Calculate linear response function " << arIrsp << endl;
   }
  
   if (!mpVscfCalcDef->Variational())
   {
      LrfContractNonVar(arIrsp);
      return; 
   }
   
   RspFunc& rsp_fct = mpVscfCalcDef->GetRspFunc(arIrsp);
   
   // get response function data
   string lr_op  = rsp_fct.GetRspOp(I_1);
   Nb lr_frq     = rsp_fct.GetRspFrq(I_1);
   string eta_op = rsp_fct.GetRspOp(I_0);
   bool asym     = rsp_fct.GetDoAsymmetric();
   Nb value = C_0;
   
   In nvecs = I_1;
   bool also_de_ex = !mpVscfCalcDef->DoNoPaired();
   if (!also_de_ex)
   {
      nvecs=I_2;         // VCI or VCC: ex + de-ex. separately.
   }
   
   // Get response vectors
   if(!rsp_fct.ComplexRspFunc() && !mpVscfCalcDef->ForceComplexRsp())
   {
      /**
       * evaluate response function non-complex case
       **/
      for (In i=I_0;i<nvecs;i++)
      {
         // For VCI change sign of freq for second term
         if (i==I_1)
         {
            lr_frq *= C_M_1;
         }
         
         // Get response vector.
         DataCont rsp_vec;
         GetFirstOrderRsp(lr_op, lr_frq, rsp_vec);
         if (mpVscfCalcDef->RspIoLevel()>I_10) 
         {
            Mout << " Norm of rsp vec " << rsp_vec.Norm() << endl;
         }
      
         // Get eta vector. 
         if (mpVscfCalcDef->RspIoLevel()>I_10)
         {
            Mout << " Requires eta vector with op = " << eta_op << endl;
         }
         DataCont eta_vec;
         eta_vec.GetFromExistingOnDisc(NrspPar(), mpVscfCalcDef->Name()+"_eta_vec_"+eta_op); 
         eta_vec.SaveUponDecon(true);
         if (mpVscfCalcDef->RspIoLevel()>I_10) 
         {
            Mout << " Norm of eta vec " << eta_vec.Norm() << endl;
         }
         
         // Make contraction.
         if (also_de_ex)
         {
            In half = NrspPar()/I_2;
            Nb value_e = Dot(eta_vec,rsp_vec,I_0,half);
            Nb value_d = Dot(eta_vec,rsp_vec,half,half);
            value += value_d + value_e;
         }
         else 
         {
            if (asym && i==I_1)
               value -= Dot(eta_vec,rsp_vec);
            else
               value += Dot(eta_vec,rsp_vec);
         }
      }
   
      rsp_fct.SetValue(value);
   }
   else
   {
      /**
       * complex case
       **/
      Nb im_value = C_0;
      Nb gamma = rsp_fct.Gamma();

      for (In i=I_0;i<nvecs;i++)
      {
         // For VCI change sign of freq for second term
         if (i==I_1)
         {
            lr_frq *= C_M_1; // multiply by -1
            gamma  *= C_M_1; // -1
         }
         
         // Get response vector.
         ComplexVector<DataCont,DataCont> rsp_vec;
         GetFirstOrderRsp(lr_op, lr_frq, gamma, rsp_vec);
         if (mpVscfCalcDef->RspIoLevel()>I_10) 
         {
            Mout << " Norm of rsp vec " << rsp_vec.Norm() << endl;
         }
      
         // Get eta vector. 
         if (mpVscfCalcDef->RspIoLevel()>I_10)
         {
            Mout << " Requires eta vector with op = " << eta_op << endl;
         }
         DataCont eta_vec;
         eta_vec.GetFromExistingOnDisc(NrspPar(), mpVscfCalcDef->Name()+"_eta_vec_"+eta_op); 
         eta_vec.SaveUponDecon(true);
         if (mpVscfCalcDef->RspIoLevel()>I_10) 
         {
            Mout << " Norm of eta vec " << eta_vec.Norm() << endl;
         }
         
         // Make contraction.
         if (also_de_ex)
         {
            In half = NrspPar()/I_2;
            // real part
            Nb value_e = Dot(eta_vec,rsp_vec.Re(),I_0,half);
            Nb value_d = Dot(eta_vec,rsp_vec.Re(),half,half);
            value += value_d + value_e;
            // im part
            Nb im_value_e = Dot(eta_vec,rsp_vec.Im(),I_0,half);
            Nb im_value_d = Dot(eta_vec,rsp_vec.Im(),half,half);
            im_value += im_value_d + im_value_e;
         }
         else 
         {
            if (asym && i==I_1)
            {
               value    -= Dot(eta_vec,rsp_vec.Re());
               im_value -= Dot(eta_vec,rsp_vec.Im());
            }
            else
            {
               //std::cout << " dot    = " << Dot(eta_vec,rsp_vec.Re()) << std::endl;
               //std::cout << " im_dot = " << Dot(eta_vec,rsp_vec.Im()) << std::endl;
               value    += Dot(eta_vec,rsp_vec.Re());
               im_value += Dot(eta_vec,rsp_vec.Im());
            }
         }
         
         MidasWarning("File association for Variational linear response not tested!");
         rsp_fct.AssociateFile({lr_op}, analysis::rsp().linear_rsp_vec + std::string((i==I_1 ? "+" : "-")) + "re", rsp_vec.Re().Label());
         rsp_fct.AssociateFile({lr_op}, analysis::rsp().linear_rsp_vec + std::string((i==I_1 ? "+" : "-")) + "im", rsp_vec.Im().Label());
      }

      rsp_fct.SetValue(value);
      rsp_fct.SetImValue(im_value);
   }
   
   rsp_fct.SetHasBeenEval(true);
}
/**
*  Contract Quadratic RF sigma vectors with Linear parameters to give
*  QRF.
*/
void Vscf::QrfContract(In& i_rsp) 
{
   if (!mpVscfCalcDef->Variational())
   {
      Mout << " Qrf not implemented yet for non-variational methods " << endl; 
      return; 
   }
   // First we need to make the same expansion as in the AddToSet function
   set<RspVecInf> rsp_vec_inf_set;
   Nb qrf_value=C_0;
   for(In i_sigma=I_0;i_sigma<I_6;i_sigma++) {
      // loop over the 6 sigma vectors
      if (i_sigma==I_0) { // X Y in <<X;Y,Z>>
         string sigma_op1=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_0);
         string sigma_op2=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_1);
         Nb sigma_frq=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspFrq(I_1);
         string rsp_op=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_2);
         Nb rsp_frq=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspFrq(I_2);
         Nb msigma_frq=C_M_1*sigma_frq;
         Nb mrsp_frq=C_M_1*rsp_frq;
         QrfContribution(qrf_value,sigma_op1,sigma_op2,sigma_frq,rsp_op,mrsp_frq);
         QrfContribution(qrf_value,sigma_op1,sigma_op2,msigma_frq,rsp_op,rsp_frq);
      }
      if (i_sigma==I_0) { // X Z in <<X;Y,Z>>
         string sigma_op1=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_0);
         string sigma_op2=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_2);
         Nb sigma_frq=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspFrq(I_2);
         string rsp_op=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_1);
         Nb rsp_frq=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspFrq(I_1);
         Nb msigma_frq=C_M_1*sigma_frq;
         Nb mrsp_frq=C_M_1*rsp_frq;
         QrfContribution(qrf_value,sigma_op1,sigma_op2,sigma_frq,rsp_op,mrsp_frq);
         QrfContribution(qrf_value,sigma_op1,sigma_op2,msigma_frq,rsp_op,rsp_frq);
      }
      if (i_sigma==I_0) { // Y X in <<X;Y,Z>>
         string sigma_op1=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_1);
         string sigma_op2=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_0);
         Nb sigma_frq=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspFrq(I_0);
         string rsp_op=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_2);
         Nb rsp_frq=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspFrq(I_2);
         Nb msigma_frq=C_M_1*sigma_frq;
         Nb mrsp_frq=C_M_1*rsp_frq;
         QrfContribution(qrf_value,sigma_op1,sigma_op2,sigma_frq,rsp_op,mrsp_frq);
         QrfContribution(qrf_value,sigma_op1,sigma_op2,msigma_frq,rsp_op,rsp_frq);
      }
      if (i_sigma==I_0) { // Y Z in <<X;Y,Z>>
         string sigma_op1=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_1);
         string sigma_op2=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_2);
         Nb sigma_frq=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspFrq(I_2);
         string rsp_op=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_0);
         Nb rsp_frq=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspFrq(I_0);
         Nb msigma_frq=C_M_1*sigma_frq;
         Nb mrsp_frq=C_M_1*rsp_frq;
         QrfContribution(qrf_value,sigma_op1,sigma_op2,sigma_frq,rsp_op,mrsp_frq);
         QrfContribution(qrf_value,sigma_op1,sigma_op2,msigma_frq,rsp_op,rsp_frq);
      }
      if (i_sigma==I_0) { // Z X in <<X;Y,Z>>
         string sigma_op1=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_2);
         string sigma_op2=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_0);
         Nb sigma_frq=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspFrq(I_0);
         string rsp_op=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_1);
         Nb rsp_frq=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspFrq(I_1);
         Nb msigma_frq=C_M_1*sigma_frq;
         Nb mrsp_frq=C_M_1*rsp_frq;
         QrfContribution(qrf_value,sigma_op1,sigma_op2,sigma_frq,rsp_op,mrsp_frq);
         QrfContribution(qrf_value,sigma_op1,sigma_op2,msigma_frq,rsp_op,rsp_frq);
      }
      if (i_sigma==I_0) { // Z Y in <<X;Y,Z>>
         string sigma_op1=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_2);
         string sigma_op2=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_1);
         Nb sigma_frq=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspFrq(I_1);
         string rsp_op=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(I_0);
         Nb rsp_frq=(mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspFrq(I_0);
         Nb msigma_frq=C_M_1*sigma_frq;
         Nb mrsp_frq=C_M_1*rsp_frq;
         QrfContribution(qrf_value,sigma_op1,sigma_op2,sigma_frq,rsp_op,mrsp_frq);
         QrfContribution(qrf_value,sigma_op1,sigma_op2,msigma_frq,rsp_op,rsp_frq);
      }
   }
   qrf_value/=C_2;
   (mpVscfCalcDef->GetRspFunc(i_rsp)).SetValue(qrf_value);
   (mpVscfCalcDef->GetRspFunc(i_rsp)).SetHasBeenEval(true);
}
/**
* Helper function for QrfContract
**/
void Vscf::QrfContribution(Nb& qrf_value,string& sigma_op1,string& sigma_op2,Nb& sigma_frq,
                           string& rsp_op,Nb& rsp_frq) 
{
   // Get sigma vec
   In qrf_index=-I_1;
   for(In j=I_0;j<mpVscfCalcDef->NrspVecs();j++) {  
      if((mpVscfCalcDef->GetRspVecInf(j)).GetNorder() == I_2) {
         if((mpVscfCalcDef->GetRspVecInf(j)).GetNonStandardType()=="SECOND_ORDER_RSP_VEC")
            continue;
         qrf_index++;
         if((mpVscfCalcDef->GetRspVecInf(j)).GetRspOp(I_0) == sigma_op1 &&
            (mpVscfCalcDef->GetRspVecInf(j)).GetRspOp(I_1) == sigma_op2 &&
            (mpVscfCalcDef->GetRspVecInf(j)).GetRspFrq(I_1) == sigma_frq ) {
            break;
         }
      }
   }
   // Get rsp vec
   In lin_rsp=-I_1;
   for(In j=I_0;j<mpVscfCalcDef->NrspVecs();j++) {  
      if((mpVscfCalcDef->GetRspVecInf(j)).GetNorder() == I_1) {
         lin_rsp++;
         if((mpVscfCalcDef->GetRspVecInf(j)).GetRspOp(I_0) == rsp_op &&
            (mpVscfCalcDef->GetRspVecInf(j)).GetRspFrq(I_0) == rsp_frq ) {
            break;
         }
      }
   }
   // Get rsp vec
   DataCont rsp_vec;
   string vecname=mpVscfCalcDef->Name()+"_p1rsp_vec_"+std::to_string(lin_rsp);
   rsp_vec.GetFromExistingOnDisc(NrspPar(),vecname);
   rsp_vec.SaveUponDecon();
   if (gDebug)
      Mout << "LRV = " << vecname << endl;
   // Get sigma vec
   DataCont sigma_vec;
   vecname=mpVscfCalcDef->Name()+"_qrf_vec_"+std::to_string(qrf_index);
   sigma_vec.GetFromExistingOnDisc(NrspPar(),vecname);
   sigma_vec.SaveUponDecon(); 
   if(gDebug)
      Mout << "QRF = " << vecname << endl;
   // Contract
   /* for a future vscf implementation
   if (!mpVscfCalcDef->DoNoPaired())
   {
      In half = NrspPar()/I_2;
      Nb value_e = Dot(sigma_vec,rsp_vec,I_0,half);
      Nb value_d = Dot(sigma_vec,rsp_vec,half,half);
      //if (gDebug)
         Mout << " Value e = " << value_e << " Value d = " << value_d << endl;
      qrf_value += value_d - value_e;
   }
   */
   if(gDebug)
      Mout << "Added: " << Dot(sigma_vec,rsp_vec) << endl;
   qrf_value+=Dot(sigma_vec,rsp_vec);
}
/**
*
**/
void Vscf::CrfContract(In& arIrsp) {
   RspFunc this_rsp=mpVscfCalcDef->GetRspFunc(arIrsp);
   // 1) loop over all permutations of (operator,frq) pairs
   vector<pair<string,Nb> > oper_frq_vec;
   oper_frq_vec.push_back(make_pair(this_rsp.GetRspOp(I_0),this_rsp.GetRspFrq(I_0)));
   oper_frq_vec.push_back(make_pair(this_rsp.GetRspOp(I_1),this_rsp.GetRspFrq(I_1)));
   oper_frq_vec.push_back(make_pair(this_rsp.GetRspOp(I_2),this_rsp.GetRspFrq(I_2)));
   oper_frq_vec.push_back(make_pair(this_rsp.GetRspOp(I_3),this_rsp.GetRspFrq(I_3)));
   // MBH, JUST FOR DEBUG
   vector<pair<string,Nb> > check_vec=oper_frq_vec;
   check_vec.erase(check_vec.begin());
   //CheckSecondOrderParams(check_vec);
   // END DEBUG
   sort(oper_frq_vec.begin(),oper_frq_vec.end());
   In actual_permut=I_0;
   Nb f=C_0;
   Nb fx=C_0;
   Nb gx=C_0;
   Nb h=C_0;
   do {
      actual_permut++;
      if(gDebug) {
         Mout << "\nWill now handle cubic response term with info:" << endl << endl;
         Mout << "<<";
         for(In i=0;i<4;i++)
            Mout << oper_frq_vec[i].first << ",";
         Mout << ">>(";
         for(In i=0;i<4;i++)
            Mout << oper_frq_vec[i].second << ",";
         Mout << ")" << endl;
      }
      // Do the G^x transformation.
      f+=ComputeFContribution(oper_frq_vec);
      gx+=ComputeGXContribution(oper_frq_vec);
      h+=ComputeHContribution(oper_frq_vec);
      fx+=ComputeFXContribution(oper_frq_vec);
      // for the first three operators, compute quadratic response function
      /*
      Nb quad=C_0;
      string s=GetSecondOrderRspVecName(C_0,C_0,oper_frq_vec[0].first,oper_frq_vec[1].first);
      string sigma_name=mpVscfCalcDef->Name()+"_crf_helper_tmp_";
      DataCont sigma_vec(NrspPar(),C_0,"OnDisc",sigma_name,false);
      {
         DataCont vec1;
         vec1.GetFromExistingOnDisc(NrspPar(),s);
         vec1.SaveUponDecon(true);
         Nb frq=C_0;
         s=GetFirstOrderRspVecName(C_0,oper_frq_vec[2].first);
         DataCont vec2;
         vec2.GetFromExistingOnDisc(NrspPar(),s);
         vec2.SaveUponDecon(true);
         GeneralFTransformation(vec1,sigma_vec,frq);
         quad-=C_I_2*Dot(sigma_vec,vec2);
         Mout << "Contrib A = " << C_I_2*Dot(sigma_vec,vec2) << endl;
      }
      // next...
      s=GetSecondOrderRspVecName(C_0,C_0,oper_frq_vec[0].first,oper_frq_vec[2].first);
      {
         DataCont vec1;
         vec1.GetFromExistingOnDisc(NrspPar(),s);
         vec1.SaveUponDecon(true);
         sigma_vec.Zero();
         Nb frq=C_0;
         s=GetFirstOrderRspVecName(C_0,oper_frq_vec[1].first);
         DataCont vec2;
         vec2.GetFromExistingOnDisc(NrspPar(),s);
         vec2.SaveUponDecon(true);
         GeneralFTransformation(vec1,sigma_vec,frq);
         quad-=C_I_2*Dot(sigma_vec,vec2);
         Mout << "Contrib B = " << C_I_2*Dot(sigma_vec,vec2) << endl;
      }
      // next...
      s=GetSecondOrderRspVecName(C_0,C_0,oper_frq_vec[1].first,oper_frq_vec[2].first);
      {
         DataCont vec1;
         vec1.GetFromExistingOnDisc(NrspPar(),s);
         vec1.SaveUponDecon(true);
         sigma_vec.Zero();
         Nb frq=C_0;
         s=GetFirstOrderRspVecName(C_0,oper_frq_vec[0].first);
         DataCont vec2;
         vec2.GetFromExistingOnDisc(NrspPar(),s);
         vec2.SaveUponDecon(true);
         GeneralFTransformation(vec1,sigma_vec,frq);
         quad-=C_I_2*Dot(sigma_vec,vec2);
         Mout << "Contrib C = " << C_I_2*Dot(sigma_vec,vec2) << endl;
      }
      Mout << "<<" << oper_frq_vec[0].first << oper_frq_vec[1].first
           << oper_frq_vec[2].first << ">>static = " << quad << endl;
   */
   } while(next_permutation(oper_frq_vec.begin(),oper_frq_vec.end()));
   Mout << "For CRF: " << endl << " F = " << f << "\n H = " << h << "\n FX = "
        << fx << "\n GX = " << gx << endl;
   Nb crsp_contribution=f+fx+gx+h;
   Nb fac=(Nb)24/((Nb)actual_permut);
   Mout << "Number of permutations: " << actual_permut
        << " => multiply by: " << fac << endl;
   crsp_contribution*=fac;
   (mpVscfCalcDef->GetRspFunc(arIrsp)).SetValue(crsp_contribution);
   (mpVscfCalcDef->GetRspFunc(arIrsp)).SetHasBeenEval(true);
   Mout << "CRSP = " << crsp_contribution << endl;
   Mout << "With first  order part: " << fac*(gx+h) << endl;
   Mout << "With second order part: " << fac*(f+fx) << endl;
}

Nb Vscf::ComputeFContribution(vector<pair<string,Nb> > aPerm) {
   if(gDebug)
      Mout << "In ComputeFContribution..." << endl;
   // loop over: -i+j and +i-j contributions
   vector<In> perms(2);
   perms[0]=-1;
   perms[1]=1;
   Nb result=C_0;
   In act_perm=0;
   do { 
      act_perm++;
      DataCont vec1;
      Nb frq1=(Nb)perms[0]*aPerm[0].second;
      Nb frq2=(Nb)perms[0]*aPerm[1].second;
      Nb sum_frq=frq1+frq2;
      string dc_name=GetSecondOrderRspVecName(frq1,frq2,aPerm[0].first,aPerm[1].first);
      vec1.GetFromExistingOnDisc(NrspPar(),dc_name);
      vec1.SaveUponDecon(true);
      DataCont vec2;
      frq1=(Nb)perms[1]*aPerm[2].second;
      frq2=(Nb)perms[1]*aPerm[3].second;
      sum_frq+=frq1+frq2;
      dc_name=GetSecondOrderRspVecName(frq1,frq2,aPerm[2].first,aPerm[3].first);
      vec2.GetFromExistingOnDisc(NrspPar(),dc_name);
      vec2.SaveUponDecon(true);
      // transform vec1 by F^X and dot with vec2
      // use calculateSigma function
      string sigma_name=mpVscfCalcDef->Name()+"_crf_helper_tmp_";
      DataCont sigma_vec(NrspPar(),C_0,"OnDisc",sigma_name,false);
      sum_frq/=(C_2*C_M_1);
      GeneralFTransformation(vec1,sigma_vec,sum_frq);
      result+=Dot(sigma_vec,vec2);
   } while(next_permutation(perms.begin(),perms.end()));
   return result/(C_8);
}

Nb Vscf::ComputeHContribution(vector<pair<string,Nb> > aPerm) {
   if(gDebug)
      Mout << "In ComputeHContribution..." << endl;
   // loop over contributions
   vector<In> perms(4);
   perms[0]=-1;
   perms[1]=-1;
   perms[2]=1;
   perms[3]=1;
   Nb result=C_0;
   In act_perm=0;
   do {
      act_perm++;
      DataCont vec1;
      Nb frq=(Nb)perms[0]*aPerm[0].second;
      Nb sum_frq=frq;
      string dc_name=GetFirstOrderRspVecName(frq,aPerm[0].first);
      vec1.GetFromExistingOnDisc(NrspPar(),dc_name);
      vec1.SaveUponDecon(true);
      DataCont vec2;
      frq=(Nb)perms[1]*aPerm[1].second;
      sum_frq+=frq;
      dc_name=GetFirstOrderRspVecName(frq,aPerm[1].first);
      vec2.GetFromExistingOnDisc(NrspPar(),dc_name);
      vec2.SaveUponDecon(true);
      DataCont vec3;
      frq=(Nb)perms[2]*aPerm[2].second;
      sum_frq+=frq;
      dc_name=GetFirstOrderRspVecName(frq,aPerm[2].first);
      vec3.GetFromExistingOnDisc(NrspPar(),dc_name);
      vec3.SaveUponDecon(true);
      DataCont vec4;
      frq=(Nb)perms[3]*aPerm[3].second;
      sum_frq+=frq;
      dc_name=GetFirstOrderRspVecName(frq,aPerm[3].first);
      vec4.GetFromExistingOnDisc(NrspPar(),dc_name);
      vec4.SaveUponDecon(true);
      vector<DataCont> vec;
      vec.push_back(vec1);
      vec.push_back(vec2);
      vec.push_back(vec3);
      vec.push_back(vec4);
      vector<In> plus,minus;
      for(In i=0;i<perms.size();i++) {
         if(perms[i]<0)
            minus.push_back(i);
         else
            plus.push_back(i);
      }
      // minus here, as we below assume a minus outside everything...
      sum_frq/=(C_M_1*C_4);
      // 1) p1, m1 => p2, m2 dottes og p1 transformeres
      string sigma_name=mpVscfCalcDef->Name()+"_crf_helper_tmp_";
      DataCont sigma_vec(NrspPar(),C_0,"OnDisc",sigma_name,false);
      GeneralFTransformation(vec[plus[0]],sigma_vec,sum_frq);
      result-=Dot(sigma_vec,vec[minus[0]])*Dot(vec[plus[1]],vec[minus[1]]);
      sigma_vec.Zero();
      // 2) p1, m2 => p2, m1 dottes of p1 trans
      GeneralFTransformation(vec[plus[0]],sigma_vec,sum_frq);
      result-=Dot(sigma_vec,vec[minus[1]])*Dot(vec[plus[1]],vec[minus[0]]);
      sigma_vec.Zero();
      // 3) p2, m1 => p1, m2 dottes of p2 trans
      GeneralFTransformation(vec[plus[1]],sigma_vec,sum_frq);
      result-=Dot(sigma_vec,vec[minus[0]])*Dot(vec[plus[0]],vec[minus[1]]);
      sigma_vec.Zero();
      // 3) p2, m2 => p1, m1 dottes of p2 trans
      GeneralFTransformation(vec[plus[1]],sigma_vec,sum_frq);
      result-=Dot(sigma_vec,vec[minus[1]])*Dot(vec[plus[0]],vec[minus[0]]);
   } while(next_permutation(perms.begin(),perms.end()));
   if(gDebug) {
      Mout << "\nH made: " << act_perm << " permutations.\n" << endl;
      Mout << "HContribution: " << C_8*result/(24.e0*24.e0) << endl;
   }
   return C_8*result/(24.e0*24.e0);
}

Nb Vscf::ComputeFXContribution(vector<pair<string,Nb> > aPerm) {
   if(gDebug)
      Mout << "In ComputeFXContribution..." << endl;
   // loop over: -i+j and +i-j contributions
   vector<In> perms(2);
   perms[0]=-1;
   perms[1]=1;
   Nb result=C_0;
   In act_perm=0;
   do {
      act_perm++;
      DataCont vec1;
      Nb frq=(Nb)perms[0]*aPerm[1].second;
      string dc_name=GetFirstOrderRspVecName(frq,aPerm[1].first);
      vec1.GetFromExistingOnDisc(NrspPar(),dc_name);
      vec1.SaveUponDecon(true);
      DataCont vec2;
      Nb frq1=(Nb)perms[1]*aPerm[2].second;
      Nb frq2=(Nb)perms[1]*aPerm[3].second;
      dc_name=GetSecondOrderRspVecName(frq1,frq2,aPerm[2].first,aPerm[3].first);
      vec2.GetFromExistingOnDisc(NrspPar(),dc_name);
      vec2.SaveUponDecon(true);
      // transform vec1 by F^X and dot with vec2
      // use calculateSigma function
      string sigma_name=mpVscfCalcDef->Name()+"_crf_helper_tmp_";
      DataCont sigma_vec(NrspPar(),C_0,"OnDisc",sigma_name,false);
      Nb expt_val=C_0;
      string oper=aPerm[0].first;
      In oper_nr=gOperatorDefs.GetOperatorNr(oper);
      if(oper_nr == -I_1)
         MIDASERROR("In ComputeFXContribution: Did not find the requested operator!");
      CalculateSigma(vec1,sigma_vec,oper,oper_nr,expt_val);
      result+=Dot(sigma_vec,vec2);
   } while(next_permutation(perms.begin(),perms.end()));
   return result/(C_2);
}

Nb Vscf::ComputeGXContribution(vector<pair<string,Nb> > aPerm) {
   if(gDebug)
      Mout << "In ComputeGXContribution..." << endl;
   // First operator is the one we want to transform with
   string trf_op=aPerm[0].first;
   // Get the eta vector for this operator
   string s=mpVscfCalcDef->Name() + "_eta_vec_" + trf_op;
   DataCont eta_vec;
   eta_vec.GetFromExistingOnDisc(NrspPar(),s);
   eta_vec.SaveUponDecon(true);
   /* Now loop over the possible contributions:
      -i-j+k
      -i+j-k
      +i-j-k
      -i+j+k
      +i-j+k
      +i+j-k
      first: Two negative indices
   */
   vector<In> perms(3);
   perms[0]=-1;
   perms[1]=-1;
   perms[2]=1;
   Nb result=C_0;
   In act_perm=0;
   do {
      act_perm++;
      vector<DataCont> vec;
      // find the three response equations
      DataCont vec1;
      Nb frq=(Nb)perms[0]*aPerm[1].second;
      string dc_name=GetFirstOrderRspVecName(frq,aPerm[1].first);
      vec1.GetFromExistingOnDisc(NrspPar(),dc_name);
      vec1.SaveUponDecon(true);
      vec.push_back(vec1);
      DataCont vec2;
      frq=(Nb)perms[1]*aPerm[2].second;
      dc_name=GetFirstOrderRspVecName(frq,aPerm[2].first);
      vec2.GetFromExistingOnDisc(NrspPar(),dc_name);
      vec2.SaveUponDecon(true);
      vec.push_back(vec2);
      DataCont vec3;
      frq=(Nb)perms[2]*aPerm[3].second;
      dc_name=GetFirstOrderRspVecName(frq,aPerm[3].first);
      vec3.GetFromExistingOnDisc(NrspPar(),dc_name);
      vec3.SaveUponDecon(true);
      vec.push_back(vec3);
      In plus,minus1,minus2;
      if(perms[0]>0) {
         plus=0;
         minus1=1;
         minus2=2;
      }
      else if(perms[1]>0) {
         plus=1;
         minus1=0;
         minus2=2;
      }
      else {
         plus=2;
         minus1=0;
         minus2=1;
      }
      // minus here, as \eta_{-i} and two neg. indices
      result-=Dot(eta_vec,vec[minus1])*Dot(vec[minus2],vec[plus]);
      result-=Dot(eta_vec,vec[minus2])*Dot(vec[minus1],vec[plus]);
   } while(next_permutation(perms.begin(),perms.end()));
   perms[0]=-1;
   perms[1]=1;
   perms[2]=1;
   do {
      act_perm++;
      vector<DataCont> vec;
      // find the three response equations
      DataCont vec1;
      Nb frq=(Nb)perms[0]*aPerm[1].second;
      string dc_name=GetFirstOrderRspVecName(frq,aPerm[1].first);
      vec1.GetFromExistingOnDisc(NrspPar(),dc_name);
      vec1.SaveUponDecon(true);
      vec.push_back(vec1);
      DataCont vec2;
      frq=(Nb)perms[1]*aPerm[2].second;
      dc_name=GetFirstOrderRspVecName(frq,aPerm[2].first);
      vec2.GetFromExistingOnDisc(NrspPar(),dc_name);
      vec2.SaveUponDecon(true);
      vec.push_back(vec2);
      DataCont vec3;
      frq=(Nb)perms[2]*aPerm[3].second;
      dc_name=GetFirstOrderRspVecName(frq,aPerm[3].first);
      vec3.GetFromExistingOnDisc(NrspPar(),dc_name);
      vec3.SaveUponDecon(true);
      vec.push_back(vec3);
      In minus,plus1,plus2;
      if(perms[0]<0) {
         minus=0;
         plus1=1;
         plus2=2;
      }
      else if(perms[1]<0) {
         minus=1;
         plus1=0;
         plus2=2;
      }
      else {
         minus=2;
         plus1=0;
         plus2=1;
      }
      // minus here, as \eta_{+i} and two positive indices
      result-=Dot(eta_vec,vec[plus1])*Dot(vec[plus2],vec[minus]);
      result-=Dot(eta_vec,vec[plus2])*Dot(vec[plus1],vec[minus]);
   } while(next_permutation(perms.begin(),perms.end()));
   if(gDebug) {
      Mout << "\nGX made: " << act_perm << " permutations.\n" << endl;
      Mout << "GXContribution: " << C_4*result/(C_6*C_6) << endl;
   }
   return C_4*result/(C_6*C_6);
}

string Vscf::GetFirstOrderRspVecName(Nb aFrq, string aOp) {
   In first_order_index=-I_1;
   In n_eqs=mpVscfCalcDef->NrspVecs();
   for(In i_fo=I_0;i_fo<n_eqs;i_fo++)
   {  
      if(mpVscfCalcDef->GetRspVecInf(i_fo).GetNorder() < I_1)
         continue;
      if(mpVscfCalcDef->GetRspVecInf(i_fo).GetNorder() > I_1)
         MIDASERROR("Did not find requested first order equation");
      first_order_index++;
      Nb frq=mpVscfCalcDef->GetRspVecInf(i_fo).GetRspFrq(I_0);
      string op=mpVscfCalcDef->GetRspVecInf(i_fo).GetRspOp(I_0);
      if(frq==aFrq && op==aOp)
      {
         break;
      }
   }
   if(first_order_index==-I_1)
      MIDASERROR("First order rsp vector not found!");
   return mpVscfCalcDef->Name()+"_p1rsp_vec_"+std::to_string(first_order_index);
}

string Vscf::GetSecondOrderRspVecName(Nb aFrq1, Nb aFrq2, string aOp1, string aOp2) {
   In second_order_index=-I_1;
   In n_eqs=mpVscfCalcDef->NrspVecs();
   for(In i_so=I_0;i_so<n_eqs;i_so++)
   {  
      if(mpVscfCalcDef->GetRspVecInf(i_so).GetNorder() < I_2)
         continue;
      if(mpVscfCalcDef->GetRspVecInf(i_so).GetNorder() > I_2)
         MIDASERROR("Did not find requested first order equation");
      if(mpVscfCalcDef->GetRspVecInf(i_so).GetNonStandardType()!="SECOND_ORDER_RSP_VEC")
         continue;
      second_order_index++;
      Nb frq1=mpVscfCalcDef->GetRspVecInf(i_so).GetRspFrq(I_0);
      Nb frq2=mpVscfCalcDef->GetRspVecInf(i_so).GetRspFrq(I_1);
      string op1=mpVscfCalcDef->GetRspVecInf(i_so).GetRspOp(I_0);
      string op2=mpVscfCalcDef->GetRspVecInf(i_so).GetRspOp(I_1);
      if(frq1==aFrq1 && frq2==aFrq2 && op1==aOp1 && op2==aOp2) 
      {
         break;
      }
   }
   if(second_order_index==-I_1)
      MIDASERROR("Second order rsp vector not found!");
   return mpVscfCalcDef->Name()+"_p2rsp_vec_"+std::to_string(second_order_index);
}
/**
*   Contract response vectors with vectors to get final response functions
* */
void Vscf::RspContract()
{
   if (mpVscfCalcDef->RspIoLevel()>I_1)
   {
      Mout << " Contracting response vectors with matrices and vectors to give"
           << " final response functions." << endl;
   }
   for (In i_rsp=I_0;i_rsp<mpVscfCalcDef->NrspFuncs();i_rsp++)
   {
      In n_order =  (mpVscfCalcDef->GetRspFunc(i_rsp)).GetNorder();
      if (gDebug)
      {
         Mout << " i_rsp = " << i_rsp << " n_order = " << n_order << endl;
      }
      
      if (n_order == I_1) 
      {
         RspExptValue(i_rsp);
      }
      else if (n_order == I_2) 
      {
         LrfContract(i_rsp);
      }
      else if (n_order == -I_1) 
      {
         ResidContractGtoX(i_rsp);
      }
      else if (n_order == -I_11) 
      {
         ResidContractXtoX(i_rsp);
      }
      else if (n_order == I_3) 
      {
         QrfContract(i_rsp);
      }
      else if (n_order == I_4) 
      {
         CrfContract(i_rsp);
      }
      else if (n_order > I_4) 
      {
         Mout << " Response not implemented to this high order yet." << endl;
      }
   }
}


/**
*  Make F^X U^i transformations.
**/
void Vscf::CalculateSigma(DataCont& arEigVec,DataCont& arSigma,const std::string& arOperName, const In& arOperNr,Nb& arExptValue) 
{
   // Set arOperNr to working operator
   MIDASERROR("QUADRATIC RESPONSE NOT IMPLEMENTED FOR VSCF YET!");
   PrepareNewOccInt(arOperNr);
   // Make transformation of eigenvector
   Nb dummy_number=C_0;
   VscfRspTransformer trf(this, mpVscfCalcDef, mpOpDef);
   trf.Transform(arEigVec,arSigma,I_0,I_1,dummy_number);
   // when done, subtract the <0|OP|0> expt. value according to the
   // response expression
   Nb e_val=mpVscfCalcDef->GetRspOpExpVal(arOperNr);
   arSigma.Axpy(arEigVec,-e_val);
   //VscfRspTrans(arEigVec,arSigma,I_1,I_0,arExptValue);
}
/**
*  Drive solution of response equations
* */
void Vscf::RspEqSolve
   (  In aNeq
   ,  std::vector<DataCont>& arRhsDc
   ,  MidasVector& arFrqVec
   ,  std::string arKey
   ,  const MidasVector& aGamma
   ,  bool aRight
   )
{
   if((aGamma.Norm() != C_0) || mpVscfCalcDef->ForceComplexRsp())
   {
      ComplexRspEqSolve(aNeq,arRhsDc,arFrqVec,arKey,aGamma,aRight);
      return;
   }

   bool restart = mpVscfCalcDef->RspRestart();
   In nvecsize = NrspPar();

   Transformer* trf = GetRspTransformer(aRight);

   
   bool also_de_ex = !mpVscfCalcDef->DoNoPaired();
   In n_eqs = aNeq;
   
   Mout << " Solve " << n_eqs << " response equations " << endl;
   ItEqSol eqsol(mpVscfCalcDef->VecStorage(), mpVscfCalcDef->GetmVscfAnalysisDir());
   eqsol.SetLinEq();
   eqsol.SetNeq(n_eqs);
   eqsol.SetTransformer(trf);
   eqsol.SetDim(nvecsize);
   eqsol.SetResidThreshold(arKey == "_mul0_vec_" ? mpVscfCalcDef->LambdaThreshold() : mpVscfCalcDef->GetRspItEqResidThr());
   eqsol.SetResidThresholdRel(arKey == "_mul0_vec_" ? mpVscfCalcDef->LambdaThreshold() : mpVscfCalcDef->GetRspItEqResidThrRel());
   eqsol.SetRescueMax(mpVscfCalcDef->GetRspItEqRescueMax());
   eqsol.SetEnerThreshold(mpVscfCalcDef->GetRspItEqEnerThr());
   eqsol.SetNiterMax(mpVscfCalcDef->GetRspItEqMaxIter());
   eqsol.SetRedDimMax(mpVscfCalcDef->GetRspRedDimMax());
   eqsol.SetBreakDim(mpVscfCalcDef->GetRspRedBreakDim());
   eqsol.SetIoLevel(mpVscfCalcDef->RspIoLevel());
   eqsol.SetTimeIt(mpVscfCalcDef->RspTimeIt());
   eqsol.SetPaired(also_de_ex); 
   string diag_method;
   if (mpVscfCalcDef->RspDiagMeth()=="DGEEVX" || mpVscfCalcDef->RspDiagMeth()=="DGEEV")
   {
      diag_method="DGESV";
   }
   else if (mpVscfCalcDef->RspDiagMeth()=="MIDAS_JACOBI")
   {
      diag_method="MIDAS_CG";
   }
   else if (mpVscfCalcDef->RspDiagMeth()=="DSYEVD")
   {
      diag_method="DSYSV";
   }
   else
   {
      MIDASERROR("In RspEqSol: What is going on with the method???");
   }
   eqsol.SetDiagMeth(diag_method);
   eqsol.SetOlsen(mpVscfCalcDef->Olsen());
   eqsol.SetTrueHDiag(mpVscfCalcDef->GetRspTrueHDiag());
   eqsol.SetTrueADiag(mpVscfCalcDef->GetRspTrueADiag());
   eqsol.SetImprovedPrecond(mpVscfCalcDef->GetRspImprovedPrecond());
   eqsol.SetPrecondExciLevel(mpVscfCalcDef->GetRspPrecondExciLevel());
   eqsol.SetLevel2Solver(mpVscfCalcDef->GetRspLevel2Solver());
   
   vector<DataCont> rsp_vecs;
   rsp_vecs.reserve(mpVscfCalcDef->GetRspRedDimMax());
   // The solution vectors on return
   string storage_mode = "OnDisc";
   if (mpVscfCalcDef->VecStorage()==I_0) 
   {
      storage_mode = "InMem";
   }
   DataCont tmp_datacont;
   for (In i_eq=I_0;i_eq<n_eqs;i_eq++)
   {
      rsp_vecs.push_back(tmp_datacont);
      string s2 = mpVscfCalcDef->Name() + arKey + std::to_string(i_eq);
      rsp_vecs[i_eq].NewLabel(s2);
      //string s3 = mpVscfCalcDef->Name() + "_rsp_rhs_"+std::to_string(i_eq);
      //rsp_rhss[i_eq].NewLabel(s3);

      if (restart) 
      {
         rsp_vecs[i_eq].ChangeStorageTo("OnDisc");
         rsp_vecs[i_eq].SetNewSize(nvecsize);
         bool robust = true;
         bool check = true;
         if (rsp_vecs[i_eq].ChangeStorageTo(storage_mode,robust,check))
         {
            Mout << " Restart vector read in succesfully for root " << i_eq << endl; 
            Mout << " Norm of restart vector " << rsp_vecs[i_eq].Norm() << endl;
         }
         else
         {
            Mout << " Restart vector readin failed for root " << i_eq << endl; 
            //restart = false;
            //Mout << " Set vector to (a rather random) unit vector " << endl; 
            //rsp_vecs[i_eq].SetToUnitVec(i_eq);
            Mout << " Set vector to first zero vector, later standard guess " << endl; 
            rsp_vecs[i_eq].Zero();
         }
      }
      else
      {
         rsp_vecs[i_eq].ChangeStorageTo(storage_mode);
         rsp_vecs[i_eq].SetNewSize(nvecsize);
      }
      rsp_vecs[i_eq].SaveUponDecon(true);
   }
   eqsol.SetRestart(restart);
   Mout << " Ready to start solving equations " << endl;
   // 
   // SOLVE IT
   if (eqsol.Solve(arFrqVec,rsp_vecs,arRhsDc)) 
   {
      Mout << " Response equations converged to requested threshold " << endl;
   }
   else
   {
      Mout << " Response equations not converged to requested threshold " << endl;
      string s1 = " Response equations not converged, for calc. = " + mpVscfCalcDef->Name();
      MidasWarning(s1);
   }

   // Niels: What's the point here???
   restart = true;

   // Solution-vector analysis
   this->LinRspOutput(rsp_vecs);

   // Finalize and cleanup
   CleanupRspTransformer(trf);
   Mout << "\n\n Response equations have been solved \n\n" << endl;
}

/**
 *    Solution-vector analysis for linear response equations
 *
 *    @param aSolution     The solution vector
 **/
void Vscf::LinRspOutput
   (  std::vector<DataCont>& aSolution
   )
{
   Mout  << "\n\n Results of reduced space iterative RSP solution " << std::endl;

   In nvecsize = NrspPar();
   In n_out = std::min(I_5, nvecsize);
   auto n_eqs = aSolution.size();

   Mout  << "\n\n Analysis of response vector includes the  " << n_out << " largest elements of vector " << std::endl;
   Mout  << " The first addresses set refers to modes and modals excited to " << std::endl;

   for(In i=I_0; i<n_eqs; ++i)
   {
      Mout << "\n\n\n Rsp_Vec_" << i << std::endl;
      if (  gDebug
         || mpVscfCalcDef->RspIoLevel()>100
         )
      {
         Mout << "\n Rsp_Eq_" << i << " Solution vector \n " << aSolution[i] << std::endl;
      }

      Mout << "\n Largest elements of vector:\n " << std::endl;
      std::vector<Nb> largest_coef(n_out);
      std::vector<In> add_largest_coef(n_out);
      aSolution[i].AddressOfExtrema(add_largest_coef, largest_coef, n_out, I_2);
      Mout << "  ";
      for(In j=I_0; j<n_out; ++j)
      {
         Mout << setw(23) << left << largest_coef[j];
         if (largest_coef[j]<C_0) Mout << " ";
         Mout << "* Psi_" <<  setw(9) << left << add_largest_coef[j];
         RspModesAndLevelsOut(Mout, add_largest_coef[j]);
         Mout << std::endl;

         if (  j < n_out-I_1
            && largest_coef[j+I_1] >= C_0
            )
         {
            Mout << " +";
         }

         if (  j < n_out-I_1
            && largest_coef[j+I_1] < C_0
            )
         {
            Mout << " ";
         }
      }

      Mout << " Norm of Rsp Vector: " << setw(25) << aSolution[i].Norm() << std::endl;
   }
}

/**
 * Solve linear equations with TensorDataCont
 *
 * @param aRhs          Right-hand-side vectors of linear equations
 * @param aRestartKey   Unique name for restart vector
 * @param aFrequencies  Frequencies for freq-shifted equations
 * @param aGamma        Gamma for damped equations
 * @param aRight        Use right-hand transformer?
 **/
void Vscf::TensorRspEqSolve
   (  std::vector<TensorDataCont>& aRhs
   ,  const std::string& aRestartKey
   ,  const MidasVector& aFrequencies
   ,  const MidasVector& aGamma
   ,  bool aRight
   )
{
   // Check for non-implemented features
   if (  aFrequencies.Norm() != C_0
      )
   {
      MIDASERROR("TensorRspEqSolve with non-zero frequencies not implemented!");
   }
   if (  aGamma.Norm() != C_0
      )
   {
      MIDASERROR("TensorRspEqSolve with non-zero gamma not implemented!");
   }

   Mout  << " |==== Vscf::TensorRspEqSolve ====|" << std::endl;

   // Initialize transformer
   Mout  << " == Initializing MidasTensorDataContTransformer == " << std::endl;
   auto* trf = static_cast<midas::vcc::TransformerV3*>(this->GetRspTransformer(aRight));
   MidasTensorDataContTransformer tdc_trf(*trf, false); // include ref in state space

   // If we have disabled the preconditioner (for some reason...)
   if (  this->mpVscfCalcDef->TensorRspNoLinSolverPrecon()
      )
   {
      // Init solver
      TensorLinearEquationSolver solver(tdc_trf, aRhs);

      // Set analysis dir
//      solver.SetAnalysisName(this->mpVscfCalcDef->Name() + "_analysis" + aRestartKey); // Analysis dir for finalizer (if implemented at some point)

      // Initialize solver variables, read in restart vectors, solve equations
      this->TensorRspEqSolveImpl(solver, aRestartKey);
   }
   // Else, use the standard solver
   else
   {
      // Init solver
      TensorDiagPreconLinearEquationSolver solver(tdc_trf, aRhs);

      // Set analysis dir
//      solver.SetAnalysisName(this->mpVscfCalcDef->Name() + "_analysis" + aRestartKey); // Analysis dir for finalizer (if implemented at some point)

      // Initialize solver variables, read in restart vectors, solve equations
      this->TensorRspEqSolveImpl(solver, aRestartKey);
   }

   // Clean up transformer
   this->CleanupRspTransformer(trf);
}

/**
* Erase selected temporary response vectors 
* */
void Vscf::RspErase()
{
   Mout << " In RspErase " << endl; 
   string oper_name;
   for (In i=I_0; i<mpVscfCalcDef->NrspOps();i++) 
   {
      oper_name = mpVscfCalcDef->GetRspOp(i);
      string s = mpVscfCalcDef->Name() + "_eta_vec_" + oper_name;
      Mout << " Removing " << s << endl;
      DataCont eta_vec(NrspPar(),C_0,"OnDisc",s,false); 
   }
}

/**
 *
 **/
Transformer* Vscf::GetRspTransformer(bool aRight)
{
   if (aRight)
   {
      VscfRspTransformer* trf = new VscfRspTransformer(this, mpVscfCalcDef, mpOpDef);
      return trf;
   }
   else
   {
      MIDASERROR("VSCF left transformation not defined.");
   }
   return NULL;
}

/**
 *
 **/
void Vscf::CleanupRspTransformer(Transformer* aTrf)
{
   delete aTrf;
}

/**
 *
 **/
bool Vscf::RspRestartFromPrev(Transformer* apTrf, DataCont& arDc, const In& arIEq,
      const In& arNVecSize,  string aStorage)
{
   return true;
}

/**
 *
 **/
void Vscf::WriteRspRestartInfo(Transformer* apTrf, const In& arNVecSize)
{
}

/**
*   Caluclate Rhs vector. For VSCF pass control to Eta calc. 
* */
void Vscf::CalculateRhs
   (  DataCont& arEta
   ,  const std::string& arOperName
   ,  In& arIoper
   ,  Nb& arExptValue
   )
{
   this->CalculateEta(arEta,arOperName,arIoper,arExptValue);
}

/**
 * Calculate Rhs vector for TensorDataCont
 **/
void Vscf::CalculateRhs
   (  TensorDataCont& arEta
   ,  const std::string& arOperName
   ,  In& arIoper
   ,  Nb& arExptValue
   )
{
   this->CalculateEta(arEta, arOperName, arIoper, arExptValue);
}

/**
*  Not implemented for VSCF yet
**/
void Vscf::GeneralFTransformation(DataCont& arDcIn, DataCont& arDcOut, Nb& aNb) 
{
}

std::vector<ComplexVector<DataCont,ZeroVector<> > > Vscf::GetComplexRhs(std::vector<DataCont>& arRhsDc) const
{
   std::vector<ComplexVector<DataCont,ZeroVector<> > > rhs;
   for(int i=0; i<arRhsDc.size(); ++i)
   {
      rhs.emplace_back();
      rhs.back().Re() = arRhsDc[i];
      rhs.back().Re().ChangeStorageTo("InMem");
      rhs.back().Im().SetNewSize(arRhsDc[i].Size());
   }
   return rhs;
}

/**
 *
 **/
void Vscf::ComplexRspEqSolve(In aNeq
                           , vector<DataCont>& arRhsDc
                           , MidasVector& arFrqVec
                           , string arKey 
                           , const MidasVector& aGamma
                           , bool aRight
                           )
{ 
   // setup stuff
   Transformer* trf = GetRspTransformer(aRight);

   MidasComplexFreqShiftedTransformer midas_trf(*trf, arFrqVec, aGamma);
   
   std::vector<ComplexVector<DataCont,ZeroVector<> > > y 
      = GetComplexRhs(arRhsDc);
   
   // get the solution
   std::vector<ComplexVector<DataCont,DataCont> > solution;
   
   switch(mpVscfCalcDef->RspComplexSolverType())
   {
      case COMPLEXSOLVER::STANDARD:
         solution = RunComplexRspEqSolve<ComplexFreqShiftedLinearEquationSolver>(y, midas_trf, arKey);
         break;
      case COMPLEXSOLVER::IMPROVED:
         solution = RunComplexRspEqSolve<ImprovedComplexFreqShiftedLinearEquationSolver>(y, midas_trf, arKey);
         break;
      case COMPLEXSOLVER::SUBSPACE:
         solution = RunComplexRspEqSolve<ComplexFreqShiftedSubspaceLinearEquationSolver>(y, midas_trf, arKey);
         break;
      case COMPLEXSOLVER::ERROR:
         MidasWarning("COMPLEXSOLVER::ERROR");
      default:
         MIDASERROR("UNKNOWN COMPLEX SOLVER TYPE.");
   }
   
   // transformer output
   Mout << trf << std::endl;

   // do some analysis
   std::vector<DataCont> im_sol;
   for(int i=0; i<solution.size(); ++i)
   {
      im_sol.emplace_back(solution[i].Im());
      im_sol.back().Normalize();
   }
   RspEigOutput(arFrqVec, im_sol);
      
   // save solution vectors to disc
   analysis::rsp().MakeAnalysisDir();

   for(int i = 0; i < solution.size(); ++i)
   {
      solution[i].Re().NewLabel(analysis::rsp().analysis_dir + "/" + mpVscfCalcDef->Name()+arKey+"re_"+std::to_string(i));
      solution[i].Re().SaveUponDecon(true);
      solution[i].Im().NewLabel(analysis::rsp().analysis_dir + "/" + mpVscfCalcDef->Name()+arKey+"im_"+std::to_string(i));
      solution[i].Im().SaveUponDecon(true);
   }

   CleanupRspTransformer(trf);
}

/**
* Perform density matrix analysis 
* */
void Vscf::DensityMatrixAnalysis() 
{
   Mout << "\n\n";
   Out72Char(Mout,'$','$','$');
   Out72Char(Mout,'$',' ','$');
   OneArgOut72(Mout," General Density matrix calculation and analysis ",'$');
   Out72Char(Mout,'$',' ','$');
   Out72Char(Mout,'$','$','$');

   // GS density analysis 
   vector<MidasMatrix> one_d_densities_vscf_bas(mpOneModeInt->GetmNoModesInInt());  // hold densities 
   CalcGsDensityMatrixVscfBasis(one_d_densities_vscf_bas); // calculate Densities 
   NaturalModalAnalysis(one_d_densities_vscf_bas); // Analyse Densities 
}
/**
* Perform density matrix analysis 
* */
void Vscf::CalcGsDensityMatrixVscfBasis(vector<MidasMatrix>& arDmats) 
{

   string s_elem="Elementary";  
   In i_oper=gOperatorDefs.GetOperatorNr(s_elem); 

   //if (gDebug) 
   {
      Mout << " Calculate GS density matrices  " << endl; 
      Mout << " Elementary operator is nr " << i_oper << endl; 
      for (auto i=I_0;i<gOperatorDefs.GetNrOfOpers();i++)
      {
         string s = gOperatorDefs[i].Name(); 
         Mout << " op nr " << i << " name " << s << endl; 
         Mout << " nr for name " << s << " is " << gOperatorDefs.GetOperatorNr(s) << endl; 
      }
   }

   if(i_oper== -I_1) MIDASERROR("In Vscf::CalcGsDensityMatrixVscfBasis - Did not find the requested operator!");
   OpDef& elementary = gOperatorDefs[i_oper];

   for (LocalModeNr i_op_mode=0;i_op_mode<mpOneModeInt->GetmNoModesInInt();i_op_mode++)
   {
      GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr  i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
      In           nbas       = mpBasDef->Nbas(i_bas_mode);
      In           nmod       = mpVscfCalcDef->Nmodals(i_bas_mode); 
      Mout << "mode " << i_bas_mode << " nbas " << nbas << " nmod " << nmod << endl; 

      MidasMatrix& dmat = arDmats[i_op_mode]; 
      dmat.SetNewSize(nmod,false,true); 
      //dmat[I_0][I_0]=C_1;

      for (In p=I_0;p<nmod;p++)
      {
         for (In q=I_0;q<nmod;q++)
         {
            Nb expt_val=C_0; 
            Mout << " Calculate expectation value for elementary shift operator ";
            Mout << " mode: " << i_g_mode << " p: " << p << " q: " << q << endl; 

            elementary.SetElementaryOperatorTo(i_op_mode,p,q); //< E^m_pq
            DataCont eta_or_xi_vec(NrspPar(),C_0,"OnDisc",s_elem,true); 
            //Mout << " operator info \n" << elementary << endl; 
            //Mout << " eta vec in " << eta_or_xi_vec.Norm() << endl; 
            CalculateRhs(eta_or_xi_vec,s_elem,i_oper,expt_val);
            //Mout << " Test expt_val " << expt_val << " mode: " << i_g_mode << " p: " 
               //<< p << " q: " << q << endl; 
            if (!mpVscfCalcDef->Variational())
            {
               Mout << " Add non-variational contribution." << endl;
               RspVecInf rsp_vec_inf(I_0,false);
               // Find equation number.
               In i_eq = mpVscfCalcDef->FindRspEq(rsp_vec_inf);
               if (!mpVscfCalcDef->GetRspVecInf(i_eq).HasBeenEval())
               MIDASERROR("Multipliers was requested but has not been solved for yet");
               DataCont mul0_vec; 
               string vecname=mpVscfCalcDef->Name()+"_mul0_vec_"+std::to_string(i_eq);
               mul0_vec.GetFromExistingOnDisc(NrspPar(),vecname);
               mul0_vec.SaveUponDecon(true);

               Nb add_value = Dot(mul0_vec,eta_or_xi_vec);
               expt_val += add_value;

            }
            dmat[p][q]=expt_val; 
         }
      }

      //if (mpVscfCalcDef->RspIoLevel()>I_1) 
      {
         //Mout << " In CalcGsDensityMatrixVscfBasis " << endl;
         Mout << "\n Density matrix for mode " << i_op_mode << " is: \n" << dmat << endl; 
      } 
      Mout << " Trace of mode " << i_op_mode << " density matrix is: " << dmat.Trace() << endl; 

      arDmats[i_op_mode]=dmat;
   } 
} 
/**
* Perform natural modal  analysis 
* */
void Vscf::NaturalModalAnalysis(vector<MidasMatrix>& arDmats) 
{
   if (mpVscfCalcDef->DoItNaMo())
   {
      Mout << " Analysis for iterative natural modals " << endl; 
   }

   bool all_converged=true; 
   Nb max_change=C_0;
   for (LocalModeNr i_op_mode=0;i_op_mode<mpOneModeInt->GetmNoModesInInt();i_op_mode++)
   {
      GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i_op_mode);
      LocalModeNr  i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
      In           nbas       = mpBasDef->Nbas(i_bas_mode);
      In           nmod       = arDmats[i_op_mode].Nrows(); // For the time being... 
      Mout << " In NaturalModalAnalysis nbas " << nbas << " nmod " << nmod << endl; 

      MidasMatrix modals_readin(nmod,nbas);
      mModals.DataIo(IO_GET,modals_readin,nmod*nbas,nmod,nbas,ModalOffSet(i_op_mode));
      MidasMatrix modals=Transpose(modals_readin); 
      MidasMatrix new_modals(modals);


      // Natural modal analysis: 
      MidasMatrix dmat_sym(arDmats[i_op_mode]); 
      if (mpVscfCalcDef->SymDensForNaMo()) dmat_sym.Symmetrize(); 
      MidasMatrix diff=dmat_sym-arDmats[i_op_mode]; 
      Mout << "\n Density matrix analysis for mode " << i_op_mode << endl; 
      Mout << " Difference between Density matrix and symmetriced density matrix has sum of norm squared elements " << diff.Norm2() << endl; 
      MidasMatrix d_natmod(dmat_sym.Nrows()); 
      MidasVector d_natocc(dmat_sym.Nrows()); 
      string diagmeth="DGEEVX"; 
      Diag(dmat_sym,d_natmod,d_natocc,diagmeth,true,false); // Use general diag metho for potentially non-sym density. 
      //Diag(dmat_sym,d_natmod,d_natocc,mpVscfCalcDef->RspDiagMeth(),true,false);  

      Mout << "\n Mode " << i_op_mode << " Natural occupation numbers: \n" << d_natocc; 
      Mout << " Sum of natural occupation numbers " <<d_natocc.SumEle() <<  " (should be one) " << endl; 
      Mout << " Eigenvectors:\n " << d_natmod << endl; 

      // 
      
      Mout << "\n Mode " << i_op_mode << " Natural occupation numbers and modals " << endl; 
      Mout << "--------------------------------------------------------------------------";
      Mout << endl; 
      for (In i=nmod-I_1;i>=I_0;i--) 
      {
         Mout << " Mode " << i_op_mode << " natural modal " << i << " occupation = " << d_natocc[i] << endl; 
         MidasVector v(nmod);
         d_natmod.GetCol(v,i); 
         Mout << " Mode " << i_op_mode << " natural modal " << i << " in current modal basis = \n" << v; 
         MidasVector u(nbas);
         u=modals*v; 
         MidasVector z(nbas);
         In imod=nmod-i-1; //< VSCF modals and natural modals are ordered in reversed order  
         modals.GetCol(z,imod); 
         if (Dot(u,z)<C_0) u.Scale(-C_1);  //< Preserve phase of new modal is like the old for 
                                           // easy comparison and safer restarts. 
         Mout << " Mode " << i_op_mode << " natural modal " 
            << i << " in primitive modal basis, old modals and diff: \n";
         MidasVector x=u; 
         x-=z; 
         for (In j=I_0;j<nbas;j++)
         {
            Mout << right << setw(25) << u[j] << " " << z[j] << " " << x[j] << endl; 
         }
         if (i>0) Mout << " --- " << endl; 
         Nb norm=x.Norm(); 
         Nb weighted_norm=norm*d_natocc[i]; 
         Nb thr=mpVscfCalcDef->GetItNaMoThr(); 
         Mout << " Norm of difference between new and old: " << norm << endl; 
         Mout << " Norm of difference weighted with natural occupation of new: " << weighted_norm << " (thr = " << thr << ")"<< endl; 
         if (weighted_norm >max_change) max_change=weighted_norm; 
         if (weighted_norm>thr) 
         {
            all_converged=false; 
            Mout << 
            " Iterative modals not converged yet for modal " << i << " of mode " << i_op_mode << endl; 
          // Norm of difference between old and new modals....
         }
         else 
         {
            Mout << 
            " Iterative modals converged for modal " << i << " of mode " << i_op_mode << endl; 
         }

         if (mpVscfCalcDef->DoItNaMo()) 
         {
            Mout << " Storing new modals " << endl; 
            new_modals.AssignCol(u,imod);  // Recall reverse order ... 
         }
      }
      Mout << "--------------------------------------------------------------------------";
      Mout << endl; 

      if (mpVscfCalcDef->DoItNaMo()) 
      {
         Mout << "\n Old modal matrix \n " << right << modals << endl; 
         Mout << "\n New modal matrix \n " << right << new_modals << endl; 
         MidasVector occmodal(nbas); 
         new_modals.GetCol(occmodal,I_0);///< ASSUMING occupied modal has been set to 0 in VSCF - and stays zero!!! 
         mOccModals.PieceIo(IO_PUT,occmodal,nbas,OccModalOffSet(i_op_mode));
         modals_readin=Transpose(new_modals);  // modals_readin: Same ordering as one file.
         mModals.DataIo(IO_PUT,modals_readin,nmod*nbas,nmod,nbas,ModalOffSet(i_op_mode));
      }
   }
   if (all_converged)
   {
      Mout << " All modals are converged in the Iterative Natural Modal iteration " << endl; 
      SetItNaMoConv(true); 
   }
   else
   {
      Mout << " Not all modals are converged in the Iterative Natural Modal iteration " << endl; 
   } 
   Mout << " Maximum modal change weighted with occupation in the last iteration was " << max_change << endl; 

} 
/**
* Transform densitry from Modal basis to primitive Basis 
* */
void Vscf::TransformDensityFromVscfToPrim(MidasMatrix& arDensVscf, MidasMatrix& arDensPrim, const In& arM)
{
   Mout << " to be made " << endl; 
   //GlobalModeNr i_g_mode   = mpOpDef->GetGlobalModeNr(i_op_mode);
   //LocalModeNr i_bas_mode = mpBasDef->GetLocalModeNr(i_g_mode);
   //In nbas       = mpBasDef->Nbas(i_bas_mode);

}



/**
*   Prepare Eta0 
* */
void Vscf::CalculateEta0() 
{
   MIDASERROR(" No meaning in VSCF - shouldnt be here "," WHATS UP? "); 
}

/**
*   Prepare Eta0 
* */
void Vscf::CalculateTensorEta0() 
{
   MIDASERROR(" No meaning in VSCF - shouldnt be here "," WHATS UP? "); 
}

/**
*   Caluclate Xi vector. For VSCF pointless.  
* */
void Vscf::CalculateXi
   (  DataCont& arEta
   ,  const std::string& arOperName
   ,  In& arIoper
   ,  Nb& arExptValue
   )
{
   MIDASERROR(" Xi is pointless for VSCF - shouldnt be here "," WHATS UP? "); 
}

/**
*   Caluclate Xi vector. For VSCF pointless.  
* */
void Vscf::CalculateXi
   (  TensorDataCont& arEta
   ,  const std::string& arOperName
   ,  In& arIoper
   ,  Nb& arExptValue
   )
{
   MIDASERROR(" Xi is pointless for VSCF - shouldnt be here "," WHATS UP? "); 
}


