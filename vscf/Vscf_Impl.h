/**
************************************************************************
* 
* @file    Vscf_Impl.h
*
* @date    26-01-2018
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Implementation of VSCF template functions.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef VSCF_IMPL_H_INCLUDED
#define VSCF_IMPL_H_INCLUDED

#include "vscf/Vscf.h"
#include "vcc/TensorDataCont.h"
#include "input/Input.h"

using namespace midas;

/**
 *
 **/
template
   <  class SOLVER
   >
auto Vscf::RunComplexRspEqSolve
   (  std::vector<ComplexVector<DataCont,ZeroVector<> > >& y
   ,  MidasComplexFreqShiftedTransformer& midas_trf
   ,  const std::string& arKey
   ) 
   -> decltype(std::declval<SOLVER>().Solution())
{
   SOLVER ies(midas_trf,y);

   InitializeLinearSolverFromRspCalcDef(ies, mpVscfCalcDef);
   ies.SetSaveToDisc(mpVscfCalcDef->ItEqSaveToDisc());
   ies.CheckLinearDependence() = mpVscfCalcDef->RspItEqCheckLinearDependenceOfSpace();
   ies.RelativeConvergence() = mpVscfCalcDef->RspRelConv();
   
   ies.AddRestart(mpVscfCalcDef->Name()+arKey);
   for(auto it = mpVscfCalcDef->ItEqRestartVector().begin(); it!=mpVscfCalcDef->ItEqRestartVector().end(); ++it)
   {
      ies.AddRestart(*it);
   }
   
   if(!ies.Solve())
   {
      MidasWarning("Complex Linear Solver not converged! :(");
   }

   return ies.Solution();
}

/**
 *
 **/
template
   <  class SOLVER
   >
void Vscf::TensorRspEigSolve
   (  SOLVER& aSolver
   ,  bool aRight
   )
{
   // Add restarts
   std::string key = std::string("_rsp_eigvec_") + std::string(aRight ? "":"left_");
   for(const auto& restart : this->mpVscfCalcDef->ItEqRestartVector())
   {
      auto restart_key = restart + key + "tensor";
      Mout  << " Try to add restart vector with basename: " << restart_key << std::endl;
      aSolver.AddRestart(restart_key);
   }

   // Solve the equations
   Mout << " Solving eigenvalue equations..." << std::endl;
   if (  !aSolver.Solve()
      )
   {
      Mout << " Tensor rsp eig solver not converged :( " << std::endl;
      MidasWarning("Tensor rsp eig solver not converged");
   }

   // Save eigvals to disc
   auto&& eigvals = aSolver.GetEigenvalues();
   {
      const std::string eigval_re_filename = mpVscfCalcDef->Name() + "_rsp_eigval_re" + (aRight? "":"_left") + "_tensor";
      const std::string eigval_im_filename = mpVscfCalcDef->Name() + "_rsp_eigval_im" + (aRight? "":"_left") + "_tensor";
      
      DataCont eigval_re(eigvals.Re(), "InMem", eigval_re_filename, true);
      DataCont eigval_im(eigvals.Im(), "InMem", eigval_im_filename, true);
   }

   // Save eigvecs to disc
   auto&& eigvecs = aSolver.GetSolVec();
   {
      const std::string eigvec_filename = mpVscfCalcDef->Name() + key + "tensor";

      if (  !this->mpVscfCalcDef->GetVccRspDecompInfo().empty()
         )
      {
         MidasWarning("CP-VCC response eigenvectors are NOT recompressed before write to disc!");
      }
      
      for(In i=0; i<eigvecs.size(); ++i)
      {
         const std::string eigvec_filename_i = eigvec_filename + "_" + std::to_string(i);
         eigvecs[i].WriteToDisc(eigvec_filename_i);
      }
   }

   // Analysis
   if (  !eigvecs.back().CheckOverflow()
      )
   {
      // Convert solution vector to DataCont
      auto size = eigvecs.size();
      std::vector<DataCont> dc_solvec;
      dc_solvec.reserve(size);
      for(size_t i=0; i<size; ++i)
      {
         dc_solvec.emplace_back(DataContFromTensorDataCont(eigvecs[i]));
      }

      RspEigOutput(eigvals.Re(), dc_solvec);
   }
}


/**
* Drive calculation of response functions.
* */
template
   <  class VEC_T
   >
void Vscf::RspFuncDrv()
{
   Out72Char(Mout,' ','*','*');
   Mout << "\n\n Begin Response Function Calculation\n\n"  << endl;

   if (mpVscfCalcDef->RspIoLevel()>I_5) 
      Mout << " In RspFuncDrv; find response functions" << endl;
   Mout << " Calculate " << mpVscfCalcDef->NrspFuncs() << " response functions." << endl;

   if (  !mpVscfCalcDef->Variational()
      )
   {
      if constexpr   (  std::is_same_v<VEC_T, DataCont>
                     )
      {
         this->CalculateEta0(); 
      }
      else if constexpr (  std::is_same_v<VEC_T, TensorDataCont>
                        )
      {
         this->CalculateTensorEta0(); 
      }
      else
      {
         auto type = std::string(std::type_index(typeid(std::decay<VEC_T>::type)).name());
         MIDASERROR("Vscf::RspFuncDrv not implemented for type: " + type);
      }
   }
   PrepareRspOpers<VEC_T>();    // Prepare the response operators
   FindRspEqToSolve();   // Find the response equations to solve 
   RspEqSolveDrv<VEC_T>();      // Solve the response equations

   if (mpVscfCalcDef->GetNumVccF() != C_0)
   {
      CalcNumVccF();
//      CalcNumVccF<VEC_T>();
   }
   
   RspContract();        // Contract response vectors with vectors to get final response functions
   PertTheoryZPVC();     // Construct the PT ZPVC to the response operators
//   RspContract<VEC_T>();        // Contract response vectors with vectors to get final response functions
//   PertTheoryZPVC<VEC_T>();     // Construct the PT ZPVC to the response operators

   Mout << "\n\n Results for response functions " << endl;
   Out72Char(Mout,'+','-','+');
   for (In i_rsp=I_0;i_rsp<mpVscfCalcDef->NrspFuncs();i_rsp++) 
   {
      Mout << "Order: " << (mpVscfCalcDef->GetRspFunc(i_rsp)).GetNorder() << " ";
      mpVscfCalcDef->RspFuncOut(i_rsp);
   }
   Out72Char(Mout,'+','-','+');

   mpVscfCalcDef->PrintRspToFile();

   Mout << "\n\n End of Response Function Calculation \n\n"  << endl;

   std::vector<RspFunc> sos_rsp_funcs;
   if(mpVscfCalcDef->GetSosRsp())
   {   
      // If there are excited states calculated, use potentially info 
      if (mpVscfCalcDef->GetRspNeig()>I_0)
      {
         SosRspFunc(sos_rsp_funcs);
         DoIR();
         DoRaman();
//         SosRspFunc<VEC_T>(sos_rsp_funcs);
//         DoIR<VEC_T>();
//         DoRaman<VEC_T>();
      }
      Mout << "N SOS RF = " << sos_rsp_funcs.size() << endl;

      // add the SOS response functions to the total set of
      // response functions
      for(In i=0;i<sos_rsp_funcs.size();++i)
      {
         mpVscfCalcDef->AddRspFunc(sos_rsp_funcs[i]);
      }
   }

   // mbh: if some approximate frequency dependent response functions
   // has been requested, calculate them here
   // Mout << "NOW DO " << mpVscfCalcDef->NApproxRspFunc() << " APPROX RF" << endl;
   if(mpVscfCalcDef->NApproxRspFunc()>I_0) 
   {
      Mout << "\n\n Results for APPROXIMATE(!) response functions " << endl;
      Out72Char(Mout,'+','-','+');
      for(In iar=I_0;iar<mpVscfCalcDef->NrspFuncs();iar++) 
      {
         if(!(mpVscfCalcDef->GetRspFunc(iar)).GetApproxRspFunc())
            continue;
         // Calculate f-function
         for(In sos=I_0;sos<sos_rsp_funcs.size();sos++) 
         {
            // Calculate the F function.
            if(sos_rsp_funcs[sos]==mpVscfCalcDef->GetRspFunc(iar)) 
            {
               // Get static part
               Nb f_static=sos_rsp_funcs[sos].Value();
               // Now get all frq. dep. parts!
               for(In sos2=I_0;sos2<sos_rsp_funcs.size();sos2++) 
               {
                  bool match=true;
                  if(sos_rsp_funcs[sos2].GetNopers()!=(mpVscfCalcDef->GetRspFunc(iar)).GetNopers())
                     continue;
                  for(In op=I_0;op<sos_rsp_funcs[sos].GetNopers();op++) 
                  {
                     if(sos_rsp_funcs[sos].GetRspOp(op)!=sos_rsp_funcs[sos2].GetRspOp(op))
                        match=false;
                  }
                  if(match) 
                  {
                     // we have a match! Print result
                     Nb f_dynamic=sos_rsp_funcs[sos2].Value();
                     Nb f_func=f_dynamic/f_static;
                     Mout << "F FUNCTION = " << f_func << " , " 
                          << sos_rsp_funcs[sos2].GetRspFrq(I_1) << " , #";
                     for(In oper=I_0;oper<sos_rsp_funcs[sos2].GetNopers();oper++)
                        Mout << sos_rsp_funcs[sos2].GetRspOp(oper);
                     Mout << "# , " << (mpVscfCalcDef->GetRspFunc(iar)).Value()*f_func << endl;
                     RspFunc rsp_out=sos_rsp_funcs[sos2];
                     Nb value=(mpVscfCalcDef->GetRspFunc(iar)).Value()*f_func;
                     rsp_out.SetValue(value);
                     Mout << rsp_out << endl;
                  }
               }
            }
         }
      }
      Out72Char(Mout,'+','-','+');
   }
   if (mpVscfCalcDef->DoDensityMatrixAnalysis())
   {
      this->DensityMatrixAnalysis(); 
//      this->DensityMatrixAnalysis<VEC_T>(); 
   } 

   Out72Char(Mout,' ','*','*');
}

/**
* Prepare the response operators
* */
template
   <  class VEC_T
   >
void Vscf::PrepareRspOpers()
{
   // Loop through response functions and extract operator names.
   std::set<std::string> rsp_ops;
   In first_order=I_0;
   for (In i_rsp=I_0; i_rsp<mpVscfCalcDef->NrspFuncs(); ++i_rsp)
   {
      In n_order = (mpVscfCalcDef->GetRspFunc(i_rsp)).GetNorder();
      if (  n_order == I_1
         )
      {
         ++first_order;
      }
      for(In i_ord=I_0; i_ord<std::abs(n_order%I_10); ++i_ord)
      {
         rsp_ops.insert((mpVscfCalcDef->GetRspFunc(i_rsp)).GetRspOp(i_ord));
      }
   }

   // List operator names.
   In n_rsp_ops = rsp_ops.size();
   mpVscfCalcDef->SetNrspOps(n_rsp_ops);
   Mout << "\n There are " << n_rsp_ops << " different response operators:" << std::endl;
   for(const auto& op : rsp_ops)
   {
         Mout << "    Oper = " << op << std::endl;
   }

   In not_found = I_0;
   In i_c = I_0;
   for (const auto& op : rsp_ops)
   {
      // Find operator number
      std::string oper_name = op;
      In i_oper = gOperatorDefs.GetOperatorNr(oper_name);
      mpVscfCalcDef->AddRspOp(oper_name, i_c);
  
      // Check that non-energy operators does not contain kinetic energy operator terms
      if (  gOperatorDefs[i_oper].GetOperWithKeoTerms()
         && !  OpDef::msOpInfo[oper_name].GetType().Is(oper::energy)
         )
      {
         MIDASERROR("Kinetic energy operator terms detected in non-energy operator " + oper_name + ", please investigate");
      }
      
      if (  i_oper > -I_1
         )
      {
         // Calculate rhs response intermediate vectors 
         std::string s1 = "_eta_vec_"; 
         if (  !mpVscfCalcDef->Variational()
            )
         {
            s1 = "_xi_vec_";
         }
         std::string s = mpVscfCalcDef->Name() + s1 + oper_name;
         Nb expt_val = C_0;

         // Initialize pointer to vector
         std::unique_ptr<VEC_T> eta_vec = nullptr;

         // If DataCont
         if constexpr   (  std::is_same_v<VEC_T, DataCont>
                        )
         {
            eta_vec = std::make_unique<DataCont>(NrspPar(), C_0, "OnDisc", s, true); 
         }
         // If TensorDataCont
         else if constexpr (  std::is_same_v<VEC_T, TensorDataCont>
                           )
         {
            // Niels: We do not have the necessary info (TensorDecompInfo) available to initialize the TensorDataCont here
            // This will be done in the CalculateRhs function
            eta_vec = std::make_unique<TensorDataCont>();
         }
         else
         {
            auto type = std::string(std::type_index(typeid(std::decay<VEC_T>::type)).name());
            MIDASERROR("Vscf::PrepareRspOpers not implemented for type: " + type);
         }

         // Perform calculation
         this->CalculateRhs(*eta_vec, oper_name, i_oper, expt_val);

         mpVscfCalcDef->AddRspOpExptVal(expt_val, i_c);

         // Write TensorDataCont solution
         if constexpr   (  std::is_same_v<VEC_T, TensorDataCont>
                        )
         {
            eta_vec->WriteToDisc(s + "_tensor");
         }
      }
      else 
      {
         ++not_found;
         Mout << " Operator " << oper_name << " NOT FOUND." << std::endl;
      }

      ++i_c;
   }

   // Catch error
   if (  not_found > 0
      )
   {
      MIDASERROR("Some response operators have not been defined.");
   }
}

/**
*  Drive solution of response equations AND construction of various vectors. 
*
*  Templated for DataCont and TensorDataCont
* */
template
   <  class VEC_T
   >
void Vscf::RspEqSolveDrv()
{
   this->SolveZerothOrderEq<VEC_T>();    // Solve N_order = 0 equations
   this->SolveFirstOrderEq<VEC_T>();     // Solve all N_order = 1 standard equations
   this->SolveVccMEq<VEC_T>();           // Solve for VCC M vector.
   this->SolveXToXTransitions<VEC_T>();  // solve all N_order = -11 equations
   this->CreateSigmaVectors<VEC_T>();    // Create sigma intermediates for use in quad. rsp.
   this->SolveSecondOrderEq<VEC_T>();    // Solve all N_order = 2 equations (for cubic rsp)
   this->CalcNonVarEtaX<VEC_T>();        // For non-variational methods, calcluate etaX vectors if necessary.
}

/**
*  Solve all zeroth-order equations
* */
template
   <  class VEC_T
   >
void Vscf::SolveZerothOrderEq() 
{
   // Zeoth order left equations , THERE SHOULD BE AT MOST ONE
   In i_0l_begin=I_0;
   std::vector<In> n_0l;
   In n_eqs_tot = mpVscfCalcDef->NrspVecs();
   for(In i=I_0; i<n_eqs_tot; ++i)
   {
      if (  mpVscfCalcDef->GetRspVecInf(i).GetNorder() < I_0
         ) 
      {
         ++i_0l_begin;
      }
      else 
      {
         if (  mpVscfCalcDef->GetRspVecInf(i).GetNorder() != I_0
            || mpVscfCalcDef->GetRspVecInf(i).GetRight()
            ) 
         {
            break;
         }
         n_0l.push_back(i);
      }
   }
   
   if (  n_0l.size() > I_0
      )  
   {
      Mout << " Solve zeroth-order multiplier equations " << std::endl;
      
      if (  n_0l.size() > I_1
         )
      {
         MIDASERROR(" Surprised to find more than one left 0-order vector."); 
      }

      // Solve the actual equations for t-bar. 
      std::string seta = mpVscfCalcDef->Name() + "_eta0_vec"; 
      std::string key = "_mul0_vec_";
      bool right = false;

      if constexpr   (  std::is_same_v<VEC_T, DataCont>
                     )
      {
         MidasVector frq_vec(I_1,C_0);
         MidasVector gamma_vec(I_1,C_0);
         std::vector<DataCont> rsp_rhss(I_1);
         {
            DataCont eta0; 
            eta0.GetFromExistingOnDisc(NrspPar(), seta);
            eta0.SaveUponDecon(); 
            rsp_rhss[I_0]=eta0; 
            rsp_rhss[I_0].Scale(C_M_1);  // Right hand side is -eta0.
         }

         RspEqSolve(I_1,rsp_rhss,frq_vec, key, gamma_vec, right);
      }
      else if constexpr (  std::is_same_v<VEC_T, TensorDataCont>
                        )
      {
         // Read RHS and scale by -1
         std::vector<TensorDataCont> rhs(1);
         rhs[0].ReadFromDisc(seta + "_tensor");
         rhs[0].Scale(-C_1);

         // Set up frequency vectors (not used here)
         MidasVector frq_vec(I_1,C_0);
         MidasVector gamma_vec(I_1,C_0);

         // Solve equations
         auto tensor_key = key + "tensor";
         this->TensorRspEqSolve(rhs, tensor_key, frq_vec, gamma_vec, right);
      }
      else
      {
         auto type = std::string(std::type_index(typeid(std::decay<VEC_T>::type)).name());
         MIDASERROR("Vscf::SolveZerothOrderEq not implemented for type: " + type);
      }

      // Set evaluation flag
      mpVscfCalcDef->GetRspVecInf(i_0l_begin).SetHasBeenEval(true);
   }
}

/**
*  Solve all first order equations
**/
template
   <  class VEC_T
   >
void Vscf::SolveFirstOrderEq() 
{
   In n_eqs_tot = mpVscfCalcDef->NrspVecs();

   // Set up storage mode for DataCont version
   std::string storage_mode = "OnDisc";
   if (  mpVscfCalcDef->VecStorage()==I_0
      )
   {
      storage_mode = "InMem";
   }

   // First order right equations of standard type.
   std::vector<In> n_1r;
   std::vector<In> asym_1r;
   for(In i=I_0; i<n_eqs_tot; ++i)
   {
      const auto& vecinf = mpVscfCalcDef->GetRspVecInf(i);
      if (  vecinf.GetNorder() == I_1
         && vecinf.GetRight()
         && !vecinf.IsNonStandardType()
         ) 
      {
         if (  vecinf.GetRspOp(I_0).find("ANTI") != string::npos
            && mpVscfCalcDef->GetAlsoLinearAsym()
            ) 
         {
            asym_1r.push_back(i);
         }
         else 
         {
            n_1r.push_back(i);
         }
      }
   }
   std::sort(n_1r.begin(), n_1r.end());   
      
   if (  n_1r.size() == 0
      )
   {
      return;
   }

   // Find right hand sides 
   Mout << " Indices of first-order right parameter equations: " << n_1r << std::endl;
   std::set<std::string> set_of_rhs;
   for(In i=I_0; i<n_1r.size(); ++i)
   { 
      In i_r=n_1r[i];
   
      if (  gDebug
         )
      {
         Mout << " Solving for vector: " << mpVscfCalcDef->GetRspVecInf(i_r) << std::endl;
      }
      
      // Find rhs..
      if (  mpVscfCalcDef->GetRspVecInf(i_r).GetNorder() == I_1
         )
      {
         if (  gDebug
            )
         {
            Mout << " requires RHS with operator " << mpVscfCalcDef->GetRspVecInf(i_r).GetRspOp(I_0) << std::endl;
         }

         set_of_rhs.insert(mpVscfCalcDef->GetRspVecInf(i_r).GetRspOp(I_0));
      }
   }
   
   // Create right hand sides of 1st ord. eq.
   Mout << " There are " << set_of_rhs.size() << " required rhs vectors " << std::endl;
   std::vector<VEC_T> rsp_rhss;
   rsp_rhss.reserve(mpVscfCalcDef->GetRspRedDimMax());
   MidasVector frq_vec(n_1r.size(), C_0);
   MidasVector gamma_vec(n_1r.size(), C_0);

   for(const auto& oper : set_of_rhs)
   {
      for(In i_eq=I_0; i_eq<n_1r.size(); ++i_eq)
      { 
         In i = n_1r[i_eq];
         if (  mpVscfCalcDef->GetRspVecInf(i).GetRspOp(I_0) == oper
            )
         {
            // Set frequency and gamma for i_eq
            frq_vec[i_eq] = mpVscfCalcDef->GetRspVecInf(i).GetRspFrq(I_0);
            gamma_vec[i_eq] = mpVscfCalcDef->GetRspVecInf(i).Gamma();
            
            Mout << " pairing frq: " << frq_vec[i_eq] << " with gamma: " << gamma_vec[i_eq] << std::endl;

            // Get precalculated RHS vector form disc.
            // For VSCF: Actually rhs = S^2 g = -(eta_i, eta_i)
            //           And eta_i contains plus and minus eta_i in it.
            // For VCI:  Eta vector.
            // For VCC:  Xi vector.
            std::string s1 = mpVscfCalcDef->Variational() ? "_eta_vec_" : "_xi_vec_"; 
            std::string vecname = mpVscfCalcDef->Name() + s1 + oper;

            // Niels: What is this one used for???
            std::string s2 = mpVscfCalcDef->Name() + "_rsp_rhs_vec_" + std::to_string(i_eq); 

            if constexpr   (  std::is_same_v<VEC_T, DataCont>
                           )
            {
               DataCont vec;
               vec.GetFromExistingOnDisc(NrspPar(),vecname);
               vec.SaveUponDecon(true);
               rsp_rhss.push_back(DataCont());
               rsp_rhss[i_eq].NewLabel(s2);
               rsp_rhss[i_eq] = vec; 

               In n_scale = NrspPar();
               if (!mpVscfCalcDef->DoNoPaired()) n_scale /= I_2;
               rsp_rhss[i_eq].Scale(C_M_1,I_0,n_scale);
               rsp_rhss[i_eq].ChangeStorageTo(storage_mode);
               rsp_rhss[i_eq].SaveUponDecon(false);
            }
            else if constexpr (  std::is_same_v<VEC_T, TensorDataCont>
                              )
            {
               TensorDataCont tdc_vec;
               tdc_vec.ReadFromDisc(vecname + "_tensor");
               rsp_rhss.emplace_back(tdc_vec);

               // We are not doing the strange "partial scaling" with TensorDataCont. Do we need to?
               MidasWarning("Niels: First-order response RHSs are simply scaled by -1 for TensorDataCont. Is that correct?");

               rsp_rhss[i_eq].Scale(C_M_1);
            }
         }
      }
   }
   
   // Solve 1-order parameter equations 
   if (  mpVscfCalcDef->IoLevel() > I_10
      )
   {
      Mout << " frequency vector " << frq_vec << endl;
   }
   
   // DataCont version
   if constexpr   (  std::is_same_v<VEC_T, DataCont>
                  )
   {
      this->RspEqSolve(n_1r.size(),rsp_rhss,frq_vec,"_p1rsp_vec_",gamma_vec);
   }
   // TensorDataCont version
   else if constexpr (  std::is_same_v<VEC_T, TensorDataCont>
                     )
   {
      this->TensorRspEqSolve(rsp_rhss, "_p1rsp_vec_tensor", frq_vec, gamma_vec);
   }
   // Error
   else
   {
      auto type = std::string(std::type_index(typeid(std::decay<VEC_T>::type)).name());
      MIDASERROR("Vscf::SolveFirstOrderEq not implemented for type: " + type);
   }

   In i_new_rsp = n_1r.size();
   for(In i=I_0; i<n_1r.size(); ++i) 
   {
      // Set evaluation flag
      In i_r = n_1r[i];
      this->mpVscfCalcDef->GetRspVecInf(i_r).SetHasBeenEval(true);

      // Check if we need to calculate asymmetric response vectors
      if (  !mpVscfCalcDef->GetAlsoLinearAsym()
         )
      {
         continue;
      }

      /* mbh: at this point we can also set the asymmetric response vectors, they are simply
      *  related to the symmetric ones via a phase factor! (Only in the case of an approximate
      *  electronic asym. linear response function)         
      * 
      *  find out whether the current response vector should be copied to an asym one*/
      std::string new_name;
      bool found_oper = false;
      In j_r = I_0;
      Nb el_asym_frq = C_0, el_sym_frq = C_0;
      for(In j=I_0; j<asym_1r.size(); ++j) 
      {
         j_r=asym_1r[j];
         std::string test_str = this->mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0);
         test_str.erase(test_str.find("_ANTI"), 5);
         const auto& op_info_sym  = OpDef::msOpInfo[mpVscfCalcDef->GetRspVecInf(i_r).GetRspOp(I_0)];
         const auto& op_info_asym = OpDef::msOpInfo[mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0)];
         el_sym_frq  = op_info_sym .GetFrq(0);
         el_asym_frq = op_info_asym.GetFrq(0);
         if (  mpVscfCalcDef->GetRspVecInf(i_r).GetRspOp(I_0) == test_str
            && mpVscfCalcDef->GetRspVecInf(i_r).GetRspFrq(I_0) == mpVscfCalcDef->GetRspVecInf(j_r).GetRspFrq(I_0)
            && el_asym_frq == el_sym_frq
            )
         {
            if (  gDebug
               )
            {
               Mout << "Adding Rsp. vec. i = " << i_new_rsp << " j_r = " << j_r << std::endl;
            }
            
            // Set new_name
            if constexpr   (  std::is_same_v<VEC_T, DataCont>
                           )
            {
               new_name = this->mpVscfCalcDef->Name()+"_p1rsp_vec_"+std::to_string(i_new_rsp);
            }
            else if constexpr (  std::is_same_v<VEC_T, TensorDataCont>
                              )
            {
               new_name = this->mpVscfCalcDef->Name()+"_p1rsp_vec_tensor_"+std::to_string(i_new_rsp);
            }

            ++i_new_rsp;
            this->mpVscfCalcDef->GetRspVecInf(j_r).SetHasBeenEval(true);
            found_oper=true;
         }
         if (  found_oper
            )
         {
            break;
         }
      }
      if (  !found_oper
         )
      {
         continue;
      }

      // Find scaling factor for asym rsp vec
      Nb scale_fac=C_0;
      if (  mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0).find("X") != mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0).npos
         )
      {
         // XX, XY, or XZ: if it is Z, we just take that
         // otherwise we go with X
         if (  mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0).find("Z") != mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0).npos
            )
         {
            scale_fac=mpVscfCalcDef->GetAsymScale(2);
         }
         else
         {
            scale_fac=mpVscfCalcDef->GetAsymScale(0);
         }
      }
      else if  (  mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0).find("Y") != mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0).npos
               )
      {
         // YY, or YZ: if it is Z, we just take that
         // otherwise we go with X
         if (  mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0).find("Z") != mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0).npos
            )
         {
            scale_fac=mpVscfCalcDef->GetAsymScale(2);
         }
         else
         {
            scale_fac=mpVscfCalcDef->GetAsymScale(1);
         }
      }
      else
      {
         scale_fac = mpVscfCalcDef->GetAsymScale(2);
      }

      // Set final scaling factor
      scale_fac=(-el_asym_frq/scale_fac);

      if (  gDebug
         )
      {
         Mout  << "Scale fac for: " << mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(I_0)
               << " is: " << scale_fac << std::endl;
      }

      std::string base_name = this->mpVscfCalcDef->Name()+"_p1rsp_vec_";

      if constexpr   (  std::is_same_v<VEC_T, DataCont>
                     )
      {
         std::string name = this->mpVscfCalcDef->Name()+"_p1rsp_vec_"+std::to_string(i);
         DataCont rsp_vec;
         rsp_vec.GetFromExistingOnDisc(NrspPar(), name);
         rsp_vec.SaveUponDecon(true);
         DataCont new_rsp_vec;
         new_rsp_vec.SetNewSize(NrspPar());
         new_rsp_vec.NewLabel(new_name);
         new_rsp_vec.SaveUponDecon(true);
         new_rsp_vec=rsp_vec;
         new_rsp_vec.Scale(scale_fac);
      }
      else if constexpr (  std::is_same_v<VEC_T, TensorDataCont>
                        )
      {
         TensorDataCont rsp_vec;
         rsp_vec.ReadFromDisc(base_name + "tensor_" + std::to_string(i));
         rsp_vec.Scale(scale_fac);
         rsp_vec.WriteToDisc(new_name);
      }

   }

   // Now move the response vectors such that they correspond to the
   // number they have in the RspVecInf set.
   if (  mpVscfCalcDef->GetAlsoLinearAsym()
      ) 
   {
      // Set bool
      constexpr bool tensor = std::is_same_v<VEC_T, TensorDataCont>;

      In i_1r_begin=n_1r[I_0];
      // first move sym. to name_tmp_1, etc...
      for (In i=I_0; i<n_1r.size(); ++i) 
      {
         std::string sym_new_name=mpVscfCalcDef->Name() + "_p1rsp_vec_" + (tensor ? "tensor_" : "") + std::to_string(i) + "_tmp";
         std::string name = mpVscfCalcDef->Name() + "_p1rsp_vec_" + (tensor ? "tensor_" : "") + std::to_string(i);

         // Remove old file if it exists
         if (  InquireFile(sym_new_name)
            )
         {
            std::remove(sym_new_name.c_str());
         }

         std::rename(name.c_str(), sym_new_name.c_str());

//         DataCont d;
//         d.GetFromExistingOnDisc(NrspPar(),name);
//         d.SaveUponDecon(true);
//         d.NewLabel(sym_new_name);
         if (  gDebug
            )
         {
            Mout << "Moving sym: " << i << " to tmp..." << std::endl;
         }
      }
      // Move all the asym. vectors
      for(In i=I_0; i<asym_1r.size(); ++i) 
      {
         In j_r=asym_1r[i];
         std::string asym_new_name = mpVscfCalcDef->Name()+"_p1rsp_vec_" + (tensor ? "tensor_" : "")+std::to_string(j_r-i_1r_begin);
         std::string name = mpVscfCalcDef->Name()+"_p1rsp_vec_" + (tensor ? "tensor_" : "")+std::to_string(i+n_1r.size());

         // Remove old file if it exists
         if (  InquireFile(asym_new_name)
            )
         {
            std::remove(asym_new_name.c_str());
         }

         std::rename(name.c_str(), asym_new_name.c_str());
//         DataCont d;
//         d.GetFromExistingOnDisc(NrspPar(),name);
//         d.SaveUponDecon(true);
//         d.NewLabel(asym_new_name);
         if (  gDebug
            )
         {
            Mout << "Moving asym: " << i+n_1r.size() << " to: " << j_r-i_1r_begin << std::endl;
         }
      }
      // Now move all the sym vectors
      for(In i=I_0; i<n_1r.size(); ++i) 
      {
         In j_r=n_1r[i];
         std::string sym_new_name=mpVscfCalcDef->Name()+"_p1rsp_vec_" + (tensor ? "tensor_" : "")+std::to_string(j_r-i_1r_begin);
         std::string name=mpVscfCalcDef->Name()+"_p1rsp_vec_" + (tensor ? "tensor_" : "")+std::to_string(i)+"_tmp";

         // Remove old file if it exists
         if (  InquireFile(sym_new_name)
            )
         {
            std::remove(sym_new_name.c_str());
         }

         std::rename(name.c_str(), sym_new_name.c_str());

//         DataCont d;
//         d.GetFromExistingOnDisc(NrspPar(),name);
//         d.SaveUponDecon(true);
//         d.NewLabel(sym_new_name);
         if (  gDebug
            )
         {
            Mout << "Moving sym: " << i << "_tmp to: " << j_r-i_1r_begin << std::endl;
         }
      }
   }
   Mout << " Solved all the needed response equations I could." << std::endl;
}


/**
 * Solve driver for different solver types (using the it_solver framework)
 *
 *    @param aSolver       The solver
 *    @param aRestartKey   Name extension for restart vector
 **/
template
   <  class SOLVER
   >
void Vscf::TensorRspEqSolveImpl
   (  SOLVER& aSolver
   ,  const std::string& aRestartKey
   )
{
   // Initialize from calcdef
   InitializeLinearSolverFromRspCalcDef(aSolver, this->mpVscfCalcDef);

   // Initialize internal variables
   aSolver.Initialize();

   // Add restarts
   for(const auto& restart : this->mpVscfCalcDef->ItEqRestartVector())
   {
      auto restart_name = restart + aRestartKey;

      Mout  << " Try to add restart vector with basename: " << restart_name << std::endl;
      aSolver.AddRestart(restart_name);
   }

   // Solve the equations
   Mout << " Solving linear equations..." << std::endl;
   if (  !aSolver.Solve()
      )
   {
      Mout << " Tensor rsp solver not converged :( " << std::endl;
      MidasWarning("Tensor rsp solver not converged");
   }

   // Write solution to disk
   auto&& solution = aSolver.Solution();
   {
      const std::string sol_filename = mpVscfCalcDef->Name() + aRestartKey + "_";

      if (  !this->mpVscfCalcDef->GetVccRspDecompInfo().empty()
         )
      {
         MidasWarning("CP-VCC response solution vectors are NOT recompressed before write to disc!");
      }
      
      for(In i=0; i<solution.size(); ++i)
      {
         const std::string sol_filename_i = sol_filename + "_" + std::to_string(i);
         solution[i].WriteToDisc(sol_filename_i);
      }
   }

   // Solution-vector analysis (if no overflow)
   if (  !solution.back().CheckOverflow()
      )
   {
      // Convert solution vector to DataCont
      auto size = solution.size();
      std::vector<DataCont> dc_solvec;
      dc_solvec.reserve(size);
      for(size_t i=0; i<size; ++i)
      {
         dc_solvec.emplace_back(DataContFromTensorDataCont(solution[i]));
      }

      this->LinRspOutput(dc_solvec);
   }
}

/**
 * Solve M(omega_f) (A + omega_f*I) = F*R(f) for M(omega_f).
 **/
template
   <  class VEC_T
   >
void Vscf::SolveVccMEq()
{
   // NOT IMPL!!!
   if constexpr   (  std::is_same_v<VEC_T, TensorDataCont>
                  )
   {
      MIDASERROR("Vscf::SolveVccMEq not implemented for TensorDataCont!");
   }
   else
   {
      std::string storage = "OnDisc";
      if (mpVscfCalcDef->VecStorage()==I_0)
         storage = "InMem";

      // Find indices for M equations.
      vector<In> m_idx;
      for (In i=I_0; i<mpVscfCalcDef->NrspVecs(); ++i)
      {
         const RspVecInf& vecinf = mpVscfCalcDef->GetRspVecInf(i);
         if (vecinf.GetNorder() == I_1 &&
             vecinf.GetNonStandardType() == "M")
            m_idx.push_back(i);
      }

      In neqs = m_idx.size();
      if (neqs == 0)
         return;

      Mout << " Solving for VCC M vectors." << endl;
      
      // Setup right-hand sides and frequencies.
      vector<DataCont> rhss;     // The right hand sides of the equations, i.e. F*R(f) for state f.
      rhss.reserve(neqs);
      MidasVector freqs(neqs);   // The frequencies used to shift A.
      MidasVector gamma(neqs,C_0);
      for (In i=I_0; i<neqs; ++i)
      {
         const RspVecInf& vecinf = mpVscfCalcDef->GetRspVecInf(m_idx[i]);
         In state = vecinf.RightState();
         freqs[i] = -vecinf.GetRspFrq(I_0);

         DataCont tmp_fr(NrspPar(), C_0, storage, "tmp", false);
         CalculateFR(tmp_fr, state);
         tmp_fr.Scale(-C_1);
         rhss.push_back(tmp_fr);
         rhss.back().NewLabel(mpVscfCalcDef->Name() + "_rsp_vcc_fr_" + std::to_string(state));
         rhss.back().SaveUponDecon();
      }

      // Solve equations.
      RspEqSolve(neqs, rhss, freqs, "_rsp_vcc_m_", gamma, false);

      // Set "evaluated" flag to true even though we don't know if equations are converged.
      for (In i=I_0; i<neqs; ++i) {
         mpVscfCalcDef->GetRspVecInf(m_idx[i]).SetHasBeenEval(true);
      }
   }
}

/**
*
*
**/
template
   <  class VEC_T
   >
void Vscf::SolveSecondOrderEq()
{
   // NOT IMPL!!!
   if constexpr   (  std::is_same_v<VEC_T, TensorDataCont>
                  )
   {
      MIDASERROR("Vscf::SolveVccMEq not implemented for TensorDataCont!");
   }
   else
   {
      /* ONLY FOR VCI CURRENTLY!
      if (!mpVscfCalcDef->Vci()) { 
         Mout << "The solution of second order equations is Only implemented for VCI currently!" << endl;
         return;
      }*/
      Mout << "Entering SolveSecondOrderEq" << endl;

      In n_eqs_tot = mpVscfCalcDef->NrspVecs();
      string storage_mode = "OnDisc";
      if (mpVscfCalcDef->VecStorage()==I_0) storage_mode = "InMem";
         

      // second order equations
      In i_2r_begin=I_0;
      vector<In> n_2r;
      vector<In> asym_2r;
      for (In i=I_0;i<n_eqs_tot;i++)
      {
         if (mpVscfCalcDef->GetRspVecInf(i).GetNorder() < I_2)
         {
            i_2r_begin++;
         }
         else
         {
            if (mpVscfCalcDef->GetRspVecInf(i).GetNorder() != I_2)
               break;
            if(mpVscfCalcDef->GetRspVecInf(i).GetNonStandardType()=="SECOND_ORDER_RSP_VEC")
               n_2r.push_back(i);
         }
      }
      Mout << "I have " << n_2r.size() << " 2nd order rsp. vecs. to solve" << endl;
      if (n_2r.size() == 0 )
         return;

      // mbh: check for vectors related by permutation
      vector<vector<In> > book_keep;
      for (In i=I_0;i<n_2r.size();i++)
      {
         In i_r=n_2r[i];
         string c_op1=mpVscfCalcDef->GetRspVecInf(i_r).GetRspOp(0);
         string c_op2=mpVscfCalcDef->GetRspVecInf(i_r).GetRspOp(1);
         Nb c_f1=mpVscfCalcDef->GetRspVecInf(i_r).GetRspFrq(0);
         Nb c_f2=mpVscfCalcDef->GetRspVecInf(i_r).GetRspFrq(1);
         vector<In> tmp_book;
         for (In j=i;j<n_2r.size();j++)
         {
            bool cont=false;
            for(In k=0;k<book_keep.size();k++) {
               for(In l=0;l<book_keep[k].size();l++) {
                  if(j==book_keep[k][l])
                     cont=true;
               }
            }
            if(cont)
               continue;
            if(i==j) {
               tmp_book.push_back(j);
               continue;
            }
            In j_r=n_2r[j];
            string op1=mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(0);
            string op2=mpVscfCalcDef->GetRspVecInf(j_r).GetRspOp(1);
            Nb f1=mpVscfCalcDef->GetRspVecInf(j_r).GetRspFrq(0);
            Nb f2=mpVscfCalcDef->GetRspVecInf(j_r).GetRspFrq(1);
            if( ((c_op1==op2 && c_f1==f2) && (c_op2==op1 && c_f2==f1)) ) {
               //if(gDebug)
                  Mout << "Equal SO rsp. vecs: " << endl << mpVscfCalcDef->GetRspVecInf(j_r)
                       << endl << mpVscfCalcDef->GetRspVecInf(i_r) << endl;
               tmp_book.push_back(j);
            }
         }
         if(tmp_book.size()>0) { 
            Mout << "Inserting book: " << tmp_book << endl;
            book_keep.push_back(tmp_book);
         }
      }
      vector<In> n_2r_unique;
      for(In i=0;i<book_keep.size();i++)
         n_2r_unique.push_back(n_2r[book_keep[i][0]]);
      vector<In> n_2r_save=n_2r;
      n_2r=n_2r_unique;
      Mout << "OLD: " << n_2r_save << endl;
      Mout << "NEW: " << n_2r << endl;

      // calculate the second order right-hand sides:
      //
      // VCI: -P(XY)F^X*\lambda_Y(\omega_y)
      
      vector<DataCont> rhss(n_2r.size());
      //rhss.reserve(mpVscfCalcDef->GetRspRedDimMax());
      MidasVector frq_vec(n_2r.size(),C_0);
      MidasVector gamma_vec(n_2r.size(),C_0);
      for (In i=I_0;i<n_2r.size();i++)
      {
         In i_r=n_2r[i];
         // loop over the XY YX permutations
         for(In i_permut=I_0;i_permut<I_2;i_permut++)
         {
            Nb frq=mpVscfCalcDef->GetRspVecInf(i_r).GetRspFrq(i_permut);
            frq_vec[i]+=frq;
            string oper=mpVscfCalcDef->GetRspVecInf(i_r).GetRspOp(i_permut);
            if(gDebug) {
               Mout << "Second order eq, op[" << i_permut << "]" << oper
                    << " and frq = " << frq_vec[i] << endl;
            }
            // find first order equation with this info
            In first_order_index=-I_1;
            for(In i_fo=I_0;i_fo<n_eqs_tot;i_fo++)
            {
               if(mpVscfCalcDef->GetRspVecInf(i_fo).GetNorder() < I_1)
                  continue;
               if(mpVscfCalcDef->GetRspVecInf(i_fo).GetNorder() > I_1)
                  MIDASERROR("Did not find requested first order equation");
               first_order_index++;
               Nb first_order_frq=mpVscfCalcDef->GetRspVecInf(i_fo).GetRspFrq(I_0);
               string first_order_op=mpVscfCalcDef->GetRspVecInf(i_fo).GetRspOp(I_0);
               if(first_order_frq==frq && first_order_op==oper) {
                  break;
               }
            }
            if(first_order_index==-I_1)
               MIDASERROR("First order equation not found!");
            string dc_name=mpVscfCalcDef->Name()+"_p1rsp_vec_"+std::to_string(first_order_index);
            DataCont first_order_vec;
            first_order_vec.GetFromExistingOnDisc(NrspPar(),dc_name);
            first_order_vec.SaveUponDecon(true);
            // contract this with the correct F matrix
            In oper_nr=-I_1;
            // here change operator to the one not defining 
            // the above first order vector
            oper=mpVscfCalcDef->GetRspVecInf(i_r).GetRspOp(I_1-i_permut%I_2);
            oper_nr=gOperatorDefs.GetOperatorNr(oper);
            if(oper_nr == -I_1)
               MIDASERROR("In SolveSecondOrderEq: Did not find the requested operator!");
            // Make transformation
            string so_name=mpVscfCalcDef->Name()+"_sec_ord_rhs_tmp_"+std::to_string(i);
            DataCont so_rhs(NrspPar(),C_0,"OnDisc",so_name,false);
            Nb expt_value=C_0;
            CalculateSigma(first_order_vec,so_rhs,oper,oper_nr,expt_value);
            if(i_permut==I_0)
               rhss[i]=so_rhs;
            else
               rhss[i].Axpy(so_rhs,C_1);
         }
         string rhs_name=mpVscfCalcDef->Name()+"_sec_ord_rhs_vec_"+std::to_string(i);
         rhss[i].NewLabel(rhs_name);
         rhss[i].Scale(C_M_1,I_0,NrspPar());
         rhss[i].SaveUponDecon(false);
      }
      // We have a vector of all the RHS
      // and a vector with all the frequencies
      RspEqSolve(n_2r.size(),rhss,frq_vec,"_unique_p2rsp_vec_",gamma_vec);
      // mbh: now treat the equivalent ones
      for(In i=0;i<book_keep.size();i++) {
         string u_name=mpVscfCalcDef->Name()+"_unique_p2rsp_vec_"+std::to_string(i);
         DataCont u_data;
         u_data.SetNewSize(I_0);
         u_data.GetFromExistingOnDisc(NrspPar(),u_name);
         u_data.SaveUponDecon(false);
         if(!u_data.ChangeStorageTo("InMem",true)) {
            MIDASERROR("In second order rsp part...");
         }
         for(In j=0;j<book_keep[i].size();j++) {
            DataCont data(u_data);
            data.SaveUponDecon(true);
            string name=mpVscfCalcDef->Name()+"_p2rsp_vec_"+std::to_string(book_keep[i][j]);
            data.NewLabel(name);
         }
      }
      n_2r=n_2r_save;
      Mout << " Solved all the needed SECOND ORDER response equations I could " << endl;
      for (In i=I_0;i<n_2r.size();i++) 
      {
         In i_r=n_2r[i];
         mpVscfCalcDef->GetRspVecInf(i_r).SetHasBeenEval(true);
      }
   }
}

/**
*  Compute all sigma vectors in order to compute matrix elements: 
*
*  <i|X|j> = F^X * U^(j) * U^i = sigma^(xj) * U^i
*
* */
template
   <  class VEC_T
   >
void Vscf::SolveXToXTransitions()
{
   // NOT IMPL!!!
   if constexpr   (  std::is_same_v<VEC_T, TensorDataCont>
                  )
   {
      MIDASERROR("Vscf::SolveXToXTransitions not implemented for TensorDataCont!");
   }
   else
   {
      // Make set of sigma vectors: sigma(x,i) = F^x * U^(i)

      In i_ref=I_0;
      for(In i=I_0;i<mpVscfCalcDef->NrspVecs();i++) 
      { // loop over all rspvecs and generate them
         // Select the right ones meaning n_order = -11
         if ((mpVscfCalcDef->GetRspVecInf(i)).GetNorder() == -I_11) 
         {
            //Get the correct eigenvector
            In eig_vec=(mpVscfCalcDef->GetRspVecInf(i)).LeftState();
            string vecname = mpVscfCalcDef->Name() + "_rsp_eigvec_" + std::to_string(eig_vec);
            DataCont Eig;
            Eig.GetFromExistingOnDisc(NrspPar(),vecname);
            Eig.SaveUponDecon();
            //get the right operator and oper. number
            string OperName=(mpVscfCalcDef->GetRspVecInf(i)).GetRspOp(I_0);
            Nb ExptValue=C_0;;
            In OperNr=gOperatorDefs.GetOperatorNr(OperName);
            if(OperNr == -I_1)
               MIDASERROR("In SolveXToX: Did not find the requested operator!");
            //set the output, initialize to 0 vector
            if (gDebug) 
            {
               Mout << "Calling calc. Sigma with eigenvector: " << vecname << endl
                    << Eig << endl;
            }
            DataCont Sigma(NrspPar(),C_0,"OnDisc",mpVscfCalcDef->Name()+"_sigma_vec_"+std::to_string(i_ref),true);
            CalculateSigma(Eig,Sigma,OperName,OperNr,ExptValue);
            if(gDebug) 
            {
               Mout << "After calc. Sigma, Sigma vector is: "
                    << mpVscfCalcDef->Name()+"_sigma_vec_"+std::to_string(i_ref) << endl
                    << Sigma << endl;
            }
            i_ref++;
         }
      }
   }
}

/**
*  Solve variational second order RE. Essentially means make a set of 
*  vectors:
*            <<X;Y,Z>>_(y,z) = P(X,Y,Z) * F^Z * L^X(wx) * L^Y(wy) = P(X,Y,Z) * vec^(z,x) * L^Y(wy)
*
*  Then use the ResidCont. Routine to make the vec * L contractions
**/
template
   <  class VEC_T
   >
void Vscf::CreateSigmaVectors() 
{
   // NOT IMPL!!!
   if constexpr   (  std::is_same_v<VEC_T, TensorDataCont>
                  )
   {
      MIDASERROR("Vscf::CreateSigmaVectors not implemented for TensorDataCont!");
   }
   else
   {
      // first check for order 
      In i_ref=I_0;
      for(In i=I_0;i<mpVscfCalcDef->NrspVecs();i++) 
      {
         if ((mpVscfCalcDef->GetRspVecInf(i)).GetNorder() == I_2
            && !(mpVscfCalcDef->GetRspVecInf(i)).IsNonStandardType()) 
         {
            // We have to generate a sigma vector.
            In lin_rsp=-I_1;
            DataCont rsp_vec;
            string rv_op;
            for(In i_rsp=I_0;i_rsp<mpVscfCalcDef->NrspVecs();i_rsp++) 
            {  
               if((mpVscfCalcDef->GetRspVecInf(i_rsp)).GetNorder() == I_1) 
               {
                  lin_rsp++;
                  if ((mpVscfCalcDef->GetRspVecInf(i)).GetRspOp(I_1) == 
                      (mpVscfCalcDef->GetRspVecInf(i_rsp)).GetRspOp(I_0) &&
                      (mpVscfCalcDef->GetRspVecInf(i)).GetRspFrq(I_1) == 
                      (mpVscfCalcDef->GetRspVecInf(i_rsp)).GetRspFrq(I_0)) 
                  {
                     rv_op=(mpVscfCalcDef->GetRspVecInf(i_rsp)).GetRspOp(I_0);
                     string vecname=mpVscfCalcDef->Name()+"_p1rsp_vec_"+std::to_string(lin_rsp);
                     rsp_vec.GetFromExistingOnDisc(NrspPar(),vecname);
                     rsp_vec.SaveUponDecon(true);
                     break;
                  }
               }
            }
            // We have the correct Rsp. vec. transform it by the right F.
            string OperName=(mpVscfCalcDef->GetRspVecInf(i)).GetRspOp(I_0);
            In OperNr=gOperatorDefs.GetOperatorNr(OperName);
            if(OperNr == -I_1)
               MIDASERROR("In CreateSigmaVectors: Did not find the requested operator!");
            // Make transformation
            string sigma_name=mpVscfCalcDef->Name()+"_qrf_vec_"+std::to_string(i_ref);
            DataCont sigma_vec(NrspPar(),C_0,"OnDisc",sigma_name,true);
            Nb ExptValue=C_0;
            CalculateSigma(rsp_vec,sigma_vec,OperName,OperNr,ExptValue);
            i_ref++;
         }
      }
   }
}

/**
 * For non-variational methods, calcluate etaX vectors if necessary.
 **/
template
   <  class VEC_T
   >
void Vscf::CalcNonVarEtaX()
{
   // NOT IMPL!!!
   if constexpr   (  std::is_same_v<VEC_T, TensorDataCont>
                  )
   {
      MIDASERROR("Vscf::CalcNonVarEtaX not implemented for TensorDataCont!");
   }
   else
   {
      if (mpVscfCalcDef->Variational())
         return;
      
      Mout << " Identifying and calculating required etaX vectors for non-variational wave function."
           << endl;

      // Set of operators for which etaX is needed.
      set<string> opers;
      
      // Loop over all response functions and check if etaX is needed.
      for (In i=I_0; i<mpVscfCalcDef->NrspFuncs(); ++i)
      {
         const RspFunc& rsp = mpVscfCalcDef->GetRspFunc(i);

         if (rsp.GetNorder() == -I_1)
            opers.insert(rsp.GetRspOp(I_0));
         else if (rsp.GetNorder() == I_2)
         {
            opers.insert(rsp.GetRspOp(I_0));
            opers.insert(rsp.GetRspOp(I_1));
         }
      }

      for (set<string>::const_iterator it=opers.begin(); it!=opers.end(); ++it)
      {
         // Get operator number;
         In i_oper = gOperatorDefs.GetOperatorNr(*it);
         if(i_oper == -1)
         {
            MIDASERROR("Operator "+*it+" not found in operators");
         }
         DataCont eta_vec(NrspPar(), C_0, "InMem", mpVscfCalcDef->Name()+"_eta_vec_"+*it, true); 
         Nb dummy = C_0;
         CalculateEta(eta_vec, *it, i_oper, dummy);
      }
   }
}


#endif /* VSCF_IMPL_H_INCLUDED */
