/**
************************************************************************
* 
* @file                IRData.cc
*
* Created:             20-9-2002
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*
* Short Description:   Implementing IRData class methods.
* 
* ?????? Last modified: Mon Oct 31, 2005  11:46PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <string>

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "input/OpInfo.h"
#include "vscf/IRData.h"

using namespace std;

void IRData::SetInitState(const In ai)
{
   mInitState = ai;
}

void IRData::SetFinalState(const In af)
{
   mFinalState = af;
}

void IRData::SetOmegafi(const Nb afi)
{
   mOmegafi = afi;
}

void IRData::SetMu(const oper::PropertyType aIndex, const Nb aValue)
{
   if       (oper::dipole_x == aIndex) mMuX = aValue;
   else if  (oper::dipole_y == aIndex) mMuY = aValue;
   else if  (oper::dipole_z == aIndex) mMuZ = aValue;
   else
      Mout << "IRData::SetMu() error: Bad index." << endl;
}

Nb IRData::GetMu2() const
{
   return (mMuX*mMuX + mMuY*mMuY + mMuZ*mMuZ);
}

Nb IRData::GetOscStrength() const
{
   return C_2/C_3*mOmegafi*GetMu2();
}

ostream& operator<<(ostream& as, const IRData& ar)
{
   as.setf(ios_base::scientific, ios_base::floatfield);
   midas::stream::ScopedPrecision(5, as);
   as << "IR data for transition (atomic units): "
      << ar.mFinalState << "<-" << ar.mInitState << endl
      << "   omega_fi =      " << ar.mOmegafi << endl << endl
      << "   mu_[xyz]       (" << ar.mMuX << ", " << ar.mMuY << ", "
      << ar.mMuZ << ")" << endl
      << "   |mu|^2 =        " << ar.GetMu2()*2*ar.mOmegafi << endl
      << "   Osc. Stregth =  " << ar.GetOscStrength() << endl
      << endl;
   return as;
}
