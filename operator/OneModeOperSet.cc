/**
************************************************************************
*
* @file                OneModeOperSet.cc
*
* Created:             06-06-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Class for storing the simple one mode operators from OpDef
*
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/
#include "operator/OneModeOperSet.h"
#include "operator/StateTransferOneModeOper.h"

// std headers
#include <map>
#include <vector>

// midas headers
#include "inc_gen/Const.h"
#include "input/Input.h"
#include "input/GlobalOperatorDefinitions.h"
#include "util/Io.h"
#include "operator/OneModeOper.h"
#include "inc_gen/TypeDefs.h"

/**
 *
 **/
In OneModeOperSet::NumberOfOneModeOpers() const
{
   return mData.size();
}

/**
 *
 **/
LocalOperNr OneModeOperSet::GetLocalOperNr
   (  GlobalOperNr aGOpNr
   )  const
{
   std::map<GlobalOperNr, LocalOperNr>::const_iterator it = mConvert.find(aGOpNr);
   if (it == mConvert.end())
   {
      MIDASERROR("The operator you are looking for does not exist");
   }
   return it->second;
}

/**
 *
 **/
LocalOperNr OneModeOperSet::GetLocalOperNr
   (  In aGOpNr
   )  const
{
   In i = 0;
   while(aGOpNr != mData[i])
   {
      i++;
   }
   return i;
}

void OneModeOperSet::InsertOper(In aIn)
{
   //We do not want to insert the same OMO twice
   std::map<GlobalOperNr, LocalOperNr>::iterator it = mConvert.find(aIn);
   if (it != mConvert.end())
   {
      return;
   }
   bool first_oper = !this->NumberOfOneModeOpers();
   bool only_unit_added = (this->NumberOfOneModeOpers() == I_1) && this->mUnitOpPossEx;
   mConvert.insert(std::make_pair(aIn, this->NumberOfOneModeOpers()));
   mData.push_back(aIn);

   const std::unique_ptr<OneModeOperBase<Nb>>& oper = gOperatorDefs.GetOneModeOper(aIn);

   // Checks for state-transfer operators
   bool state_transfer = (oper->Type() == OneModeOperBase<Nb>::OperType::STATETRANSFER);
   bool unit_oper = oper->GetmIsUnitOperator();
   bool st_or_unit = (state_transfer || unit_oper);
   if (  only_unit_added
      || first_oper
      )
   {
      this->mStateTransferOnly = state_transfer;
   }
   else if  (  this->mStateTransferOnly != st_or_unit
            && !only_unit_added
            )
   {
      MIDASERROR("If using state-transfer operators in a mode, no other types of operators may be added! Added operator: '" + oper->OperString() + "'.");
   }

   // Set IJMax
   if (  oper->Type() == OneModeOperBase<Nb>::OperType::STATETRANSFER
      )
   {
      this->mStateTransferIJMax = std::max(this->mStateTransferIJMax, static_cast<StateTransferOneModeOper<Nb>*>(oper.get())->MaxIJ());
   }

   // If the operator is generally initialized there is nothing more to do here
   if(!oper->IsPoly())
   {
      return;
   }

   // Set values used for integral evaluation
   if(oper->GetLDer())
   {
      if(oper->GetRDer() != I_0)
      {

         if(oper->GetPow() > mHeighestDDQpowDD)
         {
            mHeighestDDQpowDD = oper->GetPow();
         }
      }
      else if(oper->GetPow() > mHeighestDDQpow)
      {
         mHeighestDDQpow = oper->GetPow();
      }
   }
   else if(oper->GetRDer() != I_0)
   {
      if(oper->GetPow() == I_0)
      {
         if(oper->GetRDer()>mHeighestDDpow)
         {
            mHeighestDDpow = oper->GetRDer();
         }
      }
      else if(oper->GetPow() > mHeighestQpowDD)
      {
         mHeighestQpowDD = oper->GetPow();
      }
   }

   if(oper->GetPow() > mHeighestQpow)
   {
      mHeighestQpow = oper->GetPow();
   }

   if(oper->GetPow() == I_2 && (!oper->GetLDer() || oper->GetRDer() > I_0) )
   {
      //if(mQSquarePoss != -1) MIDASERROR("QSquareOper already included.");
      mQSquarePoss = mData.size() - I_1;
   }

   if (  oper->GetPow() == I_1
      && (  !oper->GetLDer()
         || oper->GetRDer() > I_0
         )
      )
   {
      mQPosition = mData.size() - I_1;
   }

   if(oper->GetPow() == I_0 && oper->GetRDer() == I_0)
   {
      if(mUnitOpPoss != -1) MIDASERROR("Unit operator already included.");
      mUnitOpPossEx = true;
      mUnitOpPoss = mData.size()-1;
   }
}
