/**
************************************************************************
*
* @file                DeltaFunctionOneModeOper.h
*
* Created:             19-02-2020
*
* Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:   Delta-function operator delta(Q-Q')
*
* Last modified:
*
* Detailed  Description:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef DELTAFUNCTIONONEMODEOPER_H_INCLUDED
#define DELTAFUNCTIONONEMODEOPER_H_INCLUDED

#include "operator/OneModeOper.h"
#include "libmda/numeric/float_eq.h"

/**
 * Operator that acts as a Dirac delta function
 **/
template
   <  typename T
   >
class DeltaFunctionOneModeOper
   :  public OneModeOperBase<T>
{
   public:
      //! Alias
      using Base = OneModeOperBase<T>;
      using OperPtr = typename Base::OperPtr;
      using type_t = typename Base::OperType;
      
      //! Sanity check
      static_assert(std::is_floating_point_v<T>, "T must be floating-point for DeltaFunctionOneModeOper.");

   private:
      //! Position of delta function in mass-weighted coordinates (not freq scaled) like the grid bounds.
      T mX = C_0;

      //!
      type_t TypeImpl
         (
         )  const override
      {
         return type_t::DELTAFUNCTION;
      }

      //!
      std::string ShowTypeImpl
         (
         )  const override
      {
         return std::string("DeltaFunctionOneModeOper");
      }

      //! Compare
      virtual bool CompareImpl
         (  const OperPtr& aPtr
         )  const override
      {
         auto ptr = static_cast<const DeltaFunctionOneModeOper*>(aPtr.get());
         return libmda::numeric::float_eq(this->GetX(), ptr->GetX());
      }


   public:
      //! Delete default c-tor
      DeltaFunctionOneModeOper() = delete;

      //! Delete copy c-tor
      DeltaFunctionOneModeOper
         (  const DeltaFunctionOneModeOper&
         )  = delete;

      //! Delete copy assignment
      DeltaFunctionOneModeOper& operator=
         (  const DeltaFunctionOneModeOper&
         )  = delete;

      //! Constructor
      DeltaFunctionOneModeOper
         (  In aRDer
         ,  bool aLDer
         ,  T aX
         )
         :  OneModeOperBase<T>
               (  aRDer
               ,  aLDer
               )
         ,  mX(aX)
      {
         // Set the operator to be a Coriolis term if we have any left or right derivatives.
         // NB: This type of operator is not necessarily part of the Watson Hamiltonian. It could also be the flux operator!
         this->mIsCoriolisTerm = (aLDer || aRDer);
      }

      //! Get the original (operator) function string
      const std::string GetOrigOperString
         (
         )  const override
      {
         return "DELTA(" + std::to_string(this->mX) + ")";
      }

      //! Get X
      T GetX
         (
         )  const
      {
         return this->mX;
      }
};

#endif /* DELTAFUNCTIONONEMODEOPER_H_INCLUDED */

