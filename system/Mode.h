/*
************************************************************************
*  
* @file                Mode.h
*
* Created:             28-01-2015
*
* Author:              Carolin Koenig (ckoenig@chem.au.dk)
*
* Short Description:   Keeps all information about one mode,
*                      including subsystems  
* 
* Last modified:       07-07-2015 (carolin)
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef MODE_H
#define MODE_H
#include "inc_gen/TypeDefs.h"
#include "nuclei/Nuclei.h"

enum class ModeType: int  {VIB,TRANS,ROT,VRT};
ostream& operator<<(ostream& os,const ModeType& arModeType);


class Mode
{
   protected:
      ModeType                                     mModeType = ModeType::VRT;

      std::vector<GlobalSubSysNr>                  mSubSysNrs;
      std::vector<MidasVector>                     mSubModes;
      std::set<std::set<GlobalSubSysNr>>           mRigid;
      std::vector<GlobalSubSysNr>                  mSubSysNrsSigma;
      std::vector<MidasVector>                     mSigma;

      Nb                                           mFreq = -C_1;
      bool                                         mFreqInitialized = false;

      std::string                                  mTag="";
  
      In  GetLocalSubSysNr(const GlobalSubSysNr& arNr) const;
      In  GetLocalSubSysNrSigma(const GlobalSubSysNr& arNr) const;
      std::set<GlobalSubSysNr> ActiveSet() const;
      void AddSubSysToSigma(const GlobalSubSysNr& arSubSysNrs, const MidasVector& arSigma)
      {
         mSubSysNrsSigma.emplace_back(arSubSysNrs);
         mSigma.emplace_back(arSigma);
      }
      void SetAllRigid(const bool& arSave=true) 
      {
         if (arSave) MidasAssert(mRigid.size()==I_0,"Rigid set of mode already set in Mode::SetAllRigid");
         mRigid.emplace(mSubSysNrs.begin(),mSubSysNrs.end());
      }
      std::set<GlobalSubSysNr> GetRigidForSubSys(const GlobalSubSysNr& arNo) const;
      void RemoveSubSysFromRigid(const GlobalSubSysNr& arNo);
   
   public:
      Mode(const ModeType& arModeType): mModeType(arModeType) {;}
      Mode(const GlobalSubSysNr& arSubSysNr, MidasVector&& arrMode, const ModeType& arModeType=ModeType::VRT):
         mModeType(arModeType)
         {AddSubSystems({arSubSysNr}, {arrMode}); if (mModeType==ModeType::TRANS || mModeType==ModeType::ROT) SetAllRigid();}
      Mode(const std::vector<GlobalSubSysNr>& arSubSysNr, std::vector<MidasVector>&& arrMode, const ModeType& arModeType=ModeType::VRT):
         mModeType(arModeType)
         {AddSubSystems(arSubSysNr,arrMode); if (mModeType==ModeType::TRANS || mModeType==ModeType::ROT) SetAllRigid(); }

      //Modify
      void Add(const Mode&);
      void AddSubSystems(const std::vector<GlobalSubSysNr>& arSubSysNr, std::vector<MidasVector>&& arrMode, const bool& arKeepSigma=false); 
      void AddSubSystems(const std::vector<GlobalSubSysNr>& arSubSysNr,const  std::vector<MidasVector>& arMode, const bool& arKeepSigma=false); 
      void AddSigma(vector<GlobalSubSysNr>&& arSubSysNrs, vector<MidasVector>&& arSigma)
            {mSubSysNrsSigma=arSubSysNrs; mSigma = arSigma;}

      void ShiftSubSysNrs(const In& arShift);
      void SetModeType(const ModeType& arModeType) {mModeType=arModeType; if (mModeType==ModeType::TRANS || mModeType==ModeType::ROT) SetAllRigid(); }
      void SetFreq(const Nb& arNb) {mFreqInitialized=true; mFreq=arNb;}
      void ScaleFreq(const Nb& arNb) {mFreq*=arNb;}
      void Clear() {mModeType=ModeType::VRT;mSubSysNrs.clear();mSubModes.clear();mSubSysNrsSigma.clear();
             mSigma.clear();mFreq=-C_1; mFreqInitialized= false; mRigid.clear(); mTag="";}
      void SetRigidSet(const std::set<std::set<GlobalSubSysNr>> arRigid) 
      {
         MidasAssert(mRigid.size()==I_0,"trying to overwrite rigid information of a mode");
         std::set<GlobalSubSysNr> intersect;
         In no_rid=I_0;
         for (auto it=arRigid.begin();it!=arRigid.end();++it)
         {
            std::set_intersection(mSubSysNrs.begin(),mSubSysNrs.end(),it->begin(),it->end(),std::inserter(intersect,intersect.end()));
            no_rid+=it->size();
         }
         MidasAssert(intersect.size()==no_rid,"Rigid information does not fit to mode");
         mRigid=arRigid;
      }
      void LimitToSubSysSet(const std::set<GlobalSubSysNr>& arSet);
      void SetTag(const std::string& arTag)  {mTag=arTag;}

      //GetInfo
      MidasVector*  GetpSubMode(const GlobalSubSysNr& arSubSys)  
         {return &mSubModes.at(GetLocalSubSysNr(arSubSys));}     
      MidasVector  GetSubMode(const GlobalSubSysNr& arSubSys) const   
         {return mSubModes.at(GetLocalSubSysNr(arSubSys));}     
      ModeType GetModeType() const {return mModeType;}
      Nb Freq() const {if (!mFreqInitialized) MidasWarning("Asking for unitialized frequency"); return mFreq;}
      Nb Entry(const GlobalSubSysNr& arSubSys, const LocalAtomNr& arNucl, const In& arXYZ) const; 
      Nb SigmaEntry(const GlobalSubSysNr& arSubSys, const LocalAtomNr& arNucl, const In& arXYZ) const; 
      std::set<std::set<GlobalSubSysNr>> GetRigid() const {return mRigid;} 
      std::string Tag() const {return mTag;}

      //Infos
      bool IsSubSysIn(const GlobalSubSysNr&) const;
      bool IsSubSysInSigma(const GlobalSubSysNr&) const;
      bool IsRigidInFc(const std::set<GlobalSubSysNr>& arSubSysSet) const;
      bool HasSigma() const {return !mSigma.empty();}
      bool HasFreq() const {return mFreqInitialized;}
      std::set<GlobalSubSysNr> SubSysInMode() const;
        
      friend ostream& operator<<(ostream& os,const Mode& arMode); 
      friend Mode operator*(const Nb& arFac,const Mode& arMode1); 
      friend Nb ModeDotSigma(const Mode& arMode1, const Mode& arMode2); 
      friend Nb ModeDotMode(const Mode& arMode1, const Mode& arMode2); 
      friend std::set<std::set<GlobalSubSysNr>> GetCommonRigid(const Mode& arMode1, const Mode& arMode2);
};
#endif //Mode_H
