/**
 *******************************************************************************
 * 
 * @file    OdeInfoOutput.cc
 * @date    22-05-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/

#include <iostream>
#include <string>
#include "ode/OdeInfoOutput.h"
#include "ode/OdeInfo.h"
#include "util/UnderlyingType.h"
#include "util/Io.h"
#include "ode/OdeErrorNorm.h"

/***************************************************************************//**
 * Output OdeInfo something like this:
 * ~~~
 *    DATABASESAVEVECTORS            = false
 *    DRIVER                         = MIDAS
 *    ERRORTYPE                      = 0 (enum OdeErrorType)
 *    FIXEDSTEPSIZE                  = false
 *    IMPROVEDINITIALSTEPSIZE        = true
 *    INITIALSTEPSIZE                = 1.000000E-02
 *    ...
 * ^a ^b                            ^c
 * ~~~
 *
 * @note
 *    Currently implemented with a not particularly pretty lambda function for
 *    extracting the value part of the result of operator<< with an any_type.
 *    Feel free to improve. 
 *    Also, not all the types have operator<< overloads in any_type, so a few
 *    cases are handled specially.
 *    MBH, May 2019.
 *
 * @param[out] arOs
 *    ostream to output to.
 * @param[in] arInfo
 *    OdeInfo whose settings are printed.
 * @param[in] aIndent
 *    Indent each line by this many spaces (`^a^).
 * @param[in] aLeftWidth
 *    Width of left-flushed left column (labels) (`^b`).
 * @param[in] aEqualSign
 *    String separating the key and value columns. (You should space-pad this.)
 * @return
 *    The arOs, after outputting.
 ******************************************************************************/
std::ostream& midas::ode::Output
   (  std::ostream& arOs
   ,  const OdeInfo& arInfo
   ,  unsigned int aIndent
   ,  unsigned int aLeftWidth 
   ,  const char* aEqualSign
   )
{
   // Store old format flags.
   const auto old_flags = arOs.flags();

   // Width, precisions and such.
   const std::string tab(aIndent, ' ');
   const std::string eq(aEqualSign);
   const Uin w = aLeftWidth;
   arOs << std::left;
   arOs << std::boolalpha;

   // For extracting the value of an any_type.
   // Expects operator<< for any_type to be "any_type: type = <type>, value = <value>".
   // Not super duper pretty.
   const std::string any_type_match = "value = ";
   auto get_anytype_val = [&any_type_match,f=arOs.flags()](const midas::ode::OdeInfo::mapped_type& v) -> std::string
      {
         std::stringstream ss;
         ss.flags(f);
         ss << v;
         auto s = ss.str();
         auto pos = s.find(any_type_match);
         if (pos != std::string::npos)
         {
            return s.substr(pos+any_type_match.size());
         }
         else
         {
            return s;
         }
      };

   for(const auto& kv: arInfo)
   {
      arOs << tab << std::setw(w) << kv.first << eq;
      // Some special cases that don't have easy printouts through any_type.
      if (kv.first == "TIMEINTERVAL")
      {
         const auto& p = kv.second.template get<std::pair<Nb,Nb>>();
         arOs << p;
      }
      else if(kv.first == "ERRORTYPE")
      {
         const auto& val = kv.second.template get<midas::ode::detail::OdeErrorType>();
         arOs << midas::util::ToUType(val) << " (enum OdeErrorType)";
      }
      // But generally we can extract using the lambda.
      else
      {
         arOs << get_anytype_val(kv.second);
      }
      arOs << '\n';
   }

   arOs << std::flush;
   arOs.flags(old_flags);
   return arOs;
}

/***************************************************************************//**
 *
 ******************************************************************************/
std::ostream& operator<<
   (  std::ostream& arOs
   ,  const midas::ode::OdeInfo& arInfo
   )
{
   return midas::ode::Output(arOs, arInfo);
}
