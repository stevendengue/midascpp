/**
************************************************************************
* 
* @file                 OdeIntegrator_Decl.h
* 
* Created:              28-03-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Declaration of OdeIntegrator class
* 
* Last modified:        March 28, 2018 (Niels Kristian Madsen)
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ODEINTEGRATOR_DECL_H_INCLUDED
#define ODEINTEGRATOR_DECL_H_INCLUDED

#include <memory>

#include "ode/OdeDriverBase.h"
#include "ode/ODE_Macros.h"

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

/**
 * Integrator class for solving ordinary differential equations of type:
 *
 * dy/dt = f(y,t)
 *
 * The requirements to the ODE template parameter are:
 *    -  Type aliases: vec_t, param_t, step_t (NB: The elements of vec_t must be of type param_t!)
 *       The y and dy/dt vectors are of type vec_t, and t is a step_t.
 *    -  Methods: Derivatives (return dy/dt vector), 
 *       ProcessNewVector (modify and check new y vector)
 **/
template
   <  typename ODE
   >
class OdeIntegrator
{
   public:
      //! Type of y vector container
      ODE_ALIAS(ODE);

   private:
      //! Pointer to concrete ODE driver
      std::unique_ptr<OdeDriverBase<ODE> > mDriver = nullptr;

   public:
      /** @name Constructors **/
      //!@{

      //! Disallow copy constructor
      OdeIntegrator
         (  const OdeIntegrator&
         )  =  delete;

      //! Disallow move constructor
      OdeIntegrator
         (  OdeIntegrator&&
         )  =  delete;

      //! Constructor
      explicit OdeIntegrator
         (  ODE* apOde
         ,  const midas::ode::OdeInfo& aInfo
         );

      //!@}

      /** @name Operator overloads **/
      //!@{

      //! Disallow copy assignment
      OdeIntegrator& operator=
         (  const OdeIntegrator&
         )  =  delete;

      //! Disallow move assignment
      OdeIntegrator& operator=
         (  OdeIntegrator&&
         )  =  delete;

      //!@}
      
      /** @name Integration interface **/
      //!@{

      //! Integrate with start vector
      void Integrate
         (  vec_t& arYStart
         );

      //! Write summary
      void Summary
         (
         )  const;

      //! Get t's from database
      const std::vector<step_t>& GetDatabaseT
         (
         )  const;

      //! Get y's from database
      const std::vector<vec_t>& GetDatabaseY
         (
         )  const;

      //!@}
};

#endif /* ODEINTEGRATOR_DECL_H_INCLUDED */
