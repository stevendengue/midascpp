/**
************************************************************************
* 
* @file                 OdeStepperBase_Decl.h
* 
* Created:              28-03-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Declaration of OdeStepperBase class
* 
* Last modified:        March 28, 2018 (Niels Kristian Madsen)
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ODESTEPPERBASE_DECL_H_INCLUDED
#define ODESTEPPERBASE_DECL_H_INCLUDED

#include <memory>
#include <string>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

#include "ode/OdeInfo.h"
#include "ode/OdeErrorNorm.h"
#include "ode/ODE_Macros.h"
#include "ode/OdeWrapper.h"

/**
 * Stepper base class
 **/
template
   <  typename ODE
   >
class OdeStepperBase
{
   public:
      //! Aliases
      ODE_ALIAS(ODE);

   protected:
      //! The step sized used in the latest step
      step_t mUsedStep = static_cast<step_t>(-1.);

      //! Old time
      step_t mTOld = static_cast<step_t>(0.);

      //! Number of good steps (no reduced step size)
      In mNGoodSteps = I_0;

      //! Number of bad steps
      In mNBadSteps = I_0;

      //! Absolute threshold
      Nb mAbsTol = C_I_10_6;

      //! Relative threshold
      Nb mRelTol = C_I_10_6;

      //! Dense output
      bool mDenseOutput = true;

      //! Use FSAL property?
      bool mUseFsal = true;

      //! Error-norm type
      midas::ode::detail::OdeErrorType mErrorType = midas::ode::detail::OdeErrorType::MEAN;

      //! Fixed step size
      bool mFixedStep = false;

      //! IO level
      In mIoLevel = I_1;

   private:
      //! Overloadable implementation of Step
      virtual step_t StepImpl
         (  step_t& arT
         ,  step_t aTrialStep
         ,  vec_t& arY
         ,  vec_t& arDyDt
         ,  midas::ode::OdeWrapper<ODE>& arOde
         ,  step_t aDistanceToOutput
         ,  bool& arForceStop
         )  = 0;

      //! Overloadable implementation of Interpolate
      virtual vec_t InterpolateImpl
         (  step_t aTime
         ,  step_t aStep
         )  const = 0;

      //! Overloadable implementation of Order
      virtual In OrderImpl
         (
         )  const = 0;

      //! Overloadable implementation of Type
      virtual std::string TypeImpl
         (
         )  const = 0;

      //! Overloadable implementation of NextStepsize
      virtual step_t NextStepSizeImpl
         (
         )  const = 0;

      //! Overloadable implementation of Summary
      virtual void SummaryImpl
         (
         )  const;

   public:
      //! Constructor from OdeInfo
      explicit OdeStepperBase
         (  const midas::ode::OdeInfo& aInfo
         );

      //! Virtual destructor
      virtual ~OdeStepperBase() = default;

      //! Interface to Step
      step_t Step
         (  step_t& arT
         ,  step_t aTrialStep
         ,  vec_t& arY
         ,  vec_t& arDyDt
         ,  midas::ode::OdeWrapper<ODE>& arOde
         ,  step_t aDistanceToOutput
         ,  bool& arForceStop
         );

      //! Interface to Interpolate
      vec_t Interpolate
         (  step_t aTime
         ,  step_t aStep
         )  const;

      //! Interface to Type
      std::string Type
         (
         )  const;

      //! Interface to Order
      In Order
         (
         )  const;

      //! Get used step size
      step_t UsedStepSize
         (
         )  const;

      //! Get next step size
      step_t NextStepSize
         (
         )  const;

      //! Get tol
      Nb AbsTol
         (
         )  const;

      //! Get tol
      Nb RelTol
         (
         )  const;

      //! Get UseFsal
      bool UseFsal
         (
         )  const;

      //! Get mFixedStep
      bool FixedStep
         (
         )  const;

      //! Summary
      void Summary
         (
         )  const;

      //! Factory
      static std::unique_ptr<OdeStepperBase<ODE> > Factory
         (  const midas::ode::OdeInfo& aInfo
         );
};

#endif /* ODESTEPPERBASE_DECL_H_INCLUDED */

