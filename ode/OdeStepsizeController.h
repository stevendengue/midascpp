/**
************************************************************************
* 
* @file                 OdeStepsizeController.h
* 
* Created:              28-03-2018 (using code from 19-10-2015)
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk) and Kasper Monrad (monrad@post.au.dk)
* 
* Short Description:    Declaration of OdeStepsizeController class
* 
* Last modified:        March 28, 2018 (Niels Kristian Madsen)
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ODESTEPSIZECONTROLLER_H_INCLUDED
#define ODESTEPSIZECONTROLLER_H_INCLUDED

#include "libmda/numeric/float_eq.h"

namespace midas::ode
{

/**
 *
 **/
template
   <  typename T
   >
class OdeStepsizeController
{
   private:
      T mBeta = static_cast<T>(0.);
      T mAlpha = static_cast<T>(1./8.);
      T mSafe = static_cast<T>(0.9);
      T mMinScale = static_cast<T>(0.333);
      T mMaxScale = static_cast<T>(6.);

      In mControlIoLevel = I_0;

      T mNextStepSize = static_cast<T>(1.);
      T mErrOld = static_cast<T>(1.e-4);
      bool mReject = false;

   protected:
      //! Check step size (and update if needed)
      bool CheckStepsize
         (  T aErr
         ,  T& arStepSize
         )
      {
         if (  mControlIoLevel > I_7
            )
         {
            Mout  << " CheckStepsize: err = " << aErr << ", h = " << arStepSize << std::endl;
         }
         // Sanity check
         if (  libmda::numeric::float_neg(aErr)
            )
         {
            MIDASERROR("Negative error put into OdeStepsizeController!");
         }

         T scale = static_cast<T>(1.);
         if (  libmda::numeric::float_leq(aErr, T(1.))
            )
         {
            // Set scaling factor
            if (  libmda::numeric::float_numeq_zero(aErr, T(1.))
               )
            {
               scale = mMaxScale;
            }
            else
            {
               scale = std::min(this->mMaxScale, std::max(this->mSafe * std::pow(aErr, -this->mAlpha) * std::pow(mErrOld, this->mBeta), this->mMinScale));
            }

            // Set next step size.
            // After a rejection, we set maxscale = 1
            if (  mReject
               )
            {
               mNextStepSize = arStepSize * std::min(scale, static_cast<T>(1.));
            }
            else
            {
               mNextStepSize = arStepSize*scale;
            }
            mErrOld = std::max(aErr, static_cast<T>(1.e-4));           // Book keeping for next call
            mReject = false;

            if (  mControlIoLevel > I_7
               )
            {
               Mout  << " Step accepted. h_next = " << mNextStepSize << std::endl;
            }

            return true;
         }
         else
         {
            scale = std::max(this->mSafe * std::pow(aErr, -this->mAlpha), mMinScale);
            arStepSize *= scale;
            mReject = true;

            if (  mControlIoLevel > I_7
               )
            {
               Mout  << " Step rejected. Try h = " << arStepSize << std::endl;
            }

            return false;
         }
      }

   public:
      //! Constructor from OdeInfo
      explicit OdeStepsizeController
         (  const midas::ode::OdeInfo& aInfo
         )
         :  mBeta
               (  aInfo.template get<Nb>("ODESTEPSIZEBETA")
               )
         ,  mAlpha
               (  aInfo.template get<Nb>("ODESTEPSIZEALPHA")
               )
         ,  mSafe
               (  aInfo.template get<Nb>("ODESTEPSIZESAFE")
               )
         ,  mMinScale
               (  aInfo.template get<Nb>("ODESTEPSIZEMINSCALE")
               )
         ,  mMaxScale
               (  aInfo.template get<Nb>("ODESTEPSIZEMAXSCALE")
               )
         ,  mControlIoLevel
               (  aInfo.template get<In>("IOLEVEL")
               )
      {
      }

      //! Get next step size
      T NextStepSize
         (
         )  const
      {
         return this->mNextStepSize;
      }
};

} /* namespace midas::ode */

#endif /* ODESTEPSIZECONTROLLER_H_INCLUDED */
