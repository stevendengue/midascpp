/**
************************************************************************
* 
* @file                 Dopr5Stepper_Decl.h
* 
* Created:              28-03-2018 (using code from 28-09-2015)
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk) and Kasper Monrad (monrad@post.au.dk)
* 
* Short Description:    Declaration of Dopr5Stepper class
* 
* Last modified:
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef DOPR5STEPPER_DECL_H_INCLUDED
#define DOPR5STEPPER_DECL_H_INCLUDED

#include "ode/AdaptiveRungeKuttaStepper.h"
#include "ode/ODE_Macros.h"

/**
 * DOPR5 stepper
 **/
template
   <  typename ODE
   >
class Dopr5Stepper
   :  public AdaptiveRungeKuttaStepper<ODE>
{
   public:
      //! Typedefs
      using Control = typename AdaptiveRungeKuttaStepper<ODE>::Control;
      using Base = typename AdaptiveRungeKuttaStepper<ODE>::Base;
      ODE_ALIAS(Base);

   private:
      //! Intermediate derivatives
      vec_t mK2, mK3, mK4, mK5, mK6;

      //! Intermediates for interpolation
      vec_t mRcont1, mRcont2, mRcont3, mRcont4, mRcont5;

      //! Error vector
      vec_t mYerr;

      //! Saved FSAL vector
      vec_t mFsalDerivative;


      //! Implementation of Interpolate
      vec_t InterpolateImpl
         (  step_t aTime
         ,  step_t aStep
         )  const override;

      //! Implementation of Type
      std::string TypeImpl
         (
         )  const override;

      //! Implementation of Order
      In OrderImpl
         (
         )  const override;

      //! Calculate derivatives and return updated y vector
      vec_t CalculateDerivativesImpl
         (  step_t aT
         ,  step_t aStepSize
         ,  const vec_t& aY
         ,  const vec_t& aDyDt
         ,  midas::ode::OdeWrapper<ODE>& arOde
         )  override;

      //! Calculate error
      step_t ErrorEstimateImpl
         (  step_t aStep
         ,  const vec_t& aOldY
         ,  const vec_t& aNewY
         )  const override;

      //! Prepare dense output
      void PrepareDenseOutputImpl
         (  step_t aStepSize
         ,  const vec_t& aYOld
         ,  const vec_t& aYNew
         ,  const vec_t& aDyDtOld
         ,  const vec_t& aDyDtNew
         ,  midas::ode::OdeWrapper<ODE>& arOde
         )  override;

      //! Resize all vector members
      void ResizeVectorsImpl
         (  const vec_t&
         )  override;

      //! Do we use FSAL?
      bool Fsal
         (
         )  const override;

      //! Get saved FSAL derivative
      vec_t GetFsalDerivative
         (
         )  const override;

   public:
      //! Constructor
      explicit Dopr5Stepper
         (  const midas::ode::OdeInfo& aInfo
         );
};

#endif /* DOPR5STEPPER_DECL_H_INCLUDED */

