/**
************************************************************************
* 
* @file                 GslOdeDriver_Decl.h
* 
* Created:              24-04-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Declaration of GslOdeDriver class
* 
* Last modified:
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifdef ENABLE_GSL

#ifndef GSLODEDRIVER_DECL_H_INCLUDED
#define GSLODEDRIVER_DECL_H_INCLUDED

#include "util/IsDetected.h"
#include "util/type_traits/Complex.h"
#include "ode/OdeDriverBase.h"
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_errno.h>

/**
 * Driver for integrating ODEs using GSL.
 **/
template
   <  typename ODE
   >
class GslOdeDriver
   :  public OdeDriverBase<ODE>
{
   public:
      //! Aliases
      using Base = OdeDriverBase<ODE>;
      ODE_ALIAS(Base);

   protected:
      using Data = typename Base::Data;

   private:
      //! Stepper type
      const gsl_odeiv2_step_type* mStepperType = nullptr;
      In mOrder = -I_1;

      //! Error scaling factors (see the GSL documentation)
      double mAy = 1.;
      double mADyDt = 0.;

      //! Overload of integration method
      void IntegrateImpl
         (  vec_t& arY
         )  override;

      //! Overload of type method
      std::string TypeImpl
         (
         )  const override;

   public:
      //! Constructor
      explicit GslOdeDriver
         (  ODE* apOde
         ,  const midas::ode::OdeInfo& aInfo
         );
};

namespace midas::ode::detail
{

template
   <  typename VEC_T
   ,  typename INDEX
   >
using has_element_access_type = decltype(std::declval<VEC_T>()[std::declval<INDEX>()]);
/**
 *
 **/
template
   <  typename ODE
   >
void compare_vector_and_pointer
   (  const typename ODE::vec_t& aVec
   ,  const double* apPtr
   )
{
   constexpr bool has_element_access = midas::util::IsDetectedV<has_element_access_type, typename ODE::vec_t, In>;

   if constexpr   (  has_element_access
                  )
   {
      size_t width = 50;
      Mout  << std::setw(width) << "vec_t"
            << std::setw(width) << "pointer"
            << std::endl;
      In j=I_0;
      auto size = ODE_Size(aVec);
      for(In i=I_0; i<size; ++i)
      {
         std::ostringstream oss_vec, oss_ptr;
         oss_vec << std::setprecision(16) << "(" << std::real(aVec[i]) << ", " << std::imag(aVec[i]) << ")";
         oss_ptr << std::setprecision(16) << "(" << apPtr[j] << ", " << apPtr[j+I_1] << ")";
         j += I_2;

         Mout  << std::setw(width) << oss_vec.str()
               << std::setw(width) << oss_ptr.str()
               << std::endl;
      }
   }
}

//! Derivative wrapper
template
   <  typename ODE
   >
int gsl_derivative_wrapper
   (  double aT
   ,  const double* apY
   ,  double* apDyDt
   ,  void* apParams
   )
{
   // Get ODE
   auto& ode = *static_cast<midas::ode::OdeWrapper<ODE>*>(apParams);

   // Setup stuff in correct data format
   typename ODE::step_t t = aT;
   typename ODE::vec_t y_vec = ode.WRAP_ShapedZeroVector();
   ODE_DataFromPointer(y_vec, apY);

   // DEBUG
   if (  gDebug
      )
   {
      Mout  << "gsl_derivative_wrapper. Compare y." << std::endl;
      compare_vector_and_pointer<ODE>(y_vec, apY);
   }

   // Calculate derivative
   auto dydt_vec = ode.WRAP_Derivative(t, y_vec);

   // Transfer data to apDyDt
   ODE_DataToPointer(dydt_vec, apDyDt);

   // DEBUG
   if (  gDebug
      )
   {
      Mout  << "gsl_derivative_wrapper. Compare dy/dt." << std::endl;
      compare_vector_and_pointer<ODE>(dydt_vec, apDyDt);
   }

   // Return success
   return GSL_SUCCESS;
}

} /* midas::ode::detail */
#endif /* GSLODEDRIVER_DECL_H_INCLUDED */ 

#endif   /* ENABLE_GSL */
