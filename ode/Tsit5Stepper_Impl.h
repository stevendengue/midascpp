/**
************************************************************************
* 
* @file                 Tsit5Stepper_Impl.h
* 
* Created:              01-05-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Implementation of Tsit5Stepper class
* 
* Last modified:
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TSIT5STEPPER_IMPL_H_INCLUDED
#define TSIT5STEPPER_IMPL_H_INCLUDED

#include "ode/Tsit5Stepper_Decl.h"
#include "ode/ODE.h"
#include "ode/OdeErrorNorm.h"

/**
 * Constructor
 *
 * @param aInfo      OdeInfo
 **/
template
   <  typename ODE
   >
Tsit5Stepper<ODE>::Tsit5Stepper
   (  const midas::ode::OdeInfo& aInfo
   )
   :  AdaptiveRungeKuttaStepper<ODE>(aInfo)
{
}

/**
 * Overload of CalculateDerivativesImpl
 * NB: In order to use the FSAL property, the OdeDriver cannot modify aDyDt between steps!
 * This should be fine in MidasOdeDriver.
 *
 * @param aT
 * @param aStepSize
 * @param aY
 * @param aDyDt
 * @param arOde
 *
 * @return
 *    Updated y vector
 **/
template
   <  typename ODE
   >
typename Tsit5Stepper<ODE>::vec_t Tsit5Stepper<ODE>::CalculateDerivativesImpl
   (  step_t aT
   ,  step_t aStepSize
   ,  const vec_t& aY
   ,  const vec_t& aDyDt
   ,  midas::ode::OdeWrapper<ODE>& arOde
   )
{
   const auto& h = aStepSize;

   // Coefficients
   static constexpr Nb  c2 = 0.161, c3 = 0.327, c4 = 0.9, c5 = 0.9800255409045097 //, c6 = 1., c7 = 1.
                     ,  b1 = 0.09646076681806523, b2 = 0.01, b3 = 0.4798896504144996, b4 = 1.379008574103742
                     ,  b5 = -3.290069515436081, b6 = 2.324710524099774 //, b7 = 0.
//                     ,  bh1 = 0.001780011052226, bh2 = 0.000816434459657, bh3 = -0.007880878010262
//                     ,  bh4 = 0.144711007173263, bh5 = -0.582357165452555, bh6 = 0.458082105929187
//                     ,  bh7 = 1./66.
                     ,  a32 = 0.3354806554923570, a42 = -6.359448489975075, a52 = -11.74888356406283
                     ,  a43 = 4.362295432869581, a53 = 7.495539342889836, a54 = -0.09249506636175525
                     ,  a62 = -12.92096931784711, a63 = 8.159367898576159, a64 = -0.07158497328140100
                     ,  a65 = -0.02826905039406838;

   // Some coefs are defined in terms of the first ones
   static constexpr Nb  a21 = c2
                     ,  a31 = c3 - a32
                     ,  a41 = c4 - a42 - a43
                     ,  a51 = c5 - a52 - a53 - a54
                     ,  a61 = 1. - a62 - a63 - a64 - a65
                     ,  e1 = b1 - 0.001780011052226
                     ,  e2 = b2 - 0.000816434459657
                     ,  e3 = b3 + 0.007880878010262
                     ,  e4 = b4 - 0.144711007173263
                     ,  e5 = b5 + 0.582357165452555
                     ,  e6 = b6 - 0.458082105929187
                     ,  e7 = -1./66.;

   // Set mK1 = aDyDt. It is saved for dense output.
   mK1 = aDyDt;

   // Calculate mK2
   vec_t ytemp = aY;
   ODE_Axpy(ytemp, mK1, h*a21);
   mK2 = arOde.WRAP_Derivative(aT+c2*h, ytemp);

   // Calculate mK3
   ytemp = aY;
   ODE_Axpy(ytemp, mK1, h*a31);
   ODE_Axpy(ytemp, mK2, h*a32);
   mK3 = arOde.WRAP_Derivative(aT+c3*h, ytemp);

   // Calculate mK4
   ytemp = aY;
   ODE_Axpy(ytemp, mK1, h*a41);
   ODE_Axpy(ytemp, mK2, h*a42);
   ODE_Axpy(ytemp, mK3, h*a43);
   mK4 = arOde.WRAP_Derivative(aT+c4*h, ytemp);

   // Calculate mK5
   ytemp = aY;
   ODE_Axpy(ytemp, mK1, h*a51);
   ODE_Axpy(ytemp, mK2, h*a52);
   ODE_Axpy(ytemp, mK3, h*a53);
   ODE_Axpy(ytemp, mK4, h*a54);
   mK5 = arOde.WRAP_Derivative(aT+c5*h, ytemp);

   // Calculate mK6
   ytemp = aY;
   ODE_Axpy(ytemp, mK1, h*a61);
   ODE_Axpy(ytemp, mK2, h*a62);
   ODE_Axpy(ytemp, mK3, h*a63);
   ODE_Axpy(ytemp, mK4, h*a64);
   ODE_Axpy(ytemp, mK5, h*a65);
   auto xph = aT + h;
   mK6 = arOde.WRAP_Derivative(xph, ytemp);

   // Calculate mK7 and result (using a7i = bi)
   vec_t result = aY;
   ODE_Axpy(result, mK1, h*b1);
   ODE_Axpy(result, mK2, h*b2);
   ODE_Axpy(result, mK3, h*b3);
   ODE_Axpy(result, mK4, h*b4);
   ODE_Axpy(result, mK5, h*b5);
   ODE_Axpy(result, mK6, h*b6);
   mK7 = arOde.WRAP_Derivative(xph, result);

   // Calculate error
   ODE_Zero(this->mYerr);
   ODE_Axpy(mYerr, mK1, h*e1);
   ODE_Axpy(mYerr, mK2, h*e2);
   ODE_Axpy(mYerr, mK3, h*e3);
   ODE_Axpy(mYerr, mK4, h*e4);
   ODE_Axpy(mYerr, mK5, h*e5);
   ODE_Axpy(mYerr, mK6, h*e6);
   ODE_Axpy(mYerr, mK7, h*e7);

   return result;
}

/**
 * Prepare dense output.
 *
 * @param aStepSize        Step size
 * @param aYOld            Old y vector
 * @param aYNew            New y vector
 * @param aDyDtOld         dy/dt at t = t_old
 * @param aDyDtNew         dy/dt at t = t_old + aStepSize
 * @param arOde            ODE object for derivatives, etc.
 **/
template
   <  typename ODE
   >
void Tsit5Stepper<ODE>::PrepareDenseOutputImpl
   (  step_t aStepSize
   ,  const vec_t& aYOld
   ,  const vec_t& aYNew
   ,  const vec_t& aDyDtOld
   ,  const vec_t& aDyDtNew
   ,  midas::ode::OdeWrapper<ODE>& arOde
   )
{
   mDyDtOld = aDyDtOld;
}

/**
 * Calculate error
 *
 * @param aStep
 * @param aOldY
 * @param aNewY
 *
 * @return
 *    Error
 **/
template
   <  typename ODE
   >
typename Tsit5Stepper<ODE>::step_t Tsit5Stepper<ODE>::ErrorEstimateImpl
   (  step_t aStep
   ,  const vec_t& aOldY
   ,  const vec_t& aNewY
   )  const
{
   auto err_squared = midas::ode::OdeErrorNorm2<ODE>(this->mYerr, this->mAbsTol, this->mRelTol, aOldY, aNewY, this->mErrorType);

   return std::sqrt(err_squared);
}

/**
 * Implementation of Interpolate
 * NB: Requires mTOld to be set!
 * 
 * @param aTime      Time
 * @param aStep      Step size
 * @return
 *    Interpolated y for t=aTime
 **/
template
   <  typename ODE
   >
typename Tsit5Stepper<ODE>::vec_t Tsit5Stepper<ODE>::InterpolateImpl
   (  step_t aTime
   ,  step_t aStep
   )  const
{
   step_t t = (aTime - this->mTOld) / aStep;
   step_t t2 = t*t;

   const Nb bt1 = -1.0530884977290216*t*(t-1.3299890189751412)*(t2 - 1.4364028541716351*t + 0.7139816917074209)
          , bt2 = 0.1017*t2*(t2 - 2.1966568338249754*t + 1.2949852507374631)
          , bt3 = 2.490627285651252793*t2*(t2 - 2.38535645472061657*t + 1.57803468208092486)
          , bt4 = -16.54810288924490272*(t - 1.21712927295533244)*(t - 0.61620406037800089)*t2
          , bt5 = 47.37952196281928122*(t - 1.203071208372362603)*(t - 0.658047292653547382)*t2
          , bt6 = -34.87065786149660974*(t - 1.2)*(t - 0.666666666666666667)*t2
          , bt7 = 2.5*(t - 1.)*(t - 0.6)*t2;

   vec_t result = this->mDyDtOld;
   ODE_Axpy(result, mK1, aStep*bt1);
   ODE_Axpy(result, mK2, aStep*bt2);
   ODE_Axpy(result, mK3, aStep*bt3);
   ODE_Axpy(result, mK4, aStep*bt4);
   ODE_Axpy(result, mK5, aStep*bt5);
   ODE_Axpy(result, mK6, aStep*bt6);
   ODE_Axpy(result, mK7, aStep*bt7);

   return result;
}

/**
 * Implementation of Type
 *
 * @return
 *    Type
 **/
template
   <  typename ODE
   >
std::string Tsit5Stepper<ODE>::TypeImpl
   (
   )  const
{
   return std::string("Tsit5Stepper");
}

/**
 * Implementation of Order
 *
 * @return
 *    Order
 **/
template
   <  typename ODE
   >
In Tsit5Stepper<ODE>::OrderImpl
   (
   )  const
{
   return I_5;
}

/**
 * Resize vector members
 *
 * @param aShape     vec_t of correct shape
 **/
template
   <  typename ODE
   >
void Tsit5Stepper<ODE>::ResizeVectorsImpl
   (  const vec_t& aShape
   )
{
   ODE_SetShape(mK1, aShape);
   ODE_SetShape(mK2, aShape);
   ODE_SetShape(mK3, aShape);
   ODE_SetShape(mK4, aShape);
   ODE_SetShape(mK5, aShape);
   ODE_SetShape(mK6, aShape);
   ODE_SetShape(mK7, aShape);

   ODE_SetShape(mYerr, aShape);

   ODE_SetShape(mDyDtOld, aShape);
}

/**
 * Do we use FSAL?
 *
 * @return
 *    true
 **/
template
   <  typename ODE
   >
bool Tsit5Stepper<ODE>::Fsal
   (
   )  const
{
   return true;
}

/**
 * Get saved FSAL derivative
 * @return
 *    Const ref to FSAL vector
 **/
template
   <  typename ODE
   >
typename Tsit5Stepper<ODE>::vec_t Tsit5Stepper<ODE>::GetFsalDerivative
   (
   )  const
{
   return this->mK7;
}

#endif /* TSIT5STEPPER_IMPL_H_INCLUDED */



