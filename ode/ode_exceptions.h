/**
************************************************************************
* 
* @file                 ode_exceptions.h
* 
* Created:              20-04-2018
* 
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
* 
* Short Description:    Exception handler for ode module
* 
* Last modified:
* 
* Copyright:
* 
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ODE_EXCEPTIONS_H_INCLUDED
#define ODE_EXCEPTIONS_H_INCLUDED

#include <exception>
#include <string>

namespace midas::ode
{

/**
 * Exception for bad derivative calculation
 **/
class bad_derivative
   :  public std::exception
{
   private:
      //! Message
      std::string mMessage;

   public:
      //! Constructor from message
      bad_derivative
         (  const std::string& aMessage
         )
         :  mMessage
               (  aMessage
               )
      {
      }

      //! What has happened!
      const char* what
         (
         )  const noexcept
      {
         return mMessage.c_str();
      }
};

} /* namespace midas::ode */

#endif /* ODE_EXCEPTIONS_H_INCLUDED */
