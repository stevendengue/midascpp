#!/usr/bin/env python

# Retrieve mass weigthed normal coordinates from
# a CFOUR vibrational calculation
# Last modified: Aug. 17, 2011
# Mikkel Bo Hansen (mbh@chem.au.dk)
#
# Requires the NORMCO, MOLDEN, and CFOUR.OUT files present in
# the directory of issuing the command
#
# Input: 
#     $1: CFOUR output file (optional - will be assumed
#         CFOUR.OUT if not specified)
#     $2: output file (optional - will be called 
#         midasifc.numder if not specified)
#
#         CORRECTION: WILL ALWAYS USE THE ABOVE NAMES 
#

import sys
from sys import stdin
import os
import os.path
import re
import math

au_a = 0.5291772108    # au to Angstrom
OUTPUT='midasifc.numder'
CFOUR_OUT='CFOUR.OUT'
n_atoms=int(0)
masses=[]

def check_for_files_present():
   # check whether "CFOUR.OUT", "MOLDEN", and "NORMCO" are
   # present
   arr=[CFOUR_OUT,'NORMCO','MOLDEN']
   for i in range(0,len(arr)):
      if not os.path.isfile(arr[i]):
         print 'ERROR! did not find %s' % (arr[i])
         sys.exit(10)

def find_number_and_symmetry_of_nc():
   #read electronic energy from CFOUR.OUT
   inp=open('CFOUR.OUT','r')
   # start by finding number of atoms
   global n_atoms
   while 1:
      line=inp.readline()
      if re.match('[a-zA-Z\-\s]+Coordinates \(in bohr\)',line) is not None:
         break
   # rread two more lines
   line=inp.readline()
   line=inp.readline()
   n_atoms=0
   while 1:
      line=inp.readline()
      if re.match('\s+\-\-\-\-\-\-\-\-\-\-\-',line) is not None:
         break
      arr=line.split()
      if arr[1] is '0':
         continue
      n_atoms+=1

   # Now find the atomic masses used
   while 1:
      line=inp.readline()
      if re.match('\s+masses used \(in AMU\)',line) is not None:
         break
   global masses
   n_lines=n_atoms/3
   if n_atoms%3 > 0:
      n_lines+=1
   for i in range(0,n_lines):
      tmp_m=inp.readline().split()
      for j in range(0,len(tmp_m)):
         masses.append(float(tmp_m[j]))
   #print str(masses)
      
   # now find normal coordinates and irreps
   regex='\s+Normal Coordinate Analysis'
   while 1:
      line=inp.readline()
      if not line:
         print 'ERROR!!!! Normal coordinate section NOT found'
         sys.exit(10)
      if re.match(regex,line) is not None:
         break
   # read six more lines
   for i in range(0,6):
      inp.readline()
   freqs=[]
   regex='\s+\-\-\-\-\-\-\-\-\-\-\-'
   while 1:
      line=inp.readline()
      if re.match(regex,line) is not None:
         break
      type=str(line.split()[len(line.split())-1])
      if type != 'VIBRATION':
         continue
      [sym,frq]=line.split()[0:2]
      freqs.append([sym,frq])
   return freqs

def find_and_write_coord():
   inp=open('MOLDEN','r')
   regex='\[ATOMS\] AU'
   out=open(OUTPUT,'w')
   while 1:
      line=inp.readline()
      if re.match(regex,line) is not None:
         break
   out.write('#0 MOLECULE INPUT\n')
   out.write('#1 XYZ\n')
   out.write('  '+str(n_atoms)+'   Aa\n')
   out.write(' this is a comment line\n')
   for i in range(0,n_atoms):
      line=inp.readline()
      [name,d1,d2,x,y,z]=line.split()
      x=float(x)*au_a
      y=float(y)*au_a
      z=float(z)*au_a
      x_s="%22.16f " % x
      y_s="%22.16f " % y
      z_s="%22.16f " % z
      s=name+' '+x_s+' '+y_s+' '+z_s
      out.write(s)
      out.write("\n")
   out.write('\n')
   out.close()

# find frequency and normal coordinates in NORMCO
def find_nc(freqs,n_atoms):
   inp=open('NORMCO','r')
   norm_coord=[]
   while 1:
      line=inp.readline()
      if not line:
         break
      if re.match('% frequency',line) is None:
         continue
      frq=inp.readline().split()[0]
      try:  
         test=float(frq)
      except:
         #print 'Could not interpret '+frq
         continue
      success=0
      for i in range(0,len(freqs)):
         test=math.fabs(float(freqs[i][1])-float(frq))
         if test<float(0.01):
            success=1
            break
      if success is not 1:
            continue
      # write the frequency
      freqs[i][0]=freqs[i][0].replace('\'','P')
      freqs[i][0]=freqs[i][0].replace('U','u')
      freqs[i][0]=freqs[i][0].replace('G','g')
      # we have a match, find normal coordinate
      indx=i
      line=inp.readline()
      #print '# atoms: %d' % n_atoms
      dist=[]
      for i in range(0,n_atoms):
         tmp=inp.readline().split()
         dist.append(tmp)
      norm_coord.append([[freqs[indx][0],frq],dist])
      #print str(norm_coord[len(norm_coord)-1])
   #print '# norm coord '+str(len(norm_coord))
   return norm_coord

def write_nc(norm_coord):
   out=open(OUTPUT,'a')
   out.write('#1 FREQ\n' + str(len(norm_coord)) + ' cm-1\n')
   for i in range(0,len(norm_coord)):
      out.write("%22.16f %s\n" % (float(norm_coord[i][0][1]),norm_coord[i][0][0]))
   out.write('\n')
   out.write('#1 VIBCOORD\nau\n')
   for i in range(0,len(norm_coord)):
      out.write('#2 COORDINATE\n')
      for j in range(0,n_atoms):
         nx=float(norm_coord[i][1][j][0])/math.sqrt(float(masses[j]))
         ny=float(norm_coord[i][1][j][1])/math.sqrt(float(masses[j]))
         nz=float(norm_coord[i][1][j][2])/math.sqrt(float(masses[j]))
         out.write("%22.16f %22.16f %22.16f\n" % (nx,ny,nz))
   out.write('#0 MOLECULE INPUT END\n')
   out.close()
      
def main():
   global OUTPUT
   global CFOUR_OUT
   if len(sys.argv) is 2:
      CFOUR_OUT=sys.argv[1]
   if len(sys.argv) is 3:
      CFOUR_OUT=sys.argv[1]
      OUTPUT=sys.argv[2]

   check_for_files_present()

   print 'Will use the CFOUR output file:                '+str(CFOUR_OUT)
   print 'Normal coordinates and etc will be written to: '+str(OUTPUT)
   print 
   print '***************** Word of caution! **********************'
   print '  * Linear molecules are treated in the CooV/DooH PG\'s  *'
   print '  * These are NOT supported by the MidasCpp program!    *'
   print '  * Defining the correct Abelian IRREPS must be done    *'
   print '    manually!                                           *'
   print '  * Semi-Numerical Hessians will put wrong structure in *'
   print '  * Check with full analytic or put start structure in  *'
   print '*********************************************************'
   print
   freqs=find_number_and_symmetry_of_nc()
   find_and_write_coord()
   norm_coord=find_nc(freqs,n_atoms)
   write_nc(norm_coord)
   
if __name__ == '__main__':
   main()
