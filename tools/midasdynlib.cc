#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

#include "include/midaspot/Dll.h"   // Dynlib interface
#include "include/midaspot/Cl.h" // Command cl parser

using float_type = double;

constexpr int DEBUG = 1;

/**
 *
 **/
void read_molecule_xyz_file_to_cart(const std::string& filename, cartesian& cart)
{
   std::ifstream xyz_file(filename);
   std::string   str;
      
   // Loop over lines of xyz file
   int counter = 0;
   while(std::getline(xyz_file, str))
   {
      char *c_str = const_cast<char*>(str.c_str());
    	char *rest = NULL;
    	char *token;
      
      // Read atom label
      token = strtok_r(c_str, " ", &rest);
      
      // Read coordinates
    	while (  (token = strtok_r(NULL, " ", &rest)) ) 
		{
         ((double*)cart.coord)[counter] = double_from_string(token);
         ++counter;
    	}
   }
}

/**
 *
 **/
void print_all_information(const surface& surf)
{
   for(int i = 0; i < surf.nfunctions; ++i)
   {
      property_interface_t* prop = property_introspection(&surf, i);

      std::cout << prop->name << "    " << "'" << prop->description << "'" << std::endl;
   }
}

/**
 *
 **/
void evaluate_surface(const cl_args_type& cl_args, const surface& surf)
{  
   std::string filename;
   if(auto moleculefile_option = cl_args.get<std::string>("moleculefile"))
   {
      filename = moleculefile_option.value();
   }
   else
   {
      throw std::runtime_error("No molecule file given.");
   }


   int precision = 17;
   if(auto precision_option = cl_args.get<int>("precision"))
   {
      precision = precision_option.value();
   }
   std::cout << std::setprecision(precision);
   std::cerr << std::setprecision(precision);

   if(cl_args.get<bool>("fixed"))
   {
      std::cout << std::fixed;
      std::cerr << std::fixed;
   }
   else
   {
      std::cout << std::scientific;
      std::cerr << std::scientific;
   }

   std::vector<std::string> operators;
   if(auto operator_option = cl_args.get<string_vector>("operator"))
   {
      auto operators_cl = operator_option.value();
      for(int i = 0; i < operators_cl.size(); ++i)
      {
         operators.emplace_back(operators_cl[i]);
      }
   }
   else
   {
      for(int i = 0; i < surf.nfunctions; ++i)
      {
         property_interface_t* prop = property_introspection(&surf, i);

         operators.emplace_back(std::string{prop->name});
      }
   }

   cartesian cart;
   singlepoint sp;

   allocate_cartesian(&surf, &cart);
   allocate_singlepoint(&surf, &sp, PROVIDES_VALUE);

	read_molecule_xyz_file_to_cart(filename, cart);

   for(int i = 0; i < operators.size(); ++i)
   {
      const char* name = operators[i].c_str();
      if(evaluate_property(&surf, &cart, &sp, name, PROVIDES_VALUE) == EXIT_SUCCESS)
      {
         std::cout << name << " = " << *(double*)sp.value << std::endl;
      }
      else
      {
         std::cout << "No operator with name '" << name << "'." << std::endl;
      }
   }
   
   deallocate_cartesian(&cart);
   deallocate_singlepoint(&sp);
}

/**
 *
 **/
cl_type create_cl_input()
{
   cl_type cl;
   
   cl.command("evaluate", "Evaluate dynamic library.")
      .option("--precision", "int", "Set precision of output.")
      .option("--fixed", "",  "Use fixed point format.")
      .option("--operator -o", "string",  "Operators to evaluate.", "multi")
      .argument("dynlib", "string")
      .argument("moleculefile", "string");
   cl.command("dump",     "Dump description of dynamic library.")
      .argument("dynlib", "string");

   return cl;
}

/**
 *
 **/
int main(int argc, const char* argv[])
{  
   try
   {
      //
      auto cl = create_cl_input();
      
      auto cl_args = parse_cl_args(argc, argv, cl);

      std::string dynlib_path;
      if(auto dynlib_option = cl_args.get<std::string>("dynlib"))
      {
         dynlib_path = dynlib_option.value();
      }
      else
      {
         throw std::runtime_error("No dynamic library provided.");
      }

      // Initialize surface
      surface surf;
      initialize_surface(&surf, dynlib_path.c_str());
      
      if(cl_args.get<bool>("dump"))
      {
         // Run commands
         print_all_information(surf);
      }
      if(cl_args.get<bool>("evaluate"))
      {
	      //
         evaluate_surface(cl_args, surf);
      }
      else
      {
         throw std::runtime_error("No command given.");
      }
      
      // Finalize surface
      finalize_surface(&surf);
   }
   catch(std::exception& e)
   {
      std::cerr << e.what() << std::endl;
      
      std::exit(1);
   }
   
   // Return
   return 0;
}
