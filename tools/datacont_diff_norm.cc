#include<iostream>
#include<fstream>
#include<string>
#include<stdio.h>
#include<iomanip>
#include<cstdlib>
#include<cmath>

int main(int argc, char* argv[])
//argc: argument count from the command line, files in this case
//(number of strings pointed to by argv, 
//which is usually 1 + number of arguments since the program name is included).
//argv: argument vector is the array of arguments.

{
   //Gives error/warning if argc is not 3, i.e. if other than two file as been given in command line.
   if (argc != 3) 
   {
      std::cout << "TWO COMMAND LINE ARGUMENT: The DataCont files." << std::endl;
      exit(40);
   }

   //Sets the string variable file_name to the 1st argument vector entrance (0 is the program name).
   std::string file_name1=std::string(argv[1]);
   std::string file_name2=std::string(argv[2]);
   //ifstream: Input stream class to operate on files. Defines in_file variable of the class ifstream, thus far empty.
   std::ifstream in_file1;
   std::ifstream in_file2;
   //file_name is the file being opened
   //std::ios::in opens the file for input
   //std::ios::binary opens the file in binary mode
   //
   in_file1.open(file_name1.c_str(),std::ios::in|std::ios::binary);
   in_file2.open(file_name2.c_str(),std::ios::in|std::ios::binary);

   //In above: make appropriate for two files.
   double data1;   //Define two data variables, one for each file.
   double data2;
   double nsum1 = 0;
   double nsum2 = 0;
   double dsum = 0;
   double ssum = 0;
   double psum = 0;
   std::cout << std::scientific << std::setprecision(20);
   while(in_file1.read(reinterpret_cast<char*>(&data1),sizeof(data1)) && in_file2.read(reinterpret_cast<char*>(&data2),sizeof(data2)))   //Take two files.
   {
      nsum1 += data1 * data1;
      nsum2 += data2 * data2;
      dsum  += (data1 - data2)*(data1 - data2);
      ssum  += (data1 + data2)*(data1 + data2);
      psum  += data1 * data2;
        // std::cout << data << std::endl;
   // double sum = 0
   // sum += (d1-d2)*(d1-d2)
   }
   std::cout << "|v1|               = " << sqrt(nsum1) << std::endl;      //Norm of vector one.
   std::cout << "|v2|               = " << sqrt(nsum2) << std::endl;      //Norm of vector two.
   std::cout << "|v1-v2|            = " << sqrt(dsum)  << std::endl;      //Norm of (v1 - v2).
   std::cout << "|v1+v2|            = " << sqrt(ssum)  << std::endl;      //Norm of (v1 + v2).
   std::cout << "<v1,v2>            = " << psum        << std::endl;      //Scalar product.
   std::cout << "<v1,v2>/(|v1||v2|) = " << psum/sqrt(nsum1*nsum2)   << std::endl;   // v1·v2/(|v1| |v2|).
   //After while loop: do final arithmetics, i.e. sqrt etc.
   
   return 0;
}
