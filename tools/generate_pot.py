#!/usr/bin/python2

##########################################################
#
#  author: Niels Kristian Madsen
#  
#  Generate fake PES consisting of mode blocks with strong
#  internal coupling and weak coupling to the nearest 
#  neighbours.
#
##########################################################

import optparse
import copy
import itertools
import random
import shutil

# make header for .mop file
def make_header(file):
   file.write("#0MIDASMOPINPUT\n");
   file.write("#1CONSTANTS\n");
   file.write("#1FUNCTIONS\n");
   file.write("#1OPERATORTERMS\n");

# make footer for .mop file
def make_footer(file, nmodes):
   file.write("#1MODENAMES\n");
   for m in xrange(0, nmodes):
      file.write("Q" + str(m) + " ");
   file.write("\n");
   file.write("#0MIDASMOPINPUTEND");


# generate mode-combination range for one block
def generate_mcr(order, nmodes):
   # generator for modes
   modes = xrange(0, nmodes);

   # generator of MCR
   mcr = [list(mc) for o in xrange(1, order+1) for mc in itertools.combinations(modes, o)];

   return mcr

# remove duplicates from list
def remove_duplicates(list):
   new = [];
   for i in list:
      if i not in new:
         new.append(i);
   return new;

# generate terms for block of modes
def generate_block(file, block, nmodes, opoly, mcr):
   print "Generate block " + str(block);
   for mc in mcr:
      concrete_mc = [x+(block-1)*nmodes for x in mc];
      write_potential_terms(file, concrete_mc, opoly);

# write potential terms for concrete mode combination
def write_potential_terms(file, mc, opoly, scaling=float(1)):
   print "Write potential terms for MC: " + str(mc);
   size = len(mc);
   max_power = opoly+1-size;

   # generator of polynomial-order combinations
   pow_combis = (list(prod) for prod in itertools.product(range(1,max_power+1), repeat=size) if sum(prod) <= opoly);

   # loop through operators
   for op in pow_combis:
      file.write(str(operator_coef(mc, op, scaling)));
      file.write("     ");
      for i in xrange(0,len(op)):
         file.write("Q^"+str(op[i])+"(Q"+str(mc[i])+")  ");
      file.write("\n");

#
def operator_coef(mc, op, scaling):
   diff_modes = len(mc);
   term_order = sum(op);

   if diff_modes == 1 and term_order == 1:
      coef = 1.e-9+random.random()*1.e-10;
   else:
      coef = 10**(-4-diff_modes*2) / float( 10*random.random()+term_order**2 );
   if diff_modes == 1 and term_order == 2:
      coef *= 100;
   elif diff_modes == 2 and op[0] == 2 and op[1] == 2:
      coef *= 50;

   coef *= scaling;

   return coef;


# generate terms for coupling between blocks
def generate_block_coupling(file, block1, block2, nmodes, ointer, opoly, bcscaling):
   print "Generate coupling between block " + str(block1) + " and " + str(block2) + " of order " + str(ointer);

   # generators for modes in blocks
   modes1 = xrange((block1-1)*nmodes, block1*nmodes);
   modes2 = xrange((block2-1)*nmodes, block2*nmodes);

   all_modes = [m for j in (modes1,modes2) for m in j];

   # list of two-mode MCs between blocks
   # This makes sure, that there is at least one mode from each block in each MC!
   mcr = [list(mc) for mc in itertools.product(modes1, modes2)];

   # we must iterate over copy of mcr
   mcr_iter = copy.deepcopy(mcr);

   # Now add combinations for the remaining orders
   if ointer > 2:
      for add_order in xrange(1, ointer-1):
         for mc in mcr_iter:
            add_modes = (m for m in all_modes if m not in mc);
            add_mcs = (list(mc) for mc in itertools.combinations(add_modes, add_order));
            for add_mc in add_mcs:
               new_mc = copy.deepcopy(mc);
               new_mc.extend(add_mc);
               new_mc.sort();
               mcr.append(new_mc);
      # remove duplicates (this is stupid...)
      mcr = remove_duplicates(mcr);

   print "MCs for inter-block coupling:";
   for mc in mcr:
      print "Write potential terms for MC: " + str(mc);
      write_potential_terms(file, mc, opoly, bcscaling);

# Write property
def write_prop(file, nmodes, oper, coef):
   for m in xrange(0, nmodes):
      file.write(str(coef) + " " + oper + "(Q" + str(m) + ")\n");



# main function
def main():
   parser = optparse.OptionParser();
   parser.add_option("-f",dest="filename",type="string",help="specify output filename", default="fake_potential.mop");
   parser.add_option("-n",dest="nblocks",type="int",help="specify number of blocks", default=1);
   parser.add_option("-m",dest="nmodes",type="int",help="specify number of modes pr. block", default=3);
   parser.add_option("--ointra",dest="ointra",type="int",help="specify max coupling order between modes in the same block", default=3);
   parser.add_option("--ointer",dest="ointer",type="int",help="specify max coupling order between blocks", default=2);
   parser.add_option("-p", dest="opoly", type="int", help="specify polynomial order", default=4);
   parser.add_option("--cyclic",action="store_true", dest="cyclic", help="make the system cyclic by letting the first and last blocks couple", default=False);
   parser.add_option("-s", dest="bcscaling", type="float", help="specify relative intra/inter-block coupling strength", default=1.e-1);
   parser.add_option("--rand-seed", dest="rseed", type="int", help="specify random seed for operator coefficients", default=0);
   parser.add_option("--timedep", action="store_true", dest="timedep", help="prepare additional operators for TD calculations", default=False);
   parser.add_option("--ref-shift", dest="ref_shift", type="float", help="reference-wave-packet shift for TD calculations", default=1.e-1);

   (options,args) = parser.parse_args();
   filename=options.filename;
   nblocks=options.nblocks;
   nmodes=options.nmodes;
   ointra=options.ointra;
   ointer=options.ointer;
   opoly=options.opoly;
   cyclic=options.cyclic;
   bcscaling=options.bcscaling;
   timedep=options.timedep;
   ref_shift=options.ref_shift;

   # seed random-number generator
   random.seed( options.rseed );

   # checks
   if ointra > nmodes or ointer > nmodes*2:
      raise ValueError("Coupling order cannot be larger than the number of modes!");
   if opoly < ointra or opoly < ointer:
      raise ValueError("Polynomial order cannot be smaller than coupling order!");

   tot_modes = nmodes*nblocks;

   # PES file
   pes_file = open(filename, 'w');
   make_header(pes_file);
   mcr = generate_mcr(ointra, nmodes);
   for b in range(1, nblocks + 1):
      generate_block(pes_file, b, nmodes, opoly, mcr);
      if b < nblocks:
         generate_block_coupling(pes_file, b, b+1, nmodes, ointer, opoly, bcscaling);
      elif cyclic and nblocks > 2:
         generate_block_coupling(pes_file, b,1, nmodes, ointer, opoly, bcscaling);

   pes_file.close();

   # Additional files for TD calculation
   if timedep:
      # Reference (shifted) PES
      ref_filename = "shifted_" + filename;
      shutil.copy2(filename, ref_filename);
      ref_file = open(ref_filename, 'a');
      write_prop(ref_file, tot_modes, "Q^1", -2*ref_shift);
      make_footer(ref_file, tot_modes);
      ref_file.close();

      # q operator
      q_filename = "q_" + filename;
      q_file = open(q_filename, 'w');
      make_header(q_file);
      write_prop(q_file, tot_modes, "Q^1", 1.0);
      make_footer(q_file, tot_modes);
      q_file.close();

      # ip operator
      ip_filename = "ip_" + filename;
      ip_file = open(ip_filename, 'w');
      make_header(ip_file);
      write_prop(ip_file, tot_modes, "DDQ^1", 1.0);
      make_footer(ip_file, tot_modes);
      ip_file.close();


   # Finally, make footer for PES file
   pes_file = open(filename, 'a');
   make_footer(pes_file, tot_modes);
   pes_file.close();

#
if __name__ == "__main__":
   main();
