/**
************************************************************************
*  
* @file                datacont_to_ascii.cc
*
* Created:             09-09-2013
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   takes datacont file as arguments, converts elements to double 
*                      and outputs as ascii
* 
* Rev. Date            Comments: 
*
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include<iostream>
#include<fstream>
#include<string>
#include<stdio.h>
#include<iomanip>
#include<cstdlib>

int main(int argc, char* argv[])
{
   if (argc != 2) 
   {
      std::cout << "ONE COMMAND LINE ARGUMENT: The DataCont file." << std::endl;
      exit(40);
   }

   std::string file_name=std::string(argv[1]);
   std::ifstream in_file;
   in_file.open(file_name.c_str(), std::ios::in|std::ios::binary);

   double data;
   std::cout << std::scientific << std::setprecision(20);
   while(in_file.read(reinterpret_cast<char*>(&data),sizeof(data)))
   {
      std::cout << data << std::endl;
   }

   return 0;
}
