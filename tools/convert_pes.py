#!/usr/bin/env python2

######################################################
#
#  author: Niels Kristian Madsen
#  date:   March 9, 2017
#
#  Convert the old polynomial PES format:
#  const  1 1 2 2 3
#  ... etc.
#  to the new one:
#  const Q^2(Q0) Q^2(Q1) Q^1(Q2)
#
######################################################

import collections as col
import argparse as ap

# main function
def main():
   parser = ap.ArgumentParser(description="Convert old polynomial PES format to new (const 1 1 2 2  =>  const Q^2(Q0) Q^2(Q1)");
   parser.add_argument("-i", "--input", type=str, help="name of input file", required=True);
   parser.add_argument("-o", "--output", type=str, help="name of output file", default="new_pes.mop");
   args = parser.parse_args();
   ifile = args.input;
   ofile = args.output;

   # Define input and output
   outputfile = open(ofile,'w');
   inputfile = open(ifile,'r');

   # Define nmodes
   nmodes = 0;

   # Write header
   outputfile.write("#0MIDASMOPINPUT\n");

   # Check if we are using scaling frequencies
   iline = inputfile.readline().rstrip(); # read first line
   if iline.startswith("SCALING FREQUENCIES"):
      outputfile.write("#1FREQUENCIES\n");
      freq_line = "";
      # Loop over frequencies
      iline = inputfile.readline().rstrip();
      while iline:
         if iline.startswith("DALTON_FOR_MIDAS"):
            break;
         else:
            freq_line += iline + "   ";
         iline = inputfile.readline().rstrip();
      # Write frequencies
      outputfile.write(freq_line+'\n');

   # Read operator terms
   outputfile.write("#1OPERATORTERMS\n");
   iline = inputfile.readline().rstrip();
   while iline:
      # Split line
      split_line = iline.split();
      # Set output line to coefficient
      outline = str(split_line[0])+"   ";
      # Add extra space if coef is positive
      if float(split_line[0]) >= 0:
         outline = " "+outline;
      # Remove coef from split line and setup counter
      split_line.pop(0);
      count = col.Counter(split_line);
      # Add polynomials to output line
      for mode in count.keys():
         outline += ("Q^" + str(count[mode]) + "(Q" + str(int(mode)-1) + ")   ");
         # Save nmodes
         if int(mode) >= nmodes:
            nmodes = int(mode);
      # Write output line
      outputfile.write(outline+'\n');
      # Read next line
      iline = inputfile.readline().rstrip();
   
   inputfile.close();

   # Write mode names
   outputfile.write("#1MODENAMES\n");
   modenames = "";
   for x in xrange(0,nmodes):
      modenames += "Q"+str(x)+"   ";
   outputfile.write(modenames+'\n');

   # Write footer
   outputfile.write("#0MIDASMOPINPUTEND");


#
if __name__ == "__main__":
   main();
