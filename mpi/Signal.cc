#include "mpi/Signal.h"

#include <iostream>

#include "util/Error.h"
#include "mpi/TypeDefs.h"
#include "mpi/Wrapper.h"
#include "mpi/MpiLog.h"

namespace midas
{
namespace mpi
{

/**
 * Send signal to all slaves in communicator.
 *
 * @param aCommunicator   The communicator to send the signal to.
 * @param aSignal         The signal to send.
 **/
void SendSignal
   (  const Communicator& aCommunicator
   ,  int aSignal
   )
{
   if constexpr (MPI_DEBUG) 
   {
      WriteToLog("Entered SendSignal()");
   }

#ifdef VAR_MPI
   // only master can run a command
   if(!aCommunicator.IsMaster())
   {
      MIDASERROR(" ERROR : SendSignal() cannot be called from Slave ");
   }
   
   // Send the task nr to slaves.
   if constexpr (MPI_DEBUG)
   {
      WriteToLog("BROADCASTING SIGNAL : " + std::to_string(aSignal));
   }
   auto status = detail::WRAP_Bcast(&aSignal, 1, MPI_INT, aCommunicator.GetRank(), aCommunicator.GetMpiComm());
#endif /* VAR_MPI */
}

/**
 * Recieve signal from master rank in communicator.
 *
 * @param aCommunicator  The communicator to recieve on.
 *
 * @return Returns the recieved signal.
 **/
int RecieveSignal
   (  const Communicator& aCommunicator
   )
{
   if constexpr (MPI_DEBUG) 
   {
      WriteToLog("Entered RecieveSignal()");
   }

#ifdef VAR_MPI
   if(aCommunicator.IsMaster())
   {
      MIDASERROR(" ERROR : RecieveSignal() cannot be called from Master ");
   }
   
   // Recieve task from master.
   int task;
   auto ret = detail::WRAP_Bcast(&task, 1, MPI_INT, aCommunicator.GetMasterRank(), aCommunicator.GetMpiComm());
   if(ret != MPI_SUCCESS)
   {
      MidasWarning(" WARNING : In RecieveSignal MPI_Bcast failed.");
   }

   if constexpr (MPI_DEBUG)
   {
      WriteToLog("RECIEVED SIGNAL : " + std::to_string(task));
   }
   
   // Return the recieved signal.   
   return task;
#else
   // If we are not doing MPI we return an error signal, as there should be no slaves to recieve on
   return signal::ERROR;
#endif /* VAR_MPI */
}

} /* namespace mpi */
} /* namespace midas */
