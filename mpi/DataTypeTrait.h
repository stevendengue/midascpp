/**
************************************************************************
* 
* @file                DataTypeTrait.h
*
* Created:             10-10-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef MIDAS_MPI_DATA_TYPE_TRAIT_H_INCLUDED
#define MIDAS_MPI_DATA_TYPE_TRAIT_H_INCLUDED

#ifdef VAR_MPI
#include <mpi.h>
#include <complex>
#include "inc_gen/TypeDefs.h"
//#include "mmv/MidasVector.h"

namespace midas
{
namespace mpi
{

   /**
    * Declaration of template class to translate template type to correct MPI type.
    * Specialization for each POD type follows.
    **/
   template<class T>
   struct DataTypeTrait;

   /**
    * Specialization for 'double'.
    **/
   template<>
   struct DataTypeTrait<double>
   {
      static MPI_Datatype Get() { return MPI_DOUBLE; }
   };

   /**
    * Specialization for 'float'.
    **/
   template<>
   struct DataTypeTrait<float>
   {
      static MPI_Datatype Get() { return MPI_FLOAT; }
   };

   /**
    * Specialization for 'long double'.
    **/
   template<>
   struct DataTypeTrait<long double>
   {
      static MPI_Datatype Get() { return MPI_LONG_DOUBLE; }
   };

   /**
    * Specialization for 'char'.
    **/
   template<>
   struct DataTypeTrait<char>
   {
      static MPI_Datatype Get() { return MPI_CHAR; }
   };

   /**
    * Specialization for 'short'.
    **/
   template<>
   struct DataTypeTrait<short>
   {
      static MPI_Datatype Get() { return MPI_SHORT; }
   };

   /**
    * Specialization for 'int'.
    **/
   template<>
   struct DataTypeTrait<int>
   {
      static MPI_Datatype Get() { return MPI_INT; }
   };
   
   /**
    * Specialization for 'long int'.
    **/
   template<>
   struct DataTypeTrait<long int>
   {
      static MPI_Datatype Get() { return MPI_LONG; }
   };
   
   /**
    * Specialization for 'unsigned char'.
    **/
   template<>
   struct DataTypeTrait<unsigned char>
   {
      static MPI_Datatype Get() { return MPI_UNSIGNED_CHAR; }
   };
   
   /**
    * Specialization for 'unsigned short'.
    **/
   template<>
   struct DataTypeTrait<unsigned short>
   {
      static MPI_Datatype Get() { return MPI_UNSIGNED_SHORT; }
   };
   
   /**
    * Specialization for 'unsigned long int'.
    **/
   template<>
   struct DataTypeTrait<unsigned long int>
   {
      static MPI_Datatype Get() { return MPI_UNSIGNED_LONG; }
   };
   
   /**
    * Specialization for 'unsigned int'.
    **/
   template<>
   struct DataTypeTrait<unsigned int>
   {
      static MPI_Datatype Get() { return MPI_UNSIGNED; }
   };
   
   /**
    * Specialization for 'std::complex<double>'.
    **/
   template<>
   struct DataTypeTrait<std::complex<double> >
   {
      static MPI_Datatype Get() { return MPI_DOUBLE_COMPLEX; }
   };
   
   /**
    * Specialization for 'std::complex<float>'.
    **/
   template<>
   struct DataTypeTrait<std::complex<float> >
   {
      static MPI_Datatype Get() { return MPI_COMPLEX; }
   };
   
   /*template<>
   struct DataTypeTrait<std::complex<long double> >
   {
      static MPI_Datatype Get() { return MPI_LONG_DOUBLE_COMPLEX; }
   };*/

} /* namespace mpi */
} /* namespace midas */

#endif /* VAR_MPI */

#endif /* MIDAS_MPI_DATA_TYPE_TRAIT_H_INCLUDED */
