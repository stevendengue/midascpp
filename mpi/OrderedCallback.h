/**
************************************************************************
* 
* @file                OrderedCallback.h
*
* Created:             23-07-2013
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Function to do function call backs in rank order fashion,
*                      when using MPI
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef MIDAS_MPI_ORDERED_CALLBACK_H_INCLUDED
#define MIDAS_MPI_ORDERED_CALLBACK_H_INCLUDED

#ifdef VAR_MPI
#include<mpi.h>
#endif /* VAR_MPI */
#include<functional>
#include<iostream>

#include "mpi/Communicator.h"
#include "inc_gen/TypeDefs.h"

namespace midas
{
namespace mpi
{

/**
 * Do a rank ordered callback on a communicator of a given function.
 * Useful for getting un-mangled output from different MPI processes in an MPI run.
 *
 * @param aComm   The communicator on which to do the ordered callback.
 * @param aFunc   The function to call.
 * @param aTs     Arguments to the function.
 **/
template
   <  class F
   ,  class... Ts
   >
inline void OrderedCallback
   (  const Communicator& aComm
   ,  F aFunc
   ,  Ts&&... aTs
   )
{
#ifdef VAR_MPI
   for(int i = 0; i < aComm.GetNrProc(); ++i)
   {
      if(aComm.GetRank() == i)
      {
         aFunc(std::forward<Ts>(aTs)...);
      }
      Barrier(aComm);
   }
#else 
   aFunc(std::forward<Ts>(aTs)...);
#endif /* VAR_MPI */ 
}

} /* mpi */
} /* namespace midas */

#endif /* MIDAS_MPI_ORDERED_CALLBACK_H_INCLUDED */
