/**
************************************************************************
* 
* @file                ForLoop.h
*
* Created:             10-10-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Function for expanding a loop using mpi
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDAS_MPI_FOR_LOOP_H_INCLUDED
#define MIDAS_MPI_FOR_LOOP_H_INCLUDED

#ifdef VAR_MPI
#include <mpi.h>
#include "mpi/CommunicatorManager.h"
#include "mpi/UtilFunc.h"
#endif /* VAR_MPI */

// std headers
#include <vector>
#include <utility>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"

namespace midas
{
namespace mpi
{
namespace detail
{

/**
 * Implementation of ForLoop.
 **/
class ForLoopImpl
{
   private:
#ifdef VAR_MPI
      /**
       *
       **/
      template<class T>
      void VectorCommunication
         (  const Communicator& aParentCommunicator
         ,  GeneralMidasVector<T>& arVec
         ,  In aNrSpawnProc
         ,  In aStart
         ,  In aEnd
         ,  const std::vector<std::pair<In, In> >& arMPIRanks
         ,  const std::vector<std::pair<In, In> >& arTasks
         )  const
      {
         if(aParentCommunicator.GetRank() == I_0)
         {
            // If parent rank 0, we collect the result.
            MPI_Request mpi_req[arTasks.size()-1];
            for(In i = I_1; i < arTasks.size(); ++i)
            {
               MPI_Irecv(arVec.data() + arTasks[i].first, arTasks[i].second - arTasks[i].first, 
                         DataTypeTrait<T>::Get(), arMPIRanks[i].first, 0, 
                         aParentCommunicator.GetMpiComm(), &mpi_req[i-1]);
            }
            MPI_Status status;
            for(In i = I_1; i < arTasks.size(); ++i)
            {
               MPI_Wait(&mpi_req[i-1],&status);
            }
         }
         else if(get_CommunicatorManager().GetCurrentCommunicator().GetRank() == I_0)
         {
            // All current rank zero send the result to parent rank 0.
            MPI_Request mpi_req;
            MPI_Isend(arVec.data() + aStart, aEnd-aStart, 
                      DataTypeTrait<T>::Get(), 0, 0,  
                      get_CommunicatorManager().GetCurrentCommunicator().GetMpiComm(), &mpi_req);
         }

         // Then parent rank 0 broadcasts result to everybody
         MPI_Bcast(arVec.data(), arVec.Size(), DataTypeTrait<T>::Get(), I_0, aParentCommunicator.GetMpiComm());
      }
#endif /* VAR_MPI */
      
      std::vector<std::pair<In, In> > TaskDivision(In,In,In) const;
      In i4_div_rounded(In,In) const;
   
   public:
      template
         <  class FF
         ,  class T
         ,  class... Args
         >
      void RunLoop
         (  const FF&& f
         ,  int aNrSpawnProc
         ,  GeneralMidasVector<T>& aRes
         ,  Args&&... args
         )
      {
         In start = 0;
         In end = aRes.Size();
         aRes.Zero();
#ifdef VAR_MPI
         aNrSpawnProc = std::min(aNrSpawnProc, aRes.Size()); //We can only spawn as many processes as we have jobs
         aNrSpawnProc = std::max(aNrSpawnProc, 1);
         aNrSpawnProc = std::min(aNrSpawnProc, get_CommunicatorManager().GetCurrentCommunicator().GetNrProc()); // We can only spawn as many processes as we
                                                                                       // have in the current communicator
         
         std::vector<std::pair<In, In> > mpi_procs_comm = TaskDivision(get_CommunicatorManager().GetCurrentCommunicator().GetNrProc(), 0, aNrSpawnProc - 1);
         In proc_nr = I_0;
         In curr_rank = get_CommunicatorManager().GetCurrentCommunicator().GetRank();
         while(proc_nr < mpi_procs_comm.size() && mpi_procs_comm[proc_nr].first <= curr_rank)
         {
            ++proc_nr;
         }
         --proc_nr;
         
         // split communicator
         const auto& comm = get_CommunicatorManager().GetCurrentCommunicator();
         CommunicatorHandle handle = comm.SplitCommunicator(comm.GetName() + "_Split", proc_nr, curr_rank - mpi_procs_comm[proc_nr].first);
         
         std::vector<std::pair<In, In> > start_end = TaskDivision(aRes.size(), 0, aNrSpawnProc - 1);
         start = start_end[proc_nr].first;
         end = start_end[proc_nr].second;
#endif /* VAR_MPI */
         for(In i = start; i < end; ++i)
         {
            aRes[i] =  f(i, std::forward<Args>(args)...);
         }
#ifdef VAR_MPI
         VectorCommunication(comm, aRes, aNrSpawnProc, start, end, mpi_procs_comm, start_end);
#endif /* VAR_MPI */
      }
};

} /* namespace detail */


/**
 * Interface to mpi::ForLoop
 **/
template
   <  class F
   ,  class T
   ,  class ... Args
   >
auto ForLoop
   (  F&& f
   ,  int aNrSpawnProc
   ,  GeneralMidasVector<T>& aRes
   ,  Args&&... args
   )
   -> decltype(detail::ForLoopImpl().RunLoop(std::forward<F>(f), aNrSpawnProc, aRes, std::forward<Args>(args)...))
{
   return detail::ForLoopImpl().RunLoop(std::forward<F>(f), aNrSpawnProc, aRes, std::forward<Args>(args)...);
}

} /* namespace mpi_util */
} /* namespace midas */

#endif /* MIDAS_MPI_FOR_LOOP_H_INCLUDED */
