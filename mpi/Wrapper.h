#ifndef MIDAS_MPI_WRAPPER_H_INCLUDED
#define MIDAS_MPI_WRAPPER_H_INCLUDED

#include "mpi/TypeDefs.h"
#include "mpi/Communicator.h"
#include "mpi/Info.h"

namespace midas
{
namespace mpi
{

// Some forward declarations.
class Blob;

namespace detail
{
//! MPI_Barrier wrapper
int WRAP_Barrier( MPI_Comm comm );

//! MPI_Send wrapper
int WRAP_Send(const void *buffer, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm);

//! MPI_Recv wrapper
int WRAP_Recv(void *buffer, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Status *status);

//! MPI_Bcast wrapper
int WRAP_Bcast(void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm);

//! MPI_Ibcast wrapper
int WRAP_Ibcast(void* buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm, MPI_Request* req);

//! MPI_Wait wrapper
int WRAP_Wait(MPI_Request* req, MPI_Status* status);

//! MPI_Abort
int WRAP_Abort(MPI_Comm comm, int errorcode);
}

/* ******************************************************************
 * 
 * Smart wrapper interface
 *
 ********************************************************************/
/**
 * Bcast
 **/
//! Bcast Blob
int Bcast(Blob& blob, const Communicator& communicator = CommunicatorWorld(), int source = 0);

//! Bcast blob, default source
int Bcast(Blob& blob, const Communicator& communicator = CommunicatorWorld());

/**
 * Barrier
 **/
//! Barrier on communicator
int Barrier(const Communicator& communicator = CommunicatorWorld());

} /* namespace mpi */
} /* namespace midas */

#endif /* MIDAS_MPI_WRAPPER_H_INCLUDED */
