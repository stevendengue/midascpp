#ifndef MIDAS_MPI_FILETOSTRING_H_INCLUDED
#define MIDAS_MPI_FILETOSTRING_H_INCLUDED

#include <string>
#include <fstream>
#include <streambuf>

#include "mpi/Info.h"
#include "mpi/Communicator.h"


namespace midas
{
namespace mpi
{

/**
 * Read complete ifstream into a string.
 *
 * @param aFileName   The file to read
 *
 * @return Return contents of file as a string.
 **/
std::string FileToString
   (  const std::string& aFileName
   ,  const Communicator& aCommunicator = CommunicatorWorld()
   );

/**
 *
 **/
void StringToFile
   (  const std::string& aFileName
   ,  const std::string& aStr
   ,  const Communicator& aCommunicator = CommunicatorWorld()
   );

/**
 * Get istringstream from read-in file.
 *
 * @param aFileName   The filename of the file to read into stream.
 *
 * @return  Returns an istringstream containing the contents of the file.
 **/
std::istringstream FileToStringStream
   (  const std::string& aFileName
   ,  const Communicator& aCommunicator = CommunicatorWorld()
   );

/**
 *
 **/
void StringStreamToFile
   (  const std::string& aFileName
   ,  const std::stringstream& aSstream
   ,  const Communicator& aCommunicator = CommunicatorWorld()
   );

} /* namespace mpi */
} /* namespace midas */

#endif /* MIDAS_MPI_FILETOSTRING_H_INCLUDED */
