#ifndef MIDAS_MPI_FILETOSTRING_H_INCLUDED
#define MIDAS_MPI_FILETOSTRING_H_INCLUDED

#include <string>
#include <fstream>
#include <streambuf>

#include "util/Error.h"

#include "mpi/Info.h"
#include "mpi/Communicator.h"
#include "mpi/TypeDefs.h"
#include "mpi/Wrapper.h"
#include "mpi/MpiLog.h"

#include "libmda/util/stacktrace.h"

namespace midas
{
namespace mpi
{

/**
 * Read complete ifstream into a string.
 *
 * @param aFileName      The file to read
 * @param aCommunicator  The communicator to send the file over.
 *
 * @return Return contents of file as a string.
 **/
std::string FileToString
   (  const std::string& aFileName
   ,  const Communicator& aCommunicator
   )
{
   // Some debug output
   if constexpr (MPI_DEBUG)
   {
      WriteToLog(" ENTERING FILETOSTRING : " + aFileName);
   }
   
   //
   std::string str;
   
   // Master rank reads file from disc, and if we are in an MPI region Master sends out file and Slaves wait to recieve.
   if(aCommunicator.IsMaster())
   {
      // MPI master ( and serial )
      std::ifstream file(aFileName, std::ios::in);
      if (!file) 
      {
         MIDASERROR(" Rank: "+std::to_string(midas::mpi::GlobalRank())+" could not open file to read: " + aFileName);
      }
      
      // find size of file
      file.seekg(0, std::ios::end);
      auto file_size = file.tellg();
      file.seekg(0, std::ios::beg);
      
      if(file_size > 1024*1024*1024)
      {
         MIDASERROR(aFileName + " is to large (> 1GB).");
      }
      
      // load file into string
      str.reserve(file_size);
      str.assign  (  (std::istreambuf_iterator<char>(file)) // extra parenteses needed to avoid the "most vexing parse" problem
                  ,  std::istreambuf_iterator<char>()
                  );

#ifdef VAR_MPI
      if(IsMpiRegion())
      {
         if constexpr (MPI_DEBUG)
         {
            WriteToLog(" Master is broadcasting file.");
         }

         // if MPI we Bcast the file/string
         int size = str.size();
         int status = 0;
         if ((status = detail::WRAP_Bcast(&size, 1, MPI_INT, aCommunicator.GetMasterRank(), aCommunicator.GetMpiComm())) != 0)
         {
            MIDASERROR("Non-zero status from WRAP_Bcast: "+std::to_string(status));
         }
         if ((status = detail::WRAP_Bcast(const_cast<char*>(str.c_str()), str.size(), MPI_BYTE, aCommunicator.GetMasterRank(), aCommunicator.GetMpiComm())) != 0)
         {
            MIDASERROR("Non-zero status from WRAP_Bcast: "+std::to_string(status));
         }
      }
   }
   else
   {
      if(IsMpiRegion())
      {
         if constexpr (MPI_DEBUG)
         {
            WriteToLog(" Slave is broadcasting file recieve.");
         }
         
         // MPI slave, recieve the file/string
         int size;
         int status = 0;
         if ((status = detail::WRAP_Bcast(&size, 1, MPI_INT, aCommunicator.GetMasterRank(), aCommunicator.GetMpiComm())) != 0)
         {
            MIDASERROR("Non-zero status from WRAP_Bcast: "+std::to_string(status));
         }
         str.resize(size);
         if ((status = detail::WRAP_Bcast(const_cast<char*>(str.c_str()), str.size(), MPI_BYTE, aCommunicator.GetMasterRank(), aCommunicator.GetMpiComm())) != 0)
         {
            MIDASERROR("Non-zero status from WRAP_Bcast: "+std::to_string(status));
         }
      }
#endif /* VAR_MPI */
   }

   return str;
}

/**
 *
 **/
void StringToFile
   (  const std::string& aFileName
   ,  const std::string& aStr
   ,  const Communicator& aCommunicator 
   )
{
   if(aCommunicator.IsMaster())
   {
      std::ofstream file(aFileName);
      file << aStr << std::endl;
   }
}

/**
 * Get istringstream from read-in file.
 *
 * @param aFileName   The filename of the file to read into stream.
 * @param aCommunicator  The communicator to send the file over.
 *
 * @return  Returns an istringstream containing the contents of the file.
 **/
std::istringstream FileToStringStream
   (  const std::string& aFileName
   ,  const Communicator& aCommunicator
   )
{
   auto str = FileToString(aFileName, aCommunicator);
   return std::istringstream{str};
}

/**
 *
 **/
void StringStreamToFile
   (  const std::string& aFileName
   ,  const std::stringstream& aSstream
   ,  const Communicator& aCommunicator
   )
{
   StringToFile(aFileName, aSstream.str(), aCommunicator);
}

} /* namespace mpi */
} /* namespace midas */

#endif /* MIDAS_MPI_FILETOSTRING_H_INCLUDED */
