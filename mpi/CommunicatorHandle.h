/**
************************************************************************
* 
* @file                CommunicatorHandle.h
*
* Created:             10-10-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Class to control RAII/SBRM (Scope-Bound Resource Management) for CommunicatorManager splits.
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef MIDAS_MPI_COMMUNICATOR_HANDLE_H_INCLUDED
#define MIDAS_MPI_COMMUNICATOR_HANDLE_H_INCLUDED

#include <string>

namespace midas
{
namespace mpi
{

/**
 * Class to control the current process's communicator.
 **/ 
class CommunicatorHandle
{
   private:
      //!
      std::string mName;
      //! Should we pop on destruction.
      bool mPop = true;

   public:
      //! Default constructor.
      CommunicatorHandle(const std::string& aName)
         :  mName(aName)
      {
      }

      //! Destructor. Will pop communicator.
      ~CommunicatorHandle();
         
      //! Dismiss the pop.
      void Dismiss() { mPop = false; }
};

} /* namespace mpi */
} /* namespace midas */

#endif /* MIDAS_MPI_COMMUNICATOR_HANDLE_H_INCLUDED */
