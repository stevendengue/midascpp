#ifndef MIDAS_MPI_FILESTREAM_H_INCLUDED
#define MIDAS_MPI_FILESTREAM_H_INCLUDED

#include <iostream>
#include <fstream>
#include <mutex>

#include "util/Error.h"
#include "mpi/Info.h"

namespace midas
{
namespace mpi
{

/*!
 *
 **/
class OFileStream
   :  public std::ofstream  
{
   private:
      using mutex_type = std::mutex;

   public:
      enum StreamType : int { ERROR, MPI_ALL_DISTRIBUTED, MPI_MASTER };

      //! Default c-tor
      OFileStream
         (
         )
         :  std::ofstream
               (
               )
         ,  mStreamType
               (  StreamType::MPI_MASTER
               )
      {
      };
      
      //! Constructor
      OFileStream
         (  const std::string& aFilename
         ,  const StreamType&  aStreamType    = StreamType::MPI_MASTER
         ,  std::ios_base::openmode aOpenmode = std::ios_base::out
         );

      //! Close file on master/all depending on StreamType.
      void close();
      
      //! Output type T
      template<class T>
      OFileStream& operator<<(T&& t)
      {
         // Lock the stream of requested
         std::unique_lock<mutex_type> stream_lock(mStreamMutex, std::defer_lock);
         if(mLockStream)
         {
            stream_lock.lock();
         }
         
         // Do write out
         switch(mStreamType)
         {
            case MPI_MASTER:
            {
               if(IsMaster())
               {
                  dynamic_cast<std::ofstream&>(*this) << t;
               }
               break;
            }
            case MPI_ALL_DISTRIBUTED:
            {
               dynamic_cast<std::ofstream&>(*this) << t;
               break;
            }
            case ERROR:
            default:
               MIDASERROR("Unknown 'StreamType'.");
         }

         return *this;
      }
      
      //!
      OFileStream& operator<<(std::ostream& (*f)(std::ostream&))
      { 
         // Lock the stream if requested
         std::unique_lock<mutex_type> stream_lock(mStreamMutex, std::defer_lock);
         if(mLockStream)
         {
            stream_lock.lock();
         }
         
         // Do write out
         switch(mStreamType)
         {
            case MPI_MASTER:
            {
               if(IsMaster())
               {
                  dynamic_cast<std::ofstream&>(*this) << f;
               }
               break;
            }
            case MPI_ALL_DISTRIBUTED:
            {
               dynamic_cast<std::ofstream&>(*this) << f;
               break;
            }
            case ERROR:
            default:
               MIDASERROR("Unknown 'StreamType'.");
         }

         return *this;
      }

   private:
      //! Type of stream
      StreamType    mStreamType;

      //! Stream mutex for locking the stream when outputting.
      mutex_type    mStreamMutex;

      //! Lock the stream when outputting.
      bool          mLockStream = true;
};

} /* namespace mpi */
} /* namespace midas */

#endif /* MIDAS_MPI_FILESTREAM_H_INCLUDED */
