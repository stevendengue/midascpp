/**
************************************************************************
* 
* @file                UtilFunc.h
*
* Created:             10-10-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:   Utility function for doing MPI_AllReduce on a GeneralMidasVector
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef MIDAS_MPI_UTIL_FUNC_H_INCLUDED
#define MIDAS_MPI_UTIL_FUNC_H_INCLUDED

#ifdef VAR_MPI
#include<mpi.h>
#include<complex>
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mpi/DataTypeTrait.h"

namespace midas
{
namespace mpi
{

/** 
 * AllReduce for GeneralMidasVector.
 * Using DataTypeTraits we 'Get()' the type of the GeneralMidasVector allowing us not to worry about the data type when we code,
 * it will be handled auto-magically.
 *
 * @param arVector  The MidasVector to reduce.
 * @param aComm     The MPI communicator to call reduce on.
 * @param aOper     The reduction operator.
 **/
template<class T>
void AllReduceMidasVector
   (  GeneralMidasVector<T>& arVector
   ,  MPI_Comm aComm
   ,  MPI_Op aOper = MPI_SUM
   )
{
   GeneralMidasVector<T> temp(arVector);
   MPI_Allreduce(temp.data(), arVector.data(), arVector.Size(), DataTypeTrait<T>::Get(), aOper, aComm);
}

/**
 * Broadcast a GeneralMidasVector.
 * Using DataTypeTraits we 'Get()' the type of the GeneralMidasVector allowing us not to worry about the data type when we code,
 * it will be handled auto-magically.
 *
 * @param arVector  The MidasVector to broadcast.
 * @param aRoot     The root of the broadcast.
 * @param aComm     The MPI communicator to broadcast on.
 **/
template<class T>
void MPIBroadcastMidasVector
   (  GeneralMidasVector<T>& arVector
   ,  In aRoot = I_0
   ,  MPI_Comm aComm = get_CommunicatorManager().GetCurrentCommunicator().GetMpiComm()
   )
{
   MPI_Bcast(arVector.data(), arVector.Size(), DataTypeTrait<T>::Get(), aRoot, aComm);
}

} /* namespace mpi */
} /* namespace midas */

#endif /* VAR_MPI */

#endif /* MIDAS_MPI_UTIL_FUNC_H_INCLUDED */
