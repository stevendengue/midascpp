/**
************************************************************************
* 
* @file                Ode1D.h
*
* Created:             24-10-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Class for Ode1D(ordinatry differential equations 1D)
* 
* Last modified: man mar 21, 2005  11:50
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ODE1D_H
#define ODE1D_H

#include "inc_gen/TypeDefs.h"

class Ode1D
{
   private:
      Nb mTimeInit;
      Nb mSolInit;
      Nb mTimeEnd;
      PointerToFunc2 msfn;
   public:
      Ode1D(Nb t0, Nb x0, Nb tend, Nb (*f)(Nb, Nb));
      Nb* Euler(In n) const;
      Nb* EulerImp(In n) const;
      Nb* EulerPc(In n) const;
      Nb* Rk2(In n) const;
      Nb* Rk4(In n) const;
};

#endif

