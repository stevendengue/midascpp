/**
************************************************************************
* 
* @file                TinkerMM.h
*
* Created:             31-04-2007
*
* Author:              Mikkel Bo Hansen (mbh@chem.au.dk) 
*
* Short Description:   TinkerMM datatype
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TINKER_MM_H_INCLUDED
#define TINKER_MM_H_INCLUDED

// std headers
#include <string>
#include <vector>
#include <map>
#include <iostream> 

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "nuclei/Nuclei.h"
#include "util/Io.h"

// using declarations
using std::map;
using std::string;
using std::vector;
using std::ostream;

// Constants needed for the interface
// new programs must be specified here
// + have their own realization, or be
// run through the GENERIC realization.

/**
* Construct a definition of a single point
* */
class TinkerMM
{
   private:
      string mTinkerXyzFile;
      string mParamFile;
      Nb mEnergy;
      MidasVector mGradient;
      MidasMatrix mHessian;
      In mNatoms;
      bool mDoGradient;
      bool mDoHessian;

   public:
      ///< Constructors
      TinkerMM();
      TinkerMM(const string&);
      ///< destructor: virtual
      ~TinkerMM();

      ///< Below here is the gets and sets - selfexplanatory
      void SetDoGradient(bool aB) {mDoGradient=aB;}
      bool GetDoGradient() {return mDoGradient;}
      void SetDoHessian(bool aB) {mDoHessian=aB;}
      bool GetDoHessian() {return mDoHessian;}
      void SetParamFile(const string& aS) {mParamFile=aS;}
      void SetTinkerXyzFile(const string& aS) {mTinkerXyzFile=aS;}
      Nb GetEnergy() {return mEnergy;}
      MidasVector GetGradient() {return mGradient;}
      MidasMatrix GetHessian() {return mHessian;}
      In GetNatoms() const {return mNatoms;}

      ///< Below here follows the "real" functions
   
      ///< Evaluation function
      void Evaluate();
      ///< Setup function
      void Setup(const vector<Nuclei>);
      ///< Update Geometry function
      void UpdateGeometry(const vector<Nuclei>);
};

#endif /* TINKER_MM_H_INCLUDED */
