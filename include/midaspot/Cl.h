/*=============================================================*
 *
 * midaspot/Cl.h header begin
 *
 *=============================================================*/
#ifndef MIDASPOT_CL_H_INCLUDED
#define MIDASPOT_CL_H_INCLUDED

/*************************************************************** 
 * 
 * Includes needed for command-line (cl) interface
 * 
 ***************************************************************/
#include <string>
#include <cstring>
#include <iostream>
#include <cmath>
#include <fstream>
#include <sstream>
#include <exception>
#include <stdexcept>
#include <vector>
#include <string>
#include <sstream>
#include <limits>
#include <iomanip>
#include <functional>
#include <any>
#include <optional>
#include <cassert>
#include <map>
#include <list>

/*************************************************************** 
 * 
 * Command-line (cl) input handling code
 * 
 ***************************************************************/
/**
 * Convert string to integral or numeric type
 **/
#define GENERATE_FROM_STRING(type, func) \
type type##_from_string(const std::string& str) \
{ \
   size_t pend; \
   type result; \
   \
   try \
   { \
      result = func (str, &pend); \
   } \
   catch(std::exception& e) \
   { \
      throw std::runtime_error(std::string(e.what()) + ": Could not convert '" + str + "' to '" #type "'."); \
   } \
   if(pend != str.size()) \
   { \
      std::cerr << " Did not read complete string '" << str << "', when converting to '" #type "'." << std::endl; \
   } \
   return result; \
} 

GENERATE_FROM_STRING(int,    std::stoi);
GENERATE_FROM_STRING(double, std::stod);
GENERATE_FROM_STRING(float,  std::stof);

#undef GENERATE_FROM_STRING

using double_vector = std::vector<double>;
using int_vector    = std::vector<int>;
using bool_vector   = std::vector<bool>;
using string_vector = std::vector<std::string>;

/**
 * Convert string value to value of type 'type'
 **/
std::any any_from_string(const std::string& type, const std::string& value)
{
   std::any any_value;

   if(false)
   {
   }
   else if(type == "double")
   {
      any_value = double_from_string(value);
   }
   else if(type == "int")
   {
      any_value = int_from_string(value);
   }
   else if(type == "bool")
   {
      any_value = bool(value == "true" ? true : false);
   }
   else if(type == "string")
   {
      any_value = value;
   }

   return any_value;
}

/**
 *
 **/
std::any any_vec_from_type(const std::string& type)
{
   std::any any_vec;

   if(false)
   {
   }
   else if(type == "double")
   {
      any_vec = double_vector{};
   }
   else if(type == "int")
   {
      any_vec = int_vector{};
   }
   else if(type == "bool")
   {
      any_vec = bool_vector{};
   }
   else if(type == "string")
   {
      any_vec = string_vector{};
   }

   return any_vec;
}

/**
 *
 **/
void append_to_any_vec(std::any& any_vec, const std::string& type, const std::string& value)
{
   if(false)
   {
   }
   else if(type == "double")
   {
      double_vector* vec = std::any_cast<double_vector>(&any_vec);
      vec->emplace_back(double_from_string(value));
   }
   else if(type == "int")
   {
      int_vector* vec = std::any_cast<int_vector>(&any_vec);
      vec->emplace_back(int_from_string(value));
   }
   else if(type == "bool")
   {
      bool_vector* vec = std::any_cast<bool_vector>(&any_vec);
      vec->emplace_back(value == "true" ? true : false);
   }
   else if(type == "string")
   {
      string_vector* vec = std::any_cast<string_vector>(&any_vec);
      vec->emplace_back(value);
   }
}


/**
 * Split string with delimeter.
 **/
std::vector<std::string> split_string(const std::string& str, char delimeter = ' ')
{
   std::vector<std::string> split;
   std::stringstream s_stream(str);
   std::string elem;
   while(std::getline(s_stream, elem, delimeter))
   {
      if(!elem.empty())
      {
         split.emplace_back(elem);
      }
   }
   return split;
}

/**
 * Left trim a string
 **/
std::string ltrim(const std::string& str, const std::string& remove = " \n\r\t\f\v")
{
   size_t start = str.find_first_not_of(remove);
	return (start == std::string::npos) ? "" : str.substr(start);
}

/*************************************************************** 
 * 
 * Commandline (cl) input handling code
 * 
 ***************************************************************/
/**
 * Struct for tokenizing commandline input
 **/
struct cl_tokenizer_type
{
   int          argc;
   const char** argv;

   int          iarg = 0;

   const char* next(bool except = true)
   {
      iarg += 1;
      const char* ret = iarg < argc ? argv[iarg] : nullptr;
      if(except && !ret)
      {
         throw(std::runtime_error("No tokens left."));
      }
      return ret;
   }

   int num() const
   {
      return iarg;
   }
};

/**
 * Entry that defines a commandline option, e.g. '--help'
 **/
struct cl_entry_type
{
   using string_type        = std::string;
   using string_vector_type = std::vector<std::string>;
   using function_type      = std::function<void(cl_tokenizer_type&, const cl_entry_type&)>;
   
   string_type        long_name;
   string_vector_type entry;
   string_type        type;
   string_type        description;
   string_type        nargs;

   function_type func = {};
};

/**
 *
 **/
struct cl_args_type
{
   using map_type             = std::map<std::string, std::any>;

   map_type map;
   int      nargs = 0;


   template<class T>
   std::optional<T> get(const std::string& key) const
   {
      auto iter = map.find(key);
      if(iter != map.end())
      {
         return {std::any_cast<T>(iter->second)};
      }
      return std::nullopt;
   }
};

/**
 *
 **/
void cl_args_map_insert(cl_args_type& cl_args, const cl_entry_type& entry, cl_tokenizer_type& tokenizer)
{
   auto iter = cl_args.map.find(entry.long_name);
   if(iter != cl_args.map.end())
   {
      if(entry.nargs == "exclusive")
      {
         throw std::runtime_error("Option '" + entry.long_name + "' already set.");  
      }
      else
      {
         append_to_any_vec(iter->second, entry.type, tokenizer.next());
      }
   }
   else
   {
      if(entry.nargs == "exclusive")
      {
         if(entry.type.empty())
         {
            cl_args.map.emplace(entry.long_name, any_from_string("bool", "true"));
         }
         else
         {
            cl_args.map.emplace(entry.long_name, any_from_string(entry.type, tokenizer.next()));
         }
      }
      else
      {
         auto emplace_pair = cl_args.map.emplace(entry.long_name, any_vec_from_type(entry.type));
         append_to_any_vec(emplace_pair.first->second, entry.type, tokenizer.next());
      }
   }
}

// Some forward declarations
struct cl_type;
void parse_cl_args_impl(cl_tokenizer_type& tokenizer, const cl_type& cl);

/**
 *
 **/
struct cl_type
{
   struct string_pair
   {
      std::string first;
      std::string second;
   };

   using entry_vector_type    = std::vector<cl_entry_type>;
   using commands_vector_type = std::list<cl_type>;
   using argument_vector_type = std::vector<string_pair>;

   entry_vector_type commands;
   entry_vector_type options;
   commands_vector_type cls;

   argument_vector_type arguments;
   int nargs = 0;
   mutable int iargs = 0;

   cl_args_type* cl_args = nullptr;

   /**
    *
    **/
   cl_type()
   {
      this->option("--help -h", "", "Print this message.");
   }

   /**
    *
    **/
   void check_is_valid() const
   {
      assert(cl_args);
   }

   /**
    *
    **/
   void set_cl_args(cl_args_type* args)
   {
      cl_args = args;
      for(auto& cmd : cls)
      {
         cmd.set_cl_args(args);
      }
   }

   /**
    *
    **/
   cl_type& command(const std::string& cmd, const std::string& description)
   {
      // Create command
      cls.emplace_back();

      auto& cl = cls.back();
         
      // Create command handler
      commands.emplace_back(cl_entry_type{cmd, std::vector<std::string>{cmd}, "", description});
      
      cl_entry_type& entry = commands.back();
      entry.func = [this, &cl, cmd](cl_tokenizer_type& tokenizer, const cl_entry_type& entry)
         {
            cl_args->map.emplace(cmd, any_from_string("bool", "true"));
            parse_cl_args_impl(tokenizer, cl);
         };
      
      // Return new 'cl_type' to provide a handle for the new command
      return cl;
   }

   /**
    *
    **/
   cl_type& option(const std::string& opt, const std::string& type, const std::string& description, const std::string& nargs = "exclusive", const std::string& default_value = "")
   {
      auto option_vector   = split_string(opt);
      //auto argument_vector = split_string(arguments);
      
      assert(option_vector.size() > 0);

      auto long_name = ltrim(option_vector[0], "-");
      
      // Create option handler
      options.emplace_back(cl_entry_type{long_name, option_vector, type, description, nargs}); 

      cl_entry_type& entry = options.back();
      entry.func = [this](cl_tokenizer_type& tokenizer, const cl_entry_type& entry)
         {
            cl_args_map_insert(*cl_args, entry, tokenizer);
         };

      //if(!default_value.empty())
      //{
      //   cl_args->map.emplace(entry.long_name, any_from_string());
      //}
      
      // Return 'this' for command chaining
      return *this;
   }
   
   /**
    *
    **/
   cl_type& argument(const std::string& arg, const std::string& type)
   {
      arguments.emplace_back(string_pair{arg, type});

      ++nargs;

      return *this;
   }

   /**
    *
    **/
   bool handle_argument(const std::string& token) const
   {
      if(iargs >= nargs)
      {
         return false;
      }
      const string_pair& pair = arguments[iargs];

      assert(cl_args);
      cl_args->map.emplace(pair.first, any_from_string(pair.second, token));
      ++(cl_args->nargs);
      ++iargs;
      return true;
   }
};

/**
 * Search for commandline option in an commandline input map
 **/
const cl_entry_type* search_cl(const cl_type& cl, const std::string& token)
{
   const auto search = [](const cl_type::entry_vector_type& entry_vector, const std::string& token){
      for(int i = 0; i < entry_vector.size(); ++i)
      {
         const cl_entry_type& cl_entry = entry_vector[i];
         for(int j = 0; j < cl_entry.entry.size(); ++j)
         {
            const auto& entry_str = cl_entry.entry[j];
            if(token == entry_str)
            {
               return &(cl_entry);
            }
         }
      }
      return static_cast<const cl_entry_type*>(nullptr);
   };

   const cl_entry_type* ptr;
   if (  (ptr = search(cl.commands, token)) )
   {
      return ptr;
   }
   if (  (ptr = search(cl.options, token)) )
   {
      return ptr;
   }
   return nullptr;
}

/**
 * Print usage message and exit
 **/
void usage(const cl_tokenizer_type& tokenizer, const cl_type& cl, int exit_code = 0)
{
   // Some constants for formatting print out
   std::string tab   = "   ";
   int         width = 18;
   
   auto output_entry_vector = [&tab, &width](std::ostream* ptr_ostr, const cl_type::entry_vector_type& entry_vector)
      {
         for(int i = 0; i < entry_vector.size(); ++i)
         {
            auto& cl_entry = entry_vector[i];
            std::string message;
            for(int i = 0; i < cl_entry.entry.size(); ++i)
            {
               if(i != 0)
               {
                  message += " ";
               }
               message += cl_entry.entry[i];
            }
            if(!cl_entry.type.empty())
            {
               message += " <" + cl_entry.long_name + ">";
            }
            auto size = message.size();
            *ptr_ostr << tab << std::setw(width) << message;
            if(size < width)
            {
               *ptr_ostr << cl_entry.description << "\n";
            }
            else
            {
               *ptr_ostr << "\n" << tab << std::setw(width) << " " << cl_entry.description << "\n";
            }
         }

      };


   // Set ostream to std::cerr if non-0 exit code
   std::ostream* ptr_ostr = &std::cout;
   if(exit_code != 0)
   {
      ptr_ostr = &std::cerr;
   }
   *ptr_ostr << std::left;

   // Print message
   if(exit_code != 0)
   {
      for(int i = 0; i < tokenizer.argc; ++i)
      {
         *ptr_ostr << tokenizer.argv[i] << " ";
      }
      *ptr_ostr << "\n\n";
   }
   
   if(tokenizer.argc > 0)
   {
      *ptr_ostr << "Usage: " << tokenizer.argv[0] << " [options]";
   }
   for(int i = 0; i < cl.arguments.size(); ++i)
   {
      *ptr_ostr << " " << cl.arguments[i].first;
   }
   *ptr_ostr << "\n\n";
   
   if(!cl.commands.empty())
   {
      *ptr_ostr << "Commands:\n";
      output_entry_vector(ptr_ostr, cl.commands);
      *ptr_ostr << "\n";
   }

   if(!cl.options.empty())
   {
      *ptr_ostr << "Options:\n";
      output_entry_vector(ptr_ostr, cl.options);
      *ptr_ostr << "\n";
   }

   *ptr_ostr << std::flush;

   // Exit
   std::exit(exit_code);
}

/**
 * Overload for usage function
 **/
void usage(const cl_type& cl, int exit_code = 0)
{
   cl_tokenizer_type tokenizer{0, nullptr};
   usage(tokenizer, cl, exit_code);
}

/**
 * Parse commandline arguments and call handlers in 'cl_map'
 **/
void parse_cl_args_impl(cl_tokenizer_type& tokenizer, const cl_type& cl)
{
   const char* token;

   while (  (token = tokenizer.next(false))  )
   {
      const cl_entry_type* iter = search_cl(cl, token);
      if(iter)
      {
         // Handle option of found
         iter->func(tokenizer, *iter);
      }
      else
      {
         // If option not found we try to set a filename
         std::string arg = std::string(token);
         if(arg.at(0) == '-')
         {
            throw std::runtime_error("Unknown option : " + arg);
         }
         
         if(!cl.handle_argument(arg))
         {
            throw std::runtime_error("Unable to handle argument: '" + arg + "'.");
         }
      }
   }

   if(cl.cl_args->get<bool>("help"))
   {
      usage(tokenizer, cl, 0);
   }
}

/**
 *
 **/
cl_args_type parse_cl_args(int argc, const char* argv[], const cl_type& cl)
{
   cl_args_type cl_args;
   cl_tokenizer_type tokenizer{argc, argv};

   const_cast<cl_type&>(cl).set_cl_args(&cl_args);

   parse_cl_args_impl(tokenizer, cl);

   return cl_args;
}

#endif /* MIDASPOT_CL_H_INCLUDED */
/*=============================================================*
 *
 * midaspot/Cl.h header end
 *
 *=============================================================*/
