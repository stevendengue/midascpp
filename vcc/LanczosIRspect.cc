/**
************************************************************************
* 
* @file                LanczosIRspect.cc
*
* Created:             14-08-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*
* Short Description:   Lanczos IR spectrum class implementation.
* 
* Last modified: Mon Jan 18, 2010  04:25PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <string>
#include <map>
#include <sstream>
#include <algorithm>
#include <cctype>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "util/MidasStream.h"
#include "util/MidasSystemCaller.h"
#include "input/RspCalcDef.h"
#include "input/VscfCalcDef.h"
#include "input/VccCalcDef.h"
#include "input/Input.h"
#include "vcc/LanczosIRspect.h"
#include "vcc/LanczosRspFunc.h"
#include "vcc/LanczosLinRspFunc.h"
#include "vcc/LanczosSpectBlock.h"
#include "mmv/MidasMatrix.h"
#include "mpi/Impi.h"

using namespace std;

LanczosIRspect::LanczosIRspect(const string& aName,
                              const string& aXdip, const string& aYdip, const string& aZdip,
                              const string& aAnalyze, const string& aAnalysisBlocks, const bool aPrintIR,
                              const Nb aWeightLim, const Nb aPeakLim):
mName(aName), mXdip(aXdip), mYdip(aYdip), mZdip(aZdip), mPrintIRTable(aPrintIR), mWeightLimit(aWeightLim), mPeakLimit(aPeakLim)
{
   // all output is now directed to gAnalysisDir
   mName=gAnalysisDir+"/"+mName;
   // Check for ANALYZE keyword.
   string analyze = aAnalyze;
   transform(analyze.begin(), analyze.end(), analyze.begin(), (int (*)(int))std::toupper);
   if (analyze == "")
      mAnalyze = false;
   else if (analyze == "MINIMA" || analyze == "USER")
      mAnalyze = true;
   else 
      MIDASERROR(" #4 LanczosIR: ANALYZE parameter unknown.");

   // Check for ANALYSISBLOCKS keyword.
   if (aAnalysisBlocks != "")
   {
      if (aAnalysisBlocks[0] != '(' || aAnalysisBlocks[aAnalysisBlocks.size()-1] != ')')
         MIDASERROR(" #4 LanczosIR: ANALYSISBLOCKS format must be (freq1:freq2:...).");
      string freqstr = aAnalysisBlocks.substr(1, aAnalysisBlocks.size()-2);
      vector<string> freqs = SplitString(freqstr, ":");
      for (In i=I_0; i<freqs.size(); ++i)
      {
         Nb freq = C_0;
         istringstream is(freqs[i]);
         is >> freq;
         mAnalysisBlocks.push_back(freq/C_AUTKAYS);
      }
   }
}

void LanczosIRspect::Create(VccCalcDef* aVccCalcDef, Vcc* aVcc)
{
   // Get Lanczos response functions.
   LanczosLinRspFunc* lcsx=dynamic_cast<LanczosLinRspFunc*>(aVccCalcDef->GetLanczosRspFunc(mXdip));
   LanczosLinRspFunc* lcsy=dynamic_cast<LanczosLinRspFunc*>(aVccCalcDef->GetLanczosRspFunc(mYdip));
   LanczosLinRspFunc* lcsz=dynamic_cast<LanczosLinRspFunc*>(aVccCalcDef->GetLanczosRspFunc(mZdip));
   if (NULL==lcsx || NULL==lcsy || NULL==lcsz)
   {
      Mout << " LanczosIRspect::Create():" << endl
           << " IR spectrum: " << *this << endl
           << " One or more response functions was not found." << endl;
      MIDASERROR("");
   }

   // Check operators.
   if (  lcsx->GetOper1()!=lcsx->GetOper2() 
      || !OpDef::msOpInfo[lcsx->GetOper1()].GetType().Is(oper::dipole_x)
      || lcsy->GetOper1()!=lcsy->GetOper2() 
      || !OpDef::msOpInfo[lcsy->GetOper1()].GetType().Is(oper::dipole_y)
      || lcsz->GetOper1()!=lcsz->GetOper2() 
      || !OpDef::msOpInfo[lcsz->GetOper1()].GetType().Is(oper::dipole_z)
      )
   {
      MIDASERROR(" LanczosIRspect::Create(): Wrong operator types");
   }

   // Check consistency among functions.
   if (lcsx->GetFrqStart() != lcsy->GetFrqStart() || lcsx->GetFrqStart() != lcsz->GetFrqStart() ||
       lcsx->GetFrqEnd() != lcsy->GetFrqEnd()     || lcsx->GetFrqEnd() != lcsz->GetFrqEnd()     ||
       lcsx->GetFrqStep() != lcsy->GetFrqStep()   || lcsx->GetFrqStep() != lcsz->GetFrqStep()   ||
       lcsx->GetGamma() != lcsy->GetGamma()       || lcsx->GetGamma() != lcsz->GetGamma())
      MIDASERROR(" LanczosIRspect::Create(): Response function parameters not consistent.");

   // Get necessary values.
   Nb frqstart = lcsx->GetFrqStart();
   Nb frqend   = lcsx->GetFrqEnd();
   Nb frqstep  = lcsx->GetFrqStep();
   Nb gamma    = lcsz->GetGamma();
   In nfrq = In((frqend-frqstart)/frqstep) + I_1;
   MidasVector xints(nfrq);
   MidasVector yints(nfrq);
   MidasVector zints(nfrq);
   MidasVector ints(nfrq);
   MidasVector fints(nfrq);
   lcsx->AddImVals(xints); 
   lcsy->AddImVals(yints); 
   lcsz->AddImVals(zints);
   {
      // Setup output file.
      string filename = mName + ".dat";
      ofstream file_stream(filename.c_str(), ios_base::trunc);
      MidasStreamBuf file_buf(file_stream);
      MidasStream file(file_buf);
      file << "# IR absorption spectrum generated using Lanczos chains." << endl
           << "# Operators: xdip=" << mXdip << endl
           << "#            ydip=" << mYdip << endl
           << "#            zdip=" << mZdip << endl
           << "# Gamma (damping factor): " << gamma*C_AUTKAYS << " cm-1" << endl
           << "# Columns: freq. (cm-1)   Absorption (arb. units)   "
           << "Abs (xdip)   Abs (ydip)   Abs (zdip)" << endl;
   
      file.precision(16); 
      for (In i=I_0; i<nfrq; ++i)
      {
         Nb frq = frqstart + i*frqstep;
         xints[i] *= frq;
         yints[i] *= frq;
         zints[i] *= frq;
         ints[i] = xints[i]+yints[i]+zints[i];
         file << frq*C_AUTKAYS << "   " << ints[i]
              << "   " << xints[i] << "   " << yints[i] << "   " << zints[i] << endl;
      }
      file_stream.close();
   }
   Mout << "Between the two creators " << endl;
   if(aVcc->GetMethod() == VCC_METHOD_VCC)
   {
      xints.Zero();
      yints.Zero();
      zints.Zero();
      lcsx->AddFImContribs(xints);
      lcsy->AddFImContribs(yints);
      lcsz->AddFImContribs(zints);
      // Setup output file.
      string filename = mName + "_Fcontrib.dat";
      Mout << "Creating file : " << filename << endl;
      ofstream file_stream(filename.c_str(), ios_base::trunc);
      MidasStreamBuf file_buf(file_stream);
      MidasStream file(file_buf);
      file << "# IR absorption spectrum generated using Lanczos chains." << endl
           << "# Operators: xdip=" << mXdip << endl
           << "#            ydip=" << mYdip << endl
           << "#            zdip=" << mZdip << endl
           << "# Gamma (damping factor): " << gamma*C_AUTKAYS << " cm-1" << endl
           << "# Columns: freq. (cm-1)   Absorption (arb. units)   "
           << "Abs (xdip)   Abs (ydip)   Abs (zdip)" << endl;

      file.precision(16);
      for (In i=I_0; i<nfrq; ++i)
      {
         Nb frq = frqstart + i*frqstep;
         xints[i] *= frq;
         yints[i] *= frq;
         zints[i] *= frq;
         fints[i] = xints[i]+yints[i]+zints[i];
         file << frq*C_AUTKAYS << "   " << fints[i]
              << "   " << xints[i] << "   " << yints[i] << "   " << zints[i] << endl;
      }
      file_stream.close();
   }
   GenerateGnuPlot();
   
   IdentifyPeaks(ints, frqstart, frqend, frqstep);
   if (mAnalyze)
      AnalyzeSpect(aVccCalcDef, aVcc, ints, frqstart, frqend, frqstep);
}

void LanczosIRspect::GenerateGnuPlot() const
{
   string filename = mName + ".plot";
   ofstream file_stream(filename.c_str(), ios_base::trunc);
   MidasStreamBuf file_buf(file_stream);
   MidasStream file(file_buf);
   
   file << "set term postscript enhanced" << endl
        << "set output \"" << mName << ".eps\"" << endl << endl
        << "set xlabel \"Frequency (cm^{-1})\"" << endl
        << "set ylabel \"Intensity (arb. units)\"" << endl << endl
        << "plot \"" << mName << ".dat\" u 1:2 w lines" << endl;
   file_stream.close();

   if(midas::mpi::IsMaster())
   {
      system_command_t cmd = {"gnuplot", mName + ".plot"};
      MIDASSYSTEM(cmd);
   }
}

/**
 * Search for peaks by finding positions of most negative curvature.
 **/
void LanczosIRspect::IdentifyPeaks(const MidasVector& aInty,
                                   Nb aFrqStart, Nb aFrqEnd, Nb aFrqStep)
{
   In nfrq = In((aFrqEnd-aFrqStart)/aFrqStep) + I_1;
   for (In i=I_3; i<nfrq-I_3; ++i)
   {
      // Seidler: This should be optimized to avoid recalculation.
      Nb der0   = (aInty[i+I_1]-aInty[i-I_1])/aFrqStep/C_2;
      Nb derp1  = (aInty[i+I_2]-aInty[i])/aFrqStep/C_2;
      Nb derm1  = (aInty[i]-aInty[i-I_2])/aFrqStep/C_2;
      Nb derp2  = (aInty[i+I_3]-aInty[i+I_1])/aFrqStep/C_2;
      Nb derm2  = (aInty[i-I_1]-aInty[i-I_3])/aFrqStep/C_2;
      Nb curv0  = (derp1-derm1)/aFrqStep/C_2;
      Nb curvp1 = (derp2-der0)/aFrqStep/C_2;
      Nb curvm1 = (der0-derm2)/aFrqStep/C_2;
      if (curv0<C_0 && curv0<curvm1 && curv0<curvp1)
      {
         mPeaks.push_back(aFrqStart+i*aFrqStep);
         mPeaks_inty.push_back(aInty[i]);
      }
   }

   string filename = mName + "_stick.dat";
   ofstream file_stream(filename.c_str(), ios_base::trunc);
   MidasStreamBuf file_buf(file_stream);
   MidasStream file(file_buf);
   file << "# IR stick spectrum generated from full spectrum." << endl
        << "# Columns: freq. (cm-1)     Absorption" << endl;
   file.precision(16); 
   Nb max=C_0;
   for (In i=I_0; i<mPeaks.size(); ++i)
   {
      file << mPeaks[i]*C_AUTKAYS << "    0.0" << endl
           << mPeaks[i]*C_AUTKAYS << "    " << mPeaks_inty[i] << endl
           << mPeaks[i]*C_AUTKAYS << "    0.0" << endl;
      if (mPeaks_inty[i]>max) max=mPeaks_inty[i]; 
   }
   Mout << " Peak positions and Intensities - abs and relative to max: " << endl; 
   Mout << " --------------------------------------------------------- " << endl; 
   for (In i=I_0; i<mPeaks.size(); ++i)
   {
      Mout << " " << mPeaks[i]*C_AUTKAYS << "    " << mPeaks_inty[i] << "  " << mPeaks_inty[i]/max << endl; 
   }
   Mout << endl;
   file_stream.close();
}

void LanczosIRspect::AnalyzeSpect(VccCalcDef* aVccCalcDef, Vcc* aVcc,
                                  const MidasVector& aInty,
                                  Nb aFrqStart, Nb aFrqEnd, Nb aFrqStep)
{
   Mout << " Analyzing Lanczos IR spectrum with name: '" << mName << "'" << endl;
   
   vector<LanczosLinRspFunc*> lcs(3);
   lcs[0] = dynamic_cast<LanczosLinRspFunc*>(aVccCalcDef->GetLanczosRspFunc(mXdip));
   lcs[1] = dynamic_cast<LanczosLinRspFunc*>(aVccCalcDef->GetLanczosRspFunc(mYdip));
   lcs[2] = dynamic_cast<LanczosLinRspFunc*>(aVccCalcDef->GetLanczosRspFunc(mZdip));

   if (mAnalysisBlocks.empty())
      InitAnalysisBlocksFromMinima(aInty, aFrqStart, aFrqEnd, aFrqStep);
   mAnalysisBlocks.push_back(aFrqEnd);   // Necessary for the generation of block objects below.
   //New code
   vector<In> poss(3);
   poss[0] = I_0;
   poss[1] = I_0;
   poss[2] = I_0;
   vector<Nb>::iterator it = mAnalysisBlocks.begin();
   for(In j = I_0; j < mAnalysisBlocks.size(); ++j)
   {
      vector<Nb> aTemp;
      for(In i=I_0; i<I_3; ++i)
      {
         MidasVector eigvals;
         MidasMatrix eigvecs;
         MidasVector weights;
         lcs[i]->GetEigSolutions(eigvals, eigvecs);
         while(poss[i] < eigvals.Size() && eigvals[poss[i]] < mAnalysisBlocks[j])
         {
            aTemp.push_back(eigvals[poss[i]]);
            poss[i]++;
         }
      }
      if(aTemp.size() == I_0)
         mAnalysisBlocks.erase(it+j);
   }
   
   //CheckAnalysisBlocksForEigvals
   // Setup blocks to be analyzed
   vector<LanczosSpectBlock> blocks;
   LanczosSpectBlock curblock;
   In nfrq = In((aFrqEnd-aFrqStart)/aFrqStep) + I_1;
   Nb aMaxInt = C_0;//Getting the int. of the largest block
   for (In i=I_1; i<nfrq-I_1; ++i)
   {
      curblock.mIntegrInty += aInty[i]*aFrqStep;
      if (aFrqStart + i*aFrqStep >= mAnalysisBlocks[blocks.size()])
      {
         curblock.mEnd = aFrqStart+i*aFrqStep;
         aMaxInt = max(curblock.mIntegrInty,aMaxInt);
         blocks.push_back(curblock);
         curblock.mBegin = aFrqStart+i*aFrqStep;
         curblock.mIntegrInty = C_0;
      }
   }
   curblock.mEnd = aFrqEnd;
   blocks.push_back(curblock);
   Mout << "    Number of blocks: " << blocks.size() << endl;

   for (In i=I_0; i<blocks.size(); ++i)
      blocks[i].mWeights.ChangeStorageTo("InMem");
   
   // Get weights of excitations in each block.
   for (In i=I_0; i<I_3; ++i)
   {
      Mout << "    Analysis for operator '" << lcs[i]->GetOper1() << endl;//"' (one dot per block): ";
      MidasVector eigvals;
      MidasMatrix eigvecs;
      MidasMatrix lefteigvecs;
      MidasVector weights;
      MidasVector imeigvals;
      if(aVcc->GetMethod() == VCC_METHOD_VCI)
      {
         lcs[i]->GetEigSolutions(eigvals, eigvecs);
         imeigvals.SetNewSize(eigvals.Size());
         imeigvals.Zero();
      }
      else if(aVcc->GetMethod() == VCC_METHOD_VCC)
      {
         lcs[i]->GetEigSolutions(weights, eigvals, imeigvals, eigvecs, lefteigvecs);
      }
      In cur_eigval = I_0;
      for (In b=I_0; b<blocks.size(); ++b)
      {
         for (In e=cur_eigval; e<eigvals.Size(); ++e)
         {
            if (eigvals[e] < blocks[b].mEnd)
            {
               MidasVector eigv(eigvecs.Nrows());   // Eigenvector of tridiagonal matrix.
               DataCont x;                          // Approximate eigenvector of A matrix.
               eigvecs.GetCol(eigv, e);
               if(aVcc->GetMethod() == VCC_METHOD_VCI)
               {
                  lcs[i]->TransformStoX(eigv, x);
                  x.Pow(C_2);
               }
               else if(aVcc->GetMethod() == VCC_METHOD_VCC)
               {
                  DataCont y;
                  MidasVector leigv(eigvecs.Nrows());
                  lefteigvecs.GetCol(leigv, e);
                  if(imeigvals[e] == C_0)
                  {
                     lcs[i]->TransformStoX(eigv, leigv, x, y);
                     MidasVector xk(x.Size()), yk(x.Size());
                     x.DataIo(IO_GET, xk, x.Size());
                     y.DataIo(IO_GET, yk, x.Size());
                     for(In k = I_0; k < x.Size(); k++)
                     {
                        xk[k] = xk[k]*xk[k];
                     }
                     x.DataIo(IO_PUT, xk, x.Size());
                  }
                  else
                  {
                     DataCont xIm;
                     DataCont yIm;
                     lcs[i]->TransformStoX(eigv, leigv, x, y);
                     eigvecs.GetCol(eigv, e+I_1);
                     lefteigvecs.GetCol(leigv, e+I_1);
                     lcs[i]->TransformStoX(eigv, leigv, xIm, yIm);
                     MidasVector xk(x.Size()), yk(x.Size()), xImk(x.Size()), yImk(x.Size());
                     x.DataIo(IO_GET, xk, x.Size());
                     y.DataIo(IO_GET, yk, x.Size());
                     xIm.DataIo(IO_GET, xImk, x.Size());
                     yIm.DataIo(IO_GET, yImk, x.Size());
                     for(In k = I_0; k < x.Size(); k++)
                     {
                        xk[k] = xk[k]*xk[k]+xImk[k]*xImk[k];
                     }
                     x.DataIo(IO_PUT, xk, x.Size());
                     e++;
                  }
               }
               if (blocks[b].mWeights.Size() == I_0)
               {
                  blocks[b].mWeights.SetNewSize(x.Size());
                  blocks[b].mWeights.Zero();
               }
               if(aVcc->GetMethod() == VCC_METHOD_VCI)
               {
                  const In nout = I_20;
                  vector<Nb> weights(nout);
                  vector<In> addr(nout);
                  x.AddressOfExtrema(addr, weights, nout, I_1);
                  VccTransformer* trf = dynamic_cast<VccTransformer*>(aVcc->GetRspTransformer());
                  if (NULL == trf)
                     MIDASERROR("LanczosSpectBlock, operator<<: Error in getting Xvec.");
                  blocks[b].mWeights.Axpy(x, eigvals[e]*eigv[I_0]*eigv[I_0]);
               }
               else if(aVcc->GetMethod() == VCC_METHOD_VCC)
               {
                  blocks[b].mWeights.Axpy(x, fabs(weights[e]));
               }
               blocks[b].mNstates++;
            }
            else if(eigvals[e] > blocks[b].mEnd)
            {
               cur_eigval = e;
               break;
            }
         }
         //Mout << "." << std::flush;
      }
      Mout << endl;
   }   
   for (In b=I_0; b<blocks.size(); ++b)
   {
      blocks[b].mWeights.Scale(C_1/blocks[b].mWeights.SumEle());
      blocks[b].mMaxInt = aMaxInt;
   }

   Mout << endl
        << " Lanczos IR spectrum analysis results:" << endl;
   for (In b=I_0; b<blocks.size(); ++b)
   {
      blocks[b].mVcc = aVcc;
      Mout << " Block #" << b+1 << ":" << endl
           << blocks[b] << endl;

      // Extra check to make me (seidler) feel confident.
      if (fabs(blocks[b].mWeights.SumEle() - C_1) > 0.00000001)
      {
         Mout << " Warning:" << endl
              << " Sum of weights differ significantly from 1: " << blocks[b].mWeights.SumEle()
              << endl;
      }
   }
}

void LanczosIRspect::InitAnalysisBlocksFromMinima(const MidasVector& aInty,
                                                  Nb aFrqStart, Nb aFrqEnd, Nb aFrqStep)
{
   mAnalysisBlocks.clear();
   In nfrq = In((aFrqEnd-aFrqStart)/aFrqStep) + I_1;
   for (In i=I_1; i<nfrq-I_1; ++i)
      if (aInty[i]<aInty[i-I_1] && aInty[i]<aInty[i+I_1])
         mAnalysisBlocks.push_back(aFrqStart+i*aFrqStep);
}

ostream& operator<<(ostream& aOut, const LanczosIRspect& aSpec)
{
   Mout << "LIR: Name: " << aSpec.mName
        << " opers: xdip=" << aSpec.mXdip << " ydip=" << aSpec.mYdip << " zdip=" << aSpec.mZdip;
   return aOut;
}
