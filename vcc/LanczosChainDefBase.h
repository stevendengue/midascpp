/**
************************************************************************
* 
* @file                LanczosChainDefBase.h
*
* Created:             12-01-2011
*
* Author:              Ian H. Godtliebsen  (mrgodtliebsen@hotmail.com)
*
* Short Description:   A base class for Lanczos chain definitions.
* 
* Last modified: Wed Jan 12, 2011  14:31PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef LANCZOSCHAINDEFBASE_H
#define LANCZOSCHAINDEFBASE_H

#include <string>
using std::string;
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"

class LanczosChainDef;
class NHermLanczosChainDef;
class BandLanczosChainDef;
class NHBandLanczosChainDef;

class LanczosChainDefBase
{
   protected:
      In mLength;
      In mOrtho;

   public:
      LanczosChainDefBase() {};
      LanczosChainDefBase(In aLength,In aOrtho) {mLength=aLength; mOrtho=aOrtho;}

      virtual ~LanczosChainDefBase() {};

      static LanczosChainDefBase* Factory(const bool aNHerm, const string aOper2, const In aLength, const In aOrtho); 
      static LanczosChainDefBase* Factory(const bool aNHerm, const In aChainLen, 
                                          const In aBlockSize, const In aOrthoChain, 
                                          const bool aSaveChain, int aSaveInterval, const bool aTestChain, const Nb aDefThresh, const In aLBLockSize=0,
                                          const bool aDeflate=true, const Nb aConvThresh=C_0,
                                          const Nb aConvVal=C_0);

      virtual void  AddXoper(const string& aXoper) {return;}
      virtual void  AddOper(const string& aOper) {return;}
};

#endif //LANCZOSCHAINDEFBASE_h
