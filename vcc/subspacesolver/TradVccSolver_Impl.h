/**
 *******************************************************************************
 * 
 * @file    TradVccSolver_Impl.h
 * @date    10-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef TRADVCCSOLVER_IMPL_H_INCLUDED
#define TRADVCCSOLVER_IMPL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "input/ModeCombiOpRange.h"
#include "mmv/MidasVector.h"
#include "td/tdvcc/trf/TrfTimTdvccMatRep.h"
#include "td/tdvcc/params/ParamsTimTdvcc.h"
#include "td/tdvcc/anal/ExpValTimTdvcc.h"
#include "td/tdvcc/anal/FvciNorm2.h"
#include "td/tdvcc/TimTdvccEnums.h"
#include "it_solver/nl_solver/SubspaceSolver.h"

namespace midas::vcc::subspacesolver
{

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   bool TradVccSolver<PARAM_T>::SolveImpl
      (
      )
   {
      // The ModeCombiOpRange used _must_ include empty ModeCombi, but this is
      // handled in VibCorrSubspaceSolver::SetupMcr().
      const ModeCombiOpRange& mcr = this->Mcr();

      // The lambdas doing the transformations.
      midas::tdvcc::TrfTimTdvccMatRep<param_t> trf(this->NModals(), this->Oper(), this->ModInts(), mcr);
      trf.IoLevel() = this->IoLevel();
      trf.TimeIt() = this->TimeIt();
      auto f_err_vec = [&trf](const vec_t& t_ampls) -> vec_t
      {
         vec_t e = trf.ErrVec(0, t_ampls);
         if (Size(e) > 0)
         {
            e[0] = param_t(0);
         }
         return e;
      };

      // Setup preconditioner if necessary (otherwise leave as dummy).
      vec_t precon = this->InvDiagJac0Precon()? this->InvDiagJac0(): vec_t();

      // Setup t_ampls solver and solve.
      absval_t res_norm_thr_per_vec = this->ResNormThr()/sqrt(2.);
      auto p_solver_t = midas::nlsolver::SubspaceSolverFactory<decltype(f_err_vec)>
         (  this->InvDiagJac0Precon()
         ,  this->UsingCrop()
         ,  f_err_vec
         ,  this->SubspaceDim()
         ,  this->MaxIter()
         ,  res_norm_thr_per_vec
         ,  precon
         );
      vec_t t_ampls(this->SizeParamCont(), param_t(0));
      const bool conv_t = p_solver_t->Solve(t_ampls);
      this->SetNIter(p_solver_t->NumIter());
      this->SetLastResNorm(p_solver_t->LastResNorm());

      // Setup l_coefs lambda and solver.
      const vec_t eta = trf.EtaVec(0, t_ampls);
      auto f_lA_p_eta = [&trf,&t_ampls,&eta](const vec_t& l_coefs) -> vec_t
      {
         vec_t res = eta;
         res += trf.LJac(0, t_ampls, l_coefs);
         if (Size(res) > 0)
         {
            res[0] = param_t(0);
         }
         return res;
      };

      // If t_ampls converged to below threshold, we can allow a slightly
      // looser threshold for l.
      if (this->LastResNorm() < this->ResNormThr())
      {
         res_norm_thr_per_vec = sqrt(pow(this->ResNormThr(),2) - pow(this->LastResNorm(),2));
      }

      // Solve.
      auto p_solver_l = midas::nlsolver::SubspaceSolverFactory<decltype(f_lA_p_eta)>
         (  this->InvDiagJac0Precon()
         ,  this->UsingCrop()
         ,  f_lA_p_eta
         ,  this->SubspaceDim()
         ,  this->MaxIter()
         ,  res_norm_thr_per_vec
         ,  std::move(precon)
         );
      vec_t l_coefs(this->SizeParamCont(), param_t(0));
      const bool conv_l = p_solver_l->Solve(l_coefs);

      // Convergence info.
      this->SetConv(conv_t && conv_l);
      this->SetNIter(this->NIter() + p_solver_l->NumIter());
      this->SetLastResNorm(sqrt(pow(this->LastResNorm(),2) + pow(p_solver_l->LastResNorm(),2)));
      this->SetExpVal(midas::tdvcc::ExpVal
         (  absval_t(0)
         ,  midas::tdvcc::ParamsTimTdvcc<param_t,GeneralMidasVector>(t_ampls, l_coefs)
         ,  trf
         ));
      this->SetSolVecs(std::vector<vec_t>{std::move(t_ampls), std::move(l_coefs)});

      return this->Conv();
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   GeneralMidasVector<typename TradVccSolver<PARAM_T>::param_t> TradVccSolver<PARAM_T>::FvciVecImpl
      (  bool aBraNotKet
      )  const
   {
      midas::tdvcc::ParamsTimTdvcc<param_t,GeneralMidasVector> params(this->SolVecs().at(0), this->SolVecs().at(1));
      if (aBraNotKet)
      {
         return midas::tdvcc::ConvertBraToFvci(params, this->NModals(), this->Mcr());
      }
      else
      {
         return midas::tdvcc::ConvertKetToFvci(params, this->NModals(), this->Mcr());
      }
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   std::string TradVccSolver<PARAM_T>::CorrType
      (
      )  const
   {
      return midas::tdvcc::StringFromEnum(midas::tdvcc::CorrType::VCC);
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename PARAM_T
      >
   std::vector<std::string> TradVccSolver<PARAM_T>::PrivGetFileDescriptors
      (
      )  const
   {
      return GetFileDescriptors();
   }

} /* namespace midas::vcc::subspacesolver */

#endif/*TRADVCCSOLVER_IMPL_H_INCLUDED*/
