#ifndef DIRPRODSPECIALMIXED_IMPL_H_INCLUDED
#define DIRPRODSPECIALMIXED_IMPL_H_INCLUDED

#include "input/ModeCombi.h"
#include "tensor/SimpleTensor.h"

namespace midas::vcc::dirprod
{

/**
 *
 **/
template
   <  typename T
   >
void DirProdTensorSimpleSpecialMixed1212
   (  NiceTensor<T>& aRes
   ,  const NiceTensor<T>& aInTensor0
   ,  const NiceTensor<T>& aInTensor1
   ,  const int aSz0
   ,  const int aSz1
   ,  const int aSz2
   ,  const int aSz3
   ,  const T aCoef
   )
{
   T* result_ptr = static_cast<SimpleTensor<T>* >(aRes.GetTensor())->GetData();
   T* in_ptr0 = static_cast<SimpleTensor<T>* >(aInTensor0.GetTensor())->GetData();
   T* in_ptr1 = static_cast<SimpleTensor<T>* >(aInTensor1.GetTensor())->GetData();

   T* in_ptr0_inner;
   T* in_ptr1_inner;
   T* in_ptr2_inner;
   T* in_ptr3_inner;
   
   in_ptr0_inner = in_ptr0;
   for (unsigned int i0 = 0; i0 < aSz0; ++i0)
   {
      in_ptr1_inner = in_ptr1;
      for (unsigned int i1 = 0; i1 < aSz1; ++i1)
      {
         in_ptr2_inner = in_ptr0_inner;
         for (unsigned int i2 = 0; i2 < aSz2; ++i2)
         {
            in_ptr3_inner = in_ptr1_inner;
            for (unsigned int i3 = 0; i3 < aSz3; ++i3)
            {
               (*result_ptr) += aCoef* (*in_ptr2_inner)* (*in_ptr3_inner);
               ++result_ptr;
               ++in_ptr3_inner;
            }
            ++in_ptr2_inner;
         }
         in_ptr1_inner += aSz3;
      }
      in_ptr0_inner += aSz2;
   }
}

/**
 *
 **/
template
   <  typename T
   >
bool DirProdTensorSimpleSpecialMixed
   (  NiceTensor<T>& aRes
   ,  const ModeCombi& aResMc
   ,  const std::vector<NiceTensor<T> >& aInTensor
   ,  const std::vector<ModeCombi>& aInMcs
   ,  const T aCoef
   )
{
   if(aInTensor.size() == 2)
   {
      if(aInMcs[0].Size() == 2 && aInMcs[1].Size() == 2)
      {
         if(  aInMcs[0].Mode(0) < aInMcs[1].Mode(0)
           && aInMcs[0].Mode(0) < aInMcs[1].Mode(1) 
           && aInMcs[1].Mode(0) < aInMcs[0].Mode(1) 
           && aInMcs[0].Mode(1) < aInMcs[1].Mode(1) 
           )
         {
            DirProdTensorSimpleSpecialMixed1212( aRes, aInTensor[0], aInTensor[1]
                                               , aInTensor[0].GetDims()[0]
                                               , aInTensor[1].GetDims()[0]
                                               , aInTensor[0].GetDims()[1]
                                               , aInTensor[1].GetDims()[1]
                                               , aCoef
                                               );
            return true;
         }
         if(  aInMcs[1].Mode(0) < aInMcs[0].Mode(0)
           && aInMcs[1].Mode(0) < aInMcs[0].Mode(1) 
           && aInMcs[0].Mode(0) < aInMcs[1].Mode(1) 
           && aInMcs[1].Mode(1) < aInMcs[0].Mode(1) 
           )
         {
            DirProdTensorSimpleSpecialMixed1212( aRes, aInTensor[1], aInTensor[0]
                                               , aInTensor[1].GetDims()[0]
                                               , aInTensor[0].GetDims()[0]
                                               , aInTensor[1].GetDims()[1]
                                               , aInTensor[0].GetDims()[1]
                                               , aCoef
                                               );
            return true;
         }
      }
      return false;
   }
   
   return false;
}

} /* namespace midas::vcc::dirprod */

#endif /* DIRPRODSPECIALMIXED_IMPL_H_INCLUDED */
