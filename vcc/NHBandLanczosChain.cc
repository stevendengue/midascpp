#include "util/Timer.h"
#include "util/conversions/FromString.h"
#include "mmv/MidasVector.h"
#include "mmv/Diag.h"
#include "NHBandLanczosChain.h"
#include "input/Input.h"
#include "input/OpDef.h"
#include "input/GlobalOperatorDefinitions.h"
#include "mmv/MidasMatrix.h"
#include "util/MidasSystemCaller.h"
#include "util/Os.h"

using namespace std;

In NHsign(Nb aNum)
{
   if(aNum >= 0)
      return 1;
   else if(aNum < 0)
      return -1;

   MIDASERROR("NHBandLanczos: Sign function mishap!");
   return 0;
}

NHBandLanczosChainDef::NHBandLanczosChainDef(In aLength, In aRBlock, In aLBlock, In aOrtho, 
                bool aSave, int aSaveInterval, Nb aDtol, Nb aConv, bool aDeflate):
        mLength(aLength), mRBlock(aRBlock), mLBlock(aLBlock), mOrtho(aOrtho), 
        mSave(aSave), mSaveInterval(aSaveInterval), mDtol(aDtol), mConv(aConv), mDeflate(aDeflate), 
        mTestChain(false) { }

bool NHBandLanczosChainDef::Compatible(const NHBandLanczosChainDef& aOther) const
{
   if (mOpers.size() != (aOther.mOpers).size() || mOrtho != aOther.mOrtho || mRBlock != aOther.mRBlock || mLBlock != aOther.mLBlock || mLength < aOther.mLength || mDtol != aOther.mDtol)
      return false;
   else
      for(In i = I_0; i<mOpers.size(); ++i)
         if(mOpers[i] != (aOther.mOpers)[i])
            return false;
   
   return true;
}

/*bool NHBandLanczosChainDef::Combine(const NHBandLanczosChainDef& aOther)
{
   if(mOpers.size() == (aOther.mOpers).size() && mOrtho == aOther.mOrtho && mBlock == aOther.mBlock && mDtol == aOther.mDtol)
   {
      for(In i=0; i<mOpers.size(); i++)
      {
         if(mOpers[i] != (aOther.mOpers)[i])
            return false;
      }
   }
   else
      return false;

   mLength = max(mLength, aOther.mLength);
   return true;
}*/

ostream& operator<<(ostream& aOut, const NHBandLanczosChainDef& aDef)
{
   aOut  << " Band Lanczos chain definition: " << endl
         << "     Chainlength:         " << aDef.GetLength() << endl
         << "     Right Block size:    " << aDef.GetRBlock() << endl
         << "     Left Block size:     " << aDef.GetLBlock() << endl
         << "     Ortho scheme:        " << aDef.GetOrtho() << endl
         << "     DefThresh:           " << aDef.GetDtol() << endl
         << "     DiagMethod:          " << aDef.GetDiagMethod() << endl
         << "     For operators:       ";
         for(In i=I_0; i<(aDef.GetOpers()).size(); i++)
         {
            aOut << aDef.GetOper(i) << " ";
         }
   aOut  << endl
         << "     Save Chain:          " << aDef.GetSave() << endl;

   return aOut;
}

NHBandLanczosChain::NHBandLanczosChain(const NHBandLanczosChainDef& aDef, Vcc* apVcc, VccTransformer* apTrf): mDef(aDef), mpVcc(apVcc), mpTrf(apTrf) 
{ 
   mFilePrefix = "nhbandchain_";
   if(mpVcc->GetMethod() == VCC_METHOD_VCC)
      mFilePrefix += "vcc_";
   else if(mpVcc->GetMethod() == VCC_METHOD_VCI)
      mFilePrefix += "vci_";
   for(In i=0; i<(mDef.GetOpers()).size(); i++)
   {
      mFilePrefix += mDef.GetOper(i);
   }
   mFilePrefix += "_ortho_" + std::to_string(mDef.GetOrtho());
   mFilePrefix += "_rb_" + std::to_string(mDef.GetRBlock());
   mFilePrefix += "_lb_" + std::to_string(mDef.GetLBlock());
   
   In nc = mpTrf->NexciXvec()-I_1;
   
   mTmatRight.SetNewSize(mDef.GetLength(), false, true);
   mTmatLeft.SetNewSize(mDef.GetLength(), false, true);
   time_t thetime = time(NULL);
   mRandSeed = In(thetime);
}

/**
 *
 **/
Nb NHBandLanczosChain::RandomNb
   (
   )
{
   mRandSeed = (8121*mRandSeed + 28411)%134456;
   return Nb(mRandSeed)/134456;
}

void NHBandLanczosChain::MakeStartVectors(const In nc, const vector<string>& aOpers)
{
   In Rdiff = aOpers.size() - mDef.GetRBlock();
   In Ldiff = aOpers.size() - mDef.GetLBlock();

   DataCont Multipliers;
   string MultiName = mpVcc->pVccCalcDef()->Name()+"_mul0_vec_0";
   Multipliers.GetFromExistingOnDisc(mpVcc->NrspPar(), MultiName);
   if(mpVcc-> GetMethod() == VCC_METHOD_VCC && ! Multipliers.ChangeStorageTo("InMem", true, false, true))
   {
      Mout << "Calculating Multipliers in NHBandLanczosChain::MakeStartVectors()." << endl;
      mpVcc->CalculateEta0();
      mpVcc->SolveZerothOrderEq();
      Multipliers.ChangeStorageTo("InMem", true, false, true);
   }
   //Mout << " Multipliers... " << Multipliers << endl;
   Multipliers.SaveUponDecon(true);
   Multipliers.ChangeStorageTo("OnDisc");

   for(In j=I_0; j<aOpers.size(); j++)
      for(In iop=I_0; iop<gOperatorDefs.GetNrOfOpers(); iop++)
         if(gOperatorDefs[iop].Name() == aOpers[j])
         {
            Nb exptval = C_0;
            DataCont eta(nc, C_0, "InMem", gOperatorDefs[iop].Name(), true);
            if(mpVcc->GetMethod() == VCC_METHOD_VCC)
               mpVcc->CalculateVccEtaX(eta, aOpers[j], iop);
            else
               mpVcc->CalculateVciEta(eta, aOpers[j], iop, exptval);
            mEta.push_back(eta);
            //Mout << " Eta " << mEta.back() << endl;

            exptval = C_0;
            DataCont xi(nc, C_0, "InMem", gOperatorDefs[iop].Name(), true);
            if(mpVcc->GetMethod() == VCC_METHOD_VCC)
               mpVcc->CalculateXi(xi, aOpers[j], iop, exptval);
            else
               xi = eta;
            mXi.push_back(xi);
            //Mout << " Xi " << mXi.back() << endl;
            
            break;
         }

   for(In i=I_0; i<mDef.GetRBlock(); i++)
   {
      Mout << " Making NH Band Lanczos right starting vector " << (i+I_1) << " from operators: " <<endl;
      Mout << "      " << aOpers[i] << endl;
      DataCont v(mXi[i]);
      mV.push_back(v);
   
      // If #operators > blocksize, the algortihm puts the rest 
      // of the operator Eta's in the last starting vector.
      if(Rdiff != 0 && i == mDef.GetRBlock()-I_1)
         for(In j=i+1; j<aOpers.size() ;j++)
         {
            Mout << "      " << aOpers[j] << endl;
            mV[i].Axpy(mXi[j], C_1);
         }
      
      //cout << "Vi Norm = " << mV[i].Norm() << endl;
      mV[i].Normalize();
   }  
   for(In i=I_0; i<mDef.GetLBlock(); i++)
   {
      Mout << " Making NH Band Lanczos left starting vector " << (i+I_1) << " from operators: " <<endl;
      Mout << "      " << aOpers[i] << endl;
      
      DataCont w(mEta[i]);
      mW.push_back(w);
   
      if(Ldiff != 0 && i == mDef.GetLBlock()-I_1)
         for(In j=i+1; j<aOpers.size() ;j++)
         {
            Mout << "      " << aOpers[j] << endl;
            mW[i].Axpy(mEta[j], C_1);
         }
   
      //cout << "Wi Norm = " << mW[i].Norm() << endl;
      mW[i].Normalize();
   }


   for(In i=I_0; i<aOpers.size(); i++)
   {
      ostringstream osEta;
      osEta << mFilePrefix + "_eta" << i;
      mEta[i].NewLabel(osEta.str());
      mEta[i].ChangeStorageTo("OnDisc");
      ostringstream osXi;
      osXi << mFilePrefix + "_xi" << i;
      mXi[i].NewLabel(osXi.str());
      mXi[i].ChangeStorageTo("OnDisc");
   }
}

void NHBandLanczosChain::NHBandDiag(In aChainLen, MidasMatrix& aRightEigVecs, 
                MidasVector& aReEigVals, MidasVector& aImEigVals, 
                MidasMatrix& aLeftEigVecs) const
{
   MidasMatrix tsolve;
   tsolve.Reassign(mTmatRight, aChainLen, aChainLen);
   Timer time_diag;

   //if(mpVcc->GetMethod() == VCC_METHOD_VCC)
   //{
      vector<Nb> DumpVec;
      MidasMatrix DumpMat;
      Diag(tsolve, aRightEigVecs, aReEigVals, aImEigVals, aLeftEigVecs, "DGEEV", DumpVec, DumpMat, true, false);
      //Diag(tsolve, aRightEigVecs, aReEigVals, aImEigVals, aLeftEigVecs, "DGEEV", DumpVec, DumpMat, true, true);
   //} 
   //else if(mpVcc->GetMethod() == VCC_METHOD_VCI)
   //   Diag(tsolve, aRightEigVecs, aReEigVals, "NR_JACOBI", false, true);

   if(gTime)
   {
      string cpu_time = " CPU time used for diagonalizing T-Matrix: ";
      string wall_time = " Wall time used for diagonalizing T-Matrix: ";
      time_diag.CpuOut(Mout,cpu_time);
      time_diag.WallOut(Mout,wall_time);
   }

   ///> Write eigenvalues to file
   string filename = gAnalysisDir+"/"+mFilePrefix+"_eigvals";
   ofstream file_stream(filename.c_str(), ios_base::trunc);
   MidasStreamBuf file_buf(file_stream);
   MidasStream file(file_buf);
   midas::stream::ScopedPrecision(15, file);
   file << fixed
        << "Column 1: Eigenvalues of T (cm-1)" << endl;
   for(In i = I_0; i<aChainLen; ++i)
      file << aReEigVals[i]*C_AUTKAYS << "   + i*"<< aImEigVals[i] << endl;
}

void NHBandLanczosChain::CheckEigVecs(MidasMatrix& aReRightEigVecs, MidasMatrix& aImRightEigVecs,
            MidasMatrix& aReLeftEigVecs, MidasMatrix& aImLeftEigVecs, MidasVector& aReEigVals,
            MidasVector& aImEigVals) const
{
   Mout << " *** Testing the Right and Left eigenvectors of the T matrix *** " << endl;
   MidasVector right_residuals(aReEigVals.Size(), C_0);
   MidasVector left_residuals(aReEigVals.Size(), C_0);
   Nb right_norm=C_0;
   Nb left_norm=C_0;
   for(int i=0; i<aReEigVals.Size(); ++i)
   {
      MidasVector re_t_v(aReEigVals.Size(),C_0);
      MidasVector im_t_v(aReEigVals.Size(),C_0);
      MidasVector re_u_t(aReEigVals.Size(),C_0);
      MidasVector im_u_t(aReEigVals.Size(),C_0);
      for(int j=0; j<mTmatRight.Nrows(); ++j)
         for(int k=0; k<mTmatRight.Ncols(); ++k)
         {
            re_t_v[j] += mTmatRight[j][k]*aReRightEigVecs[k][i];
            im_t_v[j] += mTmatRight[j][k]*aImRightEigVecs[k][i];
            re_u_t[k] += mTmatRight[j][k]*aReLeftEigVecs[j][i];
            im_u_t[k] += mTmatRight[j][k]*aImLeftEigVecs[j][i];
         }
      
      for(int j=0; j<aReEigVals.Size(); ++j)
      {
         re_t_v[j] -= aReEigVals[i]*aReRightEigVecs[j][i];
         re_t_v[j] += aImEigVals[i]*aImRightEigVecs[j][i];
         im_t_v[j] -= aImEigVals[i]*aReRightEigVecs[j][i];
         im_t_v[j] -= aReEigVals[i]*aImRightEigVecs[j][i];
         
         re_u_t[j] -= aReEigVals[i]*aReLeftEigVecs[j][i];
         re_u_t[j] += aImEigVals[i]*aImLeftEigVecs[j][i];
         im_u_t[j] -= aImEigVals[i]*aReLeftEigVecs[j][i];
         im_u_t[j] -= aReEigVals[i]*aImLeftEigVecs[j][i];
      }

      for(int j=0; j<re_t_v.Size(); ++j)
      {
         right_residuals[i] += re_t_v[j]*re_t_v[j] + im_t_v[j]*im_t_v[j];
         left_residuals[i] += re_u_t[j]*re_u_t[j] + im_u_t[j]*im_u_t[j];
         right_norm += aReRightEigVecs[j][i]*aReRightEigVecs[j][i] 
            + aImRightEigVecs[j][i]*aImRightEigVecs[j][i];
         left_norm += aReLeftEigVecs[j][i]*aReLeftEigVecs[j][i] 
            + aImLeftEigVecs[j][i]*aImLeftEigVecs[j][i];
      }
      right_residuals[i] = sqrt(right_residuals[i])/sqrt(right_norm);
      left_residuals[i] = sqrt(left_residuals[i])/sqrt(left_norm);
   }

   for(int i=0; i<aReEigVals.Size(); ++i)
   {
      Mout << aReEigVals[i] << "+i*" << aImEigVals[i] << " ";
      Mout << " Right Norm = " << right_residuals[i];
      if(right_residuals[i] >= 1e-10)
         Mout << " <-- Warning! " << endl;
      else
         Mout << endl;
   }
   Mout << endl;
   for(int i=0; i<aReEigVals.Size(); ++i)
   {
      Mout << " Left Norm = " << left_residuals[i];
      if(left_residuals[i] >= 1e-10)
         Mout << " <-- Warning! " << endl;
      else
         Mout << endl;
   }
   Mout << " *** Done testing Right and Left Eigenvectors of the T matrix *** " << endl;
}

void NHBandLanczosChain::GetRightStartVector(const In aVec, DataCont& aV) const
{
   ostringstream os;
   os << mFilePrefix << "_v" << aVec;
   aV.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
   if(!aV.ChangeStorageTo("InMem", true, false, true))
      MIDASERROR("Error reading v(j) in NHBandLanczosChain::GetRightStartVector().");
}

void NHBandLanczosChain::GetLeftStartVector(const In aVec, DataCont& aW) const
{
   ostringstream os;
   os << mFilePrefix << "_w" << aVec;
   aW.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
   if(!aW.ChangeStorageTo("InMem", true, false, true))
      MIDASERROR("Error reading w(j) in NHBandLanczosChain::GetLeftStartVector().");
}

void NHBandLanczosChain::GetXiVector(const In aVec, DataCont& aXi) const
{
   ostringstream os;
   os << mFilePrefix << "_xi" << aVec;
   aXi.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
   if(!aXi.ChangeStorageTo("InMem", true, false, true))
      MIDASERROR("Error reading xi(j) in NHBandLanczosChain::GetXiVector().");
}

void NHBandLanczosChain::GetEtaVector(const In aVec, DataCont& aEta) const
{
   ostringstream os;
   os << mFilePrefix << "_eta" << aVec;
   aEta.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
   if(!aEta.ChangeStorageTo("InMem", true, false, true))
      MIDASERROR("Error reading eta(j) in NHBandLanczosChain::GetEtaVector().");
}

const vector<Nb>* NHBandLanczosChain::GetDelta() const
{
   return &mDelta;
}

MidasMatrix NHBandLanczosChain::GetV() const
{
   In   nc = mpTrf->NexciXvec()-I_1;   // Number of Response functions
   MidasMatrix v(nc,mDef.GetLength());
   Mout << " Im here " << endl;
   for(In i=I_0; i<mDef.GetLength(); ++i)
   {
      DataCont vi;
      ostringstream vos;
      vos << mFilePrefix << "_v" << i;
      vi.GetFromExistingOnDisc(nc, vos.str());
      if(! vi.ChangeStorageTo("InMem", true, false, true))
         MIDASERROR("NHBandLanczosChain::GetV(): Error reading v(i).");
      vi.SaveUponDecon(true);
      v.AssignCol((*vi.GetVector()),i);
      Mout << " i can assign col " << i << endl;
   }
   Mout << " but not here " << endl;
   return v;
}

void NHBandLanczosChain::FullOrtho(DataCont& aVvec, DataCont& aWvec, const In aJ, 
   const In pc, const In mc, const In repeat, bool& aBreakdown, const vector<Nb>& aDelta)
{
   In n = max(mc, pc);
   
   if(aJ-n > I_0)
      for(In i_repeat=I_0; i_repeat<repeat; i_repeat++)
      {
         for(In k=I_0; k<aJ-n; k++)
         {
            ostringstream vos;
            vos << mFilePrefix << "_v" << k;
            DataCont v;
            v.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, vos.str());
            if(! v.ChangeStorageTo("InMem", true, false, true))
               MIDASERROR("Error reading v(j) in orthogonalization step.");
            ostringstream wos;
            wos << mFilePrefix << "_w" << k;
            DataCont w;
            w.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, wos.str());
            if(! w.ChangeStorageTo("InMem", true, false, true))
               MIDASERROR("Error reading w(j) in orthogonalization step.");
            Nb dotV = Dot(w, aVvec)/aDelta[k];
            Nb dotW = Dot(v, aWvec)/aDelta[k];
            aVvec.Axpy(v, -dotV);
            aWvec.Axpy(w, -dotW);
         }
     }
   
   /// SKAL FIXES TIL AT TAGE DE SIDSTE VEKTORER HVIS DER HAR VAERET DEFLATION !...

   In k_init = max(I_0, In(aJ-n));
   for(In i_repeat=I_0; i_repeat<repeat; i_repeat++)
      for(In k=k_init; k<aJ; ++k)
      {
         Nb dotV = Dot(mW[k],aVvec)/aDelta[k];
         Nb dotW = Dot(mV[k],aWvec)/aDelta[k];
         aVvec.Axpy(mV[k], -dotV);
         aWvec.Axpy(mW[k], -dotW);
      }
   //cout << " Bi Norm after full Ortho = " << Dot(aVvec, aWvec) << endl;
}

bool NHBandLanczosChain::Restart(In& aJ, In& aPc, In& aMc, bool& aFullyDeflated)
{
   Mout << " Attempting to restart NHerm Band Lanczos chain calculation using old data." << endl;
   
   // Get the restart information
   midas::filesystem::Copy(gSaveDir + "/" + mFilePrefix + ".tar.gz", mFilePrefix + ".tar.gz");
   system_command_t cmd = {"tar", "xvf", mFilePrefix + ".tar.gz"};
   MIDASSYSTEM(cmd);
   
   In l, m;
   In restart;
   string restart_filename = mFilePrefix+"_restart";
   if(!InquireFile(restart_filename))
   {
      Mout << " No restart file found, so I'm evaluating the whole chain."  << endl;
      return false;
   }
   ifstream restart_file(restart_filename.c_str());   
   
   string rtmat_filename = mFilePrefix+"_rtmat";
   if(!InquireFile(rtmat_filename))
   {
      Mout << " No rtmat file found, so I'm evaluating the whole chain." << endl;  
      return false;
   }
   ifstream rtmat_file(rtmat_filename.c_str());
   
   string ltmat_filename = mFilePrefix+"_ltmat";
   if(!InquireFile(ltmat_filename))
   {
      Mout << " No ltmat file found, so I'm evaluating the whole chain." << endl;  
      return false;
   }
   ifstream ltmat_file(ltmat_filename.c_str());
   
   string delta_filename = mFilePrefix+"_delta";
   if(!InquireFile(delta_filename))
   {
      Mout << " No delta file found, so I'm evaluating the whole chain." << endl;  
      return false;
   }
   ifstream delta_file(delta_filename.c_str());

   Mout << " Restart, rtmat, and ltmat files found. Assuming all q-vector files are present. " << endl
        << " If they are not calculation will fail! " << endl;
   
   int counter = 0;
   while(!restart_file.eof() && counter < mDef.GetLength())
   {
      restart_file >> restart;
      restart_file >> aMc;
      restart_file >> aPc;
      ++counter;
   }

   if(aMc == I_0 || aPc == I_0)
      aFullyDeflated = true;

   aJ = min(mDef.GetLength(), restart + 1);

   while(!rtmat_file.eof())
   {
      rtmat_file >> l;
      rtmat_file >> m;
      if(l < mTmatRight.Nrows() && m < mTmatRight.Ncols())
      {
         rtmat_file >> mTmatRight[l][m];
      }
      else
      {
         double dump;
         rtmat_file >> dump;
      }
   }
   
   while(!ltmat_file.eof())
   {
      ltmat_file >> l;
      ltmat_file >> m;
      if(l < mTmatLeft.Nrows() && m < mTmatLeft.Ncols())
      {
         ltmat_file >> mTmatLeft[l][m];
      }
      else
      {
         double dump;
         ltmat_file >> dump;
      }
   }
   
   //
   std::string str;
   counter = 0;
   while(std::getline(delta_file, str) && counter < aJ)
   {
      mDelta.emplace_back(midas::util::FromString<double>(str));
   }
   
   ///> Load in band lanczos vectors
   for(In i=0; i<(aJ-aMc); i++)
   {
      DataCont v;
      mV.push_back(v);
      mV.back().GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, mFilePrefix+"_v"+std::to_string(i));
      mV.back().SaveUponDecon(true);
   }
   
   for(In i=(aJ-aMc); i<aJ; i++)
   {
      DataCont v;
      mV.push_back(v);
      mV.back().GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, mFilePrefix+"_v"+std::to_string(i));
      mV.back().SaveUponDecon(true);
      if(!mV.back().ChangeStorageTo("InMem",true, false, true))
      {
         Mout << " Band Lanczos restart failed to read v" << i << " file." << endl;
         mV.clear();
         aJ = 0;
         aMc = mDef.GetRBlock();
         return false;
      }
   }

   for(In i=0; i<(aJ-aPc); i++)
   {
      DataCont w;
      mW.push_back(w);
      mW.back().GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, mFilePrefix+"_w"+std::to_string(i));
      mW.back().SaveUponDecon(true);
   }
   
   for(In i=(aJ-aPc); i<aJ; i++)
   {
      DataCont w;
      mW.push_back(w);
      mW.back().GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, mFilePrefix+"_w"+std::to_string(i));
      mW.back().SaveUponDecon(true);
      if(!mW.back().ChangeStorageTo("InMem",true, false, true))
      {
         Mout << " Band Lanczos restart failed to read w" << i << " file." << endl;
         mV.clear();
         mW.clear();
         aJ = 0;
         aMc = mDef.GetRBlock();
         aPc = mDef.GetLBlock();
         return false;
      }
   }
   
   ///> Load in unused vectors
   for(In i=aJ; i<(aJ+aMc); i++)
   {
      DataCont v;
      mV.push_back(v);
      mV.back().GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, mFilePrefix+"_unused_v"+std::to_string(i));
      mV.back().SaveUponDecon(true);
      if(! mV.back().ChangeStorageTo("InMem", true, false, true))
      {
         Mout << " Band Lanczos restart failed to read unused v-vector " << i << " file." << endl;
         mV.clear();
         aJ = 0;
         aMc = mDef.GetRBlock();
         return false;
      }
   }

   for(In i=aJ; i<(aJ+aPc); i++)
   {
      DataCont w;
      mW.push_back(w);
      mW.back().GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, mFilePrefix+"_unused_w"+std::to_string(i));
      mW.back().SaveUponDecon(true);
      if(! mW.back().ChangeStorageTo("InMem", true, false, true))
      {
         Mout << " Band Lanczos restart failed to read unused w-vector " << i << " file." << endl;
         mV.clear();
         mW.clear();
         aJ = 0;
         aMc = mDef.GetRBlock();
         aPc = mDef.GetLBlock();
         return false;
      }
   }

   // LAV IF DER TJEKKER OM mQ HAR RETTE STOERRELSE

   for(In i=0; i<(mDef.GetOpers()).size(); i++)
   {
      DataCont eta;
      mEta.push_back(eta);
      mEta.back().GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, mFilePrefix+"_eta"+std::to_string(i));
      mEta.back().SaveUponDecon(true);
      DataCont xi;
      mXi.push_back(xi);
      mXi.back().GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, mFilePrefix+"_xi"+std::to_string(i));
      mXi.back().SaveUponDecon(true);
   }

   return true;
}

void NHBandLanczosChain::CleanOldSave
   (
   )  const
{
   RmFile(mFilePrefix + ".tar.gz");
}

void NHBandLanczosChain::SaveChain
   (  const std::vector<In>& RDefIndex
   ,  const std::vector<In>& LDefIndex
   )
{
   string rTfilename = mFilePrefix + "_rtmat";
   ofstream rt_matrix_file(rTfilename.c_str(), ios_base::trunc);
   rt_matrix_file << scientific << setprecision(20);
   for(In i=0; i<mTmatRight.Nrows(); i++)
   {
      In m=max(I_0, (i-mDef.GetRBlock()-I_1));
      In k=min(mTmatRight.Ncols(), (i+mDef.GetRBlock()+I_1));
      for(In j=m; j<k; j++)
         if(mTmatRight[i][j] != 0)
            rt_matrix_file << i << " " << j << " " << mTmatRight[i][j] << endl;
   }
   for(In i=0; i<RDefIndex.size(); ++i)
      for(int j=RDefIndex[i]; j<mTmatRight.Ncols(); ++j)
         rt_matrix_file << RDefIndex[i] << " " << j << " " << mTmatRight[RDefIndex[i]][j] << endl;
   
   string lTfilename = mFilePrefix + "_ltmat";
   ofstream lt_matrix_file(lTfilename.c_str(), ios_base::trunc);
   lt_matrix_file << scientific << setprecision(20);
   for(In i=0; i<mTmatLeft.Nrows(); i++)
   {
      In m=max(I_0, (i-mDef.GetLBlock()-I_1));
      In k=min(mTmatLeft.Ncols(), (i+mDef.GetLBlock()+I_1));
      for(In j=m; j<k; j++)
         if(mTmatLeft[i][j] !=0)
            lt_matrix_file << i << " " << j << " " << mTmatLeft[i][j] << endl;
   }
   for(In i=0; i<LDefIndex.size(); ++i)
      for(int j=LDefIndex[i]; j<mTmatLeft.Ncols(); ++j)
         lt_matrix_file << LDefIndex[i] << " " << j << " " << mTmatLeft[LDefIndex[i]][j] << endl;
   
   string delta_filename = mFilePrefix + "_delta";
   ofstream delta_file(delta_filename.c_str(), ios_base::trunc);
   delta_file << scientific << setprecision(20);
   for(In i=0; i<mDelta.size(); ++i)
      delta_file << mDelta[i] << endl;
   
   Mout << " Saving chain: " << mFilePrefix << " for restart." << endl;
   
   // Create tar command.
   system_command_t cmd = {"tar", "-cvf", mFilePrefix + ".tar.gz"};
   auto match = midas::filesystem::SearchDir(midas::os::Getcwd(), std::regex(mFilePrefix));
   for(const auto& f : match)
   {
      cmd.emplace_back(f);
   }
   
   // Run tar command
   MIDASSYSTEM(cmd);
   
   // Copy back tar file
   midas::filesystem::Copy(mFilePrefix + ".tar.gz", gSaveDir + "/" + mFilePrefix + ".tar.gz");
   
   Mout << "Savedir: " << gSaveDir << endl;
}



/*bool NHBandLanczosChain::TestConvergence(const In ItNum, const In pc ,const vector<In>& DefIndex)
{
   // Dette stykke kode kan i dens nuværende form,
   // kun bruges til at give en nogenlunde ide om egenværdiernes konvergens.
   
   Nb init_C_0 = C_0;
   // string How("NR_JACOBI");
   MidasMatrix TestT(ItNum, init_C_0); // Flyt evt ned under if-saetningen
   MidasMatrix TestEigenVecs(ItNum, init_C_0); // -||-
   MidasVector TestEigenVals(ItNum, init_C_0); // -||-
   bool notconverged=true;
   In NumEigenTest = ItNum - mDef.GetBlock();
   Nb res=I_0;
   Nb sum=I_0;

   Mout << " I will now test convergence " << endl;

   if((pc == 0 || ItNum%10==I_0) && NumEigenTest > mDef.GetBlock())
   {
      for(In i=I_0; i<ItNum; i++) // FIX TIL KUN AT TAGE DE INDGANGE MED NOGET I...
      {
         for(In j=I_0; j<ItNum; j++)
         {
            TestT[i][j] = mT[i][j];
         }
      }

      Diag(TestT, TestEigenVecs, TestEigenVals, mDef.GetDiagMethod(), false, true);
      
      for(In i=I_0; i<NumEigenTest; i++)
      {
         res = I_0;
         sum = I_0;

         //Calculate residual for eigenpair i
         for(In j=(NumEigenTest + 1); j<ItNum; j++) // From j+1 to j+p
         {
            for(In k=j-pc; k<NumEigenTest ;k++)
            {
               sum = sum + mT[j][k]*TestEigenVecs[k][i];
            }
            // Account for deflation errors...
            for(In k=I_0; k<DefIndex.size(); k++)
            {
              sum = sum + mT[j][DefIndex[k]]*TestEigenVecs[DefIndex[k]][i];
            }

            res = res + sum*sum;
         }

         res = sqrt(res);
         if(res <= mDef.GetConv())
         {
            Mout << i <<": Eigenvalue " << TestEigenVals[i] 
            << " is CONVERGED with a residual of " << res << "." << endl;
         }
         else
         {
            Mout << i << ": Eigenvalue " << TestEigenVals[i] 
            << " is NOT CONVERGED with a residual of " << res << "." << endl;  
         }
      }

   }

   return notconverged;
}*/

void NHBandLanczosChain::TransformRight(DataCont& avj, const In anc, const In aJ, const In aType)
{
   Nb dummy=C_0;
   DataCont Avj(anc, C_0,"InMem", "temp" , false);
   
   if(aType == VCC_METHOD_VCC)
      mpTrf->SetType(TRANSFORMER::VCCJAC);
   
   mpTrf->Transform(avj, Avj, I_1, I_0, dummy);
   mV.push_back(Avj);
   mV.back().NewLabel(mFilePrefix+"_unused_v" + std::to_string(aJ+mDef.GetRBlock()));
}

void NHBandLanczosChain::TransformLeft(DataCont& awj, const In anc, const In aJ, const In aType)
{
   Nb dummy=C_0;
   DataCont Awj(anc,C_0,"InMem", "temp" , false);
   
   if(aType == VCC_METHOD_VCC)
      mpTrf->SetType(TRANSFORMER::LVCCJAC);

   mpTrf->Transform(awj, Awj, I_1, I_0, dummy);
   mW.push_back(Awj);
   mW.back().NewLabel(mFilePrefix+"_unused_w" + std::to_string(aJ+mDef.GetLBlock()));
}

void NHBandLanczosChain::TransformStoX(const MidasVector& aEigVec, DataCont& aX) const
{
   aX.SetNewSize(mpTrf->NexciXvec() -I_1);
   aX.Zero();
   aX.ChangeStorageTo("InMem");

   for(In i=I_0; i<aEigVec.Size(); ++i)
   {
      ostringstream os;
      os <<mFilePrefix << "_v" << i;
      DataCont v;
      v.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
      if(! v.ChangeStorageTo("InMem", true, false, true))
      MIDASERROR("BandLanczosChain::TransformStoX(): Error reading v(j).");
      aX.Axpy(v, aEigVec[i]);
   }
}

void NHBandLanczosChain::Evaluate()
{
   Mout << " Evaluating Non-Hermitian Band Lanczos chain with chain definition: " << endl  << mDef << endl;
   
   In            nc = mpTrf->NexciXvec()-I_1;   // Number of Response functions
   vector<In>  RDefIndex;                     // Holds index for the deflated right vectors.
   vector<In>  LDefIndex;                     // Holds index for the deflated left vectors.
   bool          notconverged=true;            // Checks for convergence.
   bool         fullydeflated=false;            // Checks for full deflation
   bool        RDef;
   bool        LDef;
   bool        breakdown = false;
   In            mc=mDef.GetRBlock();
   In            pc=mDef.GetLBlock();
   Nb            vjNorm=C_0;
   Nb            wjNorm=C_0;
   Nb          CheckDef;
   In            j=I_0;                        // Iteration counter.
   In          init_chainidx=I_0;
   Timer       time_band;                    // Timer.
   In          repeat = I_0;
         
   string vjnormname=mFilePrefix+"_vjnorm";
   ofstream vjnorm_file(vjnormname.c_str());
   vjnorm_file << scientific << setprecision(20);
   string wjnormname=mFilePrefix+"_wjnorm";
   ofstream wjnorm_file(wjnormname.c_str());
   wjnorm_file << scientific << setprecision(20);
   
   midas::stream::ScopedPrecision(20, Mout);
   ///> Reserve the max space needed in vectors mV, mW, and mDelta.
   mV.reserve(mDef.GetLength() + mc);
   mW.reserve(mDef.GetLength() + pc);
   mDelta.reserve(mDef.GetLength());
   
   if(mpVcc->pVccCalcDef()->RspRestart() && Restart(j, mc, pc, fullydeflated))
   {
      // Restart if keyword is set and restart files are present
      Mout  << " Band Lanczos response chain: Restarted using old data." << endl
            << " Current chain length: " << j << endl;
   }
   else
   {
      // else make #mDef.GetBlock() start vectors.
      MakeStartVectors(nc, mDef.GetOpers());
   }

   //MakeStartVectors(nc, mDef.GetOpers(),false,false,mDef.GetSameStartVecs(),mDef.GetExtraRandom());
   //MakeStartVectors(nc);
   
   string restart_filename = mFilePrefix+"_restart";
   ofstream restart_file(restart_filename.c_str(), ios::app);   

   std::string transformdump_filename = mFilePrefix + "_transformdump";
   std::ofstream transformdump_file(transformdump_filename);
   transformdump_file << std::scientific << std::setprecision(20) << std::showpos;

   /*MidasMatrix W;                            // Skal en if omkring
   W.SetNewSize(mDef.GetLength(), false, true);
   In OrthoCount = 0;
   MidasMatrix OrthoIndex;*/

   if(mpVcc->GetMethod() == VCC_METHOD_VCI)
      mpTrf->SetType(TRANSFORMER::VCIRSP);

   // Calculate BandLanczos vectors
   while(notconverged && mc>I_0 && pc>I_0 && j<mDef.GetLength())
   {
      RDef=true;
      LDef=true;
      
      while(RDef || LDef)
      {
         switch(mDef.GetOrtho())
         {
            case LANCZOS_ORTHO_NONE:
            {
               if(j==0)
                  Mout << "Not doing any reorthogonalizations." << endl;
               break;
            }
            case LANCZOS_ORTHO_FULL:
            {
               if (j==0)
                  Mout << "Using Full Orthogonalization scheme." << endl;
               FullOrtho(mV[j], mW[j], j, pc, mc, I_1, breakdown, mDelta);
               break;
            }
            /*case LANCZOS_ORTHO_PERIODIC:
            {
               if (j==0)
                  Mout << "Using experimental Periodic Orthogonalization scheme." << endl;
               Periodic_Ortho(mQ[j], W, j, pc, qjNorm, OrthoCount);
               break;
            }
            case LANCZOS_ORTHO_PARTIAL:
            {
               if(j==0)
                  Mout << "Using experimental Partial Orthogonalization scheme." << endl;
               Partial_Ortho(mQ[j], W, j, pc, qjNorm, OrthoCount, OrthoIndex);
               break;
            }*/
            default:
            {
               ///> Output for developers ;)
               MIDASERROR(" Error in orthogonalization scheme switch in NHBandLanczosChain::Evaluate(). ");
            }
         }
         
         if(RDef)
            vjNorm = mV[j].Norm();
         if(LDef)
            wjNorm = mW[j].Norm();

         if(mDef.GetDeflate())
         {
            ///> Check for right deflation
            CheckDef = mV[j].Norm();
            if(CheckDef < mDef.GetDtol())
            {
               ///> Deflation
               Mout  << " A right side deflation occured on iteration " << j << "," << endl
                     << " as norm of vector was = " << CheckDef << "." << endl
                     << " I'm deleting the vector and restarting the iteration with the next one." << endl;
               if(j-mc >= I_0)
               {
                  RDefIndex.push_back(j - mc);
               }
            
               ///> Change storage to disc for vector j-pc, which isn't used in further iterations.
               if(j-mc >= I_0)
               {
                  mV[j-mc].ChangeStorageTo("OnDisc");
               }
               mc--;
            
               ///> Check to se if all vectors are deflated. If so, then STOP.
               if(mc == I_0)
               {
                  //Mout << " Fully Deflated " << endl;
                  fullydeflated = true;
                  RDef=false;
                  LDef=false;
                  // Pop_back to deleted deflated vector ?
               }
               else
               {
                  // Delete deflated vector and move the rest one index back.
                  for(In k=j; k<j+mc; k++)
                     mV[k]=mV[k+I_1];
               
                  mV.pop_back(); // Delete the dulicated vector k+pc
                  breakdown = false;
                  continue; // skal maaske overvejes lidt mere...
               }
            }
            else
               RDef = false;
      
            ///> Check for left deflation
            CheckDef = mW[j].Norm();
            if(CheckDef < mDef.GetDtol())
            {
               // Deflation
               Mout  << " A left side deflation occured on iteration " << j << "," << endl
                     << " as norm of vector was = " << CheckDef << "." << endl
                     << " I'm deleting the vector and restarting the iteration with the next one." << endl;
               if(j-pc >= I_0)
               {
                  LDefIndex.push_back(j - pc);
               }
            
               // Change storage to disc for vector j-pc, which isn't used in further iterations.
               Mout << " Deflation at: " << j << endl;
               Mout << " j-pc: " << j-pc << endl;
               if(j-pc>=I_0)
               {
                  mW[j-pc].ChangeStorageTo("OnDisc");
               }
               pc--;
            
               // Check to se if all vectors are deflated. If so, then STOP.
               if(pc == I_0)
               {
                  //Mout << " Fully Deflated " << endl;
                  fullydeflated = true;
                  RDef=false;
                  LDef=false;
               }
               else
               {
                  // Delete deflated vector and move the rest one index back.
                  for(In k=j; k<j+pc; k++)
                     mW[k]=mW[k+I_1];
               
                  mW.pop_back(); // Delete the dulicated vector k+pc
                  breakdown = false;
               }
            }
            else
               LDef = false;
         }
         else
         {
            RDef=false;
            LDef=false;
         }
      }
      
      if(mc!=I_0 && pc!=I_0)
      {
         // No deflation.
         // Give a label to vector j.
         ostringstream osV;
         osV << mFilePrefix+"_v" << j;
         (mV[j]).NewLabel(osV.str());
         (mV[j]).SaveUponDecon(true);
         
         ostringstream osW;
         osW << mFilePrefix+"_w" << j;
         (mW[j]).NewLabel(osW.str());
         (mW[j]).SaveUponDecon(true);
         
         if(j-mc>=I_0)
            mTmatRight[j][j-mc] = vjNorm;
         if(j-pc>=I_0)
            mTmatLeft[j][j-pc] = wjNorm;

         vjnorm_file << mV[j].Norm() << endl;
         wjnorm_file << mW[j].Norm() << endl;

         mV[j].Normalize();
         mW[j].Normalize();

         mDelta.push_back(Dot(mV[j],mW[j]));
         // check for breakdown
         if(fabs(mDelta[j]) <= C_NB_EPSILON || breakdown)
         {
            Mout << " Omagad! We have a breakdown... BREAK IT DOWN!" << endl;
            breakdown = true;
            break;
         }
         
         TransformRight(mV[j], nc, j, mpVcc->GetMethod());
         In k_0=max(I_0,j-pc);
         for(In k=k_0; k<j; ++k)
         {
            mTmatRight[k][j] = Dot(mW[k],mV[j+mc])/mDelta[k];
            mV[j+mc].Axpy(mV[k], -mTmatRight[k][j]);
         }
         for(In k=I_0; k<RDefIndex.size(); ++k)
         {
            mTmatRight[RDefIndex[k]][j] = Dot(mW[RDefIndex[k]],mV[j+mc])/mDelta[RDefIndex[k]];
            mV[j+mc].Axpy(mV[LDefIndex[k]], -mTmatRight[LDefIndex[k]][j]);
         }
         
         TransformLeft(mW[j], nc, j, mpVcc->GetMethod());
         k_0=max(I_0,j-mc);
         for(In k=k_0; k<j; ++k)
         {
            mTmatLeft[k][j] = Dot(mW[j+pc],mV[k])/mDelta[k];
            mW[j+pc].Axpy(mW[k], -mTmatLeft[k][j]); 
         }
         for(In k=I_0; k<LDefIndex.size(); ++k)
         {
            mTmatLeft[LDefIndex[k]][j] = Dot(mW[j+pc],mV[LDefIndex[k]])/mDelta[LDefIndex[k]];
            mW[j+pc].Axpy(mW[LDefIndex[k]], -mTmatLeft[LDefIndex[k]][j]);
         }
         
         for(In k=j+I_1; k<j+mc+I_1; ++k)
         {
            Nb dot = Dot(mW[j],mV[k])/mDelta[j];
            if(k-mc >= I_0)
               mTmatRight[j][k-mc] = dot;
            
            mV[k].Axpy(mV[j], -dot);
         }
         for(In k=j+I_1; k<j+pc+I_1; ++k)
         {
            Nb dot = Dot(mW[k],mV[j])/mDelta[j];
            if(k-pc >=I_0)
               mTmatLeft[j][k-pc] = dot;
            
            mW[k].Axpy(mW[j], -dot);
         }
         
         for(In k=I_0; k<LDefIndex.size(); ++k)
         {
            mTmatRight[j][LDefIndex[k]] = mTmatLeft[LDefIndex[k]][j]*mDelta[LDefIndex[k]]/mDelta[j];
         }
                 
         ///> Change storage to disc for vector j-pc, which isn't used in further iterations.
         if(j-mc>=I_0)
            mV[j-mc].ChangeStorageTo("OnDisc");
         if(j-pc>=I_0)
            mW[j-pc].ChangeStorageTo("OnDisc");

         ///> Print Progress
         if (j == init_chainidx)
         {
            Mout << " Required iterations: " << mDef.GetLength()-init_chainidx << endl
            //<< " First iteration: " << time(NULL)-t0 << " s"
            << endl << " Progress (one . is 10 iterations):" << endl << " ";
         }
         else if ( (j-init_chainidx + 1)%10 == I_0)
            Mout << "." << std::flush;

         restart_file << j << " " << mc << " " << pc << endl;
         
         if(mDef.GetSaveInterval() && ( (j + 1) % mDef.GetSaveInterval() == 0) )
         {
            CleanOldSave();

            for(In i = j - mc; i < j + mc + 1; ++i)
            {
               if(i >= I_0)
               {
                  mV[i].DumpToDisc();
               }
            }
            for(In i = j - pc; i < j + pc + 1; ++i)
            {
               if(i >= I_0)
               {
                  mW[i].DumpToDisc();
               }
            }

            SaveChain(RDefIndex, LDefIndex);
         }
         
         j++;
      
      }
   }

   Mout << endl;
   
   //restart_file << j << " " << mc << " " << pc << endl;
   restart_file.close();
   
   ///> Midas output.   
   Mout << "Stopped after " << j << " iterations." << endl;
   if(fullydeflated)
   {
      Mout << "The Krylov Space is fully deflated." << endl;
   }
   else
   {
      Mout << "The Krylov Space is NOT fully deflated!" << endl;
   }
   
   ///> Make V^(j) and W^(j)
   for(In i=j-mc; i<j+mc; i++)
   {
      if(i >= I_0)
      {   
         mV[i].SaveUponDecon(true);
         mV[i].ChangeStorageTo("OnDisc");
      }
   }
   mV.resize(j);
   for(In i=j-pc; i<j+pc; i++)
   {
      if(i >= I_0)
      {
         mW[i].SaveUponDecon(true);
         mW[i].ChangeStorageTo("OnDisc");
      }
   }
   mW.resize(j);

   // Debug and benchmark outputs
   if(gDebug || mDef.GetTest())
   {
      Mout << " Making NH-BandLanczos test files. " << endl;
      CalcOrtho();
      OutputRT(mDelta);
      TestRT(j,nc,mDelta);
      TestResidual(j,nc);
      TestFullSpace(j);
   }

   //Save T-matrix to disc for Restart.
   //T-matrix is stored in a sparse form.
   // MOVE TO SAVECHAIN FUNCTION!
   //string rTfilename = mFilePrefix + "_rtmat";
   //ofstream rt_matrix_file(rTfilename.c_str(), ios_base::trunc);
   //rt_matrix_file << scientific << setprecision(20);
   //for(In i=0; i<mTmatRight.Nrows(); i++)
   //{
   //   In m=max(I_0, (i-mDef.GetRBlock()-I_1));
   //   In k=min(mTmatRight.Ncols(), (i+mDef.GetRBlock()+I_1));
   //   for(In j=m; j<k; j++)
   //      if(mTmatRight[i][j] !=0)
   //         rt_matrix_file << i << " " << j << " " << mTmatRight[i][j] << endl;
   //}
   //for(In i=0; i<RDefIndex.size(); ++i)
   //   for(int j=RDefIndex[i]; j<mTmatRight.Ncols(); ++j)
   //      rt_matrix_file << RDefIndex[i] << " " << j << " " << mTmatRight[RDefIndex[i]][j] << endl;
   //
   //string lTfilename = mFilePrefix + "_ltmat";
   //ofstream lt_matrix_file(lTfilename.c_str(), ios_base::trunc);
   //lt_matrix_file << scientific << setprecision(20);
   //for(In i=0; i<mTmatLeft.Nrows(); i++)
   //{
   //   In m=max(I_0, (i-mDef.GetLBlock()-I_1));
   //   In k=min(mTmatLeft.Ncols(), (i+mDef.GetLBlock()+I_1));
   //   for(In j=m; j<k; j++)
   //      if(mTmatLeft[i][j] !=0)
   //         lt_matrix_file << i << " " << j << " " << mTmatLeft[i][j] << endl;
   //}
   //for(In i=0; i<LDefIndex.size(); ++i)
   //   for(int j=LDefIndex[i]; j<mTmatLeft.Ncols(); ++j)
   //      lt_matrix_file << LDefIndex[i] << " " << j << " " << mTmatLeft[LDefIndex[i]][j] << endl;
   //
   //string delta_filename = mFilePrefix + "_delta";
   //ofstream delta_file(delta_filename.c_str(), ios_base::trunc);
   //delta_file << scientific << setprecision(20);
   //for(In i=0; i<mDelta.size(); ++i)
   //   delta_file << mDelta[i] << endl;

   // save chain if needed
   if(mDef.GetSave())
   {
      CleanOldSave();
      SaveChain(RDefIndex, LDefIndex);
   }
   
   if(gTime)
   {
      string cpu_time = " CPU time used in evaluation of NHerm Band Lanczos chain ";
      string wall_time = " Wall time used in evaluation of NHerm Band Lanczos chain ";
      time_band.CpuOut(Mout,cpu_time);
      time_band.WallOut(Mout,wall_time);
   }

   Mout << "I have now evaluated the non-hermitian band Lanczos chain. Thanks for swinging by :)" << endl;
}

void NHBandLanczosChain::Diagonalize(MidasMatrix& aMat, MidasMatrix& aRightEigVecs, 
                MidasMatrix& aLeftEigVecs, MidasVector& aReEigVals, MidasVector& aImEigVals)
{
   if(mpVcc->GetMethod() == VCC_METHOD_VCC)
   {
      vector<Nb> DumpVec;
      MidasMatrix DumpMat;
      Diag(aMat, aRightEigVecs, aReEigVals, aImEigVals, aLeftEigVecs, "DGEEV", DumpVec, DumpMat, true, true);
   } 
   else if(mpVcc->GetMethod() == VCC_METHOD_VCI)
      Diag(aMat, aRightEigVecs, aReEigVals, "NR_JACOBI", false, true);
}


/* *
* Test Functions !
* */
void NHBandLanczosChain::CalcOrtho()
{
   Mout << " I R Calculating V and W matrix bi-orthonormality " << endl;
   In nc = mpTrf->NexciXvec() - I_1;
   // Setup file containing dot products..
   string filename = mFilePrefix + "_biorthogonality";
   ofstream ortho_file(filename.c_str(), ios_base::trunc);
   ortho_file << scientific << setprecision(20);
   MidasMatrix ortho_mat(In(mV.size()), In(mW.size()));
   
   for (In i=I_0; i<mV.size(); i++)
   {
      DataCont vi;
      ostringstream vos;
      vos << mFilePrefix << "_v" << i;
      vi.GetFromExistingOnDisc(nc, vos.str());
      if(! vi.ChangeStorageTo("InMem", true, false, true))
         MIDASERROR("NHBandLanczosChain::CalcOrtho(): Error reading v(i).");
      vi.SaveUponDecon(true);
      for (In j=I_0; j<mW.size(); j++)
      {
         DataCont wj;
         ostringstream wos;
         wos << mFilePrefix << "_w" << j;
         wj.GetFromExistingOnDisc(nc, wos.str());
         if(! wj.ChangeStorageTo("InMem", true, false, true))
            MIDASERROR("NHBandLanczosChain::CalcOrtho(): Error reading w(j).");
         wj.SaveUponDecon(true);
         ortho_mat[i][j] = Dot(vi,wj);
      }
   }

   ortho_file << ortho_mat << endl;
}

void NHBandLanczosChain::OutputRT(const vector<Nb>& aDelta)
{
   string filename = mFilePrefix + "_Tmat";
   ofstream tmat_file(filename.c_str(), ios_base::trunc);
   tmat_file << scientific << setprecision(20);

   tmat_file << mTmatRight << endl;
   
   for(In i=0; i<mTmatLeft.Nrows(); ++i)
      for(In j=0; j<mTmatLeft.Ncols(); ++j)
         mTmatLeft[i][j] = mTmatLeft[i][j]*(aDelta[i]/aDelta[j]);

   mTmatLeft.Transpose();
   string filename_lt = mFilePrefix + "_LTmat";
   ofstream ltmat_file(filename_lt.c_str(), ios_base::trunc);
   ltmat_file << scientific << setprecision(20);
   
   ltmat_file << mTmatLeft << endl;
}

/**
 *
 **/
void NHBandLanczosChain::PrintTmatrices
   (  In aIter
   )  const
{
   std::ofstream rtmat_file(mFilePrefix + "_RTmatprint_" + std::to_string(aIter));
   rtmat_file << std::scientific << std::setprecision(20) << mTmatRight << std::endl;
   
   std::ofstream ltmat_file(mFilePrefix + "_LTmatprint_" + std::to_string(aIter));
   ltmat_file << std::scientific << std::setprecision(20) << mTmatLeft << std::endl;
}

/**
 *
 **/
void NHBandLanczosChain::PrintLanczosVectors
   (  In aIter
   )
{
   // print V
   std::ofstream rvvec_file(mFilePrefix + "_Vvecprint_" + std::to_string(aIter));
   rvvec_file << std::scientific << std::setprecision(20) << std::showpos;
   for(In i = 0; i < mV.size(); ++i)
   {
      bool ondisc = false;
      if( (ondisc = mV[i].OnDisc()) )
      {
         mV[i].ChangeStorageTo("InMem", true, false, true);
      }
      auto vec = mV[i].GetVector();
      rvvec_file << (*vec);

      if(ondisc)
      {
         mV[i].ChangeStorageTo("OnDisc");
      }
   }
   
   // print W
   std::ofstream rwvec_file(mFilePrefix + "_Wvecprint_" + std::to_string(aIter));
   rwvec_file << std::scientific << std::setprecision(20) << std::showpos;
   for(In i = 0; i < mW.size(); ++i)
   {
      bool ondisc = false;
      if( (ondisc = mW[i].OnDisc()) )
      {
         mW[i].ChangeStorageTo("InMem", true, false, true);
      }
      auto vec = mW[i].GetVector();
      rwvec_file << (*vec);

      if(ondisc)
      {
         mW[i].ChangeStorageTo("OnDisc");
      }
   }
}

/**
 *
 **/
void NHBandLanczosChain::PrintDelta
   (  In aIter
   )  const
{
   std::ofstream deltafile(mFilePrefix + "_delta_" + std::to_string(aIter));
   deltafile << std::scientific << std::setprecision(20) << std::showpos << mDelta;
}


void NHBandLanczosChain::TestResidual(const In ItNum, const In anc)
{
   Mout << " I am testing NH-BandLanczos residuals. " << endl;

   Nb init_C_0=C_0;
   MidasMatrix TestT(ItNum, init_C_0);
   MidasMatrix RightEigVecs(ItNum, init_C_0);
   MidasMatrix LeftEigVecs(ItNum, init_C_0);
   MidasVector ReEigVals(ItNum, init_C_0);
   MidasVector ImEigVals(ItNum, init_C_0);
   MidasVector RightResiduals(ItNum, init_C_0);
   MidasVector LeftResiduals(ItNum, init_C_0);
   MidasMatrix TestV(anc, ItNum);
   MidasMatrix TestW(anc, ItNum);
   MidasMatrix ReX(anc, ItNum, init_C_0);
   MidasMatrix ImX(anc, ItNum, init_C_0);
   MidasMatrix ReY(anc, ItNum, init_C_0);
   MidasMatrix ImY(anc, ItNum, init_C_0);

   for(In i = I_0; i<ItNum; ++i)
      for(In j = I_0; j<ItNum; ++j)
         TestT[i][j] = mTmatRight[i][j];

   Diagonalize(TestT, RightEigVecs, LeftEigVecs, ReEigVals, ImEigVals);
   
   string filename_eig = mFilePrefix+"_eigvals";
   ofstream file_stream_eig(filename_eig.c_str(), ios_base::trunc);
   MidasStreamBuf file_buf_eig(file_stream_eig);
   MidasStream file_eig(file_buf_eig);
   file_eig << fixed << setprecision(15)
        << "Column 1: Eigenvalues of T (cm-1)" << endl;
   for(In i = I_0; i<mDef.GetLength(); ++i)
      file_eig << ReEigVals[i]*C_AUTKAYS << "   + i*"<< ImEigVals[i] << endl;
         
   for(In i = I_0; i<ItNum; ++i)
   {
      ostringstream vos;
      vos << mFilePrefix << "_v" << i;
      DataCont v;
      v.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, vos.str());
      if(! v.ChangeStorageTo("InMem", true, false, true))
         MIDASERROR("Error reading v(j) in NHBandLanczosChain::TestResidual()");

      TestV.AssignCol(*(v.GetVector()),i);

      ostringstream wos;
      wos << mFilePrefix << "_w" << i;
      DataCont w;
      w.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, wos.str());
      if(! w.ChangeStorageTo("InMem", true, false, true))
         MIDASERROR("Error reading w(j) in NHBandLanczosChain::TestResidual()");

      TestW.AssignCol(*(w.GetVector()),i);
   }
   
   for(In i = I_0; i<ReEigVals.Size(); ++i)
      Mout << "Eigval " << i << " =   " << ReEigVals[i] << " +  i*" << ImEigVals[i] << endl;

   for(In i = I_0; i<ReEigVals.Size(); ++i)
   {
      if(ImEigVals[i] == C_0)
      {
         for(In j = I_0; j<TestV.Nrows(); ++j)
            for(In k = I_0; k<TestV.Ncols(); ++k)
            {
               ReX[j][i] += TestV[j][k]*RightEigVecs[k][i];
            }
      }
      else
      {
         for(In j = I_0; j<TestV.Nrows(); ++j)
            for(In k = I_0; k<TestV.Ncols(); ++k)
            {
               ReX[j][i] += TestV[j][k]*RightEigVecs[k][i];
               ReX[j][i+1] += TestV[j][k]*RightEigVecs[k][i];
            }
         for(In j = I_0; j<TestV.Nrows(); ++j)
            for(In k = I_0; k<TestV.Ncols(); ++k)
            {
               ImX[j][i] += TestV[j][k]*RightEigVecs[k][i+1];
               ImX[j][i+1] += -TestV[j][k]*RightEigVecs[k][i+1];
            }
         ++i;
      }
   }

   if(mpVcc->GetMethod() == VCC_METHOD_VCC)
   {
      mpTrf->SetType(TRANSFORMER::VCCJAC);
      Mout << " Method is VCC " << endl;
   }
   if(mpVcc->GetMethod() == VCC_METHOD_VCI)
   {
      mpTrf->SetType(TRANSFORMER::VCIRSP);
      Mout << " Method is VCI " << endl;
   }
   
   for(In i= I_0; i<ItNum; ++i)
   {
      DataCont AReX(anc, C_0,"InMem", "AReX" , false);
      MidasVector ReDummyVec(anc);
      ReX.GetCol(ReDummyVec, i);
      DataCont ReEigenVec(ReDummyVec,"InMem", "ReEigVec");
      
      DataCont AImX(anc, C_0,"InMem", "AImX" , false);
      MidasVector ImDummyVec(anc);
      ImX.GetCol(ImDummyVec, i);
      DataCont ImEigenVec(ImDummyVec, "InMem", "ImEigVec");
      
      Nb Dummy=C_0;
      mpTrf->Transform(ReEigenVec, AReX, I_1, I_0, Dummy);
      Dummy = C_0;
      mpTrf->Transform(ImEigenVec, AImX, I_1, I_0, Dummy);

      AReX.Axpy(ReEigenVec, -ReEigVals[i]);
      AReX.Axpy(ImEigenVec, ImEigVals[i]);
      AImX.Axpy(ReEigenVec, -ImEigVals[i]);
      AImX.Axpy(ImEigenVec, -ReEigVals[i]);

      MidasVector* AReXVec=AReX.GetVector();
      MidasVector* AImXVec=AImX.GetVector();

      for(In j=I_0; j<AReXVec->Size(); ++j)
         RightResiduals[i] += (*AReXVec)[j]*(*AReXVec)[j] + (*AImXVec)[j]*(*AImXVec)[j];
      
      Nb eigvecnorm = sqrt(Dot(ReEigenVec,ReEigenVec) + Dot(ImEigenVec,ImEigenVec));
      RightResiduals[i] = sqrt(RightResiduals[i])/eigvecnorm;
      Mout << " Norm " << RightResiduals[i] << endl;
   }

   string filename = mFilePrefix + "_residuals";
   ofstream residual_file(filename.c_str(), ios_base::trunc);
   residual_file << scientific << setprecision(20);

   for(In i=I_0; i<ItNum; ++i)
      residual_file << ReEigVals[i] << " +i*" << ImEigVals[i] << "  " << RightResiduals[i] << endl;
   
   DataCont vcc_solver_eigvals;
   In vcc_solver_neigvals=mpVcc->pVccCalcDef()->GetRspNeig();
   Mout << " Neigvals = " << vcc_solver_neigvals << endl;
   ostringstream vcc_solver_string;
   vcc_solver_string << mpVcc->pVccCalcDef()->Name()+"_rsp_eigval";
   Mout << " string = " << vcc_solver_string.str() << endl;
   vcc_solver_eigvals.GetFromExistingOnDisc(vcc_solver_neigvals, vcc_solver_string.str());
   if(! vcc_solver_eigvals.ChangeStorageTo("InMem", true, false, true))
      MIDASERROR("NHBandLanczosChain::TestResiduals(): Error reading vcc_solver_eigvals.");
   vcc_solver_eigvals.SaveUponDecon(true);
   
   In i=I_0,j=I_0;
   Nb vcc_solver_eigval;
   Nb eigval_residual;
   while(i<vcc_solver_neigvals && j<mDef.GetLength())
   {
      vcc_solver_eigvals.DataIo(IO_GET,i,vcc_solver_eigval);
      eigval_residual = (vcc_solver_eigval-ReEigVals[j])/ReEigVals[j];
      Mout << "oldsolver-bandlanczos diff " << j << " = " << eigval_residual << endl;
      ++i;
      ++j;
   }

   /*for(In i = I_0; i<ItNum; ++i)
      for(In j = I_0; j<ItNum; ++j)
         TestT[i][j] = mTmatRight[j][i];*/
   
   /*for(In i = I_0; i<ReEigVals.Size(); ++i)
   {
      if(ImEigVals[i] == C_0)
      {
         for(In j = I_0; j<TestW.Nrows(); ++j)
            for(In k = I_0; k<TestW.Ncols(); ++k)
            {
               ReY[j][i] += TestW[j][k]*LeftEigVecs[k][i];
            }
      }
      else
      {
         for(In j = I_0; j<TestW.Nrows(); ++j)
            for(In k = I_0; k<TestW.Ncols(); ++k)
            {
               ReY[j][i] += TestW[j][k]*LeftEigVecs[k][i];
               ReY[j][i+1] += TestW[j][k]*LeftEigVecs[k][i];
            }
         for(In j = I_0; j<TestW.Nrows(); ++j)
            for(In k = I_0; k<TestW.Ncols(); ++k)
            {
               ImY[j][i] += TestW[j][k]*LeftEigVecs[k][i+1];
               ImY[j][i+1] += -TestW[j][k]*LeftEigVecs[k][i+1];
            }
         ++i;
      }
   }

   if(mpVcc->GetMethod() == VCC_METHOD_VCC)
   {
      mpTrf->SetType(TRANSFORMER::LVCCJAC);
      Mout << " Method is LVCC " << endl;
   }
   if(mpVcc->GetMethod() == VCC_METHOD_VCI)
   {
      mpTrf->SetType(TRANSFORMER::VCIRSP);
      Mout << " Method is VCI " << endl;
   }
   
   for(In i= I_0; i<ItNum; ++i)
   {
      DataCont AReY(anc, C_0,"InMem", "AReY" , false);
      MidasVector ReDummyVec(anc);
      ReY.GetCol(ReDummyVec, i);
      DataCont ReEigenVec(ReDummyVec,"InMem", "ReEigVec");
      
      DataCont AImY(anc, C_0,"InMem", "AImY" , false);
      MidasVector ImDummyVec(anc);
      ImY.GetCol(ImDummyVec, i);
      DataCont ImEigenVec(ImDummyVec, "InMem", "ImEigVec");
      
      Nb Dummy=C_0;
      mpTrf->Transform(ReEigenVec, AReY, I_1, I_0, Dummy);
      Dummy = C_0;
      mpTrf->Transform(ImEigenVec, AImY, I_1, I_0, Dummy);

      AReY.Axpy(ReEigenVec, -ReEigVals[i]);
      AReY.Axpy(ImEigenVec, ImEigVals[i]);
      AImY.Axpy(ReEigenVec, -ImEigVals[i]);
      AImY.Axpy(ImEigenVec, -ReEigVals[i]);

      MidasVector* AReYVec=AReY.GetVector();
      MidasVector* AImYVec=AImY.GetVector();

      for(In j=I_0; j<AReYVec->Size(); ++j)
         LeftResiduals[i] += (*AReYVec)[j]*(*AReYVec)[j] + (*AImYVec)[j]*(*AImYVec)[j];
      
      Nb eigvecnorm = sqrt(Dot(ReEigenVec,ReEigenVec) + Dot(ImEigenVec,ImEigenVec));
      LeftResiduals[i] = sqrt(LeftResiduals[i])/eigvecnorm;
      Mout << " Left Norm " << LeftResiduals[i] << endl;
   }*/
   Mout << "Done testing residuals " << endl;
}

/**
 *
 **/
void NHBandLanczosChain::TestRT
   (  const In ItNum
   ,  const In anc
   ,  const std::vector<Nb>& aDelta
   )  
{
   Mout << " I am testing NH-BandLanczos Right Tmat. " << endl;

   Nb init_C_0 = C_0;
   MidasMatrix RTtest(ItNum, init_C_0);
   MidasMatrix Diff(ItNum, init_C_0);
   MidasMatrix TestV(anc, ItNum);
   MidasMatrix TestW(anc, ItNum);
   MidasMatrix D(ItNum, init_C_0);
   Nb diff_norm;
   Nb diff_norm2;

   //RTtest.SetNewSize(mDef.GetLength(), false, true);
   //Diff.SetNewSize(mDef.GetLength(), false, true);

   if(mpVcc->GetMethod() == VCC_METHOD_VCC)
   {
      mpTrf->SetType(TRANSFORMER::VCCJAC);
      Mout << " Method is VCC " << endl;
   }
   if(mpVcc->GetMethod() == VCC_METHOD_VCI)
   {
      mpTrf->SetType(TRANSFORMER::VCIRSP);
      Mout << " Method is VCI " << endl;
   }
   
   for(In i = I_0; i<ItNum; ++i)
   {
      ostringstream vos;
      vos << mFilePrefix << "_v" << i;
      DataCont v;
      v.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, vos.str());
      if(! v.ChangeStorageTo("InMem", true, false, true))
         MIDASERROR("Error reading v(j) in NHBandLanczosChain::TestRT()");

      TestV.AssignCol(*(v.GetVector()),i);

      ostringstream wos;
      wos << mFilePrefix << "_w" << i;
      DataCont w;
      w.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, wos.str());
      if(! w.ChangeStorageTo("InMem", true, false, true))
         MIDASERROR("Error reading w(j) in NHBandLanczosChain::TestRT()");

      TestW.AssignCol(*(w.GetVector()),i);

      Nb dummy=C_0;
      DataCont vj(anc);
      mpTrf->Transform(mV[i], vj, I_1, I_0, dummy);
      for(In j=I_0; j<ItNum; ++j)
      {
         RTtest[j][i] = Dot(vj,mW[j])/aDelta[j];
      }
   }
   
   /*Mout << " W: " << TestW.Nrows() << " x " << TestW.Ncols() << endl;
   TestW.Transpose();
   Mout << " W^t: " << TestW.Nrows() << " x " << TestW.Ncols() << endl;
   Mout << " V: " << TestV.Nrows() << " x " << TestV.Ncols() << endl;
   
   D = TestW*TestV;

   string filename_d = mFilePrefix + "_d";
   ofstream d_matrix_file(filename_d.c_str(), ios_base::trunc);
   d_matrix_file << scientific << setprecision(20);
   d_matrix_file << D << endl;*/
   
   //RTtest = D*RTtest;
   /*Mout << "RTtest" << endl;
   Mout << "Rows " << RTtest.Nrows() << endl;
   Mout << "Cols " << RTtest.Ncols() << endl;
   Mout << "mTmatRight" << endl;
   Mout << "Rows " << mTmatRight.Nrows() << endl;
   Mout << "Cols " << mTmatRight.Ncols() << endl;*/
   Diff = RTtest - mTmatRight;
   diff_norm = Diff.Norm();
   diff_norm2 = Diff.Norm2();

   string filename_t = mFilePrefix + "_t_test";
   ofstream t_test_matrix_file(filename_t.c_str(), ios_base::trunc);
   t_test_matrix_file << scientific << setprecision(20);
   t_test_matrix_file << RTtest << endl;

   string filename_diff = mFilePrefix + "_diff";
   ofstream diff_matrix_file(filename_diff.c_str(), ios_base::trunc);
   diff_matrix_file << scientific << setprecision(20);
   diff_matrix_file << diff_norm << endl;
   diff_matrix_file << diff_norm2 << endl << endl;
   diff_matrix_file << Diff << endl;
}

void NHBandLanczosChain::TestFullSpace(const In ItNum)
{
   Mout << " I am test NH-BandLanczos right and left spaces..." << endl;
   MidasMatrix sv(ItNum,ItNum,C_0);
   MidasMatrix sw(ItNum,ItNum,C_0);
   MidasMatrix sveigvecs(ItNum,ItNum);
   MidasVector sveigvals(ItNum);
   MidasMatrix sweigvecs(ItNum,ItNum);
   MidasVector sweigvals(ItNum);
   
   for(In i=I_0; i<ItNum;++i)
   {
      ostringstream vios;
      vios << mFilePrefix << "_v" << i;
      DataCont vi;
      vi.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, vios.str());
      if(! vi.ChangeStorageTo("InMem", true, false, true))
         MIDASERROR("Error reading v(i) in NHBandLanczosChain::TestFullSpace()");
      for(In j=I_0; j<ItNum;++j)
      {
         ostringstream vjos;
         vjos << mFilePrefix << "_v" << j;
         DataCont vj;
         vj.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, vjos.str());
         if(! vj.ChangeStorageTo("InMem", true, false, true))
            MIDASERROR("Error reading v(j) in NHBandLanczosChain::TestFullSpace()");
         
         sv[i][j] = Dot(vi,vj);
      }
   }

   Diag(sv,sveigvecs,sveigvals,"DGEEV");
   string filename = mFilePrefix + "_sveigvals";
   ofstream sveigvals_file(filename.c_str(), ios_base::trunc);
   sveigvals_file << scientific << setprecision(20);
   for(In i=I_0; i<sveigvals.Size(); ++i)
      sveigvals_file << sveigvals[i] << endl;

   for(In i=I_0; i<ItNum;++i)
   {
      ostringstream wios;
      wios << mFilePrefix << "_w" << i;
      DataCont wi;
      wi.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, wios.str());
      if(! wi.ChangeStorageTo("InMem", true, false, true))
         MIDASERROR("Error reading w(i) in NHBandLanczosChain::TestFullSpace()");
      for(In j=I_0; j<ItNum;++j)
      {
         ostringstream wjos;
         wjos << mFilePrefix << "_w" << j;
         DataCont wj;
         wj.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, wjos.str());
         if(! wj.ChangeStorageTo("InMem", true, false, true))
            MIDASERROR("Error reading w(j) in NHBandLanczosChain::TestFullSpace()");
         
         sw[i][j] = Dot(wi,wj);
      }
   }
   
   Diag(sw,sweigvecs,sweigvals,"DGEEV");
   string filenamesw = mFilePrefix + "_sweigvals";
   ofstream sweigvals_file(filenamesw.c_str(), ios_base::trunc);
   sweigvals_file << scientific << setprecision(20);
   for(In i=I_0; i<sweigvals.Size(); ++i)
      sweigvals_file << sweigvals[i] << endl;
   
}

/*void NHBandLanczosChain::TransformStoX(const MidasVector& aEigVec, DataCont& aX) const
{
   aX.SetNewSize(mpTrf->NexciXvec() -I_1);
   aX.Zero();
   aX.ChangeStorageTo("InMem");

   for(In i=I_0; i<aEigVec.Size(); ++i)
   {
      ostringstream os;
      os <<mFilePrefix << "_q" << i;
      DataCont q;
      q.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
      if(! q.ChangeStorageTo("InMem", true))
         MIDASERROR("NHBandLanczosChain::TransformStoX(): Error reading q(j).");
      aX.Axpy(q, aEigVec[i]);
   }
}*/
