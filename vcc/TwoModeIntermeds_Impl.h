/**
************************************************************************
* 
* @file                TwoModeIntermeds_Impl.h
*
* Created:             21-5-2007
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*
* Short Description:   Organizing intermediates for specific VCC[2]
*                      calculation.
* 
* Last modified: 
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include <map>

// My headers:
#include "vcc/TwoModeIntermeds.h"
#include "inc_gen/math_link.h" 
#include "input/OpDef.h"

template<typename T>
In TwoModeIntermeds<T>::Nmodals
   (  LocalModeNr aMode
   )  const
{
   return mNmodals[aMode];
}

template<typename T>
TwoModeIntermeds<T>::TwoModeIntermeds()
{
}

template<typename T>
TwoModeIntermeds<T>::TwoModeIntermeds(std::vector<In> aNmodals, const OpDef* aOpDef,
                                   const ModeCombiOpRange& arMcr, bool aZ)
{
   Initialize(std::move(aNmodals), aOpDef, arMcr, aZ);
}

template<typename T>
TwoModeIntermeds<T>::~TwoModeIntermeds()
{
   for (In m0=I_0; m0<mXC.size(); ++m0)
      for (In m1=I_0; m1<mXC[m0].size(); ++m1)
         for (auto it=mXC[m0][m1].begin(); it != mXC[m0][m1].end(); ++it)
            if (it->second != nullptr)
               delete it->second;
}

template<typename T>
TwoModeIntermeds<T>::TwoModeIntermeds(const TwoModeIntermeds& arOther)
   :  mNmodals(arOther.mNmodals)
   ,  mXaddr(arOther.mXaddr)
   ,  mX(arOther.mX)
   ,  mXC(arOther.mXC) // Copies the vec<vec<list<pair<In,MV>>>> structure; handle allocs later.
   ,  mY(arOther.mY)
   ,  mZ(arOther.mZ)
{
   for (In m0=I_0; m0<mXC.size(); ++m0)
      for (In m1=I_0; m1<mXC[m0].size(); ++m1)
      {  
         auto it=mXC[m0][m1].begin();
         auto it_oth = arOther.mXC[m0][m1].begin();
         for(
            ;  it != mXC[m0][m1].end()
            ;  ++it, ++it_oth
            )
            if (it_oth->second != nullptr)
               it->second = new midasvector_t(*(it_oth->second));
      }
}

/**
 * Allocate space for intermediates.
 * Z intermediates are only relevant for VCI and are optional.
 *
 * @param[in] aNmodals
 *    The number of modals for each mode.
 * @param[in] aOpDef
 *    The operator.
 * @param[in] arMcr
 *    Shall contain the ModeCombi%s of the excitation amplitude/coef. range.
 * @param[in] aZ
 *    Optionally calculate Z intermediates.
 */
template<typename T>
void TwoModeIntermeds<T>::Initialize(std::vector<In> aNmodals, const OpDef* aOpDef, 
                                  const ModeCombiOpRange& arMcr, bool aZ)
{
   mNmodals = std::move(aNmodals);

   // Allocate space for Y intermediates.
   In n_modes = aOpDef->NmodesInOp();
   mY.resize(n_modes);
   for (auto it=mY.begin(); it!=mY.end(); ++it)
   {
      it->resize(n_modes);
      for (auto y=it->begin(); y!=it->end(); ++y)
         *y = C_0;
   }
   
   // Initialize X / XC intermediate addresses.
   mXaddr.resize(aOpDef->NmodesInOp());
   mXC.resize(aOpDef->NmodesInOp());
   In i=I_0;
   for (auto it=mXaddr.begin(); it!=mXaddr.end(); ++it)
   {
      mXC[i].resize(aOpDef->NmodesInOp());
      it->resize(aOpDef->NmodesInOp());
      for (auto jt=it->begin(); jt!=it->end(); ++jt)
         (*jt) = -I_1;
      ++i;
   }
  
   In x_addr = I_0; 
   for(auto it_mc = arMcr.Begin(2), end = arMcr.End(2); it_mc != end; ++it_mc)
   {
      const auto& mc = *it_mc;

      LocalModeNr m0 = mc.Mode(I_0);
      LocalModeNr m1 = mc.Mode(I_1);
      In opers0 = aOpDef->NrOneModeOpers(m0);
      In opers1 = aOpDef->NrOneModeOpers(m1);
      In modals0 = this->Nmodals(m0);
      In modals1 = this->Nmodals(m1);

      mXaddr[m0][m1] = x_addr;
      x_addr += opers1 * (modals0);   // Need space for modals0-1 coefficients and 1 norm^2.
      mXaddr[m1][m0] = x_addr;
      x_addr += opers0 * (modals1);
   }
   mX.SetNewSize(x_addr);
   mX.Zero();
   
   if (aZ)
   {
      mZ.resize(aOpDef->NmodesInOp());
      LocalModeNr i_m = I_0;
      for (auto m = mZ.begin(); m != mZ.end(); ++m)
      {
         m->resize(aOpDef->NrOneModeOpers(i_m), value_t(0));
         i_m++;
      }
   }
}

/**
 * Generating (m1, oper1)XC(m0) intermediates.
 **/
template<typename T>
void TwoModeIntermeds<T>::InitializeXCintermeds(const OpDef* aOpDef)
{
   midasvector_t x_intermed;
   for (LocalModeNr m1=I_0; m1<aOpDef->NmodesInOp(); ++m1)
   {
      std::list<In> act_terms;
      for (In i_term=I_0; i_term<aOpDef->NactiveTerms(m1); ++i_term)
      {
         In term = aOpDef->TermActive(m1, i_term);
         if (aOpDef->NfactorsInTerm(term) != I_1)
            act_terms.push_back(term);
      }

      while (false == act_terms.empty())
      {
         In first_term = act_terms.front();

         // Set up std::vectors to store combined X intermediates.
         // Reserve space for coefficients and norm^2. Then set size to hold only the
         // coefficients. Norm will be put in later.
         std::vector<midasvector_t*> xc_intermeds(aOpDef->NmodesInOp());
         std::vector<bool> xc_intermeds_exist(aOpDef->NmodesInOp());
         for (In m0=I_0; m0<aOpDef->NmodesInOp(); ++m0)
         {
            xc_intermeds[m0] = new midasvector_t();
            In modals = this->Nmodals(m0);
            xc_intermeds[m0]->Reserve(modals);
            xc_intermeds[m0]->SetNewSize(modals-I_1);
            xc_intermeds[m0]->Zero();
            xc_intermeds_exist[m0] = false;
         }
         
         LocalOperNr cur_oper = I_0;     // Operator specific for intermediate currently being created.
         if (aOpDef->ModeForFactor(first_term, I_0) == m1)
            cur_oper = aOpDef->OperForFactor(first_term, I_0);
         else
            cur_oper = aOpDef->OperForFactor(first_term, I_1);

         // Now generate (m1, cur_oper)XC(m0).
         std::list<In>::iterator it_term = act_terms.begin();
         while (it_term != act_terms.end())
         {
            In term = *it_term;

            LocalOperNr oper1 = I_0;
            LocalModeNr m2 = I_0;
            LocalOperNr oper2 = I_0;
            if (aOpDef->ModeForFactor(term, I_0) == m1)
            {
               oper1 = aOpDef->OperForFactor(term, I_0);
               m2 = aOpDef->ModeForFactor(term, I_1);
               oper2 = aOpDef->OperForFactor(term, I_1);
            }
            else
            {
               oper1 = aOpDef->OperForFactor(term, I_1);
               m2 = aOpDef->ModeForFactor(term, I_0);
               oper2 = aOpDef->OperForFactor(term, I_0);
            }
            
            if (oper1 != cur_oper)
            {
               ++it_term;
               continue;
            }
            it_term = act_terms.erase(it_term);
            
            value_t coef = aOpDef->Coef(term);
            for (In m0=I_0; m0<aOpDef->NmodesInOp(); ++m0)
            {
               if (m0 == m2)
                  continue;
               
               // Adding (m2,oper2)X(m0) to (m1, oper1)XC(m0).
               if (GetXintermed(m0, m2, oper2, x_intermed))
               {
                  xc_intermeds_exist[m0] = true;
                  *(xc_intermeds[m0]) += coef * x_intermed;
               }
            }
         }

         for (In m0=I_0; m0<aOpDef->NmodesInOp(); ++m0)
            if (xc_intermeds_exist[m0])
            {
               absval_t norm2 = xc_intermeds[m0]->Norm2();
               In modals = this->Nmodals(m0);
               xc_intermeds[m0]->SetNewSize(modals, true);
               (*xc_intermeds[m0])[modals-I_1] = norm2;
               mXC[m0][m1].push_back(std::make_pair(cur_oper, xc_intermeds[m0]));
            }
            else
               delete xc_intermeds[m0];
      }
   }
}

template<typename T>
void TwoModeIntermeds<T>::PutXintermed(const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr oper, const midasvector_t& aVec)
{
   // Test for consistency.
   if (-I_1 == mXaddr[aM0][aM1])
   {
      Mout << "TwoModeIntermed::PutXintermed()" << endl
           << "   aM0 =  " << aM0 << endl
           << "   aM1 =  " << aM1 << endl
           << "   oper = " << oper << endl;
      Error("TwoModeIntermed::PutXintermed(): Trying to put non-existing intermed");
   }

   In addr = mXaddr[aM0][aM1];
   In n_mod = this->Nmodals(aM0);
   addr += oper * (n_mod);

   for (In i=I_0; i<(n_mod-I_1); ++i)
      mX[addr+i] = aVec[i];

   mX[addr+n_mod-I_1] = aVec.Norm2();
}

template<typename T>
bool TwoModeIntermeds<T>::GetXintermed(const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr oper, midasvector_t& aVec) const
{
   In addr = mXaddr[aM0][aM1];
   if (-I_1 == addr)
      return false;

   In n_mod = this->Nmodals(aM0);
   addr += oper * n_mod;

   aVec.SetNewSize(n_mod-I_1);
   for (In i=I_0; i<(n_mod-I_1); ++i)
      aVec[i] = mX[addr+i];

   return true;
}

/**
 * @param[in] aM0
 * @param[in] aM1
 * @param[in] oper
 * @param[out] aNorm2
 *    If function returns false, this argument is unmodified.
 *    If function returns true, this is the norm^2 of the (aM0,aM1,oper) X-intermeds.
 *    Note (MBH, Jan 2019): aNorm2 is set to the _real_ part an mX element
 *    (which has value_t, can be complex), so we rely on mX[add+n_mod-I_1]
 *    actually being the norm^2, i.e. with only a real component. Scary...
 * @return
 *    Whether any X-intermeds. are stored, i.e. whether the function call was successful.
 **/
template<typename T>
bool TwoModeIntermeds<T>::GetXnorm2(const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr oper, absval_t& aNorm2) const
{
   In addr = mXaddr[aM0][aM1];
   if (-I_1 == addr)
      return false;

   In n_mod = this->Nmodals(aM0);
   addr += oper * n_mod;
   aNorm2 = std::real(mX[addr+n_mod-I_1]);
   return true;
}

template<typename T>
bool TwoModeIntermeds<T>::GetXCintermeds(const LocalModeNr aM0, const LocalModeNr aM1, midasvector_t* aXC) const
{
   bool res = false;
   for (auto it=mXC[aM0][aM1].begin(); it != mXC[aM0][aM1].end(); ++it)
   {
      In op1 = it->first;
      In coefs = it->second->Size() - I_1;
      aXC[op1].SetNewSize(coefs);
      for (In i=I_0; i<coefs; ++i)
         aXC[op1][i] = (*(it->second))[i];
      res = true;
   }
   return res;
}

template<typename T>
const std::list<std::pair<In, typename TwoModeIntermeds<T>::midasvector_t*> >& TwoModeIntermeds<T>::GetXCintermeds(const LocalModeNr aM0, const LocalModeNr aM1) const
{
   return mXC[aM0][aM1];
}

template<typename T>
void TMI_DirProd(const TwoModeIntermeds<T>& aIntermed0, const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr aOperM1,
      const TwoModeIntermeds<T>& aIntermed2, const LocalModeNr aM2, const LocalModeNr aM3, const LocalOperNr aOperM3,
      typename TwoModeIntermeds<T>::midasvector_t& aRes)
{
   In addr0 = aIntermed0.mXaddr[aM0][aM1];
   In n_mod0 = aIntermed0.Nmodals(aM0);
   addr0 += aOperM1 * n_mod0;

   In addr2 = aIntermed2.mXaddr[aM2][aM3];
   In n_mod2 = aIntermed2.Nmodals(aM2);
   addr2 += aOperM3 * n_mod2;

   for (In idx0=I_0; idx0<(n_mod0-I_1); ++idx0)
      for (In idx2=I_0; idx2<(n_mod2-I_1); ++idx2)
         aRes[idx0*(n_mod2-I_1)+idx2] = aIntermed0.mX[addr0+idx0] * aIntermed2.mX[addr2+idx2];
}  


template<typename T>
void TMI_DirProd(const TwoModeIntermeds<T>& aX, const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr aOp1,
                 const typename TwoModeIntermeds<T>::midasvector_t& aC, const LocalModeNr aM2, typename TwoModeIntermeds<T>::midasvector_t& aRes)
{
   In addr = aX.mXaddr[aM0][aM1];
   In n_mod0 = aX.Nmodals(aM0);
   addr += aOp1*n_mod0;

   In n_mod2 = aX.Nmodals(aM2);

   if (aM0 < aM2)
      for (In idx0=I_0; idx0<(n_mod0-I_1); ++idx0)
         for (In idx2=I_0; idx2<(n_mod2-I_1); ++idx2)
            aRes[idx0*(n_mod2-I_1)+idx2] = aX.mX[addr+idx0] * aC[idx2];
   else
      for (In idx2=I_0; idx2<(n_mod2-I_1); ++idx2)
         for (In idx0=I_0; idx0<(n_mod0-I_1); ++idx0)
            aRes[idx2*(n_mod0-I_1)+idx0] = aC[idx2] * aX.mX[addr+idx0];
}


template<typename T>
std::ostream& operator<<(std::ostream& aOut, const TwoModeIntermeds<T>& aArg)
{
   Mout << "Y(m0,m1):" << endl;
   Mout << "Printing not implemented yet." << endl;

   Mout << endl << "(m1,oper)X(m0,a):" << endl;
   Mout << "mX.Size() = " << aArg.mX.Size() << endl;
  
   In i_m0 = I_0;
   for (auto m0=aArg.mXaddr.begin(); m0!=aArg.mXaddr.end(); ++m0)
   {
      aOut << "m0 = " << i_m0 << endl;
      In i_m1 = I_0;
      for (auto m1=m0->begin(); m1!=m0->end(); ++m1)
      {
         aOut << "   m1 = " << i_m1 << "      addr: " << *m1 << endl;
         i_m1++;
      }
      i_m0++;
   }
   
   Mout << endl << "(m0,oper)Z:" << endl;
   i_m0 = I_0;
   for (auto m0 = aArg.mZ.begin(); m0 != aArg.mZ.end(); ++m0)
   {
      aOut << "m0 = " << i_m0 << "  ";
      for (auto oper = m0->begin(); oper != m0->end(); ++oper)
         aOut << "  " << *oper;
      aOut << endl;
      i_m0++;
   }
   
   return aOut;
}
