/**
 *******************************************************************************
 * 
 * @file    Vcc2ContribsEta_Impl.h
 * @date    04-02-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refact., templ.
 *
 * @brief
 *    VCC[2]/H2 contributions. Refactored out of the VccTransformer class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2CONTRIBSETA_IMPL_H_INCLUDED
#define VCC2CONTRIBSETA_IMPL_H_INCLUDED

// Midas headers.
#include "inc_gen/Const.h"
#include "input/ModeCombi.h"
#include "input/OpDef.h"
#include "vcc/ModalIntegrals.h"
#include "mmv/MidasVector.h"

namespace midas::vcc::vcc2
{

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
Vcc2ContribsEta<T>::Vcc2ContribsEta
   (  const opdef_t* const apOpDef
   ,  const modalintegrals_t& arModInts
   )
   :  Vcc2ContribsBase<T>
      (  apOpDef
      ,  arModInts
      )
{
   SanityCheck();
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsEta<T>::TwoModeEta1(const ModeCombi& aM0, midasvector_t& aRes) const
{
   //Mout << "In TwoModeEta1 " << endl;
   LocalModeNr m0 = aM0.Mode(I_0);
   In n_act = this->pOpDef()->NactiveTerms(m0);
   In n_modals = this->NModals(m0)-I_1;

   for (In i=I_0; i<n_act; i++)
   { 
      midasvector_t res_up(n_modals);
      In term=this->pOpDef()->TermActive(m0,i);
      value_t fact=this->pOpDef()->Coef(term);
      LocalOperNr oper = I_0;

      if (this->pOpDef()->NfactorsInTerm(term) == I_2)
      {
         LocalModeNr op_m0 = this->pOpDef()->ModeForFactor(term, I_0);
         LocalModeNr op_m1 = this->pOpDef()->ModeForFactor(term, I_1);
         LocalOperNr oper0 = this->pOpDef()->OperForFactor(term, I_0);
         LocalOperNr oper1 = this->pOpDef()->OperForFactor(term, I_1);
         if ( op_m0 != m0 )
         {
            fact *= this->ModInts().GetOccElement( op_m0, oper0);
            oper = oper1;
         } 
         else
         {
            fact *= this->ModInts().GetOccElement( op_m1, oper1);
            oper = oper0;
         } 
      }
      else if (this->pOpDef()->NfactorsInTerm(term) == I_1 ) oper = this->pOpDef()->OperForFactor(term, I_0);

      ModeCombi mc_empty;
      midasvector_t inp_up(I_1);
      inp_up[I_0] = fact;
      this->ModInts().Contract(aM0, mc_empty, m0, oper, res_up, inp_up, I_1, true);
      aRes += res_up;
   } 
}

/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsEta<T>::TwoModeEta2(const ModeCombi& aM0, midasvector_t& aRes) const
{
   //Mout << "In TwoModeEta2 " << endl;
   In m0 = aM0.Mode(I_0);
   In m1 = aM0.Mode(I_1);

   In n_modals = this->NModals(m0)-I_1;
   In n_exci= aRes.Size();

   In i_first_term = I_0;
   In n_terms = I_0;
   if (!this->pOpDef()->TermsForMc(aM0, i_first_term, n_terms)) return;

   midasvector_t int_res(aRes.Size());
   ModeCombi mc_empty;

   for (In i = I_0 ; i < n_terms; i++)
   {
      In i_term = i_first_term + i;
      LocalModeNr mode0 = this->pOpDef()->ModeForFactor(i_term,I_0);
      LocalOperNr oper0 = this->pOpDef()->OperForFactor(i_term,I_0);
      LocalOperNr oper1 = this->pOpDef()->OperForFactor(i_term,I_1);
      value_t fact=this->pOpDef()->Coef(i_term);
      midasvector_t inp_up(I_1);
      inp_up[I_0] = fact;
      ModeCombi mc_0;
      mc_0.InsertMode(m0);
      midasvector_t res_up(n_modals);
      midasvector_t res_up2(n_exci);

      if (mode0 == m0)
      {
         this->ModInts().Contract(mc_0, mc_empty, m0, oper0, res_up, inp_up, I_1, true);
         this->ModInts().Contract(aM0, mc_0, m1, oper1, res_up2, res_up, I_1, true);
      }
      else if (mode0 == m1)
      {
         this->ModInts().Contract(mc_0, mc_empty, m0, oper1, res_up, inp_up, I_1, true);
         this->ModInts().Contract(aM0, mc_0, m1, oper0, res_up2, res_up, I_1, true);
      }

      int_res += res_up2;
   } 
   
   aRes += int_res;
   //Mout << "aRes: " << aRes << endl; 
}


/***************************************************************************//**
 *
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsEta<T>::SanityCheck
   (
   )  const
{
   // Check modal integrals type.
   this->CheckTransType(this->ModInts(), modalintegrals_t::TransT::T1, "T1");
}

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2CONTRIBSETA_IMPL_H_INCLUDED*/
