/**
 *******************************************************************************
 * 
 * @file    Vcc2TransBase_Impl.h
 * @date    11-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2TRANSBASE_IMPL_H_INCLUDED
#define VCC2TRANSBASE_IMPL_H_INCLUDED

// Midas headers.
#include "util/Error.h"
#include "input/OpDef.h"
#include "input/ModeCombiOpRange.h"
#include "vcc/ModalIntegrals.h"
#include "vcc/TwoModeIntermeds.h"

namespace midas::vcc::vcc2
{

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   >  
Vcc2TransBase<T>::Vcc2TransBase
   (  const opdef_t* const apOpDef
   ,  const modalintegrals_t& arRawModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const bool aOnlyOneMode
   ,  const Uin aIoLevel
   ,  const bool aTimeIt
   )
   :  mpOpDef(apOpDef)
   ,  mrRawModInts(arRawModInts)
   ,  mrMcr(arMcr)
   ,  mNModals(VecNModals(mrRawModInts))
   ,  mMcAddresses(VecMcAddresses(mrMcr, mNModals))
   ,  mOnlyOneMode((mrMcr.GetMaxExciLevel() < 2)? true: aOnlyOneMode)
   ,  mIoLevel(aIoLevel)
   ,  mTimeIt(aTimeIt)
{
   SanityCheck();
}

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   >  
void Vcc2TransBase<T>::InitializeXIntermeds
   (  intermeds_t& arIntermeds
   ,  const modalintegrals_t& arModInts
   ,  const datacont_t& arDc
   )  const
{
   // Create intermediates (m1,oper)X(m0,a).
   const ModeCombiOpRange& mcr = this->Mcr();
   for(auto it_mc = mcr.Begin(2), end = mcr.End(2); it_mc != end; ++it_mc)
   {
      const ModeCombi& mc = *it_mc;

      LocalModeNr m0 = mc.Mode(I_0);
      LocalModeNr m1 = mc.Mode(I_1);

      In n_amps = this->NexciForModeCombi(mc.ModeCombiRefNr());
      midasvector_t amps(n_amps);
      arDc.DataIo(IO_GET, amps, n_amps, mc.Address());

      // Contract with first mode.
      ModeCombi mc1(I_1);
      mc1.InsertMode(m1);
      midasvector_t res(this->NModals(m1)-I_1);
      for (In i_oper=I_0; i_oper<this->pOpDef()->NrOneModeOpers(m0); ++i_oper)
      {
         res.Zero();
         arModInts.Contract(mc1, mc, m0, i_oper, res, amps, -I_1);
         arIntermeds.PutXintermed(m1, m0, i_oper, res);
      }

      // Contract with second mode.
      ModeCombi mc0(I_1);
      mc0.InsertMode(m0);
      res.SetNewSize(this->NModals(m0)-I_1);
      for (In i_oper=I_0; i_oper<this->pOpDef()->NrOneModeOpers(m1); ++i_oper)
      {
         res.Zero();
         arModInts.Contract(mc0, mc, m1, i_oper, res, amps, -I_1);
         arIntermeds.PutXintermed(m0, m1, i_oper, res);
      }
   }
}

/***************************************************************************//**
 * @param[out] arIntermeds
 *    Assign to Z intermediates of this object.
 * @param[in] arModInts
 *    The modal integrals to store intermediates for.
 * @param[in] arDc
 *    Relevant parameters; C-coefs. for VCI, L-coefs. for L-Jacobian. Not used
 *    by other models.
 * @param[in] aLJac
 *    Set to true when called for L-Jacobian. Signals the use of transposed
 *    modal integrals.
 ******************************************************************************/
template
   <  typename T
   >  
void Vcc2TransBase<T>::InitializeZIntermeds
   (  intermeds_t& arIntermeds
   ,  const modalintegrals_t& arModInts
   ,  const datacont_t& arDc
   ,  const bool aLJac
   )  const
{
   const ModeCombiOpRange& mcr = this->Mcr();
   for(auto it_mc = mcr.Begin(1), end = mcr.End(1); it_mc != end; ++it_mc)
   {
      const ModeCombi& mc = *it_mc;

      LocalModeNr m0 = mc.Mode(I_0);

      In n_coefs = this->NexciForModeCombi(mc.ModeCombiRefNr());
      midasvector_t coefs(n_coefs);
      arDc.DataIo(IO_GET, coefs, n_coefs, mc.Address());
      
      ModeCombi mc_empty(I_0);
      for (In i_oper=I_0; i_oper<this->pOpDef()->NrOneModeOpers(m0); ++i_oper)
      {
         midasvector_t res(I_1, C_0);
         arModInts.Contract(mc_empty, mc, m0, i_oper, res, coefs, -I_1, aLJac);
         arIntermeds.AssignToZ(m0,i_oper,res[I_0]);
      }
   }
}

/***************************************************************************//**
 * @param[in] arParams
 *    Parameter container from which to extract the 1-mode parameters.
 *    Uses the ModeCombi addresses for finding relevant params. in the
 *    container.
 * @return
 *    Vectors containing the 1-mode parameters for each mode, each of size
 *    `NModals(m)`, with a zero at index 0, and the `NModals(m)-1` parameters
 *    at indices `1,...,NModals(m)-1`.
 ******************************************************************************/
template
   <  typename T
   >  
std::vector<typename Vcc2TransBase<T>::midasvector_t> Vcc2TransBase<T>::Extract1ModeParams
   (  const datacont_t& arParams
   )  const
{
   const auto& mcr = this->Mcr();
   std::vector<midasvector_t> v_mv;
   v_mv.reserve(mcr.NumModeCombisWithExcLevel(I_1));
   for(auto it = mcr.Begin(I_1), end = mcr.End(I_1); it != end; ++it)
   {
      // Extract the NModals(mode)-1 params, leaving a zero at index 0.
      midasvector_t mv(this->NModals(it->Mode(I_0)), value_t(0));
      arParams.DataIo(IO_GET, mv, mv.Size()-I_1, it->Address(), I_1, I_1);
      v_mv.emplace_back(std::move(mv));
   }
   return v_mv;
}

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   >  
void Vcc2TransBase<T>::Transform
   (  const datacont_t& arIn
   ,  datacont_t& arOut
   )  const
{
   // Output info.
   if (gDebug || IoLevel() > I_10 || TimeIt())
   {  
      Mout  << "Entering " << PrettyClassName() << "::Transform(...)\n"
            << "   Excitation level = " << (OnlyOneMode()? "1-mode": "2-mode") << '\n'
            << "   Transformer type = " << PrettyTransformerType() << '\n'
            << "   Operator name    = " << pOpDef()->Name() << '\n'
            << std::endl;
   }

   // Sanity checks.
   SanityCheckDc(arIn, "arIn");
   SanityCheckDc(arOut, "arOut", AddrOffsetDcOut());

   // The actual implementation, see derived classes.
   clock_t clock_tot = clock();
   TransformImpl(arIn, arOut);
   fclock_t t_tot = fclock_t(clock() - clock_tot) / CLOCKS_PER_SEC;

   // Finalize here or in derived?

   // Output info.
   if (TimeIt())
   {
      Mout  << "Total CPU  time: " << t_tot << " s  "
            << "(in " << PrettyClassName() << "::Transform(...))"
            << std::endl;
   }
   if (gDebug || IoLevel() > I_10 || TimeIt())
   {
      Mout  << "Exiting " << PrettyClassName() << "::Transform(...)\n"
            << std::endl;
   }
}

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   >  
std::vector<Uin> Vcc2TransBase<T>::VecNModals
   (  const modalintegrals_t& arModInts
   )
{
   std::vector<Uin> v;
   v.reserve(arModInts.NModes());
   for(LocalModeNr m = 0; m < arModInts.NModes(); ++m)
   {
      v.emplace_back(arModInts.NModals(m));
   }  
   return v;
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
std::vector<Uin> Vcc2TransBase<T>::VecMcAddresses
   (  const ModeCombiOpRange& arMcr
   ,  const std::vector<Uin>& arNModals
   )
{  
   // Check sanity of arMcr vs. arNModals.
   // If not sane, the returned vector will be of right size, but contain all
   // zeroes. SanityCheck() will provide a better error message later.
   bool sane = true;
   if (arMcr.NumberOfModes() > 0)
   {
      if (arMcr.SmallestMode() < 0 || arMcr.LargestMode() >= arNModals.size())
      {
         sane = false;
      }
   }

   std::vector<Uin> v;
   v.reserve(arMcr.Size() + I_1);
   Uin a = 0;
   for(const auto& mc: arMcr)
   {     
      v.emplace_back(a);
      if (sane)
      {
         a += NumParams(mc, arNModals);
      }
   }
   v.emplace_back(a);
   return v;
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2TransBase<T>::SanityCheck
   (
   )  const
{
   std::stringstream ss;

   // ModeCombi level in operator and ModeCombiOpRange.
   if (pOpDef()->McLevel() > I_2)
   {
      MIDASERROR("Vcc2TransBase: Operator MC level (= "+std::to_string(pOpDef()->McLevel())+") > 2."); 
   }
   if (Mcr().GetMaxExciLevel() > I_2)
   {
      MIDASERROR("Vcc2TransBase: MCR max level (= "+std::to_string(Mcr().GetMaxExciLevel())+") > 2."); 
   }
   
   // Num. modes.
   if (  pOpDef()->NmodesInOp() != RawModInts().NModes()
      || pOpDef()->NmodesInOp() != NModals().size()
      || pOpDef()->NmodesInOp() != Mcr().NumberOfModes()
      )
   {
      ss << "Num. modes mismatch; OpDef (= " << pOpDef()->NmodesInOp()
         << "), ModalIntegrals (= " << RawModInts().NModes()
         << "), Mcr() (= " << Mcr().NumberOfModes()
         << "), NModals() size (= " << NModals().size()
         << ")."
         ;
      MIDASERROR(ss.str());
   }

   // The local->global mode number mapping in OpDef must be 1-to-1, otherwise
   // things will crash.
   for(LocalModeNr m = 0; m < pOpDef()->NmodesInOp(); ++m)
   {
      if (pOpDef()->GetGlobalModeNr(m) != GlobalModeNr(int(m)))
      {
         ss << "OpDef local->global mode numbers not directly 1-to-1; "
            << "GetGlobalModeNr("<<m<<") = " << pOpDef()->GetGlobalModeNr(m)
            << " != " << m
            << ".\nThis will not work for current implementation."
            ;
         MIDASERROR(ss.str());
      }
   }

   // Num. modals.
   for(const auto& n: NModals())
   {
      if (n < I_1)
      {
         MIDASERROR("Found a num.modals < 1; this is invalid.");
      }
   }

   // ModeCombiOpRange should contain the empty ModeCombi as well as all
   // 1-ModeCombis, _and_ the modes should be 0,...,M-1; the implementations
   // rely on these assumptions in some/many cases.
   const auto& mcr = Mcr();
   if (mcr.NumEmptyMCs() == 0)
   {
      MIDASERROR("ModeCombiOpRange must contain the empty ModeCombi, but does not.");
   }
   Uin mc1_m = 0;
   std::vector<In> mc1_expect(1);
   for(auto it_mc1 = mcr.Begin(1), end = mcr.End(1); it_mc1 != end; ++it_mc1, ++mc1_m)
   {
      mc1_expect.at(0) = mc1_m;
      if (mc1_expect != it_mc1->MCVec())
      {
         ss << "Unexpected 1-ModeCombi; expected " << mc1_expect
            << ", got " << it_mc1->MCVec() << "."
            ;
         MIDASERROR(ss.str());
      }
   }
   if (mc1_m != this->RawModInts().NModes())
   {
      ss << "Unexpected number of 1-ModeCombis; expected " << this->RawModInts().NModes()
         << ", counted " << mc1_m
         ;
      MIDASERROR(ss.str());
   }

   // ModeCombi addresses.
   if (mMcAddresses.size() != mcr.Size()+I_1)
   {
      ss << "Num ModeCombi addresses mismatch; mMcAddresses.size() = " << mMcAddresses.size()
         << ", mcr.Size()+I_1 = " << mcr.Size()+I_1
         ;
      MIDASERROR(ss.str());
   }
   else
   {
      for(Uin i = 0; i < mcr.Size(); ++i)
      {
         if (mMcAddresses.at(i) != mcr.GetModeCombi(i).Address())
         {
            ss << "ModeCombi address mismatch; mMcAddresses.at("<<i<<") = " << mMcAddresses.at(i)
               << ", mcr.GetModeCombi("<<i<<").Address() = " << mcr.GetModeCombi(i).Address()
               ;
            MIDASERROR(ss.str());
         }
      }
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2TransBase<T>::SanityCheckDc
   (  const datacont_t& arDc
   ,  const std::string& arErrStr
   ,  const In aSizeCorrection
   )  const
{
   std::stringstream ss;

   const auto& exp_size = mMcAddresses.at(mMcAddresses.size() - 1) + aSizeCorrection;
   const auto& size = arDc.Size();
   if (exp_size != size)
   {
      ss << "Size mismatch; expected " << exp_size << ", got " << size << " (" << arErrStr << ").";
      MIDASERROR(ss.str());
   }
}

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2TRANSBASE_IMPL_H_INCLUDED*/
