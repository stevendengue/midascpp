/**
 *******************************************************************************
 * 
 * @file    Vcc2ContribsReg.cc
 * @date    04-02-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refact., templ.
 *
 * @brief
 *    VCC[2]/H2 contributions. Refactored out of the VccTransformer class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "vcc/vcc2/Vcc2ContribsReg.h"
#include "vcc/vcc2/Vcc2ContribsReg_Impl.h"

#include "inc_gen/TypeDefs.h"

// Define instatiation macro.
#define INSTANTIATE_VCC2CONTRIBSREG_RSP_METHODS(T, RSP) \
   namespace midas::vcc::vcc2 \
   { \
      template void Vcc2ContribsReg<T>::TwoModeRefRef<RSP>(value_t&) const; \
      template void Vcc2ContribsReg<T>::TwoModeM0ref<RSP>(const LocalModeNr, midasvector_t&) const; \
      template void Vcc2ContribsReg<T>::TwoModeM0T2<RSP> \
         (  const In \
         ,  midasvector_t& \
         ,  const twomodeintermeds_t& \
         )  const; \
      template void Vcc2ContribsReg<T>::TwoModeM0M1ref<RSP>(const ModeCombi& aMc, midasvector_t& aRes) const; \
      template void Vcc2ContribsReg<T>::TwoModeM0M1T2<RSP> \
         (  const ModeCombi& \
         ,  midasvector_t& \
         ,  const twomodeintermeds_t& \
         ,  const value_t \
         ,  clock_t* const \
         ,  clock_t* const \
         ,  In* const \
         ,  In* const \
         ,  In* const \
         ,  In* const \
         )  const; \
      template void Vcc2ContribsReg<T>::TwoModeM0M1T2T2<RSP> \
         (  const ModeCombi& \
         ,  midasvector_t& \
         ,  const twomodeintermeds_t& \
         ,  const twomodeintermeds_t* const \
         ,  clock_t* const \
         ,  clock_t* const \
         ,  In* const \
         ,  In* const \
         )  const; \
   } /* namespace midas::vcc::vcc2 */ \

#define INSTANTIATE_VCC2CONTRIBSREG(T) \
   namespace midas::vcc::vcc2 \
   { \
      template class Vcc2ContribsReg<T>; \
   } /* namespace midas::vcc::vcc2 */ \
   INSTANTIATE_VCC2CONTRIBSREG_RSP_METHODS(T, false); \
   INSTANTIATE_VCC2CONTRIBSREG_RSP_METHODS(T, true); \


// Instantiations.
INSTANTIATE_VCC2CONTRIBSREG(Nb);
INSTANTIATE_VCC2CONTRIBSREG(std::complex<Nb>);

#undef INSTANTIATE_VCC2CONTRIBSREG
#undef INSTANTIATE_VCC2CONTRIBSREG_RSP_METHODS

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
