/**
 *******************************************************************************
 * 
 * @file    Vcc2ContribsBase_Decl.h
 * @date    04-02-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refact., templ.
 *
 * @brief
 *    VCC[2]/H2 contributions. Refactored out of the VccTransformer class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2CONTRIBSBASE_DECL_H_INCLUDED
#define VCC2CONTRIBSBASE_DECL_H_INCLUDED

// Standard headers.
#include <string>
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"

// Forward declarations.
template<typename> class ModalIntegrals;
class OpDef;
class ModeCombiOpRange;

namespace midas::vcc::vcc2
{
   /************************************************************************//**
    * @brief
    *    Handles computation of the individual contributions in VCC[2]/H2
    *    transformations.
    *
    * This specific class only contains the base members and utility methods
    * common to all other classes in the hierarchy; no methods for actually
    * computing any form of VCC[2] contributions.
    *
    * See these papers for the theoretical details:
    * - [Computation of expectation values from vibrational coupled-cluster at
    * the two-mode coupling level](https://doi.org/10.1063/1.3560027)
    * - [Towards fast computations of correlated vibrational wave functions: Vibrational coupled cluster 
    * response excitation energies at the two-mode coupling level](https://doi.org/10.1063/1.2907860)
    *
    * This class hierarchy re-organizes the methods for individual VCC[2]
    * contributions, preivously found in the VccTransformer class, and
    * templates the methods.
    *
    * The aim is to only provide the necessary methods and members to the
    * derived classes, according to which type of contributions they are
    * intended to compute.
    * Furthermore, there is some additional internal consistency checks for the
    * classes, and they are made rather rigid to avoid manipulations after
    * construction that would cause inconsistent results for different
    * contributions.
    *
    * @note
    *    To a large extent, the class members are const-refs/pointers to
    *    external objects, which is (of course) to save memory and allocation
    *    time. Take care not to invalidate these externally, and not to cause
    *    any pointers/references to dangle.
    ***************************************************************************/
   template
      <  typename T
      >
   class Vcc2ContribsBase
   {
      protected:
         using value_t = T;
         using modalintegrals_t = ModalIntegrals<value_t>;
         using modalintegrals_trans_t = typename modalintegrals_t::TransT;
         using opdef_t = OpDef;

         Vcc2ContribsBase(const Vcc2ContribsBase&) = delete;
         Vcc2ContribsBase& operator=(const Vcc2ContribsBase&) = delete;
         Vcc2ContribsBase(Vcc2ContribsBase&&) = default;
         Vcc2ContribsBase& operator=(Vcc2ContribsBase&&) = default;
         virtual ~Vcc2ContribsBase() = default;

         //! Constructs the bare minimum; operator structure and associated modal integrals.
         Vcc2ContribsBase
            (  const opdef_t* const apOpDef
            ,  const modalintegrals_t& arModInts
            );

         //!{
         //! Const access to members.
         const opdef_t* const pOpDef() const {return mpOpDef;}
         const modalintegrals_t& ModInts() const {return mrModInts;}
         //!}

         //!{
         //! The number of modals for a mode, or all modes.
         Uin NModals(const LocalModeNr aM) const {return mNModals[aM];}
         const std::vector<Uin>& VecNModals() const {return mNModals;}
         //!}

         //! Check the RAW/T1/R1 type of modal integrals against expected type.
         static void CheckTransType
            (  const modalintegrals_t&
            ,  const modalintegrals_trans_t&
            ,  const std::string&
            );

      private:
         //! The operator "structure"; coefficients and which 1-mode opers. for each term.
         const opdef_t* const mpOpDef;

         //! The 1-mode operator matrix elements/integrals in the modal basis.
         const modalintegrals_t& mrModInts;

         //! The number of modals for each mode.
         const std::vector<Uin> mNModals;

         //! Extract the number of modals for each mode from a modal integrals object.
         static std::vector<Uin> VecNModals(const modalintegrals_t&);

         //! Internal consistency of container sizes and such.
         void SanityCheck() const;
   };

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2CONTRIBSBASE_DECL_H_INCLUDED*/
