/**
 *******************************************************************************
 * 
 * @file    Vcc2TransLJac_Decl.h
 * @date    11-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2TRANSLJAC_DECL_H_INCLUDED
#define VCC2TRANSLJAC_DECL_H_INCLUDED

// Midas headers.
#include "vcc/vcc2/Vcc2TransFixedAmps.h"
#include "vcc/TwoModeIntermeds.h"

// Forward declarations.
namespace midas::vcc::vcc2
{
   template<typename> class Vcc2ContribsLJac;
} /* namespace midas::vcc::vcc2 */

namespace midas::vcc::vcc2
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename T
      >
   class Vcc2TransLJac final
      :  public Vcc2TransFixedAmps<T>
   {
      public:
         using typename Vcc2TransBase<T>::value_t;
         using typename Vcc2TransBase<T>::absval_t;
         using typename Vcc2TransBase<T>::energy_t;
         using typename Vcc2TransBase<T>::opdef_t;
         using typename Vcc2TransBase<T>::modalintegrals_t;
         using typename Vcc2TransBase<T>::midasvector_t;
         using typename Vcc2TransBase<T>::datacont_t;

         Vcc2TransLJac(const Vcc2TransLJac&) = delete;
         Vcc2TransLJac& operator=(const Vcc2TransLJac&) = delete;
         Vcc2TransLJac(Vcc2TransLJac&&) = default;
         Vcc2TransLJac& operator=(Vcc2TransLJac&&) = default;
         ~Vcc2TransLJac() override = default;

         Vcc2TransLJac
            (  const opdef_t* const apOpDef
            ,  const modalintegrals_t& arRawModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const datacont_t& arTAmps
            ,  const bool aOnlyOneMode = false
            ,  const Uin aIoLevel = 0
            ,  const bool aTimeIt = false
            );

      private:
         using vcc2contribsljac_t = midas::vcc::vcc2::Vcc2ContribsLJac<value_t>;
         using typename Vcc2TransBase<T>::intermeds_t;
         using typename Vcc2TransBase<T>::fclock_t;

         /******************************************************************//**
          * @brief
          *    Holds various intermediates, some based on T-amplitudes, some
          *    based on L-coefficients.
          *
          * The T-based intermediates are constant w.r.t. different calls to
          * Transform(). The L-based intermediates must be calculated at each
          * call to Transform(), since different L-coefs. are used.
          * Hence the member is declared `mutable`, since the SetupZIntermeds()
          * called at each Transform() does not affect the state of the object
          * from the client's perspective.
          *********************************************************************/
         mutable intermeds_t mT2Intermeds;

         const intermeds_t& T2Intermeds() const {return mT2Intermeds;}

         std::string PrettyClassName() const override;
         std::string PrettyTransformerType() const override;

         intermeds_t ConstructT2Intermeds(const datacont_t& arParams) const;
         void SetupZIntermeds(const datacont_t& arLCoefs) const;

         //!
         void TransformImpl(const datacont_t& arIn, datacont_t& arOut) const override;

         void TwoModeTransformerImpl
            (  const vcc2contribsljac_t& arVcc2Contribs
            ,  const intermeds_t& arIntermeds
            ,  const datacont_t& arIn
            ,  datacont_t& arOut
            )  const;

   };

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2TRANSLJAC_DECL_H_INCLUDED*/
