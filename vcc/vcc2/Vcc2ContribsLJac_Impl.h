/**
 *******************************************************************************
 * 
 * @file    Vcc2ContribsLJac_Impl.h
 * @date    04-02-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refact., templ.
 *
 * @brief
 *    VCC[2]/H2 contributions. Refactored out of the VccTransformer class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2CONTRIBSLJAC_IMPL_H_INCLUDED
#define VCC2CONTRIBSLJAC_IMPL_H_INCLUDED

// Midas headers.
#include "input/OpDef.h"
#include "vcc/ModalIntegrals.h"
#include "vcc/TwoModeIntermeds.h"

namespace midas::vcc::vcc2
{

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   >
Vcc2ContribsLJac<T>::Vcc2ContribsLJac
   (  const opdef_t* const apOpDef
   ,  const modalintegrals_t& arModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const datacont_t& arTAmps
   ,  const datacont_t& arLCoefs
   )
   :  Vcc2ContribsBaseAmps<T>
         (  apOpDef
         ,  arModInts
         ,  arMcr
         ,  arTAmps
         )
   ,  mrLCoefs(arLCoefs)
{
   SanityCheck();
}

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeLHTau(const ModeCombi& aM0, midasvector_t& aRes, const twomodeintermeds_t& aIntermeds ) const
{
   LocalModeNr m0 = aM0.Mode(I_0);
   In n_act = this->pOpDef()->NactiveTerms(m0);
   In n_gcoef = this->NModals(m0)-I_1;

   for (In i=I_0; i<n_act; ++i)
   {
      In term = this->pOpDef()->TermActive(m0, i);

      if (this->pOpDef()->NfactorsInTerm(term) == I_1)
         continue;

      value_t coef = this->pOpDef()->Coef(term);

      LocalModeNr op_m0 = this->pOpDef()->ModeForFactor(term, I_0);
      LocalModeNr op_m1 = this->pOpDef()->ModeForFactor(term, I_1);
      LocalOperNr oper0 = this->pOpDef()->OperForFactor(term, I_0);
      LocalOperNr oper1 = this->pOpDef()->OperForFactor(term, I_1);

      midasvector_t res_up(n_gcoef, C_0);
      midasvector_t inp_up(I_1);
      ModeCombi mc_empty;
      if (m0 == op_m0)
      {
         inp_up[I_0] = aIntermeds.GetZ(op_m1,oper1) * coef;
         this->ModInts().Contract(aM0, mc_empty, op_m0, oper0, res_up, inp_up, I_1, true);
         aRes += res_up;
      }
      if (m0 == op_m1)
      {
         inp_up[I_0] = aIntermeds.GetZ(op_m0,oper0) * coef;
         this->ModInts().Contract(aM0, mc_empty, op_m1, oper1, res_up, inp_up, I_1, true);
         aRes += res_up;
      }
   }
}

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeLM0TauHp(const ModeCombi& aM0, midasvector_t& aRes) const
{
   LocalModeNr m0 = aM0.Mode(I_0);
   In n_lcoef = this->NModals(m0)-I_1;
   midasvector_t lcoef(n_lcoef);
   this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), aM0.Address());
   In n_act = this->pOpDef()->NactiveTerms(m0);

   for (In i=I_0; i<n_act; ++i)
   {
      In term = this->pOpDef()->TermActive(m0, i);
      value_t fact = this->pOpDef()->Coef(term);   
      for (In i_fac=I_0; i_fac<this->pOpDef()->NfactorsInTerm(term); ++i_fac)
      {
         LocalModeNr op_mode = this->pOpDef()->ModeForFactor(term, i_fac);
         LocalOperNr oper = this->pOpDef()->OperForFactor(term, i_fac);
         fact *= this->ModInts().GetOccElement(op_mode, oper);
      }
      aRes -= fact *lcoef; //
   }
}

/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeLM0HfTau(const ModeCombi& aM0, midasvector_t& aRes) const
{
   LocalModeNr m0 = aM0.Mode(I_0);
   In n_lcoef = this->NModals(m0)-I_1;
   midasvector_t lcoef(n_lcoef);
   this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), aM0.Address());
   In n_act = this->pOpDef()->NactiveTerms(m0);

   for (In i=I_0; i<n_act; ++i)
   {
      In term = this->pOpDef()->TermActive(m0, i);

      // for (In i_r1_fac=I_0; i_r1_fac<this->pOpDef()->NfactorsInTerm(term); ++i_r1_fac)
      // {
      midasvector_t forw_res(n_lcoef,C_0);
      value_t fact = this->pOpDef()->Coef(term);   
      for (In i_fac=I_0; i_fac<this->pOpDef()->NfactorsInTerm(term); ++i_fac)
      {
         LocalModeNr op_mode = this->pOpDef()->ModeForFactor(term, i_fac);
         if (op_mode == m0)
         { 
            //left-forward contraction
            ModeCombi mc_forw(I_1);   
            mc_forw.InsertMode(m0); 
            LocalOperNr oper = this->pOpDef()->OperForFactor(term, i_fac);
            forw_res.Zero();
            this->ModInts().Contract(mc_forw, mc_forw, op_mode, oper, forw_res, lcoef, I_0, true);
         }
         else
         {
            LocalModeNr mode = this->pOpDef()->ModeForFactor(term, i_fac);
            LocalOperNr oper = this->pOpDef()->OperForFactor(term, i_fac);
            fact *= this->ModInts().GetOccElement(mode, oper);
         }
      }
      aRes += fact * forw_res;
      // }
   }
}

/***************************************************************************//**
 * Only H1 part.
 *
 * @note
 *    Not used...? -MBH, Feb 2019.
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeLM0cH1Tau(const ModeCombi& aM0, midasvector_t& aRes) const
{
   LocalModeNr m0 = aM0.Mode(I_0);
   In n_lcoef = this->NModals(m0)-I_1;
   midasvector_t lcoef(n_lcoef);
   this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), aM0.Address());
   In n_act = this->pOpDef()->NactiveTerms(m0);

   for (In i=I_0; i<n_act; ++i)
   {
      In term = this->pOpDef()->TermActive(m0, i);

      if ( this->pOpDef()->NfactorsInTerm(term) == I_1)
      {
         LocalOperNr oper = this->pOpDef()->OperForFactor(term, I_0);
         midasvector_t forw_res(n_lcoef,C_0);
         value_t fact = this->pOpDef()->Coef(term);   

         In op_mode =  m0;
         value_t hpass = this->ModInts().GetOccElement(op_mode, oper);

         //left-forward contraction
         ModeCombi mc_forw(I_1);   
         mc_forw.InsertMode(m0); 
         forw_res.Zero();
         this->ModInts().Contract(mc_forw, mc_forw, op_mode, oper, forw_res, lcoef, I_0, true);
         aRes += fact * (forw_res - hpass*lcoef);
      }
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeLM0cH2Tau(const ModeCombi& aM0, midasvector_t& aRes, const twomodeintermeds_t& aIntermeds ) const
{
   LocalModeNr m0 = aM0.Mode(I_0);
   In n_lcoef = this->NModals(m0)-I_1;
   midasvector_t lcoef(n_lcoef);
   this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), aM0.Address());
   In n_act = this->pOpDef()->NactiveTerms(m0);

   for (In i=I_0; i<n_act; ++i)
   {
      In term = this->pOpDef()->TermActive(m0, i);
      if (this->pOpDef()->NfactorsInTerm(term) != I_2) continue;

      value_t fact = this->pOpDef()->Coef(term);   

      //Lm0Hm0fHm1pTaum0 contribution
      value_t afact = fact;
      midasvector_t forw_res(n_lcoef,C_0);

      LocalModeNr op_m0 = this->pOpDef()->ModeForFactor(term, I_0);
      LocalModeNr op_m1 = this->pOpDef()->ModeForFactor(term, I_1);
      LocalOperNr oper0 = this->pOpDef()->OperForFactor(term, I_0);
      LocalOperNr oper1 = this->pOpDef()->OperForFactor(term, I_1);
      if (op_m0 == m0)
      { 
         //left-forward contraction
         ModeCombi mc_forw(I_1);   
         mc_forw.InsertMode(m0); 
         forw_res.Zero();
         this->ModInts().Contract(mc_forw, mc_forw, op_m0, oper0, forw_res, lcoef, I_0, true);
         afact *= this->ModInts().GetOccElement(op_m1, oper1);
      }
      if (op_m1 == m0)
      { 
         //left-forward contraction
         ModeCombi mc_forw(I_1);   
         mc_forw.InsertMode(m0); 
         forw_res.Zero();
         this->ModInts().Contract(mc_forw, mc_forw, op_m1, oper1, forw_res, lcoef, I_0, true);
         afact *= this->ModInts().GetOccElement(op_m0, oper0);
      }

      aRes += afact * forw_res; 

      //Lm0Taum0Hm0pHm1p contribution
      afact = fact;
      for (In i_fac=I_0; i_fac<this->pOpDef()->NfactorsInTerm(term); ++i_fac)
      {
         LocalModeNr op_mode = this->pOpDef()->ModeForFactor(term, i_fac);
         LocalOperNr oper = this->pOpDef()->OperForFactor(term, i_fac);
         afact *= this->ModInts().GetOccElement(op_mode, oper);
      }
      aRes -= afact *lcoef; //

      //Lm1Hm0dHm1uTaum0 contribution

      midasvector_t res_up(n_lcoef, C_0);
      midasvector_t inp_up(I_1);
      ModeCombi mc_empty;
      if (m0 == op_m0)
      {
         inp_up[I_0] = aIntermeds.GetZ(op_m1,oper1) * fact;
         this->ModInts().Contract(aM0, mc_empty, op_m0, oper0, res_up, inp_up, I_1, true);
         aRes += res_up;
      }
      if (m0 == op_m1)
      {
         inp_up[I_0] = aIntermeds.GetZ(op_m0,oper0) * fact;
         this->ModInts().Contract(aM0, mc_empty, op_m1, oper1, res_up, inp_up, I_1, true);
         aRes += res_up;
      }
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeLHddTTau(const ModeCombi& aM0, midasvector_t& aRes, const twomodeintermeds_t& aIntermeds ) const
{
   LocalModeNr m0 = aM0.Mode(I_0); 
   In n_act = this->pOpDef()->NactiveTerms(m0);
   In n_lcoef = this->NModals(m0)-I_1;
   const ModeCombiOpRange& mcr = this->Mcr();

   for (LocalModeNr m2=I_0; m2<this->pOpDef()->NmodesInOp(); m2++)
   {
      if (m2 == m0)
         continue;

      const ModeCombi& mc2 = mcr.GetModeCombi(m2+I_1);
      In n_lcoef2 = this->NexciForModeCombi(mc2.ModeCombiRefNr()); 
      midasvector_t lcoef(n_lcoef2);
      this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), mc2.Address());

      midasvector_t res_up(n_lcoef,C_0);
      for (In i=I_0; i<n_act; ++i)
      {
         In term = this->pOpDef()->TermActive(m0, i);

         if (this->pOpDef()->NfactorsInTerm(term) == I_1)
            continue; 

         value_t coef = this->pOpDef()->Coef(term);

         LocalModeNr op_m0 = this->pOpDef()->ModeForFactor(term, I_0);
         LocalModeNr op_m1 = this->pOpDef()->ModeForFactor(term, I_1);
         LocalOperNr oper0 = this->pOpDef()->OperForFactor(term, I_0);
         LocalOperNr oper1 = this->pOpDef()->OperForFactor(term, I_1);

         midasvector_t res_down(I_1);
         //ModeCombi aM2;
         ModeCombi mc_empty;
         midasvector_t int_coef = lcoef;
         int_coef  *= coef;
         ModeCombi mc_m0;
         mc_m0.InsertMode(m0);
         midasvector_t x_intermed;
         if ((m0 == op_m0) & (op_m1 != m2))
         {
            //ModeCombi aM2;
            //aM2.InsertMode(m2);
            //aM2.InsertMode(op_m1);

            if (aIntermeds.GetXintermed(m2,op_m1,oper1, x_intermed))  
            {
               In n = this->NModals(m2)-I_1;
               for (In i_mdl = I_0; i_mdl<n; i_mdl++)
               {
                  res_down[I_0] += x_intermed[i_mdl]*int_coef[i_mdl];
               }
            } 

            this->ModInts().Contract(mc_m0, mc_empty, m0, oper0, res_up, res_down, I_1, true);

         }
         if ((m0 == op_m1) & (op_m0 != m2))
         {
            //ModeCombi aM2;
            //aM2.InsertMode(m2);
            //aM2.InsertMode(op_m0);

            if (aIntermeds.GetXintermed(m2,op_m0,oper0, x_intermed))  
            {
               In n = this->NModals(m2)-I_1;
               for (In i_mdl = I_0; i_mdl<n; i_mdl++)
               {
                  res_down[I_0] += x_intermed[i_mdl]*int_coef[i_mdl];
               }
            } 

            this->ModInts().Contract(mc_m0, mc_empty, m0, oper1, res_up, res_down, I_1, true);
         }
      }
      aRes += res_up;
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeLTauHddT(const ModeCombi& aM0, midasvector_t& aRes, const twomodeintermeds_t& aIntermeds) const
{
   In m0 = aM0.Mode(I_0); 
   In n_lcoef = this->NModals(m0)-I_1;
   midasvector_t lcoef(n_lcoef);
   this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), aM0.Address());
   value_t res=C_0;

   for (In m1=I_0; m1<this->pOpDef()->NmodesInOp(); ++m1)
   {
      if (m1 == m0)
         continue;

      res -= aIntermeds.GetY(m0,m1);
   } 
   aRes += res*lcoef;
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeL1Tau2(const ModeCombi& aM0, midasvector_t& aRes) const
{
   In m0 = aM0.Mode(I_0);
   In m1 = aM0.Mode(I_1);

   const ModeCombiOpRange& mcr = this->Mcr();

   for (In m2=I_0; m2<this->pOpDef()->NmodesInOp(); m2++)
   {
      if ((m2 != m0) & (m2 != m1)) continue;

      const ModeCombi& mc2 = mcr.GetModeCombi(m2+I_1);
      In n_lcoef = this->NexciForModeCombi(mc2.ModeCombiRefNr()); 
      midasvector_t lcoef(n_lcoef);

      this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), mc2.Address());

      LocalModeNr m_0 = m2;
      LocalModeNr m_up;
      if (m2 == m0) m_up = m1;
      else   m_up = m0;

      In n_act = this->pOpDef()->NactiveTerms(m_up);

      for (In i_term=I_0; i_term<n_act; ++i_term)
      {
         midasvector_t l_vec = lcoef;
         In term = this->pOpDef()->TermActive(m_up, i_term);
         value_t fact = this->pOpDef()->Coef(term);  
         midasvector_t up_res (aRes.Size(),C_0);

         //       if (this->pOpDef()->NfactorsInTerm(term) != I_1) continue ;
       if (this->pOpDef()->NfactorsInTerm(term) == I_2)
         {
            midasvector_t int_res(n_lcoef,C_0);
            for (In i_fac=I_0; i_fac<this->pOpDef()->NfactorsInTerm(term); ++i_fac)
            {
               LocalModeNr op_mode = this->pOpDef()->ModeForFactor(term, i_fac);

               if (op_mode == m_up) continue;
               if ((op_mode != m_up) & (op_mode == m_0))
               {
                  LocalOperNr oper1 = this->pOpDef()->OperForFactor(term, i_fac);
                  this->ModInts().Contract(mc2, mc2, op_mode, oper1, int_res, l_vec, I_0, true);
                  l_vec=int_res;
               }
               if ((op_mode != m_up) & (op_mode != m_0))
               {
                  LocalOperNr oper1 = this->pOpDef()->OperForFactor(term, i_fac);
                  l_vec *= this->ModInts().GetOccElement(op_mode, oper1);
               }
            }
         }

         LocalOperNr oper = this->pOpDef()->OperForOperMode(term,m_up);
         this->ModInts().Contract(aM0, mc2, m_up, oper, up_res, l_vec, I_1, true);
         aRes += fact * up_res;
      }
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeL2Vcc1pt2(const ModeCombi& aM0, midasvector_t& aRes) const
{ 
   LocalModeNr m0 = aM0.Mode(I_0);

   for (LocalModeNr m1=I_0; m1<this->pOpDef()->NmodesInOp(); m1++)
   {
      if (m1 == m0)
         continue;

      ModeCombi mc01;
      mc01.InsertMode(m0);
      mc01.InsertMode(m1);

      ModeCombiOpRange::const_iterator it_mc;
      if (!this->Mcr().Find(mc01, it_mc))
      {
         continue;
      }
      In addr = it_mc->Address();
      In n_amps = this->NexciForModeCombi(it_mc->ModeCombiRefNr()); 

      midasvector_t lcoef(n_amps);
      this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), addr);

      In n_act = this->pOpDef()->NactiveTerms(m0);
      for (In i=I_0; i<n_act; ++i)
      {
         In term =  this->pOpDef()->TermActive(m0,i);

         if (this->pOpDef()->NfactorsInTerm(term) == I_1)
            continue; 

         value_t coef = this->pOpDef()->Coef(term);
         LocalModeNr op_m0 = this->pOpDef()->ModeForFactor(term, I_0);
         LocalModeNr op_m1 = this->pOpDef()->ModeForFactor(term, I_1);
         LocalOperNr oper0 = this->pOpDef()->OperForFactor(term, I_0);
         LocalOperNr oper1 = this->pOpDef()->OperForFactor(term, I_1);
         ModeCombi mc0;
         mc0.InsertMode(m0);
         midasvector_t inp_down = coef*lcoef;
         midasvector_t res_down(this->NModals(m0)-I_1);
         midasvector_t forw_res(this->NModals(m0)-I_1, C_0);
         midasvector_t int_res(this->NModals(m0)-I_1, C_0);

         if ((op_m0 == m0) & (op_m1 == m1))
         {
            this->ModInts().Contract(mc0, mc01, m1, oper1, res_down, inp_down, -I_1, true);
            this->ModInts().Contract(mc0, mc0, m0, oper0, forw_res, res_down, I_0, true);
            int_res += forw_res;
            //int_res += -res_down*this->ModInts().GetOccElement(m0, oper0);
            int_res -= res_down*this->ModInts().GetOccElement(m0, oper0);
         }
         else if ((op_m0 == m1) & (op_m1 == m0))
         {
            this->ModInts().Contract(mc0, mc01, m1, oper0, res_down, inp_down, -I_1, true);
            this->ModInts().Contract(mc0, mc0, m0, oper1, forw_res, res_down, I_0, true);
            int_res += forw_res;
            //int_res += -res_down*this->ModInts().GetOccElement(m0, oper1);
            int_res -= res_down*this->ModInts().GetOccElement(m0, oper1);
         }
         aRes += int_res;
      }
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeLm0m1THTau1(const ModeCombi& aM0, midasvector_t& aRes) const
{
   LocalModeNr m0 = aM0.Mode(I_0);
   In n_act = this->pOpDef()->NactiveTerms(m0);
   In n_gcoef = this->NModals(m0)-I_1;

   value_t int_res = C_0;
   for (In m1=I_0; m1<this->pOpDef()->NmodesInOp(); m1++)
   {
      if (m1 == m0)
         continue;

      ModeCombi mc01(I_0);
      mc01.InsertMode(m0);
      mc01.InsertMode(m1);

      ModeCombiOpRange::const_iterator it_mc;
      if (!this->Mcr().Find(mc01, it_mc))
      {
         continue;
      }
      In addr = it_mc->Address();
      In n_amps = this->NexciForModeCombi(it_mc->ModeCombiRefNr()); 

      midasvector_t lcoef(n_amps,C_0);
      this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), addr);

      midasvector_t tvec(n_amps,C_0);
      this->TAmps().DataIo(IO_GET, tvec, tvec.Size(), addr);

      if (n_amps != tvec.Size()) MIDASERROR("Error in TwoModeLm0m1THTau1: something wrong with dimension");
      for (In i_ele=I_0; i_ele < n_amps; i_ele++)
      {
         int_res += lcoef[i_ele]*tvec[i_ele]; 
      }
   }
   for (In i=I_0; i < n_act; i++)
   {
      In term = this->pOpDef()->TermActive(m0,i);
      LocalOperNr oper =I_0;
      value_t occ = C_1;
      if (this->pOpDef()->NfactorsInTerm(term) == I_1)
      {
         oper = this->pOpDef()->OperForFactor(term,I_0);
      }
      else if (this->pOpDef()->NfactorsInTerm(term) == I_2) 
      {
         LocalModeNr op_m0 = this->pOpDef()->ModeForFactor(term, I_0);
         LocalModeNr op_m1 = this->pOpDef()->ModeForFactor(term, I_1);
         LocalOperNr oper0 = this->pOpDef()->OperForFactor(term, I_0);
         LocalOperNr oper1 = this->pOpDef()->OperForFactor(term, I_1);
         if (op_m0 == m0)
         {
            occ = this->ModInts().GetOccElement(op_m1,oper1);
            oper = oper0;
         }
         else
         {
            occ = this->ModInts().GetOccElement(op_m0,oper0);
            oper = oper1;
         }
      }
      else 
      {
         MIDASERROR("Three mode terms not allowed");
      }
      value_t fact = this->pOpDef()->Coef(term)*occ;   
      midasvector_t inp_up(I_1,int_res);
      midasvector_t res(n_gcoef, C_0);
      ModeCombi mc_empty;
      this->ModInts().Contract(aM0, mc_empty, m0, oper, res, inp_up, I_1, true);
      aRes -= fact*res;
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeLm1m2THTau1(const ModeCombi& aM0, midasvector_t& aRes) const
{
   LocalModeNr m0 = aM0.Mode(I_0);
   In n_act = this->pOpDef()->NactiveTerms(m0);
   In n_gcoef = this->NModals(m0)-I_1;

   for (In i=I_0; i<n_act; i++)
   {
      In m1;
      In term = this->pOpDef()->TermActive(m0,i);
      value_t fact = this->pOpDef()->Coef(term);   
      In oper0=I_0;
      In oper1=I_0;

      value_t res=C_0;
      if (this->pOpDef()->NfactorsInTerm(term) == I_2)
      {
         LocalModeNr op_mA = this->pOpDef()->ModeForFactor(term, I_0);
         LocalModeNr op_mB = this->pOpDef()->ModeForFactor(term, I_1);
         LocalOperNr operA = this->pOpDef()->OperForFactor(term, I_0);
         LocalOperNr operB = this->pOpDef()->OperForFactor(term, I_1);
         if (op_mA == m0)
         {
            oper1 = operB;
            oper0 = operA;
            m1 = op_mB;
         }
         else
         {
            oper1 = operA;
            oper0 = operB;
            m1 = op_mA;
         }
         value_t pass = this->ModInts().GetOccElement(m1,oper1);
         for (In m2=I_0; m2<this->pOpDef()->NmodesInOp(); m2++)
         {
            if (m2 == m0 || m2 == m1)
               continue;

            ModeCombi mc12;
            mc12.InsertMode(m1);
            mc12.InsertMode(m2);

            ModeCombiOpRange::const_iterator it_mc;
            if (!this->Mcr().Find(mc12, it_mc))
            {
               continue;
            }
            In addr = it_mc->Address();
            In n_amps = this->NexciForModeCombi(it_mc->ModeCombiRefNr()); 

            midasvector_t lcoef(n_amps);
            this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), addr);
            midasvector_t tvec(n_amps);
            this->TAmps().DataIo(IO_GET, tvec, tvec.Size(), addr);

            midasvector_t l_mod(n_amps);
            l_mod -= pass*lcoef; 
            midasvector_t l_forw(n_amps);
            this->ModInts().Contract(mc12, mc12, m1, oper1, l_forw, lcoef, I_0, true);
            l_mod += l_forw;


            In n_ele = lcoef.Size();
            if (n_ele != tvec.Size()) MIDASERROR("Error in TwoModeLm1m2THTau1: something wrong with dimension");
            for (In i_ele=I_0; i_ele < n_ele; i_ele++)
            {
               res += l_mod[i_ele]*tvec[i_ele]; 
            }
         }
         res *= fact;
      }

      midasvector_t inp_up(I_1,res);
      midasvector_t res_up(n_gcoef, C_0);
      ModeCombi mc_empty;
      this->ModInts().Contract(aM0, mc_empty, m0, oper0, res_up, inp_up, I_1, true);
      aRes += res_up;
   }  
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeL2HTau2(const ModeCombi& aM0, midasvector_t& aRes) const
{
  ModeCombiOpRange::const_iterator it_mc;
  if (!this->Mcr().Find(aM0, it_mc))
     return;
  In addr = it_mc->Address();
  In n_amps = aRes.Size(); 

  midasvector_t lcoef(n_amps);
  this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), addr);

  for (In i_m=I_0; i_m < aM0.Size(); i_m++)
  {
     LocalModeNr m0 = aM0.Mode(i_m); 
     In n_act = this->pOpDef()->NactiveTerms(m0);
     for (In i=I_0; i<n_act; ++i)
     {
        In term =  this->pOpDef()->TermActive(m0,i);
        value_t coef = this->pOpDef()->Coef(term);

        if (this->pOpDef()->NfactorsInTerm(term) != I_1) continue;
         {
           LocalModeNr op_mode = this->pOpDef()->ModeForFactor(term, I_0);
           LocalOperNr oper = this->pOpDef()->OperForFactor(term, I_0);
           midasvector_t l_forw(n_amps,C_0);
           midasvector_t l_mod(lcoef);
           l_mod *=coef; 
           this->ModInts().Contract(aM0, aM0, op_mode, oper, l_forw, l_mod, I_0, true);
           aRes += l_forw;
           l_mod *= this->ModInts().GetOccElement(op_mode,oper);
           aRes -= l_mod;
           }
     }
  }
  
  LocalModeNr m0 = aM0.Mode(I_0);
  LocalModeNr m1 = aM0.Mode(I_1);
  In n_act = this->pOpDef()->NactiveTerms(m0);
  for (In i=I_0; i<n_act; ++i)
  {
     In term =  this->pOpDef()->TermActive(m0,i);
     value_t coef = this->pOpDef()->Coef(term);
     if (this->pOpDef()->NfactorsInTerm(term) != I_2) continue;

     LocalModeNr op_ma = this->pOpDef()->ModeForFactor(term, I_0);
     LocalOperNr opera = this->pOpDef()->OperForFactor(term, I_0);
     LocalModeNr op_mb = this->pOpDef()->ModeForFactor(term, I_1);
     LocalOperNr operb = this->pOpDef()->OperForFactor(term, I_1);

     value_t pass_a = this->ModInts().GetOccElement(op_ma,opera);
     value_t pass_b = this->ModInts().GetOccElement(op_mb,operb);
     midasvector_t l_mod(lcoef);
     l_mod *=coef; 

     if ((op_ma == m0 && op_mb == m1) || (op_ma == m1 && op_mb == m0))
     { 
     midasvector_t l_forw_a(n_amps,C_0);
     midasvector_t l_forw_b(n_amps,C_0);
     midasvector_t l_forw2(n_amps,C_0);


     this->ModInts().Contract(aM0, aM0, op_ma, opera, l_forw_a, l_mod, I_0, true);
     this->ModInts().Contract(aM0, aM0, op_mb, operb, l_forw2, l_forw_a, I_0, true);
     aRes += l_forw2;

     aRes -= l_mod*(pass_a*pass_b);
     
     }
    else if (op_ma != m1 && op_ma != m0) 
     {
     midasvector_t l_forw_0(n_amps,C_0);
     this->ModInts().Contract(aM0, aM0, m0, operb, l_forw_0, l_mod, I_0, true);
     value_t pass = this->ModInts().GetOccElement(op_ma,opera);
     aRes += l_forw_0*pass; 
     aRes -= l_mod*(pass*pass_b);
     } 
     else if  (op_mb != m0 && op_mb != m1)
     {
     midasvector_t l_forw_0(n_amps,C_0);
     this->ModInts().Contract(aM0, aM0, m0, opera, l_forw_0, l_mod, I_0, true);
     value_t pass = this->ModInts().GetOccElement(op_mb,operb);
     aRes += l_forw_0*pass; 
     aRes -= l_mod*(pass*pass_a);
     }
  }  
  n_act = this->pOpDef()->NactiveTerms(m1);
  for (In i=I_0; i<n_act; ++i)
  {
     In term =  this->pOpDef()->TermActive(m1,i);
     value_t coef = this->pOpDef()->Coef(term);
     if (this->pOpDef()->NfactorsInTerm(term) != I_2) continue;
     LocalModeNr op_ma = this->pOpDef()->ModeForFactor(term, I_0);
     LocalOperNr opera = this->pOpDef()->OperForFactor(term, I_0);
     LocalModeNr op_mb = this->pOpDef()->ModeForFactor(term, I_1);
     LocalOperNr operb = this->pOpDef()->OperForFactor(term, I_1);

     value_t pass_a = this->ModInts().GetOccElement(op_ma,opera);
     value_t pass_b = this->ModInts().GetOccElement(op_mb,operb);

     if (op_ma == m0 || op_mb == m0) continue;
     midasvector_t l_mod(lcoef);
     l_mod *=coef; 

     if (op_ma != m1) 
     {
     midasvector_t l_forw_1(n_amps,C_0);
     this->ModInts().Contract(aM0, aM0, m1, operb, l_forw_1, l_mod, I_0, true);
     value_t pass = this->ModInts().GetOccElement(op_ma,opera);
     aRes += l_forw_1*pass; 
     aRes -= l_mod*(pass*pass_b);
     } 
     else if  (op_mb != m1)
     {
     midasvector_t l_forw_1(n_amps,C_0);
     this->ModInts().Contract(aM0, aM0, m1, opera, l_forw_1, l_mod, I_0, true);
     value_t pass = this->ModInts().GetOccElement(op_mb,operb);
     aRes += l_forw_1*pass; 
     aRes -= l_mod*(pass*pass_a);
     }

  }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeL2THTau2(const ModeCombi& aMc, midasvector_t& aRes) const
{
   value_t res = C_0;
   for (In i_m=I_0; i_m < aMc.Size(); i_m++)
   {
      In m0 = aMc.Mode(i_m); 
      for (In m2=I_0; m2<this->pOpDef()->NmodesInOp(); m2++)
      {
         if (m2 == m0)
            continue;

         ModeCombi mc02;
         mc02.InsertMode(m0);
         mc02.InsertMode(m2);

         ModeCombiOpRange::const_iterator it_mc;
         if (!this->Mcr().Find(mc02, it_mc))
         {
            continue;
         }
         In addr = it_mc->Address();
         In n_amps = this->NexciForModeCombi(it_mc->ModeCombiRefNr()); 

         midasvector_t lcoef(n_amps);
         this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), addr);
         midasvector_t tvec(n_amps);
         this->TAmps().DataIo(IO_GET, tvec, tvec.Size(), addr);

         In n_ele = lcoef.Size();
         if (n_ele != tvec.Size()) MIDASERROR("Error in TwoModeL2THTau2: something wrong with dimension");
         for (In i_ele=I_0; i_ele < n_ele; i_ele++)
         {
            res -= lcoef[i_ele]*tvec[i_ele]; 
         }
      }
   }

   ModeCombiOpRange::const_iterator it_mc;
   if (!this->Mcr().Find(aMc, it_mc))
      return;
   In addr = it_mc->Address();
   In n_amps = this->NexciForModeCombi(it_mc->ModeCombiRefNr()); 
   midasvector_t lcoef(n_amps);
   this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), addr);
   midasvector_t tvec(n_amps);
   this->TAmps().DataIo(IO_GET, tvec, tvec.Size(), addr);

   In n_ele = lcoef.Size();
   value_t sub = C_0;

   if (n_ele != tvec.Size()) MIDASERROR("Error in TwoModeL2THTau2: something wrong with dimension");
   for (In i_ele=I_0; i_ele < n_ele; i_ele++)
   {
      sub += lcoef[i_ele]*tvec[i_ele]; 
   }
   res += sub;

   In i_first_term = I_0;
   In n_terms = I_0;
   if (!this->pOpDef()->TermsForMc(aMc, i_first_term, n_terms)) return;
   for (In i = I_0 ; i < n_terms; i++)
   {
      In i_term = i_first_term + i;
      LocalModeNr mode0 = this->pOpDef()->ModeForFactor(i_term,I_0);
      LocalOperNr oper0 = this->pOpDef()->OperForFactor(i_term,I_0);
      LocalModeNr mode1 = this->pOpDef()->ModeForFactor(i_term,I_1);
      LocalOperNr oper1 = this->pOpDef()->OperForFactor(i_term,I_1);
      value_t fact=this->pOpDef()->Coef(i_term);
      value_t int_res = res * fact;
      ModeCombi mc_empty;
      ModeCombi aM1;
      aM1.InsertMode(mode1);
      midasvector_t inp_up(I_1,int_res);
      midasvector_t int_up(this->NModals(mode1)-I_1, C_0);
      midasvector_t res_up(n_amps,C_0);
      this->ModInts().Contract(aM1, mc_empty, mode1, oper1, int_up, inp_up, I_1, true);
      this->ModInts().Contract(aMc, aM1, mode0, oper0, res_up, int_up, I_1, true);
      aRes += res_up;
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeL2Tau2HT(const ModeCombi& aMc, midasvector_t& aRes, const twomodeintermeds_t& aIntermeds) const
{
   In m0 = aMc.Mode(I_0);
   In m1 = aMc.Mode(I_1);
   
   ModeCombiOpRange::const_iterator it_mc;
   if (!this->Mcr().Find(aMc, it_mc))
      return;
   In addr = it_mc->Address();
   In n_amps = this->NexciForModeCombi(it_mc->ModeCombiRefNr()); 
   midasvector_t lcoef(n_amps);
   this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), addr);

   value_t y_sum = C_0;
   for (In m2=I_0; m2<this->pOpDef()->NmodesInOp(); ++m2)
   {
      if (m2 == m0) // If m2 == m1, we get the Y[m0][m1] contribution which is ok.
         continue;
      y_sum -= aIntermeds.GetY(m0,m2);
      y_sum -= aIntermeds.GetY(m1,m2);
   }
   aRes += y_sum * lcoef;
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeLm1m2HTau2(const ModeCombi& aM0, midasvector_t& aRes) const
{
   for (In i_m=I_0; i_m < aM0.Size(); i_m++)
   {
      LocalModeNr m0 = aM0.Mode(i_m); 
      ModeCombi aM1 = aM0;
      aM1.RemoveMode(m0);
      LocalModeNr m1 = aM1.Mode(I_0);
      In n_act = this->pOpDef()->NactiveTerms(m0);


      for (In i=I_0; i<n_act; ++i)
      {

         In term =  this->pOpDef()->TermActive(m0,i);

         if (this->pOpDef()->NfactorsInTerm(term) == I_1)
            continue; 

         LocalModeNr op_ma = this->pOpDef()->ModeForFactor(term, I_0);
         LocalModeNr op_mb = this->pOpDef()->ModeForFactor(term, I_1);
         LocalOperNr opera = this->pOpDef()->OperForFactor(term, I_0);
         LocalOperNr operb = this->pOpDef()->OperForFactor(term, I_1);
         In m2 = I_0;
         In oper0 = I_0;
         In oper2 = I_0;

         if (op_ma != m0 && op_ma != m1)
         {
            m2 = op_ma;
            oper2 = opera; 
            oper0 = operb;
         }
         else if (op_mb != m0 && op_mb != m1)
         {
            m2 = op_mb;
            oper2 = operb; 
            oper0 = opera;
         }
         else continue; 

         ModeCombiOpRange::const_iterator it_mc12;
         if (!this->Mcr().Find(ModeCombi(std::set<In>{m1,m2},-1), it_mc12))
         {
            continue;
         }
         const auto& mc12 = *it_mc12;
         const In addr = mc12.Address();
         const In n_amps_mc12 = this->NexciForModeCombi(mc12.ModeCombiRefNr()); 
         const In n_amps_aM0 = this->NexciForModeCombi(aM0.ModeCombiRefNr());

         midasvector_t lcoef(n_amps_mc12);
         this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), addr);

         const ModeCombi mc1(std::set<In>{m1},-1);
         lcoef *= this->pOpDef()->Coef(term);
         midasvector_t res_down(this->NModals(m1)-I_1,C_0);
         midasvector_t res_up(n_amps_aM0,C_0);
         this->ModInts().Contract(mc1, mc12, m2, oper2, res_down, lcoef, -I_1, true);
         this->ModInts().Contract(aM0, mc1, m0, oper0, res_up, res_down, I_1, true);
         aRes += res_up;
      }
   }   
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeL2H1XTau1(const ModeCombi& aM0, midasvector_t& aRes, const twomodeintermeds_t& aIntermeds) const
{
   LocalModeNr m0 = aM0.Mode(I_0); 
   In n_act = this->pOpDef()->NactiveTerms(m0);
   In n_gcoef = this->NModals(m0)-I_1;
   LocalOperNr oper0 = I_0;

   for (LocalModeNr m2=I_0; m2<this->pOpDef()->NmodesInOp(); m2++)
   {
      if (m2 == m0)
         continue;

      ModeCombi mc02;
      mc02.InsertMode(m0);
      mc02.InsertMode(m2);

      ModeCombiOpRange::const_iterator it_mc;
      if (!this->Mcr().Find(mc02, it_mc))
      {
         continue;
      }
      In addr = it_mc->Address();
      In n_amps = this->NexciForModeCombi(it_mc->ModeCombiRefNr()); 

      midasvector_t lcoef(n_amps,C_0);
      this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), addr);

      for (In i=I_0; i<n_act; ++i)
      {
         In term = this->pOpDef()->TermActive(m0, i);
         midasvector_t x_intermed(n_gcoef,C_0);
         if (this->pOpDef()->NfactorsInTerm(term) == I_1)
         {
            value_t coef = this->pOpDef()->Coef(term);
            oper0 = this->pOpDef()->OperForFactor(term, I_0);
            if (aIntermeds.GetXintermed(m2,m0,oper0,x_intermed))
            {
               midasvector_t res1(n_gcoef,C_0);
               LXContractor(lcoef,mc02,m2,x_intermed,res1,aM0);
               res1 *=coef;
               aRes -= res1;
            }
         }
         else if (this->pOpDef()->NfactorsInTerm(term) == I_2) 
         {
            In oper1=I_0;
            In m1=I_0; 

            value_t coef = this->pOpDef()->Coef(term);
            LocalModeNr op_ma = this->pOpDef()->ModeForFactor(term, I_0);
            LocalModeNr op_mb = this->pOpDef()->ModeForFactor(term, I_1);
            LocalOperNr opera = this->pOpDef()->OperForFactor(term, I_0);
            LocalOperNr operb = this->pOpDef()->OperForFactor(term, I_1);

            if (op_ma == m0) 
            {
               oper0 = opera;
               oper1 = operb;
               m1 = op_mb; 
            }
            else if (op_mb == m0) 
            {
               oper0 = operb;
               oper1 = opera;
               m1 = op_ma; 
            }
            if (m1 != m2)
            {
               if (aIntermeds.GetXintermed(m2,m1,oper1, x_intermed))  
               {
                  midasvector_t res1(n_gcoef,C_0);
                  midasvector_t forw_res(n_gcoef,C_0);
                  LXContractor(lcoef, mc02,m2,x_intermed,res1,aM0); 
                  res1 *=coef;
                  value_t pass = this->ModInts().GetOccElement(m0,oper0);
                  aRes -= pass*res1; 
                  this->ModInts().Contract(aM0, aM0, m0, oper0, forw_res, res1, I_0, true);
                  aRes += forw_res; 
               } 
               if (aIntermeds.GetXintermed(m2,m0,oper0, x_intermed))
               {
                  midasvector_t res2(n_gcoef,C_0);
                  LXContractor(lcoef, mc02,m2,x_intermed,res2,aM0); 
                  res2 *=coef;
                  res2 *= this->ModInts().GetOccElement(m1,oper1);
                  aRes -= res2; 
               }
            }
            else if (m1== m2) 
            {
               if (aIntermeds.GetXintermed(m2,m0,oper0, x_intermed))
               {
                  midasvector_t l_forw(n_amps,C_0);
                  //da cambiare: prima contrai X e h, poi con L
                  this->ModInts().Contract(mc02, mc02, m2, oper1, l_forw, lcoef, I_0, true);
                  midasvector_t res2(n_gcoef,C_0);
                  LXContractor(l_forw, mc02,m2,x_intermed,res2,aM0); 
                  res2 *=coef;
                  aRes -= res2;
               }

            }
         }
      }
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::TwoModeL2HTTau2(const ModeCombi& aMc, midasvector_t& aRes, const twomodeintermeds_t& aIntermeds) const
{
   for (In i_m=I_0; i_m < aMc.Size(); i_m++)
   {
      In m0 = aMc.Mode(i_m); 
      ModeCombi aM0 = aMc;
      ModeCombi aM1 = aMc;
      aM1.RemoveMode(m0);
      In m1 = aM1.Mode(I_0);
      aM0.RemoveMode(m1);

      for (In m3=I_0; m3<this->pOpDef()->NmodesInOp(); ++m3)
      {

         if (m3 == m0 || m3 == m1) continue;
         ModeCombi mc03;
         mc03.InsertMode(m0); 
         mc03.InsertMode(m3); 

         ModeCombiOpRange::const_iterator it_mc;
         if (!this->Mcr().Find(mc03, it_mc))
         {
            continue;
         }
         In addr = it_mc->Address();
         In n_amps = this->NexciForModeCombi(it_mc->ModeCombiRefNr()); 
         midasvector_t lcoef(n_amps);
         this->LCoefs().DataIo(IO_GET, lcoef, lcoef.Size(), addr);

         midasvector_t int_res(aRes.Size(),C_0);

         const std::list<std::pair<In, midasvector_t*> >& xc_list = aIntermeds.GetXCintermeds(m3, m1);
         for (auto it=xc_list.begin(); it!=xc_list.end(); ++it)
         {
            In oper1= it->first;
            midasvector_t res_up (aRes.Size(),C_0);
            midasvector_t inp_up(this->NModals(m0)-I_1,C_0);
            midasvector_t xc_vec(this->NModals(m3)-I_1,C_0);
            //midasvector_t l_mod=lcoef;
            for (In i = I_0 ; i < xc_vec.Size(); i++) xc_vec[i]= (*(it->second))[i];
            LXContractor(lcoef, mc03, m3, xc_vec, inp_up, aM0); 
            this->ModInts().Contract(aMc, aM0, m1, oper1, res_up, inp_up, I_1, true);
            int_res += res_up;
         }
         aRes += int_res;
         int_res.Zero(); 

         In i_first_term = I_0;
         In n_terms = I_0;
         if (!this->pOpDef()->TermsForMc(aMc, i_first_term, n_terms)) return;
         for (In i = I_0 ; i < n_terms; i++)
         {
            In i_term = i_first_term + i;
            LocalModeNr op_ma = this->pOpDef()->ModeForFactor(i_term,I_0);
            LocalOperNr opera = this->pOpDef()->OperForFactor(i_term,I_0);
            LocalOperNr operb = this->pOpDef()->OperForFactor(i_term,I_1);
            value_t fact=this->pOpDef()->Coef(i_term);
            midasvector_t l_mod = lcoef*fact;

            In oper0 = I_0;
            In oper1 = I_0;

            if (op_ma == m0)
            {
               oper0 = opera;
               oper1 = operb;
            }
            else
            {
               oper0 = operb;
               oper1 = opera;
            }
            midasvector_t x_intermed(this->NModals(m3)-I_1,C_0);
            if (aIntermeds.GetXintermed(m3,m0,oper0, x_intermed))  
            {
               midasvector_t inp_up(this->NModals(m0)-I_1,C_0);
               midasvector_t res_up (aRes.Size(),C_0);
               LXContractor(l_mod, mc03, m3, x_intermed, inp_up, aM0); 
               this->ModInts().Contract(aMc, aM0, m1, oper1, res_up, inp_up, I_1, true);
               int_res += res_up;
            } 
         }
         aRes -= int_res;
      }
   }
}


/***************************************************************************//**
 *       
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::SanityCheck
   (
   )  const
{
   std::stringstream ss;

   // Check modal integrals type.
   this->CheckTransType(this->ModInts(), modalintegrals_t::TransT::T1, "T1");

   // Check that sizes of T-amps./L-coefs. container match.
   Uin tamps_size = this->TAmps().Size();
   Uin lcoef_size = this->LCoefs().Size();
   if (tamps_size != lcoef_size)
   {
      ss << "T-amps. container size (= " << tamps_size
         << ") != L-coef. container size (= " << lcoef_size
         << ")."
         ;
      MIDASERROR(ss.str());
   }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
void Vcc2ContribsLJac<T>::LXContractor(const midasvector_t& aLvec, const ModeCombi& aL_mc, const In a_m, const midasvector_t& aXint, midasvector_t& aRes, const ModeCombi& aR_mc) const
{
   In ma = aL_mc.Mode(I_0);
   In mb = aL_mc.Mode(I_1);
   In mf = aR_mc.Mode(I_0);
   In na = this->NModals(ma)-I_1;
   In nb = this->NModals(mb)-I_1;
   In* k = NULL;
   In* l = NULL;
   In i = I_0;
   In j = I_0;

   if (ma == a_m && mb == mf)
   {
      k = &j;
      l = &i;
   }
   else if (mb == a_m && ma == mf)
   {
      k = &i;
      l = &j;
   } 
   else MIDASERROR("Error in LXContractor: something wrong with modes");
   for (i=I_0; i < na; i++ ) 
   {
      for (j=I_0; j < nb; j++)
      {
         aRes[*k] += aLvec[i*nb+j]*aXint[*l]; 
      } 
   }
}

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2CONTRIBSLJAC_IMPL_H_INCLUDED*/
