/**
 *******************************************************************************
 * 
 * @file    Vcc2ContribsEta_Decl.h
 * @date    04-02-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refact., templ.
 *
 * @brief
 *    VCC[2]/H2 contributions. Refactored out of the VccTransformer class.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2CONTRIBSETA_DECL_H_INCLUDED
#define VCC2CONTRIBSETA_DECL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "vcc/vcc2/Vcc2ContribsBase.h"

// Forward declarations.
template<typename> class GeneralMidasVector;
class ModeCombi;

namespace midas::vcc::vcc2
{
   /************************************************************************//**
    * @brief
    *    Handles computation of the individual contributions in VCC[2]/H2
    *    transformations.
    *
    * This specific class is for computing the \f$ \eta \f$-vector, with
    * elements
    * \f[
    *    \eta_\mu = \langle \Phi \vert \widetilde{H} \tau_\mu \exp(T) \vert \Phi \rangle
    * \f]
    * In VCC[2]/H2 the cluster operator \f$ T = T_2 \f$ (because the modal
    * integrals of have been T1-transformed); since \f$ \widetilde{H} \f$ can at
    * most down-contract in two modes, the exponential in \f$ \tau_\mu
    * \exp(T_2) \f$ reduces to the identity, and therefore this class doesn't
    * depend on the \f$ T_2 \f$-amplitudes, and the \f$ T_1 \f$-amplitudes are
    * already incorporated in the modal integrals.
    *
    * See these papers for the theoretical details:
    * - [Computation of expectation values from vibrational coupled-cluster at
    * the two-mode coupling level](https://doi.org/10.1063/1.3560027)
    * - [Towards fast computations of correlated vibrational wave functions: Vibrational coupled cluster 
    * response excitation energies at the two-mode coupling level](https://doi.org/10.1063/1.2907860)
    *
    * This class hierarchy re-organizes the methods for individual VCC[2]
    * contributions, preivously found in the VccTransformer class, and
    * templates the methods.
    *
    * The aim is to only provide the necessary methods and members to the
    * derived classes, according to which type of contributions they are
    * intended to compute.
    * Furthermore, there is some additional internal consistency checks for the
    * classes, and they are made rather rigid to avoid manipulations after
    * construction that would cause inconsistent results for different
    * contributions.
    *
    * @note
    *    To a large extent, the class members are const-refs/pointers to
    *    external objects, which is (of course) to save memory and allocation
    *    time. Take care not to invalidate these externally, and not to cause
    *    any pointers/references to dangle.
    ***************************************************************************/
   template
      <  typename T
      >
   class Vcc2ContribsEta
      :  public Vcc2ContribsBase<T>
   {
      public:
         using typename Vcc2ContribsBase<T>::value_t;
         using typename Vcc2ContribsBase<T>::modalintegrals_t;
         using typename Vcc2ContribsBase<T>::opdef_t;
         using midasvector_t = GeneralMidasVector<value_t>;

         Vcc2ContribsEta(const Vcc2ContribsEta&) = delete;
         Vcc2ContribsEta& operator=(const Vcc2ContribsEta&) = delete;
         Vcc2ContribsEta(Vcc2ContribsEta&&) = default;
         Vcc2ContribsEta& operator=(Vcc2ContribsEta&&) = default;
         ~Vcc2ContribsEta() override = default;

         //! Only needs the operator and associated modal integrals.
         Vcc2ContribsEta
            (  const opdef_t* const apOpDef
            ,  const modalintegrals_t& arModInts
            );

         /******************************************************************//**
          * @name Contributions
          *********************************************************************/
         //!@{
         void TwoModeEta1(const ModeCombi& aM0, midasvector_t& aRes) const;
         void TwoModeEta2(const ModeCombi& aM0, midasvector_t& aRes) const;
         //!@}

      private:
         //! Internal consistency of container sizes and such.
         void SanityCheck() const;
   };

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2CONTRIBSETA_DECL_H_INCLUDED*/
