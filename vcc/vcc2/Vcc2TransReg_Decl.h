/**
 *******************************************************************************
 * 
 * @file    Vcc2TransReg_Decl.h
 * @date    11-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef VCC2TRANSREG_DECL_H_INCLUDED
#define VCC2TRANSREG_DECL_H_INCLUDED

// Midas headers.
#include "vcc/vcc2/Vcc2TransBase.h"

// Forward declarations.
namespace midas::vcc::vcc2
{
   template<typename> class Vcc2ContribsReg;
} /* namespace midas::vcc::vcc2 */

namespace midas::vcc::vcc2
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template
      <  typename T
      >
   class Vcc2TransReg final
      :  public Vcc2TransBase<T>
   {
      public:
         using typename Vcc2TransBase<T>::value_t;
         using typename Vcc2TransBase<T>::absval_t;
         using typename Vcc2TransBase<T>::energy_t;
         using typename Vcc2TransBase<T>::opdef_t;
         using typename Vcc2TransBase<T>::modalintegrals_t;
         using typename Vcc2TransBase<T>::midasvector_t;
         using typename Vcc2TransBase<T>::datacont_t;

         Vcc2TransReg(const Vcc2TransReg&) = delete;
         Vcc2TransReg& operator=(const Vcc2TransReg&) = delete;
         Vcc2TransReg(Vcc2TransReg&&) = default;
         Vcc2TransReg& operator=(Vcc2TransReg&&) = default;
         ~Vcc2TransReg() override = default;

         Vcc2TransReg
            (  const opdef_t* const apOpDef
            ,  const modalintegrals_t& arRawModInts
            ,  const ModeCombiOpRange& arMcr
            ,  const bool aOnlyOneMode = false
            );

         bool& DoingVcc() {return mDoingVcc;}
         bool DoingVcc() const {return mDoingVcc;}

      private:
         using vcc2contribsreg_t = midas::vcc::vcc2::Vcc2ContribsReg<value_t>;
         using typename Vcc2TransBase<T>::intermeds_t;
         using typename Vcc2TransBase<T>::fclock_t;
         using typename Vcc2TransBase<T>::fscr_t;

         bool mDoingVcc = true;

         std::string PrettyClassName() const override;
         std::string PrettyTransformerType() const override;

         //!
         void TransformImpl(const datacont_t& arIn, datacont_t& arOut) const override;

         void TwoModeTransformerImpl
            (  const vcc2contribsreg_t& arVcc2Contribs
            ,  intermeds_t& arIntermeds
            ,  const datacont_t& arIn
            ,  datacont_t& arOut
            )  const;
         void OneModeTransformerImpl
            (  const vcc2contribsreg_t& arVcc2Contribs
            ,  intermeds_t& arIntermeds
            ,  const datacont_t& arIn
            ,  datacont_t& arOut
            )  const;
   };

} /* namespace midas::vcc::vcc2 */

#endif/*VCC2TRANSREG_DECL_H_INCLUDED*/
