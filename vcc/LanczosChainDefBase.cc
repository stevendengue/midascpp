/**
************************************************************************
* 
* @file                LanczosChainDefBase.cc
*
* Created:             12-01-2011
*
* Author:              Ian H. Godtliebsen  (mrgodtliebsen@hotmail.com)
*
* Short Description:   Functions for Lanczos chain base definition.
* 
* Last modified: Wed Jan 12, 2011  14:31PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include "vcc/LanczosChainDefBase.h"
#include "vcc/LanczosChain.h"
#include "vcc/NHermLanczosChain.h"
#include "vcc/BandLanczosChain.h"
#include "vcc/NHBandLanczosChain.h"

LanczosChainDefBase* LanczosChainDefBase::Factory(const bool aNHerm, const string aOper2, const In aChainLen, const In aOrthoChain)
{
   if(aNHerm)
   {
      return new NHermLanczosChainDef(aOper2, aChainLen, aOrthoChain);
   }
   else
   {
      return new LanczosChainDef(aOper2, aChainLen, aOrthoChain);
   }

   return NULL;
}

LanczosChainDefBase* LanczosChainDefBase::Factory(const bool aNHerm, const In aChainLen, 
             const In aBlockSize, const In aOrthoChain, const bool aSaveChain, int aSaveInterval, const bool aTestChain,
             const Nb aDefThresh, const In aLBlockSize, const bool aDeflate, const Nb aConvThresh, 
             const Nb aConvVal)
{
   if(aNHerm)
   {
      return new NHBandLanczosChainDef(aChainLen, aBlockSize, aLBlockSize, aOrthoChain, aSaveChain, aSaveInterval, aDefThresh, C_0, aDeflate);
   }
   else
   {
      return new BandLanczosChainDef(aChainLen, aBlockSize, aOrthoChain, aSaveChain, aTestChain, aDefThresh, aConvThresh, aConvVal, aDeflate);
   }
   
   return NULL;
}

