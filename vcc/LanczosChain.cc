/**
************************************************************************
* 
* @file                LanczosChain.cc
*
* Created:             11-08-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*
* Short Description:   Implementation of LanczosChainDef and
*                      LanczosChain classes.
* 
* Last modified: Mon Jan 18, 2010  02:05PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <string>
#include <algorithm>
#include <ctime>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Io.h"
#include "util/MidasStream.h"
#include "util/Timer.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "vcc/LanczosRspFunc.h"
#include "vcc/LanczosChain.h"
#include "input/Input.h"
#include "input/OpDef.h"
#include "input/GlobalOperatorDefinitions.h"

using namespace std;

LanczosChainDef::LanczosChainDef(const string& aOper, In aLength, In aOrtho,
                                 vector<string>* const aXopers):
   LanczosChainDefBase(aLength,aOrtho), mOper(aOper)//, mLength(aLength), mOrtho(aOrtho)
{
   if (aXopers != NULL)
      mXopers = *aXopers;
}


bool LanczosChainDef::Combine(const LanczosChainDef& aOther)
{
   if (mOper != aOther.mOper || mOrtho != aOther.mOrtho)
      return false;

   mLength = max(mLength, aOther.mLength);
   copy(aOther.mXopers.begin(), aOther.mXopers.end(), back_inserter(mXopers));
   sort(mXopers.begin(), mXopers.end());
   mXopers.erase(unique(mXopers.begin(), mXopers.end()), mXopers.end());
   return true;
}

bool LanczosChainDef::Compatible(const LanczosChainDef& aOther) const
{
   if (mOper != aOther.mOper ||
       mOrtho != aOther.mOrtho ||
       mLength < aOther.mLength)
      return false;

   for (In i=I_0; i<aOther.mXopers.size(); ++i)
      if (find(mXopers.begin(), mXopers.end(), aOther.mXopers[i]) == mXopers.end())
         return false;

   return true;
}

ostream& operator<<(ostream& aOut, const LanczosChainDef& aDef)
{
   //aOut << "oper: '" << aDef.mOper << "' length: " << aDef.mLength << " x-opers: ";
   aOut << "oper: '" << aDef.GetOper() << "' length: " << aDef.GetLength() << " x-opers: ";
   for (In i=I_0; i<aDef.mXopers.size()-I_1; ++i)
      aOut << "'" << aDef.mXopers[i] << "',";
   aOut << "'" << aDef.mXopers.back() << "'";
   aOut << " ortho: " << LanczosRspFunc::StringForConst(aDef.mOrtho);
   return aOut;
}

LanczosChain::LanczosChain(const LanczosChainDef& aDef, Vcc* apVcc, VccTransformer* apTrf):
   mDef(aDef), mCalcOrtho(false),
   mpVcc(apVcc), mpTrf(apTrf)
{
   mFilePrefix = "lcschn_" + mDef.GetOper() += "_ortho" + std::to_string(mDef.GetOrtho());
}

void LanczosChain::Evaluate()
{
   Mout << endl;
   Mout << " Evaluating chain with defintion: " << mDef << endl;

   In       nc = mpTrf->NexciXvec()-I_1;                        // Number of response parameters.
   DataCont qj(nc, C_0, "InMem", mFilePrefix+"_qj", true);     // q(j).
   DataCont qjm1(nc, C_0, "InMem", mFilePrefix+"_qjm1", true); // q(j-1).
   DataCont A_qj(nc);                                          // A*q(j).
   DataCont r(nc);                                             // Intermediate vector;
   Nb       dummy = C_0;                                       // For transformer.
   In       init_chainidx = I_0;                               // Initial chain length. May be
                                                               // non-zero if we restart.
   Timer    time_lanc;

   // Set size of vectors containing the Lanczos chain.
   mTalpha.SetNewSize(mDef.GetLength());
   mTbeta.SetNewSize(mDef.GetLength());
   mX.resize(mDef.NXopers());
   for (In i=I_0; i<mX.size(); ++i)
      mX[i].SetNewSize(mDef.GetLength());
   
   mEtaNorm = CalcEta(mDef.GetOper(), qjm1);
   CalcXetas();
   mpTrf->SetType(TRANSFORMER::VCIRSP);
   if (mpVcc->pVccCalcDef()->RspRestart() && Restart(init_chainidx, r, qjm1))
   {
      Mout << " Lanczos response chain: Restarted using old data." << endl
           << " Current chain length: " << init_chainidx << endl;
   }
   else
   {
      // In the loop below we start with j=1 so this is the initial guess, q(0) = eta.
      //if (mCalcOrtho || mDef.GetOrtho()!=LANCZOS_ORTHO_NONE)
      //{
         qjm1.NewLabel(mFilePrefix+"_q0");
         qjm1.ChangeStorageTo("OnDisc");
      //}
      qjm1.NewLabel(mFilePrefix+"_qjm1", true, true);   // qjm1 here corresponds to q(0).
      
      // Calculate alpha(0) = q(0) * A * q(0).
      mpTrf->Transform(qjm1, A_qj, I_1, I_0, dummy);
      mTalpha[I_0] = Dot(qjm1, A_qj);
      
      for (In i=I_0; i<mX.size(); ++i)
         mX[i][I_0] = Dot(mXetas[i], qjm1);

      // Calculate r = (A-alpha(0)) * q(0) and beta(0)
      r = A_qj;
      r.Axpy(qjm1, -mTalpha[I_0]);
      mTbeta[I_0] = r.Norm();
      
      WriteToFile(I_0, ios_base::trunc);
      init_chainidx = I_1;
   }
   
   for (In j=init_chainidx; j<mDef.GetLength(); ++j)
   {
      time_t t0 = time(NULL);
      
      // Calculate q(j) and alpha(j).
      qj = r;
      qj.Scale(C_1/mTbeta[j-I_1]);
      dummy = C_0;
      mpTrf->Transform(qj, A_qj, I_1, I_0, dummy);
      mTalpha[j] = Dot(qj, A_qj);
      for (In k=I_0; k<mX.size(); ++k)
         mX[k][j] = Dot(mXetas[k], qj);
      
      // Calculate r = A*q(j) - alpha(j)*q(j) - beta(j-1)*q(j-1). 
      r = A_qj;
      r.Axpy(qj, -mTalpha[j]);
      r.Axpy(qjm1, -mTbeta[j-I_1]);
      
      if (mDef.GetOrtho() != LANCZOS_ORTHO_NONE)
         Orthogonalize(r, j);
           
      // Calculate beta(j). If this is the last element in the chain, beta is not really part
      // of the T matrix but stored anyway for generating error estimates.
      mTbeta[j] = r.Norm();
      WriteToFile(j, ios_base::app);
     
      // Save q(j) vector if necessary.
      //if (mCalcOrtho || mDef.GetOrtho()!=LANCZOS_ORTHO_NONE)
         SaveQvec(qj, j);

      // Update q(j-1) for next iteration. This is not done in final iteration
      // in order to save q(j) and q(j-1) for future restart.
      if (j != mDef.GetLength()-I_1)
         qjm1 = qj;
   
      // Print progress indication.
      if (j == init_chainidx)
         Mout << " Required iterations: " << mDef.GetLength()-init_chainidx << endl
              << " First iteration: " << time(NULL)-t0 << " s"
              << endl << " Progress (one . is 10 iterations):" << endl << " ";
      else if ( (j-init_chainidx)%10 == I_0)
         Mout << "." << std::flush;
   }
   Mout << endl;
   
   if(gTime)
   {
      string cpu_time = " CPU time used in evaluation of Lanczos chain ";
      string wall_time = " Wall time used in evaluation of Lanczos chain ";
      time_lanc.CpuOut(Mout,cpu_time);
      time_lanc.WallOut(Mout,wall_time);
   }

   
   if (mCalcOrtho)
      CalcOrtho();
}

void LanczosChain::WriteToFile(In aIdx, ios_base::openmode mode)
{
   // Write chain.
   ofstream file_stream;
   file_stream.open(string(gAnalysisDir+"/"+mFilePrefix).c_str(), mode);
   MidasStreamBuf file_buf(file_stream);
   MidasStream file(file_buf);
   file << scientific << setprecision(20) << mTalpha[aIdx] << "   " << mTbeta[aIdx] << endl;
   file_stream.close();

   for (In i=I_0; i<mX.size(); ++i)
   {
      string s = gAnalysisDir+"/"+mFilePrefix + "_" + mDef.GetXoper(i);
      file_stream.open(s.c_str(), mode);
      MidasStreamBuf ms_buf(file_stream);
      MidasStream ms(file_buf);
      ms << scientific << setprecision(20) << mX[i][aIdx] << endl;
      file_stream.close();
   }
}

Nb LanczosChain::CalcEta(const string& aOper, DataCont& aDc)
{
   In iop = I_0;;
   for (iop=I_0; iop<gOperatorDefs.GetNrOfOpers(); ++iop)
      if (gOperatorDefs[iop].Name() == aOper)
         break;
  
   if (iop == gOperatorDefs.GetNrOfOpers())
      MIDASERROR("LanczosChain::CalcEta(): Operator '" + aOper + "' not found.");
  
   Nb exptval = C_0;                          // Not used but required as argument.
   mpVcc->CalculateVciEta(aDc, aOper, iop, exptval);
   Nb norm = aDc.Norm();
   aDc.Scale(C_1/norm);
   return norm;
}

void LanczosChain::CalcXetas()
{
   In nc = mpTrf->NexciXvec()-I_1;                        // Number of response parameters.
   const vector<string>& xopers = mDef.GetXopers();
   for (In i=I_0; i<xopers.size(); ++i)
   {
      mXetas.push_back(DataCont(nc, C_0));
      mXetaNorms.push_back(CalcEta(xopers[i], mXetas[i]));
   }
}

void LanczosChain::SaveQvec(DataCont& aQj, In aJ)
{
   DataCont qj(aQj);
   ostringstream os;
   os << mFilePrefix+"_q" << aJ;
   qj.NewLabel(os.str());
   qj.ChangeStorageTo("OnDisc");
   qj.SaveUponDecon(true);
}

/**
 * Orthogonalize aVec to q(0) ... q(j).
 * Presently only full orthogonalization available.
 * More elaborate methods may be implemeneted.
 **/
void LanczosChain::Orthogonalize(DataCont& aVec, In aJ)
{
   for (In i_repeat=I_0;i_repeat<I_3;i_repeat++)
   {
      for (In k=I_0; k<aJ; k++)
      {
         ostringstream os;
         os << mFilePrefix << "_q" << k;
         DataCont q;
         q.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
         if (! q.ChangeStorageTo("InMem",true, false, true))
         {
            MIDASERROR("Error reading q(" + std::to_string(k) + ") in orthogonalization step '" + os.str() + "'.");
         }
         aVec.Orthogonalize(q);
      }
   }
}

/**
 * Restart from a previous calculation.
 * aR and aQjm1 are initialized for use in iterative algorithm used to generate T.
 *
 * The [mFilePrefix] file is supposed to have the format
 *
 * alpha(0)    beta(0)
 * alpha(1)    beta(1)
 *  ...         ...   
 * alpha(k)    beta(k)
 *
 * where beta(k) is not actually part of the T matrix but
 * is useful for error measures.
 *
 * In addition, a set of [mFilePrefix]_[Xoper] files must be present which contains
 * the dot products between q(j) and the eta^X vector for Xoper.
 **/
bool LanczosChain::Restart(In& aLength, DataCont& aR, DataCont& aQjm1)
{
   // Read q(j) and q(j-1).
   DataCont qj;
   qj.SetNewSize(I_0);
   qj.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, mFilePrefix+"_qj");
  
   DataCont qjm1; 
   qjm1.SetNewSize(I_0);
   qjm1.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, mFilePrefix+"_qjm1");

   if (! qj.ChangeStorageTo("InMem",true, false, true))
   {
      Mout << " Lanczos restart failed to read q(j) file." << endl; 
      return false; 
   }
   
   if (! qjm1.ChangeStorageTo("InMem",true, false, true))
   {
      Mout << " Lanczos restart failed to read q(j-1) file." << endl; 
      return false; 
   }

   // Read in chain up to specified chain length or as many as possible. 
   ifstream chain_file(mFilePrefix.c_str(), ios_base::in);
   string s;
   aLength = I_0;
   while (getline(chain_file,s) && aLength<mDef.GetLength())
   {
      istringstream is(s);
      is >> mTalpha[aLength];
      is >> mTbeta[aLength];
      aLength++;
   }
   chain_file.close();

   // Read [mFilePrefix]_[Xoper] files into mX.
   for (In i=I_0; i<mX.size(); ++i)
   {
      string fn = mFilePrefix + "_" + mDef.GetXoper(i);
      ifstream file(fn.c_str(), ios_base::in);
      In idx = I_0;
      while (getline(file,s) && idx<mDef.GetLength())
      {
         istringstream is(s);
         is >> mX[i][idx];
         idx++;
      }
      file.close();
      if (idx != aLength)
      {
         Mout << " Lanczos restart: Cannot use file: '" << fn << "'." << endl
              << " Attemting to reconstruct from q(j) vectors...";
         if (RestoreXfromQ(i,aLength))
            Mout << "Success." << endl;
         else
         {
            Mout << " Failure." << endl << " Restart not possible." << endl;
            return false;
         }
      }
   }

   // Prepare aR and aQjm1 for iterative algorithm.
   if (aLength < mDef.GetLength()-I_1)
   {
      // Calculate r = (A-alpha(j)) * q(j) - b(j-1) * q(j-1).
      DataCont A_qj(mpTrf->NexciXvec()-I_1);                   // A*q(j).
      Nb dummy = C_0;
      mpTrf->Transform(qj, A_qj, I_1, I_0, dummy);
      aR = A_qj;
      aR.Axpy(qj, -mTalpha[aLength]);
      aR.Axpy(qjm1, -mTbeta[aLength-I_1]);
      if (mDef.GetOrtho() != LANCZOS_ORTHO_NONE)
         Orthogonalize(aR, aLength);

      aQjm1 = qj;
   }

   return true;
}

bool LanczosChain::RestoreXfromQ(In aIdx, In aLength)
{
   // Try to restore values.
   for (In j=I_0; j<aLength; j++)
   {
      ostringstream os;
      os << mFilePrefix << "_q" << j;
      DataCont q;
      q.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
      if (! q.ChangeStorageTo("InMem",true,false,true))
         return false;
      mX[aIdx][j] = Dot(q,mXetas[aIdx]);
   }
  
   // Save values on files. 
   string fn = gAnalysisDir+"/"+mFilePrefix + "_" + mDef.GetXoper(aIdx);
   ofstream file_stream;
   file_stream.open(fn.c_str(), ios_base::trunc);
   MidasStreamBuf file_buf(file_stream);
   MidasStream file(file_buf);
   file << scientific << setprecision(20);
   for (In i=I_0; i<aLength; ++i)
      file << mX[aIdx][i] << endl;
   file_stream.close();
  
   return true;
}

/**
 * Calculate dot products of q(j) vectors assuming they are stored on disc.
 **/
void LanczosChain::CalcOrtho()
{
   In nc = mpTrf->NexciXvec() - I_1;
   
   // Setup file containing dot products..
   string filename = gAnalysisDir+"/"+mFilePrefix + "_q_orthogonality";
   ofstream orto_file_stream(filename.c_str(), ios_base::trunc);
   MidasStreamBuf orto_file_buf(orto_file_stream);
   MidasStream orto_file(orto_file_buf);
   orto_file << scientific << setprecision(20);

   ostringstream os;
   for (In i=I_0; i<mDef.GetLength(); i++)
   {
      DataCont qi(I_0);
      os.str("");
      os << mFilePrefix << "_q" << i;
      qi.GetFromExistingOnDisc(nc, os.str());
      qi.SaveUponDecon(true);
      for (In j=I_0; j<=i; j++)
      {
         DataCont qj(0);
         ostringstream os;
         os << mFilePrefix << "_q" << j;
         qj.GetFromExistingOnDisc(nc, os.str());
         qj.SaveUponDecon(true);
         orto_file << i << " " << j << " " << Dot(qi,qj) << endl;
      }
   }
}

bool LanczosChain::GetAlphaRange(MidasVector& aVals, In aBegin, In aLen) const
{
   if (aBegin+aLen > mTalpha.Size())
      return false;
   for (In i=I_0; i<aLen; ++i)
      aVals[i] = mTalpha[aBegin+i];
   return true;
}

bool LanczosChain::GetBetaRange(MidasVector& aVals, In aBegin, In aLen) const
{
   if (aBegin+aLen > mTbeta.Size())
      return false;

   for (In i=I_0; i<aLen; ++i)
      aVals[i] = mTbeta[aBegin+i];
   return true;
}

bool LanczosChain::GetXRange(MidasVector& aVals, In aBegin, In aLen, const string& aOper) const
{
   In opidx = GetXoperIdx(aOper);
   if (aBegin+aLen > mX[opidx].Size())
      return false;
   
   for (In i=I_0; i<aLen; ++i)
      aVals[i] = mX[opidx][i];
   return true;
}

Nb LanczosChain::GetXetaNorm(const string& aOper) const
{
   In opidx = GetXoperIdx(aOper);
   return mXetaNorms[opidx];
}

In LanczosChain::GetXoperIdx(const string& aOper) const
{
   const vector<string>& opers = mDef.GetXopers();
   In i=I_0;
   for (i=I_0; i<opers.size(); ++i)
      if (opers[i] == aOper)
         return i;

   MIDASERROR(" LanczosChain::GetXoperIdx(): Operator '" + aOper + "' not found.");
   return -I_1;
}

void LanczosChain::Diag(In aDim, MidasVector* aEigVals, MidasMatrix* aEigVecs) const
{
   MidasVector a;
   MidasVector b;
   a.Reassign(mTalpha, aDim);
   b.Reassign(mTbeta, aDim-I_1);

   MidasVector eigvals(aDim);
   MidasMatrix eigvecs(aDim);
   
   DiagSymTridiag(a, b, b, eigvals, eigvecs);

   // Write eigenvalues to file.
   string filename = gAnalysisDir+"/"+mFilePrefix + "_eigvals";
   ofstream file_stream(filename.c_str(), ios_base::trunc);
   MidasStreamBuf file_buf(file_stream);
   MidasStream file(file_buf);
   file << fixed << setprecision(15)
        << "# Column 1: Eigenvalues of T (cm-1)" << endl
        << "# Column 2: beta[i] * eigvec[i] (error estimate, cm-1)" << endl
        << "# Column 3: eigval[i]*eigvec[i]^2 (proportional to intensity contribution)" << endl;
   for (In i=I_0; i<aDim; ++i)
      file << eigvals[i]*C_AUTKAYS << "   "
           << fabs(mTbeta[aDim-I_1]*eigvecs[aDim-I_1][i]*C_AUTKAYS) << "   "
           << eigvals[i]*eigvecs[I_0][i]*eigvecs[I_0][i] << endl;

   if (NULL !=aEigVals)
      *aEigVals = eigvals;
   if (NULL != aEigVecs)
      *aEigVecs = eigvecs;
}

/**
 * Transform S (eigenvector of T) to X (approximate eigenvector of full natrix).
 * Require q vectors to be available.
 **/
void LanczosChain::TransformStoX(const MidasVector& aS, DataCont& aX) const
{
   aX.SetNewSize(mpTrf->NexciXvec()-I_1);
   aX.Zero();
   aX.ChangeStorageTo("InMem");

   for (In i=I_0; i<aS.Size(); ++i)
   {
      ostringstream os;
      os << mFilePrefix << "_q" << i;
      DataCont q;
      q.GetFromExistingOnDisc(mpTrf->NexciXvec()-I_1, os.str());
      if (! q.ChangeStorageTo("InMem",true, false, true))
         MIDASERROR("LanczosChain::TransformStoX(): Error reading q(j).");
      aX.Axpy(q, aS[i]);
   }
}

MidasVector LanczosChain::TransVecWithChain(MidasVector& aV) 
{
   In size=mpTrf->NexciXvec()-I_1;
   // Get chain
   // MidasMatrix q_vec(size,mDef.GetLength());
   MidasVector result(size,C_0);
   for (In i=I_0; i<mDef.GetLength(); i++)
   {
      DataCont q(I_0);
      string s=mFilePrefix+"_q"+std::to_string(i);
      q.GetFromExistingOnDisc(size,s);
      q.SaveUponDecon(true);
      MidasVector v(size);
      q.DataIo(IO_GET,v,size);
      // q_vec.AssignCol(v,i);
      result=result+v*aV[i];
   }
   return result;
}

MidasVector LanczosChain::GetSpecificQVec(In aVec)
{
   In size = mpTrf->NexciXvec()-I_1;
   DataCont q(I_0);
   string s = mFilePrefix+"_q"+std::to_string(aVec);
   q.GetFromExistingOnDisc(size,s);
   q.SaveUponDecon(true);
   MidasVector v(size);
   q.DataIo(IO_GET,v,size);
   return v;
}

ostream& operator<<(ostream& aOut, const LanczosChain& aChain)
{
   aOut << " Lanczos chain definition: " << aChain.mDef<< endl
        << "              File prefix: " << aChain.mFilePrefix  << endl;
   return aOut;
}
