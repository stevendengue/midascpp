/**
************************************************************************
* 
* @file                 VccStateModeCombi.cc
*
* Created:              09-05-2016
*
* Author:               Mads Boettger Hansen (mb.hansen@chem.au.dk)
*
* Short Description:    References to a mode combination and its corresponding
*                       dimensions (number of modals per mode).
* 
* Last modified:        09-05-2016 (Mads Boettger Hansen)
* 
* Detailed Description: A class holding a (const reference to a) specific ModeCombi
*                       (from a ModeCombiOpRange) and a (const reference to a)
*                       vector with the dimensions of the modes (i.e. number of
*                       modals per mode, minus 1 for the reference state).
*                       Initially intended for use with the VccStateSpace
*                       class, which indeed defines the space of VCC states
*                       through the ModeCombiOpRange and the dimensions/'num.
*                       modes per mode'.
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include <string>
#include <utility>

#include "vcc/VccStateSpace.h"
#include "vcc/VccStateModeCombi.h"
#include "util/Error.h"

// -----------------------------------------------------------------------------
//    Private member functions, constructors
// -----------------------------------------------------------------------------
VccStateModeCombi::VccStateModeCombi(const VccStateSpace& arVSS, iter_type&& arMCiter, Uin aRefNumber)
   :  mMCiter(std::move(arMCiter))
   ,  mrDims(arVSS.Dims())
   ,  mrAddresses(arVSS.Addresses())
   ,  mRefNumber(aRefNumber)
{
}

// -----------------------------------------------------------------------------
//    Constructors, etc.
// -----------------------------------------------------------------------------
// Default constructors.
VccStateModeCombi::VccStateModeCombi(const VccStateModeCombi&) = default;
VccStateModeCombi::VccStateModeCombi(VccStateModeCombi&&) = default;
VccStateModeCombi::~VccStateModeCombi() = default;

// Named constructors.
VccStateModeCombi VccStateModeCombi::ConstructBegin(const VccStateSpace& arVSS)
{
   return VccStateModeCombi(arVSS, arVSS.MCR().begin(), I_0);
}

VccStateModeCombi VccStateModeCombi::ConstructEnd(const VccStateSpace& arVSS)
{
   return VccStateModeCombi(arVSS, arVSS.MCR().end(), arVSS.NumMCs());
}

// -----------------------------------------------------------------------------
//    Iterator logic
// -----------------------------------------------------------------------------
// Comparison operators.
bool VccStateModeCombi::operator==(const VccStateModeCombi& arOther) const
{
   return (mMCiter == arOther.mMCiter);
}

bool VccStateModeCombi::operator!=(const VccStateModeCombi& arOther) const
{
   return !(*this == arOther);
}

// Increment/decrement operators.
VccStateModeCombi& VccStateModeCombi::operator++()
{
   ++mMCiter;
   ++mRefNumber;
   return *this;
}

VccStateModeCombi  VccStateModeCombi::operator++(int)
{
   VccStateModeCombi temp(*this);
   operator++();
   return temp;
}

VccStateModeCombi& VccStateModeCombi::operator--()
{
   --mMCiter;
   --mRefNumber;
   return *this;
}

VccStateModeCombi  VccStateModeCombi::operator--(int)
{
   VccStateModeCombi temp(*this);
   operator--();
   return temp;
}

// -----------------------------------------------------------------------------
//    Member access
// -----------------------------------------------------------------------------
const ModeCombi& VccStateModeCombi::MC() const
{
   return *mMCiter;
}

Uin VccStateModeCombi::RefNumber() const
{
   return mRefNumber;
}

Uin VccStateModeCombi::AddressBegin() const
{
   return mrAddresses[mRefNumber];
}

Uin VccStateModeCombi::AddressEnd() const
{
   return mrAddresses[mRefNumber+I_1];
}

Uin VccStateModeCombi::TensorSize() const
{
   return AddressEnd() - AddressBegin();
}

bool VccStateModeCombi::ContainsAddress(Uin aAddress) const
{
   return AddressBegin() <= aAddress && aAddress < AddressEnd();
}

std::vector<Uin> VccStateModeCombi::GetDims() const
{
   // Default construct, then reserve space.
   std::vector<Uin> dims;
   dims.reserve(mMCiter->Size());

   // Fill in the relevant dimensions according to the ModeCombi, then return.
   for(const auto& mode: mMCiter->MCVec())
   {
      dims.push_back(mrDims[mode]);
   }

   return dims;
}

std::vector<In> VccStateModeCombi::GetNModals() const
{
   // Default construct, then reserve space.
   std::vector<In> n_modals;
   n_modals.reserve(mMCiter->Size());

   // Fill in the relevant n_modals according to the ModeCombi, then return.
   for(const auto& mode: mMCiter->MCVec())
   {
      n_modals.push_back(static_cast<In>(mrDims[mode] + I_1));
   }

   return n_modals;
}

std::vector<Uin> VccStateModeCombi::GetIndexVecFromAddress(Uin aAddr, bool aRelAddr) const
{
   // Find the address relative to the beginning address of the MC.
   aAddr -= aRelAddr ? I_0 : AddressBegin();

   // Assert that the address is contained in this MC (the rel. addr. is less
   // than the size of the tensor).
   MidasAssert(aAddr < TensorSize(), "Address not contained in this MC.");

   // Get the dimensions vector and modify it in-place to get the resulting
   // index vector.
   std::vector<Uin> vec = GetDims();

   // Get the multi-index by "division-with-remainder".
   // E.g.
   //    dims = {3, 4, 5}
   //    addr = 37
   //    subsize = 60
   //    Loop, 0th iteration: d == 3; subsize = 20;  d = 37/20 = 1; addr = 17;
   //          1st iteration: d == 4; subsize =  5;  d = 17/ 5 = 3; addr =  2;
   //          2nd iteration: d == 5; subsize =  1;  d =  2/ 1 = 2; addr =  0;
   //    --> index == {1, 3, 2}
   Uin subsize = TensorSize();
   for(auto&& d: vec)
   {
      subsize  /= d;
      d        =  aAddr/subsize;
      aAddr -= d*subsize;
   }

   return vec;
}

// -----------------------------------------------------------------------------
//    Non-member functions related to class
// -----------------------------------------------------------------------------
// Strings with modes and dimensions.
std::string StringOfModes(
   const VccStateModeCombi& arVSMC, 
   const std::vector<std::string>& arFormat
)
{
   MidasAssert(arFormat.size() == I_3, "Wrong number of elements in formatting vector.");

   // Initiate string with opening delimiter.
   std::string s(arFormat.front());

   // Append elements to string.
   for(const auto& mode: arVSMC.MC().MCVec())
   {
      s.append(std::to_string(mode) + arFormat[I_1]);
   }
   // Delete last separator if any.
   auto pos = s.rfind(arFormat[I_1]);
   if(pos != std::string::npos)
   {
      s.erase(pos);
   }
   // Append closing delimiter.
   s.append(arFormat.back());

   return s;
}

std::string StringOfDims (
   const VccStateModeCombi& arVSMC, 
   const std::vector<std::string>& arFormat
)
{
   MidasAssert(arFormat.size() == I_3, "Wrong number of elements in formatting vector.");

   // Initiate string with opening delimiter.
   std::string s(arFormat.front());

   // Get dimensions.
   const auto dims = arVSMC.GetDims();

   // Append elements to string.
   for(const auto& dim: dims)
   {
      s.append(std::to_string(dim) + arFormat[I_1]);
   }
   // Delete last separator if any.
   auto pos = s.rfind(arFormat[I_1]);
   if(pos != std::string::npos)
   {
      s.erase(pos);
   }
   // Append closing delimiter.
   s.append(arFormat.back());

   return s;
}

std::string StringOfNModals(
   const VccStateModeCombi& arVSMC, 
   const std::vector<std::string>& arFormat
)
{
   MidasAssert(arFormat.size() == I_3, "Wrong number of elements in formatting vector.");

   // Initiate string with opening delimiter.
   std::string s(arFormat.front());

   // Get number of modals per mode.
   const auto n_modals = arVSMC.GetNModals();

   // Append elements to string.
   for(const auto& n: n_modals)
   {
      s.append(std::to_string(n) + arFormat[I_1]);
   }
   // Delete last separator if any.
   auto pos = s.rfind(arFormat[I_1]);
   if(pos != std::string::npos)
   {
      s.erase(pos);
   }
   // Append closing delimiter.
   s.append(arFormat.back());

   return s;
}

