/**
************************************************************************
* 
* @file                TwoModeIntermeds_Decl.h 
*
* Created:             51-5-2007
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Organizing intermediates for specific VCC[2]
*                      calculation.
* 
* Last modified: Thu Dec 14, 2006  03:56PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TWOMODEINTERMEDS_DECL_H_INCLUDED
#define TWOMODEINTERMEDS_DECL_H_INCLUDED

// std headers
#include <list>
#include <vector>
#include <utility>
#include <iostream>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/type_traits/Complex.h"
#include "mmv/MidasVector.h"

class ModeCombiOpRange;
class OpDef;

// Forward declaration of class and friends.
template<typename T> class TwoModeIntermeds;

template<typename T>
void TMI_DirProd
   (  const TwoModeIntermeds<T>& aIntermed0
   ,  const LocalModeNr aM0
   ,  const LocalModeNr aM1
   ,  const LocalOperNr aOperM1
   ,  const TwoModeIntermeds<T>& aIntermed2
   ,  const LocalModeNr aM2
   ,  const LocalModeNr aM3
   ,  const LocalOperNr aOperM3
   ,  typename TwoModeIntermeds<T>::midasvector_t& aRes
   );

template<typename T>
void TMI_DirProd
   (  const TwoModeIntermeds<T>& aX
   ,  const LocalModeNr aM0
   ,  const LocalModeNr aM1
   ,  const LocalOperNr aOp1
   ,  const typename TwoModeIntermeds<T>::midasvector_t& aC
   ,  const LocalModeNr aM2
   ,  typename TwoModeIntermeds<T>::midasvector_t& aRes
   );

template<typename T>
std::ostream& operator<<(std::ostream& aOut, const TwoModeIntermeds<T>& aArg);


/***************************************************************************//**
 *
 ******************************************************************************/
template<typename T>
class TwoModeIntermeds
{
public:
   using value_t = T;
   using absval_t = midas::type_traits::RealTypeT<value_t>;
   using midasvector_t = GeneralMidasVector<value_t>;

private:
   using invec1d = std::vector<In>;
   using invec2d = std::vector<std::vector<In>>;
   using vec1d = std::vector<value_t>;
   using vec2d = std::vector<std::vector<value_t>>;

   std::vector<In> mNmodals;

   // Start of intermediates. Value is -1 if no intermediate exists.
   invec2d mXaddr;

   //! Vec of X intermeds. and norms; for each (m0,m1,oper) a block of X intermeds. and their norm^2.
   midasvector_t mX;

   // For addressing XC intermediates. These are not available for all operators, so addressing
   // is [m0][m1] -> std::list (oper, midasvector_t*) std::pairs.
   std::vector<std::vector<std::list<std::pair<In,midasvector_t*> > > > mXC;

   In Nmodals(LocalModeNr) const;

   //! h(m0) h(m1) * T(m0,m1) contractions (all terms in H).
   std::vector<std::vector<value_t> > mY;

   //! h(m0, oper) * T(m0) contractions, (m0,oper)Z. Index [m0][oper]
   vec2d mZ;
   
public:
   TwoModeIntermeds();
   TwoModeIntermeds(std::vector<In> aNmodals, const OpDef* aOpDef,
                    const ModeCombiOpRange&, bool aZ=false);
   ~TwoModeIntermeds();

   //@{
   //! Implement these properly if enabling any!! Notice the raw pointers in mXC!! -MBH, Mar 2019.
   TwoModeIntermeds(const TwoModeIntermeds&);
   TwoModeIntermeds(TwoModeIntermeds&&) = default;
   TwoModeIntermeds& operator=(const TwoModeIntermeds&) = delete;
   TwoModeIntermeds& operator=(TwoModeIntermeds&&) = delete;
   //@}

   void Initialize(std::vector<In> aNmodals, const OpDef* aOpDef,
                   const ModeCombiOpRange&, bool aZ=false);

   void InitializeXCintermeds(const OpDef* aOpDef);
   
   // For X intermediate the indexing is (aM1, aOper)X(aM0,a)
   void PutXintermed(const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr aOper, const midasvector_t& aVec);
   bool GetXintermed(const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr aOper, midasvector_t& aVec) const;

   bool GetXnorm2(const LocalModeNr aM0, const LocalModeNr aM1, const LocalOperNr oper, absval_t& aNorm2) const;

   // aXC should point to array of MidasVectors. The number of MidasVectors should be
   // equal to the number of operators for mode aM1.
   bool GetXCintermeds(const LocalModeNr aM0, const LocalModeNr aM1, midasvector_t* aXC) const;
 
   // Get a std::list of pointers to the XC intermediates. 
   const std::list<std::pair<In, midasvector_t*> >& GetXCintermeds(const LocalModeNr aM0, const LocalModeNr aM1) const; 

   //! Get the (m0,m1) Y intermediate.
   value_t GetY(const LocalModeNr m0, const LocalModeNr m1) const {return mY[m0][m1];}

   //! Add a contribution to the (m0,m1) Y intermediate.
   void AddToY(const LocalModeNr m0, const LocalModeNr m1, const value_t aVal) {mY[m0][m1] += aVal;}

   //! Get the (m0,oper) Z intermediate.
   value_t GetZ(const LocalModeNr m0, const LocalOperNr oper) const {return mZ[m0][oper];}

   //! Assign value to the (m0,oper) Z intermediate.
   void AssignToZ(const LocalModeNr m0, const LocalOperNr oper, const value_t aVal) {mZ[m0][oper] = aVal;}

   friend void TMI_DirProd<>
      (  const TwoModeIntermeds<T>& aIntermed0
      ,  const LocalModeNr aM0
      ,  const LocalModeNr aM1
      ,  const LocalOperNr aOperM1
      ,  const TwoModeIntermeds<T>& aIntermed2
      ,  const LocalModeNr aM2
      ,  const LocalModeNr aM3
      ,  const LocalOperNr aOperM3
      ,  typename TwoModeIntermeds<T>::midasvector_t& aRes
      );

   //! Dir.prod. of (aM1, aOp1)X(aM0) for aX and the aC corresponding to excitations in mode aM2.
   friend void TMI_DirProd<>
      (  const TwoModeIntermeds<T>& aX
      ,  const LocalModeNr aM0
      ,  const LocalModeNr aM1
      ,  const LocalOperNr aOp1
      ,  const typename TwoModeIntermeds<T>::midasvector_t& aC
      ,  const LocalModeNr aM2
      ,  typename TwoModeIntermeds<T>::midasvector_t& aRes
      );
   
   friend std::ostream& operator<< <>(std::ostream& aOut, const TwoModeIntermeds<T>& aArg);
};
   
#endif // TWOMODEINTERMEDS_DECL_H_INCLUDED
