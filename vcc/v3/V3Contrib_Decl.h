/**
************************************************************************
* 
* @file                V3Contrib_Decl.h
*
* Created:             16-10-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*                      Niels Kristian Madsen (nielksm@chem.au.dk), templating
*
* Short Description:   Basic interface for contributions used by V3
*                      transformer.
* 
* Last modified: Tue May 05, 2009  12:04PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef V3CONTRIB_DECL_H_INCLUDED
#define V3CONTRIB_DECL_H_INCLUDED

// std headers
#include <vector>
#include <iostream>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "input/ModeCombi.h"

/** @name Forward declarations **/
//!@{

template
   <  typename T
   >
class GeneralDataCont;
using DataCont = GeneralDataCont<Nb>;

template
   <  typename T
   >
class GeneralTensorDataCont;
using TensorDataCont = GeneralTensorDataCont<Nb>;

template
   <  typename T
   >
class NiceTensor;

namespace midas::vcc
{
template
   <  typename T
   >
class TensorSumAccumulator;
} /* namespace midas::vcc */

//! V3Contrib
namespace midas::vcc::v3
{
template
   <  typename T
   ,  template<typename...> typename DATA
   >
class V3Contrib;

template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedProd;

template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedProdL;

class FndT;

class FTCommutator;
}  /* namespace midas::vcc::v3 */

//!@}

namespace midas::vcc::v3
{

/**
 * Base class for contributions (terms) used in TransformerV3.
 *
 * Templated for use in MCTDH
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
class V3Contrib
{
   public: 
      //! Alias
      using evaldata_t = DATA<T>;

      //! Virtual destructor.
      virtual ~V3Contrib
         (
         )  = default;
      
      /** 
       * Used for copying intermediate when only base class pointer is avaliable.
       * This function creates a new intermediate of the correct type on the heap.
       */
      virtual std::unique_ptr<V3Contrib<T, DATA> > Clone
         (
         )  const = 0;
      
      //! Evaluate this contribution. Result is added to the aDcOut data container.
      virtual void Evaluate
         (  const evaldata_t& aEvalData
         ,  GeneralDataCont<T>& aDcOut
         )  = 0;
      
      //! Evaluate this contribution. Result is added to the aDcOut data container.
      virtual void Evaluate
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMc
         ,  GeneralMidasVector<T>& arRes
         )
      {
         MIDASERROR("Not implemented for " + this->Type());
      }
      
      //! Evaluate this contribution. Result is added to the aDcOut data container.
      virtual void Evaluate
         (  const evaldata_t& aEvalData
         ,  GeneralTensorDataCont<T>& arDcOut
         )
      {
         MIDASERROR("Not implemented for " + this->Type());
      }
      
      //! Evaluate this contribution. Result is added to the aDcOut data container.
      virtual void Evaluate
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMc
         ,  NiceTensor<T>& arRes
         )
      {
         MIDASERROR("Not implemented for " + this->Type());
      }
      
      //! Evaluate this contribution. Result is added to the aDcOut data container.
      virtual void Evaluate
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMc
         ,  TensorSumAccumulator<T>& arRes
         )
      {
         MIDASERROR("Not implemented for " + this->Type());
      }
      
      //! Write contribution as latex math std::string.
      virtual void Latex
         (  std::ostream& aOut
         )  const; 
      
      //! Print contribution.
      virtual std::ostream& Print
         (  std::ostream& aOut
         )  const = 0; 
      
      //! Write specs.
      virtual void WriteSpec
         (  std::ostream& aOut
         )  const = 0;
      
      //! Factory for creating V3Contrib's.
      static std::unique_ptr<V3Contrib> Factory
         (  const std::string& aSpec
         );

      //! Read V3-contributions (V3c) file. 
      static bool ReadV3cFile
         (  const std::string& aFilename
         ,  std::vector<std::unique_ptr<V3Contrib> >& aContribs
         );
      
      //! Write V3-contributions (V3c) file. 
      static void WriteV3cFile
         (  const std::string& aFilename
         ,  std::vector<std::unique_ptr<V3Contrib> >& aContribs
         );
   
      //!@{
      //! Accounting done for screening stuff.
      virtual In Screened
         (
         )  const
      {
         MIDASERROR("Not implemented.");
         return -1;
      }
      virtual In NotScreened
         (
         )  const
      {
         MIDASERROR("Not implemented.");
         return -1;
      }
      virtual In Total
         (
         )  const
      {
         MIDASERROR("Not implemented.");
         return -1;
      }
      //!@}

      //! Get excitation level.
      virtual In ExciLevel
         (
         )  const
      {
         MIDASERROR("Not implemented.");
         return -1;
      }

      //! Get type.
      virtual std::string Type
         (
         )  const = 0;
};

template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::ostream& operator<<
   (  std::ostream& aOut
   ,  const V3Contrib<T, DATA>& aContrib
   )
{
   return aContrib.Print(aOut);
}

} /* namespace midas::vcc::v3 */


#endif /* V3CONTRIB_DECL_H_INCLUDED */
