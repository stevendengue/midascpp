/**
************************************************************************
* 
* @file                IntermedCmbL.h
*
* Created:             03-12-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*                      Niels Kristian Madsen (nielskm@chem.au.dk), templating
*
* Short Description:   Interface for IntermedCmbL used in left-hand
*                      transformations.
* 
* Last modified: Mon Mar 22, 2010  01:39PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDCMBL_DECL_H_INCLUDED
#define INTERMEDCMBL_DECL_H_INCLUDED

#include <vector>
#include <iostream>
#include <string>
#include <algorithm>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"
#include "input/ModeCombi.h"
#include "IntermedL.h"
#include "Scaling.h"

template
   <  class T
   >
class NiceTensor;

namespace midas::vcc
{

template
   <  typename T
   >
class TensorSumAccumulator;

namespace v3
{

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedCmbL
   :  public IntermedL<T, DATA>
{
   public:
      using evaldata_t = DATA<T>;
      using real_t = midas::type_traits::RealTypeT<T>;

   protected:
      std::unique_ptr<IntermedL<T, DATA> > mIntermedL;
      std::unique_ptr<IntermedT<T, DATA> > mIntermedT;

      std::vector<In> mExci;             ///< Exposed purely excited modes.
      std::vector<In> mExciOper;         ///< Exposed excited modes with asscoiated operator.
      std::vector<In> mOcc;              ///< Exposed occupied modes with no operator.
      std::vector<std::vector<In> > mOccOper;          ///< Exposed occupied modes with associated operator.
      std::vector<In> mSumModes;         ///< Modes to be summed.
      std::vector<In> mOperModes;        ///< Modes with associated operator which can
                                         ///< therefore not be summed even if in common
                                         ///< between L and T.
    
      std::vector<In> mCmodes;           ///< Assigned concrete modes.
      std::vector<In> mCopers;           ///< Assigned operators.
     
      In mHighestModeNo;    ///< Highest general mode number in intermediate.
      
      void GetIntermedSpec(std::string::const_iterator& aPos, std::string& aSpec); 
      void UpdateInternalState(const std::vector<In>& aOperModes);
     
      //! Do sum in combined left intermediate. F is the function to be run, T... is arguments for function
      // funtion to be passed must take (const evaldata_t&, std::vector<In>&, ...) where ... is any number of other arguments
      template
         <  class F
         ,  class... Ts
         >
      void DoSum
         (  const evaldata_t& aEvalData
         ,  In aSumIdx
         ,  In aFirstMode
         ,  std::vector<In>& aCmodes
         ,  F func
         ,  Ts&&... t
         )  const;

      //! Functions to pass to DoSum to Evaluate intermediate contribution
      //!@{
      void EvalContrib
         (  const evaldata_t& aEvalData
         ,  std::vector<In>& aCmodes
         ,  GeneralMidasVector<T>& aRes
         )  const;

      void EvalContribTensor
         (  const evaldata_t& aEvalData
         ,  std::vector<In>& aCmodes
         ,  TensorSumAccumulator<T>& aRes
         )  const;
      //!@}

      //! Function to pass to DoSum to Evaluate intermediate norm 2 estimate
      void EvalNorm2
         (  const evaldata_t& aEvalData
         ,  std::vector<In>& aCmodes
         ,  real_t& aLNorm
         ,  real_t& aTNorm
         )  const;
      
   public:
      IntermedCmbL();
      IntermedCmbL(const std::string& aSpec);
      IntermedCmbL
         (  std::unique_ptr<IntermedL<T, DATA> >&& aImL
         ,  std::unique_ptr<IntermedT<T, DATA> >&& aImT
         ,  const std::vector<In>& aOperModes
         );
      IntermedCmbL(const IntermedCmbL<T, DATA>& aOrig);
      IntermedCmbL& operator=(const IntermedCmbL<T, DATA>& aOrig);
      std::unique_ptr<Intermediate<T, DATA> > Clone
         (
         )  const override
      {
         return std::make_unique<IntermedCmbL<T, DATA> >(*this);
      }
      
      bool AssignConcreteModesOpers(const std::vector<In>& aCmodes, const std::vector<In>& aCopers) override;

      //! Evaluate interface
      //!@{
      bool Evaluate
         (  const evaldata_t&
         ,  GeneralMidasVector<T>&
         )  const override;
      
      bool Evaluate
         (  const evaldata_t&
         ,  NiceTensor<T>&
         )  const override;
      //!@}


      void GetConcreteModes(std::vector<In>& aCmodes) const override;
      void GetConcreteOpers(std::vector<In>& aCopers) const override;
      void RegisterInternalIntermeds(IntermediateMachine<T, GeneralMidasVector, DATA>& aInMachine) override;
      void RegisterInternalIntermeds(IntermediateMachine<T, NiceTensor, DATA>& aInMachine) override;
      bool CmpKind(const Intermediate<T, DATA>* const aIntermed) const override;
      In ExciLevel() const override {return mExci.size() + mExciOper.size();}
      void GetModes(std::vector<In>& aModes) const override;
      void GetExciModes(std::vector<In>& aModes) const override;
      void GetOccModes(std::vector<In>& aModes) const override;
      void GetOperOccModes(std::vector<In>& aModes) const override;
      In HighestModeNo() const override;
      Scaling GetScaling(bool=false) const override;
      Scaling GetCost(bool=false) const override;
      Scaling GetNumber() const override;
      
      void GetPureExciModes(std::vector<In>& aModes) const override
      {std::copy(mExci.begin(), mExci.end(), std::back_inserter(aModes));}

      void GetOperExciModes(std::vector<In>& aModes) const override
      {std::copy(mExciOper.begin(), mExciOper.end(), std::back_inserter(aModes));}
      
      bool OnlyAmps() const override {return false;}
      
      std::ostream& Print(std::ostream& aOut) const override;
      void Latex(std::ostream& aOut) const override;
      void WriteSpec(std::ostream& aOut) const override;
      
      void PrintInternalState() const;
      void PrintScaling(std::ostream& aOut) const;

      bool Norm2(const evaldata_t&, real_t&) const override;

      std::string Type() const override { return "IntermedCmbL"; }
};

}  /* namespace v3 */
}  /* namespace midas::vcc */

#endif // INTERMEDCMBL_H
