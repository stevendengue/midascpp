/**
************************************************************************
* 
* @file                IntermedCmbL_Impl.h
*
* Created:             03-12-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*                      Niels Kristian Madsen (nielskm@chem.au.dk), templating
*
* Short Description:   Left-hand vector combined intermediate.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDCMBL_IMPL_H_INCLUDED
#define INTERMEDCMBL_IMPL_H_INCLUDED

#include <iostream>
#include <algorithm>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"
#include "mmv/MMVT.h"
#include "VccEvalData.h"
#include "Intermediate.h"
#include "IntermedL.h"
#include "input/OpDef.h"
#include "IntermedCmbL_Decl.h"
#include "vcc/GenDownCtr.h"
#include "vcc/TensorGeneralDownContraction.h"
#include "IntermediateMachine.h"
#include "vcc/TransformerV3.h"
#include "util/DynamicCastUniquePtr.h"
#include "EvalDataTraits.h"

#include "libmda/numeric/float_eq.h"


namespace midas::vcc::v3
{

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedCmbL<T, DATA>::IntermedCmbL
   (
   )
   :  IntermedL<T, DATA>()
   ,  mIntermedL(nullptr)
   ,  mIntermedT(nullptr)
{
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedCmbL<T, DATA>::IntermedCmbL
   (  const std::string& aSpec
   )
   :  IntermedL<T, DATA>()
{
   std::string::const_iterator pos = aSpec.begin();
   std::string spec;
   GetIntermedSpec(pos, spec);
   auto im = Intermediate<T, DATA>::Factory(spec);
   mIntermedL = midas::util::DynamicCastUniquePtr<IntermedL<T, DATA>>(std::move(im));
   if (  !mIntermedL
      )
   {
      Mout << "IntermedCmbL<T, DATA>::IntermedCmbL():" << std::endl
           << "aSpec: " << aSpec << std::endl
           << "Intermediate '" << spec << "' is not of type IntermedL." << std::endl;
      MIDASERROR("");
   }

   spec.clear();
   GetIntermedSpec(pos,spec);
   auto im_t = Intermediate<T, DATA>::Factory(spec);
   mIntermedT = midas::util::DynamicCastUniquePtr<IntermedT<T, DATA>>(std::move(im_t));
   if (  !mIntermedT
      )
   {
      Mout << "IntermedCmbL<T, DATA>::IntermedCmbL():" << std::endl
           << "aSpec: " << aSpec << std::endl
           << "Intermediate '" << spec << "' is not of type IntermedT." << std::endl;
      MIDASERROR("");
   }

   std::vector<In> oper_modes;
   detail::ReadVector(pos, oper_modes);
   std::sort(oper_modes.begin(), oper_modes.end());
   
   UpdateInternalState(oper_modes);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedCmbL<T, DATA>::IntermedCmbL
   (  std::unique_ptr<IntermedL<T, DATA> >&& aImL
   ,  std::unique_ptr<IntermedT<T, DATA> >&& aImT
   ,  const std::vector<In>& aOperModes
   )
   :  mIntermedL
         (  std::move(aImL)
         )
   ,  mIntermedT
         (  std::move(aImT)
         )
{
   this->UpdateInternalState(aOperModes);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedCmbL<T, DATA>::IntermedCmbL
   (  const IntermedCmbL<T, DATA>& aOrig
   )
   :  IntermedL<T, DATA>(aOrig)
   ,  mExci(aOrig.mExci)
   ,  mExciOper(aOrig.mExciOper)
   ,  mOcc(aOrig.mOcc)
   ,  mOccOper(aOrig.mOccOper)
   ,  mSumModes(aOrig.mSumModes)
   ,  mOperModes(aOrig.mOperModes)
   ,  mHighestModeNo(aOrig.mHighestModeNo)
{
   mIntermedL = midas::util::DynamicCastUniquePtr<IntermedL<T, DATA> >(aOrig.mIntermedL->Clone());
   mIntermedT = midas::util::DynamicCastUniquePtr<IntermedT<T, DATA> >(aOrig.mIntermedT->Clone());
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedCmbL<T, DATA>& IntermedCmbL<T, DATA>::operator=
   (  const IntermedCmbL<T, DATA>& aOrig
   )
{
   IntermedL<T, DATA>::operator=(aOrig);
   if (this == &aOrig)
      return *this;

   mIntermedL = midas::util::DynamicCastUniquePtr<IntermedL<T, DATA> >(aOrig.mIntermedL->Clone());
   mIntermedT = midas::util::DynamicCastUniquePtr<IntermedT<T, DATA> >(aOrig.mIntermedT->Clone());
   
   mExci          = aOrig.mExci;
   mExciOper      = aOrig.mExciOper;
   mOcc           = aOrig.mOcc;
   mOccOper       = aOrig.mOccOper;
   mSumModes      = aOrig.mSumModes;
   mOperModes     = aOrig.mOperModes;
   mHighestModeNo = aOrig.mHighestModeNo;

   return *this;
}

/**
 * Read intermediate specification between '[' and ']'.
 * When done, aPos point to character following the final ']'.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::GetIntermedSpec
   (  std::string::const_iterator& aPos
   ,  std::string& aSpec
   )
{
   if (*aPos != '[')
      MIDASERROR("IntermedCmbL<T, DATA>::GetIntermedSpec(): Expected '[' in aSpec.");
   aPos++;

   In brackets = I_1;
   while (aPos != aSpec.end())
   {
      char c = *(aPos++);
      if (c == '[')
         brackets++;
      else if (c == ']')
         brackets--;

      if (brackets == I_0)
         break;
      else
         aSpec.push_back(c);
   }
}

/**
 * Update the internal state, i.e. figure out what modes to sum and what modes to expose.
 *
 * Rules:
 * 1) By construction, all excited modes in T are also excited in L:
 *    a) Expose all other modes on L as purely excited.
 *    b) Sum all common modes not associated with operator.
 *    c) Expose remaining purely excited modes as occupied with no associated operator.
 *    d) Expose remaining forward modes on T as occupied with associated operator. 
 *
 * 2) Down contracted modes on T:
 *    a) If not on L, expose separately as occupied with associated operator.
 *    b) If on L, expose as excited with associated operator. Remove these modes from
 *       the list of exposed purely excited modes.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::UpdateInternalState
   (  const std::vector<In>& aOperModes
   )
{
   mExci.clear();
   mExciOper.clear();
   mOcc.clear();
   mOccOper.clear();
   mSumModes.clear();
   mOperModes.clear();
   
   std::vector<In> l_exci;                               // Excited modes on L.
   std::vector<In> t_exci;                               // All excited modes on T.
   std::vector<In> t_exci_noop;                          // Purely excited modes on T.
   std::vector<In> t_exci_op;                            // Forward contracted modes on T.
   std::vector<In> t_occ_oper;                           // Down contracted modes on T.
   mIntermedL->GetExciModes(l_exci);
   mIntermedT->GetPureExciModes(t_exci);
   mIntermedT->GetOperExciModes(t_exci);
   mIntermedT->GetPureExciModes(t_exci_noop);
   mIntermedT->GetOperExciModes(t_exci_op);
   mIntermedT->GetOperOccModes(t_occ_oper);
   std::sort(l_exci.begin(), l_exci.end());
   std::sort(t_exci.begin(), t_exci.end());
   std::sort(t_exci_noop.begin(), t_exci_noop.end());
   std::sort(t_exci_op.begin(), t_exci_op.end());
   std::sort(t_occ_oper.begin(), t_occ_oper.end());

   // Only some of the modes with associated operator indices may be relevant for summation.
   std::set_intersection(t_exci_noop.begin(), t_exci_noop.end(), aOperModes.begin(), aOperModes.end(), std::back_inserter(mOperModes));
   
   // Sec. (1a)
   std::set_difference(l_exci.begin(), l_exci.end(), t_exci.begin(), t_exci.end(), std::back_inserter(mExci));
   
   // Sec. (1b)
   std::vector<In> ltpe_cmn;    // Common modes between L and purely excited T.
   std::set_difference(t_exci_noop.begin(), t_exci_noop.end(), mOperModes.begin(), mOperModes.end(), std::back_inserter(mSumModes));

   // Sec. (1c)
   std::set_intersection(t_exci_noop.begin(), t_exci_noop.end(), mOperModes.begin(), mOperModes.end(), std::back_inserter(mOcc));
   
   // Sec. (1c) - for modes forward contracted on T.
   if (! t_exci_op.empty())
      mOccOper.push_back(t_exci_op);

   // Sec. (2a)
   std::vector<In> tmp;
   std::set_difference(t_occ_oper.begin(), t_occ_oper.end(), l_exci.begin(), l_exci.end(), std::back_inserter(tmp));
   if (! tmp.empty())
      mOccOper.push_back(tmp);

   // Sec. (2b)
   tmp.clear();
   std::set_intersection(t_occ_oper.begin(), t_occ_oper.end(), l_exci.begin(), l_exci.end(), std::back_inserter(mExciOper));
   std::set_difference(mExci.begin(), mExci.end(), mExciOper.begin(), mExciOper.end(), std::back_inserter(tmp));
   mExci = tmp;

   mHighestModeNo = HighestModeNo();
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::PrintInternalState
   (
   )  const
{
   using ::operator<<;
   Mout << "IntermedCmbL:" << std::endl
        << "   L:                " << *mIntermedL << std::endl
        << "   t:                " << *mIntermedT << std::endl
        << "   exci:             " << mExci << std::endl
        << "   exci,oper:        " << mExciOper << std::endl
        << "   occ:              " << mOcc << std::endl
        << "   occ,oper:         ";
   for (In i=I_0; i<mOccOper.size(); ++i)
      Mout << mOccOper[i];
   Mout << std::endl
        << "   sum modes:        " << mSumModes << std::endl
        << "   oper modes:       " << mOperModes << std::endl
        << "   highest mode no.: " << mHighestModeNo << std::endl
        << "   scaling:          " << GetScaling() << std::endl;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCmbL<T, DATA>::AssignConcreteModesOpers
   (  const std::vector<In>& aCmodes
   ,  const std::vector<In>& aCopers
   )
{
   mCmodes = aCmodes;
   mCopers = aCopers;

   std::vector<In> exci_mc;
   exci_mc.reserve(mExci.size()+mExciOper.size());
   for (In i=I_0; i<mExci.size(); ++i)
      exci_mc.push_back(aCmodes[mExci[i]]);
   for (In i=I_0; i<mExciOper.size(); ++i)
      exci_mc.push_back(aCmodes[mExciOper[i]]);
   std::sort(exci_mc.begin(), exci_mc.end());
   for (In i=I_1; i<exci_mc.size(); ++i)
      if (exci_mc[i] == exci_mc[i-I_1])
         return false;         // Some concrete modes appear twice.
   this->mExciMc.ReInit(exci_mc);
   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCmbL<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  GeneralMidasVector<T>& aRes
   )  const
{
   In res_size = I_1;
   for (In i=I_0; i<this->mExciMc.Size(); ++i)
   {
      res_size *= (aEvalData.NModals(this->mExciMc.Mode(i)) - I_1);
   }
   aRes.SetNewSize(res_size);
   aRes.Zero();

   std::vector<In> c_modes(mHighestModeNo + I_1);
   // ian: fixed an overcounting in mCmodes... hope its correct (tests seem fine)...
   In size = std::min(c_modes.size(), mCmodes.size());
   //for (In i=I_0; i<mHighestModeNo+I_1; ++i)
   for (In i=I_0; i<size; ++i)
   {
      c_modes[i] = mCmodes[i];
   }

   auto fn = std::bind(std::mem_fn(&IntermedCmbL<T, DATA>::EvalContrib), this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
   
   DoSum(aEvalData, I_0, I_0, c_modes, fn, aRes);
  
   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCmbL<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  NiceTensor<T>& aRes
   )  const
{
   std::vector<unsigned> l_dims(this->mExciMc.Size());
   for(size_t i=0; i<l_dims.size(); ++i)
   {
      l_dims[i] = aEvalData.NModals(this->mExciMc.Mode(i)) - I_1;
   }

   if (  aRes.IsNullPtr()
      )
   {
      Mout  << " IntermedCmbL: aRes is nullptr! Initialize..." << std::endl;
      bool cp = false;

      if constexpr   (  DataAllowsDecompositionV<evaldata_t>
                     )
      {
         cp = aEvalData.DecomposedTensors();
      }

      auto tens_type =  cp
                     ?  BaseTensor<T>::typeID::CANONICAL
                     :  BaseTensor<T>::typeID::SIMPLE;

      aRes = l_dims.empty() ? NiceScalar<T>(0., cp) : NiceTensor<T>(l_dims, tens_type);                                                                   
   }

   // ian: fixed an overcounting in mCmodes... hope its correct (tests seem fine)...
   std::vector<In> c_modes(this->mHighestModeNo + I_1);
   In size = std::min(c_modes.size(), this->mCmodes.size());

   for (In i=I_0; i<size; ++i)
   {
      c_modes[i] = this->mCmodes[i];
   }

   auto fn =   std::bind
                  (  std::mem_fn
                        (  &IntermedCmbL<T, DATA>::EvalContribTensor
                        )
                  ,  this
                  ,  std::placeholders::_1
                  ,  std::placeholders::_2
                  ,  std::placeholders::_3
                  );
   
   std::unique_ptr<TensorSumAccumulator<T> > sum_acc = nullptr;
   if constexpr   (  DataAllowsDecompositionV<evaldata_t>
                  )
   {
      sum_acc  = std::make_unique<TensorSumAccumulator<T> > 
               (  aRes.GetDims()
               ,  aEvalData.GetDecomposer()
               ,  aEvalData.GetAllowedRank()
               );
   }
   else
   {
      sum_acc  = std::make_unique<TensorSumAccumulator<T> > 
               (  aRes.GetDims()
               );
   }

   DoSum(aEvalData, I_0, I_0, c_modes, fn, *sum_acc);

   aRes = sum_acc->EvaluateSum();

   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
template
   <  class F
   ,  class... Ts
   >
void IntermedCmbL<T, DATA>::DoSum
   (  const evaldata_t& aEvalData
   ,  In aSumIdx
   ,  In aFirstMode
   ,  std::vector<In>& aCmodes
   ,  F func
   ,  Ts&&... t
   )  const
{
   if (aSumIdx > In(mSumModes.size())-I_1)
   {
      func(aEvalData, aCmodes, std::forward<Ts>(t)...);
      return;
   }

   for (In cmode=aFirstMode; cmode<aEvalData.NModes(); ++cmode)
   {
      aCmodes[mSumModes[aSumIdx]] = cmode;
      DoSum(aEvalData, aSumIdx+I_1, cmode+I_1, aCmodes, func, std::forward<Ts>(t)...);
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::EvalContrib
   (  const evaldata_t& aEvalData
   ,  std::vector<In>& aCmodes
   ,  GeneralMidasVector<T>& aRes
   )  const
{
   if constexpr   (  DataAllowsMidasVectorV<evaldata_t>
                  )
   {
      auto& intermeds = aEvalData.template GetIntermediates<GeneralMidasVector>();

      if (! (mIntermedL->AssignConcreteModesOpers(aCmodes, mCopers) &&
             mIntermedT->AssignConcreteModesOpers(aCmodes, mCopers)))
         return;

      GeneralMidasVector<T> l_res;
      GeneralMidasVector<T> t_res;
      if (! (intermeds.GetIntermed(*mIntermedL, aEvalData, l_res) &&
             intermeds.GetIntermed(*mIntermedT, aEvalData, t_res)))
         return;
  
      ModeCombi l_mc;
      ModeCombi t_mc;
      mIntermedL->GetExciMc(l_mc);
      mIntermedT->GetExciMc(t_mc);

      GenDownCtr<T, DATA> gd_ctr(aEvalData, l_mc, t_mc);
      gd_ctr.Contract(l_res, t_res, aRes);
      
      return;
   }
   else
   {
      MIDASERROR("Not implemented for given DATA type!");
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::EvalContribTensor
   (  const evaldata_t& aEvalData
   ,  std::vector<In>& aCmodes
   ,  TensorSumAccumulator<T>& aRes
   )  const
{
   if constexpr   (  DataAllowsNiceTensorV<evaldata_t>
                  )
   {
      auto& intermeds = aEvalData.template GetIntermediates<NiceTensor>();

      // Return if we cannot assign
      if (  !  (  mIntermedL->AssignConcreteModesOpers(aCmodes, mCopers)
               && mIntermedT->AssignConcreteModesOpers(aCmodes, mCopers)
               )
         )
      {
         return;
      }

      // Get the intermeds
      NiceTensor<T> l_res;
      NiceTensor<T> t_res;
      if (  !  (  intermeds.GetIntermed(*mIntermedL, aEvalData, l_res)
               && intermeds.GetIntermed(*mIntermedT, aEvalData, t_res)
               )
         )
      {
         return;
      }
  
      ModeCombi l_mc;
      ModeCombi t_mc;
      mIntermedL->GetExciMc(l_mc);
      mIntermedT->GetExciMc(t_mc);

      TensorGeneralDownContraction<T, DATA> gd_ctr(aEvalData, l_mc, t_mc);

      aRes += gd_ctr.Contract(l_res, t_res);

      return;
   }
   else
   {
      MIDASERROR("Not implemented for given DATA type!");
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::EvalNorm2
   (  const evaldata_t& aEvalData
   ,  std::vector<In>& aCmodes
   ,  real_t& aLNorm2
   ,  real_t& aTNorm2
   )  const
{
   if (! (mIntermedL->AssignConcreteModesOpers(aCmodes, mCopers) &&
          mIntermedT->AssignConcreteModesOpers(aCmodes, mCopers)))
      return;
   
   real_t new_lnorm2 = 0, new_tnorm2 = 0;

   if (  aEvalData.TensorTransform()
      )
   {
      if constexpr   (  DataAllowsNiceTensorV<evaldata_t>
                     )
      {
         if(! (aEvalData.template GetIntermediates<NiceTensor>().GetIntermedNorm2(*mIntermedL, aEvalData, new_lnorm2) &&
               aEvalData.template GetIntermediates<NiceTensor>().GetIntermedNorm2(*mIntermedT, aEvalData, new_tnorm2)    ))
            return;
      }
      else
      {
         MIDASERROR("Not implemented for given DATA type!");
      }
   }
   else
   {
      if constexpr   (  DataAllowsMidasVectorV<evaldata_t>
                     )
      {
         if(! (aEvalData.template GetIntermediates<GeneralMidasVector>().GetIntermedNorm2(*mIntermedL, aEvalData, new_lnorm2) &&
               aEvalData.template GetIntermediates<GeneralMidasVector>().GetIntermedNorm2(*mIntermedT, aEvalData, new_tnorm2)    ))
            return;
      }
      else
      {
         MIDASERROR("Not implemented for given DATA type!");
      }
   }

   aLNorm2+=new_lnorm2;
   aTNorm2+=new_tnorm2;
   
   return;
}  

/**
 * Store concrete modes in aCmodes. The order is:
 * [Excited w/o operator] [Excited w/ operator] [Occupied w/o operator] [Occupied w/ operator].
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::GetConcreteModes
   (  std::vector<In>& aCmodes
   )  const
{
   // Reserve space.
   In nm = mExci.size() + mExciOper.size() + mOcc.size();
   for (In i=I_0; i<mOccOper.size(); ++i)
      nm += mOccOper[i].size();
   aCmodes.reserve(nm);
   aCmodes.resize(I_0);

   // Add concrete mode numbers.
   for (In i=I_0; i<mExci.size(); ++i)
      aCmodes.push_back(mCmodes[mExci[i]]);
   std::sort(aCmodes.begin(), aCmodes.end());

   std::vector<In>::iterator it = aCmodes.end();
   for (In i=I_0; i<mExciOper.size(); ++i)
      aCmodes.push_back(mCmodes[mExciOper[i]]);
   std::sort(it, aCmodes.end());

   it = aCmodes.end();
   for (In i=I_0; i<mOcc.size(); ++i)
      aCmodes.push_back(mCmodes[mOcc[i]]);
   std::sort(it, aCmodes.end());

   for (In i=I_0; i<mOccOper.size(); ++i)
   {
      it = aCmodes.end();
      for (In k=I_0; k<mOccOper[i].size(); ++k)
         aCmodes.push_back(mCmodes[mOccOper[i][k]]);
      std::sort(it, aCmodes.end());
   }
}

/**
 * Store concrete operator in aCopers. The order is:
 * [Excited] [Occupied].
 * Operators must be ordered according to the corresponding concrete mode numbers.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::GetConcreteOpers
   (  std::vector<In>& aCopers
   )  const
{
   // Reserve space.
   In no = mExciOper.size();
   for (In i=I_0; i<mOccOper.size(); ++i)
      no += mOccOper[i].size();
   aCopers.reserve(no);
   aCopers.resize(I_0);
   
   std::vector<std::pair<In,In> > mo;               // (mode,operator) pairs.
   mo.reserve(no);

   for (In i=I_0; i<mExciOper.size(); ++i)
      mo.push_back(std::make_pair(mCmodes[mExciOper[i]], mCopers[mExciOper[i]]));
   std::sort(mo.begin(), mo.end());
   for (In i=I_0; i<mo.size(); ++i)
      aCopers.push_back(mo[i].second);

   for (In i=I_0; i<mOccOper.size(); ++i)
   {
      mo.clear();
      for (In k=I_0; k<mOccOper[i].size(); ++k)
         mo.push_back(std::make_pair(mCmodes[mOccOper[i][k]], mCopers[mOccOper[i][k]]));
      std::sort(mo.begin(), mo.end());
      for (In i=I_0; i<mo.size(); ++i)
         aCopers.push_back(mo[i].second);
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCmbL<T, DATA>::CmpKind
   (  const Intermediate<T, DATA>* const aIntermed
   )  const
{
   const IntermedCmbL<T, DATA>* const other = dynamic_cast<const IntermedCmbL<T, DATA>* >(aIntermed);
   if (  !other
      )
   {
      return false;
   }

   if (mOccOper.size() == other->mOccOper.size())
   {
      for (In i=I_0; i<mOccOper.size(); ++i)
      {
         if (mOccOper[i].size() != other->mOccOper[i].size())
         {
            return false;
         }
      }
   }
   else
   {
      return false;
   }
   
   return mIntermedL->CmpKind(other->mIntermedL.get())   &&
          mIntermedT->CmpKind(other->mIntermedT.get())   &&
          mExci.size()      == other->mExci.size()       &&
          mExciOper.size()  == other->mExciOper.size()   &&
          mOcc.size()       == other->mOcc.size()        &&
          mOperModes.size() == other->mOperModes.size();
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::RegisterInternalIntermeds
   (  IntermediateMachine<T, GeneralMidasVector, DATA>& aImMachine
   )
{
   mIntermedL->RegisterInternalIntermeds(aImMachine);
   mIntermedT->RegisterInternalIntermeds(aImMachine);
   aImMachine.RegisterIntermediate(*mIntermedL);
   aImMachine.RegisterIntermediate(*mIntermedT);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::RegisterInternalIntermeds
   (  IntermediateMachine<T, NiceTensor, DATA>& aImMachine
   )
{
   mIntermedL->RegisterInternalIntermeds(aImMachine);
   mIntermedT->RegisterInternalIntermeds(aImMachine);
   aImMachine.RegisterIntermediate(*mIntermedL);
   aImMachine.RegisterIntermediate(*mIntermedT);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::GetModes
   (  std::vector<In>& aModes
   )  const
{
   std::copy(mExci.begin(), mExci.end(), std::back_inserter(aModes));
   std::copy(mExciOper.begin(), mExciOper.end(), std::back_inserter(aModes));
   std::copy(mOcc.begin(), mOcc.end(), std::back_inserter(aModes));
   for (In i=I_0; i<mOccOper.size(); ++i)
      std::copy(mOccOper[i].begin(), mOccOper[i].end(), std::back_inserter(aModes));
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::GetExciModes
   (  std::vector<In>& aModes
   )  const
{
   std::copy(mExci.begin(), mExci.end(), std::back_inserter(aModes));
   std::copy(mExciOper.begin(), mExciOper.end(), std::back_inserter(aModes));
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::GetOccModes
   (  std::vector<In>& aModes
   )  const
{
   std::copy(mOcc.begin(), mOcc.end(), std::back_inserter(aModes));
   for (In i=I_0; i<mOccOper.size(); ++i)
      std::copy(mOccOper[i].begin(), mOccOper[i].end(), std::back_inserter(aModes));
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::GetOperOccModes
   (  std::vector<In>& aModes
   )  const
{
   for (In i=I_0; i<mOccOper.size(); ++i)
      std::copy(mOccOper[i].begin(), mOccOper[i].end(), std::back_inserter(aModes));
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
In IntermedCmbL<T, DATA>::HighestModeNo
   (
   )  const
{  
   std::vector<In> modes = mSumModes;   // Simple but a little inefficient due to copying of vectors.
   GetModes(modes);
   In high = -I_1;
   for (In i=I_0; i<modes.size(); ++i)
      if (modes[i] > high)
         high = modes[i];
   return high;
}  

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::WriteSpec
   (  std::ostream& aOut
   )  const
{
   using ::operator<<;
   aOut << "cmbl[";
   mIntermedL->WriteSpec(aOut);
   aOut << "][";
   mIntermedT->WriteSpec(aOut);
   aOut << "]" << mOperModes;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedCmbL<T, DATA>::GetScaling
   (  bool aCpTensors
   )  const
{
   Scaling number = GetNumber();
   Scaling cost = GetCost(aCpTensors);
   return Scaling(number.mM+cost.mM, number.mO+cost.mO, std::max(cost.mN, number.mN));
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedCmbL<T, DATA>::GetCost
   (  bool aCpTensors
   )  const
{
   if (  aCpTensors
      )
   {
      MIDASERROR("Scaling of IntermedCmbL not implemented for CP tensors!");
   }
   std::vector<In> l_modes;
   mIntermedL->GetExciModes(l_modes);                      // Quite inefficient way to get number
   return Scaling(mSumModes.size(), I_0, l_modes.size());  // of modes in <L|
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedCmbL<T, DATA>::GetNumber
   (
   )  const
{
   In m = mExci.size() + mExciOper.size() + mOcc.size();
   In o = mExciOper.size();
   In n = mExci.size() + mExciOper.size();
   for (In i=I_0; i<mOccOper.size(); ++i)
   {
      m += mOccOper[i].size();
      o += mOccOper[i].size();
   }
   return Scaling(m, o, n);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::ostream& IntermedCmbL<T, DATA>::Print
   (  std::ostream& aOut
   )  const
{
   using ::operator<<;
   aOut << "cmbl[" << *mIntermedL << "][" << *mIntermedT << "]" << mOperModes
        << "{id:" << this->mRefNo << "}";
   return aOut;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::Latex
   (  std::ostream& aOut
   )  const
{
   aOut << "IntermedCmbL<T, DATA>::Latex(): Not implemented";
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmbL<T, DATA>::PrintScaling
   (  std::ostream& aOut
   )  const
{  
   Scaling number = GetNumber();  
   Scaling cost = GetCost(); // Also with CP ?
   Scaling tot = GetScaling(false);
//   Scaling cp = GetScaling(true);
   aOut << "n: " << number << "   c: " << cost << "   t: " << tot;
}  

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCmbL<T, DATA>::Norm2
   (  const evaldata_t& aEvalData
   ,  real_t& aNorm2
   )  const
{
   aNorm2 = 1.0;

   std::vector<In> c_modes(mHighestModeNo + I_1);
   // ian: fixed an overcounting in mCmodes... hope its correct (tests seem fine)...
   In size = std::min(c_modes.size(), mCmodes.size());
   //for (In i=I_0; i<mHighestModeNo+I_1; ++i)
   for (In i=I_0; i<size; ++i)
   {
      c_modes[i] = mCmodes[i];
   }

   auto fn = std::bind(mem_fn(&IntermedCmbL<T, DATA>::EvalNorm2),this,
         std::placeholders::_1,std::placeholders::_2,std::placeholders::_3,std::placeholders::_4);

   real_t l_norm2 = 0.0;
   real_t t_norm2 = 0.0;
   DoSum(aEvalData, I_0, I_0, c_modes, fn, l_norm2, t_norm2);
   
   aNorm2 *= (l_norm2*t_norm2);

   return true;
}

}  /* namespace midas::vcc::v3 */

#endif /* INTERMEDCMBL_IMPL_H_INCLUDED */
