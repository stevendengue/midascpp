/**
************************************************************************
* 
* @file                IntermedCoef_Impl.h
*
* Created:             10-11-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*                      Niels Kristian Madsen (nielskm@chem.au.dk), templating
*
* Short Description:   Coefficient intermediate allowing iteration over
*                      operators.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDCOEF_IMPL_H_INCLUDED
#define INTERMEDCOEF_IMPL_H_INCLUDED

#include <iostream>
#include <algorithm>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "IntermediateOperIter.h"
#include "IntermedProd.h"
#include "vcc/TransformerV3.h"
#include "input/OpDef.h"
#include "tensor/Scalar.h"
#include "EvalDataTraits.h"

#include "libmda/numeric/float_eq.h"



namespace midas::vcc::v3
{

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedCoef<T, DATA>::IntermedCoef
   (
   )
   :  IntermediateOperIter<T, DATA>()
   ,  mCurTerm(-I_1)
   ,  mFinalTerm(-I_1)
{
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedCoef<T, DATA>::IntermedCoef
   (  const std::string& aSpec
   )
   :  IntermediateOperIter<T, DATA>()
   ,  mCurTerm(-I_1)
   ,  mFinalTerm(-I_1)
{
   std::string::const_iterator pos = aSpec.begin();
   detail::ReadVector(pos, mModes);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedCoef<T, DATA>::IntermedCoef
   (  const IntermedCoef<T, DATA>& aOrig
   )
   :  IntermediateOperIter<T, DATA>(aOrig)
   ,  mModes(aOrig.mModes)
   ,  mMc(aOrig.mMc)
   ,  mCGmodeMap(aOrig.mCGmodeMap)
   ,  mCurTerm(aOrig.mCurTerm)
   ,  mFinalTerm(aOrig.mFinalTerm)
{
}

/**
 *
 **/
namespace intermed_coef::detail
{
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::vector<In>& GetCVec
   (
   )
{
   static std::vector<In> c_vec;
   return c_vec;
}
} /* namespace intermed_coef::detail */
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCoef<T, DATA>::AssignConcreteModes
   (  const std::vector<In>& aCmodes
   )
{
   mCGmodeMap.clear();
   intermed_coef::detail::GetCVec<T, DATA>().resize(mModes.size());
   for (In m=I_0; m<mModes.size(); ++m)
   {
      In c_mode = aCmodes[mModes[m]];            // Concrete mode.
      intermed_coef::detail::GetCVec<T, DATA>()[m] = c_mode;
      mCGmodeMap.emplace_back(c_mode, mModes[m]);
   }
   std::sort(intermed_coef::detail::GetCVec<T, DATA>().begin(), intermed_coef::detail::GetCVec<T, DATA>().end());
   mMc.ReInit(intermed_coef::detail::GetCVec<T, DATA>());
   std::sort(mCGmodeMap.begin(), mCGmodeMap.end());
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCoef<T, DATA>::GetConcreteModes
   (  std::vector<In>& aCmodes
   )  const
{
   aCmodes = mMc.MCVec();
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCoef<T, DATA>::GetConcreteOpers
   (  std::vector<In>& aCopers
   )  const
{
   MIDASERROR("IntermedCoef<T, DATA>::GetConcreteOpers(): Not implemented. Use iteration.");
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCoef<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  GeneralMidasVector<T>& aRes
   )  const
{
   T coef = aEvalData.OperatorCoef(mCurTerm);
   aRes.SetNewSize(I_1);
   aRes[I_0] = coef;
   if (coef==C_0)
      return false;
   else
      return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCoef<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  NiceTensor<T>& aRes
   )  const
{
   T coef = aEvalData.OperatorCoef(mCurTerm);

   bool cp_tensor = false;
   if constexpr   (  DataAllowsDecompositionV<evaldata_t>
                  )
   {
      cp_tensor = aEvalData.DecomposedTensors();
   }

   aRes = NiceScalar<T>(coef, cp_tensor);
   if (coef==C_0)
      return false;
   else
      return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::ostream& IntermedCoef<T, DATA>::Print
   (  std::ostream& aOut
   )  const
{
   using ::operator<<;
   aOut << "Cit";
   aOut << "(";
   std::copy(mModes.begin(), mModes.end(), std::ostream_iterator<In>(aOut, ""));
   aOut << ")";
   return aOut;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCoef<T, DATA>::Latex
   (  std::ostream& aOut
   )  const
{
   std::vector<In> modes(mModes);
   std::sort(modes.begin(), modes.end());
   aOut << "c_{";
   for (In i=I_0; i<modes.size(); ++i)
   {
      aOut << "m_{" << modes[i] << "}";
   }
   aOut << "}";
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCoef<T, DATA>::WriteSpec
   (  std::ostream& aOut
   )  const
{
   using ::operator<<;
   aOut << "c" << mModes;
}

/**
 * Initialize operator iterator.
 * @param aEvalData       Data to evaluate for.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCoef<T, DATA>::InitOperIter
   (  const evaldata_t& aEvalData
   )
{
   In first_term = I_0;
   In n_terms = I_0;
   if (  !aEvalData.TermsForMc(mMc, first_term, n_terms)
      )
   {
      mFinalTerm = -I_1;
      return false;
   }
   mCurTerm = first_term-I_1;
   mFinalTerm = first_term+n_terms-I_1;
   return true;
}

/**
 * Same as InitOperIter(const evaldata_t&).
 * @param aEvalData Data to evaluate for. 
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCoef<T, DATA>::InitOperIterTensor
   (  const evaldata_t& aEvalData
   )
{
   In first_term = I_0;
   In n_terms = I_0;
   if (  !aEvalData.TermsForMc(mMc, first_term, n_terms)
      )
   {
      mFinalTerm = -I_1;
      return false;
   }
   mCurTerm = first_term-I_1;
   mFinalTerm = first_term+n_terms-I_1;
   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCoef<T, DATA>::AssignNextOpers
   (  const evaldata_t& aEvalData
   ,  std::vector<LocalOperNr>& aCopers
   )
{
   mCurTerm++;
   if (mCurTerm > mFinalTerm)
      return false;
   
   aCopers = aEvalData.GetOpers(mCurTerm);
   
   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCoef<T, DATA>::AssignNextOpersTensor
   (  const evaldata_t& aEvalData
   ,  std::vector<LocalOperNr>& aCopers
   )
{
   return this->AssignNextOpers(aEvalData, aCopers);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCoef<T, DATA>::AssignOpers
   (  const std::vector<LocalOperNr>& aCopers
   ,  std::vector<In>& aGenOpers
   )  const
{
   // aCopers is sorted according to concrete mode numbers.
   // Translate concrete mode numbers to general mode numbers.
   // Store operator at this index in aGenOpers.
   for (In o=I_0; o<aCopers.size(); ++o)
   {
      In g_mode = mCGmodeMap[o].second;
      aGenOpers[g_mode] = aCopers[o];
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCoef<T, DATA>::CmpKind
   (  const Intermediate<T, DATA>* const aIntermed
   )  const
{
   const IntermedCoef<T, DATA>* const other = dynamic_cast<const IntermedCoef<T, DATA>* >(aIntermed);
   if (  !other
      )
   {
      return false;
   }

   return mModes.size() == other->mModes.size();
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
In IntermedCoef<T, DATA>::HighestModeNo
   (
   )  const
{
   In high = -I_1;
   for (In i=I_0; i<mModes.size(); ++i)
   {
      if (mModes[i] > high)
      {
         high = mModes[i];
      }
   }
   return high;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedCoef<T, DATA>::GetScaling
   (  bool aCpTensors
   )  const
{
   return Scaling(mModes.size(), mModes.size(), I_0);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedCoef<T, DATA>::GetCost
   (  bool aCpTensors
   )  const
{     
   return Scaling(I_0, I_0, I_0);      
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedCoef<T, DATA>::GetNumber
   (
   )  const
{
   return Scaling(mModes.size(), mModes.size(), I_0);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCoef<T, DATA>::Norm2
   (  const evaldata_t& aEvalData
   ,  real_t& aNorm2
   )  const
{ 
   aNorm2 = midas::util::AbsVal2(aEvalData.OperatorCoef(mCurTerm));
   if (  libmda::numeric::float_eq(aNorm2, real_t(0.))
      )
   {
      return false;
   }
   else
   {
      return true; 
   }
}

} /* namespace midas::vcc::v3 */

#endif /* INTERMEDCOEF_IMPL_H_INCLUDED */
