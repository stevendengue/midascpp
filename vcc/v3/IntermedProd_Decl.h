/**
************************************************************************
* 
* @file                IntermedProd.h
*
* Created:             07-10-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Representing a product of intermediates.
* 
* Last modified: Mon Mar 22, 2010  04:25PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDPROD_DECL_H
#define INTERMEDPROD_DECL_H

#include <vector>
#include <iostream>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/MultiIndex.h"
#include "vcc/v3/V3Contrib.h"
#include "vcc/v3/Intermediate.h"
#include "vcc/v3/IntermedExci.h"
#include "vcc/v3/IntermediateOperIter.h"
#include "vcc/v3/ModeSum.h"
#include "vcc/v3/Scaling.h"
#include "vcc/v3/IntermediateMachine.h"
#include "vcc/TensorSumAccumulator.h"
#include "tensor/NiceTensor.h"

namespace midas::vcc::v3
{

/**
 * V3 contribution which is a product of intermediates.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermedProd
   :  public V3Contrib<T, DATA>
{
   public:
      //! Alias
      using evaldata_t = DATA<T>;
      using Base = V3Contrib<T, DATA>;
      using real_t = midas::type_traits::RealTypeT<T>;

   private:
      size_t screened = 0;
      size_t not_screened = 0;
      size_t total = 0;
      
      void incr_screened()     { ++screened; }
      void incr_not_screened() { ++not_screened; }
      void incr_total()        { ++total; }
      
   protected:
      //! Coefficient in front of term
      T mCoef;
      
      //! Sums over modes and operators.
      std::vector<ModeSum> mSums;

      //! Intermediates to be evaluated
      std::vector<IntermedPtr<T, DATA> > mIntermeds;

      //! Intermediate for iterating over operators.
      std::unique_ptr<IntermediateOperIter<T, DATA> > mOpIterIntermed = nullptr;

      //! Distribution of concrete modes from bra state.
      std::vector<std::vector<In> > mModeGuides;
    
      //! Special intermediate used in case of left-hand transformations.
      std::unique_ptr<IntermedExci<T, DATA> > mIntermedTau = nullptr;
      
      //! Total excitation level.
      In mExciLevel;

      //! Concrete mode numbers.
      std::vector<In> mCmodes;
   
      //! Assume that mOpIterIntermed is stored by intermediate machine. Affects scaling results.
      bool mAssumeOpItStored = false;
      

      //! Set modes guides as specified by aGuides std::string.
      virtual void AddModeGuides
         (  const std::string& aGuides
         );
      
      //! Set sizes and values of internal variables based on intermediates.
      virtual void UpdateInternalState
         (
         );
   
      //! Evaluate sums of IntermedProd for given mode combination.
      //@{
      virtual void DoSum
         (  const evaldata_t& aEvalData
         ,  In aSum
         ,  In aSumIdx
         ,  In aFirstMode
         ,  const ModeCombi& aMp
         ,  GeneralMidasVector<T>& arRes
         );

      virtual void DoSum
         (  const evaldata_t& aEvalData
         ,  In aSum
         ,  In aSumIdx
         ,  In aFirstMode
         ,  const ModeCombi& aMp
         ,  NiceTensor<T>& arRes
         );

      virtual void DoSum
         (  const evaldata_t& aEvalData
         ,  In aSum
         ,  In aSumIdx
         ,  In aFirstMode
         ,  const ModeCombi& aMp
         ,  TensorSumAccumulator<T>& arRes
         );
      //@}
      
      /** @name Loop over operators */
      //!@{
      virtual void LoopOperators
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMp
         ,  GeneralMidasVector<T>& arRes
         );

      virtual void LoopOperators
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMp
         ,  NiceTensor<T>& arRes
         );

      virtual void LoopOperators
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMp
         ,  TensorSumAccumulator<T>& arRes
         );
      //!@}
   
      /** @name Evaluate intermediates */
      //!@{
      virtual void EvalIntermeds
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMp
         ,  GeneralMidasVector<T>& arRes
         ,  const std::vector<In>& aOpers
         ,  const GeneralMidasVector<T>& aOpItRes
         );

      virtual void EvalIntermeds
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMp
         ,  NiceTensor<T>& arRes
         ,  const std::vector<In>& aOpers
         ,  const NiceTensor<T>& arOpItRes
         );

      virtual void EvalIntermeds
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMp
         ,  TensorSumAccumulator<T>& arRes
         ,  const std::vector<In>& aOpers
         ,  const NiceTensor<T>& arOpItRes
         );
      //!@}
   
      /**
       * Functions for evaluating product in case of a left-hand transformation.
       * NB: These are only for debugging and testing!!!
       * The left transform is handled by IntermedProdL.
       **/
      //!@{
      template
         <  typename D = DATA<T>
         >
      void EvaluateLeft
         (  const evaldata_t& aEvalData
         ,  GeneralDataCont<T>& arDcOut
         ,  std::enable_if_t<std::is_same_v<D, VccEvalData<Nb> > >* = nullptr
         );

      template
         <  typename D = DATA<T>
         >
      void EvaluateMpLeft
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMp
         ,  const GeneralMidasVector<T>& aLvec
         ,  GeneralDataCont<T>& arDcOut
         ,  std::enable_if_t<std::is_same_v<D, VccEvalData<Nb> > >* = nullptr
         );

      template
         <  typename D = DATA<T>
         >
      void DoSumLeft
         (  const evaldata_t& aEvalData
         ,  In aSum
         ,  In aSumIdx
         ,  In aFirstMode
         ,  const ModeCombi& aMp
         ,  const GeneralMidasVector<T>& aLvec
         ,  GeneralDataCont<T>& arDcOut
         ,  std::enable_if_t<std::is_same_v<D, VccEvalData<Nb> > >* = nullptr
         );

      template
         <  typename D = DATA<T>
         >
      void LoopOperatorsLeft
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMp
         ,  const GeneralMidasVector<T>& aLvec
         ,  GeneralDataCont<T>& arDcOut
         ,  std::enable_if_t<std::is_same_v<D, VccEvalData<Nb> > >* = nullptr
         );
      //!@}
        
      //! Identify combined intermediates using a recursive algorithm
      void IdentifyCmbIntermeds
         (  std::unique_ptr<IntermedProd>&& arImProd
         ,  std::vector<Scaling>& arScalings
         ,  std::vector<std::pair<std::unique_ptr<IntermedProd>, std::vector<Scaling> > >& arFinalList
         ,  bool aCpTensors
         )  const;
   
      //! Screening (only enabled for Nb and VccEvalData)
      bool ScreenIntermedsAmps
         (  const evaldata_t&
         )  const;
      
      /**
       * @brief Evaluate the value of this product given the left-hand excitation ModeCombi denoted aMp.
       * @param aEvalData Data for evaluation.
       * @param aMp       Specific mode combination to evaluate for.
       * @param arRes     On output: Result of evaluation (will be added additively to full result vector).
       **/
      //!@{
      virtual void EvaluateMp
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMp
         ,  GeneralMidasVector<T>& arRes
         );

      virtual void EvaluateMp
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMp
         ,  NiceTensor<T>& arRes
         );

      virtual void EvaluateMp
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMp
         ,  TensorSumAccumulator<T>& arRes
         );
      //!@}
   
   public: 
      //! Deleted default ctor
      IntermedProd() = delete; 
      
      //! Copy constructor.
      IntermedProd(const IntermedProd&);

      //! Copy assignment.
      IntermedProd& operator=(const IntermedProd&);

      //! c-tor from std::string vector
      IntermedProd(const std::vector<std::string>& aSpec);

      //! Clone function.
      std::unique_ptr<Base> Clone
         (
         )  const override
      {
         return std::make_unique<IntermedProd>(*this);
      }
      
      /** @name Evaluate interface **/
      //!@{

      //! Evaluate products. Results are added to the arDcOut data container.
      void Evaluate
         (  const evaldata_t& aEvalData
         ,  GeneralDataCont<T>& arDcOut
         )  override;

      //! Evaluate products for mode combi. Results are added to the arRes NiceTensor.
      void Evaluate
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMc
         ,  GeneralMidasVector<T>& arRes
         )  override;

      //! Evaluate products. Results are added to the arDcOut tensor data container.
      void Evaluate
         (  const evaldata_t& aEvalData
         ,  GeneralTensorDataCont<T>& arDcOut
         )  override;

      //! Evaluate products for mode combi. Results are added to the arRes NiceTensor.
      void Evaluate
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMc
         ,  NiceTensor<T>& arRes
         )  override;

      //! Evaluate products for mode combi. Results are added to the arRes tensor sum.
      void Evaluate
         (  const evaldata_t& aEvalData
         ,  const ModeCombi& aMc
         ,  TensorSumAccumulator<T>& arRes
         )  override;
      //!@}
      
      //! register intermediates with instance of IntermediateMachine for later re-use
      template
         <  template<typename> typename VECTOR
         >
      void RegisterIntermeds
         (  IntermediateMachine<T, VECTOR, DATA>& aInMachine
         );
   
      //! Identify intermediates and modify this product to use them.
      virtual void IdentifyCmbIntermeds
         (  Scaling& arMaxScaling
         ,  bool = false
         );
     
      //! Append all modes in product to arModes.
      virtual void GetAllModes
         (  std::vector<In>& arModes
         )  const;
      
      //! Append all excited modes in product to arModes.
      virtual void GetAllExciModes
         (  std::vector<In>& arModes
         )  const;
   
      //! Get scaling of IntermedProd
      virtual Scaling GetScaling
         (  bool = false
         )  const;
      
      //! Print term in latex math code.
      void Latex
         (  std::ostream& aOut
         )  const override; 

      //! Print term.
      std::ostream& Print
         (  std::ostream& aOut
         )  const override;

      //! Write something :D
      void WriteSpec
         (  std::ostream& aOut
         )  const override;
      
      //!@{
      //! Screening statistics.
      In Screened()    const override { return screened; }
      In NotScreened() const override { return not_screened; }
      In Total()       const override { return total; }
      //!@}
      
      //! Get Excitation level.
      In ExciLevel
         (
         )  const override
      {
         return mExciLevel;
      }

      //! Get type.
      std::string Type
         (
         )  const override
      {
         return "IntermedProd";
      }
};

} /* namespace midas::vcc::v3 */

#endif /* INTERMEDPROD_DECL_H */
