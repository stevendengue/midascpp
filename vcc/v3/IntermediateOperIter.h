/**
************************************************************************
* 
* @file                IntermediateOperIter.h
*
* Created:             10-11-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Interfaces for intermediates allowing iteration
*                      over operators.
* 
* Last modified: Wed Feb 03, 2010  11:40AM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDIATEOPERITER_H_INCLUDED
#define INTERMEDIATEOPERITER_H_INCLUDED

#include <vector>
#include <map>
#include <iostream>
#include <string>
#include <algorithm>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "Intermediate.h"
#include "input/ModeCombi.h"
#include "Scaling.h"
#include "vcc/v3/VccEvalData.h"
#include "vcc/TensorSumAccumulator.h"

namespace midas::vcc::v3
{

/**
 * This class describes an extension to the Intermediate interface allowing
 * iteration over operators. This means that searching for individual operator
 * combinations can be avoided. Instead, all existing operator combinations
 * are obtained by an iteration interface.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
class IntermediateOperIter
   :  public Intermediate<T, DATA>
{
   using evaldata_t = DATA<T>;
   using real_t = midas::type_traits::RealTypeT<T>;

   public:
      //! Default constructor
      IntermediateOperIter
         (
         )
         :  Intermediate<T, DATA>
               (
               )
      {
      }

      //! Copy constructor
      IntermediateOperIter
         (  const IntermediateOperIter& aOrig
         )
         :  Intermediate<T, DATA>
               (  aOrig
               )
      {
      }
      
      bool AssignConcreteModesOpers
         (  const std::vector<In>& aCmodes
         ,  const std::vector<In>& aCopers
         )  override
      {
         MIDASERROR("IntermediateOperIter::AssignConcreteModesOpers(): Not supported.");
         return false;
      }

      //! Niels: Trying to fix clang++ error
      virtual bool OnlyAmps() const override = 0;
      
      virtual void AssignConcreteModes(const std::vector<In>& aCmodes) = 0;
      ///< For these kinds of intermediates we only assign modes.

      virtual bool InitOperIter(const evaldata_t& aEvalData) = 0;
      ///< Initialize operator iteration. After calling this functiom, AssignNextOpers()
      ///< should be called repeatedly to get all operator combinations.
      
      virtual bool InitOperIterTensor(const evaldata_t& aEvalData) = 0;
      ///< Initialize operator iteration. After calling this functiom, AssignNextOpers()
      ///< should be called repeatedly to get all operator combinations.
      
      virtual bool AssignNextOpers(const evaldata_t& aEvaldata, std::vector<LocalOperNr>& aCopers) = 0;
      ///< Assign next set of operators. aOpers is updated with the operator numbers.
      
      virtual bool AssignNextOpersTensor(const evaldata_t& aEvaldata, std::vector<LocalOperNr>& aCopers) = 0;
      ///< Assign next set of operators. aOpers is updated with the operator numbers.
      
      virtual void AssignOpers(const std::vector<LocalOperNr>& aCOopers, std::vector<In>& aGenOpers) const = 0;
      ///< aCopers is the vector obtained from AssignFirstOpers() / AssignNextOpers(), i.e. a
      ///< consecutive list op operators. These are copied to aGenOpers using using the
      ///< relation mGenOpers[general mode] = operator.
      
      virtual void GetPureExciVectors(std::vector<std::vector<In> >& aExciVectors) const = 0;
      ///< Get all the vectors describing purely excited modes.
      
      virtual void GetOperExciVectors(std::vector<std::vector<In> >& aOperExciVectors) const = 0;
      ///< Get all the vectors describing excited modes with associated operator indices.

      virtual void GetNoOperOccVectors(std::vector<std::vector<In> >& aNoOperOccVectors) const = 0;
      ///< Get all the vectors of occupied modes with no associated operators.
};

} /* namespace midas::vcc::v3 */

#endif // INTERMEDIATEOPERITER_H_INCLUDED
