/**
************************************************************************
* 
* @file                IntermedCmb_Impl.h
*
* Created:             13-11-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*                      Niels Kristian Madsen (nielskm@chem.au.dk), templating
*
* Short Description:   Intermediate combining two other intermediates.
* 
* Last modified:
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDCMB_IMPL_H_INCLUDED
#define INTERMEDCMB_IMPL_H_INCLUDED

#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
#include <utility> 

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/CallStatisticsHandler.h"
#include "V3Contrib.h"
#include "vcc/TransformerV3.h"
#include "IntermediateMachine.h"
#include "Intermediate.h"
#include "IntermediateOperIter.h"
#include "input/OpDef.h"
#include "Scaling.h"
#include "vcc/DirProd.h"
#include "vcc/TensorProductAccumulator.h"
#include "EvalDataTraits.h"
#include "util/DynamicCastUniquePtr.h"


namespace midas::vcc::v3
{

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedCmb<T, DATA>::IntermedCmb
   (
   )
   :  IntermediateOperIter<T, DATA>()
{
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedCmb<T, DATA>::IntermedCmb
   (  const std::string& aSpec
   )
   :  IntermediateOperIter<T, DATA>()
   ,  mCopersCur(mCopers.end())
   ,  mCopersCurTensor(mCopersTensor.end())
   ,  mFirstOperSet(false)
   ,  mAssumeOpItStored(false)
{
   std::string::const_iterator pos = aSpec.begin();
   std::string spec;
   GetIntermedSpec(pos, spec);
   auto im = Intermediate<T, DATA>::Factory(spec);
   mOpIterIntermed = midas::util::DynamicCastUniquePtr<IntermediateOperIter<T, DATA>>(std::move(im));
   if (  !mOpIterIntermed
      )
   {
      Mout << "IntermedCmb<T, DATA>::IntermedCmb():" << std::endl
           << "aSpec: " << aSpec << std::endl
           << "Intermediate '" << spec << "' is not of type IntermedOperIter." << std::endl;
      MIDASERROR("");
   }

   spec.clear();
   this->GetIntermedSpec(pos, spec);
   mIntermed = Intermediate<T, DATA>::Factory(spec);
 
   // Setup common occupied modes not to be summed.
   mOpIterIntermed->GetNoOperOccVectors(mOccNoOper);
   std::vector<In> nosum;
   detail::ReadVector(pos, nosum);
   std::sort(nosum.begin(),nosum.end());
   mOccNoOper.push_back(nosum);
   
   this->UpdateInternalState();
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedCmb<T, DATA>::IntermedCmb
   (  std::unique_ptr<IntermediateOperIter<T, DATA> >&& aOpIter
   ,  IntermedPtr<T, DATA>&& aIm
   ,  const std::vector<In>& aNoSum
   )
   :  IntermediateOperIter<T, DATA>()
   ,  mOpIterIntermed
         (  std::move(aOpIter)
         )
   ,  mIntermed
         (  std::move(aIm)
         )
   ,  mCopersCur(mCopers.end())
   ,  mCopersCurTensor(mCopersTensor.end())
   ,  mFirstOperSet(false)
   ,  mAssumeOpItStored(false)
{
   mOpIterIntermed->GetNoOperOccVectors(mOccNoOper);
   mOccNoOper.push_back(aNoSum);
   std::sort(mOccNoOper.back().begin(), mOccNoOper.back().end());
  
   this->UpdateInternalState();
}

/**
 * Copy constructor:
 * Only copy the variable defining the intermediate.
 * New calls to AssignConcreteModes(), InitOperIter(), etc. will be necessary.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedCmb<T, DATA>::IntermedCmb
   (  const IntermedCmb<T, DATA>& aOrig
   )
   :  IntermediateOperIter<T, DATA>(aOrig)
   ,  mSumModes(aOrig.mSumModes)
   ,  mOcc(aOrig.mOcc)
   ,  mOccNoOper(aOrig.mOccNoOper)
   ,  mExci(aOrig.mExci)
   ,  mExciOper(aOrig.mExciOper)
   ,  mCmodes(aOrig.mCmodes.size())
   ,  mExciMc(aOrig.mExciMc)
   ,  mCopersCur(mCopers.end())
   ,  mCopersCurTensor(mCopersTensor.end())
   ,  mFirstOperSet(false)
   ,  mAssumeOpItStored(aOrig.mAssumeOpItStored)
{
   mOpIterIntermed = midas::util::DynamicCastUniquePtr<IntermediateOperIter<T, DATA> >(aOrig.mOpIterIntermed->Clone());
   mIntermed = aOrig.mIntermed->Clone();
}

/**
 *    Initialize the mode vectors. This can be done based on knowledge about the to intermediates
 *    and the mOccNoOper.back() vector which specifies the common modes not to be summed.
 *
 *    The rules are as follows:
 *
 *    First a definition:
 *    opit: The IntermediateOperIter object.
 *    
 *    Consider the intermediate (i.e. the mIntermed object):
 *    1) For the occupied modes with an associated operator index:
 *       a) The modes in common with "nosum" are already taken care of and added as
 *          mOccNoOper.back(). However, this code removes them form the mOcc vector.
 *       b) For the remaining modes, the ones in common with opit->mOcc should be
 *          moved to the mSumModes vector.
 *       c) Otherwise, the modes must be in one of the opit->mExciOper vectors.
 *          Check each vector and move the common modes to a new vector in mExci
 *          since the operator indices can be summed away.
 *    
 *    2) For each excited mode with no operator index:
 *       a) If the mode is in mOcc, add it to the mExciOper.
 *       b) If some modes are in common with one of the opit->mOccNoOper they should be move
 *          to a new mExci vector since they are now excited.
 *       c) The remaining excited modes should just be yet another mExci vector.
 *
 *    3) For each excited mode with an associated operator index.
 *       a) These modes has to be in the mOcc vector since the must originally be associated with
 *          some operator and hence the coefficient. The operator index can be summed away and the
 *          mode should be put in mExci.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::UpdateInternalState
   (
   )
{
   mOpIterIntermed->GetOperOccModes(mOcc);
   mOpIterIntermed->GetPureExciVectors(mExci);
   mOpIterIntermed->GetOperExciVectors(mExciOper);
   std::sort(mOcc.begin(), mOcc.end());
   std::sort(mExci.begin(), mExci.end());
   std::sort(mExciOper.begin(), mExciOper.end());
   
   // Sec. (1): Handle occupied modes with operator index in intermediate.
   std::vector<In> im_occ_op;
   mIntermed->GetOperOccModes(im_occ_op);
   std::sort(im_occ_op.begin(), im_occ_op.end());
  
   // Sec. (1a) 
   const std::vector<In>& nosum = mOccNoOper.back();
   std::vector<In> tmp;
   std::set_difference(mOcc.begin(), mOcc.end(), nosum.begin(), nosum.end(), std::back_inserter(tmp));
   mOcc = tmp;
   tmp.clear();
   std::set_difference(im_occ_op.begin(), im_occ_op.end(), nosum.begin(), nosum.end(), std::back_inserter(tmp));
   im_occ_op = tmp;
  
   // Sec. (1b) 
   tmp.clear();
   std::set_intersection(mOcc.begin(), mOcc.end(), im_occ_op.begin(), im_occ_op.end(), std::back_inserter(mSumModes));
   std::set_difference(mOcc.begin(), mOcc.end(), mSumModes.begin(), mSumModes.end(), std::back_inserter(tmp));
   mOcc = tmp;
   tmp.clear();
   std::set_difference(im_occ_op.begin(), im_occ_op.end(), mSumModes.begin(), mSumModes.end(), std::back_inserter(tmp));
   im_occ_op = tmp;

   // Sec. (1c)
   for (In i=I_0; i<mExciOper.size(); ++i)
   {
      tmp.clear();
      std::set_intersection(mExciOper[i].begin(), mExciOper[i].end(), im_occ_op.begin(), im_occ_op.end(), std::back_inserter(tmp));
      if (  !tmp.empty()
         )
      {
         mExci.push_back(tmp);
         tmp.clear();
         std::set_difference(mExciOper[i].begin(), mExciOper[i].end(), mExci.back().begin(), mExci.back().end(), std::back_inserter(tmp));
         mExciOper[i] = tmp;
         tmp.clear();
         std::set_difference(im_occ_op.begin(), im_occ_op.end(), mExci.back().begin(), mExci.back().end(), std::back_inserter(tmp));
         im_occ_op = tmp;
      }
   }

   // Check to see if we got it right.
   if (  !im_occ_op.empty()
      )
   {
      Mout << "IntermedCmb<T, DATA>::UpdateInternalState(): im_occ_op not empty: " << im_occ_op << std::endl;
      MIDASERROR("This can not happen.");
   }
     
   // Sec. (2): Handle excited modes without operator index in intermediate.
   std::vector<In> im_exci_noop;
   mIntermed->GetPureExciModes(im_exci_noop);
   std::sort(im_exci_noop.begin(), im_exci_noop.end());
 
   // Sec. (2a) 
   tmp.clear();
   std::set_intersection(mOcc.begin(), mOcc.end(), im_exci_noop.begin(), im_exci_noop.end(), std::back_inserter(tmp));
   if (  !tmp.empty()
      )
   {
      mExciOper.push_back(tmp);
      tmp.clear();
      std::set_difference(mOcc.begin(), mOcc.end(), mExciOper.back().begin(), mExciOper.back().end(), std::back_inserter(tmp));
      mOcc = tmp;
      tmp.clear();
      std::set_difference(im_exci_noop.begin(), im_exci_noop.end(), mExciOper.back().begin(), mExciOper.back().end(), std::back_inserter(tmp));
      im_exci_noop = tmp;
   }

   // Sec. (2b)
   for (In i=I_0; i<mOccNoOper.size(); ++i)
   {
      tmp.clear();
      std::set_intersection(mOccNoOper[i].begin(), mOccNoOper[i].end(), im_exci_noop.begin(), im_exci_noop.end(), std::back_inserter(tmp));
      if (  !tmp.empty()
         )
      {
         mExci.push_back(tmp);
         tmp.clear();
         std::set_difference(mOccNoOper[i].begin(), mOccNoOper[i].end(), mExci.back().begin(), mExci.back().end(), std::back_inserter(tmp));
         mOccNoOper[i] = tmp;
         tmp.clear();
         std::set_difference(im_exci_noop.begin(), im_exci_noop.end(), mExci.back().begin(), mExci.back().end(), std::back_inserter(tmp));
         im_exci_noop = tmp;
      }
   }

   // Sec. (2c)
   if (  !im_exci_noop.empty()
      )
   {
      mExci.push_back(im_exci_noop);
   }
   
   // Sec. (3): Handle excited modes with operator index in intermediate.
   std::vector<In> im_exci_op;
   mIntermed->GetOperExciModes(im_exci_op);
   std::sort(im_exci_op.begin(), im_exci_op.end());

   // Sec. (3a)
   tmp.clear();
   std::set_difference(mOcc.begin(), mOcc.end(), im_exci_op.begin(), im_exci_op.end(), std::back_inserter(tmp));
   mOcc = tmp;
   if (  !im_exci_op.empty()
      )
   {
      mExci.push_back(im_exci_op);
   }

   mCmodes.resize(max(mOpIterIntermed->HighestModeNo(), mIntermed->HighestModeNo()) + I_1);

   //PrintState(Mout);
}

/**
 * Print the current internal state of the intermediate.
 * @param aOut             Stream to print to.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::PrintState
   (  std::ostream& aOut
   )  const
{
   using ::operator<<;
   aOut << "IntermedCmb: " << *this << std::endl
        << "   this:                      " << this << std::endl
        << "   Occ modes w oper idx:      " << mOcc << std::endl
        << "   Occ modes w/o oper idx:    " << std::endl;
   for (In i=I_0; i<mOccNoOper.size(); ++i)
      aOut << "      " << mOccNoOper[i] << std::endl;
   aOut  << "   Excited modes:" << std::endl;
   for (In i=I_0; i<mExci.size(); ++i)
      aOut << "      " << mExci[i] << std::endl;
   aOut << "   Excited modes w/ oper idx:" << std::endl;
   for (In i=I_0; i<mExciOper.size(); ++i)
      aOut << "      " << mExciOper[i] << std::endl;
   aOut << "Internal:" << std::endl
        << "   mSumModes:              " << mSumModes << std::endl
        << "   mCmodes.size():         " << mCmodes.size() << std::endl;
}

/**
 * Read intermediate specification between '[' and ']'.
 * When done, aPos point to character following the final ']'.
 * @param aPos
 * @param aSpec On output the specification of the intermediate
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::GetIntermedSpec
   (  std::string::const_iterator& aPos
   ,  std::string& aSpec
   )
{
   if (  *aPos != '['
      )
   {
      MIDASERROR("IntermedCmb<T, DATA>::GetIntermedSpec(): Expected '[' in aSpec.");
   }

   aPos++;

   In brackets = I_1;
   while (  aPos != aSpec.end()
         )
   {
      char c = *(aPos++);
      if (  c == '['
         )
      {
         ++brackets;
      }
      else if  (  c == ']'
               )
      {
         --brackets;
      }
      
      if (  brackets == I_0
         )
      {
         break;
      }
      else
      {
         aSpec.push_back(c);
      }
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedCmb<T, DATA>& IntermedCmb<T, DATA>::operator=
   (  const IntermedCmb<T, DATA>& aOrig
   )
{
   IntermediateOperIter<T, DATA>::operator=(aOrig);
   if (this == &aOrig)
      return *this;

   mSumModes         = aOrig.mSumModes;
   mOcc              = aOrig.mOcc;
   mOccNoOper        = aOrig.mOccNoOper;
   mExci             = aOrig.mExci;
   mExciOper         = aOrig.mExciOper;
   mAssumeOpItStored = aOrig.mAssumeOpItStored;
   mCmodes.resize(aOrig.mCmodes.size());
   
   mOpIterIntermed = midas::util::DynamicCastUniquePtr<IntermediateOperIter<T, DATA> >(aOrig.mOpIterIntermed->Clone());
   mIntermed = aOrig.mIntermed->Clone();
   
   mCopers.clear();
   mCopersCur = mCopers.end();

   mCopersTensor.clear();
   mCopersCurTensor = mCopersTensor.end();

   mFirstOperSet = false;

   return *this;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCmb<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  GeneralMidasVector<T>& aRes
   )  const
{
   aRes.Reassign(*(mCopersCur->second));

   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCmb<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  NiceTensor<T>& aRes
   )  const
{
   aRes = mCopersCurTensor->second;

   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
In IntermedCmb<T, DATA>::ExciLevel
   (
   )  const
{
   In n=I_0;
   for (In i=I_0; i<mExci.size(); ++i)
      n += mExci[i].size();
   for (In i=I_0; i<mExciOper.size(); ++i)
      n += mExciOper[i].size();
   return n;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::GetOperExciModes
   (  std::vector<In>& aModes
   )  const
{
   for (In i=I_0; i<mExciOper.size(); ++i)
   {
      std::copy(mExciOper[i].begin(), mExciOper[i].end(), std::back_inserter(aModes));
   }
}

/**
 *
 **/
// Avoid new and delete of vector for each call
namespace intermed_cmb::detail
{
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::vector<In>& GetCModes
   (
   )
{
   static std::vector<In> c_modes;

   return c_modes;
}
} /* namespace intermed_cmb::detail */
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::AssignConcreteModes
   (  const std::vector<In>& aCmodes
   )
{
   In cs = std::min(mCmodes.size(), aCmodes.size());
   for (In i=I_0; i<cs; ++i)
   {
      mCmodes[i] = aCmodes[i];
   }

   // Initialize mapping of concrete to general modes for mOcc modes.
   mCGmap.clear();
   for (In i=I_0; i<mOcc.size(); ++i)
   {
      mCGmap.emplace_back(aCmodes[mOcc[i]], mOcc[i]);
   }

   // Initialize MC for excited modes.
   In n_exci = this->ExciLevel();
   intermed_cmb::detail::GetCModes<T, DATA>().resize(n_exci);
   In count = I_0;
   for (In i=I_0; i<mExci.size(); ++i)
   {
      for (In k=I_0; k<mExci[i].size(); ++k)
      {
         intermed_cmb::detail::GetCModes<T, DATA>()[count] = mCmodes[mExci[i][k]];
         ++count;
      }
   }
   for (In i=I_0; i<mExciOper.size(); ++i)
   {
      for (In k=I_0; k<mExciOper[i].size(); ++k)
      {
         intermed_cmb::detail::GetCModes<T, DATA>()[count] = mCmodes[mExciOper[i][k]];
         mCGmap.emplace_back(aCmodes[mExciOper[i][k]], mExciOper[i][k]);
         ++count;
      }
   }
   std::sort(intermed_cmb::detail::GetCModes<T, DATA>().begin(), intermed_cmb::detail::GetCModes<T, DATA>().end());
   mExciMc.ReInit(intermed_cmb::detail::GetCModes<T, DATA>());
   std::sort(mCGmap.begin(), mCGmap.end());
}

/**
 * Store concrete mode numbers in order:
 * [Excited modes with associated operator] [Excited modes w/o associated operator]
 * [Occupied modes with associated operator] [Occupied modes where operator has been summed]
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::GetConcreteModes
   (  std::vector<In>& aCmodes
   )  const
{
   aCmodes.clear();
   
   std::vector<In> tmp;
   for (In i=I_0; i<mExciOper.size(); ++i)
   {
      tmp.clear();
      for (In k=I_0; k<mExciOper[i].size(); ++k)
      {
         tmp.push_back(mCmodes[mExciOper[i][k]]);
      }
      std::sort(tmp.begin(), tmp.end());
      std::copy(tmp.begin(), tmp.end(), std::back_inserter(aCmodes));
   } 

   for (In i=I_0; i<mExci.size(); ++i)
   {
      tmp.clear();
      for (In k=I_0; k<mExci[i].size(); ++k)
      {
         tmp.push_back(mCmodes[mExci[i][k]]);
      }
      std::sort(tmp.begin(), tmp.end());
      std::copy(tmp.begin(), tmp.end(), std::back_inserter(aCmodes));
   } 

   tmp.clear();
   for (In i=I_0; i<mOcc.size(); ++i)
   {
      tmp.push_back(mCmodes[mOcc[i]]);
   }
   std::sort(tmp.begin(), tmp.end());
   std::copy(tmp.begin(), tmp.end(), std::back_inserter(aCmodes));

   for (In i=I_0; i<mOccNoOper.size(); ++i)
   {
      tmp.clear();
      for (In k=I_0; k<mOccNoOper[i].size(); ++k)
      {
         tmp.push_back(mCmodes[mOccNoOper[i][k]]);
      }
      std::sort(tmp.begin(), tmp.end());
      std::copy(tmp.begin(), tmp.end(), std::back_inserter(aCmodes));
   } 
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::GetConcreteOpers
   (  std::vector<In>& aCopers
   )  const
{
   MIDASERROR("IntermedCmb<T, DATA>::GetConcreteOpers(): Not implemented. Use iteration.");
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::GetModes
   (  std::vector<In>& aModes
   )  const
{
   for (In i=I_0; i<mExciOper.size(); ++i)
   {
      std::copy(mExciOper[i].begin(), mExciOper[i].end(), std::back_inserter(aModes));
   }
   for (In i=I_0; i<mExci.size(); ++i)
   {
      std::copy(mExci[i].begin(), mExci[i].end(), std::back_inserter(aModes));
   }
   for (In i=I_0; i<mOccNoOper.size(); ++i)
   {
      std::copy(mOccNoOper[i].begin(), mOccNoOper[i].end(), std::back_inserter(aModes));
   }
   
   std::copy(mOcc.begin(), mOcc.end(), std::back_inserter(aModes));
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::GetExciModes
   (  std::vector<In>& aModes
   )  const
{
   for (In i=I_0; i<mExciOper.size(); ++i)
   {
      std::copy(mExciOper[i].begin(), mExciOper[i].end(), std::back_inserter(aModes));
   }
   for (In i=I_0; i<mExci.size(); ++i)
   {
      std::copy(mExci[i].begin(), mExci[i].end(), std::back_inserter(aModes));
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::GetOccModes
   (  std::vector<In>& aModes
   )  const
{
   std::copy(mOcc.begin(), mOcc.end(), std::back_inserter(aModes));
   for (In i=I_0; i<mOccNoOper.size(); ++i)
   {
      std::copy(mOccNoOper[i].begin(), mOccNoOper[i].end(), std::back_inserter(aModes));
   }
}

/**
 * For ease of implementation and efficiency, all intermediates for the current
 * set of concrete modes are calcluated upon initialization of the iteration
 * mechanism. They are then stored for each operator set.
 *
 * @param aEvalData Data to evaluate for.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCmb<T, DATA>::InitOperIter
   (  const evaldata_t& aEvalData
   )
{
   mCopers.clear();

   this->SumMode(aEvalData, I_0, I_0);
   
   // The first call to AssignNextOpers() should not increase mCopersCur.
   mFirstOperSet = true;
   mCopersCur = mCopers.begin();

   return   (  mCopersCur != mCopers.end()   );
}

/**
 * For ease of implementation and efficiency, all intermediates for the current
 * set of concrete modes are calcluated upon initialization of the iteration
 * mechanism. They are then stored for each operator set. 
 * This function does it for the new tensor transformer.
 *
 * @param aEvalData Data to evaluate for.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCmb<T, DATA>::InitOperIterTensor
   (  const evaldata_t& aEvalData
   )
{
   mCopersTensor.clear();
   mCopersTensorTemp.clear();

   this->SumModeTensor(aEvalData, I_0, I_0);
   
   for(auto& elem : mCopersTensorTemp)
   {
      mCopersTensor.emplace(elem.first, elem.second.EvaluateSum());
   }
   mCopersTensorTemp.clear();

   // The first call to AssignNextOpers() should not increase mCopersCur.
   mFirstOperSet = true;
   mCopersCurTensor = mCopersTensor.begin();
   
   return ( this->mCopersCurTensor != this->mCopersTensor.end()   );
}

/**
 * Create summation index by recursive calls, and subsequently call LoopOpers
 * with complete summation index.
 *
 * @param aEvalData  Data to evaluate for
 * @param aIdx       Some index? :D
 * @param aCurCmode  Some mode
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::SumMode
   (  const evaldata_t& aEvalData
   ,  In aIdx
   ,  In aCurCmode
   )
{
   if (  aIdx >= mSumModes.size()
      )
   {
      this->LoopOpers(aEvalData);
      return;
   }

   for (In cmode=aCurCmode; cmode<aEvalData.NModes(); ++cmode)
   {
      mCmodes[mSumModes[aIdx]] = cmode;
      this->SumMode(aEvalData, aIdx+I_1, cmode+I_1);
   }
}

/**
 * Create summation index by recursive calls, and subsequently call LoopOpers
 * with complete summation index.  This function does it for the new tensor
 * transformer.
 *
 * @param aEvalData  Data to evaluate for
 * @param aIdx       Some index? :D
 * @param aCurCmode  Some mode
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::SumModeTensor
   (  const evaldata_t& aEvalData
   ,  In aIdx
   ,  In aCurCmode
   )
{
   if (  aIdx >= mSumModes.size()
      )
   {
      this->LoopOpersTensor(aEvalData);
      return;
   }

   for (In cmode=aCurCmode; cmode<aEvalData.NModes(); ++cmode)
   {
      mCmodes[mSumModes[aIdx]] = cmode;
      this->SumModeTensor(aEvalData, aIdx+I_1, cmode+I_1);
   }
}

/**
 * Loop over all operators using mOpIterIntermed.
 * For each set of operators, extract the ones we do not sum into a set 'opers'.
 * Add the product of the two intermediates to the reult vector corresponding to 'opers'.
 *
 * @param aEvalData     Data to evaluate for
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::LoopOpers
   (  const evaldata_t& aEvalData
   )
{
   if constexpr   (  DataAllowsMidasVectorV<evaldata_t>
                  )
   {
      auto& intermeds = aEvalData.template GetIntermediates<GeneralMidasVector>();

      mOpIterIntermed->AssignConcreteModes(mCmodes);
      if (  !intermeds.InitIntermedOperIter(mOpIterIntermed, aEvalData)
         )
      {
         return;
      }

      std::vector<In> opit_opers;         // Operators provided to us when iterating using OpIterIntermed.
      opit_opers.resize(mCmodes.size());
      GeneralMidasVector<T> opit_res;          // Value of OpIterIntermed.
      std::vector<std::pair<In, In> > co_pairs;// Pairs of concrete modes and corresponding operators.
      std::vector<LocalOperNr> opers;     // Operators not summed, i.e. to be exposed to the outside world.
      while (  intermeds.GetIntermedNextOpers(mOpIterIntermed, aEvalData, opit_opers, opit_res)
            )
      {
         if (  !mIntermed->AssignConcreteModesOpers(mCmodes, opit_opers)
            )
         {
            continue;
         }

         GeneralMidasVector<T> im_res;
         if (  !intermeds.GetIntermed(*mIntermed, aEvalData, im_res)
            )
         {
            continue;
         }
         
         // Extract relevant operators to be exposed. Sort corresponding to concrete modes.
         // This is necessary since the operators should correspond to the mode numbers
         // obtained usign GetConcreteModes().
         co_pairs.clear();
         opers.clear();
         for (In i=I_0; i<mOcc.size(); ++i)
         {
            co_pairs.push_back(std::make_pair(mCmodes[mOcc[i]], opit_opers[mOcc[i]]));
         }
         for (In i=I_0; i<mExciOper.size(); ++i)
         {
            for (In k=I_0; k<mExciOper[i].size(); ++k)
            {
               co_pairs.push_back(std::make_pair(mCmodes[mExciOper[i][k]], opit_opers[mExciOper[i][k]]));
            }
         }
         std::sort(co_pairs.begin(), co_pairs.end());
         for (In i=I_0; i<co_pairs.size(); ++i)
         {
            opers.push_back(co_pairs[i].second);
         }

         GeneralMidasVector<T> res;
         if (opit_res.Size() == I_1)
         {
            res.SetNewSize(im_res.Size());
            res = opit_res[I_0] * im_res;
         }
         else if (im_res.Size() == I_1)
         {
            res.SetNewSize(opit_res.Size());
            res = im_res[I_0] * opit_res;
         }
         else
         {
            // Do direct product...
            std::vector<MidasVector> vecs(2);
            std::vector<ModeCombi>   mcs(2);

            vecs[0].Reassign(opit_res);
            vecs[1].Reassign(im_res);
            mOpIterIntermed->GetExciMc(mcs[0]);
            mIntermed->GetExciMc(mcs[1]);

            ModeCombi tot_mc;
            tot_mc.Union(mcs[0], mcs[1]);

            // Setup vector with number of modals.
            std::vector<In> res_modals(tot_mc.Size());
            for (In i=I_0; i<tot_mc.Size(); ++i)
            {
               res_modals[i] = aEvalData.NModals(tot_mc.Mode(i))-I_1;
            }
            std::vector<std::vector<In> > modals(2);
            for (In i=I_0; i<I_2; ++i)
            {
               modals[i].resize(mcs[i].Size());
               for (In k=I_0; k<mcs[i].Size(); ++k)
               {
                  modals[i][k] = aEvalData.NModals(mcs[i].Mode(k))-I_1;
               }
            }
            
            res.SetNewSize(opit_res.Size()*im_res.Size());
            res.Zero();
            dirprod::DirProd2(res, tot_mc, res_modals, vecs, mcs, modals);
         }
         
         auto it = mCopers.find(opers);
         if (  it == mCopers.end()
            )
         {
            mCopers.insert(std::make_pair(opers, std::make_unique<GeneralMidasVector<T> >(res)));
         }
         else
         {
            (*it->second) += res;
         }
      }
   }
   else
   {
      MIDASERROR("Not implemented for given DATA type!");
   }
}

/**
 * Loop over all operators using mOpIterIntermed.
 * For each set of operators, extract the ones we do not sum into a set 'opers'.
 * Add the product of the two intermediates to the reult vector corresponding to 'opers'.
 * This function does for the new tensor transformer.
 *
 * @param aEvalData        Data to evaluate for
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::LoopOpersTensor
   (  const evaldata_t& aEvalData
   )
{
   if constexpr   (  DataAllowsNiceTensorV<evaldata_t>
                  )
   {
      auto& intermeds = aEvalData.template GetIntermediates<NiceTensor>();

      mOpIterIntermed->AssignConcreteModes(mCmodes);
      if (  !intermeds.InitIntermedOperIter(mOpIterIntermed, aEvalData)
         )
      {
         return;
      }

      std::vector<In> opit_opers;         // Operators provided to us when iterating using OpIterIntermed.
      opit_opers.resize(mCmodes.size());
      
      NiceTensor<T> opit_res; 
      NiceTensor<T> im_res; 
      
      std::vector<std::pair<In, In> > co_pairs;// Pairs of concrete modes and corresponding operators.
      std::vector<LocalOperNr> opers;          // Operators not summed, i.e. to be exposed to the outside world.
      while (  intermeds.GetIntermedNextOpers(mOpIterIntermed, aEvalData, opit_opers, opit_res)
            )
      {
         if (  !mIntermed->AssignConcreteModesOpers(mCmodes, opit_opers)
            )
         {
            continue;
         }

         im_res = NiceTensor<T>();
         if (  !intermeds.GetIntermed(*mIntermed, aEvalData, im_res)
            )
         {
            continue;
         }
         
         // Extract relevant operators to be exposed. Sort corresponding to concrete modes.
         // This is necessary since the operators should correspond to the mode numbers
         // obtained usign GetConcreteModes().
         co_pairs.clear();
         opers.clear();
         for (In i=I_0; i<mOcc.size(); ++i)
         {
            co_pairs.push_back(std::make_pair(mCmodes[mOcc[i]], opit_opers[mOcc[i]]));
         }
         for (In i=I_0; i<mExciOper.size(); ++i)
         {
            for (In k=I_0; k<mExciOper[i].size(); ++k)
            {
               co_pairs.push_back(std::make_pair(mCmodes[mExciOper[i][k]], opit_opers[mExciOper[i][k]]));
            }
         }
         std::sort(co_pairs.begin(), co_pairs.end());
         for (In i=I_0; i<co_pairs.size(); ++i)
         {
            opers.push_back(co_pairs[i].second);
         }
         
         // find result mc
         ModeCombi opit_mc;
         mOpIterIntermed->GetExciMc(opit_mc);
         ModeCombi im_mc;
         mIntermed->GetExciMc(im_mc);
            
         ModeCombi tot_mc;
         tot_mc.Union(opit_mc, im_mc);

         // find tensor else contruct it
         auto it = mCopersTensorTemp.find(opers);
         if (it == mCopersTensorTemp.end())
         {
            // find result size
            std::vector<unsigned int> res_modals(tot_mc.Size());
            for (In i=I_0; i<tot_mc.Size(); ++i)
            {
               res_modals[i] = aEvalData.NModals(tot_mc.Mode(i))-I_1;
            }
            if constexpr   (  DataAllowsDecompositionV<evaldata_t>
                           )
            {
               it = mCopersTensorTemp.emplace
                  (  opers
                  ,  TensorSumAccumulator<T>
                     (  res_modals
                     ,  aEvalData.GetDecomposer()
                     ,  aEvalData.GetAllowedRank()
                     )
                  ).first;
            }
            else
            {
               it = mCopersTensorTemp.emplace
                  (  opers
                  ,  TensorSumAccumulator<T>
                     (  res_modals
                     ,  nullptr
                     ,  I_0
                     )
                  ).first;
            }
         }
         auto& res = it->second;

         // Check if MCs overlap
         bool mcs_overlap = tot_mc.Size() < (opit_mc.Size() + im_mc.Size());
         if (  mcs_overlap
            && gDebug
            )
         {
            Mout  << " IntermedCmb: MCs overlap!\n"
                  << "    opit_mc:  " << opit_mc << "\n"
                  << "    im_mc:    " << im_mc << "\n"
                  << "    Term:     ";
            this->Latex(Mout);
            Mout  << std::endl;
            Mout  << " opit_res:\n" << opit_res << "\n"
                  << " im_res:\n" << im_res << "\n"
                  << " res before dirprod:\n" << res.Tensor().ToSimpleTensor() << "\n"
                  << std::flush;
         }
         
         // create product with coef := 1 (and reserved size 2)
         std::unique_ptr<TensorProductAccumulator<T> > tensor_accumulator = nullptr;

         if constexpr   (  midas::vcc::v3::DataAllowsDecompositionV<evaldata_t>
                        )
         {
            tensor_accumulator = std::make_unique<TensorProductAccumulator<T> >
                                       (  1.0
                                       ,  2
                                       ,  aEvalData.TensorProductScreening().first
                                       ,  aEvalData.TensorProductScreening().second
                                       ,  aEvalData.TensorProductLowRankLimit()
                                       ,  mcs_overlap
                                       );
         }
         else
         {
            tensor_accumulator = std::make_unique<TensorProductAccumulator<T> >
                                       (  1.0
                                       ,  2
                                       ,  C_0
                                       ,  C_0
                                       ,  I_0
                                       ,  mcs_overlap
                                       );
         }
         tensor_accumulator->AddTensor(std::move(opit_res), std::move(opit_mc));
         tensor_accumulator->AddTensor(std::move(im_res)  , std::move(im_mc)  );

         tensor_accumulator->DirProd(res, tot_mc);

         // DEBUG
         if (  mcs_overlap
            && gDebug
            )
         {
            Mout  << " dirprod:\n" << res.Tensor().ToSimpleTensor() << "\n"
                  << std::flush;
         }
      }
   }
   else
   {
      MIDASERROR("Not implemented for given DATA type!");
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCmb<T, DATA>::AssignNextOpers
   (  const evaldata_t& aEvalData
   ,  std::vector<LocalOperNr>& aCopers
   )
{
   // If this is the first call, don't change mCopersCur.
   if (  mFirstOperSet
      )
   {
      mFirstOperSet = false;
      aCopers = mCopersCur->first;
      return true;
   }
   else if  (  ++mCopersCur != mCopers.end()
            )
   {
      aCopers = mCopersCur->first;
      return true;
   }
   else
   {
      return false;
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCmb<T, DATA>::AssignNextOpersTensor
   (  const evaldata_t& aEvalData
   ,  std::vector<LocalOperNr>& aCopers
   )
{
   // If this is the first call, don't change mCopersCur.
   if (  mFirstOperSet
      )
   {
      mFirstOperSet = false;
      aCopers = mCopersCurTensor->first;
      return true;
   }
   else if  (  ++mCopersCurTensor != mCopersTensor.end()
            )
   {
      aCopers = mCopersCurTensor->first;
      return true;
   }
   else
   {
      return false;
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::AssignOpers
   (  const std::vector<LocalOperNr>& aCopers
   ,  std::vector<In>& aGenOpers
   )  const
{
   // aCopers is sorted according to concrete mode numbers.
   // Translate concrete mode numbers to general mode numbers.
   // Store operator at this index in aGenOpers.
   for (In i=I_0; i<aCopers.size(); ++i)
   {
      In g_mode = mCGmap[i].second;
      aGenOpers[g_mode] = aCopers[i];
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCmb<T, DATA>::CmpKind
   (  const Intermediate<T, DATA>* const aIntermed
   )  const
{
   const IntermedCmb<T, DATA>* const other = dynamic_cast<const IntermedCmb<T, DATA>* >(aIntermed);
   if (  !other
      )
   {
      return false;
   }

   return mSumModes.size()   == other->mSumModes.size() &&
          mOcc.size()        == other->mOcc.size() &&
          CmpVecKind(mOccNoOper, other->mOccNoOper) &&
          CmpVecKind(mExci, other->mExci) &&
          CmpVecKind(mExciOper, other->mExciOper) &&
          mOpIterIntermed->CmpKind(other->mOpIterIntermed.get()) &&
          mIntermed->CmpKind(other->mIntermed.get());
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCmb<T, DATA>::CmpVecKind
   (  const std::vector<std::vector<In> >& a1
   ,  const std::vector<std::vector<In> >& a2
   )  const
{
   if (  a1.size() != a2.size()
      )
   {
      return false;
   }

   for (In i=I_0; i<a1.size(); ++i)
   {
      if (  a1[i].size() != a2[i].size()
         )
      {
         return false;
      }
   }

   return true;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
In IntermedCmb<T, DATA>::HighestModeNo
   (
   )  const
{ 
   std::vector<In> modes = mSumModes;   // Simple but a little inefficient due to copying of vectors.
   this->GetModes(modes);
   In high = -I_1;
   for (In i=I_0; i<modes.size(); ++i)
   {
      if (  modes[i] > high
         )
      {
         high = modes[i];
      }
   }
   return high;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::RegisterInternalIntermeds
   (  IntermediateMachine<T, GeneralMidasVector, DATA>& aInMachine
   )
{
   mOpIterIntermed->RegisterInternalIntermeds(aInMachine);
   mIntermed->RegisterInternalIntermeds(aInMachine);
   aInMachine.RegisterIntermediate(*mOpIterIntermed);
   aInMachine.RegisterIntermediate(*mIntermed);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::RegisterInternalIntermeds
   (  IntermediateMachine<T, NiceTensor, DATA>& aInMachine
   )
{
   mOpIterIntermed->RegisterInternalIntermeds(aInMachine);
   mIntermed->RegisterInternalIntermeds(aInMachine);
   aInMachine.RegisterIntermediate(*mOpIterIntermed);
   aInMachine.RegisterIntermediate(*mIntermed);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedCmb<T, DATA>::GetScaling
   (  bool aCpTensors
   )  const
{
   Scaling number = GetNumber();
   Scaling cost = GetCost(aCpTensors);
   In n = I_0;
   if (  aCpTensors
      )
   {
      n = std::max(cost.mN, I_1);   // Direct products are "free" to evaluate, but their recompression scales linearly with N
   }
   else
   {
      n = std::max(cost.mN, number.mN);
   }

   return Scaling(number.mM+cost.mM, number.mO+cost.mO, n, cost.mR);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedCmb<T, DATA>::GetCost
   (  bool aCpTensors
   )  const
{
   In m = mSumModes.size();       // m: Scaling wrt. modes.
   In o = mSumModes.size();       // o: Scaling wrt. operators.
   
   // For occupied modes without operator index: The modes "not summed" in this combined
   // intermediate will still have the operator indices summed.
   o += mOccNoOper.back().size();

   // If any excited modes in the intermediate has associated operator indices,
   // these will be summed.
   std::vector<In> im_exci_op;
   mIntermed->GetOperExciModes(im_exci_op);
   o += im_exci_op.size();

   // The oper. iter. intermediate may have a cost if it is not stored in the intermediate machine.
   auto opit_number = this->mOpIterIntermed->GetNumber();
   In opit_n = opit_number.mN;
   In opit_r = -I_1;
   In dir_prod_n = opit_n;
   if (  !mAssumeOpItStored
      && mOpIterIntermed->RefNo() == -I_1
      )
   {
      auto opit_cost = this->mOpIterIntermed->GetCost(aCpTensors);
      m += opit_cost.mM;
      o += opit_cost.mO;
      opit_n = opit_cost.mN;
      opit_r = opit_cost.mR;
   }
  
   // The cost wrt. the number of modals (and CP rank).
   In im_n = mIntermed->GetNumber().mN;
   In im_r = -I_1;
   dir_prod_n += im_n;
   if (mIntermed->RefNo() == -I_1)
   {
      auto cost = this->mIntermed->GetCost(aCpTensors);
      im_n = cost.mN;
      im_r = cost.mR;
   }

   In n = std::max(opit_n, im_n);                        // The maximum cost of evaluating intermediates.

   // Niels: For CP tensors, the cost of direct products is determined by their recompression
   // , which for CP-ALS scales as N^1 (but also with rank, etc.). It's complicated in this framework...
   if (  aCpTensors  )
   {
//      dir_prod_n = I_1;
      dir_prod_n = I_0;
   }
   n = std::max(n, dir_prod_n);

   In r = aCpTensors ? std::max(im_r, opit_r) : -I_1;
   
   return Scaling(m, o, n, r);
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedCmb<T, DATA>::GetNumber
   (
   )  const
{
   std::vector<In> modes(mOcc);         // All exposed modes.
   std::vector<In> opers(mOcc);         // All exposed modes with associated operators.
   std::vector<In> exci_modes;          // All excited exposed modes.
   
   for (In i=I_0; i<mOccNoOper.size(); ++i)
   {
      std::copy(mOccNoOper[i].begin(), mOccNoOper[i].end(), std::back_inserter(modes));
   }

   for (In i=I_0; i<mExci.size(); ++i)
   {
      std::copy(mExci[i].begin(), mExci[i].end(), std::back_inserter(modes));
      std::copy(mExci[i].begin(), mExci[i].end(), std::back_inserter(exci_modes));
   }
   
   for (In i=I_0; i<mExciOper.size(); ++i)
   {
      std::copy(mExciOper[i].begin(), mExciOper[i].end(), std::back_inserter(modes));
      std::copy(mExciOper[i].begin(), mExciOper[i].end(), std::back_inserter(opers));
      std::copy(mExciOper[i].begin(), mExciOper[i].end(), std::back_inserter(exci_modes));
   }

   // In the current implementation some modes may be identical.
   std::sort(modes.begin(), modes.end());
   std::sort(opers.begin(), opers.end());
   modes.erase(std::unique(modes.begin(), modes.end()), modes.end());
   opers.erase(std::unique(opers.begin(), opers.end()), opers.end());
   
   return Scaling(modes.size(),opers.size(),exci_modes.size());
}

/**
 * Specification format: cmb[i1][i2](m)
 *    i1: First intermediate
 *    i2: Second intermediate.
 *    m:  Vector of common modes not to be summed (useful since modes may be excited elsewhere.)
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::WriteSpec
   (  std::ostream& aOut
   )  const
{
   using ::operator<<;
   aOut << "cmb[";
   mOpIterIntermed->WriteSpec(aOut);
   aOut << "][";
   mIntermed->WriteSpec(aOut);
   aOut << "]" << mOccNoOper.back();
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::ostream& IntermedCmb<T, DATA>::Print
   (  std::ostream& aOut
   )  const
{
   using ::operator<<;
   aOut << "cmb[";
   if (  !mOpIterIntermed
      )
      aOut << "NULL";
   else
   {
      aOut << *mOpIterIntermed;
   }
   aOut << "][";
   if (  !mIntermed
      )
   {
      aOut << "NULL";
   }
   else
   {
      aOut << *mIntermed;
   }
   aOut << "]" << mOccNoOper.back() << "{id:" << this->mRefNo << "}";

   return aOut;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::PrintScaling
   (  std::ostream& aOut
   )  const
{
   Scaling number = GetNumber();
   Scaling cost = GetCost();  // Also with CP?
   Scaling tot = GetScaling(false);
   Scaling cp = GetScaling(true);
   aOut << "n: " << number << "   c: " << cost << "   t: " << tot << "t_cp:" << cp;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::LatexVecVec
   (  std::ostream& aOut
   ,  const std::vector<std::vector<In> >& aVec
   )  const
{
   for (In i=I_0; i<aVec.size(); ++i)
   {
      if (  !aVec[i].empty()
         )
      {
         aOut << "\\{";
         for (In j=I_0; j<aVec[i].size(); ++j)
         {
            aOut << "m_{" << aVec[i][j] << "}";
         }
         aOut << "\\}";
      }
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedCmb<T, DATA>::Latex
   (  std::ostream& aOut
   )  const
{
   aOut << "{}";
   if (  !mExciOper.empty()
      )
   {
      aOut << "^{";
      LatexVecVec(aOut, mExciOper);
      aOut << "}";
   }

   if (  !mOccNoOper.empty()
      )
   {
      aOut << "_{";
      LatexVecVec(aOut, mOccNoOper);
      aOut << "}";
   }
    
   aOut << "[";
   mOpIterIntermed->Latex(aOut);
   aOut << ",";
   mIntermed->Latex(aOut);
   aOut << "]";

   if (  !mExci.empty()
      )
   {
      aOut << "^{";
      LatexVecVec(aOut, mExci);
      aOut << "}";
   }

   if (  !mOcc.empty()
      )
   {
      aOut << "_{\\{";
      for (In i=I_0; i<mOcc.size(); ++i)
      {
         aOut << "m_{" << mOcc[i] << "}";
      }
      aOut << "\\}}";
   }
}

/**
 * function for getting norm 2 of combined intermed
 **/ 
template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedCmb<T, DATA>::Norm2
   (  const evaldata_t& aEvalData
   ,  real_t& aNorm2
   )  const
{ 
   aNorm2 = (*mCopersCur).second->Norm2();
   return true;
}

} /* namespace midas::vcc::v3 */

#endif /* INTERMEDCMB_IMPL_H_INCLUDED */
