/**
************************************************************************
* 
* @file                IntermediateMachine_Impl.h
*
* Created:             08-10-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*                      Eduard Matito (ematito@gmail.com) 
*                      Niels Kristian Madsen (nielskm@chem.au.dk), templating
*
* Short Description:   Implementation of class IntermediateMachine
* 
* Last modified:
* 
* Detailed  Description: 
*
* Register Intermediates for storing. 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef INTERMEDIATEMACHINE_IMPL_H_INCLUDED
#define INTERMEDIATEMACHINE_IMPL_H_INCLUDED

#include <iostream>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "IntermediateMachine_Decl.h"
#include "IntermedProd.h"
#include "IntermediateOperIter.h"
#include "IntermedCmbL.h"
#include "IntermedCmb.h"
#include "IntermedT.h"
#include "EvalDataTraits.h"
#include "vcc/TransformerV3.h"

#include "tensor/NiceTensor.h"
#include "tensor/Scalar.h"

namespace midas::vcc::v3
{

#define INIT_DC_SIZE 1000 ///< Initial size of DCs for storing intermediates.
#define EXT_DC_SIZE 10000 ///< Size by which DCs are extended everytime we need more space.

/**
 * The purpose of this function is to let the intermediate machine decide if it
 * wants to enable storage of the intermediate pointed to by aIntermed.
 *
 * @param aIntermed
 * @return
 *    Reference number of stored intermed, -1 if not stored.
 **/
template
   <  typename T
   ,  template<typename> typename VECTOR
   ,  template<typename...> typename DATA
   >
In IntermediateMachine<T, VECTOR, DATA>::RegisterIntermediate
   (  intermed_t& aIntermed
   )
{
   constexpr bool is_datacont = std::is_same_v<data_t, GeneralDataCont<param_t>>;

   // This will be a nullptr if the cast fails
   const auto* const t_ptr = dynamic_cast<const IntermedT<T, DATA>* const>(&aIntermed);

   if (  t_ptr
      && t_ptr->Nforw() >= this->mRestrictions.mForward.first
      && t_ptr->Nforw() <= this->mRestrictions.mForward.second
      && t_ptr->Ndown() >= this->mRestrictions.mMinDown
      )
   {
      // Check if this intermediate kind is already stored.
      for(const auto& i : mStoredIntermeds)
      {
         if (  aIntermed.CmpKind(i.get())
            ) 
         {
            In refno = i->RefNo();          
            aIntermed.SetRefNo(refno);    
            ++mStoredIntermedsCount[refno]; 
            return refno; // return  !!
         }
      }

      // Kind has not been stored before.
      In refno = mStoredIntermeds.size();
      aIntermed.SetRefNo(refno);
      mStoredIntermeds.emplace_back(aIntermed.Clone());
      mStoredIntermedsCount.emplace_back(I_1);
      mIntermedVals.emplace_back(data_t());

      if constexpr   (  is_datacont
                     )
      {
         mIntermedVals.back().SetNewSize(INIT_DC_SIZE);
      }

      mIntermedValsSize.push_back(0);
      mIntermedAddr.push_back(ModeOperMap<addr_t>());
     
      return refno; // return !!
   }
  
   // Store all combined intermediates. 
   if (  dynamic_cast<IntermedCmb<T, DATA>* >(&aIntermed)
      )
   {
      // Check if this intermediate kind is already stored.
      for(const auto& i : mStoredOpIt)
      {
         if (  aIntermed.CmpKind(i.get())
            ) 
         {
            In refno = i->RefNo();          
            aIntermed.SetRefNo(refno);    
            ++mStoredOpItCount[refno]; 
            return refno; // return !
         }
      }

      // Kind has not been stored before.
      In refno = mStoredOpIt.size();
      aIntermed.SetRefNo(refno);
      mStoredOpIt.emplace_back(midas::util::DynamicCastUniquePtr<opit_t>(aIntermed.Clone()));
      mStoredOpItCount.emplace_back(I_1);
      mOpItVals.emplace_back(data_t());
      if constexpr   (  is_datacont
                     )
      {
         mOpItVals.back().SetNewSize(INIT_DC_SIZE);
      }
      mOpItValsSize.emplace_back(0);
      mOpItAddr.emplace_back(ModeOperVectorMap<addr_t>());

      return refno; // return !
   }

   if (  dynamic_cast<IntermedCmbL<T, DATA>* >(&aIntermed)
      )
   {
      // Check if this intermediate kind is already stored.
      for(const auto& i : mStoredIntermeds)
      {
         if (  aIntermed.CmpKind(i.get())
            ) 
         {
            In refno = i->RefNo();          
            aIntermed.SetRefNo(refno);    
            ++mStoredIntermedsCount[refno]; 
            return refno; // return !
         }
      }

      // Kind has not been stored before.
      In refno = mStoredIntermeds.size();
      aIntermed.SetRefNo(refno);
      mStoredIntermeds.emplace_back(aIntermed.Clone());
      mStoredIntermedsCount.emplace_back(I_1);
      mIntermedVals.emplace_back(data_t());
      if constexpr   (  is_datacont
                     )
      {
         mIntermedVals.back().SetNewSize(INIT_DC_SIZE);
      }
      mIntermedValsSize.emplace_back(0);
      mIntermedAddr.emplace_back(ModeOperMap<addr_t>());

      return refno; // return !
   }

   
   // Re-label DataConts
   if constexpr   (  is_datacont
                  )
   {
      for (In i=I_0; i<mIntermedVals.size(); ++i)
      {
         mIntermedVals[i].NewLabel("ImVal_" + std::to_string(i));
      }
      for (In i=I_0; i<mOpItVals.size(); ++i)
      {
         mOpItVals[i].NewLabel("OpItVals_" + std::to_string(i));
      }
   }
   
   return -I_1; // return !
}

/**
 * This function puts the evaluated value of aIntermed in the arRes vector.
 * The "repository" of pre-calcluated intermediates is updated on-the-fly.
 *
 * The storage scheme is as follows:
 * 
 *     vector[ref.no]                                       DataCont for intermediates.
 *        0                                                          x
 *        1                                                          x
 *        2 --> map(aCmodes, X)                                      x
 *        3                  |                                       x
 *        4                  +-->map(aCopers, pair (begin,end))      x
 *        .                                          |     |         x
 *        .                                          |     |         x
 *        .                                          +-----+-------> x
 *        N                                                |         x
 *                                                         |         x
 *                                                         |         x
 *                                                         +-------> x
 *                                                                   x
 *                                                                   x
 * 
 * @param aIntermed  Intermediate to evaluate
 * @param aEvalData  Data for evaluation
 * @param arRes      Result of evaluation
 * @return
 *    True if everything is good!
 **/
template
   <  typename T
   ,  template<typename> typename VECTOR
   ,  template<typename...> typename DATA
   >
bool IntermediateMachine<T, VECTOR, DATA>::GetIntermed
   (  const intermed_t& aIntermed
   ,  const evaldata_t& aEvalData
   ,  vec_t& arRes
   )
{
   constexpr bool is_datacont = std::is_same_v<data_t, GeneralDataCont<param_t>>;

   In refno = aIntermed.RefNo();
   
   // check if intermediate is/should be saved,
   // if it is not we just evaluate it at and return
   // if ref == -1 intermed is not saved (calc on the fly)
   if (  refno == -I_1
      )
   {
      bool t = aIntermed.Evaluate(aEvalData, arRes);
      return t; // return
   }

   // intermediate is/should be saved
   // Get concrete modes and operators.
   std::vector<In> cmodes;
   std::vector<In> copers;
   aIntermed.GetConcreteModes(cmodes);
   aIntermed.GetConcreteOpers(copers);
  
   // Search for intermediate, i.e. the modes and operators.
   auto it_modes = mIntermedAddr[refno].find(cmodes);
   if (  it_modes != mIntermedAddr[refno].end()
      ) 
   {  
      auto it_opers = it_modes->second.find(copers);
      if (  it_opers != it_modes->second.end()
         ) 
      {
         if constexpr   (  is_datacont
                        )
         {
            In begin = it_opers->second.first;
            In end = it_opers->second.second;
            In len = end-begin;
            arRes.SetNewSize(len);
            mIntermedVals[refno].DataIo(IO_GET, arRes, len, begin);
         }
         else
         {
            In addr = it_opers->second;
            arRes = mIntermedVals[refno][addr];
            
            if (  arRes.IsNullPtr()
               )
            {
               Mout  << " arRes uninitialized in IntermediateMachine::GetIntermed:\n"
                     << "    refno:    " << refno << "\n"
                     << "    addr:     " << addr  << "\n"
                     << "    type:     " << aIntermed.Type() << "\n"
                     << std::flush;
               MIDASERROR("arRes unitialized in IntermediateMachine::GetIntermed! Intermed type = " + std::string(aIntermed.Type()));
            }
         }

         return true; // intermediate found return true
      }
   }

   // If we are here, the intermediate was not found to be calculated yet.
   // Calculate intermediate and save result in intermediate machine
   if (  aIntermed.Evaluate(aEvalData, arRes)
      )
   {
      if constexpr   (  is_datacont
                     )
      {
         In begin = mIntermedValsSize[refno];
         In end = begin+arRes.Size();
         while (mIntermedVals[refno].Size() < end+I_1)
         {
            mIntermedVals[refno].SetNewSize(mIntermedVals[refno].Size()+EXT_DC_SIZE, true);
         }
         mIntermedVals[refno].DataIo(IO_PUT, arRes, arRes.Size(), begin);
         mIntermedValsSize[refno] += arRes.Size();
         mIntermedAddr[refno][cmodes][copers].first = begin;
         mIntermedAddr[refno][cmodes][copers].second = end;
      }
      else
      {
         mIntermedAddr[refno][cmodes][copers] = mIntermedVals[refno].size();
         mIntermedVals[refno].emplace_back(arRes);
      }

      return true;
   }
   else
   {
      return false;
   }
}

/**
 * Get squared norm of intermediate
 *
 * @param aIntermed
 * @param aEvalData
 * @param arNorm2
 *
 * @return
 *    True if the norm has been calculated
 **/
template
   <  typename T
   ,  template<typename> typename VECTOR
   ,  template<typename...> typename DATA
   >
bool IntermediateMachine<T, VECTOR, DATA>::GetIntermedNorm2
   (  const intermed_t& aIntermed
   ,  const evaldata_t& aEvalData
   ,  real_t& arNorm2
   )
{
   // at this moment norm is always calced "on the fly"
   return aIntermed.Norm2(aEvalData, arNorm2); 
}

/**
 * This function is for initializing iteration over operators.
 * 
 * Why do we want to do this?
 * Because for any given combination of modes, we need all operator combinations in the
 * Hamiltonian. One way of doing this is to sum over all operators inside the IntermedProd
 * and ask for all the different intermediates one by one. However, this requires a lot of
 * searching for operator combinations. Also, we don't know what operator combinations
 * exist in the Hamiltonian, so we will have a lot of redundant searches.
 *
 * Instead, we use one intermediate in each IntermedProd which includes all modes with
 * associated operators. The most obvious example of such an intermediate is the coefficient
 * for a term in the Hamiltonian, described by the IntermedCoef class. This intermediate is
 * derived from the extended interface IntermediateOperIter which has a set of extra functions
 * relative to the basic Intermediate:
 *    - IntermediateOperIter::InitOperIter(const Evaldata& aEvalData)
 *    - IntermediateOperIter::AssignNextOpers(const Evaldata& aEvalData, std::vector<In>& aCopers)
 *    - IntermediateOperIter::AssignOpers(const std::vector<In>& aCopers, std::vector<In>& aGenOpers)
 * Calling the InitOperIter() function will initialize the intermediate.
 * Consecutive calls to AssignNextOpers() will assign the next operator combination to
 * aOpers. Calling the Evaluate() function will further provide the value of the
 * intermediate. The function AssignOpers() must be used to convert the aOpers into a
 * format needed by IntermedProd, see comments in IntermediateOperIter.h.
 *
 * Again, if the intermediate ref. no. is -1, we do not store the intermediate.
 *
 * @param aIntermed Intermediate to initialize
 * @param aEvalData
 *
 * @return
 *    False on failure
 **/ 
template
   <  typename T
   ,  template<typename> typename VECTOR
   ,  template<typename...> typename DATA
   >
bool IntermediateMachine<T, VECTOR, DATA>::InitIntermedOperIter
   (  opitptr_t& aIntermed
   ,  const evaldata_t& aEvalData
   )
{
   constexpr bool is_datacont = std::is_same_v<data_t, GeneralDataCont<param_t>>;

   In refno = aIntermed->RefNo();
   
   // check if intermediate is stored
   if (  refno == -I_1
      ) 
   {
      if constexpr   (  is_datacont
                     )
      {
         return aIntermed->InitOperIter(aEvalData); // intermediate is not stored so we initialize directly
      }
      else
      {
         return aIntermed->InitOperIterTensor(aEvalData);
      }
   }
   
   std::vector<In> cmodes;
   aIntermed->GetConcreteModes(cmodes);
   
   // search for concrete modes
   auto modes_iter = mOpItAddr[refno].find(cmodes);
   if (  modes_iter == mOpItAddr[refno].end()
      ) 
   {
      if constexpr   (  is_datacont
                     )
      {
         // Concrete modes not found: Try to calculate and store.
         if (  !aIntermed->InitOperIter(aEvalData)
            )
         {
            return false;
         }
      }
      else
      {
         // Concrete modes not found: Try to calculate and store.
         if (  !aIntermed->InitOperIterTensor(aEvalData)
            )
         {
            return false;
         }
      }
      
      std::vector<std::pair<std::vector<LocalOperNr>, addr_t> > tmp;
      modes_iter = (mOpItAddr[refno].insert(std::make_pair(cmodes, tmp))).first;
     
      // for each set of operators calculuate and store the IntermediateOperIter
      std::vector<LocalOperNr> opers;
      if constexpr   (  is_datacont
                     )
      {
         while (  aIntermed->AssignNextOpers(aEvalData, opers)
               )
         {
            vec_t res; 
            aIntermed->Evaluate(aEvalData, res);

            In begin = mOpItValsSize[refno];
            In end = begin+res.Size();
            while (mOpItVals[refno].Size() < end+I_1)
            {
               mOpItVals[refno].SetNewSize(mOpItVals[refno].Size()+EXT_DC_SIZE, true);
            }
            mOpItVals[refno].DataIo(IO_PUT, res, res.Size(), begin);
            mOpItValsSize[refno] += res.Size();
            std::pair<std::vector<LocalOperNr>, addr_t> operadd(opers, addr_t(begin, end));
            modes_iter->second.push_back(operadd);
         }
      }
      else
      {
         while (  aIntermed->AssignNextOpersTensor(aEvalData, opers)
               )
         {
            vec_t res; 
            aIntermed->Evaluate(aEvalData, res);

            auto addr = mOpItVals[refno].size();
            mOpItVals[refno].emplace_back(std::move(res));
            std::pair<std::vector<LocalOperNr>, addr_t> operadd(opers, addr);
            modes_iter->second.push_back(operadd);
         }
      }
   }
   
   // Setup iterators for future retrival of intermediates. 
   mOperIter = modes_iter->second.begin();
   mOperIterEnd = modes_iter->second.end();
   
   return true;
}


/**
 * The purpose of this function should be obvious from the above description.
 * @param aIntermed
 * @param aEvalData
 * @param aGenOpers
 * @param arRes
 *
 * @return
 *    False on failure
 **/
template
   <  typename T
   ,  template<typename> typename VECTOR
   ,  template<typename...> typename DATA
   >
bool IntermediateMachine<T, VECTOR, DATA>::GetIntermedNextOpers
   (  opitptr_t& aIntermed
   ,  const evaldata_t& aEvalData
   ,  std::vector<In>& aGenOpers
   ,  vec_t& arRes
   )
{
   constexpr bool is_datacont = std::is_same_v<data_t, GeneralDataCont<param_t>>;

   In refno = aIntermed->RefNo();
   // if refno == -1, then intermediate is not stored
   if (  refno == -I_1
      )
   {
      std::vector<LocalOperNr> opers;
      if (  !aIntermed->AssignNextOpers(aEvalData, opers)
         )
      {
         return false;
      }
      aIntermed->AssignOpers(opers, aGenOpers);
      
      // Though evaluation may return false, this function should return true since it
      // it may be possible to evaluate the intermediate for the next operator set.
      if (  !aIntermed->Evaluate(aEvalData, arRes)
         )
      {
         if constexpr   (  is_datacont
                        )
         {
            arRes.Zero();
         }
         else
         {
            bool cp_scalar = false;
            if constexpr   (  DataAllowsDecompositionV<evaldata_t>
                           )
            {
               cp_scalar = aEvalData.DecomposedTensors();
            }
            arRes = vec_t(new Scalar<param_t>(0.0, cp_scalar));
         }
      }

      return true;
   }

   if (  mOperIterEnd == mOperIter
      )
   {
      return false;
   }

   auto opers = mOperIter->first;
   aIntermed->AssignOpers(opers, aGenOpers);

   if constexpr   (  is_datacont
                  )
   {
      In begin = mOperIter->second.first;
      In end = mOperIter->second.second;
      In len = end-begin;
      arRes.SetNewSize(len);
      mOpItVals[refno].DataIo(IO_GET, arRes, len, begin);
   }
   else
   {
      auto addr = mOperIter->second;
      arRes = mOpItVals[refno][addr];
   }

   ++mOperIter;

   return true;
}

/**
 * This function is called at the beginning of each iteration to clear
 * all old intermediates.
 * If aKeepGlobal is true, intermediates depending only on the
 * ground state are not deleted. This saves recalculation in VCC
 * response calculations.
 *
 * @param aKeepGlobal Should we keep global intermediates, e.g. T intermeds, when clearing IntermediateMachine.
 **/
template
   <  typename T
   ,  template<typename> typename VECTOR
   ,  template<typename...> typename DATA
   >
void IntermediateMachine<T, VECTOR, DATA>::ClearIntermeds
   (  bool aKeepGlobal
   )
{
   for(In i=I_0; i<mStoredIntermeds.size(); ++i)
   {
      if (  !( aKeepGlobal
            && mStoredIntermeds[i]->OnlyAmps()
            )
         )
      {
         mIntermedValsSize[i] = I_0;
         mIntermedAddr[i].clear();

         if constexpr   (  std::is_same_v<VECTOR<T>, NiceTensor<T> >
                        )
         {
            mIntermedVals[i].clear(); // maybe revise
         }
      }
   }

   for(In i=I_0; i<mStoredOpIt.size(); ++i)
   {
      if (  !( aKeepGlobal
            && mStoredOpIt[i]->OnlyAmps()
            )
         )
      {
         mOpItValsSize[i] = I_0;
         mOpItAddr[i].clear();

         if constexpr   (  std::is_same_v<VECTOR<T>, NiceTensor<T> >
                        )
         {
            mOpItVals[i].clear(); // maybe revise
         }
      }
   }
}

/**
 * 
 **/
template
   <  typename T
   ,  template<typename> typename VECTOR
   ,  template<typename...> typename DATA
   >
void IntermediateMachine<T, VECTOR, DATA>::Reset
   (
   )
{
   mStoredIntermeds.clear();
   mStoredIntermedsCount.clear();
   mIntermedAddr.clear();
   mIntermedVals.clear();
   mIntermedValsSize.clear();

   mStoredOpIt.clear();
   mStoredOpItCount.clear();
   mOpItAddr.clear();
   mOpItVals.clear();
   mOpItValsSize.clear();
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename> typename VECTOR
   ,  template<typename...> typename DATA
   >
void IntermediateMachine<T, VECTOR, DATA>::ListRegisteredIntermeds
   (  std::ostream& aOut
   )  const
{
   aOut << " Registered intermediates:" << std::endl
        << " Ref no:    Count:    Intermediate:" << std::endl;
   std::ostringstream os;
   for(In i=I_0; i<mStoredIntermeds.size(); ++i)
   {
      os.str("");
      os << *mStoredIntermeds[i];
      aOut << right << std::setw(7) << i << std::setw(10) << mStoredIntermedsCount[i]
           << "     " << std::setw(70) << left << os.str();
      auto* cmbl = dynamic_cast<IntermedCmbL<T, DATA>* >(mStoredIntermeds[i].get());
      if (cmbl)
         cmbl->PrintScaling(aOut);
      aOut << std::endl;
   }
   
   aOut << std::endl
        << " Registered 'operator iterator' intermediates:" << std::endl
        << " Ref no:    Count:    Intermediate:" << std::endl;
   for(In i=I_0; i<mStoredOpIt.size(); ++i)
   {
      os.str("");
      os << *mStoredOpIt[i];
      aOut << right << std::setw(7) << i << std::setw(10) << mStoredOpItCount[i]
           << "     " << std::setw(70) << left << os.str();
      auto* cmb = dynamic_cast<IntermedCmb<T, DATA>* >(mStoredOpIt[i].get());
      if (cmb)
         cmb->PrintScaling(aOut);
      aOut << std::endl;
   }
   aOut << std::endl;
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename> typename VECTOR
   ,  template<typename...> typename DATA
   >
void IntermediateMachine<T, VECTOR, DATA>::ListRegisteredIntermedsLatex
   (  std::ostream& aOut
   )  const
{
   aOut << "{\\bf Registered intermediates:}" << std::endl
        << "\\begin{longtable}[l]{rllr}" << std::endl
        << "\\hline \\hline" << std::endl
        << "Ref. no.: & Intermediate & Scaling & \\# Registrations \\\\" << std::endl
        << "\\hline" << std::endl;
   
   for(In i=I_0; i<mStoredIntermeds.size(); ++i)
   {
      aOut << std::setw(4) << right << i << " & $";
      std::ostringstream os;
      mStoredIntermeds[i]->Latex(os);
      aOut << std::setw(40) << os.str() << "$ & $";
      os.str("");
      mStoredIntermeds[i]->GetScaling().Latex(os);    // Should also be done for CP tensors
      aOut << std::setw(30) << os.str() << "$ & "
           << std::setw(4) << right << mStoredIntermedsCount[i] << "\\\\" << std::endl;
   }
  
   aOut << "\\hline \\hline" << std::endl
        << "\\end{longtable}" << std::endl;

   aOut << "{\\bf Registered operator iterator intermediates:}" << std::endl
        << "\\begin{longtable}[l]{rllr}" << std::endl
        << "\\hline \\hline" << std::endl
        << "Ref. no.: & Intermediate & Scaling & \\# Registrations \\\\" << std::endl
        << "\\hline" << std::endl;
   
   for(In i=I_0; i<mStoredOpIt.size(); ++i)
   {
      aOut << std::setw(4) << right << i << " & $";
      std::ostringstream os;
      mStoredOpIt[i]->Latex(os);
      aOut << std::setw(40) << os.str() << "$ & $";
      os.str("");
      mStoredOpIt[i]->GetScaling().Latex(os);    // Should also be done for CP tensors
      aOut << std::setw(30) << os.str() << "$ & "
           << std::setw(4) << right << mStoredOpItCount[i] << "\\\\" << std::endl;
   }
  
   aOut << "\\hline \\hline" << std::endl
        << "\\end{longtable}" << std::endl;
}

//! Undef macros
#undef INIT_DC_SIZE
#undef EXT_DC_SIZE

} /* namespace midas::vcc::v3 */

#endif /* INTERMEDIATEMACHINE_IMPL_H_INCLUDED */
