/**
************************************************************************
* 
* @file                IntermedProd_Impl.h
*
* Created:             07-10-2008
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*
* Short Description:   Implementing IntermedProd class.
* 
* Last modified: Mon Mar 22, 2010  05:16PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <typeinfo>
#include <utility>

// midas headers
#include "vcc/v3/IntermedProd_Decl.h"
#include "vcc/v3/Intermediate.h"
#include "vcc/v3/IntermedExci.h"
#include "vcc/v3/IntermediateOperIter.h"
#include "vcc/v3/ModeSum.h"
#include "vcc/v3/Scaling.h"
#include "input/OpDef.h"
#include "util/Fractions.h"
#include "vcc/DirProd.h"
#include "vcc/GenDownCtr.h"
#include "vcc/TransformerV3.h"
#include "vcc/TensorProductAccumulator.h"
#include "tensor/Scalar.h"
#include "EvalDataTraits.h"

namespace midas::vcc::v3
{

/**
 * Construct from std::string specifications.
 *
 * @param aSpec
 *    Specifications std::string defining intermediate product.
 *    Vector of four std::strings.
 *       - aSpec[0]: Coefficient.
 *       - aSpec[1]: Sums.
 *       - aSpec[2]: Intermediates.
 *       - aSpec[3]: ModeGuides.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedProd<T, DATA>::IntermedProd
   (  const std::vector<std::string>& aSpec
   )
   :  V3Contrib<T, DATA>
         (
         )
{
   const std::string& coef  = aSpec[0];  // coefficient
   const std::string& sums  = aSpec[1];  // summation
   const std::string& ims   = aSpec[2];  // intermeds to contract
   const std::string& mgs   = aSpec[3];  // mode guides

   // Read coef.
   std::istringstream coef_ss(coef);
   coef_ss >> mCoef;

   // Read sums.
   std::string::const_iterator pos = sums.begin();
   while (  pos != sums.end()
         )
   {
      if (  *pos == ' '
         )
      {
         pos++;
         continue;
      }
      
      if (  *pos++ == 's'
         )
      {
         mSums.push_back(ModeSum());
         mSums.back().Init(pos);
      }
      else
      {
         std::ostringstream os;
         os << "IntermedProd::IntermedProd(): Unknown sum identifier"
            << " in sum specification for product: '" << std::endl << aSpec[2] << "'" << std::endl;
         MIDASERROR(os.str());
      }
   }

   // Read intermediate specification.
   pos = ims.begin();
   while (  pos != ims.end()
         )
   {
      if (  *pos == ' '
         )
      {
         pos++;
         continue;
      }

      std::string::const_iterator begin = pos;
      while (  *pos != ' '
            && pos != ims.end()
            )
      {
         pos++;
      }
      mIntermeds.push_back(Intermediate<T, DATA>::Factory(std::string(begin, pos)));
   }

   mOpIterIntermed = detail::GetImFromVec<IntermediateOperIter<T, DATA> >(mIntermeds);
   if (  !mOpIterIntermed
      )
   {
      std::ostringstream os;
      os << "IntermedProd::IntermedProd(): No IntermediateOperIter found in" << std::endl
         << "intermediate spec.: '" << ims << "'" << std::endl;
      MIDASERROR(os.str());
   }
      
   mIntermedTau = detail::GetImFromVec<IntermedExci<T, DATA> >(mIntermeds);
   
   if (  mgs.empty()
      )
   {
      MIDASERROR("IntermedProd::IntermedProd(): No mode guides specified.");
   }
   else
   {
      this->AddModeGuides(mgs);
   }

   this->UpdateInternalState();
}

/**
 * Copy constructor
 *
 * @param aOrig           IntermedProd to copy
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedProd<T, DATA>::IntermedProd
   (  const IntermedProd<T, DATA>& aOrig
   )
   :  V3Contrib<T, DATA>
         (  aOrig
         )
   ,  screened
         (  aOrig.screened
         )
   ,  not_screened
         (  aOrig.not_screened
         )
   ,  total
         (  aOrig.total
         )
   ,  mCoef
         (  aOrig.mCoef
         )
   ,  mSums
         (  aOrig.mSums
         )
   ,  mModeGuides
         (  aOrig.mModeGuides
         )
   ,  mExciLevel
         (  aOrig.mExciLevel
         )
   ,  mCmodes
         (  aOrig.mCmodes
         )
   ,  mAssumeOpItStored
         (  aOrig.mAssumeOpItStored
         )
{
   for(In i=I_0; i<aOrig.mIntermeds.size(); ++i)
   {
      mIntermeds.emplace_back(aOrig.mIntermeds[i]->Clone());
   }
   mOpIterIntermed = midas::util::DynamicCastUniquePtr<IntermediateOperIter<T, DATA> >(aOrig.mOpIterIntermed->Clone());
   if (  aOrig.mIntermedTau
      )
   {
      mIntermedTau = midas::util::DynamicCastUniquePtr<IntermedExci<T, DATA> >(aOrig.mIntermedTau->Clone());
   }
   else
   {
      mIntermedTau = nullptr;
   }
}

/**
 * Copy assignment.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
IntermedProd<T, DATA>& IntermedProd<T, DATA>::operator=
   ( const IntermedProd<T, DATA>& aOrig
   )
{
   if (  this == &aOrig
      )
   {
      return *this;
   }

   mCoef             = aOrig.mCoef;
   mSums             = aOrig.mSums;
   mModeGuides       = aOrig.mModeGuides;
   mExciLevel        = aOrig.mExciLevel;
   mCmodes           = aOrig.mCmodes;
   mAssumeOpItStored = aOrig.mAssumeOpItStored;
   screened = aOrig.screened;
   not_screened = aOrig.not_screened;
   total = aOrig.total;

   mIntermeds.clear();
   for (In i=I_0; i<aOrig.mIntermeds.size(); ++i)
   {
      mIntermeds.emplace_back(aOrig.mIntermeds[i]->Clone());
   }

   mOpIterIntermed = midas::util::DynamicCastUniquePtr<IntermediateOperIter<T, DATA> >(aOrig.mOpIterIntermed->Clone());
  
   if (aOrig.mIntermedTau)
   {
      mIntermedTau = midas::util::DynamicCastUniquePtr<IntermedExci<T, DATA> >(aOrig.mIntermedTau->Clone());
   }
   else
   {
      mIntermedTau = nullptr;
   }
      
   return *this;
}

/**
 * Add mode guides specified in std::string as: "(0,1,2)(1,0,2)(2,0,1)".
 * Mode guides assign concrete modes in the bra state to the mode 
 * numbers specified in the guide vectors.
 *
 * @param aGuides          ModeGuide to add in std::string format
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::AddModeGuides
   (  const std::string& aGuides
   )
{
   std::string::const_iterator pos = aGuides.begin();
   while (  pos != aGuides.end()
         )
   {
      std::vector<In> guide;
      detail::ReadVector(pos, guide);
      mModeGuides.emplace_back(guide);
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::UpdateInternalState
   (
   )
{
   mExciLevel = mOpIterIntermed->ExciLevel();
   std::vector<In> all_modes;
   mOpIterIntermed->GetModes(all_modes);

   if (  mIntermedTau
      )
   {
      mExciLevel += mIntermedTau->ExciLevel();
      mIntermedTau->GetModes(all_modes);
   }
   
   for(In i=I_0; i<mIntermeds.size(); ++i)
   {
      mExciLevel += mIntermeds[i]->ExciLevel();
      mIntermeds[i]->GetModes(all_modes);
   }

   std::sort(all_modes.begin(), all_modes.end());
   all_modes.erase(std::unique(all_modes.begin(), all_modes.end()), all_modes.end());
   In max_mode = I_0;
   for(In i=I_0; i<all_modes.size(); ++i)
   {
      if (  all_modes[i] > max_mode
         )
      {
         max_mode = all_modes[i];
      }
   }
   mCmodes.resize(max_mode+I_1);
}

/**
 * Register intermediates of IntermedProd with IntermediateMachine.
 *
 * @param arInMachine           IntermediateMachine to register with
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
template
   <  template<typename> typename VECTOR
   >
void IntermedProd<T, DATA>::RegisterIntermeds
   (  IntermediateMachine<T, VECTOR, DATA>& arInMachine
   )
{
   mOpIterIntermed->RegisterInternalIntermeds(arInMachine);
   arInMachine.RegisterIntermediate(*mOpIterIntermed);
   for(In i=I_0; i<mIntermeds.size(); ++i)
   {
      mIntermeds[i]->RegisterInternalIntermeds(arInMachine);
      arInMachine.RegisterIntermediate(*mIntermeds[i]);
   }
}

/**
 * Evaluates intermediate product
 *
 * @param aEvalData   Data to evaluate for
 * @param arDcOut     Output from evaluation. Must follow adresses from aEvalData.mXvecTrans
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  GeneralDataCont<T>& arDcOut
   )
{
   if (  mIntermedTau
      )
   {
      MIDASERROR("if this error is thrown, just comment it out... just testing something... - Ian");
      if constexpr   (  std::is_same_v<evaldata_t, VccEvalData<Nb> >
                     )
      {
         this->EvaluateLeft(aEvalData, arDcOut);
      }
      return;
   }

   GeneralMidasVector<T> res_vec;
   const ModeCombiOpRange& mcr_p = aEvalData.GetModeCombiOpRange();
   for (In i_mp=I_0; i_mp<mcr_p.Size(); ++i_mp) // loop over mode combinations in mcr
   {
      const ModeCombi& mp = mcr_p.GetModeCombi(i_mp);

      // check that modecombi has same number of modes as intermed prod returns
      if (  mp.Size() != mExciLevel
         )
      {
         continue; // continue loop over mode combinations
      }
      
      In n_exci = aEvalData.NExciForModeCombi(i_mp); // get number of excitation amplitudes in mode combination
      res_vec.SetNewSize(n_exci);
      res_vec.Zero();
      this->EvaluateMp(aEvalData, mp, res_vec);
      
      // Add result to arDcOut.
      arDcOut.DataIo(IO_PUT, res_vec, res_vec.Size(), mp.Address(), I_1, I_0, I_1, false, C_0, true);
   }
}

/**
 * Evaluates intermediate product
 * @param aEvalData   Data to evaluate for
 * @param arDcOut     Output from evaluation. Must follow adresses from aEvalData.mXvecTrans
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  GeneralTensorDataCont<T>& arDcOut
   )
{
   if (  mIntermedTau
      )
   {
      MIDASERROR("if this error is thrown, then implement EvaluateLeft() for TensorDataCont if strictly needed. - Ian");
   }

   const ModeCombiOpRange& mcr_p = aEvalData.GetModeCombiOpRange();
   for (In i_mp = I_0; i_mp < mcr_p.Size(); ++i_mp) // loop over mode combinations in mcr
   {
      const ModeCombi& mp = mcr_p.GetModeCombi(i_mp);
      if (mp.Size() != mExciLevel) // check that modecombi hase same number of modes as intermed prod returns
      {
        continue; // continue loop over mode combinations
      }

      bool is_scalar = mp.Size() == I_0;
      
      NiceTensor<T> res_vec( is_scalar
                            ? static_cast<BaseTensor<T>* >(new Scalar<T>(0.0))
                            : static_cast<BaseTensor<T>* >(new SimpleTensor<T>(aEvalData.GetTensorDims(i_mp)))
                            ); // zeroed by construction
      this->EvaluateMp(aEvalData, mp, res_vec);
      
      // Add result to arDcOut.
      if (  is_scalar
         )
      {
         T scalar;
         res_vec.GetTensor()->DumpInto(&scalar);
         arDcOut.GetModeCombiData(i_mp).ElementwiseScalarAddition(scalar);
      }
      else
      {
         arDcOut.GetModeCombiData(i_mp) += res_vec;
      }
   }
}
/**
 * Evaluates intermediate product.
 * @param aEvalData      Data to evaluate for
 * @param aMc
 * @param arRes
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMc
   ,  GeneralMidasVector<T>& arRes
   )
{
   if (  mIntermedTau
      )
   {
      MIDASERROR("if this error is thrown, then implement EvaluateLeft() for MidasVector if strictly needed. - Ian");
   }

   this->EvaluateMp(aEvalData, aMc, arRes);
}

/**
 * Evaluates intermediate product.
 * \@param arDcOut         Output from evaluation. Must follow adresses from aEvalData.mXvecTrans
 * @param aEvalData      Data to evaluate for
 * @param aMc
 * @param arRes
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMc
   ,  NiceTensor<T>& arRes
   )
{
   if (  mIntermedTau
      )
   {
      MIDASERROR("if this error is thrown, then implement EvaluateLeft() for NiceTensor if strictly needed. - Ian");
   }

   this->EvaluateMp(aEvalData, aMc, arRes);
}

/**
 * Evaluates intermediate product
 * \@param arDcOut         Output from evaluation. Must follow adresses from aEvalData.mXvecTrans
 * @param aEvalData      Data to evaluate for
 * @param aMc
 * @param arRes
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::Evaluate
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMc
   ,  TensorSumAccumulator<T>& arRes
   )
{
   if (  mIntermedTau
      )
   {
      MIDASERROR("if this error is thrown, then implement EvaluateLeft() for TensorSumAccumulator if strictly needed. - Ian");
   }

   this->EvaluateMp(aEvalData, aMc, arRes);
}

#define CREATE_EVALUATEMP(RETURNTYPE)                             \
template                                                          \
   <  typename T                                                  \
   ,  template<typename...> typename DATA                            \
   >                                                              \
void IntermedProd<T, DATA>::EvaluateMp                            \
   (  const evaldata_t& aEvalData /* all data*/                   \
   ,  const ModeCombi& aMp      /* mode combination*/             \
   ,  RETURNTYPE<T>& arRes      /* result from mode combination*/ \
   )                                                              \
{                                                                 \
  for (In i=I_0; i<mModeGuides.size(); ++i)                       \
  {                                                               \
    /* Distribute concrete modes according to mode guide. */      \
    for (In m=I_0; m<aMp.Size(); ++m)                             \
    {                                                             \
      mCmodes[mModeGuides[i][m]] = aMp.Mode(m);                   \
    }                                                             \
                                                                  \
    /* Update sum restrictions with concrete modes. */            \
    for (In s=I_0; s<mSums.size(); ++s)                           \
    {                                                             \
      for (In m=I_0; m<mSums[s].mRestrict.size(); ++m)            \
      {                                                           \
         mSums[s].mCRestrict[m] = mCmodes[mSums[s].mRestrict[m]]; \
      }                                                           \
    }                                                             \
                                                                  \
    DoSum(aEvalData, I_0, I_0, I_0, aMp, arRes);                  \
  }                                                               \
}

CREATE_EVALUATEMP(GeneralMidasVector)
CREATE_EVALUATEMP(NiceTensor)
CREATE_EVALUATEMP(TensorSumAccumulator)

#undef CREATE_EVALUATEMP // undef so we cannot mis-use

#define CREATE_DOSUM(RETURNTYPE)                                  \
template                                                          \
   <  typename T                                                  \
   ,  template<typename...> typename DATA                            \
   >                                                              \
void IntermedProd<T, DATA>::DoSum                                          \
   (  const evaldata_t& aEvalData /* generel data */                 \
   ,  In aSum                   /* specific sum to evaluate */     \
   ,  In aSumIdx                /* mode to sum over */             \
   ,  In aFirstMode             /* first mode in sum */            \
   ,  const ModeCombi& aMp      /* mode combination */             \
   ,  RETURNTYPE<T>& arRes      /* result for mode combination */  \
   )                                                              \
{                                                                 \
   /* if we have a full sum index we start to loop over operators and terminate recursion. */ \
   if (aSum >= mSums.size())                                      \
   {                                                              \
      LoopOperators(aEvalData, aMp, arRes);                        \
      return; /* return (no recursion) */                         \
   }                                                              \
                                                                  \
   const ModeSum& sum = mSums[aSum];                              \
   In mode = sum.mModes[aSumIdx];                                 \
                                                                  \
   for (In cmode=aFirstMode; cmode<aEvalData.NModes(); ++cmode) \
   {                                                              \
      if (sum.mCRestrict.end() != find(sum.mCRestrict.begin(), sum.mCRestrict.end(), cmode)) \
         continue; /* continue */                                 \
      mCmodes[mode] = cmode;                                      \
                                                                  \
      if (aSumIdx == sum.mModes.size()-I_1)                       \
         DoSum(aEvalData, aSum+I_1, I_0, I_0, aMp, arRes); /* call recursively */ \
      else                                                        \
         DoSum(aEvalData, aSum, aSumIdx+I_1, cmode+I_1, aMp, arRes); /* call recursively */ \
   }                                                              \
} 

CREATE_DOSUM(GeneralMidasVector)
CREATE_DOSUM(NiceTensor)
CREATE_DOSUM(TensorSumAccumulator)

#undef CREATE_DOSUM // undef so we cannot mis-use

/**
 * Loop over operators of same MC as current set of concrete modes in IntermedProd.
 *
 * @param aEvalData Evaluation data.
 * @param aMp       Specific mode combination.
 * @param arRes     Residual vector for mode combination.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::LoopOperators
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMp
   ,  GeneralMidasVector<T>& arRes
   )
{
   if constexpr   (  DataAllowsMidasVectorV<evaldata_t>
                  )
   {
      mOpIterIntermed->AssignConcreteModes(mCmodes); // ASSIGN CONCRETE MODES
      if (  !aEvalData.template GetIntermediates<GeneralMidasVector>().InitIntermedOperIter(mOpIterIntermed, aEvalData)
         )
      {
         return;
      }

      std::vector<In> opers;
      opers.resize(mCmodes.size());
      GeneralMidasVector<T> intermed_res;

      // loop over operators
      while (  aEvalData.template GetIntermediates<GeneralMidasVector>().GetIntermedNextOpers(mOpIterIntermed, aEvalData, opers, intermed_res)
            )
      {
         incr_total(); // increment total number of specific intermed products evaluated
         this->EvalIntermeds(aEvalData, aMp, arRes, opers, intermed_res);
      }
   }
   else
   {
      MIDASERROR("Not implemented for given DATA type");
   }
}

/**
 * Loop over operators of same ModeCombi as current set of concrete modes in IntermedProd.
 *
 * @param aEvalData Evaluation data.
 * @param aMp       Specific mode combination.
 * @param arRes     Result NiceTensor for mode combination.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::LoopOperators
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMp
   ,  NiceTensor<T>& arRes
   )
{
   if constexpr   (  DataAllowsNiceTensorV<evaldata_t>
                  )
   {
      mOpIterIntermed->AssignConcreteModes(mCmodes); // ASSIGN CONCRETE MODES
      if (  !aEvalData.template GetIntermediates<NiceTensor>().InitIntermedOperIter(mOpIterIntermed, aEvalData)
         )
      {
         return;
      }
   
      std::vector<In> opers;
      opers.resize(mCmodes.size());
      NiceTensor<T> intermed_res;
   
      // loop over operators
      while (  aEvalData.template GetIntermediates<NiceTensor>().GetIntermedNextOpers(mOpIterIntermed, aEvalData, opers, intermed_res)
            )
      {
         incr_total(); // increment total number of specific intermed products evaluated
         this->EvalIntermeds(aEvalData, aMp, arRes, opers, intermed_res);
      }
   }
   else
   {
      MIDASERROR("Not implemented for given DATA type");
   }
}

/**
 * Loop over operators of same MC as current set of concrete modes in IntermedProd.
 *
 * @param aEvalData Evaluation data.
 * @param aMp       Specific mode combination.
 * @param arRes     Result TensorSumAccumulator for mode combination.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::LoopOperators
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMp
   ,  TensorSumAccumulator<T>& arRes
   )
{
   if constexpr   (  DataAllowsNiceTensorV<evaldata_t>
                  )
   {
      mOpIterIntermed->AssignConcreteModes(mCmodes); // ASSIGN CONCRETE MODES
      if (  !aEvalData.template GetIntermediates<NiceTensor>().InitIntermedOperIter(mOpIterIntermed, aEvalData)
         )
      {
         return;
      }

      std::vector<In> opers;
      opers.resize(mCmodes.size());
      NiceTensor<T> intermed_res;

      // loop over operators
      while (  aEvalData.template GetIntermediates<NiceTensor>().GetIntermedNextOpers(mOpIterIntermed, aEvalData, opers, intermed_res)
            )
      {
         incr_total(); // increment total number of specific intermed products evaluated
         this->EvalIntermeds(aEvalData, aMp, arRes, opers, intermed_res);
      }
   }
   else
   {
      MIDASERROR("Not implemented for given DATA type");
   }
}

/**
 * Evaluate intermediate product for specific set of modes and operator.
 * @param aEvalData  evaluation data
 * @param aMp        mode combination
 * @param arRes      result
 * @param aOpers     operators
 * @param aOpItRes
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::EvalIntermeds
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMp
   ,  GeneralMidasVector<T>& arRes
   ,  const std::vector<In>& aOpers
   ,  const GeneralMidasVector<T>& aOpItRes
   ) 
{
   if constexpr   (  DataAllowsMidasVectorV<evaldata_t>
                  )
   {
      // Initialize quantities for doing direct products of intermediates.
      const In n_in = mIntermeds.size();
      std::vector<GeneralMidasVector<T> > vecs(n_in+I_1);
      std::vector<ModeCombi> mcs(n_in+I_1);
      In cur_vec = I_0;

      T coef = mCoef; 
  
      // Take care of value of intermediate used for looping over operators. 
      if (  aOpItRes.Size() == I_1
         )
      {
         coef *= aOpItRes[I_0];
      }
      else
      {
         vecs[cur_vec].Reassign(aOpItRes);
         mOpIterIntermed->GetExciMc(mcs[cur_vec]);
         cur_vec++;
      }

      /* first we assign concrete modes */
      for(In i=0; i<mIntermeds.size(); ++i)
      {
         if (  !mIntermeds[i]->AssignConcreteModesOpers(mCmodes, aOpers)
            )  // ASSIGN CONCRETE MODES
         {
            return;
         }
      }
      
      /* next if we want screening, calculate intermedprod norm */
      if constexpr   (  midas::vcc::v3::DataAllowsScreeningV<evaldata_t>
                     )
      {
         if (  aEvalData.AllowAmplitudeScreening()
            && this->ScreenIntermedsAmps(aEvalData)
            )
         {
            incr_screened();
            return;
         }
         if (  aEvalData.ScreenIntermeds()
            )
         {
            real_t norm = aOpItRes.Norm2(); // start with operator norm
            
            for(In i=0; i<mIntermeds.size(); ++i)
            {
               real_t new_norm;
               if (  !aEvalData.template GetIntermediates<GeneralMidasVector>().GetIntermedNorm2(*mIntermeds[i], aEvalData, new_norm)
                  )
               {
                  MIDASERROR("this should not happen i guess :S");
                  //return;
               }
               norm *= new_norm;
            }
            if (  norm <= aEvalData.ScreenIntermedsThresh()
               )
            {
               incr_screened();
               return;
            }
         }
      }
      incr_not_screened();

      // If not screened, Loop over and evaluate remaining intermediates.
      for(In i=I_0; i<mIntermeds.size(); ++i)
      {
         // get/calc intermed
         if (  !aEvalData.template GetIntermediates<GeneralMidasVector>().GetIntermed(*mIntermeds[i], aEvalData, vecs[cur_vec])
            )
         {
            return;
         }

         // collect intermed result
         if (  vecs[cur_vec].Size() == I_1
            ) // if dim(intermed) == 0 (scalar) put into coef
         {
            coef *= vecs[cur_vec][I_0];
         }
         else // else let rest in vecs and collect later in direct product
         {
            mIntermeds[i]->GetExciMc(mcs[cur_vec]); // save which mc now has index cur_vec
            cur_vec++;
         }
      }


      /* Test for very simple cases. */
      if (  I_0 == cur_vec
         )
      {
         GeneralMidasVector<T> add(I_1, coef);
         arRes += add;
         return;
      }
      else if  (  I_1 == cur_vec
               )
      {
         arRes += coef * vecs[I_0];
         return;
      }
 
      /* Do direct product... */
      vecs.erase(vecs.begin()+cur_vec, vecs.end()); // The last vectors are not used.
      
      // get modal numbers
      std::vector<In> res_modals(aMp.Size());
      for (In i=I_0; i<aMp.Size(); ++i)
      {
         res_modals[i] = aEvalData.NModals(aMp.Mode(i))-I_1;
      }

      std::vector<std::vector<In> > modals(cur_vec);
      for (In i=I_0; i<cur_vec; ++i)
      {
         modals[i].resize(mcs[i].Size());
         for (In k=I_0; k<mcs[i].Size(); ++k)
         {
            modals[i][k] = aEvalData.NModals(mcs[i].Mode(k))-I_1;
         }
      }
      
      // do direct product
      dirprod::DirProd2(arRes, aMp, res_modals, vecs, mcs, modals, coef);
   }
   else
   {
      MIDASERROR("Not implemented for given DATA type");
   }
}

/**
 * Evaluate intermediate product for specific set of modes and operator
 *
 * @param aEvalData Evaluation data.
 * @param aMp       Mode combination.
 * @param arRes     Result of evaluation.
 * @param aOpers    Operators.
 * @param aOpItRes
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::EvalIntermeds
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMp
   ,  NiceTensor<T>& arRes
   ,  const std::vector<In>& aOpers
   ,  const NiceTensor<T>& aOpItRes
   ) 
{
   if constexpr   (  DataAllowsNiceTensorV<evaldata_t>
                  )
   {
      // Initialize quantities for doing direct products of intermediates.
      const In n_in = mIntermeds.size();
      
      // create tensor product accumulator to take care of direct products
      TensorProductAccumulator<T> tensor_accumulator(mCoef, n_in);
      ModeCombi op_mc;
      mOpIterIntermed->GetExciMc(op_mc);
      tensor_accumulator.AddTensor(std::move(const_cast<NiceTensor<T>&>(aOpItRes)), std::move(op_mc));

      // first we assign concrete modes
      for(In i=0; i<mIntermeds.size(); ++i)
      {
         if (  !mIntermeds[i]->AssignConcreteModesOpers(mCmodes, aOpers)
            ) 
         {
            return;
         }
      }
      
      // If not screened, Loop over and evaluate remaining intermediates.
      incr_not_screened();
      for (In i = I_0; i < mIntermeds.size(); ++i)
      {
         NiceTensor<T> intermed_result;
         ModeCombi mc;

         // get/calc intermed
         if (  !aEvalData.template GetIntermediates<NiceTensor>().GetIntermed(*mIntermeds[i], aEvalData, intermed_result)
            )
         {
            return;
         }

         mIntermeds[i]->GetExciMc(mc); // get intermed mode combi
         tensor_accumulator.AddTensor(std::move(intermed_result), std::move(mc));
      }

      // Do direct product
      tensor_accumulator.DirProd(arRes, aMp);
   }
   else
   {
      MIDASERROR("Not implemented for given DATA type");
   }
}

/**
 * Evaluate intermediate product for specific set of modes and operator
 *
 * @param aEvalData Evaluation data
 * @param aMp       Mode combination
 * @param arRes     Result of evaluation
 * @param aOpers    Operators               
 * @param aOpItRes
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::EvalIntermeds
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMp
   ,  TensorSumAccumulator<T>& arRes
   ,  const std::vector<In>& aOpers
   ,  const NiceTensor<T>& aOpItRes
   ) 
{
   if constexpr   (  DataAllowsNiceTensorV<evaldata_t>
                  )
   {
      // Initialize quantities for doing direct products of intermediates.
      const In n_in = mIntermeds.size();
      
      // create tensor product accumulator to take care of direct products
      std::unique_ptr<TensorProductAccumulator<T> > tensor_product_accumulator = nullptr;

      // For complex numbers, we do not do decompositions and hence not screening, etc.
      // This allows for NOT defining TensorProductScreening, etc. functions in evaldata_t for complex numbers.
      if constexpr   (  midas::vcc::v3::DataAllowsDecompositionV<evaldata_t>
                     )
      {
         tensor_product_accumulator = std::make_unique<TensorProductAccumulator<T> >
                                 (  mCoef
                                 ,  n_in
                                 ,  aEvalData.TensorProductScreening().first
                                 ,  aEvalData.TensorProductScreening().second
                                 ,  aEvalData.TensorProductLowRankLimit()
                                 );
      }
      else
      {
         tensor_product_accumulator = std::make_unique<TensorProductAccumulator<T> >
                                 (  mCoef
                                 ,  n_in
                                 );
      }
      ModeCombi op_mc;
      mOpIterIntermed->GetExciMc(op_mc);
      tensor_product_accumulator->AddTensor(std::move(const_cast<NiceTensor<T>&>(aOpItRes)), std::move(op_mc));

      // first we assign concrete modes
      for(In i=0; i<mIntermeds.size(); ++i)
      {
         if (  !mIntermeds[i]->AssignConcreteModesOpers(mCmodes, aOpers)
            )
         {
            return;
         }
      }
      
      // If not screened, Loop over and evaluate remaining intermediates.
      incr_not_screened();
      for (In i = I_0; i < mIntermeds.size(); ++i)
      {
         NiceTensor<T> intermed_result;
         ModeCombi mc;

         // get/calc intermed
         if (  !aEvalData.template GetIntermediates<NiceTensor>().GetIntermed(*mIntermeds[i], aEvalData, intermed_result)
            )
         {
            return;
         }
         
         mIntermeds[i]->GetExciMc(mc); // get intermed mode combi
         tensor_product_accumulator->AddTensor(std::move(intermed_result), std::move(mc));
      }

      // Do direct product
      tensor_product_accumulator->DirProd(arRes, aMp);
   }
   else
   {
      MIDASERROR("Not implemented for given DATA type");
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
template
   <  typename D
   >
void IntermedProd<T, DATA>::EvaluateLeft
   (  const evaldata_t& aEvalData
   ,  GeneralDataCont<T>& arDcOut
   ,  std::enable_if_t<std::is_same_v<D, VccEvalData<Nb> > >* apDisable
   )
{
   GeneralMidasVector<T> lvec;
   const auto& mcr_p = aEvalData.GetModeCombiOpRange();
   for (In i_mp=I_0; i_mp<mcr_p.Size(); ++i_mp)
   {
      const ModeCombi& mp = mcr_p.GetModeCombi(i_mp);
      if (mp.Size() != mExciLevel)
         continue;

      In n_exci = aEvalData.NExciForModeCombi(i_mp);
      lvec.SetNewSize(n_exci);
      aEvalData.template GetExciVec<GeneralDataCont<T> >(OP::L)->DataIo(IO_GET, lvec, lvec.Size(), mp.Address(), I_1, I_0, I_1, false, C_0, true);

      EvaluateMpLeft(aEvalData, mp, lvec, arDcOut);
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
template
   <  typename D
   >
void IntermedProd<T, DATA>::EvaluateMpLeft
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMp
   ,  const GeneralMidasVector<T>& aLvec
   ,  GeneralDataCont<T>& arDcOut
   ,  std::enable_if_t<std::is_same_v<D, VccEvalData<Nb> > >* apDisable
   )
{
   for (In i=I_0; i<mModeGuides.size(); ++i)
   {
      // Distribute concrete modes according to mode guide.
      for (In m=I_0; m<aMp.Size(); ++m)
         mCmodes[mModeGuides[i][m]] = aMp.Mode(m);

      // Update sum restrictions with concrete modes.
      for (In s=I_0; s<mSums.size(); ++s)
      {
         for (In m=I_0; m<mSums[s].mRestrict.size(); ++m)
            mSums[s].mCRestrict[m] = mCmodes[mSums[s].mRestrict[m]];
      }
      DoSumLeft(aEvalData, I_0, I_0, I_0, aMp, aLvec, arDcOut);
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
template
   <  typename D
   >
void IntermedProd<T, DATA>::DoSumLeft
   (  const evaldata_t& aEvalData
   ,  In aSum
   ,  In aSumIdx
   ,  In aFirstMode
   ,  const ModeCombi& aMp
   ,  const GeneralMidasVector<T>& aLvec
   ,  GeneralDataCont<T>& arDcOut
   ,  std::enable_if_t<std::is_same_v<D, VccEvalData<Nb> > >* apDisable
   )
{
   if (aSum > In(mSums.size())-I_1)              // Be careful with signed / unsigned conversion
   {                                             // as mSums.size() is unsigned.
      this->LoopOperatorsLeft(aEvalData, aMp, aLvec, arDcOut);
      return;
   }
      
   const ModeSum& sum = mSums[aSum];
   In mode = sum.mModes[aSumIdx];

   for (In cmode = aFirstMode; cmode<aEvalData.NModes(); ++cmode)
   {
      if (sum.mCRestrict.end() != find(sum.mCRestrict.begin(), sum.mCRestrict.end(), cmode))
         continue;
      mCmodes[mode] = cmode;

      if (aSumIdx == sum.mModes.size()-I_1)
         DoSumLeft(aEvalData, aSum+I_1, I_0, I_0, aMp, aLvec, arDcOut);
      else
         DoSumLeft(aEvalData, aSum, aSumIdx+I_1, cmode+I_1, aMp, aLvec, arDcOut);
   }
}

/**
 *
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
template
   <  typename D
   >
void IntermedProd<T, DATA>::LoopOperatorsLeft
   (  const evaldata_t& aEvalData
   ,  const ModeCombi& aMp
   ,  const GeneralMidasVector<T>& aLvec
   ,  GeneralDataCont<T>& arDcOut
   ,  std::enable_if_t<std::is_same_v<D, VccEvalData<Nb> > >* apDisable
   )
{
   mOpIterIntermed->AssignConcreteModes(mCmodes);
   if (  !aEvalData.template GetIntermediates<GeneralMidasVector>().InitIntermedOperIter(mOpIterIntermed, aEvalData)
      )
   {
      return;
   }
   
   // Check if we can assign the concrete modes to the tau intermediate.
   // If yes, get relevant MCs and address in result data container.
   std::vector<In> dummy_ops(mCmodes.size(), -I_1);
   if (  !mIntermedTau->AssignConcreteModesOpers(mCmodes, dummy_ops)
      )
   {
      return;
   }
   ModeCombi mc_tau_exci;                // MC of excited modes in tau.
   ModeCombi mc_tau_tot;                 // Total MC of tau, i.e. MC of result.
   mIntermedTau->GetExciMc(mc_tau_exci);
   mIntermedTau->GetTotMc(mc_tau_tot);
   ModeCombiOpRange::const_iterator it_mc;
   In i_mc;
   if (  !aEvalData.GetModeCombiOpRange().Find(mc_tau_tot, it_mc, i_mc)
      )
   {
      return;
   }
   mc_tau_tot.AssignAddress(it_mc->Address());
   In res_size = aEvalData.NExciForModeCombi(i_mc);
   
   // Figure out the MC for the product of intermediates to be evaluated.
   ModeCombi mc_imp;                              // MC for product of intermediates.
   mc_imp.InFirstOnly(aMp, mc_tau_exci);
 
   // Initialize vector for storing product of intermediates.
   In n_im = I_1;
   for (In i=I_0; i<mc_imp.Size(); ++i)
      n_im *= (aEvalData.NModals(mc_imp.Mode(i)) - I_1);
   GeneralMidasVector<T> im_res(n_im);

   GenDownCtr<T, DATA> gd_ctr(aEvalData, aMp, mc_imp);
   GeneralMidasVector<T> down_res(aLvec.Size() / im_res.Size());  // Result of generalized down contraction.
   
   std::vector<In> opers;
   opers.resize(mCmodes.size());
   GeneralMidasVector<T> opit_res;
   GeneralMidasVector<T> res;
   res.SetNewSize(res_size);
   while (  aEvalData.template GetIntermediates<GeneralMidasVector>().GetIntermedNextOpers(mOpIterIntermed, aEvalData, opers, opit_res)
         )
   {
      im_res.Zero();
      this->EvalIntermeds(aEvalData, mc_imp, im_res, opers, opit_res);

      down_res.Zero();
      gd_ctr.Contract(aLvec, im_res, down_res);
 
      mIntermedTau->AssignConcreteModesOpers(mCmodes, opers);
      res.Zero();
      if (! mIntermedTau->ActLeft(aEvalData, down_res, res))
         MIDASERROR("IntermedProd::LoopOperatorsLeft(): mIntermedTau->ActLeft() failed.");

      arDcOut.DataIo(IO_PUT, res, res_size, mc_tau_tot.Address(), I_1, I_0, I_1, false, C_0, true);
   }
}

namespace detail
{
/**
 * Utility function used by IntermedProd::IdentifyCmbIntermeds().
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
int IdentifyCmbIntermeds_CmpUtil
   (  const std::pair<std::unique_ptr<IntermedProd<T, DATA> >, std::vector<Scaling> >& aL
   ,  const std::pair<std::unique_ptr<IntermedProd<T, DATA> >, std::vector<Scaling> >& aR
   )
{
   return aL.second < aR.second;
}
} /* namespace detail */

/**
 * Public interface function for identifying cmb. intermediates.
 * The specific choice of cmb. intermediates is determined by the lowest
 * possible scaling wrt. the number of modes.
 *
 * @param aMaxScaling      Will be set to the maximum resulting scaling including intermediates.
 * @param aCpTensors       True if we are using CP tensors
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::IdentifyCmbIntermeds
   (  Scaling& aMaxScaling
   ,  bool aCpTensors
   )
{
   aMaxScaling.Reset();
   std::vector<std::pair<std::unique_ptr<IntermedProd>, std::vector<Scaling> > > final_list;
   std::vector<Scaling> tmp;
   IdentifyCmbIntermeds(midas::util::DynamicCastUniquePtr<IntermedProd<T, DATA> >(this->Clone()), tmp, final_list, aCpTensors);
   std::sort(final_list.begin(), final_list.end(), detail::IdentifyCmbIntermeds_CmpUtil<T, DATA>);
   
   // Call copy assign
   *this = *(final_list[0].first);
   UpdateInternalState();
   aMaxScaling = final_list[0].second.front();
}

/* private function for doing the work */
template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::IdentifyCmbIntermeds
   (  std::unique_ptr<IntermedProd>&& aImProd
   ,  std::vector<Scaling>& aScalings
   ,  std::vector<std::pair<std::unique_ptr<IntermedProd>, std::vector<Scaling> > >& aFinalList
   ,  bool aCpTensors
   )  const
{
   std::vector<In> exci_modes;                 // Excited modes in intermediate product.
   GetAllExciModes(exci_modes);
   
   bool improved = false;
   //loop over sums to see if we can improve intermedprod scaling
   for (In i_sum=I_0; i_sum<aImProd->mSums.size(); ++i_sum) 
   {
      const ModeSum& sum = aImProd->mSums[i_sum];
      if (  !sum.mRestrict.empty()
         ) // if sum is restricted we cannot improve
      {
         continue;
      }
      
      // Find the intermediate corresponding to the modes in this sum.
      In im;
      for (im=I_0; im<aImProd->mIntermeds.size(); ++im)
      {
         std::vector<In> immodes;
         aImProd->mIntermeds[im]->GetModes(immodes);
         std::sort(immodes.begin(), immodes.end());
         if (  std::includes(immodes.begin(), immodes.end(), sum.mModes.begin(), sum.mModes.end())
            )
         {
            break;
         }
      }
      if (  im == aImProd->mIntermeds.size()
         )
      {
         // This can happen if the mIntermedTau is used for left-hand transformations.
         continue;
      }

      // Generate combined intermediate.
      std::vector<In> occ1;
      std::vector<In> occ2;
      std::vector<In> occ_cmn;
      std::vector<In> cmn_restrict;
      aImProd->mOpIterIntermed->GetOperOccModes(occ1);
      aImProd->mIntermeds[im]->GetOperOccModes(occ2);
      std::sort(occ1.begin(), occ1.end());
      std::sort(occ2.begin(), occ2.end());
      std::set_intersection(occ1.begin(), occ1.end(), occ2.begin(), occ2.end(), std::back_inserter(occ_cmn));
      std::set_intersection(occ_cmn.begin(), occ_cmn.end(), exci_modes.begin(), exci_modes.end(), std::back_inserter(cmn_restrict));
      auto cmb = std::make_unique<IntermedCmb<T, DATA> >
         (  midas::util::DynamicCastUniquePtr<IntermediateOperIter<T, DATA>>(aImProd->mOpIterIntermed->Clone())
         ,  aImProd->mIntermeds[im]->Clone()
         ,  cmn_restrict
         );
      
      // Only keep this one if scaling is not increased.
      cmb->SetAssumeOpItStored(true);
      aImProd->mAssumeOpItStored = true;
      Scaling imp_sc = aImProd->GetScaling(aCpTensors);
      Scaling cmb_sc = cmb->GetScaling(aCpTensors);
      cmb->SetAssumeOpItStored(false);
      aImProd->mAssumeOpItStored = false;
      Scaling max_sc(imp_sc);
      if (! aScalings.empty())
         max_sc = std::max(imp_sc, aScalings.front());
      
      if (cmb_sc <= max_sc) // if this intermed combination gives better scaling 
      {                     // we construct a new intermedprod from it and call recursively
         improved = true;
         auto cp = midas::util::DynamicCastUniquePtr<IntermedProd<T, DATA> >(aImProd->Clone());
         cp->mOpIterIntermed = std::move(cmb);
         cp->mIntermeds.erase(cp->mIntermeds.begin()+im);
         cp->mSums.erase(cp->mSums.begin()+i_sum);
         std::vector<Scaling> cp_sc(aScalings);
         cp_sc.push_back(cmb_sc);
         std::sort(cp_sc.rbegin(), cp_sc.rend());
         IdentifyCmbIntermeds(std::move(cp), cp_sc, aFinalList, aCpTensors);
      }
   }

   // if we did not improve in this "iteration" we save the intermedprod
   // in the final list of "the best" intermedprods
   if (  !improved
      )
   {
      aImProd->mAssumeOpItStored = true;
      Scaling imp_sc = aImProd->GetScaling(aCpTensors);
      aImProd->mAssumeOpItStored = false;
      aScalings.push_back(imp_sc);
      std::sort(aScalings.rbegin(), aScalings.rend());
      aFinalList.push_back(std::make_pair(std::move(aImProd), aScalings));
   }
}

template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::GetAllModes
   (  std::vector<In>& aModes
   )  const
{
   std::vector<In> modes;                      // All modes in intermediate product.
   mOpIterIntermed->GetModes(modes);
   if (  mIntermedTau
      )
   {
      mIntermedTau->GetModes(modes);
   }
   for (In i=I_0; i<mIntermeds.size(); ++i)
   {
      mIntermeds[i]->GetModes(modes);
   }
   std::sort(modes.begin(), modes.end());
   modes.erase(std::unique(modes.begin(), modes.end()), modes.end());
   std::copy(modes.begin(), modes.end(), std::back_inserter(aModes));
}

template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::GetAllExciModes
   (  std::vector<In>& aModes
   )  const
{
   std::vector<In> exci_modes;                 // Excited modes in intermediate product.
   mOpIterIntermed->GetExciModes(exci_modes);
   if (  mIntermedTau
      )
   {
      mIntermedTau->GetExciModes(exci_modes);
   }
   for (In i=I_0; i<mIntermeds.size(); ++i)
   {
      mIntermeds[i]->GetExciModes(exci_modes);
   }
   std::sort(exci_modes.begin(), exci_modes.end());
   exci_modes.erase(std::unique(exci_modes.begin(), exci_modes.end()), exci_modes.end());
   std::copy(exci_modes.begin(), exci_modes.end(), std::back_inserter(aModes));
}

/**
 * Return the scaling of the intermediate product.
 * Result depends on whether intermediates are stored by the intermediate machine
 * (checked by Intermediate::RefNo()).
 * It is assumed that mOpIterIntermed is the only intermediate where the
 * number of intermediates returned by Intermediate::GetNumber() may have any
 * scaling with \#modes and \#operators.
 **/ 
template
   <  typename T
   ,  template<typename...> typename DATA
   >
Scaling IntermedProd<T, DATA>::GetScaling
   (  bool aCpTensors
   )  const
{  
   // Scaling wrt. number of modes.
   std::vector<In> modes;
   GetAllModes(modes);
   In m = modes.size();            // Scaling: M^(#modes).

   // Scaling wrt. number of operators O is O^(#modes with assoc. operator index).
   std::vector<In> op_modes;
   mOpIterIntermed->GetOperOccModes(op_modes);
   mOpIterIntermed->GetOperExciModes(op_modes);
   In o = op_modes.size();
  
   // If the mOpIterIntermediate is not stored, some extra work may be required. 
   if (  !mAssumeOpItStored
      && mOpIterIntermed->RefNo() == -I_1
      )
   {
      m += mOpIterIntermed->GetCost(aCpTensors).mM;
      o += mOpIterIntermed->GetCost(aCpTensors).mO;
   }
  
   // Scaling wrt. number of modals: May be the cost of an intermediate or the cost
   // of the final direct product.
   In n = mOpIterIntermed->GetNumber().mN;
   In r = -I_1;
   if (  !mAssumeOpItStored
      && mOpIterIntermed->RefNo() == -I_1
      )
   {
      auto cost = mOpIterIntermed->GetCost(aCpTensors);
      n = cost.mN;
      r = cost.mR;
   }

   for (In i=I_0; i<mIntermeds.size(); ++i)
   {
      In im_cost_n = I_0;
      In im_cost_r = -I_1;
      if (  mIntermeds[i]->RefNo() == -I_1
         )
      {
         auto cost = mIntermeds[i]->GetCost(aCpTensors);
         im_cost_n = cost.mN;
         im_cost_r = cost.mR;
      }
      else
      {
         im_cost_n = mIntermeds[i]->GetNumber().mN;
      }
      n = std::max(im_cost_n, n);
      r = std::max(im_cost_r, r);
   }
   
   // N^mExciLevel == Direct product scaling or scaling of generalized down contraction
   // in case of left-hand transformation.
   if (  !aCpTensors    // Direct products are "free" for CP tensors
      )
   {
      n = std::max(n, mExciLevel);
   }
  
   // Special treatment in case of left-hand transformation.
   if (  mIntermedTau
      )
   {
      if (  aCpTensors
         )
      {
         MIDASERROR("Scaling of left transformation not implemented for CP tensors!");
      }
      In n_tau = mIntermedTau->ExciLevel();
      if (mIntermedTau->Nforw() != I_0)
         n_tau++;
      n = std::max(n, n_tau);
   }
   
   return Scaling(m, o, n);
}

template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::Latex
   (  std::ostream& aOut
   )  const
{
   std::string s;
   FracFloatLatex(std::real(mCoef), s, true);
   std::ostringstream os;
   os << std::setw(10) << std::left << s;
   aOut << "$" << os.str() << "$ & $";

   std::vector<In> exci_modes;
   GetAllExciModes(exci_modes);
   if (  exci_modes.size() > I_0
      )
   {
      os.str("");
      os << "\\mathcal{D}^{";
      for (In i=I_0; i<exci_modes.size(); ++i)
         os << "m'_{" << exci_modes[i] << "}";
      os << "}_{";
      for (In i=I_0; i<exci_modes.size(); ++i)
         os << "m_{" << exci_modes[i] << "}";
      os << "}";
   }
   aOut << std::setw(30) << std::left << os.str() << " $ & $";
   
   os.str("");
   for (In i=I_0; i<mSums.size(); ++i)
   {
      mSums[i].Latex(os);
      os << " ";
   }
   aOut << std::setw(40) << std::left << os.str() << "$ & $";
   
   os.str("");
   mOpIterIntermed->Latex(os);
   for (In i=I_0; i<mIntermeds.size(); ++i)
   {
      os << " \\; \\cdot \\;";
      mIntermeds[i]->Latex(os);
   }
   aOut << std::setw(80) << std::left << os.str() << "$ & $";
 
   os.str(""); 
   GetScaling().Latex(os);    // Should also be done for CP tensors
   aOut << std::setw(20) << std::left << os.str() << "$ \\\\";
}

template
   <  typename T
   ,  template<typename...> typename DATA
   >
std::ostream& IntermedProd<T, DATA>::Print
   (  std::ostream& aOut
   )  const
{
   using ::operator<<;
   midas::stream::ScopedPrecision(5, aOut);
   aOut << std::fixed << std::showpos << mCoef << std::noshowpos << std::scientific;
   std::ostringstream os;
   aOut << " sum size: " << mSums.size() << std::endl;
   for (In i=I_0; i<mSums.size(); ++i)
      os << " " << mSums[i];
   aOut << std::setw(30) << std::left << os.str();

   os.str("");
   os << *mOpIterIntermed << " ";
   for (In i=I_0; i<mIntermeds.size(); ++i)
      os << *mIntermeds[i] << " ";
   if (mIntermedTau)
      os << *mIntermedTau << " ";
   aOut << std::setw(60) << os.str();

   os.str("");
   for (In i=I_0; i<mModeGuides.size(); ++i)
      os << mModeGuides[i];
   aOut << std::setw(50) << os.str();

   aOut << "   " << GetScaling();       // Should also be done for CP tensors
  
   return aOut;
}

template
   <  typename T
   ,  template<typename...> typename DATA
   >
void IntermedProd<T, DATA>::WriteSpec
   (  std::ostream& aOut
   )  const
{
   using ::operator<<;
   midas::stream::ScopedPrecision(16, aOut);
   aOut << "IP '" << std::scientific << std::showpos << mCoef << std::noshowpos << "' '";
   std::ostringstream os;
   for (In i=I_0; i<mSums.size(); ++i)
   {
      mSums[i].WriteSpec(os);
      os << " ";
   }
   aOut << std::setw(30) << std::left << os.str();
   aOut << "' '";
   os.str("");
   mOpIterIntermed->WriteSpec(os);
   os << " ";
   for (In i=I_0; i<mIntermeds.size(); ++i)
   {
      mIntermeds[i]->WriteSpec(os);
      os << " ";
   }
   if (mIntermedTau)
      mIntermedTau->WriteSpec(os);
   aOut << std::setw(60) << os.str();
   aOut << "' '";
   for (In i=I_0; i<mModeGuides.size(); ++i)
      aOut << mModeGuides[i];
   aOut << "'";
}

template
   <  typename T
   ,  template<typename...> typename DATA
   >
bool IntermedProd<T, DATA>::ScreenIntermedsAmps
   (  const evaldata_t& aEvalData
   )  const
{
   if constexpr   (  DataAllowsScreeningV<DATA<T> >
                  )
   {
      Nb norm2;
      
      for(In i=0; i<mIntermeds.size(); ++i)
      {
         if(dynamic_cast<IntermedT<T, DATA>* >(mIntermeds[i].get()))
         {
            mIntermeds[i]->AmpsNorm2(aEvalData,norm2);
            if(norm2 <= aEvalData.ScreenIntermedsAmpsThresh())
            {
               return true;
            }
         }
      }

      return false;
   }
   else
   {
      MIDASERROR("IntermedProd: Screening not implemented for this DATA type. This function should not be called!");
      return false;
   }
}

} /* namespace midas::vcc::v3 */
