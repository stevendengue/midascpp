/**
************************************************************************
* 
* @file    TensorGeneralDownContraction_Decl.h
*
* @date    13-09-2017
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Wrapper for multi-index down contraction using the NiceTensor framework.
*
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef TENSORGENERALDOWNCONTRACTION_DECL_H_INCLUDED
#define TENSORGENERALDOWNCONTRACTION_DECL_H_INCLUDED

#include <vector>
#include <set>

#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"

// Forward decl.
namespace midas::vcc::v3
{
template
   <  typename T
   >
class VccEvalData;
} /* namespace midas::vcc::v3 */

class ModeCombi;

class any_type;

template
   <  class T
   >
class NiceTensor;

namespace midas::tensor
{
class TensorDecompInfo;
}  /* namespace midas::tensor */


namespace midas::vcc
{

/**
 * Wrapper class for performing multi-index down contractions during left-hand transformations.
 **/
template
   <  typename T
   ,  template<typename...> typename DATA
   >
class TensorGeneralDownContraction
{
   public:
      //!
      using evaldata_t = DATA<T>;

      //! IDs for different algorithms
      enum class algID: int
      {
         GENERAL = 0    ///< General case
      ,  ONEINDEX       ///< 1D case (one-index down contraction)
      ,  ALL            ///< Dot product in case all indices are contracted
      ,  SCALE          ///< No contraction, just scale the <L| tensor
      };

   private:
      //! Vector of index numbers to be contracted
      std::vector<unsigned> mContractionIndices;

      //! Type of contraction
      algID mAlg = algID::GENERAL;

      //! Recompress tensors after contraction
      bool mRecompressContractionResult = true;

      //! Set mEvaluateUpToCanonical in scalar results
      bool mCanonicalScalar = false;

      //! Decomposition info
      const std::set<midas::tensor::TensorDecompInfo>* mDecompInfo = nullptr;

      //! Implementation of general contraction
      NiceTensor<T> ContractGeneralImpl
         (  const NiceTensor<T>&
         ,  const NiceTensor<T>&
         )  const;

      //! Implementation of one-index contraction
      NiceTensor<T> ContractOneIndexImpl
         (  const NiceTensor<T>&
         ,  const NiceTensor<T>&
         )  const;

      //! Implementation of all-indices contraction
      NiceTensor<T> ContractAllIndicesImpl
         (  const NiceTensor<T>&
         ,  const NiceTensor<T>&
         )  const;

   public:
      //! Delete default constructor
      TensorGeneralDownContraction() = delete;

      //! Constructor from EvalData and ModeCombi%s
      TensorGeneralDownContraction
         (  const evaldata_t&
         ,  const ModeCombi&
         ,  const ModeCombi&
         );

      //! Perform contraction
      NiceTensor<T> Contract
         (  const NiceTensor<T>&
         ,  const NiceTensor<T>&
         )  const;
};

} /* namespace midas::vcc */

#endif /* TENSORGENERALDOWNCONTRACTION_DECL_H_INCLUDED */
