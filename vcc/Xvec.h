/**
************************************************************************
* 
* @file                Xvec.h
*
* Created:             02-01-2003
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Class for containing a general excitation vector  
* 
* Last modified: Wed Oct 08, 2008  06:31PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef XVEC_H
#define XVEC_H

#include <vector>
#include <string>

#include "libmda/util/any_type.h"

#include "inc_gen/TypeDefs.h"
#include "input/ModeCombiOpRange.h"
#include "input/OpDef.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"
#include "input/VccCalcDef.h"

template
   <  typename T
   >
class GeneralTensorDataCont;
using TensorDataCont = GeneralTensorDataCont<Nb>;

/**
 * @brief Excitation vector.
 **/
class Xvec 
{
   public:
      /* Assign tags */
      static struct load_in_t  {}   load_in;
      static struct load_out_t {}   load_out;
      static struct add_ref_t  {}   add_ref;
      static struct no_add_ref_t {} no_add_ref;

   private:
      using XvecData = libmda::util::any_type;

   private:
      ModeCombiOpRange  mExciRange;        ///< The excitation range.
      XvecData          mXvecData;         ///< The data vector.
      std::vector<In>   mModeCombiAddress; ///< The address of the first element of a MC.
      In                mNmodes;           ///< The number of modes in the calculation.
      
      std::vector<In>   mExciLevelAddress;
      //< The addresses where parameters for each excitation level starts.
      //< This supposes that the parameters are stored in order!.
 
      VccCalcDef*       mpVccCalcDef; ///< Pointer to the VccCalcDef defining the calc.

      std::vector<Nb>   mNorms2;      ///< Norm squared of coefs. for each
                                      ///< excitation type.
      
   public:
      Xvec();
      ~Xvec() = default; ///< default destructor

      const ModeCombiOpRange& ExciRange() const { return mExciRange; }
      ModeCombiOpRange&       ExciRange()       { return mExciRange; }
      const DataCont& GetXvecData() const { try { return mXvecData.get<DataCont>(); } catch(...) { MIDASERROR("Trying to get DataCont, but is holding TensorDataCont"); } return mXvecData.get<DataCont>(); }
      DataCont&       GetXvecData()       { try { return mXvecData.get<DataCont>(); } catch(...) { MIDASERROR("Trying to get DataCont, but is holding TensorDataCont"); } return mXvecData.get<DataCont>(); }
      const TensorDataCont& GetTensorXvecData() const;
      TensorDataCont&       GetTensorXvecData();
      const std::vector<Nb>& Norms2() const { return mNorms2; }

      void ConvertToDataCont();
      void ConvertToTensorDataCont();
      
      // Assign data to/from Xvec
      void Assign(const TensorDataCont&, load_in_t, add_ref_t); // Assign
      void Assign(TensorDataCont&, load_out_t, add_ref_t) const; // Assign
      void Assign(const TensorDataCont&, load_in_t, no_add_ref_t); // Assign
      void Assign(TensorDataCont&, load_out_t, no_add_ref_t) const; // Assign

      In ModeCombiAddress(In aI) const { return mModeCombiAddress[aI]; } ///< Get the address for a given MC.
      
      void SetModeCombiAddress
         (  const In& arModeCombiNr
         ,  const In& arAddress
         )
      {
         mModeCombiAddress[arModeCombiNr] = arAddress;
      }   ///< Set the address for a given MC.
      
      void ReInit
         (  VccCalcDef* apVccCalcDef
         ,  const OpDef* const apOpDef
         ,  const In aIsOrder=I_0
         ,  bool aLeftOper=false
         ,  Xvec* apXvecLeft=nullptr
         );
      ///< Initialize 

      void ReInit2
         (  VccCalcDef* apVccCalcDef
         ,  In aPrecondExciLevel
         ,  Xvec* apXvecOld
         ,  const OpDef* const apOpDef=nullptr
         );
      ///< Initialize smaller Xvec for improved preconditioning
      
      void CopyNoData(const Xvec&);
      
      ///> Get total number of data elements.
      In Size() const; 

      void PrintWeightsInMCs
         (  const vector<vector<In> >&
         ,  DataCont&
         ,  In = I_0
         )  const;
      
      In SinSize() const  ///< Get the number of single excitations
      {
         return (mExciLevelAddress[I_2] - mExciLevelAddress[I_1]);
      }

      In DoubSize() const  ///< Get the number of double excitations
      {
         return (mExciLevelAddress[I_3] - mExciLevelAddress[I_2]);
      }

      void AssignXvec( MidasVector& arVec ) 
      {
         mXvecData.get<DataCont>().DataIo(IO_PUT,arVec,arVec.Size());
      }     ///< Assigning the arVec to Xvec data.

      void GetXvec( MidasVector& arVec ) 
      {
         mXvecData.get<DataCont>().DataIo(IO_GET,arVec,arVec.Size());
      }     ///< Get Xvec data into arVec.

      In Nexci() const { return mXvecData.get<DataCont>().Size(); } ///< The total nr. of coefs.
      In NexciTypes() const {return mExciRange.Size();} ///< The total nr of excitation types.
      In MaxExciLevel() const {return mExciRange.GetMaxExciLevel(); }

      In NexciLevels() const {return mExciLevelAddress.size()-1;}
      ///< The total number of excitation levels.
      
      In AddressForExciLevel(In aLevel) const {return mExciLevelAddress[aLevel];}
      ///< The address for a given excitation level.
      
      //! The nr. of excitations for a certain MC.
      In NexciForModeCombi(const In& arModeCombiNr) const;
      
      void Zero() {mXvecData.get<DataCont>().Zero();}                        ///< Zero the data.
      Nb Norm(){return mXvecData.get<DataCont>().Norm();}
      void SetVscf(){mXvecData.get<DataCont>().SetToUnitVec(I_0);}          ///< Set to unit vector.

      bool Find
         (  const ModeCombi& arModeCombi
         ,  ModeCombiOpRange::const_iterator& arIterator
         ,  In& arModeCombiNr
         )  const;

      In FindModeCombiForInt(const In& aInt) const;
      // For an address, find the MC it belongs to.
      
      In SizeOut() const; ///< Output sizes etc. 

      const ModeCombi& GetModeCombi(const In& arModeCombi) const
         {return mExciRange.GetModeCombi(arModeCombi);}  ///< Get a ref to a certain MC.

      const ModeCombiOpRange& GetModeCombiOpRange() const {return mExciRange;}
      ///< Get a ref to the MCR.

      void UpdateNorms2();
      Nb Norm2ForModeCombi(const In aMc) const {return mNorms2[aMc];}

      In AddressForOccVec(const std::vector<In>& arIvec) const; //< Get address for a particular occ vec
      
      ///> Get data for mode combi
      void GetModeCombiData(MidasVector&, const ModeCombi&, bool = false) const; 
      
      ///> Get data for mode combi
      void SetModeCombiData(const MidasVector&, const ModeCombi&, bool = false); 
      
      ///> Set new label for saving data to disk
      void NewLabel(const std::string& aNewLabel, bool aMoveDataAlong);

      ///> Access reference
      Nb Ref() const;
};

#endif
