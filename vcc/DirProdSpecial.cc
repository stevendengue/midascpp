/**
 ************************************************************************
 * 
 * @file                DirProdSpecial.cc
 *
 * Created:             15-03-2019
 *
 * Author:              Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * Short Description:
 * 
 * Last modified:
 * 
 * Detailed  Description: 
 *
 * MORE    ...
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
 */

#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include <complex>
#include "inc_gen/TypeDefs.h"
#include "DirProdSpecial.h"
#include "DirProdSpecial_Impl.h"
#include "DirProdSpecialMixed_Impl.h"

namespace midas::vcc::dirprod
{

#define INSTANTIATE_DIRPROD_SPECIAL(T)                                  \
template                                                                \
bool DirProdTensorSimpleSpecial                                         \
   (  NiceTensor<T>& aRes                                               \
   ,  const ModeCombi& aResMc                                           \
   ,  const std::vector<NiceTensor<T> >& aInTensor                      \
   ,  const std::vector<ModeCombi>& aInMcs                              \
   ,  const T aCoef                                                     \
   );                                                                   \
                                                                        \
template                                                                \
bool DirProdTensorSimpleSpecialMixed                                    \
   (  NiceTensor<T>& aRes                                               \
   ,  const ModeCombi& aResMc                                           \
   ,  const std::vector<NiceTensor<T> >& aInTensor                      \
   ,  const std::vector<ModeCombi>& aInMcs                              \
   ,  const T aCoef                                                     \
   );                                                                   \
template                                                                \
void DirProdTensorSimpleSpecial2                                        \
   (  NiceTensor<T>& aRes                                               \
   ,  const NiceTensor<T>& aInTensor0                                   \
   ,  const NiceTensor<T>& aInTensor1                                   \
   ,  const T aCoef                                                     \
   );                                                                   \
template                                                                \
void DirProdTensorSimpleSpecial3                                        \
   (  NiceTensor<T>& aRes                                               \
   ,  const NiceTensor<T>& aInTensor0                                   \
   ,  const NiceTensor<T>& aInTensor1                                   \
   ,  const NiceTensor<T>& aInTensor2                                   \
   ,  const T aCoef                                                     \
   );                                                                   \
template                                                                \
void DirProdTensorSimpleSpecial4                                        \
   (  NiceTensor<T>& aRes                                               \
   ,  const NiceTensor<T>& aInTensor0                                   \
   ,  const NiceTensor<T>& aInTensor1                                   \
   ,  const NiceTensor<T>& aInTensor2                                   \
   ,  const NiceTensor<T>& aInTensor3                                   \
   ,  const T aCoef                                                     \
   );                                                                   \
template                                                                \
void DirProdTensorSimpleSpecial5                                        \
   (  NiceTensor<T>& aRes                                               \
   ,  const NiceTensor<T>& aInTensor0                                   \
   ,  const NiceTensor<T>& aInTensor1                                   \
   ,  const NiceTensor<T>& aInTensor2                                   \
   ,  const NiceTensor<T>& aInTensor3                                   \
   ,  const NiceTensor<T>& aInTensor4                                   \
   ,  const T aCoef                                                     \
   );                                                                   \
template                                                                \
void DirProdTensorSimpleSpecial6                                        \
   (  NiceTensor<T>& aRes                                               \
   ,  const NiceTensor<T>& aInTensor0                                   \
   ,  const NiceTensor<T>& aInTensor1                                   \
   ,  const NiceTensor<T>& aInTensor2                                   \
   ,  const NiceTensor<T>& aInTensor3                                   \
   ,  const NiceTensor<T>& aInTensor4                                   \
   ,  const NiceTensor<T>& aInTensor5                                   \
   ,  const T aCoef                                                     \
   );                                                                   \
template                                                                \
void DirProdTensorSimpleSpecialMixed1212                                \
   (  NiceTensor<T>& aRes                                               \
   ,  const NiceTensor<T>& aInTensor0                                   \
   ,  const NiceTensor<T>& aInTensor1                                   \
   ,  const int aSz0                                                    \
   ,  const int aSz1                                                    \
   ,  const int aSz2                                                    \
   ,  const int aSz3                                                    \
   ,  const T aCoef                                                     \
   );

INSTANTIATE_DIRPROD_SPECIAL(float);
INSTANTIATE_DIRPROD_SPECIAL(double);
INSTANTIATE_DIRPROD_SPECIAL(std::complex<float>);
INSTANTIATE_DIRPROD_SPECIAL(std::complex<double>);

#undef INSTANTIATE_DIRPROD

} /* namespace midas::vcc::dirprod */

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
