#ifndef TENSORPRODUCTACCUMULATOR_DECL_H_INCLUDED
#define TENSORPRODUCTACCUMULATOR_DECL_H_INCLUDED

#include <vector>
#include <utility> // for std pair

#include "inc_gen/TypeDefs.h"
#include "tensor/NiceTensor.h"
#include "input/ModeCombi.h"
#include "tensor/TensorDirectProduct.h"
#include "vcc/TensorSumAccumulator.h"

namespace midas::vcc
{

/**
 * Describe me!
 **/
template
   <  typename T
   >
class TensorProductAccumulator
{
   private:
      using connection_t = typename TensorDirectProduct<T>::connection_t;
      using real_t = midas::type_traits::RealTypeT<T>;
      
      bool mDone = false;                    ///< has dirprod already been evaluated?
      T mCoef;                              ///< coeffficient for direct product
      real_t mProdScreen = static_cast<real_t>(0.0); ///< screening for direct products
      real_t mScreen = static_cast<real_t>(0.0);     ///< screening for single tensors
      In mLowRankLimit = I_0;                ///< construct mode matrices (convert to CP) of tensors with lower rank than this
      std::vector<NiceTensor<T> > mTensors; ///< set of tensors
      std::vector<ModeCombi> mMcs;           ///< mode combinations corresponding to set of tensors
      bool mNormalizeTensors  = true;        ///< Normalize TensorDirectProducts before adding to TensorSum
      bool mRecompressTensors = true;        ///< Recompress each direct product individually
      bool mBalanceDirprodToCp = false;      ///< Balance the mode vectors when converting dirprod to CP
      
      //! Do a generalized direct product where indices may be shared between tensors
      bool mGeneralizedDirProd = false;

      ///> Reserve space in vectors
      void Reserve(int);
      
      ///> Make connection map
      connection_t Connection(const ModeCombi& aResMc) const;
      
      ///> Find dimensions of result from input tensors and input modecombinations
      std::vector<unsigned> Dimensions(const ModeCombi&) const;
      
      ///> Specialization for all simple tensors
      void DirProdTensorSimpleImpl
         (  NiceTensor<T>&
         ,  const ModeCombi&
         );
      
      ///> Specialization for all canonical tensors
      void DirProdTensorCanonicalImpl
         (  NiceTensor<T>&
         ,  const ModeCombi&
         );
      
      ///> Specialization for all canonical tensors
      void DirProdTensorCanonicalImpl
         (  TensorSumAccumulator<T>&
         ,  const ModeCombi&
         );

      //! Generalized dirprod for all canonical tensors
      void GeneralizedDirProdTensorCanonicalImpl
         (  TensorSumAccumulator<T>&
         ,  const ModeCombi&
         );
      
      ///> Specialization for mixed simple and canonical tensors
      void DirProdTensorMixedImpl
         (  NiceTensor<T>&
         ,  const ModeCombi&
         );
      
      ///> Implementation of direct product
      void DirProdTensor
         (  NiceTensor<T>&
         ,  const ModeCombi&
         );
      
      ///> Implementation of direct product
      void DirProdTensor
         (  TensorSumAccumulator<T>&
         ,  const ModeCombi&
         );

   public:
      ///> constructor from coefficient, size, and screening factor
      TensorProductAccumulator
         (  T = 1.0
         ,  int = 0
         ,  real_t = 0.0
         ,  real_t = 0.0
         ,  In = 0
         ,  bool = false
         );

      ///> Add a tensor to the direct product
      void AddTensor(NiceTensor<T>&&, ModeCombi&& aMc);

      ///> Do direct product 
      void DirProd
         (  NiceTensor<T>&
         ,  const ModeCombi&
         );
      
      ///> Do direct product 
      void DirProd
         (  TensorSumAccumulator<T>&
         ,  const ModeCombi&
         );

      //! Show dimensions
      void PrintDimensions
         (
         )  const;
};

} /* namespace midas::vcc */

#endif /* TENSORPRODUCTACCUMULATOR_H_INCLUDED */
