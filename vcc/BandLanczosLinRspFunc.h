/**
************************************************************************
* 
* @file                BandLanczosLinRspFunc.h
*
* Created:             09-12-2010
*
* Author:              Ian H. Godtliebsen  (ian@chem.au.dk)
*
* Short Description:   Definitions for Band Lanczos Linear response module.
* 
* Last modified: Thu Dec 09, 2010  04:30PM
* 
* Detailed  Description: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef BANDLANCZOSLINRSPFUNC_H
#define BANDLANCZOSLINRSPFUNC_H

#include <map>
#include <string>
#include <ostream>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "vcc/LanczosChain.h"
#include "vcc/BandLanczosChain.h"
#include "vcc/NHBandLanczosChain.h"
#include "vcc/LanczosRspFunc.h"

class BandLanczosLinRspFunc: public LanczosRspFunc
{
   protected:
      vector<string>       mOpers;
      MidasMatrix          mRightZ;
      MidasMatrix          mLeftZ;
      MidasMatrix          mReFtrans;
      MidasMatrix          mImFtrans;
      vector<Nb>           mFrqRange;
      vector<MidasMatrix>  mReQRsp;
      vector<MidasMatrix>  mImQRsp;
      vector<MidasMatrix>  mReLinRsp;
      vector<MidasMatrix>  mImLinRsp;
      vector<MidasMatrix>  mImLinFRsp;
      vector<MidasMatrix>  mImLinNoFRsp;

      Nb mFrqStart;
      Nb mFrqEnd;
      Nb mFrqStep;
      Nb mGamma;

      In mChainLen;
      In mBlockSize;
      In mLBlockSize;
      In mOrthoChain;
      Nb mDefThresh;
      bool mSaveChain;
      int mSaveInterval;
      bool mTestChain;
      Nb mConvThresh;
      Nb mConvVal;

      MidasVector          mReEigVals;
      MidasVector          mImEigVals;
      MidasMatrix          mReRightEigVecs;
      MidasMatrix          mImRightEigVecs;
      MidasMatrix          mReLeftEigVecs;
      MidasMatrix          mImLeftEigVecs;
      bool                 mSolvedEig;

      bool mIncludeIm;
      bool mIncludeFTerm;
      bool mHasImEigVals;
      bool mImBiOrtho;
      bool mDeflate;

      ///> Functions for initializing parameters.
      void InitOperNames(const std::map<string,string>& aParams);
      void InitFrqRange(const std::map<string,string>& aParams);
      void InitGamma(const std::map<string,string>& aParams);
      void InitChainLen(const std::map<string,string>& aParams);
      void InitBlockSize(const std::map<string,string>& aParams);
      void InitOrtho(const std::map<string,string>& aParams);
      void InitDefThresh(const std::map<string,string>& aParams);
      void InitSolver(const std::map<string,string>& aParams);
      void InitSaveChain(const std::map<string,string>& aParams);
      void InitSaveInterval(const std::map<string,string>& aParams);
      void InitTestChain(const std::map<string,string>& aParams);
      void InitIncludeIm(const std::map<string,string>& aParams);
      void InitIncludeFTerm(const std::map<string,string>& aParams);
      void InitImBiOrtho(const std::map<string,string>& aParams);
      void InitDeflate(const std::map<string,string>& aParams);
      void InitConvThresh(const std::map<string,string>& aParams);
      void InitConvVal(const std::map<string,string>& aParams);

      void ConstructionError(const string aMsg, const map<string,string>& aParams);

      ///> Functions for calcualting linear response for hermitian chain.
      void CalcZ(const BandLanczosChain& aChain);
      void CalcQRsp(const BandLanczosChain& aChain);
      void CalcLinRsp(const BandLanczosChain& aChain);

      ///> Functions for calcualting linear response for non-hermitian chain.
      void CalcRightZ(const NHBandLanczosChain& aChain);
      void CalcLeftZ(const NHBandLanczosChain& aChain);
      void CalcNHLinRsp(const NHBandLanczosChain& aChain);
      void MakeFtrans(const NHBandLanczosChain& aChain);

      ///> Used by DoEigSolutions
      void CheckForImEigVals();
      void CheckEigVecNorm();
      void BiOrthoEigVecs();

   public:
      ///> Constructors
      BandLanczosLinRspFunc(const map<string,string>& aParams, 
                            const bool aBand, const bool aNHerm);

      void Evaluate();
      vector<LanczosChainDefBase*> GetChains(string& aString) const;
      void DoEigSolutions();
      void TransformStoX(const MidasVector& aEigVec, DataCont& aX) const;

      Nb GetFrqStart()  {return mFrqStart;}
      Nb GetFrqEnd()    {return mFrqEnd;}
      Nb GetFrqStep()   {return mFrqStep;}
      Nb GetGamma()     {return mGamma;}

      MidasVector* GetReEigVals();
      MidasVector* GetImEigVals();
      MidasMatrix* GetReRightEigVecs();
      MidasMatrix* GetImRightEigVecs();
      MidasMatrix* GetReLeftEigVecs();
      MidasMatrix* GetImLeftEigVecs();
      
      MidasMatrix*         GetRightZMat() {return &mRightZ;}     
      MidasMatrix*         GetLeftZMat()  {return &mLeftZ;}     
      vector<MidasMatrix>* GetReLinRsp()  {return &mReLinRsp;}
      vector<MidasMatrix>* GetImLinRsp()  {return &mImLinRsp;}
      vector<MidasMatrix>* GetImLinFRsp()    {return &mImLinFRsp;}
      vector<MidasMatrix>* GetImLinNoFRsp()  {return &mImLinNoFRsp;}
      vector<Nb>*          GetFrqRange()  {return &mFrqRange;}
      vector<string>*      GetOpers()     {return &mOpers;}

      bool HasImEigVal() const { return mHasImEigVals; }
      bool IncludeImEigVal() const { return mIncludeIm; }
      
      std::ostream& Print(std::ostream& aOut) const;

      void PrintResponseToStream(std::ostream&) const;

      vector<RspFunc> ConvertToRspFunc() const;
      void SaveCombinedRspFunc() const;
};

#endif
