/**
************************************************************************
* 
* @file                LanczosSpectBlock.h
*
* Created:             10-11-2009
*
* Author:              Peter Seidler (seidler@chem.au.dk) 
*
* Short Description:   Analysis results for a block of a Lanczos
*                      spectrum
* 
* Last modified: Mon Jan 18, 2010  04:22PM
* 
* Detailed  Description: 
*
* MORE    ...
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef LANCZOSSPECTBLOCK_H
#define LANCZOSSPECTBLOCK_H 

#include <map>
#include <string>
#include <iostream>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"

class Vcc;

class LanczosSpectBlock
{
   protected:

   public:
      Nb mMaxInt;                     // Max intensity in the spectra
      Nb mBegin;                      // Frequency where block starts.
      Nb mEnd;                        // Frequency where block ends.
      Nb mIntegrInty;                 // Integrated intensity in block.
      DataCont mWeights;              // Accumulated weights of configurations.
      In mNstates;                    // Number of states in this block.
      Vcc* mVcc;
      
      LanczosSpectBlock(Nb aBegin=C_0, Nb aEnd=C_0);

      friend std::ostream& operator<<(std::ostream& aOut, const LanczosSpectBlock& aBlock);
};

#endif // LANCZOSSPECTBLOC_H
