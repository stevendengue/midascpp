/**
 *******************************************************************************
 * 
 * @file    TwoModeIntermeds.cc
 * @date    31-01-2019
 * @author  Peter Seidler (seidler@chem.au.dk), original
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk), refactorization
 *
 * @brief
 *    Organizing intermediates for specific VCC[2] calculation.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "vcc/TwoModeIntermeds.h"
#include "vcc/TwoModeIntermeds_Impl.h"

// Define instantiation macro.
#define INSTANTIATE_TWOMODEINTERMEDS(T) \
   template class TwoModeIntermeds<T>; \
   template void TMI_DirProd \
      (  const TwoModeIntermeds<T>& \
      ,  const LocalModeNr \
      ,  const LocalModeNr \
      ,  const LocalOperNr \
      ,  const TwoModeIntermeds<T>& \
      ,  const LocalModeNr \
      ,  const LocalModeNr \
      ,  const LocalOperNr \
      ,  typename TwoModeIntermeds<T>::midasvector_t& \
      ); \
   template void TMI_DirProd \
      (  const TwoModeIntermeds<T>& \
      ,  const LocalModeNr \
      ,  const LocalModeNr \
      ,  const LocalOperNr \
      ,  const typename TwoModeIntermeds<T>::midasvector_t& \
      ,  const LocalModeNr \
      ,  typename TwoModeIntermeds<T>::midasvector_t& \
      ); \
   template std::ostream& operator<<(std::ostream&, const TwoModeIntermeds<T>&); \


// Instantiations.
INSTANTIATE_TWOMODEINTERMEDS(Nb);
INSTANTIATE_TWOMODEINTERMEDS(std::complex<Nb>);

#undef INSTANTIATE_TWOMODEINTERMEDS

#endif/*DISABLE_PRECOMPILED_TEMPLATES*/
