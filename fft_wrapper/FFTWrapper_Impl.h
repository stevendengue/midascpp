/*
************************************************************************
*
* @file                 FFTWrapper_Impl.h
*
* Created:              14-11-2015
*
* Author:               Kasper Monrad (monrad@post.au.dk) and Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    FFT wrapping class implementation.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef FFTWRAPPER_IMPL_H_INCLUDED
#define FFTWRAPPER_IMPL_H_INCLUDED

// std headers
#include <complex>
#include <vector>
#include <iostream>

// FFT libs
#ifdef ENABLE_FFTW
#include <fftw3.h>
#endif /* ENABLE_FFTW */

// midas headers
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Error.h"
#include "util/type_traits/Complex.h"
#include "mmv/MidasVector.h"
#include "mmv/VectorToPointer.h"
#include "util/CallStatisticsHandler.h"

namespace fft::detail
{
/**
 * @param aX
 *
 * @return
 *    Smallest power of 2 which is >= aX
 **/
inline In next_power_of_2
   (  In aX
   )
{
   if (  aX < 0
      )
   {
      return 0;
   }
   
   --aX;
   aX |= aX >> 1;
   aX |= aX >> 2;
   aX |= aX >> 4;
   aX |= aX >> 8;
   aX |= aX >> 16;
   
   return aX+1;
}

/**
 * Check if a number is a power of 2
 *
 * @param aX
 *
 * @return
 *    True if aX is a power of 2
 **/
inline bool is_power_of_2
   (  In aX
   )
{
   return (aX != 0) && ((aX & (aX-1)) == 0);
}

} /* namespace fft::detail */


/**
 * @param aIn                 Input vector
 * @param arOut               Output vector
 * @param aType               Do forward or backward Fourier transform
 **/
template
   <  typename T
   ,  template<typename...> typename VEC
   >
void FFTWrapper<T, VEC>::ComputeImpl
   (  const vec_t& aIn
   ,  vec_t& arOut
   ,  transformType aType
   )  const
{
   LOGCALL("fft compute (impl)");

   auto size = aIn.size();
   MidasAssert(arOut.size() == size, "FFTWrapper::ComputeImpl: Input and output vectors must have same size!");

#ifdef ENABLE_FFTW
   auto way = (aType == transformType::FORWARD) ? FFTW_FORWARD : FFTW_BACKWARD;

   fftw_complex* b1=(fftw_complex *)fftw_malloc(sizeof(fftw_complex)*size);
   fftw_complex* b2=(fftw_complex *)fftw_malloc(sizeof(fftw_complex)*size);
   fftw_plan p1=fftw_plan_dft_1d(size, b1, b2, way, FFTW_ESTIMATE);

   for(In i = I_0; i<size; ++i) 
   {
      b1[i][I_0] = aIn[i].real();
      b1[i][I_1] = aIn[i].imag();
      b2[i][I_0] = arOut[i].real();
      b2[i][I_1] = arOut[i].imag();
   }
  
   fftw_execute(p1);

   for (In i=I_0; i<size; ++i) 
   {
      arOut[i] = T(b2[i][I_0], b2[i][I_1]);
   }

   fftw_destroy_plan(p1);
   fftw_free(b1);
   fftw_free(b2);

#else /* no FFTW. Use fallback implementation */
   // Get sign for exponent and size
   In sign = (aType == transformType::FORWARD) ? -I_1 : I_1;

   // Copy data to raw pointer
   auto data = std::make_unique<real_t[]>(2*size);
   DataToPointer(aIn, data.get());

   // Do FFT
   // 1) Reverse binary indexing
   In n = size << 1;
   In j = I_1;
   In m;
   for(In i=I_1; i<n; i+=2)
   {
      if (  j > i
         )
      {
         std::swap(data[j-1], data[i-1]);
         std::swap(data[j], data[i]);
      }
      m = size;
      while (  m >= I_2
            && j > m
            )
      {
         j -= m;
         m >>= I_1;
      }
      j += m;
   }

   // 2) Danielson-Lanczos
   In mmax = 2;
   In istep;
   real_t wtemp, wr, wpr, wpi, wi, theta, tempr, tempi;
   while (  n > mmax
         )
   {
      istep = mmax << 1; 
      theta = sign*(static_cast<real_t>(2*C_PI)/mmax);
      wtemp = std::sin(static_cast<real_t>(0.5)*theta);
      wpr = -static_cast<real_t>(2.0)*wtemp*wtemp;
      wpi = std::sin(theta);
      wr = 1.0;
      wi = 0.0;
      for(m = 1; m<mmax; m+=2)
      {
         for(In i=m; i<=n; i+=istep)
         {
            j = i + mmax;
            tempr = wr*data[j-1] - wi*data[j];
            tempi = wr*data[j] + wi*data[j-1];
            data[j-1] = data[i-1] - tempr;
            data[j] = data[i] - tempi;
            data[i-1] += tempr;
            data[i] += tempi;
         }
         wr = (wtemp=wr)*wpr-wi*wpi+wr;
         wi = wi*wpr+wtemp*wpi+wi;
      }
      mmax = istep;
   }
   
   // Copy data back into output vector
   DataFromPointer(arOut, data.get());

#endif /* ENABLE_FFTW */

   // Swap ranges of output vector such that we go from [-(N-1)/2 .. (N-1)/2] instead of [0..(N-1)]
   this->Shift1D(arOut);
}

/**
 * Swap ranges of vector. This is used when the FFT is performed by FFTW.
 *
 * @param arVec      The output vector from FFTW
 **/
template
   <  typename T
   ,  template<typename...> typename VEC
   >
void FFTWrapper<T, VEC>::Shift1D
   (  vec_t& arVec
   )  const
{
   auto n = arVec.size();

   if constexpr   (  std::is_same_v<vec_t, ComplexMidasVector>
                  )
   {
      auto iter_begin1 = arVec.begin();
      auto iter_end1   = arVec.begin().advance(n/I_2);
      auto iter_begin2 = arVec.begin().advance(n/I_2);
      std::swap_ranges(iter_begin1, iter_end1, iter_begin2);
   }
   else
   {
      auto iter_begin1 = arVec.begin();
      auto iter_end1   = iter_begin1;
      std::advance(iter_end1, n/I_2);
      auto iter_begin2 = iter_end1;
      std::swap_ranges(iter_begin1, iter_end1, iter_begin2);
   }
}


/**
 * Do zero padding.
 *
 * @param aVec       Input signal vector
 * @param aFac       Multiply signal size by this factor after going to next power of 2
 *
 * @return
 *    New, padded vector
 **/
template
   <  typename T
   ,  template<typename...> typename VEC
   >
typename FFTWrapper<T, VEC>::vec_t FFTWrapper<T, VEC>::ZeroPad
   (  const vec_t& aVec
   ,  In aFac
   )  const
{
   In vec_size = aVec.size();
   if (  vec_size == I_0
      || aFac == I_0
      )
   {
      MIDASERROR("Empty vector, cannot zeropad!");
   }
   In pow2 = fft::detail::next_power_of_2(vec_size);
   In zero_size = pow2*aFac;
   vec_t vec_zero(zero_size, T(0., 0.));

   // Copy elements of arVec to first elements of result
   std::copy(aVec.begin(), aVec.end(), vec_zero.begin());

   return vec_zero;
}

/**
 * @param aSignal       The vector containing equidistant samples of the signal
 * @param aTEnd         Length of the time interval
 * @param aType         Type of transformation
 * @param aPaddingLevel Level of zero padding. If < 0, we do no zero padding if the number of points is a power of 2.
 *
 * @return
 *    Pair of vectors containing frequencies and intensities. The result is ordered with negative frequencies first.
 **/
template
   <  typename T
   ,  template<typename...> typename VEC
   >
typename FFTWrapper<T, VEC>::result_t FFTWrapper<T, VEC>::Compute
   (  const vec_t& aSignal
   ,  real_t aTEnd
   ,  transformType aType
   ,  In aPaddingLevel
   )  const
{
   // Get number of samples
   In n = aSignal.size();

   // Init result
   result_t result;

   // Set the size of the padding factor (must be a power of two)
   bool do_padding = aPaddingLevel > I_0;
   auto pad_level =  do_padding
                  ?  fft::detail::is_power_of_2(aPaddingLevel) ? aPaddingLevel : fft::detail::next_power_of_2(aPaddingLevel)
                  :  I_1;  // If we have to pad (even though aPaddingLevel < I_0), just go to next power of two.

   // Aux. signal vector used for zero padding
   vec_t aux_signal;
   
   // Check if we have 2^k data points. If not (or if aPaddingLevel > I_0), ZeroPad.
   bool not_power_of_2 = !fft::detail::is_power_of_2(n);
   bool zeropad = not_power_of_2 || do_padding;
   if (  zeropad
      )
   {
      if (  not_power_of_2
         && gIoLevel >= I_5
         )
      {
#ifdef ENABLE_FFTW
         Mout  << " FFTWrapper: Data is not a power of 2. We do zero padding (even though FFTW supports the general case)!" << std::endl;
#else
         Mout  << " FFTWrapper: Data is not a power of 2. Do zero padding!" << std::endl;
#endif /* ENABLE_FFTW */
      }
      aux_signal = std::move(this->ZeroPad(aSignal, pad_level));
   }

   // Get ref to signal that we do FFT on
   const auto& signal = zeropad ? aux_signal : aSignal;

   // Size after padding
   auto size_out = signal.size();

   // Init result
   result.second = vec_t(size_out);

   // Call implementation
   this->ComputeImpl(signal, result.second, aType);

   // Do normalization for forward transforms
   if (  aType == transformType::FORWARD
      )
   {
      for(In i=I_0; i<size_out; ++i)
      {
         result.second[i] /= real_t(n);
      }
   }

   // Calculate frequencies
   auto& freqs = result.first;
   freqs = realvec_t(size_out);

   real_t dt = aTEnd/n;
   real_t tm = size_out*dt;
   real_t df = static_cast<real_t>(1.)/tm;
   
   real_t q = std::ceil(static_cast<real_t>(size_out + 1)/C_2);
   real_t fq = df*(q-C_1);
   for(In i=0; i<size_out; ++i)
   {
      freqs[i] = df*i - fq;
   }

   // Return result
   return result;
}

#endif /* FFTWRAPPER_IMPL_H_ INCLUDED */
