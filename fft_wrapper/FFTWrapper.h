/*
************************************************************************
*
* @file                 FFTWrapper.h
*
* Created:              14-11-2015
*
* Author:               Kasper Monrad (monrad@post.au.dk) and Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    FFT wrapping class.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef FFTWRAPPER_H_INCLUDED
#define FFTWRAPPER_H_INCLUDED

// std headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/type_traits/Complex.h"

/**
 *
 **/
template
   <  typename T = std::complex<Nb>
   ,  template<typename...> typename VEC = std::vector
   >
class FFTWrapper
{
   //! We need complex numbers
   static_assert(midas::type_traits::IsComplexV<T>, "FFTWrapper only works for complex numbers!");

   public:
      //! Alias
      using vec_t = VEC<T>;
      using real_t = midas::type_traits::RealTypeT<T>;
      using realvec_t = VEC<real_t>;
      using result_t = std::pair<realvec_t, vec_t>;

      //! Transform type
      enum class transformType : int
      {  FORWARD = 0
      ,  BACKWARD
      };

   private:
      //! Compute FFT (implementation)
      void ComputeImpl
         (  const vec_t&
         ,  vec_t&
         ,  transformType
         )  const;

      //! Shift the frequency range from 0..(N-1) to -(N-1)/2 .. +(N-1)/2 to put the zero-frequency component in the middle
      void Shift1D
         (  vec_t&
         )  const;

      //! Perform zero padding
      vec_t ZeroPad
         (  const vec_t&
         ,  In = -I_1
         )  const;

   public:
      //! Default c-tor
      FFTWrapper() = default;

      //! Compute FFT (interface). Return pair<freqs, intensities>
      result_t Compute
         (  const vec_t& aSignal
         ,  real_t aTEnd
         ,  transformType aType = transformType::FORWARD
         ,  In aPaddingLevel = -I_1
         )  const;
};

#include "FFTWrapper_Impl.h"

#endif /* FFTWRAPPER_H_INCLUDED */
