/**
***********************************************************************
*
* @file:               inc_gen/TypeDefs.h          
* 
* Created:             25-07-2001          
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing Nb/In typedefs for reals/integer
*
* Last modified: Thu Oct 16, 2008  11:51AM
*
* Detailed  Description: 
* 
* Nb is float (either float, double, or long double),
* In is integer (either short, int or long), 
* Uin is unsigned int (either short,int,or long).
* The intent is to here separate out the definition of sizes of
* representation on different machines.
* And use rather short notation of equal length for integer and real numbers.
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
* 
***********************************************************************
*/

#ifndef TYPEDEF_H_INCLUDED
#define TYPEDEF_H_INCLUDED

#include <utility>
#include <vector>

/**
* Define precision of midas standard float type by definition of logical MACRO
* */
//#define L_DOUBLE      ///< standard float type Nb is double precision
//#define L_FLOAT       ///< standard float type Nb is float precision
//#define L_LONGDOUBLE  ///< standard float type Nb is long-double precision
//#define L_VERYLONGDOUBLE  ///< float type Nb is nonstandard long long-double precision
/**
* Define precision of midas standard integer type by definition of logical MACRO
* */
//#define L_INT         ///< standard integer type In is int
//#define L_SHORTINT    ///< standard integer type In is short int
//#define L_LONGINT     ///< standard integer type In is long int 
//#define L_LONGLONGINT     ///< standard integer type In is long int 

/**
* Nb is Midas standard type of float that can be switched between float,double,long double
* in the TypeDefs.h file by definition of appropriate macro logicals.
* */
#ifdef L_DOUBLE
typedef double       Nb; ///< Nb is synonym for double
#elif defined L_FLOAT 
typedef float        Nb; ///< Nb is synonym for float
#elif defined L_LONGDOUBLE
typedef long double  Nb; ///< Nb is synonym for long double
#else
// Default
#define L_DOUBLE
typedef double Nb;
#endif

/**
* In is Midas standard type of integer that can be switched between short int, int, long int
* in the TypeDefs.h file by definition of appropriate macro logicals.
* */
#ifdef L_INT
typedef int          In; ///< In is synonym for normal int
#elif defined L_SHORTINT
typedef short int    In; ///< In is synonym for short int
#elif defined L_LONGINT
typedef long         In; ///< In is synonym for long int
#elif defined L_LONGLONGINT
typedef long long    In; ///< In is synonym for long long int
#else
// Default
#define L_INT
typedef int In;
#endif


/**
* Uin is Midas standard type of unsigned integer that can be switched between unsigned short int, 
* unsigned int, unsigned long int
* in the TypeDefs.h file by definition of appropriate macro logicals.
* */
#ifdef L_SHORTINT
typedef unsigned short int    Uin; ///< Uin is synonym for unsigned short int
#endif
#ifdef L_INT
typedef unsigned int          Uin; ///< Uin is synonym for unsigned normal int
#endif
#ifdef L_LONGINT
typedef unsigned long         Uin; ///< Uin is synonym for unsigned long  int
#endif
#ifdef L_LONGLONGINT
typedef unsigned long long    Uin; ///< Uin is synonym for unsigned long long int
#endif


/**
* Lb  is Midas standard type of char 
* */
typedef char    Lb;    /// Seems silly for the time being - maybe extend later.


/**
* Define here typenames for function pointers with Nb in and out
* */
typedef Nb (*PointerToFunc1)(Nb);           ///< PointerToFunc is a function pointer for func of 1 var.
typedef Nb (*PointerToFunc2)(Nb,Nb);        ///< PointerToFunc is a function pointer for func of 2 var.
typedef Nb (*PointerToFunc3)(Nb,Nb,Nb);     ///< PointerToFunc is a function pointer for func of 3 var.

typedef std::pair<In,In> InPair;
typedef std::pair<Nb,Nb> NbPair;
typedef std::pair<In,Nb> InNbPair;

typedef std::vector<In> InVector;
typedef std::vector<Nb> NbVector;

enum{GLOBAL, LOCAL, MODENR, OPERNR, SUBSYSNR, ATOMNR,FUSIONGROUPNR};

template<int envtype,int type>
class SafeNr
{
   private:
      int mNr;
   public:
      SafeNr(int aNr = -1) : mNr(aNr) {}
      SafeNr<envtype,type>& operator= (const int aRhsNr) {mNr = aRhsNr; return *this;}

      operator int() const {return mNr;}

      template<int M, int N> SafeNr<envtype,type>& operator=(const SafeNr<M,N>& aRhs)
      {
         static_assert(std::is_same<SafeNr<envtype,type>,SafeNr<M,N> >::value, "Wrong use of operator ="); 
         if(this==&aRhs) 
            return *this;
         mNr = aRhs.mNr; 
         return *this;
      }
     

      //bool operator== (const SafeNr<envtype,type>& aRhs) {return (this->mNr == aRhs.mNr);}
      template<int M, int N> bool operator==(const SafeNr<M,N>& aRhs) const
      {
         static_assert(std::is_same<SafeNr<envtype,type>,SafeNr<M,N> >::value, "Wrong use of operator ==");
         return mNr == aRhs.mNr;
      }

      template<int M, int N> bool operator!=(const SafeNr<M,N>& aRhs) const
      {
         static_assert(std::is_same<SafeNr<envtype,type>,SafeNr<M,N> >::value, "Wrong use of operator !=");
          return mNr != aRhs.mNr;
      }

      template<int M, int N> bool operator<(const SafeNr<M,N>& aRhs) const
      {
         static_assert(std::is_same<SafeNr<envtype,type>,SafeNr<M,N> >::value, "Wrong use of operator <");
          return mNr < aRhs.mNr;
      }

      template<int M, int N> bool operator>(const SafeNr<M,N>& aRhs) const
      {
         static_assert(std::is_same<SafeNr<envtype,type>,SafeNr<M,N> >::value, "Wrong use of operator >");
          return mNr > aRhs.mNr;
      }

      template<int M, int N> bool operator>=(const SafeNr<M,N>& aRhs) const
      {
         static_assert(std::is_same<SafeNr<envtype,type>,SafeNr<M,N> >::value, "Wrong use of operator >=");
          return mNr >= aRhs.mNr;
      }

      template<int M, int N> bool operator<=(const SafeNr<M,N>& aRhs) const
      {
         static_assert(std::is_same<SafeNr<envtype,type>,SafeNr<M,N> >::value, "Wrong use of operator <=");
          return mNr <= aRhs.mNr;
      }

      SafeNr<envtype,type>& operator++() {mNr++; return *this;}
      SafeNr<envtype,type> operator++(int) 
      {
         SafeNr<envtype,type> ret = *this;
         mNr++; 
         return ret;
      }

};

namespace std
{
   //! Hash for SafeNr, using the standard impl. of std::hash<int>.
   template<int envtype,int type>
   struct hash<SafeNr<envtype,type>>
   {
      std::size_t operator()(const SafeNr<envtype,type>& a) const noexcept
      {
         return std::hash<int>()(int(a));
      }
   };
}

typedef SafeNr<GLOBAL,MODENR> GlobalModeNr;
typedef SafeNr<GLOBAL,OPERNR> GlobalOperNr;
typedef SafeNr<LOCAL,MODENR> LocalModeNr;
typedef SafeNr<LOCAL,OPERNR> LocalOperNr;
typedef SafeNr<GLOBAL,SUBSYSNR> GlobalSubSysNr;
typedef SafeNr<GLOBAL,ATOMNR> GlobalAtomNr;
typedef SafeNr<GLOBAL,FUSIONGROUPNR> GlobalFGNr;
typedef SafeNr<LOCAL,ATOMNR> LocalAtomNr;

#endif /* TYPEDEFS_H_INCLUDED */
