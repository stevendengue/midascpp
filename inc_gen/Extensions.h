#ifndef MIDAS_INC_GEN_EXTENSIONS_H_INCLUDED
#define MIDAS_INC_GEN_EXTENSIONS_H_INCLUDED

#define input_ext    ".minp"
#define output_ext   ".mout"
#define molecule_ext ".mmol"
#define operator_ext ".mop"

#endif /* MIDAS_INC_GEN_EXTENSIONS_H_INCLUDED */
