/**
************************************************************************
* 
* @file                Warnings.cc
*
* Created:             20-12-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Store warnings
* 
* Last modified: man mar 21, 2005  11:29
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#include "Warnings.h"

#include <string>
#include <vector>
#include <set>
#include <mutex>

//class Warning
//{
//   private:
//      
//   public:
//      std::ostream& Print(std::ostream& os) const
//      {
//         return os;
//      }
//};
//
//std::ostream& operator<<(std::ostream& os, const Warning& aWarning)
//{
//   return aWarning.Print(os);  
//}


/**
* Warnings caught during input (and thereafter actually also).
* */
std::set<std::string> gWarnings;        ///< The warnings
std::mutex warning_mutex;

void PrintWarningDirectly(const std::string& aWarning)
{
   bool mute = Mout.Muted();
   Mout.Unmute();
   Mout << "[WARNING:] " << aWarning << std::endl;
   if(mute) Mout.Mute();
}

void MidasWarning(const std::string& aWarning, bool aForce)
{
   std::string warning_copy = aWarning;
   MidasWarning(std::move(warning_copy), aForce);
}

void MidasWarning(std::string&& aWarning, bool aForce)
{
   std::lock_guard<std::mutex> lock(warning_mutex);

   if(aForce || (gWarnings.find(aWarning) == gWarnings.end()))
   {
      PrintWarningDirectly(aWarning);
      gWarnings.insert(std::move(aWarning));
   }
}

void MidasWarningIf(bool aPrint, const std::string& aWarning)
{
   if(aPrint)
   {
      MidasWarning(aWarning);
   }
}

void PrintMidasWarnings(std::ostream& aOs)
{
   if (gWarnings.size()>0) 
   {
      aOs << "[WARNING:] MIDAS MAIN WARNINGS: " << std::endl;
      //for (In i_war=0; i_war<gWarnings.size(); ++i_war) 
      for (auto it=gWarnings.begin(); it!=gWarnings.end(); ++it)
      {
         aOs << "[WARNING:] " << (*it) << std::endl;
      }
   }
}
