/**
***********************************************************************
*
* \file:               Version.h          
* 
* Created:             27-02-2002          
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Give current version number/date of stability
*
* Last modified: Mon Nov 06, 2006  12:20PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
* 
***********************************************************************
*/

#ifndef VERS_H_INCLUDED
#define VERS_H_INCLUDED

#include <string>

#include "inc_gen/TypeDefs.h"

/**
* Midas version 
*/
const In MIDAS_VERSION       = 2020;
const In MIDAS_SUBVERSION    = 4;
const In MIDAS_SUBSUBVERSION = 0;

inline std::string MidasVersion()
{
   std::string subversion = std::to_string(MIDAS_SUBVERSION);
   subversion             = std::string(2 - subversion.length(), '0') + subversion;

   return   
      {  std::to_string(MIDAS_VERSION)     + "."
      +  subversion                        + "."
      +  std::to_string(MIDAS_SUBSUBVERSION)
      };
}

#endif /* VERS_H_INCLUDED */
