#ifndef MIDASCPP_INC_GEN_COMPILER_H_INCLUDED
#define MIDASCPP_INC_GEN_COMPILER_H_INCLUDED

/* 
 * Some preprocessor magic
 */
#define XMACROSTRING(s) MACROSTRING(s)
#define MACROSTRING(s) #s

#if defined __INTEL_COMPILER
   #define COMPILER intel-__INTEL_COMPILER
#elif defined __clang__
   #define COMPILER clang-__clang_major__.__clang_minor__.__clang_patchlevel__
#elif defined __GNUC__
   #define COMPILER gcc-__GNUC__.__GNUC_MINOR__.__GNUC_PATCHLEVEL__
#elif defined __PGI
   #define COMPILER pgi-__PGIC__.__PGIC_MINOR__.__PGIC_PATCHLEVEL__
#endif

#ifdef COMPILER
   #define COMPILER_STRING XMACROSTRING(COMPILER)
#else 
   #define COMPILER_STRING "unknown"
#endif

#endif /* MIDASCPP_INC_GEN_COMPILER_H_INCLUDED */
