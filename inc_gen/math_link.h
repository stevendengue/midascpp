/**
************************************************************************
* 
* @file                math_link.h
*
* Created:             31-10-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Link to used part of cmath/math.h (due to diff. comp)
* 
* Last modified: man mar 21, 2005  11:26
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MATHLINK_H
#define MATHLINK_H

#ifdef USE_MATH_H
/**
* Section for systems not supporting the cmath system but which has
* math.h
* */
#include<math.h>
#include<algorithm>      // not needed for all comp. - fixes some min/max problem 
using std::min;
using std::max;

#else /* !USE_MATH_H */
/**
* Section for systems supporting the cmath system.
* Not that some problems with min/max makes us inline algorithm.
* */
#include<cmath>
using std::fabs; 
using std::cos; 
using std::sin; 
using std::acos; 
using std::tan; 
using std::exp; 
using std::pow; 
#include<algorithm>      // not needed for all comp. - fixes some min/max problem 
using std::max;
using std::min;

#endif /* USE_MATH_H */

#endif /* MATHLINK_H */
