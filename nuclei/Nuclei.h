/**
************************************************************************
* 
* @file                Nuclei.h
*
* Created:             04-07-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Class for treating atomic nuclei
* 
* Last modified: March, 26th 2015 (carolin)
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef NUCLEI_H_INCLUDED
#define NUCLEI_H_INCLUDED

#include<iostream>
using std::ostream;
using std::istream;
#include<string>
using std::string;
#include<vector>
using std::vector;

#include"inc_gen/TypeDefs.h"
#include"inc_gen/Const.h"
#include"nuclei/Vector3D.h"
#include"mmv/MidasMatrix.h"

enum class NucTreatType: int  {ERROR,ACTIVE,INACTIVE,FROZEN,CAP};
ostream& operator<<(ostream& os,const NucTreatType& arType);
NucTreatType NucTreatTypeFromString(const std::string& arString);

/**
 * Class Nuclei: 3 Dimensional points with private coordinates and 
 * x(),y(),z(), distance, move, and angle interfaces.
 **/
class Nuclei
{
   public:
      Nuclei();                                  ///< Create uninitialized.
      Nuclei
         (  Nb aX
         ,  Nb aY
         ,  Nb aZ
         ,  Nb aQ = C_0
         ,  const std::string& aAtomLabel = "NONAME"
         ,  const std::string& aBasisLabel = "UNIN"
         ,  In aSubSysMem = -I_1
         ,  In aSubSys = -I_1
         ,  const NucTreatType& arNucTreatType = NucTreatType::ACTIVE
         );                                      ///< Create from coordinates.
      void MoveTo(Nb aX, Nb aY, Nb aZ);          ///< Move to new position
      void Shift(const Vector3D& arP3d);         ///< Shift by a const vector
      void Shift(const MidasVector& arP3d);         ///< Shift by a const vector
      Nb Angle(const Nuclei& arNuc1,const Nuclei& arNuc3) const; 
                                                 ///< Angle between aNuc1,*this, and aNuc3 (Degrees)
      Nb RadAngle(const Nuclei& arNuc1,const Nuclei& arNuc3) const;
                                                 ///< Angle between aNuc1,*this, and aNuc3 (Radians)
      Nb Dihedral(const Nuclei&, const Nuclei&, const Nuclei&); // < Dihedral angle between 
      Nb Distance(const Nuclei& arNuc) const;   ///< Distance to aNuc
      Nb Distance(const Vector3D& arP3d) const; ///< Distance to a point given a 3D vector 
      Nb DistanceToOrigo() const;               ///< Distance to origo
      Nb Repuls(const Nuclei& arNuc) const;        ///< Repulsion relative to aNuc
      In GetZFromLabel();                        ///< Convert input label to a Z number
      std::string GetCleanAtomLabel() const;
      void SetQzFromLabel();                     ///< Convert input label to both q and Z number
      void SetQfromGeneralLabel(string);         ///< Set charge fom label Atom#Number

      Nb X() const {return mX;}
      Nb Y() const {return mY;}
      Nb Z() const {return mZ;}
      Nb Q() const {return mQ;}
      In Znuc() const {return mZnuc;}
      In Anuc() const {return mAnuc;}
      In SubSys()       const {return mSubSys;}
      In SubSysMem()    const {return mSubSysMem;}
      In Index()        const {return mI;}
      
      std::string AtomLabel()  const {return mAtomLabel;} 
      std::string BasisLabel() const {return mBasisLabel;} 
      
      void ForceNonStandard() {mForceNonStandard = true;} 
      void ForceStandard() {mForceNonStandard = false;} 
      void SetDigitModest(In aI) {mDigitModest = aI;} 
      void SetShiftLenNeg(Nb aNb) {mShiftLenNeg = aNb;} 
      
      Nb ShiftLenNeg() const {return mShiftLenNeg;}
      In DigitModest() const {return mDigitModest;}
         
      //!@{
      //! Manipulate / query Nuclear treat type.
      void SetNucTreatType(const NucTreatType& arType) {mNucTreatType=arType;}
      NucTreatType GetNucTreatType() const {return mNucTreatType;}
      bool IsActive() const {return (mNucTreatType==NucTreatType::ACTIVE);}
      bool IsFrozen() const {return (mNucTreatType==NucTreatType::FROZEN);}
      bool IsCapping() const {return (mNucTreatType==NucTreatType::CAP);}
      //!@}
      
      void AssignBasisLabel(const string& arLbl) {mBasisLabel = arLbl;} 
      void PutNucToVector3D(Vector3D& aV) {aV.SetX(mX); aV.SetY(mY); aV.SetZ(mZ);}

      //!@{
      //! Setters
      void SetX(Nb arX) {mX = arX;}
      void SetY(Nb arY) {mY = arY;}
      void SetZ(Nb arZ) {mZ = arZ;}
      void SetQ(Nb arQ) {mQ = arQ;} ///< Set the charge to given charge 
      void SetZnuc(In arQ) {mZnuc = arQ;} ///< Set the nuclear charge 
      void SetAtomLabel(string& arS)  {mAtomLabel = arS;} 
      void SetAnuc(In arA) { if(arA) mAnuc = arA; else SetToMostCommonIsotope(); }
      void SetQtoZnuc() {mQ = mZnuc;}         ///< Set the charge to the Nuclear charge 
      void SetXyz(Vector3D& aV) {mX = aV.X(); mY = aV.Y(); mZ = aV.Z();}
      void SetSubSys(const In arI)       {mSubSys = arI;}
      void SetSubSysMem(const In arI)    {mSubSysMem = arI;}
      //!@}
      
      //!@{
      //! Getters
      Nb GetMass() const;
      Nb GetIsotopicWeight() const;
      //!@}

      //!
      void SetToMostCommonIsotope();
      
      //! Convert nuclei to xyz format and return as string.
      std::string OutXyzFormat(bool arConverToAangstrom=true);
      
      //! Scale coord (?)
      void ScaleCoord(const Nb& arScaleFac);     
      
      //! Rotate nuclei around origo
      void Rotate(const MidasMatrix&);     
      
      //! Are the two nuclei the same and are their coordinates "almost" equal
      bool IsNumEqual(const Nuclei& aOther, In aUlps = 2) const;
      
      //! Operator equal.
      bool operator==( const  Nuclei& arN2) const;

      friend ostream& operator<<(ostream& os,const Nuclei&); ///< output operator
      friend istream& operator>>(istream& is,Nuclei&);       ///< input  operator
      friend bool NucleiLess(const Nuclei& arN1, const  Nuclei& arN2);
      friend bool NucleiLess2(const Nuclei& arN1, const  Nuclei& arN2);

   private:
      //! Actual x coordinate
      Nb mX;
      //! Actual y coordinate
      Nb mY;                       
      //! Actual z coordinate
      Nb mZ;                       
      //! Actual charge
      Nb mQ = -1.0;                
      //! A/Z value (nuclear charge)
      In mZnuc = -1;               
      //! A value (nuclear isotope)
      In mAnuc;                    
      //! Index for atoms with same label.
      In mI;                       
      //! Subsystem nr
      In mSubSys = -1;             
      //! Member nr. for given subsystem 
      In mSubSysMem = -1;          
      //! Pointer to Actual label
      std::string mAtomLabel;      
      //! Pointer to Actual label
      std::string mBasisLabel;     
      //! If non standard io desired
      bool mForceNonStandard;      
      //! Use Modest number of digits
      In mDigitModest;             
      //! Use negl. value for shift
      Nb mShiftLenNeg;             
      //! Defines how this atom is to be treated 
      NucTreatType mNucTreatType;  
};

void ShiftNuclei(vector<Nuclei>& arNucVec, Vector3D& arShiftVec);
bool NucleiLess(const Nuclei& arN1, const  Nuclei& arN2);
            ///< Declare once more since a friend decl. is only a friend.
void FindCenterOfNuclearCharge(vector<Nuclei>& arNucVec, 
             Vector3D& arConcVec,bool aTest=true,In aIhit=I_0);
void FindCenterOfMass(vector<Nuclei>& arNucVec, Vector3D& arConcVec); ///< Fine center of mass
void FindHeavyAtom(vector<Nuclei>& arNucVec,Vector3D& ar3, In aSubSys=I_0);
Nb RepulsEnergy(vector<Nuclei>& arNucVec); 
string GetLabelFromZ(In aZnuc);
//


#endif /* NUCLEI_H_INCLUDED */
