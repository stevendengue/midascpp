MidasCpp
===========================================================

MidasCpp (**M**olecular **I**nteractions **D**ynamics **A**nd **S**imulation **C**hemistry **P**rogram **P**ackage) 
is a program package initiated by Ove Christiansen at Aarhus university with the emphasis 
of using coupled cluster theory for the description of the dynamics of the atomic nuclei. 

Quick Guide
===========

To get hold of the source code either navigate to `http://gitlab.com/midascpp/midascpp/releases`
and download a release tarball, or use a command line tool like `wget` to download a specific release:
```
wget https://gitlab.com/midascpp/midascpp/-/archive/2020.04.0/midascpp-2020.04.0.tar.gz
```
It is also possible to clone the git repository containing the MidasCpp source code:
```
 git clone  git@gitlab.com:midascpp/midascpp.git
```
For compilation you need an up-to-date C++ compiler which complies to the C++17 standard
- we recommend using `gcc` version 7.2.0 or above.
Further requirements on libraries or tested compilers are given in the manual.

To configure the compilation run
```
 ./configure --prefix=/path/to/install
```
where `/path/to/install` is the path you want MidasCpp binaries
and other files to be installed to after compilation.
Running the ``configure`` script this way sets up Makefiles etc.
for a standard build without parallelization or other features.
See the full manual for further hints on configuring or tweaking installations.
The source code can now be compiled by typing
```
  make complete
```
Using the option `complete` the source code is compiled and linked,
and afterwards MidasCpp binaries etc. are installed in the `prefix` specified when configured.
To be able to access MidasCpp from the command line append the `bin` directory of the MidasCpp installation to your `PATH`.
This can by automated by adding the following two lines to your `.bashrc`:
``
 export MIDASINSTALL=/path/to/install
 export PATH=$MIDASINSTALL/bin:$PATH
``

To verify the correctness of your MidasCpp installation it is strongly recommended to run some examples from the `test_suite`.
To do this go to `test_suite` and run `./TEST installshort`.
This will test the most important parts of MidasCpp in a few seconds.

For a more elaborate test run`./TEST installlong`, but do note this may take hours.
For a more complete overview of additional test we refer to the info in the `test_suite` itself.

Examples
=========

For more details on how to use MidasCpp we recommend taking a look at the example suite:

- [MidasCpp example suite](https://gitlab.com/midascpp/midascpp_examples)

Changes
=========

For changes in the latest version see [CHANGELOG](CHANGELOG.md).
