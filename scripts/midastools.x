#!/bin/bash

# Get name of tool to use
tool=$1
# Remove first argument to be able to pass remaining args as "$@"
shift
args="$@"

###########################################################
# Carry out commands
###########################################################

# datacont_diff_norm
if [ "$tool" == "datacont_diff_norm" ]; then
   exec $MIDAS_INSTALL_PREFIX/libexec/datacont_diff_norm.x $args
# datacont_diff_norm
elif [ "$tool" == "datacont_to_ascii" ]; then
   exec $MIDAS_INSTALL_PREFIX/libexec/datacont_to_ascii.x $args
# tensordatacont_diff_norm
elif [ "$tool" == "tensordatacont_diff_norm" ]; then
   exec $MIDAS_INSTALL_PREFIX/libexec/tensordatacont_diff_norm.x $args
# tensordatacont_to_datacont
elif [ "$tool" == "tensordatacont_to_datacont" ]; then
   exec $MIDAS_INSTALL_PREFIX/libexec/tensordatacont_to_datacont.x $args
# comparevibs
elif [ "$tool" == "comparevibs" ]; then
   exec $MIDAS_INSTALL_PREFIX/libexec/comparevibs.x $args
# autocorr_spectrum
elif [ "$tool" == "autocorr_spectrum"]; then
   exec $MIDAS_INSTALL_PREFIX/libexec/autocorr_spectrum.x $args
# mctdh_wf_analysis
elif [ "$tool" == "mctdh_wf_analysis"]; then
   exec $MIDAS_INSTALL_PREFIX/libexec/mctdh_wf_analysis.x $args
else
   echo "'" $tool "' is not recognized as a MidasCpp tool!" 
fi


# vim:syntax=sh
