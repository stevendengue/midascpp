#!/bin/bash

cwd=$(pwd)
cmd=

check_directory () {
   if [ ! -d "$1" ]; then
      mkdir $1
   fi
}

###########################################################
# Input processing
###########################################################
while test $# -gt 0
do
   case $1 in
      -*)
         echo "Unknown option '$1'." 
         ;;
      *)
         [[ -z $cmd ]] && cmd=$1 || cmd=$cmd:$1
         ;;
   esac
   shift
done

###########################################################
# Carry out commands
###########################################################

# VIm
if [ "$cmd" == "vim:install" ]; then
   echo "Installing MidasCpp syntax highligting script for VIm."
   if [ -d ~/.vim ] || [ -L ~/.vim ]; then
      # Create directories
      check_directory ~/.vim/syntax
      check_directory ~/.vim/ftdetect

      # Install scripts
      cp $MIDAS_INSTALL_PREFIX/vim/syntax/* ~/.vim/syntax
      cp $MIDAS_INSTALL_PREFIX/vim/ftdetect/* ~/.vim/ftdetect

      echo "MidasCpp VIm scripts installed successfully!"
   else
      echo "No '~/.vim' directory."
   fi
# Autocomp
elif [ "$cmd" == "autocomp:install" ]; then
   echo "Installing autocompletion"
   if [ ! -d ~/.bash_completion.d ]; then
      mkdir ~/.bash_completion.d
   fi
   if [ -f ~/.bash_completion ]; then
      mv ~/.bash_completion ~/.bash_completion.d/bash_completion
   fi
   cp $MIDAS_INSTALL_PREFIX/autocomp/midascpp_comp.bash ~/.bash_completion.d/midascpp_comp
   cp $MIDAS_INSTALL_PREFIX/autocomp/bash_completion ~/.bash_completion

   echo "Add: \"[ -f ~/.bash_completion ] && . ~/.bash_completion\" to '.bashrc'"

# Create 
elif [ "$cmd" == "create:source" ]; then
   echo "Creating 'source' file."
   source_file=$cwd/midascpp.source_me

   cp $MIDAS_INSTALL_PREFIX/template/midascpp.source_me.template $source_file
   
   midas_install_prefix_bckslsh=`echo $MIDAS_INSTALL_PREFIX | sed 's@/@\\\/@g'`
   
   #
   sed -i "s/\%install_prefix\%/$midas_install_prefix_bckslsh/g" $source_file
   # PATH
   sed -i "s/\%path\%/$midas_install_prefix_bckslsh\/bin/g" $source_file
   # INCLUDE
   sed -i "s/\%c_include_path\%/$midas_install_prefix_bckslsh\/include/g" $source_file
   sed -i "s/\%cplus_include_path\%/$midas_install_prefix_bckslsh\/include/g" $source_file
   sed -i "s/\%include\%/$midas_install_prefix_bckslsh\/include/g" $source_file

else
   echo "No command given."
fi


# vim:syntax=sh
