/**
************************************************************************
*
*  @file                MidasPotential.h
* 
*  Created:             01-01-2013
* 
*  Author:              Bo Thomsen (bothomsen@chem.au.dk)
* 
*  Short Description:   Potential class for representing a midas potential
* 
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
**/

#ifndef MIDASPOT_H_INCLUDED
#define MIDASPOT_H_INCLUDED

// std headers
#include <map>
#include <vector>
#include <string>
#include <fstream>

// midas headers
#include "potentials/GenericBasePot.h"
#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"

class MidasPotential 
   : public GenericBasePot<MidasFunctionWrapper, Nb> 
{
   private:

      //We don't want these functions called
      MidasPotential& operator=(const MidasPotential&) = delete;
      MidasPotential(const MidasPotential&) = delete;

   public:
      /**
       *
       **/
      MidasPotential
         (
         ) 
         : GenericBasePot<MidasFunctionWrapper, Nb >() 
      {
      }

      /**
       *
       **/
      MidasPotential
         ( const std::string& aFileName
         ) 
         : GenericBasePot<MidasFunctionWrapper, Nb >(aFileName)
      {
      }
      
      /**
       *
       **/
      void EvaluatePotential(const std::vector<In>&, const std::vector<MidasVector>&, MidasVector&) const;
      
      /**
       *
       **/
      Nb EvaluatePotential
         ( const std::vector<In>& aSetOfModes
         , const MidasVector& aSetOfCoord
         ) const
      {
         return EvaluateBasePotential(aSetOfModes, aSetOfCoord);
      }
};

#endif /* MIDASPOT_H_INCLUDED */
