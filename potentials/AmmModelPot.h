/**
************************************************************************
*
*  @file                AmmModelPot.h
* 
*  Created:             02/04/2008
* 
*  Author:              Daniele Toffoli (toffoli@chem.au.dk)
* 
*  Short Description:   Declares class for computing model potential for ammonia
*                       Parameterization from S. N. Yurchenko et al., JCP 123(2005), 134308
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

#ifndef AMMPOT_H
#define AMMPOT_H

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "util/Io.h"
#include "potentials/ModelPot.h"

class AmmModelPot 
   : public ModelPot
{
   private: 
      In mParMax;                ///< Maximum number of parameters
      MidasVector mParam;        ///< Parameter set
      vector<string> mParamLab;  ///< Label for the parameters
      vector<In> mIvar;          ///< variance??
      In mPotType;               ///< Potential type (=0 for CBS**-5 =1 for CBS**-4)
      bool mHasBeenInit;         ///< Initialization flag

   public:
      AmmModelPot();                                ///< Default constructor
      void SetPotType(In aPotType) {mPotType=aPotType;} ///< Set potential parametrization 
      void InitParms();                             ///< Initialize the parameters to the CBS**-5 basis
      void PrintParms();                            ///< Print out the parameters
      Nb Potential(MidasVector& arGeom);            ///< Function for potential evaluation
      Nb EvalPot(vector<Nuclei>& arGeom);           ///< Wrapper
      Nb GetMinValue();                             ///< Get the minimum value
      Nb LineSearch(Nb aA, Nb aB, Nb aC);           ///< Line search in 1 direction
      Nb f(const Nb aX);
      friend void shft3(Nb &a, Nb &b, Nb &c, const Nb d);
      Nb  brent(const Nb ax, const Nb bx, const Nb cx, const Nb tol, Nb &xmin);
};
#endif
