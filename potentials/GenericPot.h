/**
************************************************************************
*
*  @file                GenericPot.h
* 
*  Created:             01-01-2013
* 
*  Author:              Bo Thomsen (bothomsen@chem.au.dk)
* 
*  Short Description:   Potential class for representing a generic potential
* 
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

#ifndef GENERICPOT_H_INCLUDED
#define GENERICPOT_H_INCLUDED

#include <map>
#include <vector>
#include <string>
#include <fstream>

#include "util/generalfunctions/GenericFunctionWrapper.h"
#include "inc_gen/TypeDefs.h"
#include "potentials/GenericBasePot.h"

class GenericPot 
   : public GenericBasePot<GenericFunctionWrapper, Nb> 
{
   private:   
      //We don't want these functions called
      GenericPot& operator=(const GenericPot&) = delete;
      GenericPot(const GenericPot&) = delete;

   public:
      GenericPot() 
      :  GenericBasePot<GenericFunctionWrapper, Nb >() 
      {
      }

      GenericPot(const std::string& aFileName) 
      :  GenericBasePot<GenericFunctionWrapper, Nb >(aFileName)
      {
      }

      void EvaluatePotential(const std::vector<In>&, const std::vector<MidasVector>&, MidasVector&);

      Nb EvaluatePotential(const std::vector<In>& aSetOfModes, const MidasVector& aSetOfCoord) 
      {
         return EvaluateBasePotential(aSetOfModes,aSetOfCoord);
      }
};

#endif /* GENERICPOT_H_INCLUDED */
