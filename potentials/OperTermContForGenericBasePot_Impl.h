/**
************************************************************************
*
*  @file                OperTermContForGenericBasePot_Impl.h
* 
*  Created:             01-01-2013
* 
*  Author:              Bo Thomsen (bothomsen@chem.au.dk)
* 
*  Short Description:   Operator term for the generic base pot
* 
*  Last modified:       Mads Boettger Hansen (mb.hansen@chem.au.dk)
*                       (Just moved impl. into separate file.)
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

#ifndef OPERTERMCONTFORGENERICBASEPOT_IMPL_H_INCLUDED
#define OPERTERMCONTFORGENERICBASEPOT_IMPL_H_INCLUDED

/***************************************************************************//**
 * @param[in] arMCV
 *    A vector of mode numbers.
 * @return
 *    Whether the argument ModeCombi vector is equal to the one of this object.
 ******************************************************************************/
template <  template<class T> class U
         ,  class V
         >
bool OperTermContForGenericBasePot<U,V>::IsMC
   (  const std::vector<In>& arMCV
   )  const
{
   if (arMCV.size() != mModes.size())
   {
      return false;
   }

   for (In i = I_0; i < mModes.size(); ++i)
   {
      if (mModes[i] != arMCV[i])
      {
         return false;
      }
   }

   return true;
}

/**
 *
 **/
template< template<class T> class U
        , class V
        >
void OperTermContForGenericBasePot<U,V>::InsertTerm
   ( Nb aFactor
   , const std::vector<U<V>* >& arNewOper
   )
{
   In equal_pos = -I_1;
   for(In i = I_0; i < mFunctions.size(); ++i)
   {
      bool is_equal = true;
      for(In j = I_0; j < mFunctions[i].size(); ++j)
      {
         if(mFunctions[i][j] != arNewOper[j])
         {
            is_equal = false;
            break;
         }
      }
      if(is_equal)
      {
         equal_pos = i;
      }
   }
   if(equal_pos != -I_1)
   {
      mCoef[equal_pos] += aFactor;
   }
   else
   {
      mFunctions.emplace_back(arNewOper);
      mCoef.emplace_back(aFactor);
   }
}

/**
 *
 **/
template< template<class T> class U, class V >
V OperTermContForGenericBasePot<U,V>::EvaluateTerms
   (  const MidasVector& arParameter
   )  const
{
   V result = C_0;
   for (In i = I_0; i < mFunctions.size(); ++i)
   {
      V termres = mCoef[i];
      for (In j = I_0; j < arParameter.Size(); ++j)
      {
         termres *= mFunctions[i][j]->EvaluateFunction(std::vector<V>(I_1, arParameter[j]));
      }
      result += termres;
   }
   return result;
}

/**
 *
 **/
template< template<class T> class U, class V >
V OperTermContForGenericBasePot<U,V>::EvaluateTerms
   (  const MidasVector& arParameter
   ,  const MidasVector& arScaling
   )  const
{
   V result = C_0;
   V termres = C_0;
   for (In i = I_0; i < mFunctions.size(); ++i)
   {
      termres = mCoef[i];
      for (In j = I_0; j < arParameter.Size(); ++j)
      {
         termres *= mFunctions[i][j]->EvaluateFunction(std::vector<V>(I_1, arScaling[mModes[j]] * arParameter[j]));
      }
      result += termres;
   }
   return result;
}

/**
 *
 **/
template< template<class T> class U
        , class V
        >
bool OperTermContForGenericBasePot<U,V>::IsInModes
   ( const std::vector<In>& arMCV
   ) const
{
   for(In i = I_0; i < arMCV.size(); ++i)
   {
      bool is_not_found = true;
      for(In j = I_0; j < mModes.size(); ++j)
      {
         if(arMCV[i] == mModes[j])
         {
            is_not_found = false;
            break;
         }
      }
      if(is_not_found)
         return false;
   }
   return true;
}

/***************************************************************************//**
 * @return
 *    The number of terms contained.
 ******************************************************************************/
template< template<class T> class U
        , class V
        >
Uin OperTermContForGenericBasePot<U,V>::NumTerms
   (
   )  const
{
   return mCoef.size();
}

/***************************************************************************//**
 * @param[in] aTerm
 *    Index of desired term.
 * @return
 *    The coefficient of the term.
 ******************************************************************************/
template< template<class T> class U
        , class V
        >
Nb OperTermContForGenericBasePot<U,V>::CoefficientOfTerm
   (  Uin aTerm
   )  const
{
   try
   {
      return mCoef.at(aTerm);
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Out-of-range: " + std::string(oor.what()));
      return 0;
   }
}

/***************************************************************************//**
 * @param[in] aTerm
 *    Index of desired term.
 * @return
 *    The one-mode functions of the term.
 ******************************************************************************/
template< template<class T> class U
        , class V
        >
std::vector<U<V>> OperTermContForGenericBasePot<U,V>::FunctionsOfTerm
   (  Uin aTerm
   )  const
{
   try
   {
      const std::vector<U<V>*>& func_term = mFunctions.at(aTerm);
      std::vector<U<V>> v;
      v.reserve(func_term.size());
      for(const auto& ptr: func_term)
      {
         v.emplace_back(*ptr);
      }
      return v;
   }
   catch(const std::out_of_range& oor)
   {
      MIDASERROR("Out-of-range: " + std::string(oor.what()));
      return std::vector<U<V>>();
   }
}

/***************************************************************************//**
 * @param[in] arOperTerms
 *    Object to extract coefficients from.
 * @return
 *    Vector containing coefficients of all the terms.
 ******************************************************************************/
template< template<class T> class U
        , class V
        >
std::vector<Nb> Coefficients
   (  const OperTermContForGenericBasePot<U,V>& arOperTerms
   )
{
   std::vector<Nb> v;
   v.reserve(arOperTerms.NumTerms());
   for(Uin i = I_0; i < arOperTerms.NumTerms(); ++i)
   {
      v.push_back(arOperTerms.CoefficientOfTerm(i));
   }
   return v;
}

/***************************************************************************//**
 * @param[in] arOperTerms
 *    Object to extract coefficients from.
 * @return
 *    Vector containing coefficients of all the terms.
 ******************************************************************************/
template< template<class T> class U
        , class V
        >
std::vector<std::vector<U<V>>> Functions
   (  const OperTermContForGenericBasePot<U,V>& arOperTerms
   )
{
   std::vector<std::vector<U<V>>> v;
   v.reserve(arOperTerms.NumTerms());
   for(Uin i = I_0; i < arOperTerms.NumTerms(); ++i)
   {
      v.push_back(arOperTerms.FunctionsOfTerm(i));
   }
   return v;
}


#endif /* OPERTERMCONTFORGENERICBASEPOT_IMPL_H_INCLUDED */
