/**
************************************************************************
*
*  @file                MidasPotential.cc
* 
*  Created:             01-01-2013
* 
*  Author:              Bo Thomsen (bothomsen@chem.au.dk)
* 
*  Short Description:   Potential class for representing a midas potential
* 
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
**/
#include <map>
#include <vector>
#include <string>
#include <fstream>

#include "mmv/MidasVector.h"
#include "potentials/MidasPotential.h"
#include "pes/PesInfo.h"
#include "util/Io.h"
#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"

/**
 * Evaluate the potential 
**/
void MidasPotential::EvaluatePotential
   (  const std::vector<In>& arMode
   ,  const std::vector<MidasVector>& arGrid
   ,  MidasVector& arPotVals
   )  const
{
   arPotVals.SetNewSize(arGrid.size());
   for(Uin i = I_0; i < arGrid.size(); ++i)
   {
      arPotVals[i] = EvaluatePotential(arMode, arGrid[i]);
   }
}
