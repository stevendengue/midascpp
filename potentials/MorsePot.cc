/**
*************************************************************************
*
* @file                MorsePot.cc
*
* Created:             30/10/2007
*
* Author:              Daniele Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Definitions of class members for MorsePot
*
* Last modified:       30/17/2007
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

// std headers
#include <vector>
#include <iostream>
#include <fstream>
#include <cctype>
#include <map>
#include <string>
#include <algorithm>

// midas headers
#include "potentials/MorsePot.h"
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "input/Input.h"
#include "pes/Pes.h"
#include "mmv/MidasVector.h"
#include "nuclei/Nuclei.h"

////
// Register for factory
////
ModelPotRegistration<MorsePot> registerMorsePot("MORSEPOT");

/**
 * @brief Default constructor
 *        By default consider standard H2+ molecule
 *        with De=0.1026 au, Re=1.9972 au, a=0.732 au
 *        and with a Rmax value of 120 au
 **/
MorsePot::MorsePot() 
   : mMorsePotDef("")
   , mDe(0.1026)
   , ma(0.732)
   , mReq(1.9972)
   , mMuRed(0.50392)
{
}

/**
 * @brief Constructor
 **/
MorsePot::MorsePot
   ( Nb aDe
   , Nb a
   , Nb aReq
   , Nb aMured
   )
   : mMorsePotDef("")
   , mDe(aDe)
   , ma(a)
   , mReq(aReq)
   , mMuRed(aMured)
{
}

/**
 * @brief
 **/
MorsePot::MorsePot(const ModelPotInfo& aInfo)
   :  MorsePot()
{
   auto iter = aInfo.find("INITSTRING");
   if(iter != aInfo.end())
      SetFromString(iter->second);
}

/**
 * @brief Set the private members from a string
 * @param aAux 
 **/
void MorsePot::SetFromString(std::string aAux)
{
   // Transform tolower
   transform(aAux.begin(),aAux.end(),aAux.begin(), (In(*) (In)) tolower);
   mMorsePotDef = aAux;
   // Extract info from aAux 
   vector<string> aux_st_vec;

   // Go backwards and find labels;
   do {  
      string piece;
      size_t cfind = aAux.rfind(",");
      if (cfind!=aAux.npos) piece = aAux.substr(cfind);
      if (cfind==aAux.npos) piece = aAux;  // No , found take it all.
      size_t l_p = piece.size();
      while(piece.find(",")!= piece.npos) piece.erase(piece.find(","),I_1);
      aux_st_vec.push_back(piece);
      if (cfind!=aAux.npos) aAux.erase(cfind,l_p);
      if (cfind==aAux.npos) aAux.erase(I_0,l_p);
      //Mout << " in loop piece = " << piece << " and aAux " << aAux << endl;
   } while (aAux.size()>0);

   for (In i=0;i<aux_st_vec.size();i++)
   {
   //Mout << " Checking: " << aux_st_vec[i] << " for useful information" << endl;
      if (aux_st_vec[i].find("de=")!=aux_st_vec[i].npos) 
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("de"),aux_st_vec[i].find("=")+1);
         mDe=NbFromString(aux_st_vec[i]);
         if (gDebug) Mout << " mDe found to be: "  << mDe << endl;
      }
      else if (aux_st_vec[i].find("mured=")!=aux_st_vec[i].npos)
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("mured"),aux_st_vec[i].find("=")+1);
         mMuRed=NbFromString(aux_st_vec[i]);
         if (gDebug) Mout << " mMuRed found to be: "  << mMuRed << endl;
      }
      else if (aux_st_vec[i].find("acoeff=")!=aux_st_vec[i].npos)
      {
         aux_st_vec[i].erase(aux_st_vec[i].find(""),aux_st_vec[i].find("=")+1);
         Nb ma=NbFromString(aux_st_vec[i]);
         if (gDebug) Mout << " ma found to be: "  << ma << endl;
      }
      else if (aux_st_vec[i].find("req=")!=aux_st_vec[i].npos)
      {    
         aux_st_vec[i].erase(aux_st_vec[i].find("req"),aux_st_vec[i].find("=")+1);
         mReq=NbFromString(aux_st_vec[i]);
         if (gDebug) Mout << " mReq found to be: "  << mReq << endl;
      }
      else
         Mout << " No useful information found in basis set auxiliary input:" << aux_st_vec[i] <<endl;
   }

   //checks for consistency of the data
   if (mMuRed<0) MIDASERROR (" mMuRed negative in MorsePot ");
}


/**
 * @brief evaluate the potential
 * @param x point to evaluate potential
 **/
Nb MorsePot::EvalPot(Nb x) const
{
   Nb tmp = (C_1 - std::exp(-ma*(x-mReq)));
   Nb vmorse = mDe * std::pow(tmp, C_2);
   return vmorse;
}

/**
 * @brief evaluate the potential
 * @param x point to evaluate potential
 **/
MidasVector MorsePot::EvalDer(Nb x) const
{
   MidasVector der(1);
   Nb tmp1 =  2*ma*std::exp(-ma*(x-mReq));
   Nb tmp2 = -2*ma*std::exp(-2*ma*(x-mReq));
   der.at(0) = mDe * (tmp1 + tmp2);
   return der;
}

/**
 * Evaluate hessian.
 *
 * @param x   point to evaluate at
 **/
MidasMatrix MorsePot::EvalHess(Nb x) const
{
   MidasMatrix hess(I_1, I_1);
   hess.at(0, 0) = 2 * mDe * ma * ma * (std::exp(-ma * (x - mReq)) - std::exp(-2 * ma * (x - mReq)));
   return hess;
}

/**
 * Evaluate potential for nuclei positions.
 *
 * @param aName         The name of the operator/property to evaluate.
 * @param aNucVect      Molecular structure
 **/
Nb MorsePot::EvalPotImpl
   (  const std::string& aName
   ,  const std::vector<Nuclei>& aNucVect
   )  const
{
   if (aNucVect.size() != I_2)
   {
      MIDASERROR("Morse potential only inplemented for two nuclei!!");
   }
   return EvalPot(aNucVect.front().Distance(aNucVect.back()));
}

/**
 * Evaluate first derivative of potential for nuclei positions.
 * 
 * @param aName       The name of the operator/property to evaluate.
 * @param aNucVect    Molecular structure 
 **/
MidasVector MorsePot::EvalDerImpl
   (  const std::string& aName
   ,  const std::vector<Nuclei>& aNucVect
   )  const
{
   if (aNucVect.size() != I_2)
   {
      MIDASERROR("Morse potential only inplemented for two nuclei!!");
   }
   return EvalDer(aNucVect.front().Distance(aNucVect.back()));
}

/**
 * Evaluate second derivative of potential for nuclei positions.
 * 
 * @param aName       The name of the operator/property to evaluate.
 * @param aNucVect    Molecular structure 
 **/
MidasMatrix MorsePot::EvalHessImpl
   (  const std::string& aName
   ,  const std::vector<Nuclei>& aNucVect
   )  const
{
   if (aNucVect.size() != I_2)
   {
      MIDASERROR("Morse potential only inplemented for two nuclei!!");
   }
   return EvalHess(aNucVect.front().Distance(aNucVect.back()));
}

/**
 * @brief overload output for MorsePot class
 * @param arOut output stream
 * @param arPot potential to be outputtet
 **/
ostream& operator<<(ostream& arOut, const MorsePot& arPot)
{  
   arOut.setf(ios::scientific);
   arOut.setf(ios::uppercase);
   midas::stream::ScopedPrecision(18, arOut);
   arOut.setf(ios::showpoint);

   arOut << " Morse Potential Data: \n"
         << " Parameters: \n" 
         << " De    = " << arPot.mDe << "\n"
         << " a     = " << arPot.ma << "\n"
         << " Req   = " << arPot.mReq << "\n"
         << " Mu    = " << arPot.mMuRed << "\n\n"
         << " Bond length (a.u.)   "  << "   " << " Potential value  "  << std::endl;

   return arOut;
}

///**
// * Get the frequency for the Morse potential
// **/
//Nb MorsePot::MorseFreq()
//{
//   Nb pot_freq=ma*sqrt(C_2*mDe/mMuRed);
//   pot_freq*=C_AUTKAYS;
//   return pot_freq;
//}
