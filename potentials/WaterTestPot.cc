/**
*************************************************************************
*
* @file                WaterTestPot.cc
*
* Created:             01/10/2007
*
* Author:              D. Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Definitions of class members for the 
*                      Partridge-Schwenke water potential
*
* Last modified:       01/17/2007
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/


// std headers
#include <vector>
#include <string>
#include <string>
#include <iostream>
#include <fstream>
#include <cctype>
#include <map>
#include <algorithm>

// midas headers
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "input/Input.h"
#include "pes/Pes.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "potentials/WaterTestPot.h"

/**
* Default constructor
**/
TestModelPot::TestModelPot()
{
   mHasBeenInit=false;
}
/**
* Deconstructor
* */
TestModelPot::~TestModelPot()
{
   
}
/**
 * Initialize object
 * */
void TestModelPot::Init()
{
   InitConsts();
   InitArrays();
   mHasBeenInit=true;
}
/**
* Initialize the constants
* */
void TestModelPot::InitConsts()
{

}
/**
* Initialize the arrays
* */
void TestModelPot::InitArrays()
{

}
/**
 * Evaluate the potential over an assigned geometry
 * */
Nb TestModelPot::EvalPot(vector<Nuclei>& arGeom)
{
   In n_nuc=arGeom.size();
   if(n_nuc!=2) //3
      MIDASERROR(" Wrong nr. of nuclei in WaterPot");
   //it is assumed that atom #1 is Oxygen check it
   bool o_fnd=false;
   bool h1_fnd=false;
   bool h2_fnd=false;
   In p_o=-1;
   In p_h1=-1;
   In p_h2=-1;
   for (In i=0; i<n_nuc; i++)
   {
      string lab=arGeom[i].AtomLabel();
      if (!h1_fnd && lab.substr(0,1)=="H")
      {
         p_h1=i;
         h1_fnd=true;
      }
      else if(!h2_fnd && lab.substr(0,1)=="H")
      {
         p_h2=i;
         h2_fnd=true;
      }
      else
        MIDASERROR(" labels not correct in input structure ");
   }
   if (!(/*o_fnd &&*/ h1_fnd && h2_fnd))
      MIDASERROR(" geometry not properly read into ");
   if (gDebug) 
   {
      Mout << " Geometry input in NasaModelPot::EvalPot(): " << endl;
      for (In i=0; i<n_nuc; i++)
         Mout << arGeom[i]<< endl;
      Mout << endl;
   }
   //now fill the arrays:
   MidasMatrix r1(3,C_0);
   r1[0][1]=arGeom[p_h1].X();
   r1[1][1]=arGeom[p_h1].Y();
   r1[2][1]=arGeom[p_h1].Z();
   r1[0][2]=arGeom[p_h2].X();
   r1[1][2]=arGeom[p_h2].Y();
   r1[2][2]=arGeom[p_h2].Z();
   MidasMatrix dr1(3,C_0);
   Nb e1=C_0;
   cout << "Coordinates in potential for " << arGeom[p_h1].AtomLabel() << " : " << r1[0][1] << " " << r1[1][1] << " " << r1[2][1] << endl;
   cout << "Coordinates in potential for " << arGeom[p_h2].AtomLabel() << " : " << r1[0][2] << " " << r1[1][2] << " " << r1[2][2] << endl;
   //scale to Aangstrom
   //r1.Scale(C_TANG);
   Pot(r1,dr1,e1);
   return e1;
}
/** 
* calculates the energy (e1) and the derivatives (dr1) of a water monomer
* (coordinates in the array r1). The potential has been developed by
* "H. Partridge and D. W. Schwenke, J. Chem. Phys. 106, 4618 (1997)".
* Some extra code for the calculation of the energy derivatives has been
* added  by C. J. Burnham.
**/ 
void TestModelPot::Pot(MidasMatrix& r1, MidasMatrix&  dr1, Nb& e1) 
{
   //MidasVector ROH1(3,C_0);
   //for (In i=0; i<3; i++)  ROH1[i]=r1[i][1]-r1[i][0];
   //MidasVector ROH2(3,C_0);
   //for (In i=0; i<3; i++)  ROH2[i]=r1[i][2]-r1[i][0];
   MidasVector RHH(3,C_0);
   for (In i=0; i<3; i++)  RHH[i]=r1[i][1]-r1[i][2];

   //Mout << " Midasvector ROH1:  " << endl;
   //Mout << ROH1 << endl;
   //Mout << " Midasvector ROH2:  " << endl;
   //Mout << ROH2 << endl;
   //Mout << " MidasVector RHH:   " << endl;
   //Mout << RHH << endl;

   //Nb dROH1 = ROH1.Norm();
   //Nb dROH2 = ROH2.Norm();
   Nb dRHH  = RHH.Norm();
   //Mout << "  dROH1: " << dROH1 << "  dROH2: " << dROH2 << "  dROHH: " << dRHH;

   Nb costh;
   //costh=acos(( ROH1[0]*ROH2[0] + ROH1[1]*ROH2[1] + ROH1[2]*ROH2[2] ) / (dROH1*dROH2));
  

   //Nb costhe = 1.019121e+02*C_PI/C_180;
   Nb reoh = .7590978439824798/C_TANG;

   //Nb x1 = (dROH1-reoh);
   //Nb x2 = (dROH2-reoh);
   //Nb x3 = costh - costhe;
   
   Nb x1 = RHH.Norm() - reoh;
   
   cout << "Displacements in : " << endl;
   cout << "x1 : " << x1 << endl;
   //cout << "x2 : " << x2 << endl;
   //cout << "x3 : " << x3 << endl;

   e1 = x1*x1*0.1841087087670501641921251;// + x2*x2*0.1 + x3*x3*0.05;
}     
/**
 * Output overloading
 * */
ostream& operator<<(ostream& arOut, const TestModelPot& arPot)
{
   return arOut;
}
