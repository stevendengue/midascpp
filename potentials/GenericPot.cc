/**
************************************************************************
*
*  @file                GenericPot.h
* 
*  Created:             01-01-2013
* 
*  Author:              Bo Thomsen (bothomsen@chem.au.dk)
* 
*  Short Description:   Potential class for representing a generic potential
* 
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */
#include <map>
#include <vector>
#include <string>
#include <fstream>

#include "mmv/MidasVector.h"
#include "potentials/GenericPot.h"
#include "pes/PesInfo.h"
#include "util/Io.h"
#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/MidasFunctionWrapper.h"

using std::map;
using std::vector;
using std::string;
using std::fstream;

/**
 * Evaluate the potential 
**/
void GenericPot::EvaluatePotential
   ( const vector<In>& arMode
   , const vector<MidasVector>& arGrid
   , MidasVector& arPotVals
   )
{
   arPotVals.SetNewSize(arGrid.size());
   
   for (Uin i = I_0; i < arGrid.size(); ++i)
   {
      arPotVals[i] = EvaluatePotential(arMode, arGrid[i]);
   }
}

