/**
*************************************************************************
*
* @file                DoubleWellPot.cc
*
* Created:             06/03/2008
*
* Author:              Daniele Toffoli (toffoli@chem.au.dk)
*
* Short Description:   Declares class for computing double-well potentials
*                      Parameterization from J. B. Coon et al. J. Mol. Spectrosc. 20(1966), 107
*
* Last modified: Thu Dec 03, 2009  09:51AM ove
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

// std headers
#include <vector>
#include <iostream>
#include <fstream>
#include <cctype>
#include <map>
#include <string>
#include <algorithm>

// midas headers
#include "inc_gen/stdlib_link.h"
#include "inc_gen/math_link.h"
#include "inc_gen/TypeDefs.h"
#include "input/Input.h"
#include "pes/Pes.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"
#include "potentials/DoubleWellPot.h"

////
// Register for factory
////
ModelPotRegistration<DoubleWellPot> registerDoubleWellPot("DOUBLEWELLPOT");

/**
* @brief Default constructor
*        By default consider a model for the NH3 inversion problem
*        as fitted in J. B. Coon et al. J. Mol. Spectrosc. 20(1966), 107
**/
DoubleWellPot::DoubleWellPot()
{
   mHarmFreq=974.2/C_AUTKAYS;
   Nb b_val=2032.0e0/C_AUTKAYS;
   Nb rho=0.60;
   mA=b_val*exp(rho)/(exp(rho)-rho-C_1);
   mAlpha=(exp(rho)/(C_2*mA))*pow(mHarmFreq,C_2);
   mMinVal= b_val*(rho+C_1)/(exp(rho)-rho-C_1);
   mRmax=C_0;
   mPotDef="";
}

/**
 * @brief Constructor
 **/
DoubleWellPot::DoubleWellPot(Nb aB, Nb aRho, Nb aHarmFreq, Nb aRmax)
{
   Nb rho=aRho;
   mA=aB*exp(rho)/(exp(rho)-rho-C_1);
   mAlpha=(exp(rho)/(C_2*mA))*pow(mHarmFreq,C_2);
   mHarmFreq=aHarmFreq;
   mMinVal=aB*(rho+C_1)/(exp(rho)-rho-C_1);
   mRmax=aRmax;
   mPotDef="";
}

/**
 * @brief
 **/
DoubleWellPot::DoubleWellPot(const ModelPotInfo& aInfo): DoubleWellPot()
{
   auto iter = aInfo.find("INITSTRING");
   if(iter != aInfo.end())
      SetFromString(iter->second);
}

/**
 * @brief Set the private members from a string
 * @param aAux string that defines doublewell potential
 **/
void DoubleWellPot::SetFromString(string aAux)
{
   // Transform tolower
   transform(aAux.begin(),aAux.end(),aAux.begin(), (In(*) (In)) tolower);
   mPotDef = aAux;
   // Extract info from aAux 
   vector<string> aux_st_vec;
   Nb rho=C_0;
   Nb b_val=C_0;
   Nb harm_freq=C_0;
   Nb rmax=C_0;
   In n_pts=I_0;
   // Go backwards and find labels;
   do {  
      string piece;
      std::string::size_type cfind = aAux.rfind(",");
      if (cfind!=aAux.npos) piece = aAux.substr(cfind);
      if (cfind==aAux.npos) piece = aAux;  // No , found take it all.
      std::string::size_type l_p = piece.size();
      while(piece.find(",")!= piece.npos) piece.erase(piece.find(","),I_1);
      aux_st_vec.push_back(piece);
      if (cfind!=aAux.npos) aAux.erase(cfind,l_p);
      if (cfind==aAux.npos) aAux.erase(I_0,l_p);
      //Mout << " in loop piece = " << piece << " and aAux " << aAux << endl;
   } while (aAux.size()>0);

   for (In i=0; i<aux_st_vec.size(); i++)
   {
      //Mout << " Checking: " << aux_st_vec[i] << " for useful information" << endl;
      if (aux_st_vec[i].find("rho=")!=aux_st_vec[i].npos) 
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("rho"),aux_st_vec[i].find("=")+1);
         rho=NbFromString(aux_st_vec[i]);
         if (gDebug) Mout << " rho found to be: "  << rho << endl;
      }
      if (aux_st_vec[i].find("freq=")!=aux_st_vec[i].npos)
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("freq"),aux_st_vec[i].find("=")+1);
         harm_freq=NbFromString(aux_st_vec[i]);
         if (gDebug) Mout << " freq found to be: "  << harm_freq << endl;
      }
      else if (aux_st_vec[i].find("b_val=")!=aux_st_vec[i].npos)
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("b_val"),aux_st_vec[i].find("=")+1);
         b_val=NbFromString(aux_st_vec[i]);
         if (gDebug) Mout << " b_val found to be: "  << b_val << endl;
      }
      else if (aux_st_vec[i].find("rmax=")!=aux_st_vec[i].npos)
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("rmax"),aux_st_vec[i].find("=")+1);
         rmax=NbFromString(aux_st_vec[i]);
         if (gDebug) Mout << " rmax found to be: "  << rmax << endl;
      }
      else if (aux_st_vec[i].find("nint=")!=aux_st_vec[i].npos)
      {
         aux_st_vec[i].erase(aux_st_vec[i].find("nint"),aux_st_vec[i].find("=")+1);
         n_pts=atoi(aux_st_vec[i].c_str())+1;
         if (gDebug) Mout << " n_pts found to be: "  << n_pts << endl;
      }
      else
         Mout << " No useful information found in basis set auxiliary input:" << aux_st_vec[i] <<endl;
   }

   //fill the private members
   mA=b_val*exp(rho)/(exp(rho)-rho-C_1);
   mMinVal= b_val*(rho+C_1)/(exp(rho)-rho-C_1);
   mAlpha=(exp(rho)/(C_2*mA))*pow(harm_freq,C_2);;
   mHarmFreq=harm_freq;
   mRmax=fabs(rmax);
}

/**
 * @brief To streamline the interfaces we provide the following function
 *
 * @param aName    The name of the operator/property to evaluate.
 * @param aNucVect molecule as a set of nuclei
 *
 * @return    Returns the value of the potential.
 **/
Nb DoubleWellPot::EvalPotImpl
   (  const std::string& aName
   ,  const std::vector<Nuclei>& aNucVect
   ) const
{
   if(aNucVect.size() != 2)
   {
      MIDASERROR("DoubleWell potential only inplemented for two nuclei!!");
   }
   return EvalPot(aNucVect.front().Distance(aNucVect.back()));
}

/**
 * @brief Evaluate the potential over an assigned point
 * @param x evaluate potential at this point
 **/
Nb DoubleWellPot::EvalPot(Nb x) const
{
   Nb red_mass = 2.48658405611024369; //red_mass is the reduced mass of the NH3 molecule
   Nb x_p = x*std::sqrt(red_mass*C_FAMU);
   Nb tmp = pow(x_p,C_2);
   Nb v_min = mMinVal;
   Nb v_pot = C_I_2*pow(mHarmFreq,C_2)*tmp + mA*exp(-mAlpha*tmp) - v_min;
   return v_pot;
}

/**
 * @brief Overload output for DoubleWellPot class
 * @param arOut output stream
 * @param arPot potential to output
 **/
ostream& operator<<(std::ostream& arOut, const DoubleWellPot& arPot)
{  
   arOut.setf(std::ios::scientific);
   arOut.setf(std::ios::uppercase);
   midas::stream::ScopedPrecision(18, arOut);
   arOut.setf(std::ios::showpoint);

   arOut << " Double Well Potential Data: \n"
         << " Parameters:  \n"
         << " Harm. Freq.:           " << arPot.mHarmFreq << "\n"
         << " Alpha:                 " << arPot.mAlpha << "\n"
         << " A const:               " << arPot.mA << "\n"
         << " Rmax:                  " << arPot.mRmax 
         << std::endl;

   return arOut;
}
