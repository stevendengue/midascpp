/**
************************************************************************
*
*  @file                ModelPot.cc
* 
*  Created:             01-01-2013
* 
*  Author:              Bo Thomsen (bothomsen@chem.au.dk)
* 
*  Short Description:   Base class for the potentials
* 
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

#include "potentials/ModelPot.h"

#include <vector>
#include <string>

#include "potentials/MorsePot.h"
#include "potentials/PartridgePot.h"
#include "potentials/AmmModelPot.h"
#include "potentials/ArNtwopPot.h"
#include "potentials/FilePot.h"
#include "potentials/DoubleWellPot.h"
#include "pes/PesInfo.h"

/**
 * Factory for creating model potentials.
 *
 * @param aInfo    The info to create potential from.
 *
 * @return   Returns smart pointer to newly created modelpot.
 **/
std::unique_ptr<ModelPot> ModelPot::Factory
   (  const ModelPotInfo& aInfo
   )
{
   auto iter = aInfo.find("MODELPOT");
   if(iter == aInfo.end()) 
   {
      MIDASERROR("Did not find keyword MODELPOT");
   }
   
   auto keys = ModelPotFactory::get_registered_keys();
  
   auto pot = ModelPotFactory::create(iter->second, aInfo);
   if(!pot) 
   {
      MIDASERROR("Could not create potential.");
   }
   return pot;
}
