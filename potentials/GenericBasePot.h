/**
************************************************************************
*
*  @file                GenericBasePot.h
* 
*  Created:             01-01-2013
* 
*  Author:              Bo Thomsen (bothomsen@chem.au.dk)
* 
*  Short Description:   Generic class for reading and evaluating a potential
*                       Templated for convinience
* 
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

#ifndef GENERICBASEPOT_H_INCLUDED
#define GENERICBASEPOT_H_INCLUDED

// std headers
#include <map>
#include <vector>
#include <string>
#include <fstream>

// midas headers
#include "potentials/OperTermContForGenericBasePot.h"
#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/FunctionContainer.h"
#include "util/Io.h"
#include "util/conversions/FromString.h"
#include "util/conversions/VectorFromString.h"
#include "input/MidasOperatorReader.h"
#include "mmv/MidasVector.h"

/**
 * @class GenericBasePot
 *    some class for potentials :O
 **/
template< template<class T> class U // type of function, e.g. MidasFunctionWraper
        , class V // Input type for function, e.g. Nb
        >
class GenericBasePot
{
   protected:

      FunctionContainer<V>                              mPreDefFunctions;
      std::map<std::string, U<V> >                      mFunctions;
      std::vector<OperTermContForGenericBasePot<U, V> > mTermsByMCV;
      MidasVector                                       mScalingFactors;
      bool                                              mScalePot;      
      
      void GenericBasePotReadIn(const std::string&);
      
      //! Is the given ModeCombi contained, and if so return its index.
      std::pair<bool,Uin> FindTerm(const std::vector<In>&) const;

   public:
      /**
       *
       **/
      GenericBasePot() : mScalePot(false)
      {
      }
      
      /**
       *
       **/
      GenericBasePot(const std::string& aFileName) : mScalePot(false)
      {
         GenericBasePotReadIn(aFileName);
      }
      
      /**
       *
       **/
      virtual ~GenericBasePot()
      {
      }
      
      /**
       * The container of fit functions (mFunctions) is cleared in order to update variables that are being optimized on each iteration in ADGA. Potential coefficients are also updated in the function.
       **/
      void CleanAndReread(const std::string& aFileName) 
      {
         mFunctions.clear();
         mTermsByMCV.clear(); 
         GenericBasePotReadIn(aFileName);
      }
      
      /**
       *
       **/
      V EvaluateBasePotential(const std::vector<In>&, const MidasVector&) const;

      //@{
      //! Vector of coefficients/function products for the given ModeCombi.
      std::vector<Nb> CoefficientsForModeCombi(const std::vector<In>&) const;
      std::vector<std::vector<U<V>>> FunctionsForModeCombi(const std::vector<In>&) const;
      //@}

      //! Vector of scaling factors for the modes of the given ModeCombi.
      std::vector<Nb> ScalingFactorsForModeCombi(const std::vector<In>&) const;
};

/**
 * 
 **/
template< template<class T> class U
        , class V
        >
void GenericBasePot<U,V>::GenericBasePotReadIn
   (  const std::string& aFileName
   )
{
   // read in operator
   MidasOperatorReader oper_reader(aFileName);
   oper_reader.ReadOperator(aFileName);
  
   FunctionContainer<V> func_cont;
   oper_reader.GetFunctionContainer(func_cont);
   oper_reader.GetScalingVector(mScalingFactors);
   
   mScalePot = (I_0 != mScalingFactors.Size());
   
   // loop through operator terms and sort them according to mode-combination
   int modenumber = 0;
   std::map<std::string, int> modename_number_map;
   for(auto it_opers = oper_reader.GetFrontOfOperators(); it_opers != oper_reader.GetEndOfOperators(); ++it_opers)
   {
      // find mode-combination for operator term
      std::vector<In> modes;
      std::vector<U<V>* > func_vector;
      auto it_mode_names = it_opers->first.first.begin();
      for(auto it_funcs = it_opers->first.second.begin(); it_funcs != it_opers->first.second.end(); ++it_funcs, ++it_mode_names)
      {
         // If mode name is not numbered, we add an entry in the numbering map.
         auto modename_iter = modename_number_map.find(*it_mode_names);
         if(modename_iter == modename_number_map.end())
         {
            bool temp;
            std::tie(modename_iter, temp) = modename_number_map.emplace(*it_mode_names, modenumber++);
         }
         
         // Make vector of functions and vector of modes for functions
         auto ins_it = mFunctions.find(*it_funcs);
         if(mFunctions.end() == ins_it)
         {
            auto temp_it_test = mFunctions.insert(std::make_pair(*it_funcs,U<V>(*it_funcs, func_cont)));
            ins_it = temp_it_test.first;
         }
         func_vector.push_back(&ins_it->second);
         //modes.push_back(midas::util::FromString<In>(it_mode_names->substr(it_mode_names->find_first_of("0123456789"))));
         modes.push_back(modename_iter->second);
      }
     
      // check if we have already added terms for this mode-combination
      auto found = mTermsByMCV.end();
      for(auto it = mTermsByMCV.begin(); it != mTermsByMCV.end(); ++it)
      {
         if(it->IsMC(modes))
         {
            found = it;
         }
      }

      if(mTermsByMCV.end() == found) // if not, we add the mode-combination and insert term
      {
         mTermsByMCV.emplace_back(modes);
         mTermsByMCV.back().InsertTerm(it_opers->second, func_vector);
      }
      else // else we just add the term to the mode-combination
      {
         found->InsertTerm(it_opers->second,func_vector);
      }
   }
}

/***************************************************************************//**
 * @param[in] arModeCombi
 *    ModeCombi vector to look for.
 * @return
 *    Pair; `first` is whether arModeCombi was found. If true, `second` is the
 *    index of the corresponding OperTermContForGenericBasePot in mTermsByMCV.
 *    If false, `second` shouldn't be used.
 ******************************************************************************/
template< template<class T> class U
        , class V
        >
std::pair<bool,Uin> GenericBasePot<U,V>::FindTerm
   (  const std::vector<In>& arModeCombi
   )  const
{
   auto iter_mc = std::find_if
      (  mTermsByMCV.begin()
      ,  mTermsByMCV.end()
      ,  [&arModeCombi](const OperTermContForGenericBasePot<U,V>& a)->bool {return a.IsMC(arModeCombi);}
      );
   return std::make_pair(iter_mc != mTermsByMCV.end(), std::distance(mTermsByMCV.begin(), iter_mc));
}

/**
 * 
 **/
template< template<class T> class U, class V >
V GenericBasePot<U,V>::EvaluateBasePotential
   (  const std::vector<In>& arMode
   ,  const MidasVector& arGrid
   )  const
{
   In term_nr = -I_1;
   for (In i = I_0; i < mTermsByMCV.size(); ++i)
   {
      if (mTermsByMCV[i].IsMC(arMode))
      {
         term_nr = i;
         break;
      }
   }

   if (term_nr != -I_1)
   {
      if (mScalePot)
      {
         return mTermsByMCV[term_nr].EvaluateTerms(arGrid, mScalingFactors);
      }
      else
      {
         return mTermsByMCV[term_nr].EvaluateTerms(arGrid);
      }
   }
   else
   {  
      return V(C_0);
   }
}

/***************************************************************************//**
 * @param[in] arModeCombi
 *    ModeCombi vector of interest.
 * @return
 *    Vector of coefficients/functions for that ModeCombi. If this object
 *    doesn't contain terms for that ModeCombi, the returned vector will be of
 *    size 0.
 ******************************************************************************/
template< template<class T> class U
        , class V
        >
std::vector<Nb> GenericBasePot<U,V>::CoefficientsForModeCombi
   (  const std::vector<In>& arModeCombi
   )  const
{
   const auto pair_found_term = FindTerm(arModeCombi);
   if (pair_found_term.first)
   {
      return Coefficients(mTermsByMCV[pair_found_term.second]);
   }
   else
   {
      return std::vector<Nb>();
   }
}

template< template<class T> class U
        , class V
        >
std::vector<std::vector<U<V>>> GenericBasePot<U,V>::FunctionsForModeCombi
   (  const std::vector<In>& arModeCombi
   )  const
{
   const auto pair_found_term = FindTerm(arModeCombi);
   if (pair_found_term.first)
   {
      return Functions(mTermsByMCV[pair_found_term.second]);
   }
   else
   {
      return std::vector<std::vector<U<V>>>();
   }
}

/***************************************************************************//**
 * @note
 *    If `mScalePot == true`, throws MIDASERROR if any of the given modes are
 *    out-of-range.
 *
 * @param[in] arModeCombi
 *    The ModeCombi vector containing the modes of interest.
 * @return
 *    Vector containing scaling factors of the modes in arModeCombi. If this
 *    object has `mScalePot == true`, will return a vector of 1's.
 ******************************************************************************/
template< template<class T> class U
        , class V
        >
std::vector<Nb> GenericBasePot<U,V>::ScalingFactorsForModeCombi
   (  const std::vector<In>& arModeCombi
   )  const
{
   if (mScalePot)
   {
      std::vector<Nb> v;
      v.reserve(arModeCombi.size());
      for(const auto& m: arModeCombi)
      {
         if (m >= mScalingFactors.Size())
         {
            MIDASERROR( "Out-of-range: m = "+std::to_string(m)+
                        ", mScalingFactors.Size() = "+std::to_string(mScalingFactors.Size())+
                        ".");
         }
         v.push_back(mScalingFactors[m]);
      }
      return v;
   }
   else
   {
      return std::vector<Nb>(arModeCombi.size(), C_1);
   }
}


#endif /* GENERICBASEPOT_H_INCLUDED */
