/**
*************************************************************************
*
* @file                FilePot.cc
* 
* Created:             25/04/2008
* 
* Author:              Eduard Matito (eduard@chem.au.dk)
* 
* Short Description:   Definitions of class members for FilePot
*
* Last modified: Tue Jun 08, 2010  04:01PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
**/

#include "potentials/FilePot.h"

// std headers
#include <vector>
#include <iostream>
#include <fstream>
#include <cctype>
#include <map>
#include <string>
#include <algorithm>
#include <cstdlib>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "pes/PesInfo.h"
#include "pes/PesFuncs.h"
#include "util/generalfunctions/GenericFunctionWrapper.h"
#include "input/AbsPath.h"

////
// Register for factory
////
ModelPotRegistration<FilePot> registerFilePot("FILEPOT");

/**
 * Calculate Q displacements for input geometry compared to at reference geometry.
 * This is done by first calculating cartesian xyz displacements, 
 * then transforming these with the normalcoordinates (L-mat) to Q-space.
 *
 * @param arGeom   The geometry to calculate displacement for.
 *
 * @return   Returns displacements.
 **/
std::vector<Nb> FilePot::CalculateQDisplacements
   (  const std::vector<Nuclei>& arGeom
   )  const
{
   // Calculate cartesian displacement.
   const auto& reference_geom = mMolecule.GetCoord();
   std::vector<Nb> displacements(I_3 * arGeom.size());
   for(In iatom = 0; iatom < arGeom.size(); ++iatom)
   {
      In idx = iatom * 3;
      displacements[idx    ] = arGeom[iatom].X() - reference_geom[iatom].X();
      displacements[idx + 1] = arGeom[iatom].Y() - reference_geom[iatom].Y();
      displacements[idx + 2] = arGeom[iatom].Z() - reference_geom[iatom].Z();
      
      displacements[idx    ] *= std::sqrt(C_FAMU) * arGeom[iatom].GetMass();
      displacements[idx + 1] *= std::sqrt(C_FAMU) * arGeom[iatom].GetMass();
      displacements[idx + 2] *= std::sqrt(C_FAMU) * arGeom[iatom].GetMass();
   }
   
   // Calculate q-displacement from cartesiena displacement and normalcoordinates (the L-matrix).
   const auto& normalcoord = mMolecule.NormalCoord();
   std::vector<Nb> q(normalcoord.Nrows());
   for(In i = 0; i < normalcoord.Nrows(); ++i)
   {
      q[i] = C_0;
      for(In j = 0; j < normalcoord.Ncols(); ++j)
      {
         q[i] += normalcoord[i][j] * displacements[j];
      }
   }
   
   // Get and apply scalings
   MidasVector scalings;
   mOperator.GetScalingVector(scalings);

   for(In i = 0; i < q.size(); ++i)
   {
      q[i] *= scalings[i];
   }

   return q;
}


/**
 * Evaluate potential at given geometry.
 *
 * @param aName    The name of the operator/property to evaluate.
 * @param arGeom   The geometry to evaluate for.
 *
 * @return   Return the value of the potential in the given point.
 **/
Nb FilePot::EvalPotImpl
   (  const std::string& aName
   ,  const std::vector<Nuclei>& arGeom
   )  const
{
   // Get q displacements
   auto q = CalculateQDisplacements(arGeom);
   
   // Evaluate potential for displacement
   // ----------------------------------------------------------------------
   // Get functions
   FunctionContainer<Nb> function_container;
   mOperator.GetFunctionContainer(function_container);

   // Get modenames to be able to find correct index
   const auto& modenames = mOperator.GetModeNames();
   
   // Calculate value of potential
   Nb value = C_0;
   for(  auto iter = mOperator.GetFrontOfOperators()
      ;  iter != mOperator.GetEndOfOperators()
      ;  ++iter
      )
   {
      Nb product = C_1;
      for(int ifunc = 0; ifunc < iter->first.first.size(); ++ifunc)
      {
         auto& variable = iter->first.first[ifunc];
         auto& func = iter->first.second[ifunc];
         GenericFunctionWrapper<Nb> generic_function(func, function_container);
         
         In q_index = -1;
         bool found = false;
         for(In iidx = 0; iidx < modenames.size() ; ++iidx)
         {
            if(variable == modenames[iidx])
            {
               q_index = iidx;
               found = true;
            }
         }
         if(!found)
         {
            MIDASERROR("Modename '" + variable + "' not found.");
         }

         product *= generic_function.EvaluateFunction(std::vector<Nb>{q[q_index]});
      }

      // Multiply coefficient
      product *= iter->second;

      // Then add to result
      value += product;
   }

   return value;
}

/**
 * Evaluate first derivatives of file potential in cartesian xyz space.
 *
 * @param aName    The name of the operator/property to evaluate.
 * @param arGeom   The geometry to evaluate for.
 * 
 * @return    Returns the first derivatives.
 **/
MidasVector FilePot::EvalDerImpl
   (  const std::string& aName
   ,  const std::vector<Nuclei>& arGeom
   )  const
{
   MidasVector result;
   
   MIDASERROR("NOT IMPLEMENTED");

   return result;
}

/**
 * Evaluate second derivatives of file potential in cartesian xyz space.
 *
 * @param aName    The name of the operator/property to evaluate.
 * @param arGeom   The geometry to evaluate for.
 * 
 * @return    Returns the second derivatives.
 **/
MidasMatrix FilePot::EvalHessImpl
   (  const std::string& aName
   ,  const std::vector<Nuclei>& arGeom
   )  const
{
   MidasMatrix result;
   
   MIDASERROR("NOT IMPLEMENTED");

   return result;
}

/**
 * Constructor
 *
 * @param aInfo   Information on filepot.
 **/
FilePot::FilePot
   (  const ModelPotInfo& aInfo
   )
   :  ModelPot()
   ,  mMolecule
      ( molecule::MoleculeFileInfo
         {  midas::input::SearchForFile(
               const_cast<ModelPotInfo&>(aInfo)["MOLECULEFILE"]
            )
         ,  "MIDAS"
         }
      )
   ,  mOperator
      (  midas::input::SearchForFile(
            const_cast<ModelPotInfo&>(aInfo)["OPERATORFILE"]
         )
      )
{
   mOperator.ReadOperator(midas::input::SearchForFile(const_cast<ModelPotInfo&>(aInfo)["OPERATORFILE"]));
}
