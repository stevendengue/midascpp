#ifndef MIDAS_POTENTIALS_DYNLIBPOT_H_INCLUDED
#define MIDAS_POTENTIALS_DYNLIBPOT_H_INCLUDED

#include "potentials/ModelPot.h"

#include "include/midaspot/Dll.h"

/**
 * Class to hold a dynamic library potential.
 **/
class DynLibPot
   :  public ModelPot
{  
   private:
      //! Surface member for holding the dynamically loaded surface library.
      surface mSurface;
   
      //! Load structure into cartesian interface struct.
      void LoadCartesian
         (  const std::vector<Nuclei>& aStructure
         ,  cartesian& aCart
         )  const;

      //! Overloadable function to evaluate the potential.
      virtual Nb EvalPotImpl(const std::string& aName, const std::vector<Nuclei>& aStructure) const;

      //! Overloadable function to evaluate the first derivative of the potential.
      virtual MidasVector EvalDerImpl(const std::string& aName, const std::vector<Nuclei>& aStructure) const;

      //! Overloadable function to evaluate the second derivate of the potential.
      virtual MidasMatrix EvalHessImpl(const std::string& aName, const std::vector<Nuclei>& aStructure) const;

   public:
      //! Constructor from ModelPotInfo.
      DynLibPot(const ModelPotInfo&);           
      
      //! Destructor
      virtual ~DynLibPot();
      
      //! Get the type of modelpot
      std::string Type() const { return "DYNLIBPOT"; }
};

#endif /* MIDAS_POTENTIALS_DYNLIBPOT_H_INCLUDED */
