/**
************************************************************************
*
*  @file                WaterTestPot.h
* 
*  Created:             01/10/2007
* 
*  Author:              D. Toffoli (toffoli@chem.au.dk)
* 
*  Short Description:   Declares class NasaModelPot for storing the
*                       Partridge-Schwenke potential for water
* 
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

#ifndef NEWWATERPOT_H
#define NEWWATERPOT_H

// std headers
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cctype>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "nuclei/Nuclei.h"
#include "util/Io.h"

class TestModelPot
{
   private:
      bool mHasBeenInit;
   public:
      TestModelPot();                                          ///< Default constructor
      ~TestModelPot();                                         ///< Deconstructor
      void InitConsts();                                       ///< Initialize constants
      void InitArrays();                                       ///< Initialize arrays
      void Init();                                             ///< Initialize 
      void Pot(MidasMatrix& r1, MidasMatrix&  dr1, Nb& e1);    ///< Compute the Partridge-Schwenke potential
      Nb EvalPot(vector<Nuclei>& arGeom);                      ///< Evaluate the potential for a given geometry
      friend ostream& operator<<(ostream& arOut, const TestModelPot& arPot);   ///< Output overloading
};

#endif

