/**
************************************************************************
*
*  @file                OperTermContForGenericBasePot.h
* 
*  Created:             01-01-2013
* 
*  Author:              Bo Thomsen (bothomsen@chem.au.dk)
* 
*  Short Description:   Operator term for the generic base pot
* 
*  Last modified:      
* 
*  Copyright:
* 
*  Ove Christiansen, Aarhus University.
*  The code may only be used and/or copied with the written permission
*  of the author or in accordance with the terms and conditions under
*  which the program was supplied.  The code is provided "as is"
*  without any expressed or implied warranty.
*
*************************************************************************
* */

#ifndef OPERTERMCONTFORGENERICBASEPOT_H_INCLUDED
#define OPERTERMCONTFORGENERICBASEPOT_H_INCLUDED

// std headers
#include <map>
#include <vector>
#include <string>
#include <fstream>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"

/***************************************************************************//**
 * @brief
 *    Class containing all operator terms for a specific ModeCombi.
 *
 * It contains the modes of its ModeCombi, as well as the operator terms for
 * that ModeCombi. It has a sum-over-product structure, meaning that each term
 * consists of 
 *    - an associated (linear) coefficient 
 *    - one (one-mode-)function for each mode in the inherent ModeCombi
 ******************************************************************************/
template< template<class T> class U // Function wrapper type
        , class V                   // input/output type for function
        >
class OperTermContForGenericBasePot
{
   protected:
      //! The modes of the inherent ModeCombi.
      std::vector<In>                   mModes;

      //! Linear coefficients, one for each term.
      std::vector<Nb>                   mCoef;

      //! 1-mode functions; a vec of terms, each term a vec of 1-mode funcs.
      std::vector<std::vector<U<V>* > > mFunctions;
      
      ///>
      OperTermContForGenericBasePot() = delete;

   public:
      /**
       *
       **/
      OperTermContForGenericBasePot
         ( const std::vector<In>& aSetOfModes
         ) 
         : mModes(aSetOfModes) 
      {
      }
      
      /**
       *
       **/
      virtual ~OperTermContForGenericBasePot
         (
         )
      {
      }
      
      ///>
      void InsertTerm(Nb, const std::vector<U<V>* >&);

      ///>
      V EvaluateTerms(const MidasVector&, const MidasVector&) const;
      
      ///>
      V EvaluateTerms(const MidasVector&) const;
      
      ///>
      bool IsInModes(const std::vector<In>&) const;
      
      //! Whether the given ModeCombi vector is equal to this object's one.
      bool IsMC(const std::vector<In>&) const;
      
      //!
      const std::vector<In>& GetModes() {return mModes;}

      //! The number of terms.
      Uin NumTerms() const;

      //! Return coefficient for term with given index.
      Nb CoefficientOfTerm(Uin) const;

      //! Return (copies of) one-mode functions for term with given index.
      std::vector<U<V>> FunctionsOfTerm(Uin) const;
};

//! Extract coefficients into vector.
template< template<class T> class U , class V >
std::vector<Nb> Coefficients(const OperTermContForGenericBasePot<U,V>&);

//! Extract function products into vector.
template< template<class T> class U , class V >
std::vector<std::vector<U<V>>> Functions(const OperTermContForGenericBasePot<U,V>&);


#include "potentials/OperTermContForGenericBasePot_Impl.h"

#endif /* OPERTERMCONTFORGENERICBASEPOT_H_INCLUDED */
