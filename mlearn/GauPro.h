/**
************************************************************************
* 
* @file                
*
* Created:            22-03-2017         
*
* Author:             Gunnar Schmitz (gunnar.schmitz@chem.au.dk)  
*
* Short Description:  Class for doing a Gaussian process. 
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef GAUPRO_INCLUDED
#define GAUPRO_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <ctime>
#include <algorithm>
#include <numeric>

#include "util/InterfaceOpenMP.h"

#include "mlearn/GPKernel.h"
#include "mlearn/MLDescriptor.h"
#include "mlearn/MeanFunctions.h"
#include "mlearn/sampling.h"

#include "geoopt/geoutil.h"  // GS: I don't like to include it here

#include "util/CallStatisticsHandler.h"

#include "util/RandomNumberGenerator.h"

template <class T>
class GauPro
{
   public:

      enum CovAlgo { USELU, USECHOL, USESVD, USECONJGRAD, USEBKD, USESPARSE };

      
      const map<CovAlgo,std::string> map_covalgo =
      {
         {USELU,"LU decompostion"}                    ,
         {USECHOL,"Cholesky decompostion"}            ,
         {USESVD,"SVD"}                               ,
         {USECONJGRAD,"Conjugate Gradient"}           ,
         {USEBKD,"Bunch-Kaufman decompostion"}        ,
         {USESPARSE,"Sparse Approximation"}           
      };

   private:

      T one  = static_cast<T>(1.0); 
      T zero = static_cast<T>(0.0);
      T mone = static_cast<T>(-1.0); 

      // often used stuff for LAPACK routines
      char nc = 'N';
      char nt = 'T';
      char uplo = 'L';

      In mNumLayer = 1;                     ///> Numbers of Layers for Multi-Layer GPR
      In mActLayer = 1;                     ///> Numbers of active Layers for Multi-Layer GPR

      std::vector<MLDescriptor<T>> mXdata;       ///> Training data (descriptor)  
      std::vector<std::vector<T>> mYdata;        ///> Training data (property)
      std::vector<std::vector<T>> mNoise;        ///> Noise for the training data
      std::vector<std::vector<T>> mInputNoise;   ///> Noise for the training data
      In mNumData    = 0;                        ///> Number of training points
      In mSizeSubSet = 0;                        ///> Size of the sub set
      T mShift = 0.0;                            ///> Shift for the Ydata
      bool mAdaptNoise = false;                  ///> Use heteroscedastic GPR

      bool mScaleSigma2 = false;
  
      std::vector<T> mLogDetApprox;         ///> Approximation to log |K| only computed for approximated GPR methods 

      T mMaxMem = 16.0 * 1024 * 1024 * 1024; ///> How much memory can be used?
      T mMemUsed = 0;

      std::vector<std::vector<T>> mXvalid;  ///> Validation data (descriptor) Only required when Cross Validation is used
      std::vector<T> mYvalid;               ///> Validation data (property) Only required when Cross Validation is used  

      std::vector<GPKernel<T>> mKernel;     ///> Used Kernelfunction to specify the Gaussian Process
      GPMeanFunction<T>* mMean;             ///> Average value of the function
      bool mOwnMean = false;                ///> So that we can free the pointer, if we created it. I could not use smart pointers in this case :(

      // All stuff related to the covariance matrix
      CovAlgo mCovAlgo = USELU;        ///> Which alogrithm is used to decompose the matrix?
      std::vector<std::unique_ptr<T[]>> mCov;       ///> Space for the co-variance matrix. Needed by any algorithm
      std::vector<size_t> mCovSize;                 ///> Size of the co-variance matrix. Needed to avoid reallocation
      std::vector<std::unique_ptr<T[]>> mSval;      ///> Space for the singular values. Needed if mCovAlgo == USESVD
      std::vector<std::unique_ptr<int[]>> mIpiv;    ///> Space for book keeping of row exchanges. Needed if mCovAlgo == USELU
      std::vector<bool> mHaveCov;                   ///> Internal boolean do avoid that the co-variance matrix is computed if not needed
      std::vector<std::vector<T>> mTrialVector;     ///> trial vector if conjugate gradient method is used
      std::vector<std::vector<T>> mSolutVector;     ///> soultion vector if conjugate gradient method is used
      std::vector<std::vector<T>> mResVector;       ///> residual vector if conjugate gradient method is used
      std::vector<std::vector<T>> mSigVector;       

      std::vector<std::unique_ptr<T[]>> mKmm;       ///> Intermediate used for Sparse GPR
      std::vector<std::unique_ptr<T[]>> mKmn;       ///> Intermediate used for Sparse GPR
      std::vector<std::unique_ptr<T[]>> mbarKmn;    ///> Intermediate used for Sparse GPR
      std::vector<size_t> mKmmSize;
      std::vector<size_t> mKmnSize;
      std::vector<size_t> mbarKmnSize;
      //--------------------------------------------

      std::vector<std::vector<T>> mHparam;
      
      std::vector<std::unique_ptr<T[]>> mKinv;                ///> Used in hyper parameter optimization
      std::vector<std::vector<std::unique_ptr<T[]>>> mBmat;   ///> Used in hyper parameter optimization
      std::vector<std::vector<std::unique_ptr<T[]>>> mCmat;   ///> Used in hyper parameter optimization

      std::vector<size_t> mKinvSize;         ///> Used in hyper parameter optimization
      std::vector<size_t> mBmatSize;         ///> Used in hyper parameter optimization
      std::vector<size_t> mCmatSize;         ///> Used in hyper parameter optimization

      //************************** private methods ************************************

      /**
       *  @brief Performs a line search to find the best step length for the hyper parameter optimization 
       **/
      void LineSearch
         (  std::vector<T>& delta
         ,  const std::vector<T>& cur_param
         ,  const std::vector<T>& cur_grad
         ,  const In& aILayer 
         )
      {
         T ax = 0.0;
         T xx = 1.0;
         T cx = 0.0;

         bracket(ax,xx,cx,cur_param,delta,cur_grad,aILayer);
         T xmin = brent(ax,xx,cx,cur_param,delta,cur_grad,aILayer);

         for (In idx = 0; idx < delta.size(); idx++)
         {
            delta[idx] *= xmin;
         }
      }

      void shft3(T &a, T &b, T &c, const T d)
      {
         a = b;
         b = c;
         c = d;
      }

      void bracket
         (  T& ax
         ,  T& bx
         ,  T& cx
         ,  const std::vector<T>& cur_param
         ,  const std::vector<T>& delta
         ,  const std::vector<T>& cur_grad
         ,  const In& aILayer 
         )
      {
         const T GOLD=1.618034,GLIMIT=100.0,TINY=1.0e-20;
         T fu;
         T fa = EvaluatePointOnLine(ax,cur_param,delta,cur_grad,aILayer);
         T fb = EvaluatePointOnLine(bx,cur_param,delta,cur_grad,aILayer);

         if (fb > fa) 
         {
            T tmp = ax;
            ax = bx;
            bx = tmp;

            tmp = fa;
            fa = fb;
            fb = tmp;
         }
         cx = bx + GOLD * (bx-ax);
         T fc = EvaluatePointOnLine(cx,cur_param,delta,cur_grad,aILayer);
         while (fb > fc) 
         {
            T r=(bx-ax)*(fb-fc);
            T q=(bx-cx)*(fb-fa);
            T u=bx-((bx-cx)*q-(bx-ax)*r)/
               (2.0*SIGN(MAX(abs(q-r),TINY),q-r));
            T ulim=bx+GLIMIT*(cx-bx);
            if ((bx-u)*(u-cx) > 0.0) 
            {
               fu=EvaluatePointOnLine(u,cur_param,delta,cur_grad,aILayer);
               if (fu < fc) 
               {
                  ax=bx;
                  bx=u;
                  fa=fb;
                  fb=fu;
                  return;
               } 
               else if (fu > fb) 
               {
                  cx=u;
                  fc=fu;
                  return;
               }
               u=cx+GOLD*(cx-bx);
               fu=EvaluatePointOnLine(u,cur_param,delta,cur_grad,aILayer);
            } 
            else if ((cx-u)*(u-ulim) > 0.0) 
            {
               fu=EvaluatePointOnLine(u,cur_param,delta,cur_grad,aILayer);
               if (fu < fc) 
               {
                  shft3(bx,cx,u,u+GOLD*(u-cx));
                  shft3(fb,fc,fu,EvaluatePointOnLine(u,cur_param,delta,cur_grad,aILayer));
               }
            } 
            else if ((u-ulim)*(ulim-cx) >= 0.0) 
            {
               u=ulim;
               fu=EvaluatePointOnLine(u,cur_param,delta,cur_grad,aILayer);
            } 
            else 
            {
               u=cx+GOLD*(cx-bx);
               fu=EvaluatePointOnLine(u,cur_param,delta,cur_grad,aILayer);
            }
            shft3(ax,bx,cx,u);
            shft3(fa,fb,fc,fu);
         }
      }

      T EvaluatePointOnLine
         (  const T alpha
         ,  const std::vector<T>& cur_param
         ,  const std::vector<T>& delta
         ,  const std::vector<T>& cur_grad
         ,  const In& aILayer
         )
      {
         std::vector<T> hparam(cur_param.size());

         for (In idx = 0; idx < delta.size(); idx++)
         {
            hparam[idx] = cur_param[idx] + alpha * cur_grad[idx];
         }

         this->SetHyperParameters(hparam,aILayer);
         return -1.0 * GetMarginalLikelihood(aILayer);
      }

      T brent
         (  const T ax
         ,  const T bx
         ,  const T cx
         ,  const std::vector<T>& cur_param
         ,  const std::vector<T>& delta
         ,  const std::vector<T>& cur_grad
         ,  const In& aILayer
         ,  const T aTol = 3.0e-8
         )
      {
         const In ITMAX = 100;
         const T CGOLD = 0.3819660;
         const T ZEPS = std::numeric_limits<T>::epsilon()*1.0e-3;
         T a,b,d=0.0,etemp,fu,fv,fw,fx;
         T p,q,r,tol1,tol2,u,v,w,x,xm;
         T e=0.0;

         a = (ax < cx ? ax : cx);
         b = (ax > cx ? ax : cx);
         x=w=v=bx;
         fw=fv=fx=EvaluatePointOnLine(x,cur_param,delta,cur_grad,aILayer);
         for (In iter=0;iter<ITMAX;iter++) 
         {
            xm   = 0.5 * (a + b);
            tol2 = 2.0 * (tol1 = aTol * abs(x) + ZEPS);

            if (std::abs(x-xm) <= (tol2-0.5*(b-a))) 
            {
               //fmin=fx;
               return x;
            }
            if (std::abs(e) > tol1) 
            {
               r=(x-w)*(fx-fv);
               q=(x-v)*(fx-fw);
               p=(x-v)*q-(x-w)*r;
               q=2.0*(q-r);
               if (q > 0.0) p = -p;
               q=abs(q);
               etemp=e;
               e=d;
               if (abs(p) >= abs(0.5*q*etemp) || p <= q*(a-x)
                     || p >= q*(b-x))
                  d=CGOLD*(e=(x >= xm ? a-x : b-x));
               else 
               {
                  d=p/q;
                  u=x+d;
                  if (u-a < tol2 || b-u < tol2)
                     d=SIGN(tol1,xm-x);
               }
            } 
            else 
            {
               d=CGOLD*(e=(x >= xm ? a-x : b-x));
            }
            u=(abs(d) >= tol1 ? x+d : x+SIGN(tol1,d));
            fu=EvaluatePointOnLine(u,cur_param,delta,cur_grad,aILayer);
            if (fu <= fx) 
            {
               if (u >= x) a=x; else b=x;
               shft3(v,w,x,u);
               shft3(fv,fw,fx,fu);
            } 
            else 
            {
               if (u < x) a=u; else b=u;
               if (fu <= fw || w == x) 
               {
                  v=w;
                  w=u;
                  fv=fw;
                  fw=fu;
               } 
               else if (fu <= fv || v == x || v == w) 
               {
                  v=u;
                  fv=fu;
               }
            }
         }
         throw("Too many iterations in brent");
      }
 
 
      /**
       *  @brief Optimization of Hyper parameters using Resilient Propagation.
       *
       *  Purpose: Optimizes the Hyper parameters using (improved) Resilient Backpropagation.
       *           It is a heuristic optimization algorithm, which just uses the sign of
       *           the gradient.
       *
       *  @param arVerbose boolean, which enforces that the algorithm prints out information         
       *  @param arOut specifies the output stream
       *  @param arMaxIter specifies the maximal number of iterations in the optimization.
       *  @param arGeps specifies the maximal gradient norm.
       *  @param arFeps specifies the maximal change in the change of the marginal likelihood.
       *  @param aILayer is the layer for which the hyper parameters should be computed
       *
       **/
      void RpropOpt
         (  const bool& arVerbose 
         ,  ostream& arOut 
         ,  const In& arMaxIter 
         ,  const T& arGeps 
         ,  const T& arFeps 
         ,  const In& aILayer
         ) 
      {

         const bool locdbg = false; 
         const bool use_iprop = true;
         const bool use_best = false;
         const bool use_linesearch = false;
         const bool use_rmsd = true;

         if (arVerbose)
         {
            std::string optalgo = "Rprop";
            if (use_iprop) optalgo = "iRprop";

            arOut  << "  Optimization Algorithm:       " << optalgo << std::endl;
         }

         int ndim = mHparam[aILayer].size();   
         int niter = 0;

         T precision = arGeps;
         T gradnrm = 10.0;
         
         std::vector<T> cur_param; 
         cur_param = mHparam[aILayer];

         std::vector<T> best_param;
         T best_grad_norm = 10e+99;

         bool converged = false;

         std::unique_ptr<T[]> wei(new T[mNumData]);

         for (int irun = 1; irun <= 1; irun++)  // Loop left for experiments with restarts
         {

            niter = 0;

            T Delta0   = 0.1;
            T Deltamin = 1e-6;
            //T Deltamin = 1e-18;
            T Deltamax = 50.0;
            T etaminus = 0.5;
            T etaplus  = 1.2;

            std::vector<T> cur_grad(ndim);  
            std::vector<T> old_grad(ndim,C_0);  
            std::vector<T> Delta(ndim,Delta0);  

            // only needed for iRprop (improved Rprop)
            T fval_cur;
            T fval_old;

            best_param = cur_param;

            this->SetHyperParameters(cur_param,aILayer);

            // get current gradient
            gradnrm    = 0.0;
            int istart = 0;

            //--------------------------------------------------------+
            // Pre optimization
            //--------------------------------------------------------+
            In npre = 1; //15
            if (arVerbose)
            {
               arOut << std::endl << " Preoptimization: " << std::endl 
                                  << " -----------------" << std::endl << std::endl;
            }

            T stepsize = C_1;
            std::vector<T> old_param;

            for (In ipre = 0; ipre < npre; ipre++)
            {

               // Calculate Co-variance matrix
               SetupCovarianceMatrix(aILayer,true);

               // calculate w = K^{-1} f(x) 
               GetWeights(wei, true, aILayer);

               // get hyper parameter gradient 
               cur_grad = GetHderiv(ndim,wei,aILayer);
               for (int idx = istart; idx < ndim; idx++)
               {
                  cur_grad[idx] = - cur_grad[idx];
                  gradnrm += cur_grad[idx] * cur_grad[idx]; 
               }
               if (use_rmsd)
               {
                  gradnrm = std::sqrt(gradnrm / ndim);
               }
               else
               { 
                  gradnrm = std::sqrt(gradnrm);
               }

               // Determine step 
               if (!use_linesearch) 
               {
                  // Calculate stepsize 
                  if (ipre > 0) 
                  {
                     std::vector<T> dparam(ndim);
                     std::vector<T> dgrad(ndim);

                     std::transform(cur_param.begin(),cur_param.end(),old_param.begin(),dparam.begin(),std::minus<T>());
                     std::transform(cur_grad.begin(),cur_grad.end(),old_grad.begin(),dgrad.begin(),std::minus<T>());

                     T dgrad_norm = std::abs(std::inner_product(std::begin(dgrad), std::end(dgrad), std::begin(dgrad), 0.0))  ;
                     T dpdg = std::abs(std::inner_product(std::begin(dparam), std::end(dparam), std::begin(dgrad), 0.0));

                     stepsize = dpdg/dgrad_norm;
                  }
 
                  for (int idx = 0; idx < ndim; idx++)
                  {
                     if (ipre == 0) 
                     {
                        Delta[idx] = std::abs(cur_grad[idx]) / gradnrm * 0.5;
                     }
                     else
                     {
                        Delta[idx] = -cur_grad[idx] * stepsize;
                     }
                  }
               }
               else
               {
                  Delta = cur_grad;

                  LineSearch
                     (  Delta
                     ,  cur_param
                     ,  cur_grad
                     ,  aILayer
                     );
               }

               // Update parameter
               old_param = cur_param;
               for (int idx = 0; idx < ndim; idx++)
               {
                  cur_param[idx] += Delta[idx];
               }

               // set current hyper parameters
               this->SetHyperParameters(cur_param,aILayer);

               if (arVerbose) arOut << "  " << std::setw(4)  << ipre  << " ||Grad|| " << gradnrm << std::endl;
            }
            arOut << std::endl;

            // get current value of marginal likelihood
            if (use_iprop)
            {
               fval_cur = -1.0 * GetMarginalLikelihood(aILayer);
            }

            if (locdbg) arOut << "gradnrm " << gradnrm << std::endl;

            best_grad_norm = gradnrm;

            while ( !converged  && niter < arMaxIter )
            {
     
               if (use_iprop)
               {
                  fval_old = fval_cur;
                  fval_cur = -1.0 * GetMarginalLikelihood(aILayer);
               }

               T sign = 1.0;
               // perform step
               for (int idx = 0; idx < ndim; idx++)
               {
                  if (cur_grad[idx] * old_grad[idx] > 0.0)
                  {
                     Delta[idx] = std::min(Delta[idx] * etaplus, Deltamax);

                     sign = (cur_grad[idx] > 0.0) ? 1.0 : ((cur_grad[idx] < 0.0) ? -1.0 : 0.0);

                     if (cur_param[idx] - sign * Delta[idx] > 0.0)
                     {
                        cur_param[idx] += -sign * Delta[idx];                      
                     }
                     else
                     {
                        // set gradient to zero 
                        cur_grad[idx] = 0.0;
                     }

                  }
                  else if (cur_grad[idx] * old_grad[idx] < 0.0) 
                  {  
                     sign = (old_grad[idx] > 0.0) ? 1.0 : ((old_grad[idx] < 0.0) ? -1.0 : 0.0);

                     // revert old step
                     if (use_iprop)
                     {
                        // for the improved Rprop do it only if marginal likelihood increased
                        if (fval_cur > fval_old)
                        {
                           cur_param[idx] += sign * Delta[idx];                      
                        }
                     }
                     else
                     {
                        // allways do it for the original Rprop
                        cur_param[idx] += sign * Delta[idx];                      
                     }

                     // and adjust parameters
                     Delta[idx] = std::max(Delta[idx] * etaminus, Deltamin);

                     // set gradient to zero 
                     cur_grad[idx] = 0.0;
                  }
                  else
                  {
                     sign = (cur_grad[idx] > 0.0) ? 1.0 : ((cur_grad[idx] < 0.0) ? -1.0 : 0.0);

                     cur_param[idx] += -sign * Delta[idx];
                  }

               }
               old_grad = cur_grad;

               // set current hyper parameters
               this->SetHyperParameters(cur_param,aILayer);

               // Re-Calculate Covariance Matrix K 
               SetupCovarianceMatrix(aILayer,true);

               // calculate w = K^{-1} f(x) 
               GetWeights(wei, true, aILayer);

               // get new gradient
               cur_grad = GetHderiv(ndim,wei,aILayer);
               for (int idx = istart; idx < ndim; idx++)
               {
                  cur_grad[idx] = - cur_grad[idx]; 
               }
               
               // calculate grad norm
               gradnrm = 0.0; 
               for (int idx = 0; idx < ndim; idx++)
               {
                  gradnrm += cur_grad[idx]*cur_grad[idx];
               }
               if (use_rmsd) 
               {
                  gradnrm = std::sqrt(gradnrm / ndim);
               }
               else
               {
                  gradnrm = std::sqrt(gradnrm);
               }

               // keep track on best parameters so far
               if (best_grad_norm > gradnrm)
               {
                  best_param = cur_param;
                  best_grad_norm = gradnrm;
               }

               if (locdbg) arOut << "iter " << niter << " |Grad| " <<  gradnrm  << std::endl;

               if (arVerbose) 
               {
                  T loglik = 0.0;
                  if (use_iprop)
                  {
                     loglik = -1.0 * fval_cur;
                  }
                  else
                  {
                     loglik = GetMarginalLikelihood(aILayer);
                  }

                  arOut << "iter " << std::setw(4) << niter << " |Grad| " <<  gradnrm << " " << loglik << std::endl;
                  arOut << "---------------------------------------------------------------------------" << std::endl;
                  arOut << " Index         Parameter               Gradient                Delta" << std::endl << std::endl;

                  for (int ihp = 0; ihp < cur_param.size(); ihp++)
                  {
                     arOut << "  " << std::setw(4)  << ihp << " " 
                                   << (cur_param[ihp] >= 0 ? " ":"") << std::setw(10) << cur_param[ihp] << " " 
                                   << (cur_grad[ihp]  >= 0 ? " ":"") << std::setw(10) << cur_grad[ihp]  << " " 
                                   << (Delta[ihp]     >= 0 ? " ":"") << std::setw(10) << Delta[ihp] << std::endl;
                  }
                  arOut << std::endl;

               }

               niter++;

               if (std::isnan(gradnrm)) MIDASERROR("Detected NaN in hyper parameter gradient!");

               converged = gradnrm < precision;
               if (use_iprop) converged = converged && std::abs( (fval_cur - fval_old) / fval_old) < arFeps;
            }
   
         }

         if (arVerbose)
         {
            arOut << "  Is optimization converged:   " << converged << std::endl;
            arOut << "  Number of iterations:        " << niter     << std::endl;
  
            arOut << "  New Hyper parameters:        "  << std::endl;
            for (int iparam = 0; iparam < cur_param.size(); iparam++)
            {
               arOut << "     " << iparam << " " << cur_param[iparam] << std::endl;
            }

            arOut << "  New marginal likelihood:     " << GetMarginalLikelihood(aILayer) << std::endl;
            arOut << "  Final gradient norm:         " << gradnrm << std::endl;
            arOut << std::endl << "+-------------End optimizing the hyper parameters ------------+" << std::endl << std::endl;
         }

         this->SetHyperParameters(cur_param,aILayer);

         if (use_best) 
         {
            arOut << " I will use the best parameters obtained so far" << std::endl;
            mHparam[aILayer] = best_param;
         }

      }

      void SimplexOpt
         (  const bool& arVerbose
         ,  ostream& arOut 
         ,  const In& arMaxIter
         ,  const In& aILayer
         )
      {
         //------------------------------------------------------------------+
         // Optimize hyper parameters with simplex algorithm when no
         // gradient for the hyper parameters are available
         //------------------------------------------------------------------+

         if (arVerbose) 
         {
            arOut << "  Optimization Algorithm:       " << "Simplex down-hill" << std::endl;
         }

         bool converged = false;                   // logical to indicate if simplex optimization converged
         int iter = 0;                             // how many iterations were used in the optimization?
         int mxsmplx = arMaxIter;                  // maximal number of iterations in simplex algorithm
         int ndim = mHparam[aILayer].size();   

         T ftol  = 1.0e-6;                         
         T ftol2 = 1.0e-20;                
         
         std::vector<T> funcval(ndim + 1);
         std::vector< std::vector<T>> point(ndim + 1, std::vector<T>(ndim) );


         bool iterate = true;
         while (iterate)
         {
            iterate = false;

            // generate the other start vectors for simplex algorithm:
            // This is a crucial step. The initial simplex should not be too small
            for (int idx = 0; idx < ndim + 1; idx++)
            {
               for (int j = 0; j < ndim; j++)
               {
                  if (idx == -1)
                  {
                     point[idx][j] = mHparam[aILayer][j];
                  }
                  else
                  {
                     T xrnd = static_cast <T> (rand()) / static_cast <T> (RAND_MAX);

                     T xadd = -0.1 + xrnd * (0.1 + 0.1);

                     T newx = xadd + mHparam[aILayer][j];

                     point[idx][j] =  newx;
                  } 
               }
            }

            // calculate vector y -> input for simplexopt
            for (int k = 0; k < ndim + 1; k++)
            {
               funcval[k] = simplefunct_hyp(point[k],aILayer); 
            }

            // vary vectors with a simplex algorithm
            simplexopt(point, funcval, ndim, ftol, ftol2, mxsmplx, iter, converged,aILayer);

            iterate = (!converged) && iter < mxsmplx;



         }
         if (converged)
         {
            this->SetHyperParameters(point[0],aILayer);
         }

         if (arVerbose)
         {         
            arOut << "  Is optimization converged:   " << converged << std::endl;
            arOut << "  Number of iterations:        " << iter      << std::endl;
            arOut << "  New Hyper parameters:        " << point[0]  << std::endl;
            arOut << "  New marginal likelihood:     " << GetMarginalLikelihood(aILayer) << std::endl;
            arOut << std::endl << "+-------------End optimizing the hyper parameters ------------+" << std::endl << std::endl;
         }
      }


      /**
       *  @brief Cleans the correlations matrix from near singularities 
       *
       *  Purpose: Removes negative eigenvalues and therefore preserves
       *           positive definitiness. However should be used with caution        
       *
       * */
      void CleanCovarianceMatrix
         (  const In& aILayer 
         )
      {

         const bool locdbg = false;

         // Recreate Co-Variance matrix
         mCov[aILayer].reset(new T[mNumData*mNumData]);
         mCovSize[aILayer] = mNumData*mNumData;
         mKernel[aILayer].Compute(mCov[aILayer], mXdata, mXdata, mNoise[aILayer], 0, mNumData, mNumData, true);

         std::unique_ptr<T[]> eig(new T[mNumData]);
         std::unique_ptr<T[]> tmp1(new T[mNumData*mNumData]);
         std::unique_ptr<T[]> tmp2(new T[mNumData*mNumData]);

         std::copy(mCov[aILayer].get(), mCov[aILayer].get() + mNumData*mNumData, tmp1.get());

         char jobv  = 'V';
         int m = mNumData;
         int info = 0;
         int lwork = -1; 

         // first run to get proper lwork
         T dwork;
         T dum;
         midas::lapack_interface::syev(&jobv, &uplo, &m, tmp1.get(), &m, eig.get(), &dwork, &lwork, &info );
         lwork = int(dwork);

         std::unique_ptr<T[]> work(new T[lwork]);
         midas::lapack_interface::syev(&jobv, &uplo, &m, tmp1.get(), &m, eig.get(), work.get(), &lwork, &info );

         if (info != 0) Mout << "GPR: Problem in DYSEV" << std::endl;

         // Get trace of Matrix
         T trace = C_0;
         T tracecut = C_0;

         T eigplus = eig[0] * std::pow( (1.0 + std::sqrt(mNumData / 6.0)) /
                                        (1.0 - std::sqrt(mNumData / 6.0)) , 2.0);
         int imax = 0;

         if (locdbg) Mout << "eigplus " << eigplus << std::endl;

         for (int i = mNumData - 1; i >= 0 ; i--)
         {
            if (eig[i] < eigplus) 
            {
               eig[i] = C_0;
            }
         } 

         // multiply one eigenvector with the modified eigenvalues
         std::copy(tmp1.get(), tmp1.get() + mNumData*mNumData, tmp2.get());
         for (int j = 0; j < mNumData; ++j) 
         {
            for (int i = 0; i < mNumData; ++i)
            {
               tmp1[j*mNumData + i] *= eig[j];
            }
         }
     
         // Build now the output matrix 
         m = mNumData;

         midas::lapack_interface::gemm( &nc, &nc, &m, &m, &m, &one 
                                      , tmp1.get(), &m, tmp2.get(), &m 
                                      , &zero, mCov[aILayer].get(), &m );   
         
      }

      /**
       *  @brief Main driver for using conjugate gradient for Co-Variance matrix 
       *
       *  Purpose: Use of conjugate gradient to achieve better scaling for GPR. 
       *           Unfortunately it never worked efficiently due to a bad pre conditioner
       *
       * */
      void ConjGrad
         (  std::vector<T>& aSol
         ,  std::vector<T>& atrial
         ,  std::unique_ptr<T[]>& aMat
         ,  std::unique_ptr<T[]>& aRHS
         ,  T aTol
         ,  int aItMax
         ,  const bool& arVerbose
         ,  const In& aILayer
         )
      {

         const bool locdbg = false;

         int n1 = 1;
         int inc = 1;

         int nsize = atrial.size();

         // init solution vector (change this later to something more sane)
         for (int i = 0; i < nsize; i++)
         {
            aSol[i] = 0.0;
            //aSol[i] = aRHS[i] / std::max(aMat[i*nsize + i],1e-12);
         }

         std::unique_ptr<T[]> mat(new T[nsize*nsize]);
         std::unique_ptr<T[]> tmp(new T[nsize*nsize]);

         std::unique_ptr<T[]> dmat(new T[nsize*nsize]);
         std::unique_ptr<T[]> emat(new T[nsize*nsize]);
         std::unique_ptr<T[]> vec1(new T[nsize]);
         std::unique_ptr<T[]> vec2(new T[nsize]);

         // copy co-variance matrix
         std::copy(aMat.get(), aMat.get() + nsize*nsize, mat.get());
         
         // Sinkhorn-Knopp algorithm to find diagonal matrices D and E
         // 1.) init vectors
         for (int i = 0; i < nsize; i++)
         {
            vec1[i] = 1.0;
            vec2[i] = 1.0;
         }

         T r1 = C_0;
         T r2 = C_0;
         
         for (int iter = 0; iter < 1000; iter++)
         {
            // (d_1)_i = (A d_2)^{-1}_i
            // (d_2)_j = (A^T d_1)^{-1}_j

            std::copy(vec1.get(), vec1.get() + nsize, tmp.get());
            midas::lapack_interface::gemm( &nc, &nc, &nsize, &n1, &nsize, &one
                                         , aMat.get(), &nsize, tmp.get(), &nsize
                                         , &zero, vec1.get(), &nsize );

            std::copy(vec2.get(), vec2.get() + nsize, tmp.get());
            midas::lapack_interface::gemm( &nt, &nc, &nsize, &n1, &nsize, &one
                                         , aMat.get(), &nsize, tmp.get(), &nsize
                                         , &zero, vec2.get(), &nsize );

            for (int i = 0; i < nsize; i++)
            {
               vec1[i] = 1.0 / vec1[i];
               vec2[i] = 1.0 / vec2[i];
            }

            // D = diag(d1)
            // E = diag(d2)
            for (int i = 0; i < nsize*nsize; i++)
            {
               dmat[i] = 0.0;
               emat[i] = 0.0;
            }
            for (int i = 0; i < nsize; i++)
            {
               dmat[i*nsize + i] = vec1[i];
               emat[i*nsize + i] = vec2[i];
            }

            // B = D A E
            midas::lapack_interface::gemm( &nc, &nc, &nsize, &nsize, &nsize, &one
                                         , dmat.get(), &nsize, aMat.get(), &nsize
                                         , &zero, tmp.get(), &nsize );

            midas::lapack_interface::gemm( &nc, &nc, &nsize, &nsize, &nsize, &one
                                         , tmp.get(), &nsize, emat.get(), &nsize
                                         , &zero, mat.get(), &nsize );

            // r1 = max_i(B_i:)/min_i(B_i:)
            // r2 = max_j(B_:j)/min_j(B_:j)
            T minr1 = mat[0];
            T minr2 = mat[0];
            T maxr1 = mat[0];
            T maxr2 = mat[0];
            for (int i = 0; i < nsize; i++)
            {
               for (int j = 0; j < nsize; j++)
               {
                  minr1 = std::max(std::min(minr1, mat[j*nsize + i]),1e-18);
                  maxr1 = std::max(maxr1, mat[j*nsize + i]);

                  minr2 = std::max(std::min(minr2, mat[i*nsize + j]),1e-18);
                  maxr2 = std::max(maxr2, mat[i*nsize + j]);
               }
            }
            r1 = maxr1/minr1;
            r2 = maxr2/minr2;

            // Check r1 and r2
            if ( r1 < 1e-8 && r2 < 1e-8) break;
         }


         for (int i = 0; i < nsize; i++)
         {
            aRHS[i] *= dmat[i*nsize +i];
         }

         if (mSigVector.size() != mNumLayer) mSigVector.resize(mNumLayer);
         if (mResVector.size() != mNumLayer) mResVector.resize(mNumLayer);

         midas::lapack_interface::gemm( &nc, &nc, &nsize, &n1, &nsize, &one
                                      , mat.get(), &nsize, &aSol[0], &nsize
                                      , &zero, &mSigVector[aILayer][0], &nsize );

         for (int i = 0; i < nsize; i++)
         {
            mResVector[aILayer][i] = aRHS[i] - mSigVector[aILayer][i];
         }

         T res_norm2 = midas::lapack_interface::dot(&nsize, &mResVector[aILayer][0], &inc, &mResVector[aILayer][0], &inc);   
         T res_norm  = std::sqrt(res_norm2);
         const T  break_at = aTol * std::sqrt(midas::lapack_interface::dot(&nsize, aRHS.get(), &inc, aRHS.get(), &inc));  // Stop if |residual| < tol*|b|


         // init trial Vector with residual
         for (int i = 0; i < nsize; i++)
         {
            atrial[i] = mResVector[aILayer][i];
         }

         for (int iter = 0; iter < aItMax; iter++)
         {
            // Calculate K * t with t being the trial vector
            midas::lapack_interface::gemm( &nc, &nc, &nsize, &n1, &nsize, &one
                                         , mat.get(), &nsize, &atrial[0], &nsize
                                         , &zero, &mSigVector[aILayer][0], &nsize );

            // Calculate t * K * t
            T tKt = midas::lapack_interface::dot(&nsize, &atrial[0], &inc, &mSigVector[aILayer][0], &inc);   

            T  alpha  =  res_norm2 / tKt ;
            for (int i = 0; i < nsize; i++)
            {
               aSol[i]                += alpha * atrial[i];
               mResVector[aILayer][i] -= alpha * mSigVector[aILayer][i]; 
            }

            T beta = T(1.0) / res_norm2;

            res_norm2  = midas::lapack_interface::dot(&nsize, &mResVector[aILayer][0], &inc, &mResVector[aILayer][0], &inc); 
            res_norm   = std::sqrt(res_norm2); 

            if (locdbg || arVerbose) Mout << "||R|| " << res_norm << " iter " << iter << " break_at " << break_at << std::endl;

            if (res_norm <= break_at) break;
            beta *= res_norm2;

            for (int i = 0; i < nsize; i++)
            {
               atrial[i] = mResVector[aILayer][i] + beta*atrial[i];
            }

         }

         if (arVerbose) 
         {
            Mout << "Solution Vector " << aSol << std::endl;
         }

         for (int i = 0; i < nsize; i++)
         {
            aSol[i] /= emat[i*nsize + i];
            aRHS[i] /= emat[i*nsize + i];
         }

      }

      /**
       *  @brief Sets up covariance matrix and inverts it (LU decomposition) 
       *
       *  Purpose: Sets up covariance matrix and inverts it.
       *           In fact it does a LU or Cholesky decomposition
       *
       * */
      void SetupCovarianceMatrix
         (  const In& aILayer 
         ,  const bool& aDoInv = false
         )
      {
         const bool locdbg  = false;
         const bool prtCov  = false;
         const bool prteig  = false;

         //
         // Init number of Layers if necessary
         //
         if (mCov.size() != mNumLayer)
         {
            mCov.resize(mNumLayer);
            mCovSize.resize(mNumLayer,0);
         }

         //
         // Set data in multi layer case if necessary
         //
         In prevLayer = (aILayer == 0 ? 0 : aILayer - 1);
         if (mHaveCov[prevLayer] == false && aILayer > 0)
         {
            // In principle we should never get here, but to be on the save side
            this->SetDeltaData(aILayer);
         }

         if (mHaveCov[aILayer] == false)
         {
            // sanity check
            if (mNumData == 0)
            {
               MIDASERROR("No data given for GPR!");
            }
  
            if (mCovSize[aILayer] != mNumData*mNumData)
            {
               mCov[aILayer].reset(new T[mNumData*mNumData]);
               mCovSize[aILayer] = mNumData*mNumData;
            }
            if (mCovAlgo != USESPARSE) 
            {
               mKernel[aILayer].Compute(mCov[aILayer], mXdata, mXdata, mNoise[aILayer], 0, mNumData, mNumData, true, false, 1.0);
            }
            else
            {
               for (size_t i = 0; i < mNumData * mNumData; i++)
                  mCov[aILayer][i] = 0.0;
            }

            //-----------------------------------------------------------------+
            // Print some diagnostics for debugging
            //-----------------------------------------------------------------+

            if (prtCov)
            {
               Mout << "Co-variance matrix. For Layer " << aILayer << std::endl;
               prtmat(mCov[aILayer],mNumData,mNumData);

               savemat(mCov[aILayer],mNumData);
            }

            if (prteig)
            {
               std::unique_ptr<T[]> eig(new T[mNumData]);
               std::unique_ptr<T[]> tmp(new T[mNumData*mNumData]);
               std::copy(mCov[aILayer].get(), mCov[aILayer].get() + mNumData*mNumData, tmp.get());

               char jobz = 'V';
               int m = mNumData;
               int info = 0;
               int lwork = -1; 

               // first run to get proper lwork
               T dwork;
               T dum;
               midas::lapack_interface::syev( &jobz, &uplo, &m, &dum, &m
                                            , &dum, &dwork, &lwork, &info );
               lwork = int(dwork);

               std::unique_ptr<T[]> work(new T[lwork]);

               midas::lapack_interface::syev( &jobz, &uplo, &m, tmp.get(), &m
                                            , eig.get(), work.get(), &lwork, &info );


               for (int idx = 0; idx < mNumData; idx++)
               {
                  Mout << "eig " << idx << " " << eig[idx] << std::endl;
               }

            }

            //-----------------------------------------------------------------+
            // Actually do what this routine is supposed to do
            //-----------------------------------------------------------------+

            switch(mCovAlgo)
            {

               case USELU :
               {
                  LOGCALL("LU CoVar");

                  if (mIpiv.size() != mNumLayer) mIpiv.resize(mNumLayer);

                  mIpiv[aILayer].reset(new int[mNumData]);
      
                  //-------------------------------------------------------------+
                  // Do LU decomposition of K
                  //-------------------------------------------------------------+
                  int m = mNumData;
                  int nrhs = 1;
                  int info = 0;
     
                  // Get LU decompostion and save it on cov
                  if (locdbg) Mout << "Do LU decomposition" << std::endl; 
                  midas::lapack_interface::getrf(&m, &m, mCov[aILayer].get(), &m, mIpiv[aILayer].get(), &info);
      
                  // some sanity checks
                  if (info < 0) Mout << "illegal value in dgetrf" << std::endl;
                  if (info > 0) Mout << "singular matrix in dgetrf" << std::endl;

                  if (aDoInv) 
                  {
                     char diag = 'n';

                     if (mKinv.size() != mNumLayer) mKinv.resize(mNumLayer);

                     if (!mKinv[aILayer] || mKinvSize[aILayer] != mNumData*mNumData)
                     {
                        mKinv[aILayer].reset(new T[mNumData*mNumData]);
                        mKinvSize[aILayer] = mNumData*mNumData;
                     }

                     int lwork = -1;
                     T dlen;

                     // Copy LU decomposition
                     std::copy(mCov[aILayer].get(), mCov[aILayer].get() + mNumData*mNumData, mKinv[aILayer].get()); 

                     // Get optimal work
                     midas::lapack_interface::getri(&m, mKinv[aILayer].get(), &m, mIpiv[aILayer].get(), &dlen, &lwork, &info);
                     lwork = int(dlen);
                     std::unique_ptr<T[]> work(new T[lwork]);

                     // Invert from LU decompostion
                     midas::lapack_interface::getri(&m, mKinv[aILayer].get(), &m, mIpiv[aILayer].get(), work.get(), &lwork, &info);
                 
                     if (info != 0) MIDASERROR("GPR: Problem in getri");

                  }

                  mHaveCov[aILayer] = true;
                  break;
               }
               case USECHOL :
               {
                  LOGCALL("Chol CoVar");

                  const bool use_faile_safe = true;

                  //-------------------------------------------------------------+
                  // Do Cholesky decomposition of K
                  //-------------------------------------------------------------+
                  int m = mNumData;
                  int info = 0;

                  LOGCALL("GPCHOL");
                  {
                     LOGCALL("dpotrf");
                     midas::lapack_interface::potrf(&uplo, &m, mCov[aILayer].get(), &m, &info);
                  }

                  if (info != 0 && use_faile_safe)
                  {
                     // Try to clean the co-variance matrix (expensive step) in order try again to decompose it
                     CleanCovarianceMatrix(aILayer);

                     midas::lapack_interface::potrf(&uplo, &m, mCov[aILayer].get(), &m, &info);

                     if (info != 0) MIDASERROR("GPR: Problem in potrf even after cleaning");
                  }
                  else if (info != 0) 
                  {
                     Mout << "info = " << info << std::endl;
                     MIDASERROR("GPR: Problem in potrf");
                  }

                  if (aDoInv) 
                  {
                     LOGCALL("Chol Inv");

                     char diag = 'n';

                     if (mKinv.size() != mNumLayer) mKinv.resize(mNumLayer);

                     if (!mKinv[aILayer] || mKinvSize[aILayer] != mNumData*mNumData)
                     {
                        mKinv[aILayer].reset(new T[mNumData*mNumData]);
                        mKinvSize[aILayer] = mNumData*mNumData;
                     }

                     // Invert from Cholesky decompostion
                     std::copy(mCov[aILayer].get(), mCov[aILayer].get() + mNumData*mNumData, mKinv[aILayer].get()); 
                     midas::lapack_interface::potri(&uplo, &m, mKinv[aILayer].get(), &m, &info);
                 
                     // fill in not referenced parts of the matrix...
                     for (int jdx = 0; jdx < mNumData; jdx++)
                     {
                        for (int idx = 0; idx < jdx; idx++)
                        {
                           mKinv[aILayer][jdx*mNumData + idx] = mKinv[aILayer][idx*mNumData + jdx];
                        }
                     }

                     if (info != 0) MIDASERROR("GPR: Problem in potri");

                  }

                  mHaveCov[aILayer] = true;
                  break;
               }
               case USESVD :
               {
                  LOGCALL("SVD CoVar");

                  if (mSval.size() != mNumLayer) mSval.resize(mNumLayer);

                  mSval[aILayer].reset(new T[mNumData]);
                  
                  //-------------------------------------------------------------+
                  // Do SVD of K and calculate (pseudo)-Inverse
                  //-------------------------------------------------------------+
                  std::unique_ptr<T[]> sinv(new T[mNumData]);
                  T rcond = 1.e-10;

                  std::unique_ptr<T[]> tmp(new T[mNumData*mNumData]);
                  std::copy(mCov[aILayer].get(), mCov[aILayer].get() + mNumData*mNumData, tmp.get());

                  char jobu  = 'A';
                  char jobvt = 'A';
                  int m = mNumData;
                  int info = 0;
                  int lwork = -1; //std::max(std::max(10000, 4 * m), 5 * m);

                  // first run to get proper lwork
                  T dwork;
                  T dum;
                  midas::lapack_interface::gesvd(&jobu, &jobvt, &m, &m, &dum, &m
                                                , &dum, &dum, &m, &dum, &m
                                                , &dwork, &lwork, &info );
                  lwork = int(dwork);

                  std::unique_ptr<T[]> u(new T[m*m]);
                  std::unique_ptr<T[]> vt(new T[m*m]);
                  std::unique_ptr<T[]> work(new T[lwork]);

                  midas::lapack_interface::gesvd(&jobu, &jobvt, &m, &m, tmp.get(), &m
                                                , mSval[aILayer].get(), u.get(), &m, vt.get(), &m
                                                , work.get(), &lwork, &info );

                  if (info != 0) Mout << "GPR: Problem in GESVD" << std::endl;

                  T val = 0.0;
                  T sig;
                  for (int i = 0; i < mNumData; ++i) 
                  {
                     sig = mSval[aILayer][i];
                     if (sig > rcond)
                     {
                        val = 1.0 / sig;
                     }
                     else
                     {
                        val = 0.0;
                     }
                     sinv[i] = val;
                  }

                  // multiply one singularvector with the diagonal matrix of 1/singularvalues
                  for (int j = 0; j < mNumData; ++j) 
                  {
                     for (int i = 0; i < mNumData; ++i)
                     {
                        u[j*mNumData + i] *= sinv[j];
                     }
                  }
     
                  // Build now the output matrix 
                  {
                     int m = mNumData;


                     midas::lapack_interface::gemm( &nc, &nc, &m, &m, &m, &one 
                                                  , u.get(), &m, vt.get(), &m 
                                                  , &zero, mCov[aILayer].get(), &m );   
                  }

                  mHaveCov[aILayer] = true;
                  break;
               }
               case USECONJGRAD :
               {
                  if (mTrialVector.size() != mNumLayer)
                  {
                     mTrialVector.resize(mNumLayer);
                     mSolutVector.resize(mNumLayer);
                     mResVector.resize(mNumLayer);
                     mSigVector.resize(mNumLayer);
                  }

                  // Just reserve memory for algorithm
                  mTrialVector[aILayer].resize(mNumData);
                  mSolutVector[aILayer].resize(mNumData);
                  mResVector[aILayer].resize(mNumData);
                  mSigVector[aILayer].resize(mNumData);
                  break;
               }
               case USEBKD :
               {
                  LOGCALL("BKD");

                  if (mIpiv.size() != mNumLayer) mIpiv.resize(mNumLayer);

                  mIpiv[aILayer].reset(new int[mNumData]);

                  //-------------------------------------------------------------+
                  // Do Bunch-Kaufmann decomposition of K
                  //-------------------------------------------------------------+
                  int m = mNumData;
                  int info = 0;
                  int lwork = -1;
                  T dlen;

                  // Call to get optimal lwork
                  midas::lapack_interface::sytrf(&uplo, &m, mCov[aILayer].get(), &m, mIpiv[aILayer].get(), &dlen, &lwork,  &info);

                  lwork = int(dlen);
                  std::unique_ptr<T[]> work(new T[lwork]);

                  // Actual call 
                  midas::lapack_interface::sytrf(&uplo, &m, mCov[aILayer].get(), &m, mIpiv[aILayer].get(), work.get(), &lwork,  &info);

                  if (info != 0) 
                  {
                     Mout << "info = " << info << std::endl;
                     MIDASERROR("GPR: Problem in sytrf");
                  }

                  if (aDoInv) 
                  {
                     LOGCALL("BKD Inv");

                     if (mKinv.size() != mNumLayer) mKinv.resize(mNumLayer);

                     if (!mKinv[aILayer] || mKinvSize[aILayer] != mNumData*mNumData)
                     {
                        mKinv[aILayer].reset(new T[mNumData*mNumData]);
                        mKinvSize[aILayer] = mNumData*mNumData;
                     }

                     // Invert from Bunch Kaufmann decompostion
                     std::copy(mCov[aILayer].get(), mCov[aILayer].get() + mNumData*mNumData, mKinv[aILayer].get()); 
                     midas::lapack_interface::sytri(&uplo, &m, mKinv[aILayer].get(), &m, mIpiv[aILayer].get(), work.get(), &info);
      
                     // fill in not referenced parts of the matrix...
                     for (int jdx = 0; jdx < mNumData; jdx++)
                     {
                        for (int idx = 0; idx < jdx; idx++)
                        {
                           mKinv[aILayer][jdx*mNumData + idx] = mKinv[aILayer][idx*mNumData + jdx];
                        }
                     }

                     if (info != 0) MIDASERROR("GPR: Problem in sytri");

                  }

                  mHaveCov[aILayer] = true;
                  break;
               }
               case USESPARSE:
               {
                  LOGCALL("Sparse CoVar");

                  const bool use_chol = true;
                  const bool use_scal = false;

                  int inc = 1;
                  int n = mNumData;
                  int m = mSizeSubSet;
                  int info = 0;
                  char diag = 'N'; 

                  T alpha  = one;
                  T alpha1 = one;
                  if (use_scal) 
                  {
                     alpha  = 1.0;
                     alpha1 = one / alpha;
                  }

                  // Do we need to set everything up?
                  if (mKmmSize.size() != mNumLayer)    mKmmSize.resize(mNumLayer);
                  if (mKmnSize.size() != mNumLayer)    mKmnSize.resize(mNumLayer);
                  if (mbarKmnSize.size() != mNumLayer) mbarKmnSize.resize(mNumLayer);

                  if (mKmm.size() != mNumLayer) mKmm.resize(mNumLayer);
                  if (!mKmm[aILayer] || mKmmSize[aILayer] != m*m)
                  {
                     mKmm[aILayer].reset(new T[m*m]);
                     mKmmSize[aILayer] = m*m;
                  }

                  if (mKmn.size() != mNumLayer) mKmn.resize(mNumLayer);
                  if (!mKmn[aILayer] || mKmnSize[aILayer] != m*n)
                  {
                     mKmn[aILayer].reset(new T[m*n]);
                     mKmnSize[aILayer] = m*n;
                  }

                  if (mbarKmn.size() != mNumLayer) mbarKmn.resize(mNumLayer);
                  if (!mbarKmn[aILayer] || mbarKmnSize[aILayer] != m*n)
                  {
                     mbarKmn[aILayer].reset(new T[m*n]);
                     mbarKmnSize[aILayer] = m*n;
                  }

                  std::vector<T> no_noise(n,C_0);
                  std::vector<T> small_noise(n,1e-10);

                  //------------------------------------------------------------------------------------------------------------------+
                  //  
                  //  Outline
                  //  --------
                  //
                  //  Approx. co-variance matrix K_nn as
                  //
                  //    ~K_nn = K_nm K_mm^{-1} K_mn = \bar{K}_nm \bar{K}_mn
                  //
                  //    \bar{K}_mn = L_mm^{-1}  K_mn                          | L_mm Cholesky decomp. of K_mm
                  //
                  //
                  //  For its inverse including noise use 
                  //
                  //    ( ~K_nn + \sigma^2 I)^{-1} = \sigma^{-2} ( I - \bar{K}_nm (I\sigma^2 - \bar{K}_mn \bar{K}_nm)^{-1} \bar{K}_mn
                  //                               = \sigma^{-2} ( I - \bar{K}_nm B_mm^{-1} \bar{K}_mn
                  //                               = \sigma^{-2} ( I - \hat{K}_nm \hat{K}_mn}
                  //
                  //------------------------------------------------------------------------------------------------------------------+
   
                  // Calculate K_mm
                  mKernel[aILayer].Compute(mKmm[aILayer], mXdata, mXdata, small_noise, 0, m, m, true, use_scal, alpha1);

                  // Calculate K_nm
                  mKernel[aILayer].Compute(mKmn[aILayer], mXdata, mXdata, no_noise, 0, m, n, false, use_scal, alpha1);

                  // Cholesky decomposition on K_mm 
                  T LogDetKmm;
                  FormInverseSquareRoot(mKmm[aILayer],m,LogDetKmm,use_chol);

                  // Calculate bar{K}_mn += L_mm^{-1} * K_mn
                  midas::lapack_interface::gemm( &nc, &nc, &m, &n, &m, &one
                                               , mKmm[aILayer].get(), &m, mKmn[aILayer].get(), &m
                                               , &zero, mbarKmn[aILayer].get(), &m );

                  // Calculate B_mm = \bar{K}_nm * \bar{K}_mn + I \sigma^2
                  midas::lapack_interface::gemm( &nc, &nt, &m, &m, &n, &alpha
                                               , mbarKmn[aILayer].get(), &m, mbarKmn[aILayer].get(), &m
                                               , &zero, mKmm[aILayer].get(), &m );
                  
                  for (int idx = 0; idx < m; idx++)
                  {
                     mKmm[aILayer][idx*m + idx] += mNoise[aILayer][idx]; 
                  }
                  
                  // Decompose B_mm^{-1} matrix in a symmetric way
                  T LogDetBmm;
                  FormInverseSquareRoot(mKmm[aILayer],m,LogDetBmm,use_chol);

                  // Get approximation for log |K|
                  if (mLogDetApprox.size() != mNumLayer) mLogDetApprox.resize(mNumLayer, C_0);
                  mLogDetApprox[aILayer] = LogDetBmm;

                  // d) Build \hat{K}
                  T dfac = std::sqrt(alpha);
                  midas::lapack_interface::gemm( &nc, &nc, &m, &n, &m, &dfac
                                               , mKmm[aILayer].get(), &m, mbarKmn[aILayer].get(), &m
                                               , &zero, mCov[aILayer].get(), &m );

                  if (aDoInv) 
                  {
                     const bool locdbg = false;

                     if (mKinv.size() != mNumLayer) mKinv.resize(mNumLayer);

                     if (!mKinv[aILayer] || mKinvSize[aILayer] != mNumData*mNumData)
                     {
                        mKinv[aILayer].reset(new T[mNumData*mNumData]);
                        mKinvSize[aILayer] = mNumData*mNumData;
                     }

                     //
                     // K^{-1} = - 1/sigma^2 (\hat{K}_nm \hat{K}_mn - I )
                     //
                     
                     // make I
                     std::fill(mKinv[aILayer].get(), mKinv[aILayer].get() + n*n, C_0);
                     
                     for (int idx = 0; idx < n; idx++)
                     {
                        mKinv[aILayer][idx*n + idx] = one;
                     }

                     // \hat{K}_nm \hat{K}_mn - I
                     midas::lapack_interface::gemm( &nt, &nc, &n, &n, &m, &one
                                                  , mCov[aILayer].get(), &m, mCov[aILayer].get(), &m
                                                  , &mone, mKinv[aILayer].get(), &n );
                    
                     for (int idx = 0; idx < mNumData; idx++)
                     {
                        T dval = - one / mNoise[aILayer][idx];
                        for (int jdx = 0; jdx < mNumData; jdx++)
                        {
                           mKinv[aILayer][idx*n + jdx] *= dval; 
                        }
                     }

                     if (locdbg) 
                     {
                        std::unique_ptr<T[]> Knn(new T[n*n]);
                        std::unique_ptr<T[]> eye(new T[n*n]);

                        mKernel[aILayer].Compute(Knn, mXdata, mXdata, mNoise[aILayer], 0, n, n, true, use_scal, alpha1);
                        
                        midas::lapack_interface::gemm( &nc, &nc, &n, &n, &n, &one
                                                     , mKinv[aILayer].get(), &m, Knn.get(), &n
                                                     , &zero, eye.get(), &n );

                        Mout << "identity" << std::endl; 
                        prtmat(eye,n,n);
                     }

                  }

                  mHaveCov[aILayer] = true;
                  break;
               }
               default :
               {
                  MIDASERROR("Unknown algorithm to decompose covariance matrix!");
               }
            }
         }
      }

      /**
       *  @brief Calculates a first order correction for the weights. Expierence tells
       *         us that the influence is not so large. 
       *  
       **/
      void CorrectWeights
         (  std::unique_ptr<T[]>& aWei
         ,  const In& aILayer
         )
      {

         MIDASERROR("GPR: Correction of weights only tested using one layer!");

         // Calculate the covariance matrix 
         SetupCovarianceMatrix(aILayer);

         int m = mNumData;
         int nrhs = 1;
         int info = 0;

         bool addnoise = false;
         T noise = C_0; 
     
         std::vector<T> meanval(mNumData);

         // y = sum_k w_k K(x,x_k)
         for (Uin inew = 0; inew < mNumData; inew++)
         {
            int lderi = mXdata[inew].GetDerivativeOrder(); 
            meanval[inew] = mMean->Evaluate(mXdata[inew]);
            for (Uin idata = 0; idata < mNumData; idata++)
            {
               int lderj = mXdata[idata].GetDerivativeOrder(); 
               meanval[inew] +=  aWei[idata] * mKernel[aILayer].Compute(mXdata[inew], mXdata[idata], addnoise, noise, lderi, lderj);
            }
         }
                                                                        
         // fill in data in aWei (will be overwritten)
         std::unique_ptr<T[]> delta(new T[mNumData]);
         for (Uin idata = 0; idata < mNumData; idata++)
         {
            delta[idata] = mYdata[aILayer][idata] - meanval[idata] - mShift; 
         }

         // solve K d = r 
         switch(mCovAlgo)
         {
            case USELU :
            {     
               midas::lapack_interface::getrs(&nc, &m, &nrhs, mCov[aILayer].get(), &m, mIpiv[aILayer].get(), delta.get(), &m, &info);
               if (info != 0) Mout << "GPR: Problem in solving linear system (CorrectWeights)" << std::endl;
               break;
            }
            case USECHOL :
            {
               midas::lapack_interface::potrs(&uplo, &m, &nrhs, mCov[aILayer].get(), &m, delta.get(), &m, &info);
               if (info != 0) Mout << "GPR: Problem in solving linear system (CorrectWeights)" << std::endl;
               break;
            }
            case USESVD :
            {
               T* tmp = new T[mNumData];
               std::copy ( delta.get(), delta.get() + mNumData, tmp );
               {
                  int m = mNumData; 
      
                  midas::lapack_interface::gemm( &nc, &nc, &m, &nrhs, &m, &one 
                                               , mCov[aILayer].get(), &m, tmp, &m
                                               , &zero, delta.get(), &m );    
               }
               delete[] tmp;

               break; 
            }
            case USECONJGRAD :
            {
               this->ConjGrad(mSolutVector[aILayer], mTrialVector[aILayer], mCov[aILayer], delta, 1e-12, 1000, false, aILayer);
               for (Uin idata = 0; idata < mNumData; idata++)
               {
                  delta[idata] = mSolutVector[aILayer][idata];
               }
               break;
            }
            case USEBKD :
            {
               midas::lapack_interface::sytrs(&uplo, &m, &nrhs, mCov[aILayer].get(), &m, mIpiv[aILayer].get(), delta.get(), &m, &info);
               if (info != 0) Mout << "GPR: Problem in solving linear system (CorrectWeights)" << std::endl;
               break;
            }
            case USESPARSE:
            {
               SparseSol(delta,aILayer);
               break;
            }
            default:
            {
               MIDASERROR("Unknown algorithm to decompose covariance matrix!");
            }
         }

         // Add correction
         for (Uin idata = 0; idata < mNumData; idata++)
         {
            aWei[idata] += delta[idata];   
         }
      }

      /**
       *  @brief Calculates the weights for the updated mean function in the GPR. 
       *  
       **/
      void GetWeights
         (  std::unique_ptr<T[]>& aWei
         ,  const bool& aSubMean
         ,  const In& aILayer
         )
      {

         LOGCALL("GPWEIGHTS");

         // Calculate the covariance matrix 
         SetupCovarianceMatrix(aILayer);

         int m = mNumData;
         int nrhs = 1;
         int info = 0;

         // fill in data in aWei (will be overwritten)
         for (Uin idata = 0; idata < mNumData; idata++)
         {
            T dsub = (aILayer == 0 ?  mMean->Evaluate(mXdata[idata]) : C_0);
            if (!aSubMean) dsub = 0.0;
            aWei[idata] = mYdata[aILayer][idata] - dsub - mShift;
         }

         // solve K w = b 
         switch(mCovAlgo)
         {
            case USELU :
            {     
               midas::lapack_interface::getrs(&nc, &m, &nrhs, mCov[aILayer].get(), &m, mIpiv[aILayer].get(), aWei.get(), &m, &info);
               if (info != 0) Mout << "GPR: Problem in solving linear system (GetWeights)" << std::endl;
               break;
            }
            case USECHOL :
            {
               midas::lapack_interface::potrs(&uplo, &m, &nrhs, mCov[aILayer].get(), &m, aWei.get(), &m, &info);
               if (info != 0) Mout << "GPR: Problem in solving linear system (GetWeights)" << std::endl;

               break;
            }
            case USESVD :
            {
               T* tmp = new T[mNumData];
               std::copy ( aWei.get(), aWei.get() + mNumData, tmp );
               {
                  int m = mNumData; 
      
                  midas::lapack_interface::gemm( &nc, &nc, &m, &nrhs, &m, &one 
                                               , mCov[aILayer].get(), &m, tmp, &m
                                               , &zero, aWei.get(), &m );    
               }
               delete[] tmp;

               break; 
            }
            case USECONJGRAD :
            {
               this->ConjGrad(mSolutVector[aILayer], mTrialVector[aILayer], mCov[aILayer], aWei, 1e-12, 100000, true, aILayer);
               for (Uin idata = 0; idata < mNumData; idata++)
               {
                  aWei[idata] = mSolutVector[aILayer][idata];
               }
               break;
            }
            case USEBKD :
            {
               midas::lapack_interface::sytrs(&uplo, &m, &nrhs, mCov[aILayer].get(), &m, mIpiv[aILayer].get(), aWei.get(), &m, &info);
               if (info != 0) Mout << "GPR: Problem in solving linear system (GetWeights)" << std::endl;

               break;
            }
            case USESPARSE:
            {
               SparseSol(aWei,aILayer);
               break;
            }
            default:
            {
               MIDASERROR("Unknown algorithm to decompose covariance matrix!");
            }
         }

      }

      /**
       *  @brief Randomly assignes size indices to k boxes 
       *
       * */
      std::vector<int> kfold( const int& size, const int& k )
      {
        std::vector<int> indices(size);
      
        for (int i = 0; i < size; i++ )
        {
          indices[i] = i%k;
        }

        std::shuffle ( indices.begin(), indices.end(), midas::util::detail::get_mersenne() );
      
        return indices;
      }

      /**
       *  @brief Returns a error measure via Cross Validation 
       *
       * */
      T CrossValidate
         (  std::vector<T>& hparam
         ,  const In& aILayer
         )
      {
         // Save current set of hyper parameters
         std::vector<T> hparam_old = mHparam[aILayer];

         this->SetHyperParameters(hparam,aILayer);

         T error = CrossValidate();

         // reset of hyper parameters
         mHparam[aILayer] = hparam_old;

         return error;
      }

      T CrossValidateGrad( std::vector<T>& hparam
                         , int idx
                         )
      {

         T h = std::sqrt(std::numeric_limits<T>::epsilon()) * hparam[idx];

         std::vector<T> hparamp = hparam;
         std::vector<T> hparamm = hparam;

         hparamp[idx] += h;
         hparamm[idx] -= h;

         T grad = (this->CrossValidate(hparamp) - this->CrossValidate(hparamm)) / (2.0*h);

         return grad;
      }

      /**
       *  @brief Returns a error measure via Cross Validation 
       *
       * */
      T CrossValidate()
      {
         T error = C_0;

         std::tuple<T, T> gpout = this->Predict(mXvalid); 
         T meanvalues = std::get<0>(gpout);
         T stdvalues  = std::get<1>(gpout); // Information not really needed here

         size_t nelem = meanvalues.size();

         for (size_t idx = 0; idx < nelem; idx++)
         {
            error += std::pow(meanvalues[idx] - mYvalid[idx], 2.0);
         }  
         error = std::sqrt(error/nelem);

         return error;
      }

      void Init(  std::vector<GPKernel<T>>& aKernel
               ,  GPMeanFunction<T>* aMean
               ,  CovAlgo aAlgo = USECHOL
               ,  const T& aShift = 0.0
               ,  const bool& aScaleSigma2 = false
               ,  const In& aNumLayer = 1
               ,  const bool& aAdaptNoise = false
               )
      {
         mNumLayer = aNumLayer;
         mActLayer = aNumLayer;

         // allocate memory for the different Layers
         mKernel.resize(mNumLayer);
         mHparam.resize(mNumLayer);
         mNoise.resize(mNumLayer);
         mInputNoise.resize(mNumLayer);
       
         mKinvSize.resize(mNumLayer, 0);       
         mBmatSize.resize(mNumLayer, 0);
         mCmatSize.resize(mNumLayer, 0);

         for (In ilayer = 0; ilayer < mNumLayer; ilayer++)
         {
            // Copy kernel
            mKernel[ilayer].DeepCopy(aKernel[ilayer]);
            mHparam[ilayer] = aKernel[ilayer].GetHyperParameters();
         }

         mHaveCov.resize(mNumLayer, false); 

         mCovAlgo = aAlgo;

         mMean = aMean;
         mOwnMean = false; // User will delete it later

         mShift = aShift;

         mScaleSigma2 = aScaleSigma2;
      
         mAdaptNoise = aAdaptNoise;
      }

   public:

      ///> Constructor(s) 
      GauPro()
      {
         // allocate memory for the different Layers
         mKernel.resize(mNumLayer);
         mHparam.resize(mNumLayer);
         mNoise.resize(mNumLayer);
         mInputNoise.resize(mNumLayer);

         mKinvSize.resize(mNumLayer, 0);       
         mBmatSize.resize(mNumLayer, 0);
         mCmatSize.resize(mNumLayer, 0);

         // Not very intelligent inital guess, but we anyway not data yet...         
         mHparam[0] = std::vector<T>(2,1.0);  

         // As default kernel use a squared exponential one
         mKernel[0]  = GPKernel<T>(mHparam[0],GPKernel<T>::KernelType::SQEXP);
         mHaveCov.resize(mNumLayer, false); 

         mCovAlgo = USECHOL;

         mMean = new GPConstMean<T>(C_0);
         mOwnMean = true; // We will delete it later

         mShift = 0.0;
      }

      GauPro(  GPKernel<T>& aKernel
            ,  GPMeanFunction<T>* aMean
            ,  CovAlgo aAlgo = USECHOL
            ,  const T& aShift = 0.0
            ,  const bool& aScaleSigma2 = false
            ,  const In& aNumLayer = 1
            ,  const bool& aAdaptNoise = false
            )
      {
         std::vector<GPKernel<T>> KernList(aNumLayer);

         for (In ilayer = 0; ilayer < aNumLayer; ilayer++)
         {
            KernList[ilayer].DeepCopy(aKernel);
         }

         Init( KernList
             , aMean
             , aAlgo
             , aShift
             , aScaleSigma2
             , aNumLayer
             , aAdaptNoise
             );
      }

      GauPro(  std::vector<GPKernel<T>>& aKernel
            ,  GPMeanFunction<T>* aMean
            ,  CovAlgo aAlgo = USECHOL
            ,  const T& aShift = 0.0
            ,  const bool& aScaleSigma2 = false
            ,  const In& aNumLayer = 1
            ,  const bool& aAdaptNoise = false
            )
      {
         Init( aKernel
             , aMean
             , aAlgo
             , aShift
             , aScaleSigma2
             , aNumLayer
             , aAdaptNoise
             );
      }

      ///> Delete copy constructor
      GauPro(const GauPro&)  = delete;
      ///> Keep move constructor
      GauPro(GauPro&&) = default;

      ///> Destructor
      ~GauPro()
      {
         if (mOwnMean)
         {
            delete mMean;
         }
      }

      /**
       *  @brief Stores the Co-variance matrix on file  
       *
       *
       * */
      void StoreCovarianceMatrix
         (  std::ostream& arOut
         )
      { 
         //// All stuff related to the covariance matrix
         //CovAlgo mCovAlgo = USELU;        ///> Which alogrithm is used to decompose the matrix?
         //std::unique_ptr<T[]> mCov;       ///> Space for the co-variance matrix. Needed by any algorithm
         //std::unique_ptr<T[]> mSval;      ///> Space for the singular values. Needed if mCovAlgo == USESVD
         //std::unique_ptr<int[]> mIpiv;    ///> Space for book keeping of row exchanges. Needed if mCovAlgo == USELU
         //bool mHaveCov;                   ///> Internal boolean do avoid that the co-variance matrix is computed if not needed
         ////--------------------------------------------

         for (In ilayer = 0; ilayer < mNumLayer; ilayer++)
         {

            SetupCovarianceMatrix(ilayer);

            arOut << "$ALOGRITHM"  << std::endl << mCovAlgo << std::endl;
            arOut << "$DATAPOINTS" << std::endl << mNumData << std::endl;
            arOut << "$KERNEL" << std::endl << this->mKernel[ilayer].GetName() << std::endl;
            std::vector<T> hparam = this->GetHyperParameters(ilayer);
            arOut << "$HYPERPARAMETER" << std::endl << hparam.size() << std::endl;
            for (int ihparam = 0; ihparam < hparam.size(); ihparam++)
            {
               arOut << std::scientific << std::setprecision(14) << hparam[ihparam] << std::endl; 
            }

            arOut << "$DATA" << std::endl;
            T* pcov = mCov[ilayer].get();
            for (int irow = 0; irow < mNumData; irow++)
            {
               for (int jcol = 0; jcol < mNumData; jcol++)
               {
                  arOut << std::scientific << std::setprecision(16) << *(pcov++) << " ";
               }
               arOut << std::endl;
            } 

            // Write mIpiv
            if (mCovAlgo == USELU || mCovAlgo == USEBKD)
            {
               for (int idata = 0; idata < mNumData; idata++)
               {
                  arOut << mIpiv[ilayer][idata] << std::endl;
               } 
            }

            // write mSval
            if (mCovAlgo == USESVD)
            {
               for (int idata = 0; idata < mNumData; idata++)
               {
                  arOut << mSval[ilayer][idata] << std::endl;
               } 
            }

            arOut << "$NOISE" << std::endl;
            for (int idata = 0; idata < mNumData; idata++)
            {
               arOut << mNoise[ilayer][idata] << std::endl;
            }

         }
      }

      /**
       *  @brief Reads the Co-variance matrix from file  
       *
       *
       * */
      void ReadCovarianceMatrix
         (  const std::string& arFcovar
         )
      {
         std::string s;
         std::string dum;

         ifstream covarfile;
         covarfile.open(arFcovar, ios::in);

         int ndata = 0;

         In il_data  = 0;
         In il_kern  = 0;
         In il_hype  = 0;
         In il_noise = 0;

         if (covarfile.good())
         {
            while(midas::input::GetLine(covarfile, s) )
            {
               std::string datagrp = midas::util::FromString<std::string>(s);

               if ( datagrp == "$ALOGRITHM" ) 
               {
                  midas::input::GetLine(covarfile, s);
                  int ialgo = midas::util::FromString<int>(s);
                  // sanity check
                  if (mCovAlgo != ialgo)
                  {
                     MIDASERROR("Different algorithms for the co-variance matrix used!");
                  }
               }
               if ( datagrp == "$DATAPOINTS" ) 
               {
                  midas::input::GetLine(covarfile, s);
                  ndata = midas::util::FromString<int>(s);
                  // sanity check
                  if (ndata != mXdata.size())
                  {
                     Mout << "need: " << ndata << std::endl;
                     Mout << "have: " << mXdata.size() << std::endl;
                     MIDASERROR("Stored Covariance matrix has wrong size!");
                  }
               }
               if ( datagrp == "$KERNEL" )
               {
                  std::string kernelname;
                  midas::input::GetLine(covarfile, kernelname);
                  if (kernelname != this->mKernel[il_kern].GetName() )
                  {
                     MIDASERROR("Stored Covariance matrix used different kernel!");
                  }
               
                  il_kern += 1;
               }
               if ( datagrp == "$HYPERPARAMETER" )
               {
                  // We just read the hyper paramter for comparison and give a warning
                  // if they differ from those we are using. Maybe a MIDASERROR would be more appropriate??

                  std::vector<T> hparam;

                  midas::input::GetLine(covarfile, s);
                  int nsize = midas::util::FromString<int>(s);

                  for (int ihparam = 0; ihparam < nsize; ihparam++)
                  {
                     midas::input::GetLine(covarfile, s);
                     T num = midas::util::FromString<T>(s);
                     hparam.push_back(num);
                  }

                  std::vector<T> myhparam = this->GetHyperParameters(il_hype);
                  std::vector<T> diff(hparam.size());

                  for (int idx = 0; idx < hparam.size(); idx++)
                  {
                     diff[idx] = myhparam[idx] - hparam[idx];
                  }

                  T diffnorm = std::sqrt(std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0L));

                  if (diffnorm > 1.e-3 && il_hype == 0) 
                  {
                     Mout << "Hparam I  : " << myhparam << std::endl;
                     Mout << "Hparam II : " << hparam << std::endl;
                     Mout << "diffnorm " << diffnorm << std::endl;
                     MidasWarning("Your specified hyper parameter differ from those stored in the co-variance matrix. It is very unlikely that this is on purpose!");
                  }

                  this->SetHyperParameters(hparam,il_hype);

                  il_hype += 1;

               }
               if ( datagrp == "$DATA" )
               {
                  if (mCov.size() != mNumLayer) mCov.resize(mNumLayer);
                  mCov[il_data].reset(new T[ndata*ndata]);
                  T* pcov = mCov[il_data].get();
                  for (int irow = 0; irow < ndata; irow++)
                  {
                     midas::input::GetLine(covarfile, s);
                     std::istringstream str_stream(s);
                     for (int jcol = 0; jcol < ndata; jcol++)
                     {
                        str_stream >> *(pcov++);   
                     }
                  }
                  // Get (maybe) required additional information
                  if (mCovAlgo == USELU || mCovAlgo == USEBKD)
                  {
                     if (mIpiv.size() != mNumLayer) mIpiv.resize(mNumLayer);
                     mIpiv[il_data].reset(new int[ndata]);
                     for (int idata = 0; idata < mNumData; idata++)
                     {
                        midas::input::GetLine(covarfile, s);
                        std::istringstream str_stream(s);
                        str_stream >> mIpiv[il_data][idata];
                     } 
                  }
                  if (mCovAlgo == USESVD)
                  {
                     if (mSval.size() != mNumLayer) mSval.resize(mNumLayer);
                     mSval[il_data].reset(new T[ndata]);
                     for (int idata = 0; idata < mNumData; idata++)
                     {
                        midas::input::GetLine(covarfile, s);
                        std::istringstream str_stream(s);
                        str_stream >> mSval[il_data][idata];
                     } 
                  }

                  il_data += 1;
               }
               if ( datagrp == "$NOISE" )
               {
                  std::vector<T> noise;

                  for (int idata = 0; idata < mNumData; idata++)
                  {
                     midas::input::GetLine(covarfile, s);
                     T num = midas::util::FromString<T>(s);
                     noise.push_back(num);
                  }

                  mNoise[il_noise] = noise;

                  il_noise += 1;
               }

            }

            for (In ilayer = 0; ilayer < mNumLayer; ilayer++)
            {
               if (ilayer > 0)
               {
                  this->SetDeltaData(ilayer);
               }
               mHaveCov[ilayer] = true;
            }

         }
         else
         {
            MIDASERROR("Could not read from " + arFcovar + "!");
         }
      }

      /**
       *  @brief Returns the logarithm of the marginal likelihood. 
       *  
       *  Purpose Calculates the logarithm of the marginal likelihood, which is an accuracy diagnostic for the predictor.
       *          The expierence so far suggests that it should be larger than -5e+3. 
       *
       **/
      T GetMarginalLikelihood
         (  const In& aILayer = 0 
         )
      {
         const bool locdbg = false;

         // Calculate the covariance matrix 
         SetupCovarianceMatrix(aILayer);

         //---------------------------------------------------------------------------------+
         // calculate -0.5 f(x)^T K(x,x)^-1 f(x) - 0.5 log det(K) - 0.5 * n * log 2pi
         //---------------------------------------------------------------------------------+

         // 1) Part: -0.5 f(x)^T K(x,x)^-1 f(x)

         // Solve linear system of equations K(x,x) c(x) = f(x) to calculate K(x,x)^-1 f(x)
         int m = mNumData;
         int nrhs = 1;
         int info = 0;

         std::unique_ptr<T[]> cvec(new T[mNumData]);
         std::unique_ptr<T[]> bvec(new T[mNumData]);
    
         T dshift = (aILayer == 0 ? mShift : 0.0);
 
         // fill in data 
         for (Uin idata = 0; idata < mNumData; idata++)
         {
            cvec[idata] = mYdata[aILayer][idata] - dshift;
            bvec[idata] = mYdata[aILayer][idata] - dshift;
         }

         // solve K c = f  
         switch(mCovAlgo)
         {
            case USELU :
            {    
               midas::lapack_interface::getrs(&nc, &m, &nrhs, mCov[aILayer].get(), &m, mIpiv[aILayer].get(), cvec.get(), &m, &info);
               if (info != 0) Mout << "GPR: Problem in solving linear system III" << std::endl;
               break;
            }
            case USECHOL :
            {
               midas::lapack_interface::potrs(&uplo, &m, &nrhs, mCov[aILayer].get(), &m, cvec.get(), &m, &info);
               if (info != 0) Mout << "GPR: Problem in solving linear system III" << std::endl;
               break;
            }
            case USESVD :
            {
               T* tmp = new T[mNumData];
               std::memcpy( tmp, cvec.get(), sizeof(T)*mNumData );

               int m = mNumData; 
               int n = 1; 

      
               midas::lapack_interface::gemm( &nc, &nc, &m, &n, &m, &one 
                                            , mCov[aILayer].get(), &m, tmp, &m
                                            , &zero, cvec.get(), &m );    

               delete[] tmp;

               break; 
            }
            case USECONJGRAD :
            {
               this->ConjGrad(mSolutVector[aILayer], mTrialVector[aILayer], mCov[aILayer], cvec, 1e-12, 1000, false, aILayer);
               for (Uin idata = 0; idata < mNumData; idata++)
               {
                  cvec[idata] = mSolutVector[aILayer][idata];
               }
               break;
            }
            case USEBKD :
            {
               midas::lapack_interface::sytrs(&uplo, &m, &nrhs, mCov[aILayer].get(), &m, mIpiv[aILayer].get(), cvec.get(), &m, &info);
               if (info != 0) Mout << "GPR: Problem in solving linear system III" << std::endl;
               break;
            }
            case USESPARSE:
            {
               SparseSol(cvec,aILayer);
               break;
            }
            default :
            {
               MIDASERROR("Unknown algorithm to decompose covariance matrix!");
            }
         }

         int inc = 1;
         T part1 = -0.5 * midas::lapack_interface::dot(&m, bvec.get(), &inc, cvec.get(), &inc);  

         if (locdbg) Mout << "  logP Part1: " << part1 << std::endl;

         // 2) Part: -0.5 log det(K)
         T part2 = -0.5 * GetLogDet(aILayer);

         if (locdbg) Mout << "  logP Part2: " << part2 << std::endl;

         // 3) Part: -0.5 * n * log 2 pi
         T part3 = - 0.5 * mNumData * log(2 * M_PI);

         if (locdbg) Mout << "  logP Part3: " << part3 << std::endl;

         return part1 + part2 + part3;
      }


      T GetMarginalLikeLimit()
      {
         return (- 0.5 * mNumData * log(2 * M_PI));
      }

      /**
       *  @brief Calculates the entropy of the multivariant normal distribution 
       *
       * */
      T GetEntropy()
      {
         T part1 = 0.5 * mNumData * std::log(2.0 * M_PI * std::exp(1.0));

         T part2 = 0.5 * GetLogDet();

         return part1 + part2;
      }

      /**
       *  @brief Calculate base uncertainty 
       *
       *  Purpose: Calculate an estimate for the uncertainty provided by this
       *           training set for "good" predictions
       *
       * */
      T GetBaseUncertainty()
      {
         // Predict known points
         std::tuple<std::vector<T>, std::vector<T>> gpout = this->Predict(mXdata);

         // and extract uncertainty
         std::vector<T> sigval  = std::get<1>(gpout);  

         // calculate average of elements in sigval
         T mean = std::accumulate(std::begin(sigval), std::end(sigval), 0.0) / std::size(sigval);

         return mean;
      }

      /**
       *  @brief Predicts the function values for a new set of points 
       *
       *  Purpose: Predicts the function values for a new set of points
       *           using a Gaussian Process
       *
       * @param  arNewPoint vector representing the new point
       * @param  arInfo enforces energy prediction (aInfo = 0), gradient prediction (aInfo = 1) and Hessian prediction (aInfo = 2)
       * @param  arSkipUncertainty is boolean which deactivates the calculation of the uncertainty
       *
       *
       * @return Returns a tuple with a vector for the interpolated 
       *         function values and a vector with the estimated 
       *         standard deviation for this values 
       * */
      std::tuple<T, T> Predict
         (  MLDescriptor<T>& arNewPoint
         ,  const int& arInfo = 0
         ,  const bool& arSkipUncertainty = false
         )
      {
         std::vector<MLDescriptor<T>> input(1);
         input[0] = arNewPoint;
         
         std::tuple<std::vector<T>, std::vector<T>> gpout = this->Predict(input,arInfo,arSkipUncertainty);

         std::vector<T> meanval = std::get<0>(gpout);
         std::vector<T> stdval  = std::get<1>(gpout);  

         return std::make_tuple(meanval[0], stdval[0]);

      }

      /**
       *  @brief Predicts the function values for a new set of points 
       *
       *  Purpose: Predicts the function values for a new set of points
       *           using a Gaussian Process
       *
       * @param  aNewPoints vector containing the new points
       * @param  aInfo enforces energy prediction (aInfo = 0), gradient prediction (aInfo = 1) and Hessian prediction (aInfo = 2)
       * @param  aSkipUncertainty is boolean which deactivates the calculation of the uncertainty
       * @param  aUseWeights activates to use the weights provides a aWei instead of computing them from the input
       * @param  aWei are user provided weights. Note that aUseWeights has to be true and that then no uncertainty can be calculated
       * @param  aSetNumLayer determines if the number of Layers should be set by the user
       * @param  aNumLayer is the number of Layers if aSetNumLayer is true
       *
       * @return Returns a tuple with a vector for the interpolated 
       *         function values and a vector with the estimated 
       *         variance for this values 
       **/
      std::tuple<std::vector<T>, std::vector<T>> Predict
         (  std::vector<MLDescriptor<T>>& aNewPoints
         ,  const int& aInfo = 0
         ,  const bool& aSkipUncertainty = false
         ,  const bool& aUseWeights = false
         ,  const std::unique_ptr<T[]>& aWei = std::unique_ptr<T[]>(nullptr)
         ,  const bool& aSetNumLayer = false
         ,  const In& aNumLayer = 1
         )
      {
         const bool locdbg = false;
         const bool correct_wei = false;

         LOGCALL("PREDICT");

         // Set number of layers used in the prediction
         In NumLayer = (aSetNumLayer ? aNumLayer : mActLayer);

         //
         // Set important dimensions
         //
         int ndim  = aNewPoints[0].size();
         int nintp = aNewPoints.size();
         int nprop = 1;                                         // How many properties we want to predict: just energy
         if (aInfo > 0) nprop = 1 + ndim;                       // Energy + Gradient
         if (aInfo > 1) nprop = 1 + ndim + (ndim*(ndim+1)/2);   // Energy, Gradient + Hessian

         //
         // Return vectors containing mean-values and uncertainty
         //
         std::vector<T> meanval(nintp * nprop, C_0);
         std::vector<T> varval(nintp,C_0);

         for (In ilayer = 0; ilayer < NumLayer; ilayer++)
         {
            //-------------------------------------------------------------+
            // Calculate Covariance Matrix K and do LU decomp on it
            //-------------------------------------------------------------+
            if (!aUseWeights)
            {
               SetupCovarianceMatrix(ilayer);
            }
            else
            {
               if (ilayer != 0) MIDASERROR("Using precomputed weights is only possible using one layer");
               MidasWarning("GPR: I will use pre-computed weights. Therefore I can not calculate an uncertainty!");
            }

            //-------------------------------------------------------------+
            // Caculate weights
            //-------------------------------------------------------------+
            std::unique_ptr<T[]> wei(new T[mNumData]);
            if (aUseWeights)
            {
               memcpy(reinterpret_cast<void*>(wei.get()),
                      reinterpret_cast<void*>(aWei.get()), mNumData*sizeof(T));
            }
            else
            {
               GetWeights(wei, true, ilayer);
            }

            if (mCovAlgo != USECONJGRAD && correct_wei) CorrectWeights(wei,ilayer);

            //-------------------------------------------------------------+
            // Print some statistics 
            //-------------------------------------------------------------+
            if (gMLLevel > 1) 
            {
               Mout << " Marginal Likelihood: " << GetMarginalLikelihood(ilayer) << " for layer " << ilayer << std::endl;
            }

            //-------------------------------------------------------------+
            // Predict function values (interpolate)
            //-------------------------------------------------------------+
     
            bool addnoise = false;  // For predictions the noise factor is disabled 
            T noise = C_0;

            switch(aInfo)
            {
 
               case 0: // Energy prediction
               {
                  // y = sum_k w_k K(x,x_k)
                  for (Uin inew = 0; inew < nintp; inew++)
                  {
                     int lderi = aNewPoints[inew].GetDerivativeOrder();  
                     if (ilayer == 0) meanval[inew] = mMean->Evaluate(aNewPoints[inew]) + mShift;
                     for (Uin idata = 0; idata < mNumData; idata++)
                     {
                        int lderj = mXdata[idata].GetDerivativeOrder(); 
                        meanval[inew] += wei[idata] * mKernel[ilayer].Compute(aNewPoints[inew], mXdata[idata], addnoise, noise, lderi, lderj); 
                     } 
                  }
                  break;
               }

               case 1: // Energy, Gradient prediction
               case 2: // Energy, Gradient, Hessian prediction
               {

                  int nmem = ndim + 1;
                  if (aInfo > 1) nmem = (ndim*(ndim+1)/2) + ndim + 1;

                  std::unique_ptr<T[]> mat(new T[nmem*mNumData]);
                  std::unique_ptr<T[]> vec(new T[nmem]);

                  for (Uin inew = 0; inew < nintp; inew++)
                  {

                     // Energy block
                     int lderi = 0;
                     for (Uin idata = 0; idata < mNumData; idata++)
                     {
                        int lderj = mXdata[idata].GetDerivativeOrder();
                        mat[idata * nmem] =  mKernel[ilayer].Compute(aNewPoints[inew], mXdata[idata], addnoise, noise, lderi, lderj);
                     }  

                     // Gradient block
                     lderi = 1;
                     for (Uin ider_coord = 1; ider_coord <= ndim; ider_coord++)
                     {
                        for (Uin idata = 0; idata < mNumData; idata++)
                        {
                           int lderj = mXdata[idata].GetDerivativeOrder();
                           MLDescriptor<T> point = aNewPoints[inew];
                           point.DefineAsGradElem(ider_coord - 1);
                           mat[idata*nmem + ider_coord] = mKernel[ilayer].Compute(point, mXdata[idata], addnoise, noise, lderi, lderj);
                        }
                     }
              
                     // Hessian block
                     if (aInfo == 2) 
                     {
                        lderi = 2;
                        for (Uin ider_coord = 1; ider_coord <= ndim; ider_coord++)
                        {
                           for (Uin jder_coord = 1; jder_coord <= ider_coord; jder_coord++)
                           {
                              for (Uin idata = 0; idata < mNumData; idata++)
                              {
                                 int lderj = mXdata[idata].GetDerivativeOrder();
                                 MLDescriptor<T> point = aNewPoints[inew];
                                 point.DefineAsHessElem(ider_coord - 1, jder_coord - 1);
 
                                 mat[  idata * nmem 
                                     + ndim  
                                     + ider_coord*(ider_coord-1)/2 + jder_coord
                                    ] 
                                    = mKernel[ilayer].Compute(point, mXdata[idata], addnoise, noise, lderi, lderj); 
                              }
                           }
                        }

                     }
 
                     // Predict property as mat x wei = vec
                     int n = nmem; 
                     int k = mNumData;     
                     int nrhs = 1;

                     //midas::lapack_interface::gemm( &nc, &nc, &n, &nrhs, &k, &one 
                     midas::lapack_interface::gemm( &nc, &nc, &n, &nrhs, &k, &one 
                                                  , mat.get(), &n, wei.get(), &k
                                                  , &zero, vec.get(), &n );    

                     // GS TODO adjust meanvalue also for gradient and Hessian.
                     //         At the moment a non zero mean function only gives
                     //         sane results for Energy predicitions
                     if (ilayer == 0) meanval[inew*nprop] = mMean->Evaluate(aNewPoints[inew]) + mShift;
                     for (int iprop = 0; iprop < nprop; iprop++) 
                     {
                        meanval[inew*nprop+iprop] += vec[iprop];
                     }

                     if (locdbg) 
                     {
                        Mout << "Prediction:" << std::endl;
                        prtmat(vec,nmem,1);
                     }

                  }
                
                  break;
               }

               default:
               {
                  MIDASERROR("GPR:> Unknow prediction mode!");
               }
            }

            //-------------------------------------------------------------+
            // Get Error estimate for predicted points
            //-------------------------------------------------------------+
            if (ilayer == 0) // We will compute uncertainty only for first layer. It should be the dominating factor anyway
            {
               int m = mNumData;
               int nrhs = 1;
               int info = 0;

               // Var(x*,x*) = K(x*,x*) - K(x*,x) K(x,x)^-1 K(x*,x)^T 

               if (!aSkipUncertainty && !aUseWeights)
               {
                  std::unique_ptr<T[]> covarpp(new T[nintp*nintp]);     // predict predict block
                  std::unique_ptr<T[]> covarpd(new T[nintp*mNumData]);  // predict data    block
   
                  In max_points = std::max(aNewPoints.size(),mXdata.size());
                  std::vector<T> input_noise(max_points,C_0);
 
                  mKernel[ilayer].Compute(covarpp, aNewPoints, aNewPoints, input_noise, 0, nintp, nintp, true);
                  mKernel[ilayer].Compute(covarpd, aNewPoints, mXdata, input_noise, 0, nintp, mNumData, false);

                  // calculate  C(x,x*) = K(x,x)^-1 K(x*,x)^T   
                  std::unique_ptr<T[]> cmat(new T[mNumData*nintp]);   

                  // fil in data of K(x*,x) in transposed form
                  for (Uin idata = 0; idata < mNumData; idata++)
                  {
                     for (Uin inew = 0; inew < nintp; inew++)
                     {
                        cmat[inew * mNumData + idata] = covarpd[idata * nintp + inew];
                     }
                  }

                  // Solve linear system of equations to get C(x,x*)
                  nrhs = nintp;
                  switch(mCovAlgo)
                  {
                     case USELU :
                     {
                        midas::lapack_interface::getrs(&nc, &m, &nrhs, mCov[ilayer].get(), &m, mIpiv[ilayer].get(), cmat.get(), &m, &info);
                        if (info != 0) Mout << "GPR: Problem in solving linear system II" << std::endl;
                        break;
                     }
                     case USECHOL :
                     {
                        midas::lapack_interface::potrs(&uplo, &m, &nrhs, mCov[ilayer].get(), &m, cmat.get(), &m, &info);
                        if (info != 0) Mout << "GPR: Problem in solving linear system III" << std::endl;
                        break;
                     }
                     case USESVD :
                     {
                        std::unique_ptr<T[]> tmp(new T[mNumData*nintp]);
                        std::copy ( cmat.get(), cmat.get() + mNumData*nintp, tmp.get() );
                        {
                           int m = mNumData; 
      
                           midas::lapack_interface::gemm( &nc, &nc, &m, &nrhs, &m, &one 
                                                        , mCov[ilayer].get(), &m, tmp.get(), &m
                                                        , &zero, cmat.get(), &m );    
                        }

                        break; 
                     }
                     case USECONJGRAD :
                     {
                        this->ConjGrad(mSolutVector[ilayer], mTrialVector[ilayer], mCov[ilayer], cmat, 1e-12, 1000, false, ilayer);
                        for (Uin idata = 0; idata < mNumData; idata++)
                        {
                           cmat[idata] = mSolutVector[ilayer][idata];
                        }
                        break;
                     }
                     case USEBKD :
                     {
                        midas::lapack_interface::sytrs(&uplo, &m, &nrhs, mCov[ilayer].get(), &m, mIpiv[ilayer].get(), cmat.get(), &m, &info);
                        if (info != 0) Mout << "GPR: Problem in solving linear system III" << std::endl;
                        break;
                     }
                     case USESPARSE:
                     {
                        SparseSol(cmat,ilayer);
                        break;
                     }
                     default:
                     {
                        MIDASERROR("Unknown algorithm to decompose covariance matrix!");
                     }
                  }

                  // calculate B(x*,x*) =  K(x*,x) C(x,x*)
                  std::unique_ptr<T[]> bmat(new T[nintp*nintp]);   
                  {
                     int m = nintp;  
                     int n = nintp;
                     int k = mNumData;  
      
                     midas::lapack_interface::gemm( &nc, &nc, &m, &n, &k, &one 
                                                  , covarpd.get(), &m, cmat.get(), &k 
                                                  , &zero, bmat.get(), &m );    
                  }
                  
                  // We are at the moment only interested in the diagonal of Var(x*,x*) 
                  // and can later extract the standard deviation...
                  for (Uin inew = 0; inew < nintp; inew++)
                  {
                     T dtemp = covarpp[inew * nintp + inew] - bmat[inew * nintp + inew];

                     varval[inew] = dtemp;

                     if (mScaleSigma2) varval[inew] *= std::log(mNumData);  // Some Author suggest to scale the uncertainty by log n 
                                                                            // to make it more reliable
                  }
               }
            }
         }

         return make_tuple(meanval, varval ) ;
      }

      /**
       *  @brief Calculates the gradient on  the GPR surface for a point 
       *
       * */
     std::vector<T> GetGradient 
         (  MLDescriptor<T>& aPoint
         ,  const In& aILayer = 0
         )
      {
         const bool locdbg = true;

         int nsize = aPoint.size();

         if (aILayer != 0) MIDASERROR("Returning the Gradient (the old way) in GauPro only works with one Layer!");

         //-------------------------------------------------------------+
         // Calculate Covariance Matrix K and do LU decomp on it
         //-------------------------------------------------------------+
         SetupCovarianceMatrix(aILayer);
 
         //-------------------------------------------------------------+
         // Caculate weights
         //-------------------------------------------------------------+
         std::unique_ptr<T[]> wei(new T[mNumData]);
         GetWeights(wei, true,aILayer);
   
         //-------------------------------------------------------------+
         // Caluclate gradient for (interpolated) point
         //-------------------------------------------------------------+
         std::vector<T> grad(nsize);
         
         for (Uin idx = 0; idx < nsize; idx++)
         {
            grad[idx] = 0.0;
         }

         for (Uin idata = 0; idata < mNumData; idata++)
         {
            std::vector<T> gradk = mKernel[aILayer].Gradient(aPoint,mXdata[idata]);
            for (Uin idx = 0; idx < nsize; idx++)
            {
               grad[idx] += wei[idata] * gradk[idx];
            } 
         } 

         return grad ;
      }
     
      std::tuple<std::vector<T>, std::vector<T>> Scan1D
         (  MLDescriptor<T>& coord
         ,  int icoord
         ,  T mval 
         ,  T pval
         ,  int npoints
         ,  const In& aILayer = 0 
         )
      {
         const bool locdbg = true;

         //-------------------------------------------------------------+
         // Calculate Covariance Matrix K and do LU decomp on it
         //-------------------------------------------------------------+
         SetupCovarianceMatrix(aILayer);
 
         //-------------------------------------------------------------+
         // Caculate weights
         //-------------------------------------------------------------+
         std::unique_ptr<T[]> wei(new T[mNumData]);
         GetWeights(wei, true,aILayer);
         
         //CorrectWeights(wei);
   
         //-------------------------------------------------------------+
         // Predict function values (interpolate)
         //-------------------------------------------------------------+
         bool addnoise = false;
         T noise = C_0;
 
         MLDescriptor<T> point = coord;
         std::vector<T>  xval(npoints);
         std::vector<T>  ret(npoints);


         T dstart = point[icoord] - mval * point[icoord]; 
         T dend   = point[icoord] + pval * point[icoord];

         T dstep = (dend - dstart) / npoints;

         for (int iter = 0; iter < npoints; iter++)
         {
            point[icoord] = dstart + dstep * iter;

            xval[iter] = point[icoord];

            int lderi = point.GetDerivativeOrder(); 

            // y = sum_k w_k K(x,x_k)
            T meanval = mMean->Evaluate(point);
            for (Uin idata = 0; idata < mNumData; idata++)
            {
               int lderj = mXdata[idata].GetDerivativeOrder(); 
               T dval =  mKernel[aILayer].Compute(point, mXdata[idata], addnoise, noise, lderi, lderj);

               meanval +=  wei[idata] * dval;
            } 
            ret[iter] = meanval;
         }

         return std::make_tuple(xval, ret);
      } 
     


      /**
       *  @brief Sets the size of the sub set for sparse GPR  
       *
       * @param  aSizeSubSet specifies the number of inducing points
       *
       * */
      void SetSizeOfSubSet
         (  const In& aSizeSubSet 
         )
      {
         const bool locdbg = true;
         const bool use_farthest_point = true;
         const int maxiter = 1;

         mSizeSubSet = std::min(aSizeSubSet, mNumData);

         // Get List of current indices
         std::vector<In> Indices(mNumData);
         for (int i = 0; i < mNumData; ++i) Indices[i] = i;

         if (use_farthest_point)
         {
            std::unique_ptr<T[]> kern(new T[mNumData*mNumData]);  
            mKernel[0].Compute(kern, mXdata, mXdata, mNoise[0], 0, mNumData, mNumData, true);

            // Get Indices for subset by farthest point sampling
            std::vector<In> IndSubSet = mlearn::Sample(kern,aSizeSubSet,mNumData);

            // Swap elements to get new order of points
            for (In k = 0; k < aSizeSubSet; k++)
            {
               std::swap(Indices[k], Indices[IndSubSet[k]]);
            }

            if (locdbg) Mout << "New set of indices" << Indices << std::endl;

            ReorderTrainingSet(Indices);
         }
         else
         {
            // Reorder data set in a random way
            std::random_shuffle ( Indices.begin(), Indices.end() );
            Mout << "New set of indices" << Indices << std::endl;
            ReorderTrainingSet(Indices);

            for (int iter = 0; iter < maxiter; iter++)
            {
               // Get Absolute Error of Prediction for training points
               std::vector<In> IndOfMaxAbs = GetMaxErrorPoints(mNumData);

               if (locdbg) Mout << "Largest errors for " << IndOfMaxAbs << std::endl;   

               // Reorder data set
               ReorderTrainingSet(IndOfMaxAbs);
            }
         }
      }

      /**
       *  @brief Perform some diagnostics on the GPR  
       *
       * */
      void CheckGPR
         (
         )
      {
         In ilayer = 0;

         std::unique_ptr<T[]> kern(new T[mNumData*mNumData]);
         mKernel[ilayer].Compute(kern, mXdata, mXdata, mNoise[ilayer], 0, mNumData, mNumData, true, false, 1.0, true);

         // Sample whole test set, which is likely to scale not well... but this routine is anyway just for testing
         std::vector<In> Indices = mlearn::Sample(kern,mNumData,mNumData);

         ReorderTrainingSet(Indices);
         mKernel[ilayer].Compute(kern, mXdata, mXdata, mNoise[ilayer], 0, mNumData, mNumData, true, false, 1.0, true);

         for (In idx = 0; idx < mNumData; idx++)
         {
            T row = C_0;
            for (In jdx = idx; jdx < mNumData; jdx++)
            {
               row += kern[idx*mNumData + jdx];
               Mout << "   row elem " << kern[idx*mNumData + jdx] << std::endl;
            }

            Mout << " idata " << idx << " measure " << row << std::endl;
         }

      }


      /**
       *  @brief Reorders the training set based on a index vector  
       *
       * @param  aIndices is the new order of data points in the training set.
       *
       * */
      void ReorderTrainingSet
         (  std::vector<In>& aIndices
         )
      {
         In ilayer = 0;

         for (int i = 0; i < mNumData; i++) 
         {
            while (aIndices[i] != i) 
            {
               int oldIndex = aIndices[aIndices[i]]; 

               MLDescriptor<T> oldX = mXdata[aIndices[i]]; 
               T oldY = mYdata[ilayer][aIndices[i]];

               mXdata[aIndices[i]]           = mXdata[i]; 
               mYdata[ilayer][aIndices[i]]   = mYdata[ilayer][i]; 
               aIndices[aIndices[i]]         = aIndices[i]; 
  
               aIndices[i]       = oldIndex; 
               mXdata[i]         = oldX; 
               mYdata[ilayer][i] = oldY; 
            }  
         }

         for (In ilayer = 0; ilayer < mNumLayer; ilayer++)
         { 
            this->SetDeltaData(ilayer);
         }
         
         std::fill(mHaveCov.begin(), mHaveCov.end(), false);
      }
 
      /**
       *  @brief Sets the training data for the Gaussian process 
       *
       * @param  aXdata vector containing the points 
       * @param  aYdata vector containing the function values
       * @param  aNoise vector containing the noise for each point
       * */
      void SetData
         (  const std::vector<MLDescriptor<T>>& aXdata
         ,  const std::vector<T>& aYdata
         ,  const std::vector<T>& aNoise
         )
      {
         if (mYdata.size() != mNumLayer) mYdata.resize(mNumLayer);

         mXdata    = aXdata;
         mYdata[0] = aYdata; // We can only set data for the first layer, the rest is computed if needed

         mNoise[0] = aNoise;
         mInputNoise[0] = aNoise;

         mNumData = mYdata[0].size();
         mSizeSubSet = mNumData;

         for (In ilayer = 0; ilayer < mNumLayer; ilayer++)
         { 
            this->SetDeltaData(ilayer);
         }

         // We changed our data. Therefore we don't have the correct
         // Covariance matrix anymore
         std::fill(mHaveCov.begin(), mHaveCov.end(), false);

         if (mAdaptNoise) AdaptNoise();

      }


      /**
       *  @brief Adapt noise based on distances 
       *
       * */
      void AdaptNoise()
      {

         const bool zero_derivate_noise = true;
         const bool treat_near_neighbours = true;

         if (zero_derivate_noise)
         {
            In i = 0;
            for (const MLDescriptor<T>& elem : mXdata)
            {
               if (elem.GetDerivativeOrder() > 0)
               {
                  mNoise[0][i] = 1e-14;
               }
               i++;
            }
         }

         if (treat_near_neighbours)
         {
            for (In idx = 0; idx < mNumData; idx++)
            {
               if (mXdata[idx].GetDerivativeOrder() == 0)
               {
                  T dist = 99e9;
                  T rmax = C_0;
                  T rmin = 99e9;

                  T noise = C_0;
   
                  // Calculate neighrest neighbour and adapt noise according to him
                  for (In jdx = 0; jdx < mNumData; jdx++)
                  {
                     if (idx != jdx && mXdata[jdx].GetDerivativeOrder() == 0)
                     {
                        T rij = C_0;
                        for (In k = 0; k < mXdata[jdx].size(); k++)
                        {
                           rij += std::pow(mXdata[idx][k] - mXdata[jdx][k], 2.0);
                        }
                        rij = std::sqrt(rij);

                        if (rij < dist) dist = rij;
                        if (rij < rmin) rmin = rij;
                        if (rij > rmax) rmax = rij;

                     } 
                  }
  
                  In nunits = 2; 
                  for (In jdx = 0; jdx < mNumData; jdx++)
                  {
                     if (idx != jdx && mXdata[jdx].GetDerivativeOrder() == 0)
                     {
                        T rij = C_0;
                        for (In k = 0; k < mXdata[jdx].size(); k++)
                        {
                           rij += std::pow(mXdata[idx][k] - mXdata[jdx][k], 2.0);
                        }
                        rij = std::sqrt(rij);

                        T a2 = std::pow(mInputNoise[0][idx],0.7);
                        T a1 = mInputNoise[0][idx];

                        T alpha = - std::log( a1/a2 ) / ( nunits*nunits * rmin*rmin);
                        noise += a2 * std::exp(-alpha * rij*rij);

                     } 
                  }
                  
                  mNoise[0][idx] = noise;
               }
            }
         }

      }


      /**
       *  @brief Adds new training data for the Gaussian process 
       *
       * @param  aXdata vector containing the points 
       * @param  aYdata vector containing the function values
       * */
      void AddData
         (  const std::vector<MLDescriptor<T>>& aXdata
         ,  const std::vector<T>& aYdata
         )
      {
         if (mYdata.size() != mNumLayer) mYdata.resize(mNumLayer);

         int ndata_old = mYdata[0].size();
         
         mXdata.insert(std::end(mXdata),    std::begin(aXdata),    std::end(aXdata));
         mYdata.insert(std::end(mYdata[0]), std::begin(aYdata[0]), std::end(aYdata[0]));

         mNumData = mYdata[0].size();

         for (In ilayer = 0; ilayer < mNumLayer; ilayer++)
         { 
            this->SetDeltaData(ilayer);
         }

         // We changed our data. Therefore we don't have the correct
         // Covariance matrix anymore
         std::fill(mHaveCov.begin(), mHaveCov.end(), false);

      }
      
      /**
       *  @brief Adds new training data for the Gaussian process 
       *
       * @param  aXdata new point 
       * @param  aYdata new function values
       * */
      void AddData
         (  const MLDescriptor<T> aXdata
         ,  const T aYdata
         )
      {
         if (mYdata.size() != mNumLayer) mYdata.resize(mNumLayer);

         int ndata_old = mXdata.size();

         mXdata.push_back(aXdata);
         mYdata[0].push_back(aYdata);  // We can only set the data in the first layer, the rest if comnputed if required

         mNumData = mYdata[0].size();

         for (In ilayer = 0; ilayer < mNumLayer; ilayer++)
         { 
            this->SetDeltaData(ilayer);
         }

         // We changed our data. Therefore we don't have the correct
         // Covariance matrix anymore
         std::fill(mHaveCov.begin(), mHaveCov.end(), false);

      }
   
      /**
       *  @brief Erases part of the data starting from the first index 
       *
       * */
      void EraseData
         (  int ndelete
         )
      {
         if (mYdata.size() != mNumLayer) mYdata.resize(mNumLayer);

         mXdata.erase( mXdata.begin(),    mXdata.size()    > ndelete ?  mXdata.begin()    + ndelete : mXdata.end() );
         mYdata.erase( mYdata[0].begin(), mYdata[0].size() > ndelete ?  mYdata[0].begin() + ndelete : mYdata[0].end() );

         mNumData = mYdata[0].size();

         for (In ilayer = 0; ilayer < mNumLayer; ilayer++)
         { 
            this->SetDeltaData(ilayer);
         }

         std::fill(mHaveCov.begin(), mHaveCov.end(), false);

      }

      /**
       *  @brief Splits data into training and validation data (experimental) 
       *
       * */
      void SplitData()
      {
         // At the moment we use k-folding with k = 2 meaning that we generate 
         // two sets of equal size. This might be not the optimal solution...
         // 1.) Since we only want to use Cross Validation as an aid it is probably sufficient to 
         //     generate a smaller validation set
         // 2.) There are much more sophisticated ways to split the data...
         //

         // if validation set is not empty reassign it to the trainings data
         if (mXvalid.size() > 0) 
         {
            mXdata.insert( mXdata.end(),    mXvalid.begin(),    mXvalid.end() );
            mYdata.insert( mYdata[0].end(), mYvalid[0].begin(), mYvalid[0].end() );
         }

         int nhalf = mNumData / 2;
         int k = 2;
         
         // Generate vector of indices, which assign the data
         std::vector<int> indices = kfold(mNumData, k);

         std::vector<std::vector<T>> trainX;  
         std::vector<T> trainY;               

         std::vector<std::vector<T>> validX;  
         std::vector<T> validY;               

         trainX.reserve(nhalf);
         trainY.reserve(nhalf);
         validX.reserve(nhalf);
         validY.reserve(nhalf);

         for (int idx = 0; idx < mNumData; idx++)
         {
            if ( indices[idx] == 0) 
            {
               // assign to training set
               trainX.push_back(mXdata[idx]);
               trainY.push_back(mYdata[0][idx]);
            }
            else
            {
               // assign to validation set
               validX.push_back(mXdata[idx]);
               validY.push_back(mYdata[0][idx]);
            }
         }        

         // Overwrite old with new data
         mXdata    = trainX;
         mYdata[0] = trainY;

         mXvalid = validX;
         mYvalid = validY; 

         mNumData = mYdata[0].size();

         for (In ilayer = 0; ilayer < mNumLayer; ilayer++)
         { 
            this->SetDeltaData(ilayer);
         }

         // We changed our data. Therefore we don't have the correct
         // Covariance matrix anymore
         std::fill(mHaveCov.begin(), mHaveCov.end(), false);
 
      }


      /**
       *  @brief Optimizes the hyper parameters 
       **/
      void OptimizeHparams
         (  const bool& arVerbose = true
         ,  ostream& arOut = Mout
         ,  const In& arMaxIter = 500
         ,  const T& arGeps = 1.e-2
         ,  const T& arFeps = 1.e-3 
         )
      {
         if (mAdaptNoise) AdaptNoise();

         for (In ilayer = 0; ilayer < mNumLayer; ilayer++)
         {
            In MaxIter = arMaxIter;

            // In case of Multi Layer GPR provide delta information for next layer
            if (ilayer > 0)
            {
               this->SetDeltaData(ilayer);
               MaxIter = 10;
            }

            if (mActLayer > mNumLayer) break;

            if (arVerbose) 
            {
               arOut << std::endl << "+-------------Start optimizing the hyper parameters------------+" << std::endl << std::endl;
               arOut              << "  Initial marginal likelihood:  " << GetMarginalLikelihood(ilayer) << " for Layer " << ilayer << std::endl;
               arOut              << "  Limit of marginal likelihood: " << GetMarginalLikeLimit() << std::endl;
               arOut              << "  Used kernel function:         " << mKernel[ilayer].GetName() << std::endl;
               arOut              << "  Decomposition Algorithm:      " << map_covalgo.at(mCovAlgo) << std::endl;
               arOut              << "  Average imposed noise:        " << std::accumulate( mNoise[ilayer].begin(), mNoise[ilayer].end(), 0.0/ mNoise[ilayer].size());
               if (mCovAlgo == USESPARSE)
               {
                  arOut              << "  No. of inducing points:       " << mSizeSubSet << std::endl;
               }
            }

            bool usegradient = mKernel[ilayer].HaveHyperParamDeriv();

            if (usegradient)
            {
               RpropOpt(arVerbose,arOut,MaxIter,arGeps,arFeps,ilayer);  
            }
            else 
            {
               SimplexOpt(arVerbose,arOut,arMaxIter,ilayer);
            }

         }
      }

      /**
       *  @brief Returns the gradient along the direction of a hyper paramter using numerical differentiation
       *
       *  @param nparam is the number of hyper parameters
       *  @param aILayer is the Layer for which the derivatives should be calculated
       *
       **/

      std::vector<T> GetHderivNum
         (  int nparam
         ,  const In& aILayer
         )
      {

         const bool locdbg = false;

         std::vector<T> param_0 = mHparam[aILayer];
         std::vector<T> param_p = mHparam[aILayer];
         std::vector<T> param_m = mHparam[aILayer];

         // estimate good steep length as h = \sqrt(epsilon) * x for x != 0
         //T hstep = std::max(std::sqrt(std::numeric_limits<T>::epsilon()) * param_0[hidx], 10e-12) ; 
         T hstep = 1e-3; 

         std::vector<T> hgrad(nparam);

         for (Uin hidx = 0; hidx < nparam; hidx++)
         {
            // d/di J \approx (f(x0 + h) - f(x0 - h)) / (2h) 
            param_p[hidx] += hstep;
            param_m[hidx] -= hstep;

            this->SetHyperParameters(param_p,aILayer);
            T fp = GetMarginalLikelihood(aILayer);

            if (locdbg) Mout << "fp " << fp << std::endl;

            this->SetHyperParameters(param_m,aILayer);
            T fm = GetMarginalLikelihood(aILayer);

            if (locdbg) Mout << "fm " << fm << std::endl;

            // important reset to original values
            this->SetHyperParameters(param_0,aILayer);

            hgrad[hidx] =  (fp - fm) / (2.0 * hstep);
         }

         return hgrad;
      }


      /**
       *  @brief Returns the gradient along the direction of a hyper paramter
       *
       *  @param nparam is the number of hyper parameters 
       *  @param aWei is the weights for the GPR expansion
       *  @param aILayer is the layer for which ther derivatives should be calculated
       *
       **/
      std::vector<T> GetHderiv
         (  int& nparam 
         ,  std::unique_ptr<T[]>& aWei
         ,  const In& aILayer
         )
      {

         const bool DoPrtDeriv = false;
         const int maxsim = 5;

         // The elements of the gradient are given as
         // d/dhi = 0.5 * tr( (w w^T - K^-1) dK/dh_i )
         // with w = K^{-1} f(x)
      
         int nthreads = omp_get_max_threads();
         int nsim = std::min(std::min(nthreads,nparam),maxsim);

         if (mCmat.size() != mNumLayer) mCmat.resize(mNumLayer);
         if (mBmat.size() != mNumLayer) mBmat.resize(mNumLayer);

         if (mBmatSize.size() != mNumLayer) mBmatSize.resize(mNumLayer,0);
         if (mCmatSize.size() != mNumLayer) mCmatSize.resize(mNumLayer,0);

         //--------------------------------------------------------+
         // allocate memory 
         //--------------------------------------------------------+
         if (mCmat[aILayer].size() == 0)
         {
            mCmat[aILayer].resize(nsim);
            for (Uin isim = 0; isim < nsim; isim++)
            {
               if (!mCmat[aILayer][isim] || mCmatSize[aILayer] != mNumData*mNumData*nsim)
               {
                  mCmat[aILayer][isim].reset(new T[mNumData*mNumData]);
                  mCmatSize[aILayer] = mNumData*mNumData*nsim;
               }
            }
         }
         if (mBmat[aILayer].size() == 0)
         {
            mBmat[aILayer].resize(nsim);
            for (Uin isim = 0; isim < nsim; isim++)
            {
               if (!mBmat[aILayer][isim] || mBmatSize[aILayer] != mNumData*mNumData*nsim) 
               {
                  mBmat[aILayer][isim].reset(new T[mNumData*mNumData]);
                  mBmatSize[aILayer] = mNumData*mNumData*nsim;
               }
            }
         }
   
         // return vector
         std::vector<T> hgrad(nparam);

         int mthreads = nthreads - nsim + 1;


         #pragma omp parallel for num_threads(nsim) \
           default(none) \
           shared(mCmat,mBmat,zero,one,mNumData,hgrad,nparam,aWei,Mout,mthreads,aILayer)
         for (int hidx = 0; hidx < nparam; hidx++)
         {

            int ithrd = omp_get_thread_num();

            //--------------------------------------------------------+
            // calculate dK/dh_i 
            //--------------------------------------------------------+
            mKernel[aILayer].ComputeHderiv(mCmat[aILayer][ithrd], hidx, mXdata, mXdata, mthreads);         
       
            if (DoPrtDeriv) 
            { 
               int n = mNumData;
               int inc = 1;
               T hnorm = midas::lapack_interface::dot(&n, mCmat[aILayer][ithrd].get(), &inc, mCmat[aILayer][ithrd].get(), &inc );
               T hsum = C_0;
               for (int i = 0; i < n; i++){hsum += mCmat[aILayer][ithrd][i];}
               Mout << "hidx: " << hidx << " norm " << hnorm << " sum " << hsum  << std::endl; 
               prtmat(mCmat[aILayer][ithrd],mNumData,mNumData);
            }

            //--------------------------------------------------------+
            // calculate w^T dK/dh_i 
            //--------------------------------------------------------+
            std::unique_ptr<T[]> wdK(new T[mNumData]);
            {
               int n = 1;
               int m = mNumData;
               int k = mNumData;

               midas::lapack_interface::gemm( &nt, &nc, &n, &m, &k, &one 
                                            , aWei.get(), &m, mCmat[aILayer][ithrd].get(), &m 
                                            , &zero, wdK.get(), &n );   
            }

            //--------------------------------------------------------+
            // calculate B = w (w^T dK/dh_i) 
            //--------------------------------------------------------+
            {
               int m = mNumData;
               int k = 1;

               midas::lapack_interface::gemm( &nc, &nt, &m, &m, &k, &one 
                                            , aWei.get(), &m, wdK.get(), &m 
                                            , &zero, mBmat[aILayer][ithrd].get(), &m );   
            }

            //--------------------------------------------------------+
            // calculate C = K^{-1} dK/dh_i 
            //--------------------------------------------------------+
            std::unique_ptr<T[]> diag(new T[mNumData]);

            // Solve linear system of equations to get C(x,x)
            int m = mNumData;
            int nrhs = mNumData;
            int info;

            switch(mCovAlgo)
            {
               case USELU :
               case USECHOL :
               case USESVD :
               case USEBKD :
               case USESPARSE:
               {
                  // Just calculate diagonal of K^{-1} dK/dh_i
                  DiagMatMatProd(diag,mKinv[aILayer],mCmat[aILayer][ithrd]);
                  break;
               }
               case USECONJGRAD :
               {
                  this->ConjGrad(mSolutVector[aILayer], mTrialVector[aILayer], mCov[aILayer], mCmat[aILayer][ithrd], 1e-12, 1000, false, aILayer);
                  for (Uin idata = 0; idata < mNumData; idata++)
                  {
                     mCmat[aILayer][ithrd][idata] = mSolutVector[aILayer][idata];
                  }
                  break;
               }
               default:
               {
                  MIDASERROR("Unknown algorithm to decompose covariance matrix!");
               }
            }

            //--------------------------------------------------------+
            // calculate 0.5 tr( B - C )
            //--------------------------------------------------------+
            T trace = C_0;
            if (mCovAlgo == USECONJGRAD)
            {
               for (int idx = 0; idx < mNumData; idx++)
               {
                  trace += mBmat[aILayer][ithrd][idx*mNumData + idx] - mCmat[aILayer][ithrd][idx*mNumData + idx];
               }
            }
            else
            {
               for (int idx = 0; idx < mNumData; idx++)
               {
                  trace += mBmat[aILayer][ithrd][idx*mNumData + idx] - diag[idx];
               }
            }
   
            trace *= 0.5;
            hgrad[hidx] = trace;   
         }    

         return hgrad; 
      }

      /**
       *  @brief Calculation of the Hessian
       *
       *  Purpose: Calculates the Hessian for a given point.
       *
       *  @param hessian is a pointer to the space for the predicted Hessian.      
       *  @param point holds the structure for the point for which the Hessian should be calculated.
       *  @param aILayer is the layer for which the Hessian should be computed
       *
       **/
      void GetHessian( std::unique_ptr<T[]>& hessian
                     , MLDescriptor<T>& point
                     , const In& aILayer = 0
                     )
      {
         int ndim  = mXdata[0].size();

         if (aILayer != 0) MIDASERROR("Returning the Hessian (the old way) in GauPro only works with one Layer!");

         //--------------------------------------------------------+
         // Calculate Covariance Matrix K and do LU decomp on it
         //--------------------------------------------------------+
         SetupCovarianceMatrix(aILayer);

         //--------------------------------------------------------+
         // calculate w = K^{-1} f(x) 
         //--------------------------------------------------------+
         std::unique_ptr<T[]> wei(new T[mNumData]);
         GetWeights(wei, true, aILayer);

         //CorrectWeights(wei);

         // init hessian
         std::memset(hessian.get(), C_0, ndim*ndim);

         //--------------------------------------------------------+
         // calculate H = sum_k w_k H_k 
         //--------------------------------------------------------+
         std::unique_ptr<T[]> hess(new T[ndim*ndim]);
         int n = ndim*ndim;
         int inc = 1;

         for (int kdx = 0; kdx < mNumData; kdx++)
         {
            T da = wei[kdx];
            mKernel[aILayer].GetHessian(hess, point, mXdata[kdx]);
            midas::lapack_interface::axpy(&n, &da, hess.get(), &inc, hessian.get(), &inc); 
         }
         
      }


      bool HaveGradient
         (  const In& aILayer = 0
         )
      {
         return mKernel[aILayer].HaveGradient();
      }

      bool HaveHessian
         (  const In& aILayer = 0
         )
      {
         return mKernel[aILayer].HaveHessian();
      }

      std::vector<T> GetHyperParameters
         (  const In& aILayer = 0
         )
      {
         return mKernel[aILayer].GetHyperParameters();
      }

      void SetHyperParameters
         (  const std::vector<T>& hparam
         ,  const In& aILayer = 0
         )
      {
         mKernel[aILayer].SetHyperParameters(hparam);
         mHaveCov[aILayer] = false;
      }

      void StoreWeights(std::string aFileWeights)
      {
         In ilayer = 0;

         // Get Weights 
         std::unique_ptr<T[]> wei(new T[mNumData]);
         GetWeights(wei, true, ilayer);
      
         // Store them on file
         std::ofstream out;
         out.open (aFileWeights, ios::out | ios::trunc);
         out << "$WEIGHTS" << std::endl;
         T* pwei = wei.get();
         for (int iwei = 0; iwei < mNumData; iwei++)
         {
            out << std::scientific << std::setprecision(16) << *(pwei++) << std::endl;
         }
         out.close();
      }

      void ReadWeights
         (  std::unique_ptr<T[]>& aWei
         ,  std::string aFileWeights
         )
      {
         std::string s;

         // Allocate memory for Weights 
         aWei.reset(new T[mNumData]);
      
         // Read them from file
         std::ifstream input;
         input.open(aFileWeights, ios::in);
         
         if (input.good())
         {
            while(midas::input::GetLine(input, s) )
            {
               std::string datagrp = midas::util::FromString<std::string>(s);

               if ( datagrp == "$WEIGHTS" )
               {
                  for (int idata = 0; idata < mNumData; idata++)
                  {
                     midas::input::GetLine(input, s);
                     T wei = midas::util::FromString<T>(s);
                     aWei[idata] = wei;
                  }
               }
            }
         }
      }

   private:

      /**
       *  @brief Sets the data for a certain layer if multi layer GPR is used 
       *
       * */
      void SetDeltaData
         (  const In& aILayer
         )
      {
         // Sanity Check
         if (aILayer > mNumLayer) MIDASERROR("GPR: Current layer index exceeds number of layer"); 

         // quick exit
         if (aILayer == 0) return;

         In imode = 0;

         for (const MLDescriptor<T>& elem : mXdata) 
         {
            imode = std::max(imode,elem.GetDerivativeOrder());
         }

         // Get Prediction from last layer
         auto gpout = this->Predict(mXdata,imode,true,false,std::unique_ptr<T[]>(nullptr),true,aILayer);

         std::vector<T> meanval = std::get<0>(gpout);
         std::vector<T> delta(mNumData);

         mNoise[aILayer].resize(mNumData);

         this->SetHyperParameters(mHparam[aILayer-1],aILayer);

         T av_error = C_0;
         for (int idx = 0; idx < mNumData; idx++)
         {
            delta[idx] = (mYdata[0][idx] - meanval[idx]);
            av_error = std::abs(delta[idx]);

            mNoise[aILayer][idx] = mNoise[0][idx];
         }

         mYdata[aILayer] = delta;
        
         av_error /= delta.size();

         if (av_error < 1.e-7) mActLayer = std::max(1,aILayer);

      }

      /**
       *  @brief Calculate the log of the determinant of the Gram Matrix using various algorithms 
       *
       * */
      T GetLogDet(const In& aILayer)
      {
         T logDet = 0.0;  

         switch(mCovAlgo)
         {
            case USELU :
            {
               // A = PLU
               // det(A) = det(P) det(L) det(U) = (-1)^s Prod_i L_ii Prod_i U_ii
               // log det(A) = log[(-1)^s] + sum_i log[L_ii] + sum_i log[U_ii]
               // Note: L_ii computed by dgetrf are unit therfore log[L_ii] = 0
               int nrowpermut = 0;
               for (int i = 0; i < mNumData; i++)
               {
                  logDet =  logDet + std::log(mCov[aILayer][i * mNumData + i]); 
                  if (mIpiv[aILayer][i] != i + 1)     // The +1 for Fortran index convention
                  {
                     nrowpermut++; 
                  }
               }
               if (nrowpermut % 2 == 0 )
               {
                  logDet = logDet;
               }
               else
               {
                  MidasWarning("Gaussian Proces: Negative determinant. I will go on but co-variance matrix is ill conditioned!");
               }
               break;
            }
            case USECHOL :
            {
               // A = L L^T
               // det(A) = det(L) det(L^T) = Prod_i L_ii
               // log det(A) = 2 sum_i log L_ii
               for (int i = 0; i < mNumData; i++)
               {
                  logDet += std::log(mCov[aILayer][i * mNumData + i]);
               }
               logDet = 2.0 * logDet;
               break;
            }
            case USESVD :
            {
               // A = U S V^T
               // det(A) = det(U) det(S) det(V^T) = det(S) since U and V^T are orthogonal matrices
               // log det(A) = sum S_ii
               for (int i = 0; i < mNumData; i++)
               {
                  logDet += std::log(mSval[aILayer][i]);
               }
               logDet = logDet;
               break;
            }
            case USECONJGRAD :
            {
               logDet = 0.0;
               MIDASERROR("Unfinished feature (Conjugate Gradient)");
               break;
            } 
            case USEBKD :
            {
               for (int i = 0; i < mNumData; i++)
               {
                  logDet += std::log(mCov[aILayer][i * mNumData + i]);
               }
               logDet = logDet;
               break;
            }
            case USESPARSE:
            {
               logDet = mLogDetApprox[aILayer];  
               break;
            }
            default :
            {
               MIDASERROR("Unknown algorithm to decompose covariance matrix!");
            }  

         }

         return logDet;
      }


      /**
       *  @brief Form Inverse square root of given matrix
       *
       * */
      void FormInverseSquareRoot
         (  std::unique_ptr<T[]>& aMat
         ,  const int& ndim
         ,  T& arLogDet
         ,  const bool& use_chol = true
         )
      {
         const bool use_fallback = true;

         int  m = ndim;
         int  info = 0;

         if (use_chol)
         {

            char diag = 'N';
 
            int maxiter = 5;
            int iter = 0;
            T jiter = 1e-9;

            info = 1;     
            std::unique_ptr<T[]> temp(new T[m*m]);
            memcpy(reinterpret_cast<void*>(temp.get()),
                   reinterpret_cast<void*>(aMat.get()), m*m*sizeof(T));
            do 
            {
               if (iter > 0)
               {
                  memcpy(reinterpret_cast<void*>(aMat.get()),
                         reinterpret_cast<void*>(temp.get()), m*m*sizeof(T));
                  for (int idx = 0; idx < m; idx++)
                  {
                     aMat[idx*m + idx] += jiter;
                  }
               }
 
             midas::lapack_interface::potrf(&uplo, &m, aMat.get(), &m, &info);

            } while (info != 0 && iter++ <= maxiter);


            if (info !=0 && use_fallback)
            {
               MidasWarning("Cholesky decompostion failed. I will do EVD instead!");

               memcpy(reinterpret_cast<void*>(aMat.get()),
                      reinterpret_cast<void*>(temp.get()), m*m*sizeof(T));

               FormInverseSquareRoot(aMat,ndim,arLogDet,false);
               return;
            }

            if (info != 0) MIDASERROR("GPR: Problem in potrf (FormInverse)");

            // Calculate estimate for log |K|
            arLogDet = 0;
            for (int i = 0; i < m; i++)
            {
               arLogDet += 2.0 * std::log(aMat[i*m + i]);
            }
 
            // Invert Choleksy decompostion
            midas::lapack_interface::trtri(&uplo, &diag, &m, aMat.get(), &m, &info);

            // Zero out not referenced values...
            for (int idx = 0; idx < m; ++idx)
            {
               for (int jdx = 0; jdx < idx; ++jdx)
               {
                  aMat[idx*m + jdx] = 0.0;      
               }
            }

         }
         else
         {
            char uplo = 'U';
            char job  = 'V';
            int lwork = -1;
            T   dlen  = 0.0;

            std::unique_ptr<T[]> temp1(new T[m*m]);
            std::unique_ptr<T[]> temp2(new T[m*m]);
            std::unique_ptr<T[]> eig(new T[m]);

            memcpy(reinterpret_cast<void*>(temp2.get()),
                   reinterpret_cast<void*>(aMat.get()), m*m*sizeof(T));

            // first run to get dimension
            midas::lapack_interface::syev( &job, &uplo, &m, temp2.get(), &m, eig.get(), &dlen, &lwork, &info);

            lwork = int(dlen);
            std::unique_ptr<T[]> work(new T[lwork]); 

            // actual run
            midas::lapack_interface::syev( &job, &uplo, &m, temp2.get(), &m, eig.get(), work.get(), &lwork, &info);

            if (info != 0) MIDASERROR("Problem in eigen decomposition!");

            // Calculate estimate for log |K|
            arLogDet = 0;
            for (int i = 0; i < m; i++)
            {
               if (eig[i] > 1e-14) arLogDet += std::log(eig[i]);
            }

            // inverse square root of diagonal matrix
            for (int idx = 0; idx < m; idx++)
            {
               if (eig[idx] > 1e-14) 
               {
                  eig[idx] = one / std::sqrt(eig[idx]);
               }
               else
               {
                  eig[idx] = zero;
               }
            }

            // multiply the eigenvector with the diagonal matrix of eigenvalues
            for (int jdx = 0; jdx < m; jdx++)
            {
               for (int idx = 0; idx < m; idx++)
               {
                  temp1[jdx*m + idx] = temp2[jdx*m + idx] * eig[jdx];
               }
            }

            // Build output matrix
            midas::lapack_interface::gemm( &nc, &nt, &m, &m, &m, &one
                                         , temp1.get(), &m, temp2.get(), &m
                                         , &zero, aMat.get(), &m );
            
         }
         
      }


      /**
       *  @brief Main solver for Sparse GPR  
       *
       * */
      void SparseSol
         (  std::unique_ptr<T[]>& aSol
         ,  const In& aILayer
         )
      {
         int m = mSizeSubSet;
         int n = mNumData;
         int nrhs = 1;
 
         std::vector<T> u(m);
         std::vector<T> v(n);

         // 1) u = \hat{K}^T y
         midas::lapack_interface::gemm( &nc, &nc, &m, &nrhs, &n, &one 
                                      , mCov[aILayer].get(), &m, aSol.get(), &n
                                      , &zero, &u[0], &m );    

         // 2) v = \hat{K} u
         midas::lapack_interface::gemm( &nt, &nc, &n, &nrhs, &m, &one 
                                      , mCov[aILayer].get(), &m, &u[0], &m
                                      , &zero, &v[0], &n );    

         // 3) w = (y - v) / sigma^2
         for (int idx = 0; idx < n; idx++)
         {
            aSol[idx] = (aSol[idx] - v[idx]) / mNoise[aILayer][idx];
         }
      }

      /**
       *  @brief Calculates the diagonal of a Matrix Matrix product in O(n^2) steps  
       *
       * */
      void DiagMatMatProd
         (  std::unique_ptr<T[]>& diag
         ,  std::unique_ptr<T[]>& aMat1
         ,  std::unique_ptr<T[]>& aMat2
         )
      {
         // Just calculate diagonal of K^{-1} dK/dh_i
         for (int idx = 0; idx < mNumData; idx++)
         {
            diag[idx] = 0.0;
            for (int jdx = 0; jdx < mNumData; jdx++)
            {
               diag[idx] += aMat1[jdx*mNumData + idx] * aMat2[jdx*mNumData + idx];
            }
         }
      }

      /**
       *  @brief Calculates the absoulte errors for the a prediction of the reference points  
       *
       * */
      std::vector<In> GetMaxErrorPoints
         (  const In& aNum
         )
      {
         std::unique_ptr<T[]> wei(new T[mNumData]);
         GetWeights(wei, false, 0);
         
         In imode = 0;

         for (const MLDescriptor<T>& elem : mXdata) 
         {
            imode = std::max(imode,elem.GetDerivativeOrder());
         }

         auto gpout = this->Predict(mXdata,imode);

         std::vector<T> meanval = std::get<0>(gpout);

         std::vector<T>   maxabs(mNumData);
         std::vector<In>  indices(mNumData);
         for (int idx = 0; idx < mNumData; idx++)
         {
            maxabs[idx] = std::abs(meanval[idx] - mYdata[0][idx]) * std::sqrt(std::abs(wei[idx]));
            indices[idx] = idx;
         }

         // Sort indices according to maxabs
         std::sort(indices.begin(), indices.end(),
            [&maxabs](size_t i, size_t j)
               {return maxabs[i] > maxabs[j];});


         // Only keep aNum elments
         indices.resize(aNum);

         return indices;
      }


      void prtmat
         (  std::unique_ptr<T[]>& mat
         ,  int nrow
         ,  int ncol
         )
      {

         ios::fmtflags oflags( Mout.flags() );  // save current output flags

         int maxcol, mincol, i, j;
         maxcol = 0;

         while (maxcol < ncol)
         {
            mincol = maxcol + 1;
            maxcol = std::min(maxcol + 5, ncol);
            Mout << std::endl << "    ";
            for (int j = mincol - 1; j < maxcol; ++j)
            {
               Mout << "          " << j << "    ";
            }
            Mout << std::endl;

            Mout << std::endl;
            for (int i = 0; i < nrow; ++i)
            {
               Mout << std::setw(5) << i << " ";
               for (int j = mincol - 1; j < maxcol; ++j)
               {
                  T dval = mat[j*nrow + i];
                  Mout << (dval >= 0 ? " ":"") << std::setprecision(8)  << std::showpoint << dval << " ";
               }
               Mout << std::endl;
            }
         }

         Mout.flags( oflags );   // restore output flags
      }

      void savemat
         (  std::unique_ptr<T[]>& mat
         ,  int& ndata
         )
      {
         ofstream fout ("covmat.txt");    

         ios::fmtflags oflags( fout.flags() );  // save current output flags
         
         int size = ndata*ndata;

         for(int i = 0; i < size; i++)
         {
            fout<< mat[i] << std::endl;
         }

         fout.flags( oflags );   // restore output flags
      }



      T simplefunct_hyp
         (  std::vector<T> hparam
         ,  const In& aILayer
         )
      {
         this->SetHyperParameters(hparam,aILayer);
         return -1.0 * GetMarginalLikelihood(aILayer);
      }


      void simplexopt
         (  std::vector< std::vector<T> > &point
         ,  std::vector<T> &yval
         ,  int ndim
         ,  T thrrel
         ,  T thrabs
         ,  const int& arMaxIter
         ,  int &iter
         ,  bool &converged
         ,  const In& aILayer
         )
      {
  
         //----------------------------------------------------------------------*
         // Purpose: search minimum of a multidimensional function which has to 
         //          be provided in simplefunct using downhill simplex algorithm
         //
         // Literature: Numerical Recipes; Press, Teukolsky, Vetterling, Flannery;
         //             Third Edition, 2007
         //
         //----------------------------------------------------------------------*


         const bool locdbg = false;
 
         static const T eps = std::numeric_limits<T>::epsilon();  
 
         T one = 1.0;
         T two = 2.0;
         T half = 0.5;
         T zero = 0.0; 
 
         T ytry  = 0.0;
         T ysave = 0.0;

         int ihigh, ilow, inexthi;

         T reldiff = 0.0;
         T absdiff = 0.0;
         T swap;
 
         //T thrshld = thrabs;
      
         iter = 0;
      
         std::vector<T> psum(ndim);
         std::vector<T> ptry(ndim);
      
         //---------------------------------------
         // determine the midpoint of the simplex:
         //---------------------------------------
         psum = point[1];
         for (int ipoint = 1; ipoint < ndim + 1; ipoint++)
         {
            for (int idx = 0; idx < ndim; idx++)
            {
               psum[idx] += point[ipoint][idx];
            } 
         }
      
         converged = false;
      
         while (iter < arMaxIter && !converged)
         {
            //----------------------------------------------------------
            // determine highest, next highest and lowest result values:
            //----------------------------------------------------------
            if (yval[0] > yval[1]) 
            {
               ihigh = 0;
               inexthi = 1;
            }
            else
            {
               ihigh = 1;
               inexthi = 0;
            }
            ilow = 0;
      
            for (int ipoint = 0; ipoint < ndim + 1; ipoint++)
            {
               if (yval[ipoint] <= yval[ilow]) ilow = ipoint;
               if (yval[ipoint] > yval[ihigh]) 
               {
                  inexthi = ihigh;
                  ihigh   = ipoint;
               }
               else if (yval[ipoint] > yval[inexthi] && ipoint != ihigh ) 
               {
                  inexthi = ipoint;
               }
            } 
            //----------------------------------------------------------
            // calculate the relative difference between the highest and
            // lowest result values and check for convergence:
            //----------------------------------------------------------
            absdiff = std::abs(yval[ihigh] - yval[ilow]);
            reldiff = absdiff / ( half * (std::abs(yval[ihigh]) + std::abs(yval[ilow]) + eps )); // add eps so that we never divide by zero
 
            if (locdbg) Mout << "absdiff " << absdiff << " reldiff " << std::endl;
 
            if (reldiff < thrrel ||  absdiff < thrabs)
            {
               converged = true;
      
               //--------------------------------------
               // swap minimal point to first position:
               //--------------------------------------
               swap       = yval[0];
               yval[0]    = yval[ilow];
               yval[ilow] = swap;
               point[0].swap(point[ilow]);
      
               break;
            }
      
            // set threshhold for function
            //thrshld = std::max(reldiff * (std::abs(yval[ihigh]) + std::abs(yval[ilow])) * 0.01, thrabs);
      
            //--------------------------------------------------------------
            // reflected highest point at the midpoint of remaining simplex:
            //--------------------------------------------------------------
            T fac1 = (one - (-one))/ndim;
            T fac2 = fac1 - (-one);
            for (int idx = 0; idx < ndim; idx++)
            {
               ptry[idx] = fac1 * psum[idx] - fac2 * point[ihigh][idx];
            }
            ytry = simplefunct_hyp(ptry,aILayer); 
            iter = iter + 1;
      
            if (ytry < yval[ihigh])
            { 
               // accept new point...
               yval[ihigh] = ytry;
               for (int idx = 0; idx < ndim; idx++)
               {
                  psum[idx] += ptry[idx] - point[ihigh][idx] ; 
                  point[ihigh][idx] = ptry[idx]; 
               }
            }
      
            //-----------------------------------------------
            // simplex expansion or contraction:
            //-----------------------------------------------
            if (ytry <= yval[ilow])
            {
               // new point better than all other points
               //  --> expand simplex in the direction of the new point
               //      by increasing the step
               T fac1 = (one-(two))/ndim;
               T fac2 = fac1-(two);
               for (int idx = 0; idx < ndim; idx++)
               {
                  ptry[idx] += fac1 * psum[idx] - fac2 * point[ihigh][idx];
               }
               ytry = simplefunct_hyp(ptry,aILayer); 
               iter = iter + 1;
      
               if (ytry < yval[ihigh])
               {
                  // accept new point...
                  yval[ihigh] = ytry;
                  for (int idx = 0; idx < ndim; idx++)
                  {
                     psum[idx] += ptry[idx] - point[ihigh][idx];
                     point[ihigh][idx] = ptry[idx];
                  }
               }
            }
            else if (ytry >= yval[inexthi])
            {
               // not a significant improvement
               //  --> contract simplex in the present search direction
               //      by decreasing the step
               ysave = yval[ihigh];
               T fac1 = (one - (half)) / ndim;
               T fac2 = fac1 - (half);
               for (int idx = 0; idx < ndim; idx++)
               {
                  ptry[idx] += fac1 * psum[idx] - fac2 * point[ihigh][idx];
               }
               ytry = simplefunct_hyp(ptry,aILayer); 
               iter = iter + 1;
      
               if (ytry < yval[ihigh])
               {
                  // accept new point...
                  yval[ihigh] = ytry;
                  for (int idx = 0; idx < ndim; idx++)
                  {
                     psum[idx] += ptry[idx] - point[ihigh][idx];
                     point[ihigh][idx] = ptry[idx];
                  }
               }
      
               // if the result got worse, contract the whole simplex
               // around the present optimal point:
               if (ytry >= ysave) 
               {
                  for (int ipoint = 0; ipoint < ndim + 1; ipoint++)
                  {
                     if (ipoint != ilow) 
                     {
                        for (int idx = 0; idx < ndim; idx++)
                        {
                           point[ipoint][idx] = psum[idx] = half * (point[ipoint][idx] +  point[ilow][idx] );
                        }
                        yval[ipoint] = simplefunct_hyp(psum,aILayer); 
                     }
                  }
                  iter = iter + ndim;
      
                  // determine midpoint of contracted simplex:
                  for (int idx = 0; idx < ndim; idx++)
                  {
                     T sum = 0.0;
                     for (int ipoint = 0; ipoint < ndim + 1; ipoint++)
                     {
                        sum += point[ipoint][idx];
                     } 
                     psum[idx] = sum;
                  }
               }
            }
      
         }
      }

};


#endif /* GAUPRO_INCLUDED */
