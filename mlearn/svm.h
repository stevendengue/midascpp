/**
************************************************************************
* 
* @file                
*
* Created:            11-12-2017         
*
* Author:             Gunnar Schmitz (gunnar.schmitz@chem.au.dk)  
*
* Short Description:  Class for Support Vector Machines.
*                     The implementation follows mainly 
*                     Numerical Recipes Third edition pg. 894
* 
* Last modified:
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef SVM_INCLUDED
#define SVM_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <algorithm>
#include <random>

#include "mlearn/GPKernel.h"

template <class T>
class svm
{

   private:

      std::vector<std::vector<T>> mXdata;  ///> Training data (descriptor) 
      std::vector<T> mYdata;               ///> Training data (property)

      GPKernel<T> mKernel;               ///> Used Kernel
      std::unique_ptr<T[]> mGramMat;     ///> Space for the Gram matrix. 

      bool mIsAlphInit = false;   
      std::vector<T> mAlpha;
      std::vector<T> mAlphaOld;

      int mNdata = 0;  

      std::default_random_engine mGenRand;
      std::uniform_int_distribution<int> mRandDist; 
 
   public:


      ///> Constructor(s) 
      svm(  GPKernel<T>& kern
         ,  std::vector<std::vector<T>>& xdata
         ,  std::vector<T>& ydata
         )
      {
         mXdata = xdata;
         mYdata = ydata;
         mKernel = kern;

         int ndata = mXdata.size();
         
         mGramMat.reset(new T[ndata*ndata]);
         mKernel.Compute(mGramMat, mXdata, mXdata);

         mNdata = ndata;
         mAlpha.resize(ndata);
         mAlphaOld.resize(ndata);
      
         mRandDist = std::uniform_int_distribution<int>(0,4294967295);
      }

      ///> Delete copy- and move constructor
      svm(const svm&)  = delete;
      svm(svm&&)       = delete;
    
      ///> Destructor
      ~svm() = default;


      T relax( T lambda
            ,  T om) 
      {
         T dalph;
         int iter, j, jj, k, kk;
         int fnz,fub;
         Nb sum;
         std::vector<Nb> pinsum(mNdata);

         if (mIsAlphInit == false) 
         {
            for (j = 0; j < mNdata; j++) 
            {
               mAlpha[j] = 0.0;
            }
            mIsAlphInit = true;
         }

         mAlphaOld = mAlpha;

         std::vector<size_t> xindx = sort_indexes(mAlpha);
  
         for (fnz = 0; fnz < mNdata; fnz++) 
         {
            if (mAlpha[xindx[fnz]] != 0.0) break;
         }

         for (j = fnz; j < mNdata - 2; j++) 
         { 
            k = j + (mRandDist(mGenRand) % (mNdata - j));
            std::swap(xindx[j], xindx[k]);
         }

         for ( jj = 0; jj < mNdata; jj++) 
         {
            j = xindx[jj];
            sum = 0.0;

            for (kk = fnz; kk < mNdata; kk++) 
            {
               k = xindx[kk];
               sum += (mGramMat[k * mNdata +  j] + 1.0) * mYdata[k] * mAlpha[k];
            }

            mAlpha[j] = mAlpha[j] - (om / (mGramMat[j * mNdata + j] + 1.0)) * (mYdata[j] * sum - 1.0);
            mAlpha[j] = std::max(0.0, std::min(lambda, mAlpha[j]));
            if (jj < fnz && mAlpha[j]) std::swap(xindx[--fnz], xindx[jj]);
         }

         std::vector<size_t> yindx = sort_indexes(mAlpha);

         for (fnz = 0; fnz < mNdata; fnz++) 
         {
            if (mAlpha[yindx[fnz]] != 0.0) break;
         }
  
         for (fub = fnz; fub < mNdata; fub++) 
         {
            if (mAlpha[yindx[fub]] == lambda) break;
         }

         for (j = fnz; j < fub - 2; j++) 
         {
            k = j + (mRandDist(mGenRand) % (fub-j));
            std::swap(yindx[j],yindx[k]);
         }

         for (jj = fnz; jj < fub; jj++) 
         {
            j = yindx[jj];
            sum = 0.0;
            for (kk = fub; kk < mNdata; kk++) 
            {
               k = yindx[kk];
               sum += (mGramMat[k * mNdata + j] + 1.0) * mYdata[k] * mAlpha[k];
            }
            pinsum[jj] = sum;
         }

         int niter = std::max(int(0.5*(mNdata+1.0)*(mNdata-fnz+1.0) / ( std::pow(fub-fnz+1.0,2.0))), 1);

         for (iter = 0; iter < niter; iter++) 
         {
            for (jj = fnz; jj < fub; jj++) 
            {
               j = yindx[jj];
               sum = pinsum[jj];
               for (kk = fnz; kk < fub; kk++) 
               {
                  k = yindx[kk];
                  sum += (mGramMat[k * mNdata + j] + 1.0) * mYdata[k] * mAlpha[k];
               }
               mAlpha[j] = mAlpha[j] - (om/(mGramMat[j * mNdata + j] + 1.0)) * (mYdata[j] * sum - 1.0);
               mAlpha[j] = std::max(0.0, std::min(lambda,mAlpha[j]));
            }
         }

         dalph = 0.0;
         for (j = 0; j < mNdata; j++) 
         {  
            dalph += std::pow(mAlpha[j] - mAlphaOld[j], 2.0);
         }

         return sqrt(dalph);
      }


      T predict(int k) 
      {
         T sum = 0.;

         for (int j = 0; j < mNdata; j++) 
         {
            sum += mAlpha[j] * mYdata[j] * (mGramMat[k * mNdata + j] + 1.0);
         }
         return sum;
      }
      
      T predict(std::vector<T>& XPoint) 
      {
         T sum = 0.;
         for (int j = 0; j < mNdata; j++) 
         {
            sum += mAlpha[j] * mYdata[j] * ( mKernel.Compute(mXdata[j], XPoint) + 1.0);
         }
         return sum;
      }


   private:

      std::vector<size_t> sort_indexes(const std::vector<T> &v) 
      {

         // initialize original index locations
         vector<size_t> idx(v.size());
         iota(idx.begin(), idx.end(), 0);
  
         // sort indexes based on comparing values in v
         sort(idx.begin(), idx.end(),[&v](size_t i1, size_t i2) {return v[i1] < v[i2];});
  
         return idx;
     }

};


#endif /* SVM_INCLUDED */
