

#ifndef POLYNOM_INCLUDED
#define POLYNOM_INCLUDED

#include "mlearn/GPKernelBase.h"


template <class T>
class POLYNOM_KERN : public GPKernelBase<T>
{

   private:

      const bool mHaveHyperParamDeriv = true;
      const bool mHaveKernelDeriv = true;
      const bool mHaveKernelHessian = false;

      std::vector<T> mHparam;

   public:

      ///> Constructor 
      POLYNOM_KERN 
         (  std::vector<T>& hparam
         )
      {
         mHparam = hparam;
      }

      ///> Destructor
      ~POLYNOM_KERN() = default;

      std::string GetName()
      {
         return "Polynominal";
      }

      bool HaveHyperParamDeriv()   {return mHaveHyperParamDeriv;}
      bool HaveGradient()          {return mHaveKernelDeriv;}
      bool HaveHessian()           {return mHaveKernelHessian;}

      std::vector<T> GetHyperParameters()            {return mHparam;}

      void SetHyperParameters(const std::vector<T>& hparam)
      {
         mHparam.assign(hparam.begin(), hparam.end());
      }

      /**
       *  @brief Computes kernel function for a given set of points 
       *
       *  Purpose: Computes the kernel function for a given set of points
       *           xi and xj
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  addnoise specifies if the noise term is added
       * @param  noiseval specifies the added noise 
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T Compute
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const bool &addnoise
         ,  const T& noiseval
         ,  const int& lderi
         ,  const int& lderj
         )
      {
         int nsize = 0;

         // sanity check(s)
         if (xi.size() == xj.size())
         {
            nsize = xi.size();
         }
         else
         {
            Mout <<  xi.size() << " vs " << xj.size() << std::endl;
            MIDASERROR("Dimensions of points do not agree!");
         }

         T sig   = mHparam[0];
         T alpha = mHparam[1];
         T dexp  = mHparam[2];
         T cadd  = mHparam[3];
         T noise = C_0;
         if (addnoise) noise = noiseval;

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += xi[i]*xj[i];
         }

         return sig * sig * std::pow((alpha * r + cadd),dexp) + noise;

      }

      /**
       *  @brief Computes derivatvie of the kernel function w.r.t hyper parameters 
       *
       *  Purpose: Computes the derivatvie of the kernel function for a given set of points
       *           xi and xj w.r.t hyper parameter hidx
       *
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * @param  hidx index of the hyper parameter
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xk
       * */
      T ComputeHderiv
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xk
         ,  const int& hidx
         ,  const int& lderi
         ,  const int& lderj
         )
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);

         T sig   = mHparam[0];
         T alpha = mHparam[1];
         T dexp  = mHparam[2];
         T cadd  = mHparam[3];
         T sig2  = sig * sig;

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] * xk[i]);
         }

         T ret = C_0;
         switch(hidx)
         {
            case(0):
            {
               ret = 2.0 * sig * std::pow((alpha * r + cadd),dexp);
               break;
            }
            case(1):
            {
               ret =  sig2 * r * dexp * std::pow((alpha * r + cadd) ,dexp - 1.0) ;
               break;
            }
            case(2):
            {
               ret = sig2 * std::pow((alpha * r + cadd),dexp) * std::log(alpha * r + cadd);
               break;
            }
            case(3):
            {
               ret = sig2 * dexp * std::pow((alpha * r + cadd) ,dexp - 1.0);
               break;
            }
         }

         return ret;
      }

      /**
       *  @brief Computes the gradient at the point xi 
       *
       *  Purpose: Computes the geometrical gradient at point xi
       *
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      std::vector<T> Gradient
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xk
         )
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);

         std::vector<T> grad(nsize);

         T sig   = mHparam[0];
         T alpha = mHparam[1];
         T dexp  = mHparam[2];
         T cadd  = mHparam[3];
         T sig2  = sig * sig;

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            grad[i] = C_0;
            r += (xi[i] * xk[i]);
         }


         for (int i = 0; i < nsize; ++i)
         {
            grad[i] = sig2 * alpha * dexp * xk[i] * std::pow((alpha * r + cadd) ,dexp - 1.0)  ; 
         }

         return grad;
      }

      /**
       *  @brief Computes the geometrical Hessian 
       *
       *  Purpose: Computes the geometrical Hessian
       *         
       * @param  hessian is a unique_ptr to the memory for the hessian
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      void GetHessian
         (  std::unique_ptr<T[]>& hessian
         ,  const MLDescriptor<T>& xi 
         ,  const MLDescriptor<T>& xk 
         )
      {
         MIDASERROR("Hessian not implemented for this kernel!");
      }

};


#endif /* KERNEL_INCLUDED */
