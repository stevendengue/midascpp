

#ifndef SQEXP_INCLUDED
#define SQEXP_INCLUDED

#include "mlearn/GPKernelBase.h"


template <class T>
class SQEXP_KERN : public GPKernelBase<T>
{

   private:

      const bool mHaveHyperParamDeriv = true;
      const bool mHaveKernelDeriv = true;
      const bool mHaveKernelHessian = true;

      std::vector<T> mHparam;

   public:

      ///> Constructor 
      SQEXP_KERN 
         (  std::vector<T>& hparam
         )
      {
         mHparam = hparam;
      }

      ///> Destructor
      ~SQEXP_KERN() = default;

      std::string GetName()
      {
         return "Squared Exponential";
      }

      bool HaveHyperParamDeriv()   {return mHaveHyperParamDeriv;}
      bool HaveGradient()          {return mHaveKernelDeriv;}
      bool HaveHessian()           {return mHaveKernelHessian;}

      std::vector<T> GetHyperParameters()            {return mHparam;}

      void SetHyperParameters(const std::vector<T>& hparam)
      {
         mHparam.assign(hparam.begin(), hparam.end());
      }

      /**
       *  @brief Computes kernel function for a given set of points 
       *
       *  Purpose: Computes the kernel function for a given set of points
       *           xi and xj
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  addnoise specifies if the noise term is added
       * @param  noiseval specifies the added noise 
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T Compute
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const bool &addnoise
         ,  const T& noiseval
         ,  const int& lderi
         ,  const int& lderj
         )
      {
         int imode = 10 * lderi + lderj;

         int icoord,jcoord,kcoord,lcoord;
         int np;

         T kernel = C_0;

         switch (imode)
         {
            case (0):
            {
               // Energy/Energy datapoint
               kernel = sqexp_kern00(xi,xj);
               break;
            }
            case (10):
            case (1):
            {
               // Energy/Gradient datapoint
               icoord = 0;

               if (imode == 10) 
               {
                  icoord = xi.GetDerivCoord1();
                  np = 0;
               }
               if (imode == 1) 
               {
                  icoord = xj.GetDerivCoord1(); 
                  np = 1;
               }

               kernel = sqexp_kern10(xi,xj,icoord,np);
               break;
            }
            case (11):
            case (20):
            case (2):
            {
               // Gradient/Gradient or Energy Hessian datapoint
               if (imode == 11)
               {
                  jcoord = xj.GetDerivCoord1();
                  icoord = xi.GetDerivCoord1();
                  np = 1;
               }
               else if (imode == 20)
               {
                  jcoord = xi.GetDerivCoord2();
                  icoord = xi.GetDerivCoord1();
                  np = 0;
               }
               else if (imode == 2)
               {
                  jcoord = xj.GetDerivCoord2();
                  icoord = xj.GetDerivCoord1();
                  np = 2;
               }

               kernel = sqexp_kern20(xi,xj,jcoord,icoord,np);
               break;
            }
            case (12):
            case (21):
            {
               if (imode == 12) 
               {
                  icoord = xi.GetDerivCoord1();
                  jcoord = xj.GetDerivCoord1();
                  kcoord = xj.GetDerivCoord2();
                  np = 2;
               }
               else
               {
                  icoord = xi.GetDerivCoord1();
                  jcoord = xi.GetDerivCoord2();
                  kcoord = xj.GetDerivCoord1();
                  np = 1;
               }
               kernel = sqexp_kern30(xi,xj,kcoord,jcoord,icoord,np);
               break;
            }
            case (22):
            {
               // Hessian/Hessian datapoint
               lcoord = xi.GetDerivCoord2();
               kcoord = xi.GetDerivCoord1();
               jcoord = xj.GetDerivCoord2();
               icoord = xj.GetDerivCoord1();
               np = 2;

               kernel = sqexp_kern40(xi,xj,lcoord,kcoord,jcoord,icoord,np);
               break;
            }
            default:
            {
               Mout << "imode " << imode << std::endl;
               MIDASERROR("sqexp_kernel:> Unknown computation mode!");
               break;
            }
         }

         T noise = C_0;
         if (addnoise) noise = noiseval;

         kernel += noise;

         return kernel;
      }

      inline T sqexp_kern00
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         )
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xj);
      
         T sig  = mHparam[0];
         T l    = mHparam[1];
         T sig2 = sig*sig;
         T l2   = l*l;
      
         T r = (xi[0] - xj[0]) * (xi[0] - xj[0]); 
         for (int i = 1; i < nsize; ++i) 
         {
            r += (xi[i] - xj[i]) * (xi[i] - xj[i]); 
         }
      
         return sig2 * exp(-0.5 * r / l2 );
      }
      
      inline T sqexp_kern10
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& icoord
         ,  const int& nj
         )
      {
         T l2 = mHparam[1] * mHparam[1];  
      
         T sign = std::pow(-1.0,nj);
      
         T k00 = sqexp_kern00(xi,xj);
      
         return sign * -1.0 * (xi[icoord] - xj[icoord]) / l2 * k00;
      }
      
      inline T sqexp_kern20
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& jcoord
         ,  const int& icoord
         ,  const int& np
         )
      {
      
         int n0 = 0;
      
         T sign = std::pow(-1.0,np);
      
         T l2 = mHparam[1] * mHparam[1];
         T k10  = sqexp_kern10(xi,xj,jcoord,n0);
         T ret = - (xi[icoord] - xj[icoord]) / l2 * k10;
      
         if (icoord == jcoord)
         {
            T k00 = sqexp_kern00(xi,xj);
      
            ret += - k00 / l2;
         }
      
         return sign * ret;
      }
      
      inline T sqexp_kern30
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& kcoord
         ,  const int& jcoord
         ,  const int& icoord
         ,  const int& nj
         )
      {
      
         int n0 = 0;
      
         T sign = std::pow(-1.0,nj);
      
         T l2  = mHparam[1] * mHparam[1];
         T k20  = sqexp_kern20(xi,xj,kcoord,jcoord,n0);
      
         T ret = - (xi[icoord] - xj[icoord]) / l2 * k20;
      
         if (icoord == kcoord)
         {
            T k10j = sqexp_kern10(xi,xj,jcoord,n0);
            ret += - k10j / l2;
         }
      
         if (icoord == jcoord)
         {
            T k10k = sqexp_kern10(xi,xj,kcoord,n0);
            ret += - k10k / l2;
         }  
      
         return sign * ret; 
      }
      
      inline T sqexp_kern40
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& lcoord
         ,  const int& kcoord
         ,  const int& jcoord
         ,  const int& icoord
         ,  const int& np
         )
      {
      
         int n0 = 0;
      
         T sign = std::pow(-1.0,np);
      
         T l2 = mHparam[1] * mHparam[1];
         T k30  = sqexp_kern30(xi,xj,lcoord,kcoord,jcoord,n0);
      
         T ret = - (xi[icoord] - xj[icoord]) / l2 * k30;
      
         if (icoord == lcoord)
         {
            T k20kj = sqexp_kern20(xi,xj,kcoord,jcoord,n0);
            ret += - k20kj / l2;
         }  
      
         if (icoord == kcoord)
         {
            T k20lj = sqexp_kern20(xi,xj,lcoord,jcoord,n0);
            ret += - k20lj / l2;
         }
      
         if (icoord == jcoord)
         {
            T k20lk = sqexp_kern20(xi,xj,lcoord,kcoord,n0);
            ret += - k20lk / l2;
         }
      
         return sign * ret;
      }

      /**
       *  @brief Computes derivatvie of the kernel function w.r.t hyper parameters 
       *
       *  Purpose: Computes the derivatvie of the kernel function for a given set of points
       *           xi and xj w.r.t hyper parameter hidx
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  hidx index of the hyper parameter
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T ComputeHderiv
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& hidx
         ,  const int& lderi
         ,  const int& lderj
         )
      {
         int imode = 10 * lderi + lderj;
         //int imode = 10 * xi.GetDerivativeOrder() + xj.GetDerivativeOrder();

         int icoord,jcoord,kcoord,lcoord;
         int np;

         T hderiv = C_0;

         switch (imode)
         {
            case (0):
            {
               // Energy/Energy datapoint
               np = 0;
               hderiv = sqexp_h10(xi,xj,hidx,np);
               break;
            }
            case (10):
            case (1):
            {
               // Energy/Gradient datapoint
               icoord = 0;
               np = 0;

               if (imode == 10) {icoord = xi.GetDerivCoord1();}
               if (imode == 1) 
               {
                  icoord = xj.GetDerivCoord1(); 
                  np = 1;
               }

               hderiv = sqexp_h20(xi,xj,icoord,hidx,np);
               break;
            }
            case (11):
            case (20):
            case (2):
            {
               // Gradient/Gradient or Energy Hessian datapoint
               if (imode == 11)
               {
                  jcoord = xj.GetDerivCoord1();
                  icoord = xi.GetDerivCoord1();
                  np = 1;
               }
               else if (imode == 20)
               {
                  jcoord = xi.GetDerivCoord2();
                  icoord = xi.GetDerivCoord1();
                  np = 0;  
               }
               else // if (imode == 2)
               {
                  jcoord = xj.GetDerivCoord2();
                  icoord = xj.GetDerivCoord1();
                  np = 2;
               }

               hderiv = sqexp_h30(xi,xj,jcoord,icoord,hidx,np);
               break;
            }
            case (12):
            case (21):
            {
               // Gradient/Hessian datapoint
               if (imode == 12) 
               {
                  icoord = xi.GetDerivCoord1();
                  jcoord = xj.GetDerivCoord1();
                  kcoord = xj.GetDerivCoord2();
                  np = 2;
               }
               else
               {
                  icoord = xi.GetDerivCoord1();
                  jcoord = xi.GetDerivCoord2();
                  kcoord = xj.GetDerivCoord1();
                  np = 1;
               }
               hderiv = sqexp_h40(xi,xj,kcoord,jcoord,icoord,hidx,np);
               break;
            }
            case (22):
            {
               // Hessian/Hessian datapoint
               lcoord = xj.GetDerivCoord2();
               kcoord = xj.GetDerivCoord1();
               jcoord = xi.GetDerivCoord2();
               icoord = xi.GetDerivCoord1();
               np = 2;

               hderiv = sqexp_h50(xi,xj,lcoord,kcoord,jcoord,icoord,hidx,np);
               break;
            }
            default:
            {
               Mout << "imode " << imode << std::endl;
               MIDASERROR("sqexp_hderiv:> Unknown computation mode!");
               break;
            }
         }

         return hderiv;
      }

      inline T sqexp_h10
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& hidx
         ,  const int& np
         )
      {
      
         T k00 = sqexp_kern00(xi,xj);
      
         //T sign = std::pow(-1.0,np);
         T sign = (np % 2 == 0) ? 1.0 : -1.0;
      
         T ret;
         switch(hidx)
         {
            case(0):
            {
               T sig = mHparam[0];
               ret = 2.0 * k00 / sig;
               break;
            }
            default:
            {
               int nsize = this->ValidateArguments(xi,xj);
               T l = mHparam[1];
               T l3 = l * l * l;
      
               T r2 = C_0;
               for (int i = 0; i < nsize; ++i) 
               {
                  r2 += (xi[i] - xj[i]) * (xi[i] - xj[i]);
               }
               ret = r2 / l3 * k00;
               break;
            }
         }
      
         return sign * ret;
      }
      
      inline T sqexp_h20
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& icoord
         ,  const int& hidx
         ,  const int& np
         )
      {
         int n0 = 0;
      
         //T sign = std::pow(-1.0,np);
         T sign = (np % 2 == 0) ? 1.0 : -1.0;
      
         T ret;
         switch(hidx)
         {
            case(0):
            {
               T sig = mHparam[0];
               T k10 = sqexp_kern10(xi,xj,icoord,n0);
      
               ret = 2.0 * k10 / sig;
               break;
            }
            default:
            {
               T l  = mHparam[1];
               T l2 = l * l;
      
               //-------------------------------------------------------------------+
               //  -l^{-2} (x_i-x_i') d^2 k(x,x')/ dl
               //-------------------------------------------------------------------+
               T h10  = sqexp_h10(xi,xj,hidx,n0);
               T dxij = (xi[icoord] - xj[icoord]);
       
               ret = - dxij / l2 * h10 ;
      
               //-------------------------------------------------------------------+
               //  2 l^{-3} (x_i-x_i')  k(x,x')
               //-------------------------------------------------------------------+
               T l3 = l2 * l;
               T k00  = sqexp_kern00(xi,xj);
               ret += 2.0 / l3 * dxij * k00;
      
               break;
            }
         }
      
         return sign * ret;
      }
      
      inline T sqexp_h30
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& jcoord
         ,  const int& icoord
         ,  const int& hidx
         ,  const int& np
         )
      {
         int n0 = 0;
      
         //T sign = std::pow(-1.0,np);
         T sign = (np % 2 == 0) ? 1.0 : -1.0;
      
         T ret;
         switch(hidx)
         {
            case(0):
            {
               T sig = mHparam[0];
               T k20 = sqexp_kern20(xi,xj,jcoord,icoord,n0);
      
               ret = 2.0 * k20 / sig;
               break;
            }
            default:
            {
               T l  = mHparam[1];
               T l2 = l * l;
      
               //-------------------------------------------------------------------+
               //  -l_i^{-2} (x_i-x_i') d^2 k(x,x')/ dl dx_j
               //-------------------------------------------------------------------+
               T h20  = sqexp_h20(xi,xj,jcoord,hidx,n0);
               T dxij = xi[icoord]-xj[icoord];
      
               ret = - dxij / l2 * h20 ;
      
               //-------------------------------------------------------------------+
               //  2 \delta_ik l^{-3} (x_i-x_i') d k(x,x')/ dx_j
               //-------------------------------------------------------------------+
               T l3 = l2 * l;
               T k10  = sqexp_kern10(xi,xj,jcoord,n0);
               ret += 2.0 * dxij / l3 * k10;
           
               //-------------------------------------------------------------------+
               //  \delta_ij -l^{-2}  d^2 k(x,x')/ dl 
               //-------------------------------------------------------------------+
               if (icoord == jcoord)
               {
                  T h10 = sqexp_h10(xi,xj,hidx,n0);
                  ret += - h10 / l2;
               }
      
               //-------------------------------------------------------------------+
               //  2 \delta_ij l^{-3}  k(x,x') 
               //-------------------------------------------------------------------+
               if (icoord == jcoord)
               {
                  T k00   = sqexp_kern00(xi,xj);
                  ret += 2.0 * k00 / l3;
               }
      
               break;
            }
         }
      
         return sign * ret;
      }
      
      inline T sqexp_h40
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& kcoord
         ,  const int& jcoord
         ,  const int& icoord
         ,  const int& hidx
         ,  const int& np
         )
      {
         int n0 = 0;
      
         //T sign = std::pow(-1.0,np);
         T sign = (np % 2 == 0) ? 1.0 : -1.0;
      
         T ret;
         switch(hidx)
         {
            case(0):
            {
               T sig = mHparam[0];
               T k30 = sqexp_kern30(xi,xj,kcoord,jcoord,icoord,n0);
      
               ret = 2.0 * k30 / sig;
               break;
            }
            default:
            {
      
               T l  = mHparam[1];
               T l2 = l * l;
      
               //-------------------------------------------------------------------+
               //  -l^{-2} (x_i-x_i') d^3 k(x,x')/ dl dx_k dx_j
               //-------------------------------------------------------------------+
               T dxij  = (xi[icoord]-xj[icoord]);
               T h30kj = sqexp_h30(xi,xj,jcoord,kcoord,hidx,n0);
      
               ret = - dxij / l2 * h30kj;
      
               //-------------------------------------------------------------------+
               // 2 l^{-3} (x_i-x_i') d^2 k(x,x')/ dx_k dx_j
               //-------------------------------------------------------------------+
               T l3 = l2 * l; 
               T k20kj = sqexp_kern20(xi,xj,jcoord,kcoord,n0);
               ret += 2.0 * dxij / l3 * k20kj;
      
               //-------------------------------------------------------------------+
               // \delta_ik  -l^{-2}  d^2 k(x,x')/ dl dx_j
               //-------------------------------------------------------------------+
               if (icoord == kcoord)
               {
                  T h20j  = sqexp_h20(xi,xj,jcoord,hidx,n0);
                  ret += - h20j / l2;
               }
      
               //-------------------------------------------------------------------+
               // 2 \delta_ik l^{-3}  d k(x,x')/ dx_j
               //-------------------------------------------------------------------+
               if (icoord == kcoord )
               {
                  T k10j  = sqexp_kern10(xi,xj,jcoord,n0);
                  ret += 2.0 * k10j / l3;
               }
         
               //-------------------------------------------------------------------+
               // \delta_ij  -l^{-2}  d^2 k(x,x')/ dl dx_k
               //-------------------------------------------------------------------+
               if (icoord == jcoord)
               {
                  T h20k = sqexp_h20(xi,xj,kcoord,hidx,n0);
                  ret += - h20k / l2;
               }
      
               //-------------------------------------------------------------------+
               // 2 \delta_ij  l^{-3}  d k(x,x')/ dx_k
               //-------------------------------------------------------------------+
               if (icoord == jcoord)
               {
                  T k10k  = sqexp_kern10(xi,xj,kcoord,n0);
                  ret += 2.0 * k10k / l3;
               }
      
               break;
            }
         }
      
         return sign * ret;
      }
      
      inline T sqexp_h50
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& lcoord
         ,  const int& kcoord
         ,  const int& jcoord
         ,  const int& icoord
         ,  const int& hidx
         ,  const int& np
         )
      {
      
         int n0 = 0;
      
         //T sign = std::pow(-1.0,np);
         T sign = (np % 2 == 0) ? 1.0 : -1.0;
      
         T ret;
         switch(hidx)
         {
            case(0):
            {
               T sig = mHparam[0];
               T k40 = sqexp_kern40(xi,xj,lcoord,kcoord,jcoord,icoord,n0);
      
               ret = 2.0 * k40 / sig;
               break;
            }
            default:
            {
      
               T l  = mHparam[1];
               T l2 = l * l; 
      
               //-------------------------------------------------------------------+
               //  -l^{-2} (x_i-x_i') d^4 k(x,x')/ dl dx_l dx_k dx_j
               //-------------------------------------------------------------------+
               T h40lkj = sqexp_h40(xi,xj,lcoord,kcoord,jcoord,hidx,n0);
               T dxij   = (xi[icoord]-xj[icoord]);
      
               ret = - dxij / l2 * h40lkj;
      
               const bool is_ij = (icoord == jcoord); 
               const bool is_il = (icoord == lcoord); 
               const bool is_ik = (icoord == kcoord); 
      
               //-------------------------------------------------------------------+
               // 2 l^{-3} (x_i - x_i') d^3 k(x,x')/ dx_l dx_k dx_j
               //-------------------------------------------------------------------+
               T l3     = l2 * l; 
               T k30lkj = sqexp_kern30(xi,xj,lcoord,kcoord,jcoord,n0);
               ret += 2.0 * dxij / l3 * k30lkj;
      
               //-------------------------------------------------------------------+
               // - \delta_li l^{-2}  d^3 k(x,x')/ dl dx_k dx_j
               //-------------------------------------------------------------------+
               if (is_il)
               {
                  T h30kj  = sqexp_h30(xi,xj,kcoord,jcoord,hidx,n0);
                  ret += - h30kj / l2;
               }
      
               //-------------------------------------------------------------------+
               // 2 \delta_li l^{-3} d^2 k(x,x')/ dx_k dx_j
               //-------------------------------------------------------------------+
               if (is_il)
               {
                  T k20kj  = sqexp_kern20(xi,xj,kcoord,jcoord,n0);
                  ret += 2.0 * k20kj / l3;
               }
      
               //-------------------------------------------------------------------+
               // - \delta_ki l_i^{-2} d^3 k(x,x')/dl_m  dx_l dx_j
               //-------------------------------------------------------------------+
               if (is_ik)
               {
                  T h30lj  = sqexp_h30(xi,xj,lcoord,jcoord,hidx,n0);
                  ret += - h30lj / l2;
               }
      
               //-------------------------------------------------------------------+
               // 2 \delta_mi \delta_ki l_i^{-3} d^2 k(x,x')/ dx_l dx_j
               //-------------------------------------------------------------------+
               if (is_ik)
               {
                  T k20lj  = sqexp_kern20(xi,xj,lcoord,jcoord,n0);
                  ret += 2.0 * k20lj / l3;
               }
      
               //-------------------------------------------------------------------+
               // - \delta_ji l_i^{-2} d^3 k(x,x')/dl_m  dx_l dx_k
               //-------------------------------------------------------------------+
               if (is_ij)
               {
                  T h30lk  = sqexp_h30(xi,xj,lcoord,kcoord,hidx,n0);
                  ret += - h30lk / l2;
               }
      
               //-------------------------------------------------------------------+
               // 2 \delta_ij l^{-3} d^2 k(x,x')/ dx_l dx_k
               //-------------------------------------------------------------------+
               if (is_ij)
               {
                  T k20lk  = sqexp_kern20(xi,xj,lcoord,kcoord,n0);
                  ret += 2.0 * k20lk / l3;
               }
      
               break;
            }
         }
      
         return sign * ret;
      }


      /**
       *  @brief Computes the gradient at the point xi 
       *
       *  Purpose: Computes the geometrical gradient at point xi
       *
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      std::vector<T> Gradient
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xk
         )
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);

         std::vector<T> grad(nsize);

         T sig = mHparam[0];
         T width = mHparam[1];


         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            grad[i] = C_0;
            r += (xi[i] - xk[i]) * (xi[i] - xk[i]);
         }
         T l2 = width*width;

         for (int i = 0; i < nsize; ++i)
         {
            grad[i] = - sig * sig * exp(-0.5 * r / l2) * (xi[i] - xk[i]) / l2; 
         }

         return grad;
      }

      /**
       *  @brief Computes the geometrical Hessian 
       *
       *  Purpose: Computes the geometrical Hessian
       *         
       * @param  hessian is a unique_ptr to the memory for the hessian
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      void GetHessian
         (  std::unique_ptr<T[]>& hessian
         ,  const MLDescriptor<T>& xi 
         ,  const MLDescriptor<T>& xk 
         )
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);

         T sig = mHparam[0];
         T width = mHparam[1];
         T sig2 = sig*sig;

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xk[i]) * (xi[i] - xk[i]);
         }
         T l2 = width*width;
  
         for (int i = 0; i < nsize; ++i)
         {
            hessian[i*nsize + i] = - sig2 * exp(-0.5 * r/l2) * ( 1.0 / l2 
                                                               - (xi[i] - xk[i]) * (xi[i] - xk[i]) / (l2*l2)
                                                               ); 

            for (int j = 0; j < i; ++j)
            {
               T dval =  sig2 * exp(-0.5* r/l2) * ( (xi[i] - xk[i])*(xi[j] - xk[j]) / (l2*l2));

               hessian[j*nsize + i] = dval;
               hessian[i*nsize + j] = dval;
            }
         }
      }

};


#endif /* KERNEL_INCLUDED */
