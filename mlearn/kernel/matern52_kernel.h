

#ifndef MATERN52_INCLUDED
#define MATERN52_INCLUDED

#include "mlearn/GPKernelBase.h"


template <class T>
class MATERN52_KERN : public GPKernelBase<T>
{

   private:

      const bool mHaveHyperParamDeriv = true;
      const bool mHaveKernelDeriv = true;
      const bool mHaveKernelHessian = true;

      std::vector<T> mHparam;

   public:

      ///> Constructor 
      MATERN52_KERN 
         (  std::vector<T>& hparam
         )
      {
         mHparam = hparam;
      }

      ///> Destructor
      ~MATERN52_KERN()
      {
      }

      std::string GetName()
      {
         return "Matern (nu = 5/2)";
      }

      bool HaveHyperParamDeriv()   {return mHaveHyperParamDeriv;}
      bool HaveGradient()          {return mHaveKernelDeriv;}
      bool HaveHessian()           {return mHaveKernelHessian;}

      std::vector<T> GetHyperParameters()            {return mHparam;}

      void SetHyperParameters(const std::vector<T>& hparam)
      {
         mHparam.assign(hparam.begin(), hparam.end());
      }

      /**
       *  @brief Computes kernel function for a given set of points 
       *
       *  Purpose: Computes the kernel function for a given set of points
       *           xi and xj
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  addnoise specifies if the noise term is added
       * @param  noiseval specifies the added noise 
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T Compute
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const bool &addnoise
         ,  const T& noiseval
         ,  const int& lderi
         ,  const int& lderj
         )
      {
         int nsize = 0;

         // sanity check(s)
         if (xi.size() == xj.size())
         {
            nsize = xi.size();
         }
         else
         {
            Mout <<  xi.size() << " vs " << xj.size() << std::endl;
            MIDASERROR("Dimensions of points do not agree!");
         }

         T sig   = mHparam[0];
         T width = mHparam[1];
         T noise = C_0;
         if (addnoise) noise = noiseval;

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xj[i]) * (xi[i] - xj[i]);
         }
         r = std::sqrt(r);

         T arg = sqrt(5.0) * r / width;
         T l2 = width*width;
         T sig2 = sig*sig;

         return sig2 * ( 1.0 + arg + arg*arg/3.0 ) * std::exp(-arg) + noise ;

      }

      /**
       *  @brief Computes derivatvie of the kernel function w.r.t hyper parameters 
       *
       *  Purpose: Computes the derivatvie of the kernel function for a given set of points
       *           xi and xj w.r.t hyper parameter hidx
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  hidx index of the hyper parameter
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T ComputeHderiv
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& hidx
         ,  const int& lderi
         ,  const int& lderj
         )
      {
      
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xj);
      
         std::vector<T> grad(nsize);
      
         T sig = mHparam[0];
         T width = mHparam[1];
      
         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xj[i]) * (xi[i] - xj[i]);
         }
         r = std::sqrt(r);
      
         T arg = sqrt(5.0) * r / width;
         T l2 = width*width;
         T sig2 = sig*sig;
      
         T ret = C_0;
         switch(hidx)
         {
            case(0):
            {
               ret = 2.0 * sig * ( 1.0 + arg + arg*arg/3.0 ) * std::exp(-arg);
               break;
            }
            case(1):
            {
               ret =   sig2 * 5.0 * r * r * ( width + std::sqrt(5) * r) / (3.0 * std::pow(width,4.0) ) * std::exp(-arg);
               break;
            }
         }
      
         return ret;
      }

      /**
       *  @brief Computes the gradient at the point xi 
       *
       *  Purpose: Computes the geometrical gradient at point xi
       *
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      std::vector<T> Gradient
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xk
         )
      {
      
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);
      
         std::vector<T> grad(nsize);
      
         T sig   = mHparam[0];
         T width = mHparam[1];
         T sig2  = sig * sig;
      
         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            grad[i] = C_0;
            r += (xi[i] - xk[i]) * (xi[i] - xk[i]);
         }
      
         r = std::sqrt(r);
         T arg = sqrt(5.0) * r / width;
      
         for (int i = 0; i < nsize; ++i)
         {
            grad[i] = - sig2 * exp(-arg) * 5.0 * (xi[i] - xk[i]) * (std::sqrt(5.0) * r + width) / (3.0 * width * width * width) ; 
         }
      
         return grad;
      }

      /**
       *  @brief Computes the geometrical Hessian 
       *
       *  Purpose: Computes the geometrical Hessian
       *         
       * @param  hessian is a unique_ptr to the memory for the hessian
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      void GetHessian
         (  std::unique_ptr<T[]>& hessian
         ,  const MLDescriptor<T>& xi 
         ,  const MLDescriptor<T>& xk 
         )
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);

         T sig = mHparam[0];
         T width = mHparam[1];
         T sig2 = sig*sig;

         T sum = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            sum += (xi[i] - xk[i]) * (xi[i] - xk[i]);
         }
         T r = std::sqrt(sum);
         T r2 = r*r;
         T l2 = width*width;
         T l4 = l2*l2;
         T arg = sqrt(5.0) * r / width;

         for (int i = 0; i < nsize; ++i)
         {
            hessian[i*nsize + i] = -sig2 * 5.0 * exp(-arg) * ( std::sqrt(5.0) * width * r - 5.0 * (xk[i] - xi[i])*(xk[i] - xi[i]) + l2  ) / (3.0*l4) ;

            for (int j = 0; j < i; ++j)
            {
               T dval = sig2 * 5.0 * exp(-arg) * ( 5.0 * (xk[i] - xi[i])*(xk[j] - xi[j]) + l2   / (3.0*l4) );

               hessian[j*nsize + i] = dval;
               hessian[i*nsize + j] = dval;
            }
         }
      }

};


#endif /* KERNEL_INCLUDED */
