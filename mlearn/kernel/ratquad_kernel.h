

#ifndef RATQUAD_INCLUDED
#define RATQUAD_INCLUDED

#include "mlearn/GPKernelBase.h"


template <class T>
class RATQUAD_KERN : public GPKernelBase<T>
{

   private:

      const bool mHaveHyperParamDeriv = true;
      const bool mHaveKernelDeriv = false;
      const bool mHaveKernelHessian = false;

      std::vector<T> mHparam;

   public:

      ///> Constructor 
      RATQUAD_KERN 
         (  std::vector<T>& hparam
         )
      {
         mHparam = hparam;
      }

      ///> Destructor
      ~RATQUAD_KERN()
      {
      }

      std::string GetName()
      {
         return "Rational Quadratic"; 
      }

      bool HaveHyperParamDeriv()   {return mHaveHyperParamDeriv;}
      bool HaveGradient()          {return mHaveKernelDeriv;}
      bool HaveHessian()           {return mHaveKernelHessian;}

      std::vector<T> GetHyperParameters()            {return mHparam;}

      void SetHyperParameters(const std::vector<T>& hparam)
      {
         mHparam.assign(hparam.begin(), hparam.end());
      }

      /**
       *  @brief Computes kernel function for a given set of points 
       *
       *  Purpose: Computes the kernel function for a given set of points
       *           xi and xj
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  addnoise specifies if the noise term is added
       * @param  noiseval specifies the added noise 
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T Compute
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const bool &addnoise
         ,  const T& noiseval
         ,  const int& lderi
         ,  const int& lderj
         )
      {
         int nsize = 0;

         // sanity check(s)
         if (xi.size() == xj.size())
         {
            nsize = xi.size();
         }
         else
         {
            Mout <<  xi.size() << " vs " << xj.size() << std::endl;
            MIDASERROR("Dimensions of points do not agree!");
         }

         T sig   = mHparam[0];
         T width = mHparam[1];
         T alpha = mHparam[2];
         T noise = C_0;
         if (addnoise) noise = noiseval;

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xj[i]) * (xi[i] - xj[i]);
         }

         return sig * sig * std::pow( (1.0 + -0.5 * r / (alpha * width * width) ), -alpha) + noise ;

      }

      /**
       *  @brief Computes derivatvie of the kernel function w.r.t hyper parameters 
       *
       *  Purpose: Computes the derivatvie of the kernel function for a given set of points
       *           xi and xj w.r.t hyper parameter hidx
       *
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * @param  hidx index of the hyper parameter
       * @param  lderi is the derivative order for the point xi
       * @param  lderk is the derivative order for the point xk
       * */
      T ComputeHderiv
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xk
         ,  const int& hidx
         ,  const int& lderi
         ,  const int& lderk
         )
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);

         T sig   = mHparam[0];
         T width = mHparam[1];
         T alpha = mHparam[2];

         T sig2 = sig*sig;

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xk[i]) * (xi[i] - xk[i]);
         }

         T ret = C_0;
         switch(hidx)
         {
            case(0):
            {
               ret = 2.0 * sig * std::pow( (1.0 + -0.5 * r / (alpha * width * width) ), -alpha);
               break;
            }
            case(1):
            {
               ret =  -1.0/(std::pow(width,3.0)) * r * sig2 * std::pow( (1.0 + -0.5 * r / (alpha * width * width) ), -alpha - 1.0) ;
               break;
            }
            case(2):
            {
               T al2 = alpha * width * width;
               ret = -sig2 * 1.0/(al2-0.5*r)*std::pow(1 - 0.5 * r/al2,-alpha) * ( (al2 - 0.5*r) * (std::log(1.0- 0.5*r/al2) + 0.5 * r )   );  
               break;
            }
         }

         return ret;
      }

      /**
       *  @brief Computes the gradient at the point xi 
       *
       *  Purpose: Computes the geometrical gradient at point xi
       *
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      std::vector<T> Gradient
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xk
         )
      {
         MIDASERROR("Gradient not implemented for this kernel");
         std::vector<T> vec;
         return vec;
      }

      /**
       *  @brief Computes the geometrical Hessian 
       *
       *  Purpose: Computes the geometrical Hessian
       *         
       * @param  hessian is a unique_ptr to the memory for the hessian
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      void GetHessian
         (  std::unique_ptr<T[]>& hessian
         ,  const MLDescriptor<T>& xi 
         ,  const MLDescriptor<T>& xk 
         )
      {
         MIDASERROR("Hessian not implemented for this kernel");
      }

};


#endif /* KERNEL_INCLUDED */
