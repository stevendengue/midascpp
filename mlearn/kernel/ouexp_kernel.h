

#ifndef OUEXP_INCLUDED
#define OUEXP_INCLUDED

#include "mlearn/GPKernelBase.h"


template <class T>
class OUEXP_KERN : public GPKernelBase<T>
{

   private:

      const bool mHaveHyperParamDeriv = true;
      const bool mHaveKernelDeriv = true;
      const bool mHaveKernelHessian = true;

      std::vector<T> mHparam;

   public:

      ///> Constructor 
      OUEXP_KERN 
         (  std::vector<T>& hparam
         )
      {
         mHparam = hparam;
      }

      ///> Destructor
      ~OUEXP_KERN()
      {
      }

      std::string GetName()
      {
         return "Ornstein Uhlenbeck";
      }

      bool HaveHyperParamDeriv()   {return mHaveHyperParamDeriv;}
      bool HaveGradient()          {return mHaveKernelDeriv;}
      bool HaveHessian()           {return mHaveKernelHessian;}

      std::vector<T> GetHyperParameters()            {return mHparam;}

      void SetHyperParameters(const std::vector<T>& hparam)
      {
         mHparam.assign(hparam.begin(), hparam.end());
      }

      /**
       *  @brief Computes kernel function for a given set of points 
       *
       *  Purpose: Computes the kernel function for a given set of points
       *           xi and xj
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  addnoise specifies if the noise term is added
       * @param  noiseval specifies the added noise 
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T Compute
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const bool &addnoise
         ,  const T& noiseval
         ,  const int& lderi
         ,  const int& lderj
         )
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xj);

         T sig = mHparam[0];
         T width = mHparam[1];
         T noise = C_0;
         if (addnoise) noise = noiseval;

         T r = C_0;
         T l = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xj[i]) * (xi[i] - xj[i]);
         }
         r = std::sqrt(r);

         return sig * sig * exp(- r / width) + noise;

      }

      /**
       *  @brief Computes derivatvie of the kernel function w.r.t hyper parameters 
       *
       *  Purpose: Computes the derivatvie of the kernel function for a given set of points
       *           xi and xj w.r.t hyper parameter hidx
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  hidx index of the hyper parameter
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T ComputeHderiv
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& hidx
         ,  const int& lderi
         ,  const int& lderj
         )
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xj);

         std::vector<T> grad(nsize);

         T sig = mHparam[0];
         T width = mHparam[1];

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xj[i]) * (xi[i] - xj[i]);
         }
         r = std::sqrt(r);

         T ret = C_0;
         switch(hidx)
         {
            case(0):
            {
               ret = 2.0 * sig * exp(-r / width);
               break;
            }
            case(1):
            {
               ret =  sig * sig * exp(-r / width) * r /  (width*width);
               break;
            }
         }

         return ret;
      }

      /**
       *  @brief Computes the gradient at the point xi 
       *
       *  Purpose: Computes the geometrical gradient at point xi
       *
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      std::vector<T> Gradient
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xk
         )
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);

         std::vector<T> grad(nsize);

         T sig = mHparam[0];
         T width = mHparam[1];

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            grad[i] = C_0;
            r += (xi[i] - xk[i]) * (xi[i] - xk[i]);
         }

         if (r < 1.e-18) return grad;

         r = std::sqrt(r);

         for (int i = 0; i < nsize; ++i)
         {
            grad[i] = - sig * sig * exp(-r / width) * (xi[i] - xk[i]) / (r * width); 
         }

         return grad;
      }

      /**
       *  @brief Computes the geometrical Hessian 
       *
       *  Purpose: Computes the geometrical Hessian
       *         
       * @param  hessian is a unique_ptr to the memory for the hessian
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      void GetHessian
         (  std::unique_ptr<T[]>& hessian
         ,  const MLDescriptor<T>& xi 
         ,  const MLDescriptor<T>& xk 
         )
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);

         T sig = mHparam[0];
         T width = mHparam[1];
         T sig2 = sig*sig;

         T sum = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            sum += (xi[i] - xk[i]) * (xi[i] - xk[i]);
         }
         T r = std::sqrt(sum);
         T r2 = r*r;
         T l2 = width*width;
  
         if (sum < 1.e-18)
         {
            for (int i = 0; i < nsize*nsize; ++i)
            {
               hessian[i] = C_0;
            }
         }
         else
         { 
            for (int i = 0; i < nsize; ++i)
            {
               hessian[i*nsize + i] = -sig2 * exp(-r/width) * ( 1.0 / (r * width) 
                                                              - (xi[i] - xk[i]) * (xi[i] - xk[i]) / (r2*l2)
                                                              - (xi[i] - xk[i]) * (xi[i] - xk[i]) / (width * std::pow(sum,1.5))
                                                              ); 

               for (int j = 0; j < i; ++j)
               {
                  T dval =  sig2 * exp(-r / width) * ( (xi[i] - xk[i])*(xi[j] - xk[j]) / (r2*l2)
                                                     + (xi[i] - xk[i])*(xi[j] - xk[j]) / (width * std::pow(sum,1.5))
                                                     );
                  hessian[i*nsize + j] = dval;
                  hessian[j*nsize + i] = dval;
               }
            }
         }
      }

};


#endif /* KERNEL_INCLUDED */
