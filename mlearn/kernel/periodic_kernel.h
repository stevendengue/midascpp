

#ifndef PERIODIC_INCLUDED
#define PERIODIC_INCLUDED

#include "mlearn/GPKernelBase.h"


template <class T>
class PERIODIC_KERN : public GPKernelBase<T>
{

   private:

      const bool mHaveHyperParamDeriv = true;
      const bool mHaveKernelDeriv = true;
      const bool mHaveKernelHessian = false;

      std::vector<T> mHparam;

   public:

      ///> Constructor 
      PERIODIC_KERN 
         (  std::vector<T>& hparam
         )
      {
         mHparam = hparam;
      }

      ///> Destructor
      ~PERIODIC_KERN()
      {
      }

      std::string GetName()
      {
         return "Periodic";
      }

      bool HaveHyperParamDeriv()   {return mHaveHyperParamDeriv;}
      bool HaveGradient()          {return mHaveKernelDeriv;}
      bool HaveHessian()           {return mHaveKernelHessian;}

      std::vector<T> GetHyperParameters()            {return mHparam;}

      void SetHyperParameters(const std::vector<T>& hparam)
      {
         mHparam.assign(hparam.begin(), hparam.end());
      }

      /**
       *  @brief Computes kernel function for a given set of points 
       *
       *  Purpose: Computes the kernel function for a given set of points
       *           xi and xj
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  addnoise specifies if the noise term is added
       * @param  noiseval specifies the added noise 
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T Compute
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const bool &addnoise
         ,  const T& noiseval
         ,  const int& lderi
         ,  const int& lderj
         )
      {
         int nsize = 0;

         // sanity check(s)
         if (xi.size() == xj.size())
         {
            nsize = xi.size();
         }
         else
         {
            Mout <<  xi.size() << " vs " << xj.size() << std::endl;
            MIDASERROR("Dimensions of points do not agree!");
         }

         T sig   = mHparam[0];
         T width = mHparam[1];
         T param = mHparam[2];
         T noise = C_0;
         if (addnoise) noise = noiseval;

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xj[i]) * (xi[i] - xj[i]);
         }
         r = std::sqrt(r);

         return sig * sig * std::exp(- (2.0 * std::pow(sin(M_PI * r/param),2) ) / (width * width)  ) + noise;  

      }

      /**
       *  @brief Computes derivatvie of the kernel function w.r.t hyper parameters 
       *
       *  Purpose: Computes the derivatvie of the kernel function for a given set of points
       *           xi and xj w.r.t hyper parameter hidx
       *
       * @param  xi is the data points i 
       * @param  xj is the data points j
       * @param  hidx index of the hyper parameter
       * @param  lderi is the derivative order for the point xi
       * @param  lderj is the derivative order for the point xj
       * */
      T ComputeHderiv
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xj
         ,  const int& hidx
         ,  const int& lderi
         ,  const int& lderj
         )
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xj);


         T sig   = mHparam[0];
         T width = mHparam[1];
         T param = mHparam[2];

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            r += (xi[i] - xj[i]) * (xi[i] - xj[i]);
         }
         r = std::sqrt(r);

         T ret = C_0;

         T func = std::exp(- (2.0 * std::pow(sin(M_PI * r/param),2) ) / (width * width)  );

         switch(hidx)
         {
            case(0):
            {
               ret = 2.0 * sig * func ;
               break;
            }
            case(1):
            {
               ret = sig * sig * ( 4.0 * std::pow(std::sin(M_PI * r/param), 2.0) / (std::pow(param, 3.0)) ) 
                         * func ;
               break;
            }
            case (2):
            {
               ret = sig * sig * (2.0 * M_PI * r * std::sin(2.0 * M_PI * r / param) / (param*param * width*width) ) 
                         * func ;
               break;
            }
         }

         return ret;
      }

      /**
       *  @brief Computes the gradient at the point xi 
       *
       *  Purpose: Computes the geometrical gradient at point xi
       *
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      std::vector<T> Gradient
         (  const MLDescriptor<T>& xi
         ,  const MLDescriptor<T>& xk
         )
      {
         // sanity check(s)
         int nsize = this->ValidateArguments(xi,xk);

         std::vector<T> grad(nsize);

         T sig   = mHparam[0];
         T width = mHparam[1];
         T param = mHparam[2];

         T r = C_0;
         for (int i = 0; i < nsize; ++i) 
         {
            grad[i] = C_0;
            r += (xi[i] - xk[i]) * (xi[i] - xk[i]);
         }
         r = std::sqrt(r);
         T l2 = width*width;

         Mout << " r " << r << std::endl;

         if (r < 1e-16) return grad;

         for (int i = 0; i < nsize; ++i)
         {
            grad[i] = - sig * sig * 8.0 * M_PI * (xi[i] - xk[i]) / (param * l2 * r) 
                            * std::sin(2.0 * M_PI * r / param ) * std::cos(2.0 * M_PI * r / param)
                            * std::exp(-2.0 * std::pow( std::sin(M_PI * r / param), 2.0) / l2); 
         }

         Mout << "grad " << grad << std::endl;

         return grad;
      }

      /**
       *  @brief Computes the geometrical Hessian 
       *
       *  Purpose: Computes the geometrical Hessian
       *         
       * @param  hessian is a unique_ptr to the memory for the hessian
       * @param  xi is the data points i 
       * @param  xk is the data points k
       * */
      void GetHessian
         (  std::unique_ptr<T[]>& hessian
         ,  const MLDescriptor<T>& xi 
         ,  const MLDescriptor<T>& xk 
         )
      {
         MIDASERROR("Hessian not implemented for this kernel");
      }

};


#endif /* KERNEL_INCLUDED */
