MidasCpp Changelog
==============

Midas 2020.04.0
===========

Feature and bugfix release.

New Features
-----------
- General
   - Added new `midastools` tool which provides access to some data analysis tools.
   - Added new `midasdynlib` tool for PES dynamic library introspection.
   - All executables (`midascpp`, `midastools`, `midasutil`, and `midasdynlib`) now have bash autocompletion,
     which can either be installed locally with `midasutil` or loaded when sourcing a `midascpp.source_me` (preferred).
   - For reproduceability of results, MidasCpp now writes which compiler and what version was used to compile the program in the `.mout` header.
- Operators:
   - Kinetic energy terms are no longer added to operators, unless it has type `energy`, or set explicitly with `#3 KineticEnergy`.
   - Adds option `USER_DEFINED` for `#3 KineticEnergy`, 
     which should be used if kinetic terms are explicitly typed as operator terms in the operator file or definition.
   - Add keyword `#1 SetInfo` to `.mop` files. This works as `#3 SetInfo` in the `#2 Operator` block of `.minp` input.
   - Types can now be defined for properties in `midasifc.mpropinfo` when creating a PES/property-surface.
     Types defined here will be included in the corresponding `.mop` files.
   - All operator types that take frequency input under `#3 SetInfo`, will now have frequencies indexed, starting from `0`, e.g. `frq_0=0.0`.
- MidasPot^(tm)
   - Added optional `#2 REFERENCEVALUE` keyword to  operator input. This allows directly connecting an operator with a reference value and avoiding the .mrefval file, e.g. for MidasPot generation.
   - New keyword for creating MidasPot #2 TENSOROPERATOR. Will take a set/tensor of operators to be rotated together when rotations occur (e.g. kabsch rotation).
- New wave-function methods
   - Added TDVCC[2] method with time-independent modals (for 2-mode-coupled Hamiltonians)
   - Added TDEVCC[k] method with time-independent modals (using full-space matrix implementation)
   - Added EVCC[k] ground state solver (using full-space matrix implementation)
   - Added MCTDH and MCTDH[n] methods
- Machine-Learning options:
   - Added Sparse Gaussian Process Regression
   - Added kernel based Farthest point sampling
   - Added kernel normalization to increase numerical stability
   - Added Multilayer Gaussian Process Regression  
- ADGA PES construction
   - Double incremental expansion in Falcon coordinates (DIF)

Bugfixes
-----------
- Fixed some clang++ compilation warnings (tested with versions 5.0.1 and 6.0.0)
- Fixed a bug where operators where sorted alphabetically when merging multi-level surfaces. 
  This meant that one could end up calculating VSCF energies with a non-energy operator,
  if the energy operators name was not alphabetically first.
  Operators are no longer sorted, and the order will be taken from `midasifc.mpropinfo`.
- Fixed the `Makefile` system to better work on Mac. 
  Some commands did not work properly on Mac OS, but these have now been generalized.
- Fixed an issue where compilation with MPI would fail if `MPI_Comm` and `MPI_Datatype` had the same underlying type.
- Fixed bug when using `MPI_Bcast` with tensors of size 1

Changed defaults
-----------

Midas 2019.10.0
===========

Feature and bugfix release.

New Features
-----------
- MidasPot^(tm)
    - Now has the option to rotate input coordinates to reference, if linked with lapack.
    - Pes bounds will now be checked by default with a threshold of 1e-12. Check can be disabled.
 
- Interface to Midas Machine Learning algorithms
    - Stand-alone interface to Gaussian Process Regression (GPR)
        - Generation of internal coordinates based on XYZ file
        - Training based on internal coordinates, interatomic distances or xyz coordinates
        - Inference based on derivative information (squared exponential kernel)

- PES construction
    - Extrapolation of higher-order mode couplings using GPR
    - Using GPR predictor for Single Point Calculations 

- State-transfer operators
    - Operator files can now state transfer operators, |I><J|(Q), which shift occupation from |J> to |I>
      in mode Q. If using state transfer, no other types of operators can be used in a given mode.

Bugfixes
-----------
- MidasPot^(tm)
    - Operator bounds file is now actually optional.
- Falcon
    - Auxiliary coordinates are properly generated.


Changed defaults
-----------
- Integral accuracy
    - The default accuracy of the Gauss-Legendre quadrature for calculating integrals of B-splines has been increased. 
    - The old default was to calculate the nodes and weights to an accuracy of 1.e-14.
    - The new default is to use a threshold of 10 times machine epsilon. This can be changed on input.



Midas 2019.07.0
===========

Feature and bugfix release.

New Features
-----------

- Vibrational coordinates
    - Flexible adaptation of local coordinates of nuclei (FALCON) - distance-based coupling estimates
- Static grid PES construction
    - Double incremental expansion in Falcon coordinates (DIF)
    - Double incremental expansion in Falcon coordinates with auxiliary coordinate transformation (DIFACT)

Bugfixes
-----------
- Bugfix to ensure consistency of mass weighting when working with non default isotopes. This bug led to failures when constructing PESs using non-default isotopes in the previous version. 

Changed defaults
-----------

- ADGA
   - Default for `#2 AdgaScalFac` is now `1.0 1.0 15.0 30.0 30.0` instead of `1.0 2.0 15.0 30.0 30.0`, which means that the two-mode part is tighter converged than before.

Midas 2019.04.0
===========

Initial Release

New Features
-----------

- VSCF
    - ss. Energies
    - RSP Expectation Values
- VMPn
    - ss. Energies
- VAPTn
    - ss. Energies
- VCI[n]
    - Response - excitation energiesm and std. rsp. func
    - Damped response for IR and Raman
    - Lanczos response solver
- VCC[n]
    - VCC[2]/H2 Two-mode VCC, energy + lin. response
    - VCC[n] energy and lin. response
    - VCC[n] ground state tensor decomposition
    - VCC[n] damped response
    - Lanczos response solver
- ADGA PES construction
- Static grid PES construction
- Taylor Force Field
- Vibrational coordinates
    - Normal coordinates
    - Optimized, localized and Hybrid coordinates
