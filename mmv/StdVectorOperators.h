/**
************************************************************************
* 
* @file                StdVectorOperators.h
*
* Created:             12-03-2014
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   operators for std vector
* 
* Last modified: 
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef STDVECTOROPERATORS_H_INCLUDED
#define STDVECTOROPERATORS_H_INCLUDED

#include <vector>

template<class T>
std::vector<T> operator-(std::vector<T> v)
{
   for(size_t i=0; i<v.size(); ++i)
      v[i] = -v[i];
   return v;
}

template<class T>
std::vector<T> operator-(std::vector<T> v, const std::vector<T>& vv)
{
   assert(v.size() == vv.size());
   for(size_t i=0; i<v.size(); ++i)
      v[i] -= vv[i];
   return v;
}

template<class T>
std::vector<T> operator+(std::vector<T> v, const std::vector<T>& vv)
{
   assert(v.size() == vv.size());
   for(size_t i=0; i<v.size(); ++i)
      v[i] += vv[i];
   return v;
}

template<class T>
std::vector<T> operator*(T scal, std::vector<T> v)
{
   for(size_t i=0; i<v.size(); ++i)
      v[i] *= scal;
   return v;
}

template<class T>
T dot(const std::vector<T>& v1, const std::vector<T>& v2)
{
   assert(v1.size() == v2.size());
   T res = 0;
   for(size_t i=0; i<v1.size(); ++i)
     res += v1[i]*v2[i];
   return res;
}

#endif /* STDVECTOROPERATORS_H_INCLUDED */
