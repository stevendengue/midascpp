/**
************************************************************************
* 
* @file                AnalysisOf.h
*
* Created:             11-11-2012
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Class for analysing distribution of elements in vectors,
*                      matrices and tensors.
* 
* Last modified: Thu Aug 13, 2013  12:19PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef ANALYSISOF_H_INCLUDED
#define ANALYSISOF_H_INCLUDED

// std headers
#include <vector>
#include <iostream>
#include <algorithm>
#include <type_traits>

template<typename T>
class AnalysisOf
{
   public:
      typedef typename T::value_type value_type;
      typedef typename T::value_type range_type;

   private:
      const T&                mObject;
      std::vector<range_type> mRange;
      std::vector<size_t>     mAbsDist; // absolute distribution
      std::vector<size_t>     mRelDist; // relative distribution
      std::vector<size_t>     mNormDist; // distribution relative to norm of set
      value_type              mMax;
      value_type              mMin;
      value_type              mAbsMax;
      value_type              mAbsMin;
      value_type              mNorm;

   private:
      template<typename U=T>
      void DefaultRange(typename std::enable_if<std::is_floating_point<typename U::value_type>::value, typename U::value_type>::type* = nullptr);

      template<typename U=T>
      void DefaultRange(typename std::enable_if<std::is_integral<typename U::value_type>::value, typename U::value_type>::type* = nullptr);

      //void DoAnalysis(vector<size_t>&, value_type);
      void InitMaxMin();
      void InitNorm();
      
      void DoOutput(ostream&,const std::vector<size_t>&,const std::vector<range_type>&) const;
      
      template<typename CompareFunc>    
      size_t DoAboveBelow(const range_type, const std::vector<size_t>&, 
            const std::vector<range_type>&, const CompareFunc) const;
   public:
      AnalysisOf(const T& aObject);
      AnalysisOf(const T& aObject, const std::vector<range_type>& aRange);
      ~AnalysisOf() 
      { 
      }
      
      /**
       * do the different types of analysis
       **/
      void AbsAnalysis();
      void RelAnalysis();
      void NormAnalysis();
      
      /**
       * get different stats
       **/
      value_type AbsMax() const { return mAbsMax; }
      value_type AbsMin() const { return mAbsMin; }
      value_type Max()    const { return mMax; }
      value_type Min()    const { return mMin; }
      value_type Norm()   const { return mNorm; }

      /**
       * How many elements are above/below some threshold
       **/
      size_t AbsBelow(const range_type aCutOff) const
      { 
         return DoAboveBelow(aCutOff, mAbsDist, mRange, std::less<range_type>() ); 
      }
      
      size_t AbsAbove(const range_type aCutOff) const
      { 
         return DoAboveBelow(aCutOff, mAbsDist, mRange, std::greater_equal<range_type>() ); 
      }
      
      size_t RelBelow(const range_type aCutOff) const
      { 
         return DoAboveBelow(aCutOff, mRelDist, mRange, std::less<range_type>() ); 
      }
      
      size_t RelAbove(const range_type aCutOff) const
      { 
         return DoAboveBelow(aCutOff, mRelDist, mRange, std::greater_equal<range_type>() ); 
      }
      
      size_t NormBelow(const range_type aCutOff) const
      { 
         return DoAboveBelow(aCutOff, mNormDist, mRange, std::less<range_type>() ); 
      }
      
      size_t NormAbove(const range_type aCutOff) const
      { 
         return DoAboveBelow(aCutOff, mNormDist, mRange, std::greater_equal<range_type>() ); 
      }

      /**
       * output different distribution types
       **/
      void OutputAbs(std::ostream& aOstream) const 
      { 
         if(mAbsDist.size()>0) 
            DoOutput(aOstream,mAbsDist,mRange); 
      }
      void OutputRel(std::ostream& aOstream) const 
      { 
         if(mRelDist.size()>0) 
            DoOutput(aOstream,mRelDist,mRange); 
      }
      void OutputNorm(std::ostream& aOstream) const 
      { 
         if(mNormDist.size()>0) 
            DoOutput(aOstream,mNormDist,mRange); 
      }
};

/**
 * private functions
 **/
template<typename T> template<typename U>
void AnalysisOf<T>::DefaultRange(typename std::enable_if<std::is_floating_point<typename U::value_type>::value, typename U::value_type>::type*)
{
   range_type range_point = 1;
   for(size_t i=0; i<9; ++i)
   {
      mRange.push_back(range_point);
      range_point*=1e-1;
   }
}

template<typename T> template<typename U>
void AnalysisOf<T>::DefaultRange(typename std::enable_if<std::is_integral<typename U::value_type>::value, typename U::value_type>::type*)
{
   range_type range_point = 10;
   for(size_t i=0; i<9; ++i)
   {
      mRange.push_back(range_point);
      range_point-=1;
   }
}

template<typename T>
void AnalysisOf<T>::InitMaxMin()
{
   if(mObject.Size()>0)
   {
      mMax=mObject.vec_access(0);
      mAbsMax=fabs(mObject.vec_access(0));
      mMin=mObject.vec_access(0);
      mAbsMin=fabs(mObject.vec_access(0));
   }
   for(size_t index=1; index<mObject.Size(); ++index)
   {
      mMax = max(mMax,mObject.vec_access(index));
      mAbsMax = max(mAbsMax,fabs(mObject.vec_access(index)));
      mMin = min(mMin,mObject.vec_access(index));
      mAbsMin = min(mAbsMin,fabs(mObject.vec_access(index)));
   }
}

template<typename T>
void AnalysisOf<T>::InitNorm()
{
   mNorm = mObject.Norm();
}

template<typename T>
void AnalysisOf<T>::DoOutput(ostream& aOstream, 
      const std::vector<size_t>& aDist, 
      const std::vector<range_type>& aRange) const
{
   aOstream << "|--------------------------------------------|" << endl;
   aOstream << "|    Max="<<setw(13)<<left<<mMax
            << "     Min="<<setw(13)<<left<<mMin
            << " |" << endl;
   aOstream << "| AbsMax="<<setw(13)<<left<<mAbsMax
            << "  AbsMin="<<setw(13)<<left<<mAbsMin
            << " |" << endl;
   aOstream << "| NumEle="<<setw(13)<<left<<mObject.Size()
            << "    Norm="<<setw(13)<<left<<mNorm
            << " |" << endl;
   aOstream << "|--------------------------------------------|" << endl;
   aOstream << "| " 
      << setw(20) << internal << "Range" << "| "  
      << setw(20) << internal << "Distribution" << " |" << endl;
   aOstream << "|--------------------------------------------|" << endl;
   aOstream << "| >="<< setw(17) << left << aRange[0] << " | "
      << setw(20) << std::right << aDist[0] << " |" << endl;
   for(size_t i=1; i<aRange.size(); ++i)
      aOstream << "| ]"<< setw(8) << aRange[i-1] 
               <<";"   << setw(8) << aRange[i]
               <<"] | " << setw(20) << std::right << aDist[i]
               <<" |"<< endl;
   aOstream << "| <"<< setw(18) << left << aRange[aRange.size()-1] 
            << " | "<< setw(20) << std::right << aDist[aRange.size()]
            << " |" << endl;
   aOstream << "|--------------------------------------------|" << endl;
   aOstream << endl;
}

template<typename T> template<typename CompareFunc>
size_t AnalysisOf<T>::DoAboveBelow(const range_type aCutOff,
      const std::vector<size_t>& aDist,
      const std::vector<range_type>& aRange,
      const CompareFunc aCompareFunc) const
{
   size_t num_elements=0;
   for(size_t range_idx=0; range_idx<aRange.size(); ++range_idx)
   {
      if(aCompareFunc(aRange[range_idx],aCutOff))
         num_elements+=aDist[range_idx];
   }
   return num_elements;
}

/**
 * constructors
 **/
template<typename T>
AnalysisOf<T>::AnalysisOf(const T& aObject): 
   mObject(aObject),
   mRange(),
   mAbsDist(),
   mRelDist(),
   mMax(),
   mMin(),
   mAbsMax(),
   mAbsMin(),
   mNorm(0)
{ 
   DefaultRange();
   InitMaxMin();
   InitNorm();
};

template<typename T>
AnalysisOf<T>::AnalysisOf(const T& aObject, const std::vector<range_type>& aRange): 
   mObject(aObject), 
   mRange(aRange),
   mAbsDist(),
   mRelDist(),
   mMax(),
   mMin(),
   mAbsMax(),
   mAbsMin(),
   mNorm(0)
{ 
   std::sort(mRange.begin(), mRange.end(), std::greater<range_type>()); // sort the range
   InitMaxMin();
   InitNorm();
};

template<typename T>
void AnalysisOf<T>::AbsAnalysis()
{
   mAbsDist.resize(mRange.size()+1,0);
   for(size_t index=0; index<mObject.Size(); ++index)
   {
      if(fabs(mObject.vec_access(index))>=mRange[0])
      {
         ++mAbsDist[0];
         continue; // go to next element
      }
      for(size_t range_idx=1; range_idx<mRange.size(); ++range_idx)
      {
         if(fabs(mObject.vec_access(index))<mRange[range_idx-1] && fabs(mObject.vec_access(index))>=mRange[range_idx])
         {
            ++mAbsDist[range_idx];
            //goto donextabsindex;
         }
      }
      if(fabs(mObject.vec_access(index))<mRange[mRange.size()-1])
         ++mAbsDist[mRange.size()];
//donextabsindex:
   }
}

template<typename T>
void AnalysisOf<T>::RelAnalysis()
{
   mRelDist.resize(mRange.size()+1,0);

   for(size_t index=0; index<mObject.Size(); ++index)
   {
      if(fabs(mObject.vec_access(index)/mAbsMax)>=mRange[0])
      {
         ++mRelDist[0];
         continue; // go to next element
      }
      for(size_t range_idx=1; range_idx<mRange.size(); ++range_idx)
      {
         if(fabs(mObject.vec_access(index)/mAbsMax)<mRange[range_idx-1] 
               && fabs(mObject.vec_access(index)/mAbsMax)>=mRange[range_idx])
         {
            ++mRelDist[range_idx];
            //goto donextrelindex;
         }
      }
      if(fabs(mObject.vec_access(index)/mAbsMax)<mRange[mRange.size()-1])
         ++mRelDist[mRange.size()];
//donextrelindex:
   }
}

template<typename T>
void AnalysisOf<T>::NormAnalysis()
{
   mNormDist.resize(mRange.size()+1,0);

   for(size_t index=0; index<mObject.Size(); ++index)
   {
      if(fabs(mObject.vec_access(index)/mNorm)>=mRange[0])
      {
         ++mNormDist[0];
         continue; // go to next element
      }
      for(size_t range_idx=1; range_idx<mRange.size(); ++range_idx)
      {
         if(fabs(mObject.vec_access(index)/mNorm)<mRange[range_idx-1] 
               && fabs(mObject.vec_access(index)/mNorm)>=mRange[range_idx])
         {
            ++mNormDist[range_idx];
            //goto donextrelindex;
         }
      }
      if(fabs(mObject.vec_access(index)/mNorm)<mRange[mRange.size()-1])
         ++mNormDist[mRange.size()];
//donextrelindex:
   }
}

#endif // ANALYSISOF_H_INCLUDED
