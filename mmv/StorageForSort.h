#ifndef STORAGEFORSORT_H_INCLUDED
#define STORAGEFORSORT_H_INCLUDED

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"

/**
 * Helper class for sorting.
 **/
template<class T>
class GeneralStorageForSort
{
   private:
      //! Eigenvalue
      T mValue;
      //! Position of eigenvalue
      In mPoss;
   
   public:
      const T& GetValue() const 
      {
         return mValue;
      }
      
      const In& GetPoss() const 
      {
         return mPoss;
      }
      
      GeneralStorageForSort(T aValue, In aPoss)
         :  mValue(aValue)
         ,  mPoss(aPoss) 
      {
      }

      GeneralStorageForSort(const GeneralStorageForSort<T>&) = default;
      GeneralStorageForSort(GeneralStorageForSort<T>&&) = default;
      GeneralStorageForSort<T>& operator=(const GeneralStorageForSort<T>&) = default;
      GeneralStorageForSort<T>& operator=(GeneralStorageForSort<T>&&) = default;

      operator T&()
      {
         return mValue;
      }

      operator const T&() const
      {
         return mValue;
      }
};

using StorageForSort = GeneralStorageForSort<Nb>;

/**
* @brief Functions for the compare needed for sorting.
**/
struct LessThanStorageForSort_t
{
   //! @cond DOXYGEN_SUPPRESS
   template<class T>
   bool operator()(const GeneralStorageForSort<T>& a, const GeneralStorageForSort<T>& b) const
   {
      return (a.GetValue() < b.GetValue());
   }
   //! @endcond DOXYGEN_SUPPRESS
};

extern LessThanStorageForSort_t LessThanStorageForSort;

struct AbsLessThanStorageForSort_t
{
   //! @cond DOXYGEN_SUPPRESS
   template<class T>
   bool operator()(const GeneralStorageForSort<T>& a, const GeneralStorageForSort<T>& b) const
   {
      return (std::fabs(a.GetValue()) < std::fabs(b.GetValue()));
   }
   //! @endcond DOXYGEN_SUPPRESS
};

extern AbsLessThanStorageForSort_t AbsLessThanStorageForSort;

/**
 * Sort3 wrapper (Sort3 is a historic name... it was the 3rd sorting algo that was made ;D )
 */
void Sort3(MidasVector& arEigVals, MidasMatrix& arEigVecs, bool aOrderAbsVals = false);
void Sort3(MidasVector& arEigVals, MidasMatrix& arEigVecs, MidasVector& arImEigVals, MidasMatrix& arLEigVecs, bool aOrderAbsVals);

//Similar versions, but only sorts the first aNfirst eigvals/vecs:
void Sort3(MidasVector& arEigVals, MidasMatrix& arEigVecs, In aNfirst, bool aOrderAbsVals = false);
void Sort3(MidasVector& arEigVals, MidasMatrix& arEigVecs, MidasVector& arImEigVals, MidasMatrix& arLEigVecs, In aNfirst, bool aOrderAbsVals);

// Similar, but sort a range of eigvals/vecs
void Sort3
   (  MidasVector&
   ,  MidasMatrix&
   ,  MidasVector&
   ,  MidasMatrix&
   ,  In
   ,  In
   ,  bool
   );

#endif /* STORAGEFORSORT_H_INCLUDED */
