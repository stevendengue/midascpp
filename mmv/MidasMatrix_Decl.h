/**
************************************************************************
* 
* @file                MidasMatrix_Decl.h
*
* Created:             07-11-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Midas Matrix class 
* 
* Last modified: Tue May 12, 2009  10:51AM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDASMATRIX_DECL_H_INCLUDED
#define MIDASMATRIX_DECL_H_INCLUDED

// std headers
#include <iostream> 
#include <string>
#include <complex>

// libmda headers
#include "libmda/interface.h"
#include "libmda/expr/interface.h"
#include "libmda/util/index_check.h"
#include "libmda/util/dimensions_check.h"
#include "libmda/meta/std_wrappers.h"
#include "libmda/util/Requesting.h"
#include "libmda/IMDA.h"

// midas headers
#include "inc_gen/Const.h"
#include "inc_gen/TypeDefs.h"
#include "mmv_traits.h"
#include "lapack_interface/ILapack.h"

#include "util/type_traits/Complex.h"

// some forward declarations of friends necessary
// The class itself
template<class T> class GeneralMidasMatrix;
// also declare MidasVEctor
template<class T> class GeneralMidasVector;
// the output overload operator
template<class T>
std::ostream& operator<<(std::ostream&,const GeneralMidasMatrix<T>&);
//transpose 
template<class T>
GeneralMidasMatrix<T> Transpose(const GeneralMidasMatrix<T>&);
// invert
template<class T>
void SqrtInvert(GeneralMidasMatrix<T>&);
template<class T>
typename GeneralMidasMatrix<T>::value_type InvertSymmetric(GeneralMidasMatrix<T>&);
template<class T>
typename GeneralMidasMatrix<T>::value_type InvertGeneral(GeneralMidasMatrix<T>&);
template<class T>
typename GeneralMidasMatrix<T>::value_type Invert(GeneralMidasMatrix<T>&);
//! Dot of matrices considered as vectors. First argument is conjugated (for complex T).
template<class T>
T Dot(const GeneralMidasMatrix<T>&, const GeneralMidasMatrix<T>&);

#ifdef MDEBUG
using matrix_dimensions_check = libmda::util::dimensions_check_stacktrace;
using matrix_index_check = libmda::util::index_check_stack_trace;
#else // PRODUCTION BUILD
using matrix_dimensions_check = libmda::util::dimensions_nocheck;
using matrix_index_check = libmda::util::index_nocheck;
#endif

template<class T>
class GeneralMidasMatrix: 
   public libmda::expression_interface<GeneralMidasMatrix<T>
                                     , traits<GeneralMidasMatrix<T> >
                                     , matrix_dimensions_check
                                     >
{
   public:
      typedef GeneralMidasMatrix<T>             type;
      typedef typename traits<type>::value_type value_type; // T
      typedef typename traits<type>::size_type  size_type;  // In
      static const int order = traits<type>::order;     // 2
      using real_t = midas::type_traits::RealTypeT<T>;

   private:
      //
      using libmda_interface = libmda::expression_interface<GeneralMidasMatrix<T>
                                                          , traits<GeneralMidasMatrix<T> >
                                                          , matrix_dimensions_check 
                                                          >;
      //
      using index_check_policy = matrix_index_check;

      In mNrows;      ///< Number of rows
      In mNcols;      ///< Number of columns
      T** mElements;  ///< Actual elements
      std::string mLabel;  ///< label for debugging
      
      //
      void Allocate();
      void Deallocate();

      //! Implementation for wrapping all of Is[Anti][Symmetric|Hermitian]().
      template<class F>
      bool IsSymmetricImpl(real_t aThr, const F& arFunc) const;

   public:
      //! constructor and initialize from number
      GeneralMidasMatrix(const In aNrows, const In aNcols,const T aX);    
      //! constructor only
      GeneralMidasMatrix(const In aNrows, const In aNcols);                
      //! constructor only for square matrix
      explicit GeneralMidasMatrix(const In aNrows=0);
      //! constructor only for square matrix
      GeneralMidasMatrix(const In aNrows,const T aX);               
      //! Copy constructor
      GeneralMidasMatrix(const GeneralMidasMatrix<T>&);                  
      //! Move constructor
      GeneralMidasMatrix(GeneralMidasMatrix<T>&&);
      //! constructor diagonal matrix from a vector.
      GeneralMidasMatrix(const In, const GeneralMidasVector<T>&);        
      //! construction from libmda expression
      template<class A, libmda::Require_order<A,2> = 0>
      GeneralMidasMatrix(const libmda::expr::expression_base<A>& aOther);

      //! destructor
      ~GeneralMidasMatrix();                                   

      void SetLabel(std::string aLabel) { mLabel = aLabel; }
      std::string GetLabel() { return mLabel; }
      
      //! Zero outof diagonal elements
      void MakeDiagonal();
      //! Set diagonal to a number
      void SetDiagToNb(T aX=C_1);
      //! Get diagonal as a vector.
      GeneralMidasVector<T> GetDiag() const;
      //! Set matrix to a unit matrix
      void Unit();
      //! Transpose the matrix (in same container)
      void Transpose();
      //! Is the matrix square?
      bool IsSquare() const;
      //! Symmetrize the matrix (in same container)
      void Symmetrize();
      //! Is matrix symmetric.
      bool IsSymmetric(real_t arThr=C_NB_EPSILON*C_10_2) const;
      //! Is matrix anti symmetric 
      bool IsAntiSymmetric(real_t arThr=C_NB_EPSILON*C_10_2) const;
      //! Is matrix Hermitian?
      bool IsHermitian(real_t arThr=C_NB_EPSILON*C_10_2) const;
      //! Is matrix anti-Hermitian? 
      bool IsAntiHermitian(real_t arThr=C_NB_EPSILON*C_10_2) const;
      //! Max. abs. deviation from symmetric matrix.
      real_t DevFromSymmetric() const;
      //! Max. abs. deviation from antisymmetric matrix.
      real_t DevFromAntiSymmetric() const;
      //! Zero Matrix
      void Zero();
      //! Zero Col of matrix 
      void ZeroCol(In aCol);
      //! Zero Row of matrix 
      void ZeroRow(In aRow);
      //! Zero Except Col of matrix 
      void ZeroExceptCol(In aCol);
      //! Zero Except Row of matrix 
      void ZeroExceptRow(In aRow);
      //! Clear matrix
      void Clear();
      //! return number of rows
      In Nrows() const;
      //! return number of cols
      In Ncols() const;
      //! overload [] - return reference, a[i][j] now i,j element
      inline T* operator[](In aI) const { index_check_policy::apply(aI,mNrows); return mElements[aI]; } 
      value_type&       operator()(const size_type aI, const size_type aJ)       { return at(aI,aJ); }
      const value_type& operator()(const size_type aI, const size_type aJ) const { return at(aI,aJ); }
      
      /**********************/
      /** libmda interface **/
      /**********************/
      value_type&       at(const size_type aI, const size_type aJ);
      const value_type& at(const size_type aI, const size_type aJ) const;

      using libmda_interface::operator(); // for char expression interface
      
      size_type size() const;

      template<int N, libmda::iEnable_if<N==0> = 0>
      size_type extent() const;

      template<int N, libmda::iEnable_if<N==1> = 0>
      size_type extent() const;
      /*************************/
      /** libmda interface end */
      /*************************/
      
      // Get elements
      T** GetElements() {return mElements;}
      void SetNewSize(const In& aNewLength,const bool aSaveOldData = true,const bool aSetNewZero=false); ///< Redefine size of square matrix 
      void SetNewSize(const In& aNewRows,const In& aNewCols, const bool aSaveOldData = true,const bool aSetNewZero=false); ///< Redefine size of non square matrix 
      void SetPointerToNull();
      void SetData(T** aElements, In aNrows, In aNcols);
      ///< Sets data to pointer... use with caution!
      void StealData(GeneralMidasMatrix<T>& aVictimMat);

      //! Copy assignment. 
      GeneralMidasMatrix<T>& operator=(const GeneralMidasMatrix<T>&);

      //! Move assignment.
      GeneralMidasMatrix<T>& operator=(GeneralMidasMatrix<T>&&);

      using libmda_interface::operator=; // to make assignable from libmda expressions

      void Scale(const T& aX);                             ///< Scale whole matrix by aX
      void ScaleRow(const T& aX,const In& aRow);           ///< Scale a row of the matrix by aX
      void ScaleCol(const T& aX,const In& aCol);           ///< Scale a col of the matrix by aX
      void ShiftRowIndexToZero(const In& arIdx);            ///< Change an index to be at zero
      void ShiftColIndexToZero(const In& arIdx);            ///< Change an index to be at zero
      void ShiftIndexToZero(const In& arIdx);               ///< Change an index (row and col) to be at zero
      real_t NormRow(const In& aRow) const;                           ///< Calc. norm of a row 
      real_t NormCol(const In& aCol) const;                           ///< Calc. norm of a col 
      real_t MetricNormCol(const In& aCol, const GeneralMidasMatrix<T>& aMetric) const; ///< Calc. CTSC norm of a col 
      
      void NormalizeColumns();                              ///< Normalize all columns to unit norm
      void NormalizeRows();                                 ///< Normalize all rows to unit norm
      void OrthogLowdin();                                  ///< Orthonormalize columns using Lowdin approach 
      void Copy(const GeneralMidasMatrix<T>& aM, In aNewRow=-I_1, In aNewCol=-I_1);
      ///> Copy the first elements
      void Reassign(const GeneralMidasMatrix<T>& aM, In aNewRow=-I_1, In aNewCol=-I_1);
      ///> Reassign matrix to the first aNewRow x aNewCol elements of another matrix.

      void MatrixFromVector(const GeneralMidasVector<T>& arV, bool aRowbyRow=true); ///< Arrange a vector in a matrix. Default row by row 
      void FullBandMatrix(GeneralMidasMatrix<T>& arM);  ///< Reconstruct a full banded matrix from non-zero elements
      
      // NB NB NB SHOULD THESE BE FRIENDS ???!!!... 
      friend T Dot<T>(const GeneralMidasMatrix<T>&,const GeneralMidasMatrix<T>&); ///< Inner product
      friend std::ostream& operator<< <T>(std::ostream&, const GeneralMidasMatrix<T>&);                ///< Output overload
      friend value_type InvertSymmetric<T>(GeneralMidasMatrix<T>&);  ///< Invert a symmetric midasmatrix
      friend value_type InvertGeneral<T>(GeneralMidasMatrix<T>&);  ///< Invert a general midasmatrix
      friend value_type Invert<T>(GeneralMidasMatrix<T>&);  ///< Invert a symmetric or general midasmatrix
      friend void SqrtInvert<T>(GeneralMidasMatrix<T>&);     ///< M^{-1/2}
      // NBNBNBNBNBNB END !
      
      T SumEle() const;                                      ///< Sum of all elements.
      real_t SumAbsEle() const;                                   ///< Sum of absolute values of all elements.
      real_t Norm2() const;                                       ///< sum of elements squared of all elements
      real_t Norm() const;                                        ///< Usual norm (sqrt of norm2), the Frobenius norm
      T Trace() const;                                       ///< Usual norm (sqrt of norm2), the Frobenius norm
      real_t Norm2Block(Uin aRowBeg, Uin aRowEnd, Uin aColBeg, Uin aColEnd) const;
      T TraceProduct(const GeneralMidasMatrix<T>& aM) const;                      ///< Trace of a matrix product
      real_t Norm2OutOfDiagonal() const;                          ///< Sum of outofdiagonal elements square.
      real_t NormOutOfDiagonal() const;                           ///< sqrt(Sum of outofdiagonal elements square)
      T SumOutOfDiagonal() const;                            ///< sum of outofdiagonal elements 
      real_t SumAbsOutOfDiagonal() const;                         ///< sum of absolute values of outofdiagonal elements 
      real_t SumAbsOutOfDiagonalUpper() const;                    ///< sum of absolute values of outofdiagonal elements of upper triangle.
      real_t Norm2Diagonal() const;                               ///< Sum of outofdiagonal elements square.
      real_t NormDiagonal() const;                                ///< sqrt(Sum of outofdiagonal elements square)
      T LargestElement() const;                               ///< Find Largest Element values
      real_t LargestAbsElement() const;                            ///< Find Largest Absolute Element value

      void GetRow(GeneralMidasVector<T>& arV,In aIrow,In aIoff=I_0, In aIstride=I_1) const;
      ///< Get a row of the midas matrix to a MidasVector.
      
      void GetCol(GeneralMidasVector<T>& arV,In aIcol,In aIoff=I_0, In aIstride=I_1) const;
      ///< Get a column of the midas matrix to a MidasVector.
      void GetOffsetCol(GeneralMidasVector<T>& arV,In aIcol,In aIColOff=I_0) const;
      ///< Get an offset column of the midas matrix to a MidasVector.
      void GetOffsetRow(GeneralMidasVector<T>& arV,In aIrow,In aIRowOff=I_0) const;
      ///< Get an offset row of the midas matrix to a MidasVector.
      void GetSubCol(GeneralMidasVector<T>& arV, const In aIcol, const In aStart, const In aEnd);
      ///< Get a subvec of the column of the midas matrix.
      
      void AssignRow(const GeneralMidasVector<T>& arV,In aIrow,In aIoff=I_0, In aIstride=I_1);
      ///< Assign arV to row aIrow.
      
      void AssignCol(const GeneralMidasVector<T>& arV,In aICol,In aIoff=I_0, In aIstride=I_1, In aIMatOffset=I_0);
      ///< Assign aarV to column aIcol.
      
      void AssignToSubMatrix(const GeneralMidasMatrix<T>& arM,In aIrowStart,In aNrows,In aIcolStart, In aNcols);

      void SwapCols(In aIcol1,In aIcol2);
      ///< Swap two columns of a matrix.

      bool ConjGrad(GeneralMidasVector<T>& arSol, const GeneralMidasVector<T>& arRhs, 
            real_t& aResid, In& aIter, const real_t aTol= T(C_NB_EPSILON*C_10), 
            const In aMaxIter = I_10_2);
      ///< Conjugate gradient sol of A*Sol=rhs to resid residual (to be better than aTol) in iter iterations (at most MaxIter).
      // Default is to close to machine precision.

      //! Conjugate (and transpose!), i.e. result has elements `m_ij = conj(*this_ji)`.
      GeneralMidasMatrix<T> Conjugate() const;

      //! Conjugate elements of matrix (but no transposition!). No effect for real T.
      void ConjugateElems();

      void CopyDataFromPtr(T**,int,int,ilapack::col_major_t);
      void CopyDataFromPtr(T**,int,int,ilapack::row_major_t);

      real_t DifferenceNorm(const GeneralMidasMatrix<T>&) const;
      //! Condition number as ||A^-1|| * ||A||. This function assumes symmetric matrix.
      real_t ConditionNumber() const;
      //! Condition number as s_max(A) / s_min(A).
      real_t ConditionNumberSVD() const;
};

#endif // MIDASMATRIX_DECL_H_INCLUDED

