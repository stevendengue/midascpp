/**
************************************************************************
* 
* @file                DataCont.h
*
* Created:             02-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Data Container for Nb in Mem/On Disc
* 
* Last modified: Thu Nov 12, 2009  04:25PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef DATACONT_DECL_H_INCLUDED
#define DATACONT_DECL_H_INCLUDED

// std headers
#include <iostream> 
#include <string> 
#include <vector> 
#include <algorithm>
#include <memory>

// libmda headers
#include "libmda/util/static_value.h"

#include "mpi/Impi.h"

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "it_solver/ZeroVector.h"
#include "util/Io.h"

// forward declaration of friends
// The class itself
template<class T> class GeneralDataCont;
//
template<class T>
ostream& operator<<(ostream&, const GeneralDataCont<T>&);

//! \f$ a\dagger b \f$
template<class T>
T Dot(const GeneralDataCont<T>&, const GeneralDataCont<T>&,const In aIbegin=I_0,const In aN=-I_1);

//! Like Dot, but without the complex conjugation. (I.e. equivalent to Dot for real numbers.)
template<class T>
T SumProdElems
   (  const GeneralDataCont<T>&
   ,  const GeneralDataCont<T>&
   ,  const Uin aBeg = 0
   ,  const Uin aEnd = std::numeric_limits<Uin>::max()
   );

// end forward declares
//
/**
 * Class for automagically creating correct label
 **/
class LabelType
{
   private:
      std::string         mPrefix = "";
      std::string         mSuffix = "";
      std::string         mBaseLabel;
      mutable std::string mLabel;
      mutable bool        mUpdated = false;

   public:
      LabelType(const std::string& aBaseLabel = "")
         //:  mSuffix("_rank_" + std::to_string(midas::mpi::GlobalRank()))
         :  mSuffix("")
         ,  mBaseLabel(aBaseLabel)
      {
      }

      LabelType(const LabelType&)            = default;
      LabelType(LabelType&&)                 = default;
      LabelType& operator=(const LabelType&) = default;
      LabelType& operator=(LabelType&&)      = default;

      LabelType& operator=(const std::string& aBaseLabel)
      {
         this->Set(aBaseLabel);
         return *this;
      }
      
      LabelType& operator=(std::string&& aBaseLabel)
      {
         this->Set(std::move(aBaseLabel));
         return *this;
      }
      
      LabelType& operator+=(const std::string& aBaseLabel)
      {
         this->Set(aBaseLabel);
         return *this;
      }

      operator std::string()
      {
         return this->Get();
      }
      
      operator const std::string() const
      {
         return this->Get();
      }
      
      operator const std::string&() const
      {
         return this->Get();
      }
      
      void Set(std::string&& aBaseLabel) 
      {
         std::swap(mBaseLabel, aBaseLabel);
         mUpdated   = false;
      }
   
      void Set(const std::string& aBaseLabel) 
      {
         mBaseLabel = aBaseLabel;
         mUpdated   = false;
      }

      const std::string& Get() const
      {
         if(!mUpdated)
         {
            mLabel   = mPrefix + mBaseLabel + mSuffix;
            mUpdated = true;
         }
         return mLabel;
      }

      auto size() const
      {
         this->Get();
         return mLabel.size();
      }
};

inline std::ostream& operator<<(std::ostream& aOstream, const LabelType& aLabel)
{
   aOstream << aLabel.Get();
   return aOstream;
}

inline LabelType operator+(const LabelType& aLabel, const std::string& aStr)
{
   LabelType label_copy(aLabel);
   label_copy += aStr;
   return label_copy;
}

inline LabelType operator+(const std::string& aStr, const LabelType& aLabel)
{
   LabelType label_copy(aLabel);
   label_copy += aStr;
   return label_copy;
}

inline bool InquireDataContFile(const std::string& aName)
{
   // Inquire file
   LabelType label(aName);
   std::string inquire_name   = label.Get() + "_0";
   bool        inquire_result = InquireFile(inquire_name);

   // Some debug output
   if (MPI_DEBUG)
   {
      midas::mpi::WriteToLog(" Inquire DataCont '" + inquire_name + "' with result : " + (inquire_result ? "true" : "false") + ".");
   }

   return inquire_result;
}

/** 
* Data are stored in core or on disc. 
* */
template<class T>
class GeneralDataCont
{
   public:
      using real_t = midas::type_traits::RealTypeT<T>;

      /*********************************************************************//**
       * @brief
       *    Return status for I/O functions such as DataIo().
       *
       * It's a scoped enum so a given return status must be checked explicitly
       * against `SUCCESS` or `ERROR`.
       * Suggestions for usage, given some arbitrary `GeneralDataCont<T>`
       * object `dc` (the \:: just means ::, it's so doxygen won't complain):
       * -  `auto io_stat = dc.DataIo(...)`
       * -  if (io_stat == decltype(dc)\::Io\::SUCCESS) (or `ERROR`)
       * -  Or locally (not at global scope in header file!) defining 
       *    using io_t = [typename] decltype(dc)\::Io; (or similar)
       *    an call as io_t\::SUCCESS (or `ERROR`). The `typename` may/may not
       *    be necessary depending on context.
       ************************************************************************/
      enum class Io: int
      {  SUCCESS
      ,  ERROR
      };

   private:
      bool mAllInCore;                                  ///< All data are in core at this moment. 
      bool mAllOnDisc;                                  ///< All data are on disc at this moment. 
      bool mSaveUponDecon;                              ///< Save data on disc at deconstruction.
      In mNdata;                                        ///< Number of data. 
      GeneralMidasVector<T> mData;                      ///< The data as a vector if in core.
      std::string mLabel;                               ///< File label if on disc.

      //! False: other.mLabel+"_copy" when copy constructing. True: Just other.mLabel.
      bool mSameLabelWhenCopying = false;

   public:
      /**
       * Product switches
       **/
      struct Product
      {
         enum ProductType { NORMAL_TYPE, INVERSE_TYPE };

         typedef libmda::util::static_value<ProductType,ProductType::NORMAL_TYPE> (*Normal_t) ();
         typedef libmda::util::static_value<ProductType,ProductType::INVERSE_TYPE> (*Inverse_t) ();

         static inline libmda::util::static_value<ProductType,ProductType::NORMAL_TYPE> Normal() { return 0; }
         static inline libmda::util::static_value<ProductType,ProductType::INVERSE_TYPE> Inverse() { return 0; }
      };


      GeneralDataCont
         (  const GeneralMidasVector<T>&
         ,  const std::string& aStorage = "InMem"
         ,  const std::string& aFileLabel="X"
         ,  bool aSaveUponDecon = false
         );
      GeneralDataCont(const GeneralMidasMatrix<T>&, const string& aStorage = "InMem", const string& aFileLabel="X",
               bool aSaveUponDecon = false);

      GeneralDataCont(const In aN=I_0, const T aX=T(C_0), const string& aStorage = "InMem",
               const string& aFileLabel="X", bool aSaveUponDecon = false);

      GeneralDataCont(const GeneralDataCont<T>&);
      GeneralDataCont(const ZeroVector<T>&);
      ~GeneralDataCont();

      //!@{
      //! Default move ctor/assign; take ownership of everything including file label.
      GeneralDataCont(GeneralDataCont<T>&&) = default;
      GeneralDataCont& operator=(GeneralDataCont<T>&&) = default;
      //!@}

      GeneralMidasVector<T>* GetVector() {return &mData;}
      const GeneralMidasVector<T>* GetVector() const {return &mData;}

      void GetFromExistingOnDisc(const In aN,string aFileLabel="X");
      bool UseAvailableOnDisc();

      /*********************************************************************//**
       * @name DataIo
       *
       * Functions for getting/putting data from/to the DataCont object to/from
       * a MidasVector, MidasMatrix or just a single T reference. 
       * NB! Do **not** (ab)use the single-element version within loop
       * constructs, since it will cause unnecessary additional file operations
       * -- use the vector/matrix versions instead!
       ************************************************************************/
      //!@{
      //@{
      //! Get/put from/to DataCont object to/from MidasVector/MidasMatrix.
      template<int N, class... Args, std::enable_if_t<bool(N & IO_GET), int> = 0>
      Io DataIo(const IoType<N>&, GeneralMidasVector<T>&, Args&&...) const;

      template<int N, class... Args, std::enable_if_t<bool(N & IO_PUT), int> = 0>
      Io DataIo(const IoType<N>&, const GeneralMidasVector<T>&, Args&&...);

      template<int N, class... Args, std::enable_if_t<bool(N & IO_GET), int> = 0>
      Io DataIo(const IoType<N>&, GeneralMidasMatrix<T>&, Args&&...) const;

      template<int N, class... Args, std::enable_if_t<bool(N & IO_PUT), int> = 0>
      Io DataIo(const IoType<N>&, const GeneralMidasMatrix<T>&, Args&&...);
      //@}

      //@{
      //! Get/put single element from/to DataCont. **Deprecated within loops!**
      template<int N, std::enable_if_t<bool(N & IO_GET), int> = 0>
      Io DataIo(const IoType<N>&, const In&, T&) const;

      template<int N, std::enable_if_t<bool(N & IO_PUT), int> = 0>
      Io DataIo(const IoType<N>&, const In&, const T&);
      //@}
      //!@}

      In Size() const;                         ///< Return number of data.
      In TotalSize() const { return this->Size(); }
      In SizeOut() const;                      ///< Output summary of DataCont size.
      
      void SetToUnitVec(const In& arIdx);
      ///< Zero all data except index arIdx which is set to 1.
      
      void PutToNumber(const T& aNb)
      { GeneralMidasVector<T> dum; DataIo(IO_PUT,dum,Size(),I_0,I_1,I_0,I_1,true,aNb); }
      ///< Set DataContainer to a number. 
      
      void Zero();     ///< Zero all data.
      
      void SetNewSize(const In aN,bool aSaveOldData=true);
      ///< Resize container. New sections are not initialized!
      
      void Delete();   ///< Delete contents and files, so only empty container remains.

      void NewLabel(const std::string& aNewLabel,bool aMoveDataAlong=true,bool aSaveCopy=false);
      ///< Change label for container (and file names).
      
      const string& Label() const {return mLabel;}
      ///< Return label for container.
      
      bool ChangeStorageTo(std::string aStorage,bool aRobust = false, bool aCheck = false,bool aLetRestOnDisc=false);
      ///< Change storage between InMem and OnDisc
      
      bool ChangeStorageLabelOnly(std::string aStorage);   ///< Change storage label only. NO CHECKS! 

      string Storage() const
      { if (mAllOnDisc) return "OnDisc"; if (mAllInCore) return "InMem"; return "?"; }
      ///< Return storage mode.
      
      bool InMem() const {if (mAllInCore) return true; return false;} ///< Return storage logically.
      bool OnDisc() const {if (mAllOnDisc) return true; return false;} ///< Return storage logically.

      void SaveUponDecon(bool aKeep = true);
      ///< Set if data are to be stored after deconstruction of object.

      void DumpToDisc(string aS="");               ///< Writes data to disc on files aS.

      //! If true; copy of *this gets same label, no "_copy" appended (if in memory).
      void SetSameLabelWhenCopying(bool);

      //! Return Norm of data. (i.e. \f$ sqrt(a\dagger a) \f$).
      real_t Norm(In aOff=I_0, In aN=-I_1) const;

      //! Sum of aN elements starting at address aOff.
      T SumEle(In aOff=I_0, In aN=-I_1) const;
      
      //! Absolute sum of elements. For debugging only, all data put into memory!
      real_t SumAbsEle(In aOff=I_0, In aN=-I_1) const;

      //! Norm squared of aN elements starting at address aOff.
      real_t Norm2(In aOff=I_0, In aN=-I_1) const;
      
      //! Difference norm squared of aN elements starting at address aOff.
      real_t DiffNorm2(const GeneralDataCont& arOther, In aOff=I_0, In aN=-I_1) const;

      //! Return `|this_scale*(*this) - other_scale*other|^2`, or possibly with `Conj(*this)`.
      template<bool CONJ_THIS = false>
      real_t DiffNorm2Scaled
         (  const GeneralDataCont& arOther
         ,  const T aThisScale = T(1)
         ,  const T aOtherScale = T(1)
         ,  In aOff = 0
         ,  In aN = -1
         )  const;
      
      GeneralDataCont<T>& operator=(const GeneralDataCont<T>&);

      //! Prints information about object internals and associated file.
      void PrintDiagnostics(std::ostream&) const;

      //! mData += arX * arDc.mData.
      void Axpy(const GeneralDataCont<T>& arDc, const T arX);

      //@{
      //! Inserts range [aBeg;aEnd) into this starting at aPos.
      template<class InpIter>
      void Insert(Uin aPos, InpIter aBeg, InpIter aEnd);

      //! Insert value at aPos.
      void Insert(Uin aPos, T aVal);
      //@}

      //! Erases elements in range [aBeg;min(aEnd,Size())), reducing size accordingly.
      void Erase(Uin aBeg, Uin aEnd);

      /*********************************************************************//**
       * @name Loop over elements
       *
       * Loop over elements of `this` object (and other containers),
       * performing element-wise operations as specified by the functor F,
       * which must implement an `operator()` with one of these signatures
       * ```cpp
       *     void operator()([const] T&);                       // (a)
       *     void operator()([const] T&, const T&);             // (b)
       *     void operator()([const] T&, const T&, const T&);   // (c)
       * ```
       * where the `const` of the first argument depends on whether you're
       * calling the `const` member function or not, and version a/b/c
       * according to how many other CONT_Ts in the LoopOverElements() you
       * call.
       *
       * The functions correctly handle loading GeneralDataCont%s into memory
       * in batches, if either this/other is stored on disk.
       ************************************************************************/
      //!@{

      //! Loop through this, updating `this` according to F functor; example impl.: Scale.
      template<class F>
      void LoopOverElements
         (  F&& f
         ,  const Uin aBeg = 0
         ,  const Uin aEnd = std::numeric_limits<Uin>::max()
         );

      //! Loop through const `this`; example impl.: Norm2 (calculated by F functor).
      template<class F>
      void LoopOverElements
         (  F&& f
         ,  const Uin aBeg = 0
         ,  const Uin aEnd = std::numeric_limits<Uin>::max()
         )  const;

      //! Loop through this+other, updating `this` according to F functor; example impl.: Axpy.
      template<class F, class CONT_T>
      void LoopOverElements
         (  F&& f
         ,  const CONT_T& arOther
         ,  const Uin aBeg = 0
         ,  const Uin aEnd = std::numeric_limits<Uin>::max()
         );

      //! Loop through this+other, both const; example impl.: Dot (calculated by F functor).
      template<class F, class CONT_T>
      void LoopOverElements
         (  F&& f
         ,  const CONT_T& arOther
         ,  const Uin aBeg = 0
         ,  const Uin aEnd = std::numeric_limits<Uin>::max()
         )  const;

      //! Loop through this+others, updating `this` according to F functor.
      template<class F, class CONT_T>
      void LoopOverElements
         (  F&& f
         ,  const CONT_T& arOtherA
         ,  const CONT_T& arOtherB
         ,  const Uin aBeg = 0
         ,  const Uin aEnd = std::numeric_limits<Uin>::max()
         );

      //! Loop through this+others, all const.
      template<class F, class CONT_T>
      void LoopOverElements
         (  F&& f
         ,  const CONT_T& arOtherA
         ,  const CONT_T& arOtherB
         ,  const Uin aBeg = 0
         ,  const Uin aEnd = std::numeric_limits<Uin>::max()
         )  const;

      //!@}
      
      //! y[i] <- (x[i]+arX)^aI * y[i].
      void DirProd(const GeneralDataCont<T>& arDc,const T arX=T(C_0),const In aI = I_1);
      
      template<class V>
      void HadamardProduct(V&& arDc, const T aScal, typename GeneralDataCont<T>::Product::Normal_t);
      template<class V>
      void HadamardProduct(V&& arDc, const T aScal, typename GeneralDataCont<T>::Product::Inverse_t);

      void Reassign(GeneralDataCont<T>& arDc,In arDcOff=I_0,In aWhichWay=I_1,T aShift=T(C_0),const In aNdata=-I_1); 
      ///< Set elements of this equal to elements of arDc from arDcOff and onwards (or opposite).
      
      void Orthogonalize(const GeneralDataCont<T>& arDc);
      bool Normalize();
      bool IntermediateNormalize(In aI=I_0);
      void Scale(const T aNb,In arDcOff=I_0,In aN=-I_1);
      void Shift(const T aNb);
      void Pow(const T aNb);

      ////! \f$ a\dagger b \f$
      //friend T Dot<>(const GeneralDataCont<T>& arDc1, const GeneralDataCont<T>& arDc2,const In aBegin, const In aN);
      
      //! Addresses of aN extremum elements; min/max(-+1), min/max abs.vals (-+2).
      void AddressOfExtrema
         (  std::vector<In>& arAdds
         ,  std::vector<T>& arExtrema
         ,  const In aN
         ,  const In aMin=-I_1
         )  const;
      
      //! Addresses of aN values closest to/farthest away from (-+2) input nums.
      void AddressOfExtrema
         (  const std::vector<T>& arNbs
         ,  std::vector<In>& arAdds
         ,  std::vector<T>& arExtrema
         ,  const In aN
         ,  const In aMin=-I_1
         )  const;

      //! Transform vector aDcIn with aNrows x aNcols matrix stored in this.
      void TransformMx
         (  const In aNrows
         ,  const In aNcols
         ,  const GeneralDataCont<T>& arDcIn
         ,  GeneralDataCont<T>& arDcOut
         )  const;

   private:
      /*********************************************************************//**
       * @name DataIoImpl
       * 
       * Implementations for the DataIo functions. 
       *
       * A few historical notes:
       * - The functions were originally written (in the '00s, I presume) to
       *   handle both reading from (IO_GET) and writing to (IO_PUT).
       * - This meant that they could not be declared const (which they are for
       *   IO_GET), nor could the vector/matrix/scalar arguments be declared
       *   const (which they are for IO_PUT).
       * - With modern C++17 (templates, tag dispatch overloads,
       *   std::enable_if, SFINAE, constexpr if's), we've managed to separate
       *   the const/non-const parts at compile-time, meaning that the DataIo
       *   functions are now const correct.
       *
       * This is the reason for the (perhaps at first sight) obscure way these
       * DataIo and DataIoImpl functions are written. (-MBH, July 2018)
       ************************************************************************/
      //!@{
      //! Get/put matrix, implementation. To be called from DataIo.
      template 
         <  class DC
         ,  int N
         ,  class V
         ,  std::enable_if_t
            <  std::is_same_v<std::remove_const_t<V>, GeneralMidasVector<T>>
            ,  int
            >  = 0
         >
      static Io DataIoImpl
         (  DC& arDc
         ,  const IoType<N>& aPutGet
         ,  V& arV
         ,  In aLen=I_0
         ,  In aOffC=I_0
         ,  In aStrideC=I_1
         ,  In aOffV=I_0
         ,  In aStrideV=I_1
         ,  bool aPutToNb=false
         ,  T aNb=T(C_0)
         ,  bool aAddTo=false
         ,  T aScaleFac=T(C_1)
         );
      //! Get/put matrix, implementation. To be called from DataIo.
      template 
         <  class DC
         ,  int N
         ,  class M
         ,  std::enable_if_t
            <  std::is_same_v<std::remove_const_t<M>, GeneralMidasMatrix<T>>
            ,  int
            >  = 0
         >
      static Io DataIoImpl
         (  DC& arDc
         ,  const IoType<N>& aPutGet
         ,  M& arM
         ,  In aLen=I_0
         ,  In aNrows=I_0
         ,  In aNcols=I_0
         ,  In aOffC=I_0
         ,  In aStrideC=I_1
         ,  bool aPutToNb=false
         ,  T aNb=T(C_0)
         ,  In aNcolOld=-I_1
         ,  bool aAddTo=false
         ,  T aScaleFac=T(C_1)
         );
      //! Get/put a single element, implementation. To be called from DataIo.
      template 
         <  class DC
         ,  int N
         ,  class U
         ,  std::enable_if_t
            <  std::is_same_v<std::remove_const_t<U>, T>
            ,  int
            >  = 0
         >
      static Io DataIoImpl
         (  DC& arDc
         ,  const IoType<N>& aPutGet
         ,  const In& aAddress
         ,  U& aNb
         );
      //!@}

      /*********************************************************************//**
       * @name LoopOverElements [non-]const generic impls.
       *
       * Intended to be called by LoopOverElements() functions as
       * LoopOverElementsImpl(*this,...).
       * Thus the THIS_T template parameter should match non-const/const
       * GeneralDataCont<T>, and OTHER_T should match const
       * GeneralMidasVector<T>/const GeneralDataCont<T>.
       ************************************************************************/
      //!@{
      template<class THIS_T, class F>
      static void LoopOverElementsImpl(THIS_T&, F&&, const Uin, const Uin);
      template<class THIS_T, class F, class OTHER_T>
      static void LoopOverElementsImpl(THIS_T&, F&&, const OTHER_T&, const Uin, const Uin);
      template<class THIS_T, class F, class OTHER_T>
      static void LoopOverElementsImpl(THIS_T&, F&&, const OTHER_T&, const OTHER_T&, const Uin, const Uin);

      //!@}

      //! Master implementation for LoopOverElements.
      template
         <  class THIS_T
         ,  class OTHER_T
         ,  class F
         ,  size_t N
         >
      static void LoopOverElementsGenNumOthersImpl
         (  THIS_T& arThis
         ,  F&& f
         ,  const std::array<const OTHER_T*,N>& arOther
         ,  const Uin aBeg
         ,  const Uin aEnd
         );

      /*********************************************************************//**
       * @name Utility functions for LoopOverElementsGenNumOthersImpl
       ************************************************************************/
      //!@{
      template<size_t N>
      static void LoadToArray
         (  std::array<T,N>& arElems
         ,  const std::array<const GeneralMidasVector<T>*,N>& arConts
         ,  const Uin aIndex
         ,  const std::array<In,N>& arIndexOffset
         );

      template<size_t N>
      static bool AllContainersInCore(const std::array<const GeneralDataCont<T>*,N>& arConts);
      template<size_t N>
      static bool AllContainersInCore(const std::array<const GeneralMidasVector<T>*,N>& arConts);

      template<class CONT_T, class MV_T>
      static void LoadAndSetupPointer
         (  CONT_T& arCont
         ,  GeneralMidasVector<T>& arTmpMv
         ,  MV_T* & apPtr
         ,  In& arOffSet
         ,  Uin aBatchSize
         ,  Uin aAddress
         );
      //!@}
};

#endif /* DATACONT_DECL_H_INCLUDED */
