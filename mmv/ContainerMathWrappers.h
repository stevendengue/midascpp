/**
************************************************************************
* 
* @file    ContainerMathWrappers.h
*
* @date    17-12-2019
*
* @author  Niels Kristian Madsen (nielskm@chem.au.dk)
*
* @brief
*     Wrappers for various math operations on containers
*
* @note
*     These can also be extended for template template types to call themselves 
*     "recursively", e.g. calculate norm for vector<vector<Nb>> without implementing 
*     a Norm function. I have an implementation of that somewhere, but let's see if it becomes necessary.
* 
* @copyright
*     Ove Christiansen, Aarhus University.
*     The code may only be used and/or copied with the written permission 
*     of the author or in accordance with the terms and conditions under 
*     which the program was supplied.  The code is provided "as is" 
*     without any expressed or implied warranty.
* 
************************************************************************
**/

#ifndef CONTAINERMATHWRAPPERS_H_INCLUDED
#define CONTAINERMATHWRAPPERS_H_INCLUDED

#include "util/IsDetected.h"
#include "util/StaticLoop.h"
#include "util/type_traits/Tuple.h"
#include "util/Math.h"
#include "util/Error.h"
#include "util/AbsVal.h"

namespace midas::mmv
{
namespace detail
{
//! For detection
template<typename T> using has_zero_member_type = decltype(std::declval<T>().Zero());
template<typename T> using has_zero_func_type = decltype(Zero(std::declval<T>()));
template<typename T> using has_dot_member_type = decltype(std::declval<T>().Dot(std::declval<T>()));
template<typename T> using has_dot_func_type = decltype(Dot(std::declval<T>(), std::declval<T>()));
template<typename T, typename U> using has_axpy_member_type = decltype(std::declval<T>().Axpy(std::declval<T>(), std::declval<U>()));
template<typename T, typename U> using has_axpy_func_type = decltype(Axpy(std::declval<T>(), std::declval<T>(), std::declval<U>()));
template<typename T> using has_norm2_member_type = decltype(std::declval<T>().Norm2());
template<typename T> using has_norm2_func_type = decltype(Norm2(std::declval<T>()));
template<typename T> using has_inplace_multiplication_type = decltype(std::declval<T>()*=std::declval<T>());
template<typename T> using has_hadamard_product_type = decltype(std::declval<T>().HadamardProduct(std::declval<T>()));
template<typename T> using has_hadamard_product_func_type = decltype(HadamardProduct(std::declval<T&>(), std::declval<T>()));
} /* namespace detail */

/**
 * Zero a number
 *
 * @param arVec
 **/
template
   <  typename T
   ,  std::enable_if_t<std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>>* = nullptr
   >
inline void WrapZero
   (  T& arVec
   )
{
   arVec = T(0.0);
}

/**
 * Wrap Zero function
 *
 * @param arVec      Vector to be zeroed
 **/
template
   <  typename T
   ,  std::enable_if_t<!midas::type_traits::IsTupleV<T>>* = nullptr
   ,  std::enable_if_t<!(std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>)>* = nullptr
   >
void WrapZero
   (  T& arVec
   )
{
   [[maybe_unused]] constexpr bool has_member = midas::util::IsDetectedV<detail::has_zero_member_type, T>;
   constexpr bool has_func = midas::util::IsDetectedV<detail::has_zero_func_type, T>;

   if constexpr (  has_func
                )
   {
      Zero(arVec);
   }
   else if constexpr (  has_member
                     )
   {
      arVec.Zero();
   }
   else
   {
      // Allow ADL for custom begin/end
      using std::begin;
      using std::end;

      using value_t = typename std::iterator_traits<decltype(begin(arVec))>::value_type;
      static_assert(std::is_floating_point_v<value_t> || midas::type_traits::IsComplexV<value_t>, "Can only zero float or complex");
      for(auto& v : arVec)
      {
         v = value_t(0.0);
      }
   }
}

/**
 * Wrap Zero function for tuples
 *
 * @param arVec      Vector to be zeroed
 **/
template
   <  typename T
   ,  std::enable_if_t<midas::type_traits::IsTupleV<T>>* = nullptr
   >
void WrapZero
   (  T& arVec
   )
{
   midas::util::StaticFor<0, std::tuple_size_v<T>>::Loop
      (  [&arVec](auto I)
         {
            WrapZero(std::get<I>(arVec));
         }
      );
}

/**
 * Wrap Dot function
 *
 * @param aLeft
 * @param aRight
 * @return
 *    <aLeft|aRight>
 **/
template
   <  typename T
   >
inline auto WrapDotProduct
   (  const T& aLeft
   ,  const T& aRight
   ,  std::enable_if_t<std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>>* = nullptr
   )
{
   return midas::math::Conj(aLeft) * aRight;
}

/**
 * Wrap Dot function
 *
 * @param aLeft
 * @param aRight
 * @return
 *    <aLeft|aRight>
 **/
template
   <  typename T
   >
auto WrapDotProduct
   (  const T& aLeft
   ,  const T& aRight
   ,  std::enable_if_t<!midas::type_traits::IsTupleV<T>>* = nullptr
   ,  std::enable_if_t<!(std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>)>* = nullptr
   )
{
   [[maybe_unused]] constexpr bool has_member = midas::util::IsDetectedV<detail::has_dot_member_type, T>;
   constexpr bool has_func = midas::util::IsDetectedV<detail::has_dot_func_type, T>;

   if constexpr   (  has_func
                  )
   {
      return Dot(aLeft, aRight);
   }
   else if constexpr (  has_member
                     )
   {
      return aLeft.Dot(aRight);
   }
   else
   {
      // Allow ADL for custom begin/end
      using std::begin;
      using std::end;

      using value_t = typename std::iterator_traits<decltype(begin(aLeft))>::value_type;
      static_assert(std::is_floating_point_v<value_t> || midas::type_traits::IsComplexV<value_t>, "Can only dot float or complex");
      value_t result = 0.0;
      auto left_it = begin(aLeft);
      auto right_it = begin(aRight);
      auto e = end(aLeft);
      for(; left_it != e; ++left_it, ++right_it)
      {
         result += midas::math::Conj(*left_it) * (*right_it);
      }

      return result;
   }
}

/**
 * Wrap Dot function for tuples
 *
 * @param aLeft
 * @param aRight
 * @return
 *    <aLeft|aRight>
 **/
template
   <  typename T
   >
auto WrapDotProduct
   (  const T& aLeft
   ,  const T& aRight
   ,  std::enable_if_t<midas::type_traits::IsTupleV<T>>* = nullptr
   )
{
   // Use return type of first element
   using dot_t = decltype(WrapDotProduct(std::declval<std::tuple_element_t<0,T>>(), std::declval<std::tuple_element_t<0,T>>()));

   dot_t result = 0;

   midas::util::StaticFor<0,std::tuple_size_v<T>>::Loop
      (  [&result, &aLeft, &aRight](auto I)
         {
            result += WrapDotProduct(std::get<I>(aLeft), std::get<I>(aRight));
         }
      );

   return result;
}

/**
 * Wrap norm2 function
 *
 * @param aVec
 * @return
 *    ||aVec||^2
 **/
template
   <  typename T
   ,  std::enable_if_t<std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>>* = nullptr
   >
inline auto WrapNorm2
   (  const T& aVec
   )
{
   return midas::util::AbsVal2(aVec);
}

/**
 * Wrap norm2 function
 *
 * @param aVec
 * @return
 *    ||aVec||^2
 **/
template
   <  typename T
   ,  std::enable_if_t<!midas::type_traits::IsTupleV<T>>* = nullptr
   ,  std::enable_if_t<!(std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>)>* = nullptr
   >
auto WrapNorm2
   (  const T& aVec
   )
{
   [[maybe_unused]] constexpr bool has_member = midas::util::IsDetectedV<detail::has_norm2_member_type, T>;
   constexpr bool has_func   = midas::util::IsDetectedV<detail::has_norm2_func_type, T>;

   if constexpr   (  has_func
                  )
   {
      return Norm2(aVec);
   }
   else if constexpr (  has_member
                     )
   {
      return aVec.Norm2();
   }
   else
   {
      // Allow ADL for custom begin/end
      using std::begin;
      using std::end;

      using value_t = typename std::iterator_traits<decltype(begin(aVec))>::value_type;
      static_assert(std::is_floating_point_v<value_t> || midas::type_traits::IsComplexV<value_t>, "Can only take norm^2 for float or complex");
      using real_t = midas::type_traits::RealTypeT<value_t>;

      real_t norm2 = C_0;
      for(const auto& v : aVec)
      {
         norm2 += midas::util::AbsVal2(v);
      }

      return norm2;
   }
}

/**
 * Wrap norm2 function for tuples
 *
 * @param aVec
 * @return
 *    ||aVec||^2
 **/
template
   <  typename T
   ,  std::enable_if_t<midas::type_traits::IsTupleV<T>>* = nullptr
   >
auto WrapNorm2
   (  const T& aVec
   )
{
   using norm_t = decltype(WrapNorm2(std::declval<std::tuple_element_t<0,T>>()));

   norm_t norm2 = 0;

   midas::util::StaticFor<0,std::tuple_size_v<T>>::Loop
      (  [&norm2, &aVec](auto I)
         {
            norm2 += WrapNorm2(std::get<I>(aVec));
         }
      );

   return norm2;
}

/**
 * Wrap norm
 *
 * @param aVec
 * @return
 *    ||aVec||
 **/
template
   <  typename T
   >
auto WrapNorm
   (  const T& aVec
   )
{
   return std::sqrt(WrapNorm2(aVec));
}

/**
 * Wrap Axpy function
 *
 * @param arVec
 * @param aRight
 * @param aScalar
 **/
template
   <  typename T
   ,  typename U
   ,  std::enable_if_t<std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>>* = nullptr
   >
inline void WrapAxpy
   (  T& arVec
   ,  const T& aRight
   ,  U aScalar
   )
{
   arVec += aScalar*aRight;
}

/**
 * Wrap Axpy function
 *
 * @param arVec
 * @param aRight
 * @param aScalar
 **/
template
   <  typename T
   ,  typename U
   ,  std::enable_if_t<!midas::type_traits::IsTupleV<T>>* = nullptr
   ,  std::enable_if_t<!(std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>)>* = nullptr
   >
void WrapAxpy
   (  T& arVec
   ,  const T& aRight
   ,  U aScalar
   )
{
   static_assert(std::is_floating_point_v<U> || midas::type_traits::IsComplexV<U>);

   [[maybe_unused]] constexpr bool has_member = midas::util::IsDetectedV<detail::has_axpy_member_type, T, U>;
   constexpr bool has_func = midas::util::IsDetectedV<detail::has_axpy_func_type, T, U>;

   if constexpr   (  has_func
                  )
   {
      Axpy(arVec, aRight, aScalar);
   }
   else if constexpr (  has_member
                     )
   {
      arVec.Axpy(aRight, aScalar);
   }
   else
   {
      // Allow ADL for custom begin/end
      using std::begin;
      using std::end;

      auto left_it = begin(arVec);
      auto right_it = begin(aRight);
      auto e = end(arVec);
      for(; left_it != e; ++left_it, ++right_it)
      {
         (*left_it) += aScalar * (*right_it);
      }
   }
}

/**
 * Wrap Axpy function
 *
 * @param arVec
 * @param aRight
 * @param aScalar
 **/
template
   <  typename T
   ,  typename U
   ,  std::enable_if_t<midas::type_traits::IsTupleV<T>>* = nullptr
   >
void WrapAxpy
   (  T& arVec
   ,  const T& aRight
   ,  U aScalar
   )
{
   midas::util::StaticFor<0,std::tuple_size_v<T>>::Loop
      (  [&arVec, &aRight, &aScalar](auto I)
         {
            WrapAxpy(std::get<I>(arVec), std::get<I>(aRight), aScalar);
         }
      );
}

/**
 * Wrap HadamardProduct
 *
 * @param arVec
 * @param aRight
 **/
template
   <  typename T
   ,  std::enable_if_t<std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>>* = nullptr
   >
inline void WrapHadamardProduct
   (  T& arVec
   ,  const T& aRight
   )
{
   arVec *= aRight;
}

/**
 * Wrap HadamardProduct
 *
 * @param arVec
 * @param aRight
 **/
template
   <  typename T
   ,  std::enable_if_t<!midas::type_traits::IsTupleV<T>>* = nullptr
   ,  std::enable_if_t<!(std::is_floating_point_v<T> || midas::type_traits::IsComplexV<T>)>* = nullptr
   >
void WrapHadamardProduct
   (  T& arVec
   ,  const T& aRight
   )
{
   [[maybe_unused]] constexpr bool has_oper = midas::util::IsDetectedV<detail::has_inplace_multiplication_type, T>;
   [[maybe_unused]] constexpr bool has_memb = midas::util::IsDetectedV<detail::has_hadamard_product_type, T>;
   constexpr bool has_func = midas::util::IsDetectedV<detail::has_hadamard_product_func_type, T>;

   if constexpr   (  has_func
                  )
   {
      HadamardProduct(arVec, aRight);
   }
   else if constexpr (  has_memb
                     )
   {
      arVec.HadamardProduct(aRight);
   }
   else if constexpr (  has_oper
                     )
   {
      arVec *= aRight;
   }
   else
   {
      // Allow ADL
      using std::begin;
      using std::end;
      using std::distance;

      auto v_it = begin(arVec);
      auto v_end = end(arVec);
      auto prec_it = begin(aRight);
      auto prec_end = end(aRight);
      auto size_v = distance(v_it, v_end);
      auto size_prec = distance(prec_it, prec_end);
      if (  size_v != size_prec
         )
      {
         MIDASERROR("WrapHadamardProduct: Vector sizes do not match (" + std::to_string(size_v) + " vs. " + std::to_string(size_prec) + ")");
      }
      for(; v_it != v_end; ++v_it, ++prec_it)
      {
         (*v_it) *= (*prec_it);
      }
   }
}

/**
 * Wrap HadamardProduct for tuples
 *
 * @param arVec
 * @param aRight
 **/
template
   <  typename T
   ,  std::enable_if_t<midas::type_traits::IsTupleV<T>>* = nullptr
   >
void WrapHadamardProduct
   (  T& arVec
   ,  const T& aRight
   )
{
   midas::util::StaticFor<0,std::tuple_size_v<T>>::Loop
      (  [&arVec, &aRight](auto I)
         {
            WrapHadamardProduct(std::get<I>(arVec), std::get<I>(aRight));
         }
      );
}

} /* namespace midas::mmv */


#endif /* CONTAINERMATHWRAPPERS_H_INCLUDED */
