#ifndef MIDAS_MATRIX_OPER_MULT_H
#define MIDAS_MATRIX_OPER_MULT_H

#include <utility> // for std::declval

#include "libmda/meta/check_type_.h"

namespace midas
{
namespace mmv
{
   using libmda::meta::check_type_and_size_;

   /**
    * 
    **/
   template<class L, class R>
   struct mult_result
   {  
      using value_type = decltype(std::declval<typename L::value_type>()*std::declval<typename R::value_type>());
   };

   /**
    * 
    **/
   template<class L, class R>
   struct midas_matrix_oper_mult
   {
      template<int N, int M> struct order_help { static const int order = N+M-2; };
      template<int N> struct order_help<N,0>   { static const int order = N; };
      template<int N> struct order_help<0,N>   { static const int order = N; };
      
      static const int order = order_help<L::order,R::order>::order;
      
      using value_type = typename mult_result<L,R>::value_type;
      using size_type = typename L::size_type;

      // matrix * matrix
      template<class U>
      static 
      typename std::enable_if<std::is_same<U,L>::value && L::order==2 && R::order==2, value_type>::type 
      apply(const U& a_lhs, const R& a_rhs, size_type i, size_type j) 
      {
         value_type dot = 0;
         for(size_type k = 0; k<a_lhs.template extent<1>(); ++k)
            dot += a_lhs.at(i,k)*a_rhs.at(k,j);
         return dot;
      }
      
      template<class U>
      static size_type
      size(const U& a_lhs, const R& a_rhs,
            typename std::enable_if<std::is_same<U,L>::value && L::order==2 && R::order==2>::type* = 0) 
      { return a_lhs.template extent<0>()*a_rhs.template extent<1>(); }

      template<int N>
      static
      typename std::enable_if<N==0 && L::order==2  && R::order==2, size_type>::type 
      extent(const L& a_lhs, const R& a_rhs) 
      { return a_lhs.template extent<0>(); }
      
      template<int N>
      static
      typename std::enable_if<N==1 && L::order==2  && R::order==2, size_type>::type 
      extent(const L& a_lhs, const R& a_rhs) 
      { return a_rhs.template extent<1>(); }
      
      // matrix * vector
      template<class U>
      static 
      typename std::enable_if<std::is_same<U,L>::value && L::order==2 && R::order==1, value_type>::type 
      apply(const U& a_lhs, const R& a_rhs, size_type i) 
      {
         value_type dot = 0;
         for(size_type k = 0; k<a_lhs.template extent<1>(); ++k)
            dot += a_lhs.at(i,k)*a_rhs.at(k);
         return dot;
      }
      
      template<class U>
      static 
      typename std::enable_if<std::is_same<U,L>::value && L::order==2 && R::order==1, size_type>::type 
      size(const U& a_lhs, const R& a_rhs) 
      { return a_lhs.template extent<0>(); }

      template<int N>
      static
      typename std::enable_if<N==0 && L::order==2  && R::order==1, size_type>::type 
      extent(const L& a_lhs, const R& a_rhs) 
      { return a_lhs.template extent<0>(); }

      // vector * matrix
      template<class U>
      static 
      typename std::enable_if<std::is_same<U,L>::value && L::order==1 && R::order==2, value_type>::type 
      apply(const U& a_lhs, const R& a_rhs, size_type i) 
      {
         value_type dot = 0;
         for(size_type k = 0; k<a_rhs.template extent<0>(); ++k)
            dot += a_lhs.at(k)*a_rhs.at(k,i);
         return dot;
      }
      
      template<class U>
      static size_type 
      size(const U& a_lhs, const R& a_rhs, typename std::enable_if<std::is_same<U,L>::value && L::order==1 && R::order==2>::type* = 0) 
      { return a_rhs.template extent<1>(); }

      template<int N>
      static
      typename std::enable_if<N==0 && L::order==1  && R::order==2, size_type>::type 
      extent(const L& a_lhs, const R& a_rhs) 
      { return a_rhs.template extent<1>(); }

      // vector * vector ( dot )
      template<class U>
      static 
      typename std::enable_if<std::is_same<U,L>::value && L::order==1 && R::order==1, value_type>::type 
      apply(const U& a_lhs, const R& a_rhs) 
      {
         value_type dot = 0;
         for(size_type k = 0; k<a_lhs.template extent<0>(); ++k)
            dot += a_lhs.at(k)*a_rhs.at(k);
         return dot;
      }
      
      template<class U>
      static 
      typename std::enable_if<std::is_same<U,L>::value && L::order==1 && R::order==1, size_type>::type 
      size(const U& a_lhs, const R& a_rhs) 
      { return 0; }

      template<int N, class U>
      static
      typename std::enable_if<std::is_same<U,L>::value && N==0 && L::order==1 && R::order==1, size_type>::type 
      extent(const U& a_lhs, const R& a_rhs) 
      { return 0; }
      
      // matrix/vec * scal
      template<class U, typename... ints>
      static 
      typename std::enable_if<std::is_same<U,L>::value && R::order==0 && check_type_and_size_<L::order,size_type,ints...>::value, value_type>::type 
      apply(const U& a_lhs, const R& a_rhs, const ints... i) 
      { return a_lhs.at(i...)*a_rhs.at(); }
      
      template<class U>
      static 
      typename std::enable_if<std::is_same<U,L>::value && R::order==0, size_type>::type 
      size(const U& a_lhs, const R& a_rhs) 
      { return a_lhs.size(); }

      template<int N, class U>
      static
      typename std::enable_if<std::is_same<U,L>::value && R::order==0, size_type>::type 
      extent(const U& a_lhs, const R& a_rhs) 
      { return a_lhs.template extent<N>(); }

      // scal * matrix/vec
      template<class U, typename... ints>
      static 
      typename std::enable_if<std::is_same<U,R>::value && L::order==0 && check_type_and_size_<R::order,size_type,ints...>::value, value_type>::type 
      apply(const L& a_lhs, const U& a_rhs, const ints... i) 
      { return a_lhs.at()*a_rhs.at(i...); }
      
      template<class U>
      static 
      typename std::enable_if<std::is_same<U,R>::value && L::order==0, size_type>::type 
      size(const L& a_lhs, const U& a_rhs) 
      { return a_rhs.size(); }

      template<int N, class U>
      static
      typename std::enable_if<std::is_same<U,R>::value && L::order==0, size_type>::type 
      extent(const L& a_lhs, const U& a_rhs) 
      { return a_rhs.template extent<N>(); }
   }; // class midas_matrix_oper_mult

} // namespace mmv
} // namespace midas

#endif /* MIDAS_MATRIX_OPER_MULT_H */
