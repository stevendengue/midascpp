#ifndef MIDASMATRIX_H_INCLUDED
#define MIDASMATRIX_H_INCLUDED

// include declaration
#include "mmv/MidasMatrix_Decl.h"

#include "MidasVector.h" // needed in implementation of MidasMatrix

// include implementation
#include "mmv/MidasMatrix_Impl.h"

// some extra definitions
Nb pythag(const Nb a, const Nb b);

using MidasMatrix = GeneralMidasMatrix<Nb>;
using ComplexMidasMatrix = GeneralMidasMatrix<std::complex<Nb> >;

template<class T>
inline const T MAX(const T &a, const T &b)
{return b > a ? (b) : (a);}
template<class T>
inline const T MIN(const T &a, const T &b)
{return b < a ? (b) : (a);}
template<class T>
inline const T SIGN(const T &a, const T &b)
{return b >= 0 ? (a >= 0 ? a : -a) : (a >= 0 ? -a : a);}


template
   <  typename PARAM_T
   ,  typename PARAM_U
   ,  typename COEF_T
   >
void Axpy
   (  GeneralMidasMatrix<PARAM_T>& arThis
   ,  const GeneralMidasMatrix<PARAM_U>& aOther
   ,  COEF_T aCoef
   )
{
   auto nrows = arThis.Nrows();
   auto ncols = arThis.Ncols();

   assert(nrows == aOther.Nrows());
   assert(ncols == aOther.Ncols());

   for(In i=I_0; i<nrows; ++i)
   {
      for(In j=I_0; j<ncols; ++j)
      {
         arThis[i][j] += PARAM_T(aCoef * aOther[i][j]);
      }
   }
}

#endif /* MIDASMATRIX_H_INCLUDED */
