/*
************************************************************************
*
* @file                 mmv_omp_pragmas.h
*
* Created:              05-07-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Custom reductions used for OpenMP on MidasVector and MidasMatrix
*
* Last modified:        
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef MMV_OMP_PRAGMAS_H_INCLUDED
#define MMV_OMP_PRAGMAS_H_INCLUDED

#include "mmv/MidasVector.h"
#include "mmv/MidasMatrix.h"
#include "tensor/NiceTensor.h"
#include "vcc/TensorDataCont.h"

#include <complex>

//! Reduction of complex
#pragma omp declare reduction \
   (  ComplexFloat_Add \
   :  std::complex<float> \
   :  omp_out += omp_in   \
   ) \
   initializer \
      (  omp_priv = omp_orig \
      )

#pragma omp declare reduction \
   (  ComplexDouble_Add \
   :  std::complex<double> \
   :  omp_out += omp_in   \
   ) \
   initializer \
      (  omp_priv = omp_orig \
      )

#pragma omp declare reduction \
   (  ComplexNb_Add \
   :  std::complex<Nb> \
   :  omp_out += omp_in   \
   ) \
   initializer \
      (  omp_priv = omp_orig \
      )

//! Reduction of MidasVector
#pragma omp declare reduction \
   (  MidasVector_Add         \
   :  MidasVector             \
   :  omp_out += omp_in       \
   )                          \
   initializer                \
      (  omp_priv = omp_orig  \
      )

//! Reduction of ComplexMidasVector
#pragma omp declare reduction \
   (  ComplexMidasVector_Add  \
   :  ComplexMidasVector      \
   :  omp_out += omp_in       \
   )                          \
   initializer                \
      (  omp_priv = omp_orig  \
      )

//! Reduction of MidasMatrix
#pragma omp declare reduction \
   (  MidasMatrix_Add         \
   :  MidasMatrix             \
   :  omp_out += omp_in       \
   )                          \
   initializer                \
      (  omp_priv = omp_orig  \
      )

//! Reduction of ComplexMidasMatrix
#pragma omp declare reduction \
   (  ComplexMidasMatrix_Add  \
   :  ComplexMidasMatrix      \
   :  omp_out += omp_in       \
   )                          \
   initializer                \
      (  omp_priv = omp_orig  \
      )

//! Reduction of NiceTensor<Nb>
#pragma omp declare reduction \
   (  RealNiceTensor_Add      \
   :  NiceTensor<Nb>          \
   :  omp_out += omp_in       \
   )                          \
   initializer                \
      (  omp_priv = omp_orig  \
      )

//! Reduction of NiceTensor<std::complex<Nb>>
#pragma omp declare reduction       \
   (  ComplexNiceTensor_Add         \
   :  NiceTensor<std::complex<Nb>>  \
   :  omp_out += omp_in             \
   )                                \
   initializer                      \
      (  omp_priv = omp_orig        \
      )

//! Reduction of GeneralTensorDataCont<Nb>
#pragma omp declare reduction \
   (  RealTensorDataCont_Add  \
   :  GeneralTensorDataCont<Nb>  \
   :  omp_out += omp_in       \
   )                          \
   initializer                \
      (  omp_priv = omp_orig  \
      )

//! Reduction of GeneralTensorDataCont<std::complex<Nb>>
#pragma omp declare reduction       \
   (  ComplexTensorDataCont_Add     \
   :  GeneralTensorDataCont<std::complex<Nb>>  \
   :  omp_out += omp_in             \
   )                                \
   initializer                      \
      (  omp_priv = omp_orig        \
      )

#endif /* MMV_OMP_PRAGMAS_H_INCLUDED */
