/**
************************************************************************
* 
* @file                BmatBuilder.h 
*
* Created:             28-05-2017
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk) 
*
* Short Description:   Implementing a builder for the Wilson B matrix
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef BMATBUILDER_H
#define BMATBUILDER_H

#include <numeric>
#include <vector> 
using std::vector;

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/DataCont.h"
#include "mmv/MidasVector.h"

class BmatBuilder
{
  public:

   enum InternalType { SYMG2, SYMG4, STRETCH, INVSTRETCH, BEND, OUTOFPLANE, DIHEDRAL, LINCPBEND  };
   

  private:

   void GetDifferenceVector
      (  std::vector<Nb>& 
      ,  Nb& 
      ,  const Nb* 
      ,  const Nb* 
      ) const;

   void Cross
      (  std::vector<Nb>& 
      ,  const std::vector<Nb>&
      ,  const std::vector<Nb>& 
      ) const;

   void GetNormalVector
      (  std::vector<Nb>& 
      ,  const std::vector<Nb>& 
      ,  const std::vector<Nb>& 
      ) const;

   Nb arc1
      (  const Nb& x
      ,  const Nb& y
      ) const;

  public:   

   BmatBuilder() = default;      ///< Constructor 

   void GenerateRow
      (  const std::vector<Nb>& coord
      ,  const int& irow
      ,  Nb* bmat
      ,  Nb& val 
      ,  const int& itype
      ,  const std::vector<int>& iatoms 
      ,  const int& ncart
      ,  const int& idihed
      ,  Nb& savdihed
      ) const;

   void PseudoInvert
      (  Nb* bmat
      ,  const int& ncart
      ,  const int& nintern
      ) const;

   
};



#endif
