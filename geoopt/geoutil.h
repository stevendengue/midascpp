/**
 ************************************************************************
 * 
 * @file                geoutil.h 
 *
 * Created:             16-10-2018
 *
 * Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk) 
 *
 * Short Description:   General functions to do stuff to molecules
 *                      taken out of GeoOpt.h and GeoDatabase.h for
 *                      better organisation.
 * 
 * Last modified: 
 * 
 * Copyright:
 *
 * Ove Christiansen, Aarhus University.
 * The code may only be used and/or copied with the written permission 
 * of the author or in accordance with the terms and conditions under 
 * which the program was supplied.  The code is provided "as is" 
 * without any expressed or implied warranty.
 * 
 ************************************************************************
**/



#ifndef GEOUTIL_INCLUDED
#define GEOUTIL_INCLUDED

#include "geoopt/BmatBuilder.h"
#include "input/Input.h"


struct InternalCoord
{
   Nb val;
   int itype;
   int iatom = -1;
   std::vector<int> iconnect;
};

void GetBMatrix
   (  Nb* bmat
   ,  const std::vector<Nb>& cartcoord
   ,  const std::vector<InternalCoord>& interncoord
   );

void UpdateCoord
   (  vector<Nb>& cartcoord
   ,  vector<InternalCoord>& interncoord
   ,  vector<InternalCoord>& deltacoord  
   );

void CenterMolecule
   (  std::vector<Nb>& coord
   ,  std::vector<Nb>& amass
   );

void BuildProjector
   (  std::unique_ptr<Nb[]>& Projector
   ,  vector<Nb>& coord
   ,  vector<Nb>& amass
   ,  int natoms
   );

void cleanhessian
   (  std::unique_ptr<Nb[]>& hessian
   ,  vector<Nb>& coord
   ,  vector<Nb>& amass
   );

void cleangrad
   (  vector<Nb>& grad
   ,  vector<Nb>& coord
   ,  vector<Nb>& amass
   );

void NormalModeAnalysis
   (  std::vector<Nb>& xyz 
   ,  std::vector<Nb>& xyz_cart
   ,  std::vector<InternalCoord>& interncoord
   ,  std::vector<Nb>& qmass
   ,  std::vector<Nb>& amass
   ,  std::vector<Nb>& grad
   ,  std::unique_ptr<Nb[]>& hessian 
   ,  std::unique_ptr<Nb[]>& hess_cart 
   ,  const bool save_hessian
   ,  const bool intcoord
   ,  const bool have_gradient 
   ,  int natoms
   ,  int nintern
   );

void WriteInternalCoordinates
   (  ostream& out
   ,  std::vector<InternalCoord>& icoord  
   );

std::vector<InternalCoord> ReadInternalCoordinates
   (  std::ifstream& inp  
   );

void GetDelocalizedCoordinates
   (  std::vector<Nb>& eig
   ,  std::vector<Nb>& cartcoord
   ,  std::vector<InternalCoord>& interncoord
   );

std::vector<Nb> TransformHessian
   (  std::vector<Nb>& carthess
   ,  const std::vector<Nb>& cartcoord
   ,  const std::vector<InternalCoord>& interncoord
   ,  const int& natoms
   ,  const int& nintern
   ,  const int& ncart
   );

#endif /* GEOUTIL_INCLUDED */

