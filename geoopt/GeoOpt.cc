/**
************************************************************************
* 
* @file                GeoOpt.cc
*
* Created:             28-03-2017
*
* Author:              Gunnar Schmitz (gunnar.schmitz@chem.au.dk)
*
* Short Description:   Implementing GeoOpt class methods.
* 
* Last modified: 
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// Standard headers:
#include<iostream>
#include<string>
#include<list>
#include<algorithm>
#include<sstream>

// Link to standard headers:
#include "inc_gen/math_link.h" 

// My headers:
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Warnings.h"
#include "input/Input.h"
#include "input/GeoOptCalcDef.h"
#include "geoopt/GeoOpt.h"

#include "pes/molecule/MoleculeFile.h"
#include "mmv/MidasMatrix.h"

#include "mlearn/GauMixMod.h"
#include "mlearn/Kmeans.h"
//#include "mlearn/svm.h"
#include "mlearn/MLDescriptor.h"

/**
*  Constructor 
* */
GeoOpt::GeoOpt(GeoOptCalcDef* apCalcDef)
{
   mDatabase  = apCalcDef->GetDatabase();
   mCoordFile = apCalcDef->GetCoordFile();

   // Modus for the calculation
   mDoSample  = apCalcDef->GetCompModeSample();
   mDoEnergy  = apCalcDef->GetCompModeEnergy();
   mDoGrad    = apCalcDef->GetCompModeGradient();
   mDoHessian = apCalcDef->GetCompModeHessian();
   mDoOpt     = apCalcDef->GetCompModeOptimize();

   // Handling Gradient information
   mAddGrad       = apCalcDef->GetAddGrad();
   mFileAddGrad   = apCalcDef->GetFileAddGrad();
   mDamping       = apCalcDef->GetDamping();      

   // When not invoked for optimization
   mDoOnlyIcoord = apCalcDef->GetOnlyCoordAna();
   mOnlySimCheck = apCalcDef->GetOnlySimCheck();

   // Stuff for the GPR
   mKernelType    = apCalcDef->GetKernel();
   mSaveCovar     = apCalcDef->GetSaveCovar();
   mReadCovar     = apCalcDef->GetReadCovar();
   mFileCovarSave = apCalcDef->GetFileCovarSave();
   mFileCovarRead = apCalcDef->GetFileCovarRead();

   // Coordinates
   mCoordType = apCalcDef->GetCoordinateType();
}

/**
 *  @brief Writes a gradient in Turbomole format  
 *
 *  Purpose: Writes gradient in Turbomole format to be used together with Turbomole 
 *
 *  @param out is the output stream either file or stdout
 *
 *  @param coord is a vector with atomic positions  
 *
 *  @param symbols is a vector with atomic element symbols  
 *
 *  @param grad is a vector containing the gradient
 *
 *  @param energy is the energy for current geometry  
 *
 * */
void GeoOpt::WriteGradient
   (  ostream& out
   ,  vector<Nb>& coord
   ,  vector<string>& symbols
   ,  vector<Nb>& grad
   ,  Nb energy 
   )
{
   vector<vector<Nb>> coord_1d(1);
   vector<vector<Nb>> grad_1d(1);
   vector<Nb> energies_1d(1);

   coord_1d[0]    = coord;
   grad_1d[0]     = grad;
   energies_1d[0] = energy;

   WriteGradient(out, coord_1d, symbols, grad_1d, energies_1d);
}

/**
 *  @brief Writes a gradient in Turbomole format  
 *
 *  Purpose: Writes gradient in Turbomole format to be used together with Turbomole 
 *
 *  @param out is the output stream either file or stdout
 *
 *  @param coord is a vector with atomic positions  
 *
 *  @param symbols is a vector with atomic element symbols  
 *
 *  @param grad is a vector containing the gradient
 *
 *  @param energies contains the energies for the structures
 * */
void GeoOpt::WriteGradient
   (  ostream& out
   ,  vector<vector<Nb>>& coord
   ,  vector<string>& symbols
   ,  vector<vector<Nb>>& grad
   ,  vector<Nb>& energies 
   )
{

   int ndim    = coord[0].size();
   int natoms  = symbols.size();  
   int ncycles = grad.size();

   out << "$grad          cartesian gradients" << endl;

   for (int icycle = 1; icycle <= ncycles; icycle++)
   {
      int idx = icycle - 1;  

      // calculate gradient norm
      Nb gradnrm = 0.0;
      for (int i = 0; i < ndim; i++)
      {
         gradnrm += grad[idx][i]*grad[idx][i];
      }
      gradnrm = std::sqrt(gradnrm);

      out << "cycle =      " << icycle << " GPR gradient energy =    " << std::setprecision(14) << energies[idx] << " |dE/dxyz| =  " << gradnrm <<  std::endl;

      // print coordinates
      int isymb = 0;
      for (int icoord = 0; icoord < ndim; icoord += 3)
      {
         for (int ixyz = 0; ixyz < 3; ixyz++)
         {
            Nb xyz = coord[idx][icoord + ixyz];
            ios::fmtflags f( out.flags() );

            out << (xyz >= 0 ? " ":"") << std::fixed << std::showpoint;
            out << std::setprecision(8) << xyz  << "   "; 

            out.flags(f);
         }
         out << symbols[isymb++] << std::endl;
      }
      
      // print gradient
      for (int icoord = 0; icoord < ndim; icoord += 3)
      {
         for (int ixyz = 0; ixyz < 3; ixyz++)
         {
            Nb d = grad[idx][icoord + ixyz];
            ios::fmtflags f( out.flags() );

            out << (d >= 0 ? " ":"") << setw(15) << fixed <<  std::setprecision(14)  << d  << "   "; 

            out.flags(f);
         }
         out << std::endl;
      }
   }

   out << "$end" << std::endl;
}

/**
 *  @brief Reads in a gradient(s) in Turbomole format  
 *
 *  Purpose: Reads in a gradient(s) in Turbomole format to be used together with Turbomole 
 *
 *  @param fgrad is the name of the gradient file
 *
 *  @param coord is a vector with atomic positions  
 *
 *  @param symbols is a vector with atomic element symbols  
 *
 *  @param grad is a vector containing the gradient
 *
 *  @param energies is a vector for reading in the current energies
 *
 *  @param natoms contains the number of atoms
 *
 *  @param ishf tells if we want to read the gradient from an HF calculation
 *
 * */
void ReadGradientFile
   (  string fgrad
   ,  vector<vector<Nb>>& coord
   ,  vector<vector<string>>& symbols
   ,  vector<vector<Nb>>& grad
   ,  vector<Nb>& energies 
   ,  int natoms
   ,  bool ishf = false
   )
{

   string s;

   ifstream gradfile;
   gradfile.open (fgrad, ios::in );

   if (gradfile.good())
   {

      // clear old info
      coord.clear();
      symbols.clear();
      energies.clear();
      grad.clear();
      grad.clear();

      bool first = true;
      bool doread = true;

      while(midas::input::GetLine(gradfile, s) )
      {

         // 1) first line is just the datagroup section
         if (first)
         {
            string datagrp;
            std::istringstream str_stream(s);
            str_stream >> datagrp;
            first = false;
         }

         // 2) read cycle structure and gradient
         {
            string dum1, dum2, dum3, dum4, dum5, dum6;
            int icycle;
            Nb energy;

            if (!first && doread) midas::input::GetLine(gradfile, s);
            std::istringstream str_stream(s);  
        
            if (ishf)
            {
               Nb gradnrm = C_0;
               str_stream >> dum1 >> dum2 >> icycle >> dum3 >> dum4 >> dum5  >> energy;
            }
            else
            { 
               str_stream >> dum1 >> dum2 >> icycle >> dum3 >> dum4 >> dum5 >> dum6 >> energy;
            }
            energies.push_back(energy);
         } 

         // 3) read coordinates
         vector<Nb> newcoord;
         vector<string> newsymb;
         for (int iat = 0; iat < natoms; iat++)
         {
            Nb x, y, z;
            string label;

            midas::input::GetLine(gradfile, s);
            std::replace( s.begin(), s.end(), 'D', 'E');
            std::istringstream str_stream(s);

            str_stream >> x >> y >> z >> label;

            newcoord.push_back(x);
            newcoord.push_back(y);
            newcoord.push_back(z);

            newsymb.push_back(label);
         }
         coord.push_back(newcoord);
         symbols.push_back(newsymb);
         
         // 4) read gradient
         vector<Nb> newgrad;
         for (int iat = 0; iat < natoms; iat++)
         {
            Nb dx, dy, dz;

            midas::input::GetLine(gradfile, s);
            std::replace( s.begin(), s.end(), 'D', 'E');
            std::istringstream str_stream(s);

            str_stream >> dx >> dy >> dz ;

            newgrad.push_back(dx);
            newgrad.push_back(dy);
            newgrad.push_back(dz);
         }
         grad.push_back(newgrad);

         // 5) Check if this is the end
         {
            long ipos = gradfile.tellg();

            string datagrp;

            midas::input::GetLine(gradfile, s);
            std::istringstream str_stream(s);
            str_stream >> datagrp;

            if (datagrp == "$end") 
            {
               break;
            }
            else
            {
               gradfile.seekg(ipos);
               doread = false;
            }
         }

      }

      gradfile.close();
   }
}

/**
 *  @brief Sample other structures around the current one  
 *
 * */
void GeoOpt::CreateSample()
{

   // sample
   GeoDatabase mol(this->mCoordFile, 2);

   int nmol = mol.GetSize();

   // read in training data if it exists
   GeoDatabase geodb(this->mCoordType, this->mDatabase);

   for (int imol = 0; imol < nmol; imol++)
   {
      std::ofstream molfile;
      std::stringstream ss;
      ss << imol;
      std::string file = "mol." + ss.str() + ".xyz";

      if (!geodb.ContainsMolecule(1e-2, mol, imol) )
      {

         molfile.open (file, ios::out);

         mol.PrintMolecule(molfile, imol, 0.0, true);

         molfile.close();

      }
   }

}


/**
 *  @brief Does Optimization (an other things) on GPR potential  
 *
 * */
void GeoOpt::Optimize()
{

   const bool locdbg = false;

   if (mDoOnlyIcoord || mOnlySimCheck)
   {

      if (mDoOnlyIcoord)
      {
         GeoDatabase molecule(this->mCoordType, this->mCoordFile); 
     
         Mout << "coordinates" << std::endl;
 
         molecule.PrintMolecule(0, 0.0, false);
      
         ofstream molfile;
         string file = "intern_coord";

         molfile.open (file, ios::out);

         molecule.PrintMolecule(molfile, 0, 0.0, false);

         molfile.close();
      }

      if (mOnlySimCheck)
      {
         GeoDatabase database(this->mCoordType, this->mDatabase);
         GeoDatabase molecule(this->mCoordType, this->mCoordFile); 
      
         if (database.ContainsMolecule(1e-3, molecule, 0) )
         {
            Mout << "Molecule already in database" << std::endl;
         }
         else
         {
            Mout << "Molecule is unique" << std::endl;
         }
         
      }

      return;
   }

   if (mDoSample)
   {
      CreateSample();
   }

   bool dogpr = mDoEnergy || mDoGrad || mDoHessian || mDoOpt;

   //................................................... 
   // Define Gaussian Process via ... 
   //................................................... 
   if (dogpr)
   {

      //-------------------------------------------------+
      // ...training data 
      //-------------------------------------------------+
      GeoDatabase geodb(this->mCoordType, this->mDatabase);
      GeoDatabase geosp(this->mCoordType, this->mDatabase);

      std::vector<vector<Nb>> coord = geodb.GetTrajectory();
      std::vector<Nb>         ener  = geodb.GetEnergies();
     
      int ndata = coord.size();

      std::vector<MLDescriptor<Nb>> database;
      database.reserve(ndata);

      for (int imol = 0; imol < ndata; imol++)
      {
         MLDescriptor<Nb> mol(coord[imol],MLDescriptor<Nb>::SCALAR);
         database.push_back(mol);
      }       


      //GS
      for (int i = 0; i < ndata; i++)
      {
         Mout << "Energy " << ener[i] << " Coordinates " << coord[0].size()  << std::endl;
         geosp.PrintMolecule(Mout, i, 0.0 ,false); 
      }
      //GS

      // sort data according to similarity to target structure
      vector<Nb> rmsd;
      vector<Nb> noise(ndata);
      {
         GeoDatabase molecule(this->mCoordType, this->mCoordFile);
         rmsd = geodb.GetRMSD(molecule, 0);

         Mout << "Similarity to target structure:" << std::endl;
         Mout << std::endl;
         for (int imol = 0; imol < rmsd.size(); imol++)
         {
            Mout << "  Structure " << std::setw(4) << imol << " RMSD = " << rmsd[imol] << std::endl;
            //noise[imol] = 1e-9 ; //C_0; //std::pow(rmsd[imol],3.0) * 1.E-7;
            //noise[imol] = 1e-8 * rmsd[imol] ; //std::pow(rmsd[imol],3.0) * 1.E-7;
            //noise[imol] = 1e-7 ; 
            noise[imol] = 1e-9 ; 
         }
         Mout << std::endl;
      }

      //Nb minener = *(std::min_element(ener.begin(), ener.begin() + ener.size()));
      Nb minener = 0.0;
      Mout << std::endl << "  Minimal energy in training set: " << minener << std::endl << std::endl;

      Mout << "Finished reading in training data: I have " << ndata << " points"  << std::endl;

      //-------------------------------------------------+
      // ... and a kernel
      //-------------------------------------------------+

      vector<Nb> hparam{1.e-7};
      int nintern = coord[0].size();   
      int nhparam = nintern + 1;

      // read initial hyper parameters if avaialable
      std::ifstream infile("hparam.in");

      if (infile.good())
      {
         hparam.resize(0);
         Nb value;
         while ( infile >> value ) 
         {
            hparam.push_back(value);
         }

         infile.close();
         hparam.resize(nhparam);
      }
      else
      {
         hparam.insert(hparam.end(), nintern, 2.0);
      }
      Mout << "hparam " << hparam << std::endl;

      // define kernel type
      GPKernel<Nb>::KernelType ktype = mlearn::GetKernelTypeFromString<Nb>(mKernelType);
      GPKernel<Nb> kern(hparam, ktype);

      //GauPro<Nb> gp(kern, minener, GauPro<Nb>::CovAlgo::USESVD);
      GPConstMean<Nb> GPZERO(C_0);
      GauPro<Nb> gp(kern, &GPZERO, GauPro<Nb>::CovAlgo::USECHOL);
      //GauPro<Nb> gp(kern, minener, GauPro<Nb>::CovAlgo::USECONJGRAD);
      //GauPro<Nb> gp(kern, minener, GauPro<Nb>::CovAlgo::USELU);

      gp.SetData(database, ener, noise);

      if (! mReadCovar)
      {

         gp.OptimizeHparams();

         gp.SetHyperParameters(hparam, 0);

         // dump optimized set to file
         std::vector<Nb> hparam_opt = gp.GetHyperParameters(0);
         std::ofstream outfile("hparam.in"); 
         for (int iparam = 0; iparam < nhparam; iparam++)
         {
            outfile << hparam_opt[iparam] << std::endl;
         }
         outfile.close();
      }
      else
      {
         Mout << "  Read Covariance Matrix:" << std::endl;
         Mout << "  -----------------------" << std::endl;
         Mout << "  Marginal likelihood: " << gp.GetMarginalLikelihood() << std::endl;
         gp.ReadCovarianceMatrix(mFileCovarRead);
         MidasWarning("Covariance matrix read from file. Only very limited sanity checks. You know what you are doing!");
      }

      if (mSaveCovar)
      {
         ofstream outcovar;
         outcovar.open (mFileCovarSave, ios::out | ios::trunc);
         gp.StoreCovarianceMatrix(outcovar);
         outcovar.close();
      }

      //................................................... 
      // Predict energy via GPR 
      //...................................................
      if (mDoEnergy || mDoGrad)
      { 

         Mout << "..................................................................." << std::endl;
         Mout << "                  Predict energy via GPR                           " << std::endl;
         Mout << "..................................................................." << std::endl;
         Mout << std::endl;

         GeoDatabase molecule(this->mCoordType, this->mCoordFile); 

         // new coordinates and also energy..
         vector<vector<Nb>>  xnew    = molecule.GetTrajectory(); 
         vector<Nb>          newener = molecule.GetEnergies();   
         vector<string>      symbols = molecule.GetSymbols(0);     

         int nnewpoints = xnew.size();
         std::vector<MLDescriptor<Nb>> points;
         points.reserve(nnewpoints);
         for (int ipt = 0; ipt < nnewpoints; ipt++)
         {
            MLDescriptor<Nb> point(xnew[ipt],MLDescriptor<Nb>::SCALAR);
            points.push_back(point);
         }

         // predict energy
         std::tuple<vector<Nb>, vector<Nb>> gpout = gp.Predict(points);

         vector<Nb> energuess = std::get<0>(gpout);
         vector<Nb> varguess  = std::get<1>(gpout);   
         
         Nb delta = energuess[0] - newener[0];

         Mout << "Predicted energy: " << energuess[0] << " +/- " << std::abs(varguess[0]) << " Difference to correct val: " <<  delta << std::endl;

         molecule.PrintMolecule(0, varguess[0], true);
         molecule.PrintMolecule(0, varguess[0], false);

         if (false)
         {
            Mout << " Do scan" << std::endl;
            MLDescriptor<Nb> mol(xnew[0],MLDescriptor<Nb>::SCALAR);
            std::tuple<vector<Nb>, vector<Nb>> cut1d = gp.Scan1D(mol, 0, 0.8, 0.7, 30 );
            vector<Nb> xval = std::get<0>(cut1d);   
            vector<Nb> yval = std::get<1>(cut1d);   

            for (int idim = 0; idim < xval.size(); idim++)
            {
               Mout << xval[idim] << " " << yval[idim] << std::endl;
            } 
         }        

         if (mDoGrad && gp.HaveGradient())
         {
            const bool usemodpot = false;   // Use a model potential (intended for debugging)

            Mout << std::endl;
            Mout << "..................................................................." << std::endl;
            Mout << "                  Predict gradient via GPR                         " << std::endl;
            Mout << "..................................................................." << std::endl;
            Mout << std::endl;

            int natoms = molecule.GetNumAtoms();
            vector<Nb> cartcoord = molecule.GetCartCoord(0);
            vector<Nb> amass = molecule.GetAtomicMasses(0);

            MLDescriptor<Nb> descriptor(xnew[0],MLDescriptor<Nb>::SCALAR);
            vector<Nb> grad = gp.GetGradient(descriptor);

            // Read old gradient information
            vector<vector<Nb>> coordinates;
            vector<vector<Nb>> gradients;
            vector<vector<string>> newsymbols;
            vector<Nb> newenergies;

            string fgrad = "gradient";
            ReadGradientFile(fgrad, coordinates, newsymbols, gradients, newenergies, natoms); 

            if (molecule.UseInternalCoordinates())
            {
               int nintern = molecule.GetCoordSize(0);
               int ncart = 3*natoms;

               if (locdbg) Mout << "natoms " << natoms << std::endl;
               if (locdbg) Mout << "ncart " << ncart << std::endl;

               // Convert gradient in internal coordinates to cartesian coordinates
               std::unique_ptr<Nb[]> bmat = molecule.GetBMatrix(0);

               Mout << "GPR gradient (internal coordinates)" << std::endl;
               for (int icomp = 0; icomp < grad.size(); icomp++)
               {
                  Nb dval = grad[icomp];
                  Mout << " icomp = " << std::setw(4) << icomp << " value = " << (dval >= 0 ? " ":"") << dval << " type = " << molecule.GetCoordinateType(0, icomp) << std::endl;
               }
               Mout << std::endl;
            
               vector<Nb> grad_cart(ncart);

               char nn = 'N';
               char nt = 'T';
               int nvec = 1 ;
               Nb one  = 1.0;
               Nb zero = 0.0;
               
               // g_x = B g_int   
               midas::lapack_interface::gemm( &nn, &nn, &ncart, &nvec, &nintern
                                            , &one, bmat.get(), &ncart
                                            , &grad[0], &nintern, &zero
                                            , &grad_cart[0], &ncart );
               grad = grad_cart;
            }

            Mout << "GPR gradient (cartesian coordinates)" << std::endl << std::endl;
            WriteGradient(Mout, cartcoord, symbols, grad, energuess[0]); 
            Mout << std::endl;


            if (mAddGrad && usemodpot == false)
            {
               // read an additional gradient and just add them on top
               // for example read exact HF gradient and combine it 
               // with the GPR-MP2 gradient
               vector<vector<Nb>>     addcoord;
               vector<vector<Nb>>     addgrad;
               vector<vector<string>> addsymb;
               vector<Nb> addener;

               string fgrad = mFileAddGrad;
               bool ishf = true;
               ReadGradientFile(fgrad, addcoord, addsymb, addgrad, addener, natoms, ishf); 
         
               Mout << "SCF gradient" << std::endl << std::endl;
               WriteGradient(Mout, cartcoord, symbols, addgrad[0], addener[0]); 
               Mout << std::endl;

               Mout << std::endl << " Current damping is " << mDamping << std::endl << std::endl;

               for (int idx = 0; idx < grad.size(); idx++)
               {
                  grad[idx] = mDamping * grad[idx] + addgrad[0][idx];

                  // Probably a symmetry zero
                  if (std::abs(addgrad[0][idx]) < 10e-13) 
                  {
                     grad[idx] = C_0;
                  } 
               }

               // Project out rotation and translation
               cleangrad(grad, cartcoord, amass);

               // gradient is already in the correct coordinate space
               Mout << "GPR+HF gradient" << std::endl << std::endl;
               WriteGradient(Mout, cartcoord, symbols, grad, addener[0] + energuess[0]); 
               Mout << std::endl;

               Mout << "addener " << addener << std::endl; 

               energuess[0] += addener[0];
            }


            // And add new gradient information
            coordinates.push_back(cartcoord);
            gradients.push_back(grad);
            newenergies.push_back(energuess[0]);

            ofstream gradfile;
            gradfile.open ("gradient", ios::out | ios::trunc);
            WriteGradient(gradfile, coordinates, symbols, gradients, newenergies);
            gradfile.close();
            Mout << "Gradient written to file gradient" << std::endl;
         }

      }

      //................................................... 
      // Optimize structure with steepest decent + GPR 
      //...................................................

      bool doopt = mDoOpt && gp.HaveGradient();

      
      if (doopt) 
      {

         Mout << std::endl;
         Mout << "..................................................................." << std::endl;
         Mout << "                  Do optimization on GPR potential                 " << std::endl;
         Mout << "..................................................................." << std::endl;
         Mout << std::endl;

         MIDASERROR("The internal GeoOptimizer is broken at the moment!");  // We can not properly fall back to cartesian coordinates

         int niter = 0;
         int ndim = coord[0].size();               // dimension of the problem 3N in our case

         Nb gamma = 0.1;
         Nb precision = 0.001;
         Nb delta = 10.0;

         GeoDatabase molecule(this->mCoordType, this->mCoordFile); 

         // new coordinates and also energy..
         std::vector<vector<Nb>>  xnew    = molecule.GetTrajectory(); 
         std::vector<Nb>          newener = molecule.GetEnergies();   
         std::vector<string>      symbols = molecule.GetSymbols(0);     

         std::vector<Nb> cur_coord; 
         std::vector<Nb> old_coord;
         
         cur_coord = xnew[0];

         std::vector<Nb> oldgrad;

         while (delta > precision && niter < 100)
         {
            delta = 0.0;

            std::vector<Nb> diffx;
            std::transform(cur_coord.begin(),cur_coord.end(),old_coord.begin(),diffx.begin(),std::minus<Nb>());

            old_coord = cur_coord;
            MLDescriptor<Nb> old_descriptor(xnew[0],MLDescriptor<Nb>::SCALAR);
            vector<Nb> grad = gp.GetGradient(old_descriptor);

            std::vector<Nb> diffg;
            std::transform(grad.begin(),grad.end(),oldgrad.begin(),diffg.begin(),std::minus<Nb>());

            // Determine an appropriate value for the step length
            if (niter > 0) 
            {
               // Gamma = ( (x^(n) - x^(n-1))^T (dG(x^(n) - dG(x^(n-1)))  ) / || dG(x^(n) - dG(x^(n-1)) ||^2 
               gamma = std::inner_product(std::begin(diffx), std::end(diffx), std::begin(diffg), 0.0);
               gamma /= std::inner_product(std::begin(diffg), std::end(diffg), std::begin(diffg), 0.0);
            }

            for (int idx = 0; idx < ndim; idx++)
            {
               cur_coord[idx] += -gamma * grad[idx]; 
               delta += std::abs(cur_coord[idx] - old_coord[idx]);
            }
            Mout << "iter " << niter << " |dGrad| " << delta << std::endl;

            niter++;
            oldgrad = grad;
         }

         bool converged = delta < precision;

         Mout << "Converged: "   << converged << std::endl;

      }

      bool dohess = mDoHessian && gp.HaveHessian();;

      if (dohess) 
      {
         GeoDatabase molecule(this->mCoordType, this->mCoordFile); 

         FrequencyAnalysis(gp, molecule);
      }

   }   
}


/**
 *  @brief Does a normal mode analysis using the GPR potential  
 *
 *  Purpose: Does a normal mode analysis using the GPR potential for a structure 
 *
 *  @param gp contains the GPR potential
 *
 *  @param mol contains the molecule for which a normal mode analysis should be performed  
 * 
 * */
void GeoOpt::FrequencyAnalysis
   (  GauPro<Nb>& gp
   ,  GeoDatabase& mol
   )
{

   const bool doH2sample = false;
   const bool intcoord = true;  // catch this later from mol...

   // Make a copy of the coordinates
   std::vector<Nb> xyz      = mol.GetCoordinates(0);
   std::vector<Nb> xyz_cart = mol.GetCartCoord(0);

   int ndim = xyz.size();
   int natoms = mol.GetNumAtoms();
   int nintern = mol.GetCoordSize(0);
   int ncart = 3 * natoms;

   //........................................................
   // Calculate Hessian for minimized structure
   //........................................................
   std::vector<Nb> qmass = mol.GetCoordinateMasses(0);
   std::vector<Nb> amass = mol.GetAtomicMasses(0);

   // calculate hessian
   std::unique_ptr<Nb[]> hessian(new Nb[ndim*ndim]);
   if (doH2sample)
   {
      // Very special hard coded test case for debuging...
      if (ndim != 6) MIDASERROR("Special case does not work!");

      hessian[0] =  0.3630063906; hessian[6]  =  0.0037501892; hessian[12] = -0.0941024876; hessian[18] = -0.3630063906; hessian[24] = -0.0037501892;  hessian[30] =  0.0941024876; 
      hessian[1] =  0.0037501892; hessian[7]  =  0.0000387429; hessian[13] = -0.0009721651; hessian[19] = -0.0037501892; hessian[25] = -0.0000387429;  hessian[31] =  0.0009721651;
      hessian[2] = -0.0941024876; hessian[8]  = -0.0009721651; hessian[14] =  0.0243942763; hessian[20] =  0.0941024876; hessian[26] =  0.0009721651;  hessian[32] = -0.0243942763;
      hessian[3] = -0.3630063906; hessian[9]  = -0.0037501892; hessian[15] =  0.0941024876; hessian[21] =  0.3630063906; hessian[27] =  0.0037501892;  hessian[33] = -0.0941024876;
      hessian[4] = -0.0037501892; hessian[10] = -0.0000387429; hessian[16] =  0.0009721651; hessian[22] =  0.0037501892; hessian[28] =  0.0000387429;  hessian[34] = -0.0009721651;
      hessian[5] =  0.0941024876; hessian[11] =  0.0009721651; hessian[17] = -0.0243942763; hessian[23] = -0.0941024876; hessian[29] = -0.0009721651;  hessian[35] =  0.0243942763;
      
   }
   else
   {
      MLDescriptor<Nb> descriptor(xyz,MLDescriptor<Nb>::SCALAR);
      gp.GetHessian(hessian, descriptor);
   }

   MLDescriptor<Nb> descriptor(xyz,MLDescriptor<Nb>::SCALAR);
   std::vector<Nb> grad = gp.GetGradient(descriptor);

   std::vector<InternalCoord> interncoord = mol.GetIntCoord(0);

   std::unique_ptr<Nb[]> hess_cart(new Nb[ncart*ncart]); 

   NormalModeAnalysis( xyz, xyz_cart, interncoord
                     , qmass, amass
                     , grad, hessian, hess_cart, true
                     , intcoord, true, natoms, nintern);

}


 
