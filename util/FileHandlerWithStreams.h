/**
************************************************************************
* 
* @file                FileHandlerWithStreams.h
*
* Created:             06-06-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Class for handling the output of mop files
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#ifndef FILEHANDLERWITHSTREAMS_H
#define FILEHANDLERWITHSTREAMS_H

#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <stdarg.h>
#include <map>
#include <sstream>
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasVector.h"
#include "util/Io.h"
#include "util/FileHandler.h"

/**
* Quick explanation of what is going on here. We need some class to contain the information given from the fitting process 
* before we throw it to file. The reason is that we really don't know which functions we actually need to give to the output
* before we have the fitting results. Therefore we collect all the information as the fitting spits it out, and when it is done,
* i.e. the class will go out of scope and get destroyed, puts it to the files in the FileHandler in the correct order.
**/
namespace FileHandlerStreams{
   class FileHandlerWithStreams
   {
      private:
         std::stringstream                mConstantsForOutput; //< Stream for holding the output under the "Constants:" label
         std::stringstream*               mFunctionsForOutput; //< Stream for holding the output under the "Functions:" label
         std::stringstream*               mOperatorsForOutput; //< Stream for holding the output under the "Operator:" label
         std::stringstream*               mFitBasisForOutput; //< Stream for holding the output under the "FitBasis:" label
         std::stringstream*               mSplinesForOutput; //< Stream for holding the output under the "Splines:" label
         std::stringstream**              mStreams; //< Holding pointers to all the streams! ... Except mConstantsForOutput :P
         FileHandles::FileHandler         mFiles; //< This has the actual fstreams inside it
         In                               mNrOfStreams; //< How many streams do we have
         In                               mSpecOfStream; //< Which stream are we printing to
         In                               mGoingTo; //Where are we printing to: 0) 1) 2)
         bool                             mToHead; //< Are we printing to the header.. Aka Scaling/Constants
         
      public:
         FileHandlerWithStreams() 
            : mConstantsForOutput(std::string()) 
            , mFiles()
            , mNrOfStreams(I_0)
            , mSpecOfStream(-I_1)
            , mGoingTo(I_0)
            , mToHead(true)
         {
         }
         void InitStreamsAndFiles(const string& aString,In aNr,const string& aSecString){
            if(mNrOfStreams == 0)
            {
               mFiles.OpenFiles(aString, aNr, aSecString);
               mFitBasisForOutput  = new std::stringstream[aNr];
               mFunctionsForOutput = new std::stringstream[aNr];
               mOperatorsForOutput = new std::stringstream[aNr];
               mSplinesForOutput   = new std::stringstream[aNr];
               mStreams            = new std::stringstream*[I_4];
               mStreams[I_0] = mFitBasisForOutput;
               mStreams[I_1] = mFunctionsForOutput;
               mStreams[I_2] = mOperatorsForOutput;
               mStreams[I_3] = mSplinesForOutput;
               for(In i = I_0; i < aNr; ++i)
               {
                  mFunctionsForOutput[i] << std::scientific << std::setiosflags(ios::uppercase) << std::setprecision(22);
                  mOperatorsForOutput[i] << std::scientific << std::setiosflags(ios::uppercase) << std::setprecision(22);
                  mFitBasisForOutput[i]  << std::scientific << std::setiosflags(ios::uppercase) << std::setprecision(22);
                  mSplinesForOutput[i] << std::scientific << std::setiosflags(ios::uppercase) << std::setprecision(22);
               }
               mConstantsForOutput << std::scientific << std::setiosflags(ios::uppercase) << std::setprecision(22);
               mNrOfStreams = aNr;
            }
            else
               MIDASERROR("Do no call this function twice in your code");
         } //<Starts up our streams, might be a bit excessive to start all fstreams, but on the other hand we might as well
         ~FileHandlerWithStreams(); //<This does all the writing to files in the FileHandler
         FileHandlerWithStreams& operator<<(FileHandlerWithStreams& (*aFunc)(FileHandlerWithStreams&)) {return aFunc(*this);}
         //<This and the function below the class allows us to set the print to go to all streams
         FileHandlerWithStreams& operator<<(ostream& (*aFunc)(ostream&));
         //<This allows us to send endl and so forth into the streams
         FileHandlerWithStreams& operator<<(FileHandles::To_One ToOne){this->SetToHead(false);
                                               this->SetSpecOfStream(ToOne.mSpecOfStream); return *this;}
         //<For setting the output stream, we are here just using the struct from filehandles

         void Width(In aIn) {mFiles.Width(aIn);}
         void SetSpecOfStream(In aIn) {if(aIn > mNrOfStreams){MIDASERROR("Something terrible is wrong, check your code");}
                                       mSpecOfStream = aIn;} //<Helper function for setting the stream which we put our info into..
         void SetToHead(bool aBool) {mToHead = aBool;}
         void SetGoingTo(In aIn) {mGoingTo = aIn;}
         template <typename T> FileHandlerWithStreams& operator<<(T aType)
         {
            if(mToHead)
            {
               mConstantsForOutput << aType;
            }
            else
            {
               mStreams[mGoingTo][mSpecOfStream] << aType;
            }
            return *this;
         }
         //This handles all other input to the streams
   };

   static FileHandlerWithStreams& to_head(FileHandlerWithStreams& a)
   {
      a.SetToHead(true);
      return a;
   } //< Helper function for setting the streams back to printing everywhere...
   
   static FileHandlerWithStreams& to_func(FileHandlerWithStreams& a)
   {
      a.SetToHead(false);
      a.SetGoingTo(I_1);
      return a;
   } //< Helper function for setting the the output stream to FitBasis

   static FileHandlerWithStreams& to_basis(FileHandlerWithStreams& a)
   {
      a.SetToHead(false);
      a.SetGoingTo(I_0);
      return a;
   } //< Helper function for setting the the output stream to Functions

   static FileHandlerWithStreams& to_oper(FileHandlerWithStreams& a)
   {
      a.SetToHead(false);
      a.SetGoingTo(I_2);
      return a;
   } //< Helper function for setting the output stream to Operator

   static FileHandlerWithStreams& to_splines(FileHandlerWithStreams& a)
   {
      a.SetToHead(false);
      a.SetGoingTo(I_3);
      return a;

   }
}
#endif //FILEHANDLERWITHSTREAMS_H
