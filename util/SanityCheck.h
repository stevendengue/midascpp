/*
************************************************************************
*
* @file                 SanityCheck.h
*
* Created:              15-10-2018
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Functions for checking sanity of data.
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef SANITY_CHECK_H_INCLUDED
#define SANITY_CHECK_H_INCLUDED

#include "util/type_traits/Complex.h"
#include "util/IsDetected.h"

namespace midas::util
{

/**
 * Check if number is NaN
 *
 * @param aNumber
 * @return
 *    true if number is nan
 **/
template
   <  typename T
   >
bool IsNaN
   (  T aNumber
   ,  std::enable_if_t<!midas::type_traits::IsComplexV<T>>* = nullptr
   )
{
   return std::isnan(aNumber);
}

/**
 * Overload for std::complex
 *
 * @param aNumber
 * @return
 *    true if number is nan
 **/
template
   <  typename T
   ,  std::enable_if_t<midas::type_traits::IsComplexV<T>>* = nullptr
   >
bool IsNaN
   (  T aNumber
   )
{
   return std::isnan(std::real(aNumber)) || std::isnan(std::imag(aNumber));
}

/**
 * Check if number is finite.
 *
 * @param aNumber
 * @return
 *    True if finite
 **/
template
   <  typename T
   ,  std::enable_if_t<!midas::type_traits::IsComplexV<T>>* = nullptr
   >
bool IsFinite
   (  T aNumber
   )
{
   return std::isfinite(aNumber);
}

/**
 * Overload for std::complex
 *
 * @param aNumber
 * @return
 *    true if number is finite
 **/
template
   <  typename T
   ,  std::enable_if_t<midas::type_traits::IsComplexV<T>>* = nullptr
   >
bool IsFinite
   (  T aNumber
   )
{
   return std::isfinite(std::real(aNumber)) && std::isfinite(std::imag(aNumber));
}

/**
 * Check if a number is sane to work with.
 *
 * @param aNumber
 * @return
 *    true if number is sane
 **/
template
   <  typename T
   >
bool IsSane
   (  T aNumber
   )
{
   return IsFinite(aNumber) && !IsNaN(aNumber);
}

namespace detail
{
template
   <  typename T
   >
using has_sanity_check = decltype(std::declval<T>().SanityCheck());

template
   <  typename T
   >
using has_begin_member = decltype(std::declval<T>().begin());

template
   <  typename T
   >
using has_begin_friend = decltype(T::begin(std::declval<T&>()));

template
   <  typename T
   >
using has_end_member = decltype(std::declval<T>().end());

template
   <  typename T
   >
using has_end_friend = decltype(T::end(std::declval<T&>()));
} /* namespace detail */

/**
 * Default check. Assumes iterator over elements.
 *
 * @param aContainer       Container
 * @return
 *    true if no elements are nan or inf
 **/
template
   <  typename T
   ,  std::enable_if_t<!midas::util::IsDetectedV<detail::has_sanity_check, T> >* = nullptr
   ,  std::enable_if_t
      <  (  midas::util::IsDetectedV<detail::has_begin_member, T>
         || midas::util::IsDetectedV<detail::has_begin_friend, T>
         )
      && (  midas::util::IsDetectedV<detail::has_end_member, T>
         || midas::util::IsDetectedV<detail::has_end_friend, T>
         )
      >* = nullptr
   >
bool ContainerSanityCheck
   (  const T& aContainer
   )
{
   bool sane = true;

   for(const auto& elem : aContainer)
   {
      if (  !IsSane(elem)
         )
      {
         sane = false;
         break;
      }
   }

   return sane;
}

/**
 * If a specialized sanity check is implemented
 *
 * @param aContainer
 * @return
 *    The result of aContainer.SanityCheck()
 **/
template
   <  typename T
   ,  std::enable_if_t<midas::util::IsDetectedV<detail::has_sanity_check, T> >* = nullptr
   >
bool ContainerSanityCheck
   (  const T& aContainer
   )
{
   return aContainer.SanityCheck();
}

} /* namespace midas::util */

#endif /* SANITY_CHECK_H_INCLUDED */
