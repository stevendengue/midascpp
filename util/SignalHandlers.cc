#include "SignalHandlers.h"

#include <iostream>
#include <csignal>
#include <sys/ucontext.h>
#include <cstdlib>
#include <execinfo.h>
#include <unistd.h>
#include <dlfcn.h>
#include <string.h>

#include "mpi/Impi.h"


namespace midas
{
namespace util
{
namespace detail 
{

// Define macros
#define abort_exit_code -1

/**
 * Exit execution of program.
 *
 * @param exitcode    Exit code to return to calling enviroment.
 **/
void exit_execution(int exitcode)
{
   exit(exitcode);
}

/**
 * Abort execution of program.
 *
 * If MPI will call MPI_Abort, and if not MPI will call abort() which will raise SIGABRT.
 **/
void abort_execution()
{
#ifdef VAR_MPI
   mpi::detail::WRAP_Abort(mpi::CommunicatorWorld().GetMpiComm(), abort_exit_code);
#else
   abort();
#endif /* VAR_MPI */
}

/**
 * Print to stderr where to send error reports.
 **/
void print_contact_information()
{
   fprintf(stderr, "\n");
   fprintf(stderr, "################################################################\n");
   fprintf(stderr, "# \n");
   fprintf(stderr, "# Please send error reports to midascpp.support@chem.au.dk\n");
   fprintf(stderr, "# \n");
   fprintf(stderr, "# or post and issue on the MidasCpp GitLab issue board: \n");
   fprintf(stderr, "# \n");
   fprintf(stderr, "#   https://gitlab.com/groups/midascpp/issues\n");
   fprintf(stderr, "# \n");
   fprintf(stderr, "################################################################\n");
   fprintf(stderr, "\n");
}

/**
 * Print stack trace based on ucontext.
 *
 * @param context   Pointer to ucontext_t.
 **/
static void context_backtrace (ucontext_t *context)
{
    unsigned frame_number = 0;

    void *ip = NULL;
    void **bp = NULL;

#if defined(REG_RIP)
    std::cout << "REG_RIP" << std::endl;
    ip = (void*) context->uc_mcontext.gregs[REG_RIP];
    bp = (void**) context->uc_mcontext.gregs[REG_RBP];
#elif defined(REG_EIP)
    std::cout << "REG_EIP" << std::endl;
    ip = (void*) context->uc_mcontext.gregs[REG_EIP];
    bp = (void**) context->uc_mcontext.gregs[REG_EBP];
#endif

    while (bp && ip) {
        Dl_info dlinfo;
        if (!dladdr (ip, &dlinfo))
            break;

        const char *symbol = dlinfo.dli_sname;

        fprintf (stderr, "% 2d: %p <%s+%lu> (%s)\n",
                 ++frame_number,
                 ip,
                 symbol ? symbol : "(?)",
                 (uintptr_t) ip - (uintptr_t) dlinfo.dli_saddr,
                 dlinfo.dli_fname);

        if (dlinfo.dli_sname && strcmp (dlinfo.dli_sname, "main") == 0)
            break;

        ip = bp[1];
        bp = (void**) bp[0];
    }
}

/**
 * Print stacktrace to stderr.
 **/
void stacktrace(int signal)
{
   void *array[10];
   size_t size;
   
   // get void*'s for all entries on the stack
   size = backtrace(array, 10);

   char * signame = strsignal(signal);
   
   // print out all the frames to stderr
   fprintf(stderr, "Error: signal (%d) %s.\n", signal, signame);
   backtrace_symbols_fd(array, size, STDERR_FILENO);
   fprintf(stderr, "\n");
   fprintf(stderr, "If compiled with debug flags (-g) you can run 'addr2line':\n");
   fprintf(stderr, "   $ addr2line <addresses> -e /path/to/midascpp.x\n");
}

/**
 * Signal handler for SIGABRT.
 *
 * @param signal  Raised signal.
 * @param si      Pointer to siginfo_t.
 * @param ptr     Pointer to ucontext_t.
 **/
void sigabrt_handler(int signal, siginfo_t* si, void* ptr)
{
   print_contact_information();
   
   exit_execution(abort_exit_code);
}

/**
 * Setup signal handler for SIGABRT.
 **/
void setup_sigabrt_handler()
{
   struct sigaction s;
   s.sa_flags = SA_SIGINFO | SA_RESETHAND;
   s.sa_sigaction = sigabrt_handler;
   sigemptyset(&s.sa_mask);
   sigaction(SIGABRT, &s, 0);
}

/**
 * Signal handler for SIGSEGV.
 *
 * @param signal  Raised signal.
 * @param si      Pointer to siginfo_t.
 * @param ptr     Pointer to ucontext_t.
 **/
void sigsegv_handler(int signal, siginfo_t* si, void* ptr)
{
   ucontext_t *uc = (ucontext_t *)ptr;

   stacktrace(signal);

   context_backtrace(uc);

   abort_execution();
}

/**
 * Setup signal handler for SIGSEGV.
 **/
void setup_sigsegv_handler()
{
   struct sigaction s;
   s.sa_flags = SA_SIGINFO | SA_RESETHAND;
   s.sa_sigaction = sigsegv_handler;
   sigemptyset(&s.sa_mask);
   sigaction(SIGSEGV, &s, 0);
}

/**
 * Signal handler for SIGUSR2.
 *
 * Will print stacktrace.
 *
 * @param signal  Raised signal.
 * @param si      Pointer to siginfo_t.
 * @param ptr     Pointer to ucontext_t.
 **/
void sigusr2_handler(int signal, siginfo_t* si, void* ptr)
{
   ucontext_t *uc = (ucontext_t *)ptr;

   stacktrace(signal);

   //context_backtrace(uc);
}

/**
 * Setup signal hander for SIGUSR2.
 **/
void setup_sigusr2_handler()
{
   struct sigaction sa{ {0}, };
   sa.sa_sigaction = sigusr2_handler;
   sa.sa_flags = SA_SIGINFO | SA_RESTART | SA_NODEFER;
   sigaction (SIGUSR2, &sa, NULL);
}

// Undefine macros
#undef abort_exit_code

} /* namespace detail */

/**
 * Setup signal handlers for Midas.
 **/
void SetupSignalHandlers
   (
   )
{
   static bool initialized = false;
   if (!initialized)
   {
      detail::setup_sigabrt_handler();
      detail::setup_sigsegv_handler();
      detail::setup_sigusr2_handler();
      initialized = true;
   }
}

} /* namespace util */
} /* namespace midas */
