#include "Os.h"

#include <stdio.h>  /* defines FILENAME_MAX */

// Define getcwd_impl depending on OS
#ifdef WINDOWS 
   #include <direct.h>
   #define getcwd_impl _getcwd
   
   // define dummy gethostname() and chdir() for WINDOWS platform
   int gethostname(char*, int) { return -1; }
   int chdir(char*)            { return -1; }
#else
   #include <unistd.h>
   // chdir()
   // gethostname()
   #define getcwd_impl getcwd
#endif

#include <iostream>

namespace midas
{
namespace os
{
namespace detail
{

/**
 * Wrap low-level OS specific getcwd function.
 *
 * @param dir     Preallocated chararacter pointer. On output will hold current working directory.
 * @param size    Size of "dir" buffer.
 *
 * @return        Returns 0 on success else returns 'errno' set by getcwd.
 **/
int getcwd_wrap(char* dir, size_t size)
{
   if (!getcwd_impl(dir, size))
   {
      return errno;
   }

   dir[size-1] = '\0';
   
   return 0;
}

} /* namespace detail */

/**
 * Change working/running directory of the process.
 *
 * @param aDir     The directory to change to.
 * 
 * @return         Return 0 on success, else returns an errno.
 **/
int Chdir(const std::string& aDir)
{
   return chdir(aDir.c_str());
}

/**
 * Get current working directory.
 *
 * @return   Returns current workdink dir.
 **/
std::string Getcwd()
{
   //std::string dir;
   char current_path[FILENAME_MAX];
   int status = detail::getcwd_wrap(current_path, sizeof(current_path));
   if(status)
   {
      std::cerr << " COULD NOT GET CURRENT WORKING DIR " << std::endl;
   }
   return std::string(current_path);
}

/**
 * Get hostname of machine.
 *
 * @return   Returns hostname.
 **/
std::string Gethostname()
{
   #define HOST_SIZE 256
   char host[HOST_SIZE];
   #undef HOST_SIZE
   
   int status = gethostname(host, sizeof(host));

   if(status != 0)
   {
      return std::string("");
   }

   return std::string(host);
}

} /* namespace os */
} /* namespace midas */

