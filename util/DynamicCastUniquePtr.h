/*
************************************************************************
*
* @file                 DynamicCastUniquePtr.h
*
* Created:              22-03-2019
*
* Author:               Niels Kristian Madsen (nielskm@chem.au.dk)
*
* Short Description:    Dynamic cast of std::unique_ptr
*
* Last modified:
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission
* of the author or in accordance with the terms and conditions under
* which the program was supplied.  The code is provided "as is"
* without any expressed or implied warranty.
*
************************************************************************
*/

#ifndef DYNAMICCASTUNIQUEPTR_H_INCLUDED
#define DYNAMICCASTUNIQUEPTR_H_INCLUDED

#include <memory>

namespace midas::util
{

/**
 * Perform a dynamic cast of the pointer held by a std::unique_ptr
 *
 * @param   aPtr        Input pointer
 * @return
 *    New unique_ptr holding the cast pointer
 **/
template
   <  typename TO
   ,  typename FROM
   ,  template<typename> typename DELETER
   >
std::unique_ptr<TO, DELETER<TO> > DynamicCastUniquePtr
   (  std::unique_ptr<FROM, DELETER<FROM> >&& aPtr
   )
{
   // Try to cast the pointer to the desired type
   if (  TO* cast = dynamic_cast<TO*>(aPtr.get())
      )
   {
      // Init result from cast pointer
      std::unique_ptr<TO, DELETER<TO> > result(cast);

      // Ownership is taken over by the new pointer
      aPtr.release();

      // Return result
      return result;
   }
   else
   {
      // If the cast fails, return nullptr of correct type
      return std::unique_ptr<TO, DELETER<TO> >(nullptr);
   }
}

} /* namespace midas::util */

#endif /* DYNAMICCASTUNIQUEPTR_H_INCLUDED */
