/**
************************************************************************
* 
* @file                Plot2D.h
*
* Created:             4-10-2006
*
* Author:              D. Toffoli 
*
* Short Description:   Spectrum class definition and funtion
*                      declarations.
* 
* ???? Last modified: Mon Nov 06, 2006  09:11AM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef PLOT2D_H
#define PLOT2D_H

// std headers
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "mmv/MidasVector.h"

// using declarations
using std::string;

//! Class for storing the text and position of a label in 3D.
class Label2D
{
   public:
       Nb x;         ///< x coord.
       Nb y;         ///< y coord.
       Nb z;         ///< z coord.
       string lbl;   ///< Text.
};

//! Class for creating plots of data.
/** The class contins all points needed for the plot.
    On creation, the number of x-values is specified.
    It is then possible to add different shapes, e.g. gaussians
    and labels to the spectrum. */
class Plot2D
{
   protected:
       MidasVector mXvals;                  ///< x values.
       MidasVector mYvals;                  ///< y values.
       MidasVector mZvals;                  ///< z values.
       string mXlabel;
       string mYlabel;
       string mZlabel;
       string mTitle;
       vector<Label2D> mLabels;               ///< Labels to be placed on plot.

       Nb mPlotRangeXl;                     ///< Limits for axes in plot.
       Nb mPlotRangeXu;
       Nb mPlotRangeYl;
       Nb mPlotRangeYu;
       Nb mPlotRangeZl;
       Nb mPlotRangeZu;


       bool mLogScaleX;
       bool mLogScaleY;
       bool mLogScaleZ;

       string mLineStyle;                   ///< Line style that can be recognized by gnuplot.

       vector<string> mAnaFuncs;
       ///< Strings describing analytical functions to be plotted by gnuplot.
       
       vector<string> mAnaFuncTitles;       ///< Titles of analytical function.

       //void SortDataPoints(); we don't need it for the moment
       ///< Sort data points according to ascending x. Necessary for e.g. locate() to work.
       
       //In Locate(const Nb aX);              ///< Get an index in mXVals correspoindig to aX.
       void GenerateDataFile(const string& aFilename) const;

       bool mSmooth;                            ///< Do smoothing of data in plot 
       string mSmoothMethod;                    ///< Smoothing method 
   public:
       Plot2D(const In aSize);                ///< Size is no. of points on x axis.
       
       void SetRange(const Nb aXmin, const Nb aXmax, const Nb aYmin, const Nb aYmax);
       ///< Distribute numbers from aXmin to aXmax and aYmin to aYmax evenly across x and y axis. Clears z data.
       
       In SetXYZvals(const MidasVector& aX, const MidasVector& aY, const MidasVector& aZ);
       ///< Setting new x,y,z data. Returns 0 on succes.
       
       void SetXlabel(string aLabel) {mXlabel = aLabel;}
       void SetYlabel(string aLabel) {mYlabel = aLabel;}
       void SetZlabel(string aLabel) {mZlabel = aLabel;}
       void SetTitle(string aTitle) {mTitle = aTitle;}
      
       void SetPlotRange(Nb axl, Nb axu, Nb ayl, Nb ayu, Nb azl, Nb azu);
      
       void SetPlotRangeRel(Nb axl, Nb axu, Nb ayl, Nb ayu, Nb azl, Nb azu);
       ///< Set plot range relative to min,max values of x,y and z data.
       ///< xl=x,left  xr=x,right  yb=y,bottom,  yt=y,top.
       ///< E.g. x axis will run from xmin-axl*|xmin| to xmax+axr*|xmax|.

       Nb GetMinX() {return mXvals.FindMinValue();}
       Nb GetMaxX() {return mXvals.FindMaxValue();}
       Nb GetMinY() {return mYvals.FindMinValue();}
       Nb GetMaxY() {return mYvals.FindMaxValue();}
       Nb GetMinZ() {return mZvals.FindMinValue();}
       Nb GetMaxZ() {return mZvals.FindMaxValue();}


       void SetLogScaleX(bool b) {mLogScaleX = b;}
       void SetLogScaleY(bool b) {mLogScaleY = b;}
       void SetLogScaleZ(bool b) {mLogScaleZ = b;}

       void ScaleX(Nb aScale) {mXvals.Scale(aScale);}
       void ScaleY(Nb aScale) {mYvals.Scale(aScale);}
       void ScaleZ(Nb aScale) {mZvals.Scale(aScale);}

       void SetLineStyle(const string& s) {mLineStyle = s;}
       ///< Line style is to be recognized by gnuplot.

       //void AddGaussian(const Nb aXpos, const Nb aHeight, const Nb aWidth, const string& aLabel);
       ///< Add a gaussian. aWidth is FWHM.
       
       //void AddBar(const Nb aXpos, const Nb aHeight, const Nb aWidth, const string& aLabel);
       ///< Add a bar of width aWidth centered at aXpos. aWidth = 0 gives narrowest possible bar.

       void AddLabel(const Nb aXpos, const Nb aYpos, const Nb aZpos, const string& aLabel);  ///< Add a label.     
     
       //void AddAnaNormDist(const Nb aMu, const Nb aSigma, const string& aTitle = "auto");
       ///< Add an analytical normal distribution to plot

       //void SetSmooth(const string& aSmoothMethod) {mSmoothMethod=aSmoothMethod;mSmooth=true;} 
         ///< Set smmothing and method. Allows are gnuplot allowed =csplines, bezier, sbezier,acsplines, unique
    
       void MakeGnu2DPlot(const string& aFilename);   ///< Make gnuplot command file and data file.
       
       void MakeGnu2DPlot(const string& aFilename, Plot2D** aAddPlots, const In anAddPlots);
       ///< Same as MakeGnuPlot(), but create plot data, labels and analytical functions
       ///< from additional plots pointed to by aAddPlots.
       
       void MakePsFile(const string& aFilename);    ///< Plot spectrum to Postscript file.
       void MakePsFile(const string& aFilename, Plot2D** aAddPlots, const In anAddPlots);
};

// Utility functions.

void MakeMulti2DPlotPs(const string& aFilename, const string& aXlabel, const string& aYlabel,
                     const string& aZlabel,string* aTitles,
                     MidasVector* aXvals, MidasVector* aYvals, MidasVector* aZvals,
                     In anSets, bool aXlog = false, bool aYlog = false, bool aZlog = false,
                     Nb aScaleX = C_1, Nb aScaleY = C_1, Nb aScaleZ = C_1);

void MakeMulti2DPlotPsAutoX(const string& aFilename, const string& aXlabel, const string& aYlabel,
                          const string& aZlabel, string* aTitles,
                          In aFirstX, In aFirstY, MidasVector* aZvals,
                          In aNsets, bool aXlog, bool aYlog, bool aZlog,
                          Nb aScaleX, Nb aScaleY, Nb aScaleZ);


#endif // PLOT2D_H

