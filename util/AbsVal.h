#ifndef ABSVAL_H_INCLUDED
#define ABSVAL_H_INCLUDED

#include <cstdlib>  // for std::abs
#include <cmath>    // for std::fabs
#include <complex>  // for std::complex

namespace midas
{
namespace util
{

/*******************************************************************
 *
 * Integer overloads
 *
 *******************************************************************/
inline int           AbsVal(int           i) { return std::abs(i); }
inline long int      AbsVal(long int      i) { return std::abs(i); }
inline long long int AbsVal(long long int i) { return std::abs(i); }

/*******************************************************************
 *
 * Floating point overloads
 *
 *******************************************************************/
inline float       AbsVal(float       d) { return std::fabs(d); }
inline double      AbsVal(double      d) { return std::fabs(d); }
inline long double AbsVal(long double d) { return std::fabs(d); }
inline float       AbsVal2(float       d) { return d*d; }
inline double      AbsVal2(double      d) { return d*d; }
inline long double AbsVal2(long double d) { return d*d; }

/*******************************************************************
 *
 * Complex overloads
 *
 *******************************************************************/
template<class T>
inline T AbsVal2(const std::complex<T>& c) { return c.real()*c.real() + c.imag()*c.imag(); }
template<class T>
inline T AbsVal(const std::complex<T>& c) { return std::sqrt(AbsVal2(c)); }

/*******************************************************************
 *
 * Return type of AbsVal function for type T
 *
 *******************************************************************/
template<class T>
using AbsVal_t = decltype(AbsVal(T()));

} /* namespace util */
} /* namespace midas */

#endif /* ABSVAL_H_INCLUDED */
