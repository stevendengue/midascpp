#ifndef MIDAS_UTIL_FLAT_MAP_HPP_INCLUDED
#define MIDAS_UTIL_FLAT_MAP_HPP_INCLUDED

#include <iostream>

#include <utility>
#include <type_traits>

#include "util/Error.h"

namespace midas
{
namespace util
{

namespace detail
{

//! Get nth type of a variadic pack
template<std::size_t N, typename T, typename... types>
struct get_nth_type
{
    using type = typename get_nth_type<N - 1, types...>::type;
};

//! Specialization of get nth type, to stop recursion
template<typename T, typename... types>
struct get_nth_type<0, T, types...>
{
    using type = T;
};

//! Check a variadic pack contains the same type for all entries
template<typename Target, typename... Ts>
using are_same_type = std::conjunction<std::is_same<Ts, Target>... >;

//! Pack of two values
template<class T, class U>
struct pack
{
   T&& t;
   U&& u;
};

} /* namespace detail */

/**
 * Static flat map for conversion between two values.
 *
 * Can convert from value1 to value2 and from value2 to value1.
 **/
template<class T, class U, int Size>
struct FlatMap
{
   using value_type_1 = T;
   using value_type_2 = U;
   static constexpr int size = Size;

   value_type_1 value1[size];
   value_type_2 value2[size];


   /**
    * Get index of value.
    *
    * @param value   Either mValue1 or mValue2
    * @param v       The value to look for.
    *
    * @return    Return the index of 'v'.
    **/
   template<class ValueType>
   int GetIndex(const ValueType value[size], const ValueType& v)
   {
      for(int i = 0; i < size; ++i)
      {
         if(value[i] == v)
         {
            return i;
         }
      }
      return -1;
   }
   
   /**
    * Convert value1 to pointer to value2.
    *
    * @param v1    Value1 to look for.
    *
    * @return    Returns pointer to value2 corresponding to value1.
    *            If value1 is not found, returns 'nullptr'.
    **/
   value_type_2* Value1ToValue2(const value_type_1& v1)
   {
      auto i = GetIndex(value1, v1);

      if(i != -1)
      {
         return &(value2[i]);
      }
      
      return nullptr;
   }
   
   /**
    * Convert value2 to pointer to value1.
    *
    * @param v2    Value2 to look for.
    *
    * @return    Returns pointer to value1 corresponding to value2.
    *            If value2 is not found, returns 'nullptr'.
    **/
   value_type_1* Value2ToValue1(const value_type_2& v2)
   {
      auto i = GetIndex(value2, v2);

      if(i != -1)
      {
         return &(value1[i]);
      }
      
      return nullptr;
   }
   
   /**
    * Check that no values overlap.
    *
    * @return   Returns true if no values overlap, otherwise false.
    **/
   bool Check()
   {
      for(int i = 0; i < size; ++i)
      {
         for(int j = i + 1; j < size; ++j)
         {
            if (  value1[i] == value1[j])
            {
               return false;        
            }
            if (  value2[i] == value2[j])
            {
               return false;        
            }
         }
      }
      return true;
   }
}; 

/**
 * Create a pack of two values to pass to FlatMap.
 *
 * @param t   Value1
 * @param u   Value2
 *
 * @return    Returns pack of t and u to pass to FlatMap.
 **/
template<class T, class U>
auto CreatePack(T&& t, U&& u)
{
   return detail::pack<T, U>{std::forward<T>(t), std::forward<U>(u)};
}

/**
 * Create a FlatMap from a set of packed values.
 *
 * @param p   Variadic pack of packed values.
 *
 * @return    Returns FlatMap of packed values.
 **/
template<class... Ts, class... Us>
auto CreateFlatMap
   (  detail::pack<Ts, Us>&&... p
   )
{
   constexpr int size = sizeof...(p);

   using T = typename detail::get_nth_type<0, Ts...>::type;
   using U = typename detail::get_nth_type<0, Us...>::type;

   static_assert(detail::are_same_type<T, Ts...>::value, "Ts are not identical.");
   static_assert(detail::are_same_type<U, Us...>::value, "Us are not identical.");
    
   FlatMap<T, U, size> map{ { std::forward<T>(p.t)... }, { std::forward<U>(p.u)... } };

   if(!map.Check())
   {
      MIDASERROR("ERROR: Identical entries in FlatMap.");
   }
   
   return map;
}

} /* namespace util */
} /* namespace midas */

#endif /* MIDAS_UTIL_FLAT_MAP_HPP_INCLUDED */
