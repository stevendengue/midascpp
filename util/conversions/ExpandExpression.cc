/**
************************************************************************
* 
* @file                ExpandExpression.cc
*
* Created:             16-01-2014
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description:     Class for handing the EXPAND(arg1;arg2;arg3) statement
* 
* Last modified: Thu Apr 11, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
#include <string>
#include <vector>
#include <map>
#include <memory>

#include "util/conversions/BaseExpression.h"
#include "util/conversions/ExpandExpression.h"
#include "util/conversions/VectorFromString.h"
#include "util/Io.h"
namespace midas
{
namespace util
{

std::vector<std::string> ExpandExpression::Apply(const std::vector<std::vector<std::string> >& aArgs, char aSeperator) const
{
   //Mout << "Calling Expand with argument : " << aArgs << " and Seperator : \'" << aSeperator << "\'"<< endl;
   if(aArgs.size() < 3)
   {
      MIDASERROR("Too few arguments for EXPAND()");
   }

   auto it_string_to_expand = aArgs.begin();
   auto it_variables = it_string_to_expand + 1;
   //auto it_val_lists = it_string_to_expand + 2;

   bool add = false; //We only have one list of variables
   if(aArgs.size() > 3)
   {
      if(aArgs.size() != it_variables->size() + 2)
      {
         Mout << "Calling Expand with argument : " << aArgs << " and Seperator : \'" << aSeperator << "\'"<< endl;
         MIDASERROR("Something wrong with the number of variables in ExpandExpression");
      }
      add = true;
   }
   return Recursion(aArgs.front(), aArgs.begin()+2, add, (aArgs.begin()+1)->begin(), (aArgs.begin()+1)->end());
}

std::vector<std::string> ExpandExpression::Recursion(const std::vector<std::string>& arVect, 
                                                     std::vector<std::vector<std::string> >::const_iterator arValList, bool aAdd, 
                                                     std::vector<std::string>::const_iterator arCurrPoss, 
                                                     std::vector<std::string>::const_iterator arEndPoss) const
{
   if(arCurrPoss == arEndPoss)
   {
      return std::vector<std::string>(arVect);
   }

   std::vector<std::string> ret;
   std::vector<std::string> to_ret;
   if(aAdd)
   {
      ret = Recursion(arVect, arValList+1, aAdd, arCurrPoss+1, arEndPoss);
   }
   else
   {
      ret = Recursion(arVect, arValList, aAdd, arCurrPoss+1, arEndPoss);
   }

   for(auto it = ret.begin(); it != ret.end(); ++it)
   {
      for(auto it_vals = arValList->begin(); it_vals != arValList->end(); ++it_vals)
      {
         std::string for_work(*it);
         size_t poss;
         while(std::string::npos != (poss = for_work.find(*arCurrPoss)))
         {
            for_work.replace(poss, (*arCurrPoss).size(), *it_vals);
         }
         to_ret.emplace_back(for_work);
      }
   }
   return to_ret;
}

} /*namespace util*/
} /*namespace midas*/
