#include "HeaderInserter.h"

namespace midas
{
namespace stream
{

/**
 * Overide the overflow() function of streambuf, with out own function that inserts
 * the requested header if we are at the start of a new line.
 * If we are at the end of a line, we prepare myIsAtStartOfLine to output header at next write.
 *
 * @param ch   The character we are currently trying to write.
 *
 * @return     Return the returned value from sputc().
 **/
int HeaderInserter::overflow
   (  int ch 
   )
{
   int retval = 0;
   if ( ch != traits_type::eof() ) 
   {
      if ( myIsAtStartOfLine ) 
      {
         myDest->sputn( myHeader.data(), myHeader.size() );
      }
      retval = myDest->sputc( ch );
      myIsAtStartOfLine = ch == '\n';
   }
   return retval;
}

/**
 * Constructor for HeaderInserter that inserts a number of spaces.
 *
 * @param dest   Underlying streambuf.
 * @param blank  Number of spaces to put as header.
 * @param start  Is first write after a newline?
 **/
HeaderInserter::HeaderInserter
   (  std::streambuf* dest
   ,  size_t blank
   ,  bool start
   )
   :  myDest( dest )
   ,  myHeader(blank,' ')
   ,  myIsAtStartOfLine( start )
{
}

/**
 * Constructor for HeaderInserter that inserts a given header.
 *
 * @param dest   Underlying streambuf.
 * @param header The header to insert, given as a std::string.
 * @param start  Is first write after a newline?
 **/
HeaderInserter::HeaderInserter
   (  std::streambuf* dest
   ,  const std::string& header
   ,  bool start
   )
   :  myDest( dest )
   ,  myHeader(header)
   ,  myIsAtStartOfLine( start )
{
}

/**
 * Skip header even if were are at the start of a new line.
 **/
void HeaderInserter::SkipHeader
   (
   )
{
   myIsAtStartOfLine = false;
}

} /* namespace stream */
} /* namespace midas */
