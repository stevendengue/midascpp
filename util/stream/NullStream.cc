#include "NullStream.h"

// in a concrete .cpp
NullBuf null_buf_obj;
WNullBuf wnull_buf_obj;

std::ostream Mnull(&null_buf_obj);
std::wostream wMnull(&wnull_buf_obj);
