#ifndef INDENTIONSTREAM_H_INCLUDED
#define INDENTIONSTREAM_H_INCLUDED

#include <iostream>

#include "util/stream/HeaderInserter.h"

namespace midas
{
namespace stream
{

/**
 * IndentionStream
 **/
class IndentionStream
   : public std::ostream
{
   private:
      HeaderInserter mFileBuf;

   public:
      IndentionStream(std::ostream& os, const std::string& str, bool start = false)
         : std::ostream(&mFileBuf)
         , mFileBuf(os.rdbuf(),str,start)
      {
      }
      
      IndentionStream(std::ostream& os, size_t blanks = 0, bool start = false)
         : std::ostream(&mFileBuf)
         , mFileBuf(os.rdbuf(),blanks,start)
      {
      }

      void SkipHeader()
      {
         mFileBuf.SkipHeader();
      }

      void IndentPush()
      {
         mFileBuf.Push();
      }
      
      void IndentPop()
      {
         mFileBuf.Pop();
      }
};

struct IndentPush
{
   template<class S>
   void operator()(S& s)
   {
      s.IndentPush();
   }
};

struct IndentPop
{
   template<class S>
   void operator()(S& s)
   {
      s.IndentPop();
   }
};

inline IndentionStream& operator<<(IndentionStream& os, skipheader sh)
{
   sh(os);
   return os;
}

inline IndentionStream& operator<<(IndentionStream& os, IndentPush sh)
{
   sh(os);
   return os;
}

inline IndentionStream& operator<<(IndentionStream& os, IndentPop sh)
{
   sh(os);
   return os;
}

} /* namespace stream */
} /* namespace midas */

#endif /* INDENTIONSTREAM_H_INCLUDED */
