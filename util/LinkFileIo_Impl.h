/**
************************************************************************
* 
* @file                LinkFileIo_Impl.h
*
* Created:             04-10-2002
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implementing link to direct file io
* 
* Last modified: man mar 21, 2005  11:41
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
#include<fstream>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/Os.h"
#include "util/FileSystem.h"

#include "mpi/Impi.h"

// using declarations
using std::vector;
using std::string;
using std::transform;

/* 
 * Use C routines in DirFileIo.c 
 * Simulate the cray 64 bit word addressable I/O routines and
   allow for large buffering in core.

void WOPEN(unit, name, lennam, blocks, stats, ierr)
     int *unit, *lennam, *blocks, *stats, *ierr;
     char *name;

void WCLOSE(unit, ierr)
     int *unit, *ierr;

void GETWA(unit, result, addr, count, ierr)
     int *unit, *addr, *count, *ierr;
     double *result;

void GETWA2(unit, result, addr, count, ierr, nbytes)
     int *unit, *addr, *count, *ierr, *nbytes;
     double *result;

void PUTWA(unit, source, addr, count, ierr)
     int *unit, *addr, *count, *ierr;
     double *source;

void PUTWA2(unit, source, addr, count, ierr, nbytes)
     int *unit, *addr, *count, *ierr, *nbytes;
     double *source;

Currently the I/O is syncronous and unbuffered 
*
*
* Follow C linkage:
*
*/
extern "C" void WOPEN(int*, char*, int*, int*, int*, int*);
extern "C" void WCLOSE(int*, int*);
extern "C" void PUTWA(int*,double*,int*,int*,int*);
extern "C" void GETWA(int*,double*,int*,int*,int*);
extern "C" void PUTWA2(int*,char*,int*,int*,int*,int*);
extern "C" void GETWA2(int*,char*,int*,int*,int*,int*);

extern In gIoLevel;
extern vector<bool> gUnitNumbers;

/**
 * File opener function
 **/
static void FileOpen
   (  int& aUnit
   ,  const string& aName
   )
{
   int unit   = aUnit;

   if (unit == 0 ) 
   {
      int new_unit = 1;
      while (gUnitNumbers[new_unit] && new_unit <= MAX_FILE-1) new_unit++;
      unit = new_unit;
   } 
   if (unit < 0 || unit >= MAX_FILE)  
   {
      Mout << " Error in file unit number " << endl;
      Error (" Error in unit number in FileOpen "); 
   }
   if (gUnitNumbers[unit]) 
   {
      Mout << " Unit number occupied " << endl;
      Error (" Error in unit - number provided to FileOpen is occupied"); 
   }
   //if (gDebug) Mout << " Unit number assigned to " << aName << " is " << unit << endl;
   gUnitNumbers[unit] = true;


   int blocks = 64;                             // NB FOR double !!!!!  Not general.
   int stats  = 0;
   //if (gDebug) stats = 1;                      // If debug write out file information.
   int i_err  = 0;
   int l_name = aName.size();
   char* name = new char [l_name];
   aName.copy(name,l_name);

   WOPEN(&unit, name, &l_name, &blocks, &stats, &i_err);
   delete[] name;
   aUnit  = unit;

   if (i_err!= 0)
   {
      Mout << endl << " File Open error:" << endl;
      Mout << " aUnit  = " << aUnit << endl;
      Mout << " unit   = " << unit << endl;
      Mout << " aName  = " << aName << endl;
      Mout << " name   = " << name << endl;
      Mout << " l_name = " << l_name << endl;
      Mout << " blocks = " << blocks << endl;
      Mout << " stats  = " << stats << endl;
      Mout << " i_err  = " << i_err << endl; 
      MIDASERROR("Fatal error in FileOpen/WOPEN");
   }
}

/**
 * File closer function
 **/
static void FileClose(int aUnit)
{
   gUnitNumbers[aUnit] = false;
   int i_err=0;
   WCLOSE(&aUnit, &i_err);
   if (i_err!= 0)
   {
      Mout << endl << " File close error:" << endl;
      Mout << " aUnit  = " << aUnit << endl;
      Mout << " i_err  = " << i_err << endl; 
      MIDASERROR("Fatal error in FileClose/WCLOSE");
   }
}

namespace midas::util::detail
{
/**
* Io a vector from a file 
* NB if aIerr on input = -1 then the routine let 
* an error pass by and let the program continue.
* On return aIerr = i_err;
*
* @note
*     The MV template parameter should be a GeneralMidasVector<typename>. It is
*     used here to match both a const and a nonconst GeneralMidasVector, for
*     the purpose of making this function const correct. -MBH, July 2018.
*
* @note
*     For IO_PUT the MV should be 'const GeneralMidasVector'. In that case the
*     functions PUTWA or PUTWA2 will be called, taking a pointer to the data of
*     aData. However, these functions, being written in C, cannot take pointers
*     to const data, so we'll have to const_cast the aData at that place. Other
*     than that this function should be const correct. -MBH, July 2018.
*
* @note
*     The out-commented code with MPI stuff was there already when I made the
*     const-correct changes. I haven't updated that code according to the
*     const-correct changes. -MBH, July 2018.
* */
template
   <  class MV
   ,  int N
   ,  std::enable_if_t
      <  std::is_same_v
         <  std::remove_const_t<MV>
         ,  GeneralMidasVector<typename MV::value_type>
         >
      ,  int
      >
   >       
void FileIoImpl
   (  const std::string& aFileName
   ,  MV& aData
   ,  const In aAddr
   ,  const In aLength
   ,  const IoType<N>& aPutGet
   ,  In& aIerr
   ,  In aVecOff
   )
{
   // constexpr_io_type is a local constexpr equivalent of aPutGet (since it's
   // defined only in terms of the template parameter N).
   constexpr IoType<N> constexpr_io_type;

   if constexpr(bool(constexpr_io_type & IO_GET))
   {
      static_assert(!std::is_const_v<MV>, "Cannot use IO_GET with const MidasVector.");
   }
   else if constexpr(bool(constexpr_io_type & IO_PUT))
   {
      static_assert(std::is_const_v<MV>, "MidasVector should be const for IO_PUT.");
   }
   else
   {
      static_assert  (  !((aPutGet & IO_GET) || (aPutGet & IO_PUT))
                     ,  "One of IO_PUT and IO_GET must be set!"
                     );
   }

   int unit=0;
   
   //auto filename = aFileName + "_rank_" + std::to_string(midas::mpi::GlobalRank());
   auto filename = aFileName;

   //midas::mpi::WriteToLog("Doing '" + ToString(constexpr_io_type) + "' on file : '" + filename + "'.");
//#ifdef VAR_MPI
//   if(I_0 == get_mpi_comm_manager().GetMpiRank())
//   {
//      FileOpen(unit,aFileName);
//   }
//#else
   FileOpen(unit, filename);
   
//#endif
   int i_err  = 0;
   int addr   = aAddr;
   int length = aLength;
   decltype(aData.data()) data2 = (aData.data()+aVecOff);              

   int nbytes = sizeof(data2[0]);
   double test;
   int nbytdoub = sizeof(test);
   char test2;
   int nbytchar = sizeof(test2);
   bool double_io = (nbytes == nbytdoub);
   //if (gDebug && double_io) Mout << " using standard getwa/putwa " << endl;
   //if (gDebug && !double_io) Mout << " using generalized getwa/putwa " << endl;
   int ncharprtype = nbytes/nbytchar;
   //if (gDebug) Mout  << " nbytes = " << nbytes << " nbytdoub = " << nbytdoub 
      //<< " nbytchar = " << nbytchar << " ncharprtype " << ncharprtype << endl;

   // Now finally do the actual IO.
//#ifdef VAR_MPI
//   if (aPutGet & IO_GET && double_io) 
//   {
//      if(I_0 == get_mpi_comm_manager().GetMpiRank()) 
//      {
//         GETWA(&unit, static_cast<double*>(data), &addr, &length, &i_err);
//      }
//      if(gDebug)
//         cout << "Rank " << gMpiRank << " before MPI_Bcast..." << endl;
//      MPI_Bcast(&i_err,1,MPI_INT,0,get_mpi_comm_manager().GetCommunicator());
//      MPI_Bcast(data2,length,MPI_DOUBLE,0,get_mpi_comm_manager().GetCommunicator());
//   }
//   if (aPutGet & IO_GET && !double_io) 
//   {
//      if(I_0 == get_mpi_comm_manager().GetMpiRank()) 
//      {  
//         GETWA2(&unit, static_cast<char*>(data), &addr, &length, &i_err,&ncharprtype);
//      }
//      MPI_Bcast(data2,length,MPI_CHAR,0,get_mpi_comm_manager().GetCommunicator());
//      MPI_Bcast(&i_err,1,MPI_INT,0,get_mpi_comm_manager().GetCommunicator());
//   }
//   if(I_0 == get_mpi_comm_manager().GetMpiRank()) 
//   {
//      //printf("Before put (rank %i), address is: %p\n",gMpiRank,data);
//      if (aPutGet & IO_PUT && double_io) 
//      {
//         PUTWA(&unit, static_cast<double*>(data), &addr, &length, &i_err);
//      }
//      if (aPutGet & IO_PUT && !double_io)
//      {
//         PUTWA2(&unit, static_cast<char*>(data), &addr, &length, &i_err,&ncharprtype);
//      }
//   }
//   if(gDebug)
//      printf("Rank %i at Barrier in linkFIleIo\n",gMpiRank);
//#else
   if constexpr(bool(constexpr_io_type & IO_GET))
   {
      void* data = static_cast<void*>(aData.data()+aVecOff);              
      if (double_io)
      {
         GETWA(&unit, static_cast<double*>(data), &addr, &length, &i_err);
      }  
      else
      {
         GETWA2(&unit, static_cast<char*>(data), &addr, &length, &i_err,&ncharprtype);
      }
   }
   else if constexpr (bool(constexpr_io_type & IO_PUT))
   {
      // If putting, aData should be of type MV = 'const
      // GeneralMidasVector<T>'. Since we're about to call a C function
      // (PUTWA/PUTWA2) we can't preserve const'ness of the data pointer, so
      // we'll const_cast to nonconst, and HOPE that the PUTWA/PUTWA2 don't
      // modify the data pointed to.
      auto& nonconst_aData = const_cast<std::remove_const_t<MV>&>(aData);
      void* data = static_cast<void*>(nonconst_aData.data()+aVecOff);              
      if (double_io)
      {
         PUTWA(&unit, static_cast<double*>(data), &addr, &length, &i_err);
      }
      else
      {
         PUTWA2(&unit, static_cast<char*>(data), &addr, &length, &i_err,&ncharprtype);
      }
   }
   else
   {
      static_assert  (  !((aPutGet & IO_GET) || (aPutGet & IO_PUT))
                     ,  "One of IO_PUT and IO_GET must be set!"
                     );
   }
//#endif

   bool loc_debug = gDebug && gIoLevel > I_14;
   if (loc_debug)
   {
      if constexpr(bool(constexpr_io_type & IO_GET))
      {
         Mout << " Now the data has been getted" << std::endl;
      }
      if constexpr(bool(constexpr_io_type & IO_PUT))
      {
         Mout << " Now the data has been putted" << std::endl;
      }
      Mout << " data read/written is :" << std::endl;
      for (int i = 0; i < length; i++)
      {
         Mout << data2[i] << std::endl;
      }
      Mout << std::endl;
   }

   if (i_err != I_0)
   {
      // mbh: Avoid these massive printouts in production runs
      if (aIerr != -I_1 || gDebug) 
      {
         Mout << endl << " File Read/Write error:" << endl;
         Mout << " filename = " << filename << endl;
         Mout << " unit     = " << unit << endl;
         Mout << " addr     = " << addr << endl;
         Mout << " length   = " << length << endl;
         Mout << " aPutGet  = " << aPutGet << endl; 
         Mout << " i_err    = " << i_err << endl; 
      }
      if (aIerr != -I_1) 
      {
         MIDASERROR
            (  "Fatal error in FileIo. Filename = " + filename 
            +  "   with aPutGet = " + std::to_string(aPutGet) 
            +  ".  (IO_PUT = " + std::to_string(IO_PUT) 
            +  "   IO_GET = " + std::to_string(IO_GET) + ")."
            );
      }
   }
//#ifdef VAR_MPI
//   if(I_0 == get_mpi_comm_manager().GetMpiRank())
//      FileClose(unit);
//#else
   FileClose(unit);
//#endif
   aIerr = i_err;
}
} /* namespace midas::util::detail */

namespace midas::util::detail
{
/**
* Io a single Nb from a file.
*
* @note
*     See notes at the vectorized FileIoImpl (with the substitution that T her
*     is supposed to match a const/nonconst scalar type). -MBH, July 2018.
**/
template<class T, int N>
void FileIoImpl
   (  const std::string& aFileName
   ,  T& aData
   ,  const In aAddr
   ,  const IoType<N>& aPutGet
   ,  In& aIerr
   )
{
   // constexpr_io_type is a local constexpr equivalent of aPutGet (since it's
   // defined only in terms of the template parameter N).
   constexpr IoType<N> constexpr_io_type;

   if constexpr(bool(constexpr_io_type & IO_GET))
   {
      static_assert(!std::is_const_v<T>, "Cannot use IO_GET with const T.");
   }
   else if constexpr(bool(constexpr_io_type & IO_PUT))
   {
      static_assert(std::is_const_v<T>, "T should be const for IO_PUT.");
   }
   else
   {
      static_assert  (  !((aPutGet & IO_GET) || (aPutGet & IO_PUT))
                     ,  "One of IO_PUT and IO_GET must be set!"
                     );
   }

   int unit=0;
   //auto filename = aFileName + "_rank_" + std::to_string(midas::mpi::GlobalRank());
   auto filename = aFileName;
   //midas::mpi::WriteToLog("Doing '" + ToString(constexpr_io_type) + "' on file : '" + filename + "'.");
//#ifdef VAR_MPI
//   //MPI_Barrier(get_mpi_comm_manager().GetCommunicator());
//   if(I_0 == get_mpi_comm_manager().GetMpiRank())
//      FileOpen(unit, aFileName);
//#else 
   FileOpen(unit, filename);
//#endif
   int i_err  = 0;
   int addr   = aAddr;
   int length = 1;
   T* data2  = &aData;

   int nbytes = sizeof(*data2);
   double test;
   int nbytdoub = sizeof(test);
   char test2;
   int nbytchar = sizeof(test2);
   bool double_io = (nbytes == nbytdoub);
   //if (gDebug && double_io) Mout << " using standard getwa/putwa " << endl;
   //if (gDebug && !double_io) Mout << " using generalized getwa/putwa " << endl;
   int ncharprtype = nbytes/nbytchar;
   //if (gDebug) Mout  << " nbytes = " << nbytes << " nbytdoub = " << nbytdoub 
      //<< " nbytchar = " << nbytchar << " ncharprtype " << ncharprtype << endl;

   // Now finally do the actual IO.
//#ifdef VAR_MPI
//   // if IO_GET, then update all other ranks
//   if (aPutGet & IO_GET && double_io) 
//   {
//      if(I_0 == get_mpi_comm_manager().GetMpiRank()) 
//      { 
//         GETWA(&unit, static_cast<double*>(data), &addr, &length, &i_err);
//      }
//      MPI_Bcast(data2,length,MPI_DOUBLE,0,get_mpi_comm_manager().GetCommunicator());
//      MPI_Bcast(&i_err,1,MPI_INT,0,get_mpi_comm_manager().GetCommunicator());
//   }
//   if (aPutGet & IO_GET && !double_io) 
//   {
//      if(I_0 == get_mpi_comm_manager().GetMpiRank()) 
//      {  
//         GETWA2(&unit, static_cast<char*>(data), &addr, &length, &i_err,&ncharprtype);
//      }
//      MPI_Bcast(data2,length,MPI_CHAR,0,get_mpi_comm_manager().GetCommunicator());
//      MPI_Bcast(&i_err,1,MPI_INT,0,get_mpi_comm_manager().GetCommunicator());
//   }
//   if(I_0 == get_mpi_comm_manager().GetMpiRank()) 
//   {
//      if (aPutGet & IO_PUT && double_io)
//         PUTWA(&unit, static_cast<double*>(data), &addr, &length, &i_err);
//      if (aPutGet & IO_PUT && !double_io)
//         PUTWA2(&unit, static_cast<char*>(data), &addr, &length, &i_err,&ncharprtype);
//   }
//   //MPI_Barrier(get_mpi_comm_manager().GetCommunicator());
//#else
   if constexpr(bool(constexpr_io_type & IO_GET))
   {
      void* data = static_cast<void*>(&aData); 
      if (double_io)
      {
         GETWA(&unit, static_cast<double*>(data), &addr, &length, &i_err);
      }
      else
      {
         GETWA2(&unit, static_cast<char*>(data), &addr, &length, &i_err, &ncharprtype);
      }
   }
   else if constexpr(bool(constexpr_io_type & IO_PUT))
   {
      // If putting, aData should be of const type Since we're about to call a
      // C function (PUTWA/PUTWA2) we can't preserve const'ness of the data
      // pointer, so we'll const_cast to nonconst, and HOPE that the
      // PUTWA/PUTWA2 don't modify the data pointed to.
      auto& nonconst_aData = const_cast<std::remove_const_t<T>&>(aData);
      void* data = static_cast<void*>(&nonconst_aData); 
      if (double_io)
      {
         PUTWA(&unit, static_cast<double*>(data), &addr, &length, &i_err);
      }
      else
      {
         PUTWA2(&unit, static_cast<char*>(data), &addr, &length, &i_err, &ncharprtype);
      }
   }
   else
   {
      static_assert  (  !((aPutGet & IO_GET) || (aPutGet & IO_PUT))
                     ,  "One of IO_PUT and IO_GET must be set!"
                     );
   }
//#endif

   bool loc_debug = gDebug && gIoLevel > I_14;
   if (loc_debug)
   {
      if constexpr(bool(constexpr_io_type & IO_GET))
      {
         Mout << " Now the data has been getted" << std::endl;
      }
      if constexpr(bool(constexpr_io_type & IO_PUT))
      {
         Mout << " Now the data has been putted" << std::endl;
      }
      Mout << " data read/written is :" << std::endl;
      for (int i = I_0; i < length; i++)
      {
         Mout << " " << data2[i];
      }
      Mout << std::endl;
   }

   if (i_err != I_0)
   {
      Mout << endl << " File Read/Write error:" << endl;
      Mout << " filename = " << filename << endl;
      Mout << " unit     = " << unit << endl;
      Mout << " addr     = " << addr << endl;
      Mout << " length   = " << length << endl;
      Mout << " aPutGet  = " << aPutGet << endl; 
      Mout << " i_err    = " << i_err << endl; 
      if (aIerr != -I_1)
      {
         MIDASERROR( "Fatal error in FileIo. Filename = " + filename + "   with aPutGet = " + std::to_string(aPutGet) + ".  (IO_PUT = " + std::to_string(IO_PUT) + "   IO_GET = " + std::to_string(IO_GET) + ").");
      }
   }
//#ifdef VAR_MPI
//   if(I_0 == get_mpi_comm_manager().GetMpiRank())
//      FileClose(unit);
//   //MPI_Barrier(get_mpi_comm_manager().GetCommunicator());
//#else
   FileClose(unit);
//#endif
   aIerr = i_err;
}
} /* namespace midas::util::detail */
