/**
************************************************************************
* 
* @file                MidasStream.cc
*
* Created:             18-03-2011
*
* Author:              Mikkel B. Hansen(mbh@chem.au.dk)
*
* Short Description:   Tools for displaying fractions.
* 
* Last modified: Fri May 01, 2009  12:57PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// std headers
#include <iostream>
#include <iomanip>
#include <sstream>
#include <streambuf>
#include <string>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/MidasStream.h"
#include "mpi/Impi.h"

/**
 * It should be adequate to provide our own sync method
 * for the derived buffer type, right?
 *
 * 1) Output the buffer
 * 2) Reset the buffer
 * 3) flush output stream
 **/
int MidasStreamBuf::sync 
   ( 
   )
{
   ///< following two lines for avoiding
   ///< multiple output in Midas.out file
   //output << "In MidasStream, gMpiRank is: " << gMpiRank << endl;
   if(!mMute && (!mOnlyMaster || midas::mpi::GlobalRank() == 0)) 
   {
      if (!mOnlyMaster)
      {
         output << midas::mpi::CreateMessageHeader();
      }
      output << str();
      output.flush();
   }

   str("");
   return 0;
}

/**
 * Constructor.
 *
 * @param   aBuf    The stream buffer.
 **/
MidasStream::MidasStream
   (  MidasStreamBuf& aBuf
   )
   :  std::ostream(&aBuf)
   ,  mStreamBuf(aBuf)
{
   // set scientific notation and precision
   *this << std::scientific << std::setprecision(16) << std::boolalpha;
}
