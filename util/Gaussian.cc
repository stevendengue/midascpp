/**
************************************************************************
* 
* @file                Gaussian.cc
*
* Created:             20-02-2004
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Implement Gaussian class members 
* 
* Last modified: man mar 21, 2005  11:41
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/
// std headers
#include <vector> 

// midas headers
#include "inc_gen/math_link.h" 
#include "inc_gen/TypeDefs.h" 
#include "inc_gen/Const.h" 
#include "util/Gaussian.h" 

// using declarations
using std::vector;

/**
* Sum of values for a vector of Gaussians 
* */
Nb AveOfValsForVectorOfGaussians(vector<Gaussian>& arVecOfGauss,Nb aXval)
{
   In n_gauss = arVecOfGauss.size();
   Nb val = C_0;
   for (In i=I_0;i<n_gauss;i++)
   {
      val += arVecOfGauss[i].ValueFor(aXval);
   }
   return val/n_gauss;
}
