/**
 *******************************************************************************
 * 
 * @file    OperMat_Decl.h
 * @date    23-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef OPERMAT_DECL_H_INCLUDED
#define OPERMAT_DECL_H_INCLUDED

// Standard headers.
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "mmv/MidasMatrix.h"
#include "mpi/Impi.h"

// Forward declarations.
class OpDef;
template<typename> class ModalIntegrals;
template<typename> class GeneralMidasVector;

namespace midas::util::matrep
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   template<typename T>
   class OperMat
   {
      public:
         using n_modals_t = std::vector<Uin>;
         using opdef_t = OpDef;
         using modalintegrals_t = ModalIntegrals<T>;
         using vec_t = GeneralMidasVector<T>;

         enum class SymType
         {  HERMITIAN
         ,  ANTIHERMITIAN
         ,  GENERAL
         };

         //@{
         //! Default constructors. Assignments deleted.
         OperMat(const OperMat&) = delete;
         OperMat(OperMat&&) = default;
         OperMat& operator=(const OperMat&) = delete;
         OperMat& operator=(OperMat&&) = delete;
         ~OperMat();
         //@}

         //! Constructor sets up everything.
         OperMat
            (  const n_modals_t& arNModals
            ,  const opdef_t& arOpDef
            ,  const modalintegrals_t& arModInts
            ,  bool aDumpToDiskAtDestructor = false
            );

         /******************************************************************//**
          * @name Queries
          *********************************************************************/
         //!@{
         SymType GetSymType() const {return mSymType;}
         Uin FullDim() const {return mFullDim;}
         Uin Nrows() const {return FullDim();}
         Uin Ncols() const {return FullDim();}
         bool IsSquare() const {return true;}

         //! If dumping to disk, this is the file name (base); append _0 according to DataCont.
         const std::string& DumpFileName() const {return mDumpFileName;}
         //!@}

         //! Whether desctructor should dump stored matrix to disk.
         void SetDumpToDisk(bool aArg) const {mDumpToDisk = aArg;}

         /******************************************************************//**
          * @name Transforms
          *********************************************************************/
         //!@{
         template<bool TRANSPOSE = false>
         vec_t MatVecMult(const vec_t&) const;

         vec_t GetRow(Uin i) const;
         //!@}

      private:
         using mat_t = std::unique_ptr<T[]>;

         const Uin mFullDim = 0;
         const std::vector<Uin> mMpiRankRowIndices;
         const SymType mSymType = SymType::GENERAL;
         const std::string mDumpFileName;
         mutable bool mDumpToDisk = false;
         mat_t mMat;

         Uin RowBeg(Uin aRank = midas::mpi::GlobalRank()) const {return mMpiRankRowIndices.at(aRank);}
         Uin RowEnd(Uin aRank = midas::mpi::GlobalRank()) const {return mMpiRankRowIndices.at(aRank+1);}
         Uin NRowsBlock(Uin aRank = midas::mpi::GlobalRank()) const {return RowEnd(aRank)-RowBeg(aRank);}
         Uin NColsBlock() const {return this->FullDim();}
         Uin BlockSize() const {return this->NRowsBlock() * this->NColsBlock();}

         vec_t WrapGEMV
            (  const vec_t& arVec
            ,  bool aTranspose
            )  const;

         Uin RankWithRow(Uin aRow) const;
         vec_t GetRowLocal(Uin aRow) const;

         std::vector<Uin> CalcMpiRankRowIndices(Uin aMpiRanks) const;

         std::unique_ptr<T[]> ConstructMat
            (  const n_modals_t& arNModals
            ,  const opdef_t& arOpDef
            ,  const modalintegrals_t& arModInts
            )  const;

         static SymType DetermineType(const opdef_t&, const modalintegrals_t&);

         static std::string GenerateDumpFileName
            (  const n_modals_t& arNModals
            ,  const opdef_t& arOpDef
            ,  const modalintegrals_t& arModInts
            ,  Uin aRowBeg
            ,  Uin aRowEnd
            );

         static T CalcSingleElementCtrl
            (  const n_modals_t& arNModals
            ,  const opdef_t& arOpDef
            ,  const modalintegrals_t& arModInts
            ,  Uin aRowI
            ,  Uin aColJ
            );
   };

   //@{
   //! operator* overloads that wrap calls to MatVecMult().
   template<typename T>
   typename OperMat<T>::vec_t operator*(const OperMat<T>& m, const typename OperMat<T>::vec_t& v)
   {
      return m.template MatVecMult<false>(v);
   }

   template<typename T>
   typename OperMat<T>::vec_t operator*(const typename OperMat<T>::vec_t& v, const OperMat<T>& m)
   {
      return m.template MatVecMult<true>(v);
   }
   //@}

} /* namespace midas::util::matrep */

#endif/*OPERMAT_DECL_H_INCLUDED*/
