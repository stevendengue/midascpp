/**
 *******************************************************************************
 * 
 * @file    MatRepUtils_Impl.h
 * @date    12-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MATREPUTILS_IMPL_H_INCLUDED
#define MATREPUTILS_IMPL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "input/ModeCombiOpRange.h"
#include "mmv/MidasVector.h"
#include "mmv/DataCont.h"

namespace midas::util::matrep
{
   /************************************************************************//**
    *
    ***************************************************************************/
   template<typename T>
   std::vector<T> GetElements
      (  const GeneralMidasVector<T>& arCont
      ,  Uin aBegin
      ,  Uin aEnd
      )
   {
      if (aEnd > arCont.Size())
      {
         MIDASERROR("aEnd goes out-of-range.");
      }
      if (aBegin > aEnd)
      {
         MIDASERROR("aBegin > aEnd");
      }
      std::vector<T> res(aEnd - aBegin);
      for(Uin i = 0; aBegin + i < aEnd; ++i)
      {
         res[i] = arCont[aBegin + i];
      }
      return res;
   }

   /************************************************************************//**
    *
    ***************************************************************************/
   template<typename T>
   std::vector<T> GetElements
      (  const GeneralDataCont<T>& arCont
      ,  Uin aBegin
      ,  Uin aEnd
      )
   {
      if (aEnd > arCont.Size())
      {
         MIDASERROR("aEnd goes out-of-range.");
      }
      if (aBegin > aEnd)
      {
         MIDASERROR("aBegin > aEnd");
      }
      GeneralMidasVector<T> mv(aEnd - aBegin);
      arCont.DataIo(IO_GET, mv, mv.Size(), aBegin);
      return std::vector<T>(mv.begin(), mv.end());
   }

   /************************************************************************//**
    * Example:
    *     arDims = {2,2,3}
    *     arMcr  = { {}   , {0}  , {1,2}      }
    *     arVec  = { e000 , e100 , e011, e012 }
    *     return = {{e000},{e100},{e011, e012}}
    *
    * @param[in] arDims
    *    Dimensions of full space.
    * @param[in] arMcr
    *    Range of ModeCombi%s:
    * @param[in] arVec
    *    The vector which contiguously stores elements of the ModeCombi%s of
    *    arMcr. Size must correspond to number of params given by arDims and
    *    arMcr.
    * @return
    *    Container where elements of each ModeCombi have been organized in a
    *    separate vector.
    ***************************************************************************/
   template<typename T, template<typename...> class CONT_T>
   std::vector<std::vector<T>> OrganizeMcrSpaceVec
      (  const std::vector<Uin>& arDims
      ,  const ModeCombiOpRange& arMcr
      ,  const CONT_T<T>& arVec
      )
   {
      if (arMcr.NumberOfModes() > 0)
      {
         if (arMcr.SmallestMode() < 0 || arMcr.LargestMode() >= arDims.size())
         {
            MIDASERROR("arMcr mode out-of-range for arDims.");
         }
      }

      std::vector<std::vector<T>> res;
      res.reserve(arMcr.Size());
      Uin addr = 0;
      for(const auto& mc: arMcr)
      {
         Uin mc_size = NumParams(mc, arDims);
         res.emplace_back(GetElements(arVec, addr, addr + mc_size));
         addr += mc_size;
      }
      if (addr < arVec.Size())
      {
         MIDASERROR("Didn't loop all the way through arVec.");
      }

      return res;
   }

   /************************************************************************//**
    * Does the inverse of OrganizeMcrSpaceVec().
    ***************************************************************************/
   template<typename T, template<typename...> class CONT_T>
   CONT_T<T> McrOrganizedToStackedVec
      (  const std::vector<std::vector<T>>& arVec
      )
   {
      CONT_T<T> res;
      for(const auto& v: arVec)
      {
         res.Insert(res.Size(), v.begin(), v.end());
      }
      return res;
   }

} /* namespace midas::util::matrep */

#endif/*MATREPUTILS_IMPL_H_INCLUDED*/
