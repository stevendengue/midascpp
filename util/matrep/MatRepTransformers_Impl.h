/**
 *******************************************************************************
 * 
 * @file    MatRepTransformers_Impl.h
 * @date    12-02-2019
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MATREPTRANSFORMERS_IMPL_H_INCLUDED
#define MATREPTRANSFORMERS_IMPL_H_INCLUDED

// Standard headers.

// Midas headers.
#include "util/matrep/MatRepVibOper.h"
#include "util/matrep/MatRepUtils.h"
#include "util/matrep/SparseClusterOper.h"
#include "util/matrep/ShiftOperBraketLooper.h"
#include "util/matrep/OperMat.h"
#include "input/ModeCombiOpRange.h"
#include "lapack_interface/GESV.h"

namespace midas::util::matrep
{

/***************************************************************************//**
 * Calculates the braket `<arBra| E^m_rs|arKet>`.
 * Does so efficiently without constructing any operator matrices, using matrep
 * logic to multiply/sum the necessary elements of bra and ket.
 * See MatRepVibOper<T>::ShiftOper() (and friends) for more details.
 *
 * @note
 *    About `CONJ_BRA`:
 *     - If you're passing `|arBra>` to this function, use `CONJ_BRA = true` (this
 *     function is responsible for conjugation).
 *     - If you're passing `<arBra|` to this function, use `CONJ_BRA = false` (this
 *     function shouldn't do conjugation, it's been done already).
 *
 * @param[in] arBra
 *    The bra vector. See note on `CONJ_BRA` above. Size() must equal
 *    Product(arDims).
 * @param[in] arKet
 *    The ket vector. Size() must equal Product(arDims).
 * @param[in] arDims
 *    The dimensions (number of modals) of the each mode of the system.
 * @param[in] arModes
 *    The modes the shift operator works on. (`m,...`)
 * @param[in] arCrea
 *    The creation indices of the shift operator. (`r,...`)
 * @param[in] arAnni
 *    The annihilation indices of the shift operator. (`s,...`)
 * @return
 *    The value of the braket `<arBra| E^m_rs|arKet>`.
 ******************************************************************************/
template<bool CONJ_BRA, typename T>
T ShiftOperBraket
   (  const GeneralMidasVector<T>& arBra
   ,  const GeneralMidasVector<T>& arKet
   ,  const std::vector<Uin>& arDims
   ,  const std::set<Uin>& arModes
   ,  const std::vector<Uin>& arCrea
   ,  const std::vector<Uin>& arAnni
   )
{
   // Assertions.
   auto info = [&arBra, &arKet, &arDims, &arModes, &arCrea, &arAnni](std::string&& s) -> std::string
   {
      std::stringstream ss;
      ss << s << '\n'
         << "   arBra.Size() = " << arBra.Size() << '\n'
         << "   arKet.Size() = " << arKet.Size() << '\n'
         << "   arDims       = " << arDims << '\n'
         << "   arModes      = " << arModes << '\n'
         << "   arCrea       = " << arCrea << '\n'
         << "   arAnni       = " << arAnni << '\n'
         ;
      return ss.str();
   };
   const Uin full_dim = Product(arDims);
   if (arBra.Size() != full_dim || arKet.Size() != full_dim)
   {
      MIDASERROR(info("Size mismatch (arBra, arKet vs. Product(arDims))."));
   }
   if (arModes.size() != arCrea.size() || arModes.size() != arAnni.size())
   {
      MIDASERROR(info("Size mismatch (arModes, arCrea, arAnni)."));
   }
   if (arDims.size() < arModes.size())
   {
      MIDASERROR(info("Size mismatch (arDims.size() < arModes.size())."));
   }
   if (!arModes.empty() && *arModes.rbegin() >= arDims.size())
   {
      MIDASERROR(info("Mode numbers (arModes) go out-of-range w.r.t. arDims."));
   }
   {
      Uin i = 0;
      for(auto it_m = arModes.begin(), end = arModes.end(); it_m != end; ++it_m, ++i)
      {
         if (arCrea.at(i) >= arDims.at(*it_m) || arAnni.at(i) >= arDims.at(*it_m))
         {
            MIDASERROR(info("arCrea or arAnni out-of-range, i = "+std::to_string(i)+"."));
         }
      }
   }

   // Loop over "free" indices (those not specified by arModes).
   // Add up the values to get the result.
   const std::vector<Uin> p_cum      = CumulativeProduct(arDims);
   const std::set<Uin>    m_free     = ComplementaryModes(arDims.size(), arModes);
   const std::vector<Uin> d_free     = SubsetDims(m_free, arDims);
   const std::vector<Uin> p_cum_free = SubsetDims(m_free, p_cum);
   const Uin tot_free_dim = Product(d_free);

   auto abs_ind_fix = [&arModes, &p_cum](const std::vector<Uin>& v) -> Uin
   {
      Uin a = 0;
      Uin i = 0;
      for(const auto& m: arModes)
      {
         a += v.at(i)*p_cum.at(m);
         ++i;
      }
      return a;
   };
   Uin abs_an = abs_ind_fix(arAnni);
   Uin abs_cr = abs_ind_fix(arCrea);
   std::vector<Uin> mult_i_free(p_cum_free.size(), 0);

   T val = 0;
   for(Uin i = 0; i < tot_free_dim; ++i)
   {
      if constexpr(CONJ_BRA)
      {
         val += midas::math::Conj(arBra[abs_cr]) * arKet[abs_an];
      }
      else
      {
         val += arBra[abs_cr] * arKet[abs_an];
      }
      const In incr = IncrMultiIndex(mult_i_free, d_free, p_cum_free);
      abs_an += incr;
      abs_cr += incr;
   }
   return val;
}

/***************************************************************************//**
 * A (comparatively) faster \f$ \exp(\alpha T) \f$ implementation, working on a
 * specific vector. As opposed to MatRepVibOper<T>::ExpClusterOper(), this
 * function avoids the heavy matrix-matrix multiplications; in stead it takes
 * the cluster operator matrix and transforms the given vector using only
 * matrix-vector multiplications.
 *
 * @param[in] arOper
 *    Sparse matrix representation of the cluster operator T.
 * @param[in] arAmpls
 *    The cluster amplitudes used for the transformation. Number and ordering
 *    must fit with how arOper was constructed from dimensions (N_modals) and
 *    ModeCombiOpRange.
 * @param[in] aVec
 *    The vector to be transformed. (Of full direct-product space dimension.)
 * @param[in] aCoef
 *    The coefficient \f$ \alpha \f$ in \f$ \exp(\alpha T) \f$.
 * @return
 *    The vector resulting from the \f$ \exp(\alpha T) \f$ transformation of
 *    aVec.
 *     - `DEEXC: false: exp(aT)|aVec>`
 *     - `DEEXC: true:  <aVec|exp(aT)`
 *     - `CONJ: false/true`: conjugate elements of arAmpls _and_ aCoef
 ******************************************************************************/
template<bool DEEXC, bool CONJ, typename T>
GeneralMidasVector<T> TrfExpClusterOper
   (  const SparseClusterOper& arOper
   ,  const GeneralMidasVector<T>& arAmpls
   ,  GeneralMidasVector<T> aVec
   ,  T aCoef
   )
{
   using vec_t = decltype(aVec);

   // SparseClusterOper does the size assertions.
   // It's non-standard to have something on the diagonal in a VCC cluster
   // operator; corresponds to an amplitude for the empty ModeCombi. Therefore
   // not implemented for that case (it makes T not nil-potent).
   if (arOper.ContainsEmptyMC() && arAmpls.Size() > 0 && arAmpls[0] != T(0))
   {
      std::stringstream ss;
      ss << "Non-zero amp. for the empty MC (= " << arAmpls[0] << ").";
      MIDASERROR(ss.str());
   }

   // Lambda for handling multiplication by matrix transformation.
   auto mat_mult = [&arOper,&arAmpls](const vec_t& v) -> vec_t
   {
      return arOper.Transform<DEEXC,CONJ>(arAmpls,v);
   };

   // The given vector is the k = 0 term (identity).
   vec_t vk = aVec;

   // Then loop up to number of modes.
   const Uin n_modes = arOper.NumModes();
   if constexpr(CONJ)
   {
      aCoef = midas::math::Conj(aCoef);
   }
   for(Uin k = 1; k <= n_modes; ++k)
   {
      vk = mat_mult(vk);
      vk.Scale(aCoef/T(k));
      aVec += vk;
   }

   // Cluster operators are nilpotent, T^k = 0 for k > M.
   if (Norm(mat_mult(vk)) != 0)
   {
      MIDASERROR("T^(M+1) not zero as expected.");
   }

   return aVec;
}

/***************************************************************************//**
 * See the other TrfVccErrVec() for description.
 ******************************************************************************/
template<typename T>
GeneralMidasVector<T> TrfVccErrVec
   (  const OperMat<T>& arOperMat
   ,  const SparseClusterOper& arClustOper
   ,  const GeneralMidasVector<T>& arTAmps
   )
{
   using vec_t = GeneralMidasVector<T>;
   const auto& H = arOperMat;

   if (!H.IsSquare() || H.Ncols() != arClustOper.FullDim())
   {
      std::stringstream ss;
      ss << "Size mismatch; H.IsSquare() = " << std::boolalpha << H.IsSquare()
         << ", H.Ncols() = " << H.Ncols()
         << ", arClustOper.FullDim() = " << arClustOper.FullDim()
         << ".";
      MIDASERROR(ss.str());
   }

   // exp(-T) H exp(+T) |ref>
   // one mat-vec-mult. at a time to keep scaling down.
   vec_t v(arClustOper.FullDim(), T(0));
   v[0] = 1;
   v = TrfExpClusterOper<false,false>(arClustOper, arTAmps, std::move(v), T(+1));
   v = vec_t(H * v);
   v = TrfExpClusterOper<false,false>(arClustOper, arTAmps, std::move(v), T(-1));
   return MatRepVibOper<T>::ExtractToMcrSpace(v, arClustOper.Mcr(), arClustOper.Dims());
}

/***************************************************************************//**
 * Calculates the VCC error vector in the direct-product space using
 * MatRepVibOper.
 *
 * @param[in] arNModals
 *    Determines number of modes and modals per mode of direct-product space.
 * @param[in] arOpDef
 *    Determines the sum-over-product structure, i.e. coefficients and 1-mode
 *    operators of each term, of the (Hamiltonian) operator.
 * @param[in] arModInts
 *    The matrix elements of each 1-mode operator for each mode. Must be in
 *    correspondence with arOpDef and arNModals.
 * @param[in] arMcr
 *    The ModeCombiOpRange of the T amplitudes.
 * @param[in] arTAmps
 *    The T amplitudes; 1 vector per ModeCombi in arMcr, each vector containing
 *    the number of excitation parameters corresponding to arNModals.
 * @return
 *    The VCC error vector in the _excitation_ space, i.e. ordered according to
 *    arMcr and arNModals. If the empty ModeCombi is present in arMcr, that
 *    element will correspond to the VCC energy expression.
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasVector<T> TrfVccErrVec
   (  const std::vector<Uin>& arNModals
   ,  const OpDef& arOpDef
   ,  const ModalIntegrals<T>& arModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const std::vector<std::vector<T>>& arTAmps
   )
{
   OperMat<T> oper(arNModals, arOpDef, arModInts);
   SparseClusterOper clust_oper(arNModals, arMcr);
   auto ampls = McrOrganizedToStackedVec<T,GeneralMidasVector>(arTAmps);
   return TrfVccErrVec(std::move(oper), std::move(clust_oper), std::move(ampls));
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template<typename T>
GeneralMidasVector<T> TrfVci
   (  const OperMat<T>& arOperMat
   ,  const std::vector<Uin>& arNModals
   ,  const ModeCombiOpRange& arMcr
   ,  const std::vector<std::vector<T>>& arCCoefs
   )
{
   const auto Cref = CRefFullSpace(arNModals, arMcr, arCCoefs);
   if (Cref.size() != arOperMat.Ncols())
   {
      MIDASERROR("Cref.size() (which is "+std::to_string(Cref.size())+") != arOperMat.Ncols() (which is "+std::to_string(arOperMat.Ncols())+").");
   }
   const GeneralMidasVector<T> res_full_space = arOperMat * Cref;
   return MatRepVibOper<T>::ExtractToMcrSpace(res_full_space, arMcr, arNModals);
}

/***************************************************************************//**
 * Calculates the VCI transformation in the direct-product space using
 * MatRepVibOper.
 *
 * @param[in] arNModals
 *    Determines number of modes and modals per mode of direct-product space.
 * @param[in] arOpDef
 *    Determines the sum-over-product structure, i.e. coefficients and 1-mode
 *    operators of each term, of the (Hamiltonian) operator.
 * @param[in] arModInts
 *    The matrix elements of each 1-mode operator for each mode. Must be in
 *    correspondence with arOpDef and arNModals.
 * @param[in] arMcr
 *    The ModeCombiOpRange of the C coefficients.
 * @param[in] arCCoefs
 *    The C coefficients; 1 vector per ModeCombi in arMcr, each vector
 *    containing the number of excitation parameters corresponding to
 *    arNModals.
 * @return
 *    The VCI transformation result in the _excitation_ space, i.e. ordered
 *    according to arMcr and arNModals.
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasVector<T> TrfVci
   (  const std::vector<Uin>& arNModals
   ,  const OpDef& arOpDef
   ,  const ModalIntegrals<T>& arModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const std::vector<std::vector<T>>& arCCoefs
   )
{
   OperMat<T> oper(arNModals, arOpDef, arModInts);
   return TrfVci(std::move(oper), arNModals, arMcr, arCCoefs);
}

/***************************************************************************//**
 * See the other TrfVccEtaVec() for description.
 ******************************************************************************/
template<typename T>
GeneralMidasVector<T> TrfVccEtaVec
   (  const OperMat<T>& arOperMat
   ,  const ShiftOperBraketLooper& arLooper
   ,  const SparseClusterOper& arClustOper
   ,  const GeneralMidasVector<T>& arTAmps
   )
{
   using vec_t = typename MatRepVibOper<T>::vec_t;
   const auto& H = arOperMat;

   if (!H.IsSquare() || H.Ncols() != arClustOper.FullDim())
   {
      std::stringstream ss;
      ss << "Size mismatch; H.IsSquare() = " << std::boolalpha << H.IsSquare()
         << ", H.Ncols() = " << H.Ncols()
         << ", arClustOper.FullDim() = " << arClustOper.FullDim()
         << ".";
      MIDASERROR(ss.str());
   }

   // exp(T)|ref> and <ref|exp(-T) = <ref|.
   vec_t ket(arClustOper.FullDim(), T(0));
   ket[0] = 1;
   ket = TrfExpClusterOper<false,false>(arClustOper, arTAmps, std::move(ket), T(+1));

   // Hamiltonian; <ref|H and no contribution from <ref|(tau_nu)Hexp(T)|ref>.
   const vec_t bra = H.GetRow(0);

   // Loop over tau_nu.
   // Generally only evaluating first term of commutator (<ref|H tau_nu exp(T)|ref>)
   // because <ref|tau_nu H exp(T)|ref> = 0 if tau_nu is _not_ for the empty
   // mode combination.
   // However, for the empty mode combination [H,tau_nu] = 0, so we'll just
   // zero explicitly. (Handled by the looper.)
   const std::vector<std::tuple<T,const vec_t*,const vec_t*>> terms =
      {  {T(1), &bra, &ket}
      };
   return arLooper.Transform<false>(terms);
}

/***************************************************************************//**
 * Calculates the VCC \f$ \eta \f$ vector in the direct-product space using
 * MatRepVibOper.
 *
 * @param[in] arNModals
 *    Determines number of modes and modals per mode of direct-product space.
 * @param[in] arOpDef
 *    Determines the sum-over-product structure, i.e. coefficients and 1-mode
 *    operators of each term, of the (Hamiltonian) operator.
 * @param[in] arModInts
 *    The matrix elements of each 1-mode operator for each mode. Must be in
 *    correspondence with arOpDef and arNModals.
 * @param[in] arMcr
 *    The ModeCombiOpRange of the T amplitudes.
 * @param[in] arTAmps
 *    The T amplitudes; 1 vector per ModeCombi in arMcr, each vector containing
 *    the number of excitation parameters corresponding to arNModals.
 * @return
 *    The VCC \f$ \eta \f$ vector in the _excitation_ space, i.e. ordered
 *    according to arMcr and arNModals. If the empty ModeCombi is present in
 *    arMcr, that element of the result will automatically be zero result due
 *    to the commutator \f$ [H,\tau_{\{\}}] = 0 \f$.
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasVector<T> TrfVccEtaVec
   (  const std::vector<Uin>& arNModals
   ,  const OpDef& arOpDef
   ,  const ModalIntegrals<T>& arModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const std::vector<std::vector<T>>& arTAmps
   )
{
   OperMat<T> oper(arNModals, arOpDef, arModInts);
   ShiftOperBraketLooper looper(arNModals, arMcr);
   SparseClusterOper clust_oper(arNModals, arMcr);
   auto ampls = McrOrganizedToStackedVec<T,GeneralMidasVector>(arTAmps);
   return TrfVccEtaVec(std::move(oper), std::move(looper), std::move(clust_oper), std::move(ampls));
}

/***************************************************************************//**
 * See the other TrfVccRJac() for description.
 ******************************************************************************/
template<typename T>
GeneralMidasVector<T> TrfVccRJac
   (  const OperMat<T>& arOperMat
   ,  const SparseClusterOper& arClustOper
   ,  const GeneralMidasVector<T>& arTAmps
   ,  const GeneralMidasVector<T>& arRCoefs
   ,  const bool aZeroRefElemResult
   )
{
   using vec_t = typename MatRepVibOper<T>::vec_t;
   const auto& H = arOperMat;

   if (!H.IsSquare() || H.Ncols() != arClustOper.FullDim())
   {
      std::stringstream ss;
      ss << "Size mismatch; H.IsSquare() = " << std::boolalpha << H.IsSquare()
         << ", H.Ncols() = " << H.Ncols()
         << ", arClustOper.FullDim() = " << arClustOper.FullDim()
         << ".";
      MIDASERROR(ss.str());
   }

   // exp(T)|ref>
   vec_t ket(arClustOper.FullDim(), T(0));
   ket[0] = 1;
   ket = TrfExpClusterOper<false,false>(arClustOper, arTAmps, std::move(ket), T(+1));

   // exp(-T)[H,R]exp(T)|ref>
   vec_t res = vec_t(H * arClustOper.Transform<false,false>(arRCoefs, ket));
   res -= arClustOper.Transform<false,false>(arRCoefs, vec_t(H * ket));
   res = TrfExpClusterOper<false,false>(arClustOper, arTAmps, std::move(res), T(-1));
   if (aZeroRefElemResult)
   {
      res[0] = 0;
   }
   return MatRepVibOper<T>::ExtractToMcrSpace(res, arClustOper.Mcr(), arClustOper.Dims());
}

/***************************************************************************//**
 * Calculates the result of a right-multiplication of the Jacobian of the VCC
 * error vector.
 *
 * @param[in] arNModals
 *    Determines number of modes and modals per mode of direct-product space.
 * @param[in] arOpDef
 *    Determines the sum-over-product structure, i.e. coefficients and 1-mode
 *    operators of each term, of the (Hamiltonian) operator.
 * @param[in] arModInts
 *    The matrix elements of each 1-mode operator for each mode. Must be in
 *    correspondence with arOpDef and arNModals.
 * @param[in] arMcr
 *    The ModeCombiOpRange of the T amplitudes and R coefficients.
 * @param[in] arTAmps
 *    The T amplitudes; 1 vector per ModeCombi in arMcr, each vector containing
 *    the number of excitation parameters corresponding to arNModals.
 * @param[in] arRCoefs
 *    The R coefficients; 1 vector per ModeCombi in arMcr, each vector
 *    containing the number of excitation parameters corresponding to
 *    arNModals.
 * @param[in] aZeroRefElemResult
 *    Optionally, explicitly zero the element for the empty ModeCombi, if
 *    present in arMcr.
 * @return
 *    The result of a right-multiplication of the Jacobian of the VCC error
 *    vector, in the _excitation_ space, i.e. ordered according to arMcr and
 *    arNModals. If the empty ModeCombi is present; T/R parameters for the
 *    empty ModeCombi will not contribute to the result, but there will be a
 *    non-trivial value for that element of the result vector. This is not
 *    always regarded as part of the Jacobian transformation and can therefore
 *    be explicitly zeroed by setting aZeroRefElemResult true, for consistency
 *    with calculations by other methods.
 *    If aZeroRefElemResult false, the empty ModeCombi element will be equal to
 *    \f$ \eta^T R \f$.
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasVector<T> TrfVccRJac
   (  const std::vector<Uin>& arNModals
   ,  const OpDef& arOpDef
   ,  const ModalIntegrals<T>& arModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const std::vector<std::vector<T>>& arTAmps
   ,  const std::vector<std::vector<T>>& arRCoefs
   ,  const bool aZeroRefElemResult
   )
{
   OperMat<T> oper(arNModals, arOpDef, arModInts);
   SparseClusterOper clust_oper(arNModals, arMcr);
   auto t_ampls = McrOrganizedToStackedVec<T,GeneralMidasVector>(arTAmps);
   auto r_coefs = McrOrganizedToStackedVec<T,GeneralMidasVector>(arRCoefs);
   return TrfVccRJac(std::move(oper), std::move(clust_oper), std::move(t_ampls), std::move(r_coefs), aZeroRefElemResult);
}

/***************************************************************************//**
 * See the other TrfVccLJac() for description.
 ******************************************************************************/
template<typename T>
GeneralMidasVector<T> TrfVccLJac
   (  const OperMat<T>& arOperMat
   ,  const ShiftOperBraketLooper& arLooper
   ,  const SparseClusterOper& arClustOper
   ,  const GeneralMidasVector<T>& arTAmps
   ,  const GeneralMidasVector<T>& arLCoefs
   )
{
   using vec_t = typename MatRepVibOper<T>::vec_t;
   const auto& H = arOperMat;

   if (!H.IsSquare() || H.Ncols() != arClustOper.FullDim())
   {
      std::stringstream ss;
      ss << "Size mismatch; H.IsSquare() = " << std::boolalpha << H.IsSquare()
         << ", H.Ncols() = " << H.Ncols()
         << ", arClustOper.FullDim() = " << arClustOper.FullDim()
         << ".";
      MIDASERROR(ss.str());
   }

   // exp(T)|ref>.
   vec_t ket(arClustOper.FullDim(), T(0));
   ket[0] = 1;
   ket = TrfExpClusterOper<false,false>(arClustOper, arTAmps, std::move(ket), T(+1));

   // <ref|Lexp(-T)
   // L is defined as sum(l_nu tau_nu^dagger)
   // Since L _excites_ from the reference in <ref|L,
   // we just call SparseClusterOper::RefToFullSpace<CONJ=false>().
   // The exp(-T) on the other hand DE-excites when working on a bra, so call
   // it with DEEXC=true.
   vec_t bra = arClustOper.RefToFullSpace(arLCoefs);
   bra = TrfExpClusterOper<true,false>(arClustOper, arTAmps, std::move(bra), T(-1));

   // Hamiltonian; <ref|exp(-T)H and Hexp(T)|ref>.
   const vec_t bra_H = vec_t(bra * H);
   const vec_t H_ket = vec_t(H * ket);

   // Loop over tau_nu.
   // For the empty mode combination [H,tau_nu] = 0, so we'll just zero
   // explicitly (handled automatically by looper). (The commutator will in
   // principle evaluate to zero, but there _can_ be some numerical noise.)
   const std::vector<std::tuple<T,const vec_t*,const vec_t*>> terms =
      {  {T(1),  &bra_H, &ket}
      ,  {T(-1), &bra, &H_ket}
      };
   return arLooper.Transform<false>(terms);
}

/***************************************************************************//**
 * Calculates the result of a left-multiplication of the Jacobian of the VCC
 * error vector.
 *
 * @param[in] arNModals
 *    Determines number of modes and modals per mode of direct-product space.
 * @param[in] arOpDef
 *    Determines the sum-over-product structure, i.e. coefficients and 1-mode
 *    operators of each term, of the (Hamiltonian) operator.
 * @param[in] arModInts
 *    The matrix elements of each 1-mode operator for each mode. Must be in
 *    correspondence with arOpDef and arNModals.
 * @param[in] arMcr
 *    The ModeCombiOpRange of the T amplitudes and L coefficients.
 * @param[in] arTAmps
 *    The T amplitudes; 1 vector per ModeCombi in arMcr, each vector containing
 *    the number of excitation parameters corresponding to arNModals.
 * @param[in] arLCoefs
 *    The L coefficients; 1 vector per ModeCombi in arMcr, each vector
 *    containing the number of excitation parameters corresponding to
 *    arNModals.
 * @return
 *    The result of a left-multiplication of the Jacobian of the VCC error
 *    vector, in the _excitation_ space, i.e. ordered according to arMcr and
 *    arNModals. If the empty ModeCombi is present in arMcr, that element of
 *    the result will automatically be zero result due to the commutator 
 *    \f$ [H,\tau_{\{\}}] = 0 \f$.
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasVector<T> TrfVccLJac
   (  const std::vector<Uin>& arNModals
   ,  const OpDef& arOpDef
   ,  const ModalIntegrals<T>& arModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const std::vector<std::vector<T>>& arTAmps
   ,  const std::vector<std::vector<T>>& arLCoefs
   )
{
   OperMat<T> oper(arNModals, arOpDef, arModInts);
   ShiftOperBraketLooper looper(arNModals, arMcr);
   SparseClusterOper clust_oper(arNModals, arMcr);
   auto t_ampls = McrOrganizedToStackedVec<T,GeneralMidasVector>(arTAmps);
   auto l_coefs = McrOrganizedToStackedVec<T,GeneralMidasVector>(arLCoefs);
   return TrfVccLJac(std::move(oper), std::move(looper), std::move(clust_oper), std::move(t_ampls), std::move(l_coefs));
}

/***************************************************************************//**
 * Calculates elements of the VCC Jacobian in the direct-product space using
 * MatRepVibOper, then converting it to/returning it in _excitation_ space.
 *
 * @param[in] arNModals
 *    Determines number of modes and modals per mode of direct-product space.
 * @param[in] arOpDef
 *    Determines the sum-over-product structure, i.e. coefficients and 1-mode
 *    operators of each term, of the (Hamiltonian) operator.
 * @param[in] arModInts
 *    The matrix elements of each 1-mode operator for each mode. Must be in
 *    correspondence with arOpDef and arNModals.
 * @param[in] arMcr
 *    The ModeCombiOpRange of the T amplitudes.
 * @param[in] arTAmps
 *    The T amplitudes; 1 vector per ModeCombi in arMcr, each vector containing
 *    the number of excitation parameters corresponding to arNModals.
 * @param[in] aDisregardEmptyMc
 *    If true, disregards the empty ModeCombi when calculating the Jacobian
 *    matrix, even if present in arMcr. This corresponds to the usual notion of
 *    the VCC Jacobian.
 *    If false (and arMcr contains the empty ModeCombi), the returned matrix
 *    will contain a row at index 0 corresponding to the VCC \f$ \eta \f$
 *    vector, and a column at index 0 that is identically zero due to the
 *    commutator \f$ [H,\tau_{\{\}}] = 0 \f$.
 * @return
 *    The VCC Jacobian matrix for the given operator and T amplitudes.
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasMatrix<T> ExplVccJac
   (  const std::vector<Uin>& arNModals
   ,  const OpDef& arOpDef
   ,  const ModalIntegrals<T>& arModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const std::vector<std::vector<T>>& arTAmps
   ,  const bool aDisregardEmptyMc
   )
{
   using mat_t = typename MatRepVibOper<T>::mat_t;
   using vec_t = typename MatRepVibOper<T>::vec_t;

   // Hamiltonian operator matrix.
   OperMat<T> oper(arNModals, arOpDef, arModInts);
   const Uin full_dim = Product(arNModals);
   const auto& H = oper;

   // Cluster operator.
   const SparseClusterOper clust_oper(arNModals, arMcr);
   const vec_t t_amps = McrOrganizedToStackedVec<T,GeneralMidasVector>(arTAmps);

   // exp(T)|ref> and Hexp(T)|ref>
   vec_t ket(full_dim, T(0));
   ket[0] = 1;
   ket = TrfExpClusterOper<false,false>(clust_oper, t_amps, std::move(ket), T(1));
   const vec_t H_ket = H * ket;

   // Set up ModeCombiOpRange without empty ModeCombi, if required.
   const ModeCombiOpRange* p_mcr = &arMcr;
   std::unique_ptr<ModeCombiOpRange> p_mcr_no_empty_mc(nullptr);
   bool removed_empty_mc = false;
   if (aDisregardEmptyMc && arMcr.NumEmptyMCs() > 0)
   {
      auto p_copy = new ModeCombiOpRange(arMcr);
      p_copy->Erase(std::vector<ModeCombi>{ModeCombi(0)});
      p_mcr_no_empty_mc.reset(p_copy);
      p_mcr = p_mcr_no_empty_mc.get();
      removed_empty_mc = true;
   }

   // Now we calculate A_{mu,nu};
   // - loop over nu, then calculate exp(-T)[H,tau_nu]exp(T)|ref>
   // - then loop over/extract all the <mu|'th elements of the resulting
   // full space vector for each tau_nu and store as columns in A.
   const auto v_p_mc_exci = McrExciIndices(*p_mcr, arNModals);
   Uin A_size = clust_oper.NumAmpls() - (removed_empty_mc? 1: 0);
   mat_t A(A_size, A_size, T(0));

   Uin j_col = 0;
   for(const auto& p_mc_exci: v_p_mc_exci)
   {
      const auto& mc = p_mc_exci.first;
      const auto& excis = p_mc_exci.second;
      const std::vector<Uin> anni(mc.size(),0);

      // Local SparseClusterOper for fast tau_mu transforms. But make a
      // dedicated function for this if you need it often!
      ModeCombiOpRange mc_mcr;
      mc_mcr.Insert(std::vector<ModeCombi>{ModeCombi(std::set<In>(mc.begin(),mc.end()),-1)});
      SparseClusterOper tau_nu(arNModals, std::move(mc_mcr));
      GeneralMidasVector<T> v_tau_nu(tau_nu.NumAmpls(), T(0));

      Uin abs_i = 0;
      for(auto it_exci = excis.begin(), end = excis.end(); it_exci != end; ++it_exci, ++j_col, ++abs_i)
      {
         // [H,tau_nu]exp(T)|ref>
         v_tau_nu[abs_i] = 1;
         vec_t v_res = H * tau_nu.Transform<false,false>(v_tau_nu, ket);
         v_res -= tau_nu.Transform<false,false>(v_tau_nu, H_ket);
         v_tau_nu[abs_i] = 0;

         // exp(-T)[H,tau_nu]exp(T)|ref>, extract and insert as column of A.
         v_res = TrfExpClusterOper<false,false>(clust_oper, t_amps, std::move(v_res), T(-1));
         const auto col_nu = MatRepVibOper<T>::ExtractToMcrSpace(v_res, *p_mcr, arNModals);
         if (col_nu.size() != A.Nrows()) MIDASERROR("Wrong column size.");
         if (j_col > A.Ncols()) MIDASERROR("j_col went out of range.");
         A.AssignCol(vec_t(col_nu), j_col);
      }
   }
   return A;
}

/***************************************************************************//**
 * Actually just wraps a call to LAPACK's linear equation solver.
 * It's the caller's responsibility that arA and arEta correspond to the same
 * set of T-amplitudes.
 *
 * @param[in] arATransp
 *    The VCC error vector Jacobian (transposed!) for some T amplitudes.
 * @param[in] arMinusEta
 *    The negative of the VCC eta vector for same T amplitudes.
 * @return
 *    The solution 
 *    \f$ \mathbf{l} \f$ to \f$ \mathbf{l}\mathbf{A} = -\mathbf{\eta} \f$
 *    where \f$ \mathbf{A}, \mathbf{\eta} \f$ are determined by the given
 *    arTamps.
 *    Bool is true on succes, false if an error (condition number/singularity)
 *    occured; the contents of the vector are undefined then.
 ******************************************************************************/
template
   <  typename T
   >
std::pair<GeneralMidasVector<T>,bool> ExplVccJacSolveEtaPlusLA
   (  const GeneralMidasMatrix<T>& arATransp
   ,  const GeneralMidasVector<T>& arMinusEta
   )
{
   auto sol_struct = GESV(arATransp, arMinusEta);
   GeneralMidasVector<T> l;
   LoadSolution(sol_struct, l);
   return std::make_pair(std::move(l), sol_struct.info == 0);
}

/***************************************************************************//**
 * See the other TrfExtVccHamDerExtAmp() for description.
 ******************************************************************************/
template<typename T>
GeneralMidasVector<T> TrfExtVccHamDerExtAmp
   (  const OperMat<T>& arOperMat
   ,  const SparseClusterOper& arClustOper
   ,  const GeneralMidasVector<T>& arTAmps
   ,  const GeneralMidasVector<T>& arSAmps
   )
{
   using vec_t = typename MatRepVibOper<T>::vec_t;
   const auto& H = arOperMat;

   if (!H.IsSquare() || H.Ncols() != arClustOper.FullDim())
   {
      std::stringstream ss;
      ss << "Size mismatch; H.IsSquare() = " << std::boolalpha << H.IsSquare()
         << ", H.Ncols() = " << H.Ncols()
         << ", arClustOper.FullDim() = " << arClustOper.FullDim()
         << ".";
      MIDASERROR(ss.str());
   }

   // exp(+S) exp(-T) H exp(+T) |ref>
   // one mat-vec-mult. at a time to keep scaling down.
   vec_t v(arClustOper.FullDim(), T(0));
   v[0] = 1;
   v = TrfExpClusterOper<false,false>(arClustOper, arTAmps, std::move(v), T(+1));
   v = vec_t(H * v);
   v = TrfExpClusterOper<false,false>(arClustOper, arTAmps, std::move(v), T(-1));
   v = TrfExpClusterOper<true,false>(arClustOper, arSAmps, std::move(v), T(+1));
   return MatRepVibOper<T>::ExtractToMcrSpace(v, arClustOper.Mcr(), arClustOper.Dims());
}

/***************************************************************************//**
 * Calculates the derivative wrt. the "extended amplitudes" (here: s) of the
 * ExtVCC Hamiltonian, using MatRepVibOper. (VCC error-vector-like.)
 *     T        = sum_mu t_mu tau_mu
 *     S        = sum_mu s_mu tau_mu^dagger
 *     H        = <ref| exp(S) exp(-T) H exp(T) |ref>
 *     dH/ds_mu =  <mu| exp(S) exp(-T) H exp(T) |ref>
 *
 * @param[in] arNModals
 *    Determines number of modes and modals per mode of direct-product space.
 * @param[in] arOpDef
 *    Determines the sum-over-product structure, i.e. coefficients and 1-mode
 *    operators of each term, of the (Hamiltonian) operator.
 * @param[in] arModInts
 *    The matrix elements of each 1-mode operator for each mode. Must be in
 *    correspondence with arOpDef and arNModals.
 * @param[in] arMcr
 *    The ModeCombiOpRange of the T amplitudes.
 * @param[in] arTAmps
 *    The T amplitudes; 1 vector per ModeCombi in arMcr, each vector containing
 *    the number of excitation parameters corresponding to arNModals.
 * @param[in] arSAmps
 *    The S amplitudes; see arTAmps.
 * @return
 *    The `dH/ds_mu` in the _excitation_ space, i.e. ordered according to
 *    arMcr and arNModals. If the empty ModeCombi is present in arMcr, that
 *    element will correspond to the ExtVCC energy expression.
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasVector<T> TrfExtVccHamDerExtAmp
   (  const std::vector<Uin>& arNModals
   ,  const OpDef& arOpDef
   ,  const ModalIntegrals<T>& arModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const std::vector<std::vector<T>>& arTAmps
   ,  const std::vector<std::vector<T>>& arSAmps
   )
{
   OperMat<T> oper(arNModals, arOpDef, arModInts);
   SparseClusterOper clust_oper(arNModals, arMcr);
   auto t_amps = McrOrganizedToStackedVec<T,GeneralMidasVector>(arTAmps);
   auto s_amps = McrOrganizedToStackedVec<T,GeneralMidasVector>(arSAmps);
   return TrfExtVccHamDerExtAmp(std::move(oper), std::move(clust_oper), std::move(t_amps), std::move(s_amps));
}

/***************************************************************************//**
 * See the other TrfExtVccHamDerClustAmp() for description.
 ******************************************************************************/
template<typename T>
GeneralMidasVector<T> TrfExtVccHamDerClustAmp
   (  const OperMat<T>& arOperMat
   ,  const ShiftOperBraketLooper& arLooper
   ,  const SparseClusterOper& arClustOper
   ,  const GeneralMidasVector<T>& arTAmps
   ,  const GeneralMidasVector<T>& arSAmps
   )
{
   using vec_t = typename MatRepVibOper<T>::vec_t;
   const auto& H = arOperMat;

   if (!H.IsSquare() || H.Ncols() != arClustOper.FullDim())
   {
      std::stringstream ss;
      ss << "Size mismatch; H.IsSquare() = " << std::boolalpha << H.IsSquare()
         << ", H.Ncols() = " << H.Ncols()
         << ", arClustOper.FullDim() = " << arClustOper.FullDim()
         << ".";
      MIDASERROR(ss.str());
   }

   // ket   = exp(T)|ref>
   // H_ket = H exp(t)|ref>
   vec_t ket(arClustOper.FullDim(), T(0));
   ket[0] = 1;
   ket = TrfExpClusterOper<false,false>(arClustOper, arTAmps, std::move(ket), T(+1));
   const vec_t H_ket = H * ket;

   // bra   = <ref|exp(S)exp(-T)
   // bra_H = <ref|exp(S)exp(-T)H
   vec_t bra(arClustOper.FullDim(), T(0));
   bra[0] = 1;
   bra = TrfExpClusterOper<false,false>(arClustOper, arSAmps, std::move(bra), T(+1));
   bra = TrfExpClusterOper<true,false>(arClustOper, arTAmps, std::move(bra), T(-1));
   const vec_t bra_H = bra * H;

   // Loop over tau_nu.
   // For the empty mode combination [H,tau_nu] = 0, so we'll just zero
   // explicitly (handled by the looper). (The commutator will in principle
   // evaluate to zero, but there _can_ be some numerical noise.)
   const std::vector<std::tuple<T,const vec_t*,const vec_t*>> terms =
      {  {T(1),  &bra_H, &ket}
      ,  {T(-1), &bra, &H_ket}
      };
   return arLooper.Transform<false>(terms);
}

/***************************************************************************//**
 * Calculates the derivative wrt. the cluster amplitudes (here: t) of the
 * ExtVCC Hamiltonian, using MatRepVibOper. (VCC L-Jac-like.)
 *     T        = sum_mu t_mu tau_mu
 *     S        = sum_mu s_mu tau_mu^dagger
 *     H        = <ref| exp(S) exp(-T) H exp(T) |ref>
 *     dH/dt_mu = <ref| exp(S) exp(-T) [H,tau_mu] exp(T) |ref>
 *              = <ref| exp(S) [bar{H},tau_mu] |ref>
 *
 * @param[in] arNModals
 *    Determines number of modes and modals per mode of direct-product space.
 * @param[in] arOpDef
 *    Determines the sum-over-product structure, i.e. coefficients and 1-mode
 *    operators of each term, of the (Hamiltonian) operator.
 * @param[in] arModInts
 *    The matrix elements of each 1-mode operator for each mode. Must be in
 *    correspondence with arOpDef and arNModals.
 * @param[in] arMcr
 *    The ModeCombiOpRange of the T amplitudes and S coefficients.
 * @param[in] arTAmps
 *    The T amplitudes; 1 vector per ModeCombi in arMcr, each vector containing
 *    the number of excitation parameters corresponding to arNModals.
 * @param[in] arSAmps
 *    The S amplitudes; see arTAmps.
 * @return
 *    The result of `dH/dt_mu`, as a vector in the _excitation_ space, i.e.
 *    ordered according to arMcr and arNModals. If the empty ModeCombi is
 *    present in arMcr, that element of the result will automatically be zero
 *    result due to the commutator \f$ [H,\tau_{\{\}}] = 0 \f$.
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasVector<T> TrfExtVccHamDerClustAmp
   (  const std::vector<Uin>& arNModals
   ,  const OpDef& arOpDef
   ,  const ModalIntegrals<T>& arModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const std::vector<std::vector<T>>& arTAmps
   ,  const std::vector<std::vector<T>>& arSAmps
   )
{
   OperMat<T> oper(arNModals, arOpDef, arModInts);
   ShiftOperBraketLooper looper(arNModals, arMcr);
   SparseClusterOper clust_oper(arNModals, arMcr);
   auto t_amps = McrOrganizedToStackedVec<T,GeneralMidasVector>(arTAmps);
   auto s_amps = McrOrganizedToStackedVec<T,GeneralMidasVector>(arSAmps);
   return TrfExtVccHamDerClustAmp(std::move(oper), std::move(looper), std::move(clust_oper), std::move(t_amps), std::move(s_amps));
}

namespace detail
{
/***************************************************************************//**
 * Common implementation for TrfExtVccLeftExpmS() and TrfExtVccRightExpmS().
 ******************************************************************************/
template<bool LEFT, typename T>
GeneralMidasVector<T> TrfExtVccExpmSLeftOrRight
   (  const SparseClusterOper& arClustOper
   ,  const GeneralMidasVector<T>& arSAmps
   ,  const GeneralMidasVector<T>& arVec
   )
{
   constexpr bool deexc = LEFT;
   using vec_t = typename MatRepVibOper<T>::vec_t;

   vec_t v = arClustOper.RefToFullSpace<false>(arVec);
   v = TrfExpClusterOper<deexc,false>(arClustOper, arSAmps, std::move(v), T(-1));

   return MatRepVibOper<T>::ExtractToMcrSpace(v, arClustOper.Mcr(), arClustOper.Dims());
}
} /* namespace detail */

/***************************************************************************//**
 * Calculates the mat-vec. product \f$ K^- v \f$, where
 *     S         = sum_mu s_mu tau_mu^dagger
 *     k^-_mu,nu = <mu | exp(-S) | nu>
 *     V         = vector with elements v_nu
 * For use in e.g. ExtVCC.
 *
 * @param[in] arClustOper
 *    Sparse cluster operator representation. Also holds info about num. modals
 *    and ModeCombiOpRange.
 * @param[in] arSAmps
 *    The S amplitudes; 1 vector per ModeCombi in arMcr, each vector containing
 *    the number of excitation parameters corresponding to arNModals.
 * @param[in] arVec
 *    Vector whose elements are MCR _excitation_-space organized, i.e.
 *    according to given arNModals and arMcr.
 * @return
 *    The result of multiplying arVec with the exp(-S) matrix from the left.
 *    Result is in the MCR _excitation_ space.
 ******************************************************************************/
template<typename T>
GeneralMidasVector<T> TrfExtVccLeftExpmS
   (  const SparseClusterOper& arClustOper
   ,  const GeneralMidasVector<T>& arSAmps
   ,  const GeneralMidasVector<T>& arVec
   )
{
   return detail::TrfExtVccExpmSLeftOrRight<true>(arClustOper, arSAmps, arVec);
}

/***************************************************************************//**
 * Calculates the mat-vec. product \f$ v K^- \f$, where
 *     S         = sum_mu s_mu tau_mu^dagger
 *     k^-_mu,nu = <mu | exp(-S) | nu>
 *     V         = vector with elements v_nu
 * For use in e.g. ExtVCC.
 *
 * @param[in] arClustOper
 *    Sparse cluster operator representation. Also holds info about num. modals
 *    and ModeCombiOpRange.
 * @param[in] arSAmps
 *    The S amplitudes; 1 vector per ModeCombi in arMcr, each vector containing
 *    the number of excitation parameters corresponding to arNModals.
 * @param[in] arVec
 *    Vector whose elements are MCR _excitation_-space organized, i.e.
 *    according to given arNModals and arMcr.
 * @return
 *    The result of multiplying arVec with the exp(-S) matrix from the right.
 *    Result is in the MCR _excitation_ space.
 ******************************************************************************/
template<typename T>
GeneralMidasVector<T> TrfExtVccRightExpmS
   (  const SparseClusterOper& arClustOper
   ,  const GeneralMidasVector<T>& arSAmps
   ,  const GeneralMidasVector<T>& arVec
   )
{
   return detail::TrfExtVccExpmSLeftOrRight<false>(arClustOper, arSAmps, arVec);
}

/***************************************************************************//**
 * @param[in] arNModals
 *    Determines number of modes and modals per mode of direct-product space.
 * @param[in] arOpDef
 *    Determines the sum-over-product structure, i.e. coefficients and 1-mode
 *    operators of each term, of the (Hamiltonian) operator.
 * @param[in] arModInts
 *    The matrix elements of each 1-mode operator for each mode. Must be in
 *    correspondence with arOpDef and arNModals.
 * @param[in] arMcr
 *    The ModeCombiOpRange of the T amplitudes.
 * @param[in] arTAmps
 *    The T amplitudes; 1 vector per ModeCombi in arMcr, each vector containing
 *    the number of excitation parameters corresponding to arNModals.
 * @return
 *    The solution 
 *    \f$ \mathbf{l} \f$ to \f$ \mathbf{l}\mathbf{A} = -\mathbf{\eta} \f$
 *    where \f$ \mathbf{A}, \mathbf{\eta} \f$ are determined by the given
 *    arTamps.
 *    If arMcr contains the empty ModeCombi, the returned vector will have a
 *    corresponding element at index 0, with a value of zero.
 *    Bool is true on succes, false if an error (condition number/singularity)
 *    occured; the contents of the vector are undefined then.
 ******************************************************************************/
template
   <  typename T
   >
std::pair<GeneralMidasVector<T>,bool> ExplVccJacSolveEtaPlusLA
   (  const std::vector<Uin>& arNModals
   ,  const OpDef& arOpDef
   ,  const ModalIntegrals<T>& arModInts
   ,  const ModeCombiOpRange& arMcr
   ,  const std::vector<std::vector<T>>& arTAmps
   )
{
   using mat_t = GeneralMidasMatrix<T>;
   using vec_t = GeneralMidasVector<T>;
   const bool trim = arMcr.NumEmptyMCs() != 0;
   mat_t A_T = ExplVccJac(arNModals, arOpDef, arModInts, arMcr, arTAmps, true);
   vec_t m_eta = TrfVccEtaVec(arNModals, arOpDef, arModInts, arMcr, arTAmps);

   // Trim away the reference element from -eta if necessary.
   // (A_T is always correct size due to 'true'.)
   if (trim)
   {
      m_eta.Erase(0,1);
   }
   // Assert sizes are right by now.
   if (A_T.Nrows() != m_eta.Size())
   {
      MIDASERROR("A_T.Nrows() (which is "+std::to_string(A_T.Nrows())+" != m_eta.Size() (which is "+std::to_string(m_eta.Size())+").");
   }

   // Scale and transpose.
   A_T.Transpose();
   Scale(m_eta, T(-1));

   // Solve.
   auto sol = ExplVccJacSolveEtaPlusLA(A_T, m_eta);

   // If the MCR contained the empty ModeCombi, put back a zero at that index.
   if (trim)
   {
      sol.first.Insert(0, T(0));
   }
   return sol;
}

/***************************************************************************//**
 * Example:
 * ~~~
 *     arDims        =  {4,4,3,2,4,2,4}
 *     arSubMcs      =  {{3}, {2,5}}
 *     arSubTensors  =  {t3, t25}
 *     return        =  {t235, {2,3,5}}
 * ~~~
 * where each element of `t235` is given by (pseudo-code)
 * ~~~
 *     t235[i,j,k] = t3[j]*t25[i,k]
 * ~~~
 * See unit test.
 *
 * @param[in] arDims
 *    Tensor dimensions for all modes of the full space.
 * @param[in] arSubMcs
 *    The sub-mode combinations corresponding to the tensors in arSubTensors.
 * @param[in] arSubTensors
 *    The sub-tensors to enter in direct product, stored in vectorized format.
 * @return
 *    Pair of resulting director product tensor and its corresponding mode
 *    combination.
 ******************************************************************************/
template<typename T>
std::pair<GeneralMidasVector<T>,std::set<Uin>> TensorDirectProduct
   (  const std::vector<Uin>& arDims
   ,  const std::vector<std::set<Uin>>& arSubMcs
   ,  const std::vector<GeneralMidasVector<T>>& arSubTensors
   )
{
   // Assert sizes.
   if (arSubMcs.size() != arSubTensors.size())
   {
      std::stringstream ss;
      ss << "arSubMcs.size() (which is " << arSubMcs.size()
         << ") != arSubTensors.size() (which is " << arSubTensors.size()
         << ")."
         ;
      MIDASERROR(ss.str());
   }

   // First get resulting mode combination and set up sub-MC dimensions.
   std::vector<std::vector<Uin>> sub_mc_dims;
   sub_mc_dims.reserve(arSubMcs.size());
   std::set<Uin> mc;
   for(const auto& sub_mc: arSubMcs)
   {
      for(const auto& m: sub_mc)
      {
         if (!mc.insert(m).second)
         {
            std::stringstream ss;
            ss << "Overlapping modes in arSubMcs = " << arSubMcs;
            MIDASERROR(ss.str());
         }
      }
      sub_mc_dims.emplace_back(SubsetDims(sub_mc, arDims));
   }

   const auto dims = SubsetDims(mc, arDims);
   GeneralMidasVector<T> v(Product(dims), T(1));
   for(Uin i = 0; i < v.Size(); ++i)
   {
      const auto spl_mi = SplitMultiIndex(MultiIndex(i,dims), mc, arSubMcs);
      for(Uin j = 0; j < spl_mi.size(); ++j)
      {
         v[i] *= arSubTensors.at(j)[AbsIndex(spl_mi.at(j),sub_mc_dims.at(j))];
      }
   }

   return std::make_pair(v, mc);
}

/***************************************************************************//**
 * 
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasVector<T> CRefFullSpace
   (  const std::vector<Uin>& arNModals
   ,  const ModeCombiOpRange& arMcr
   ,  const std::vector<std::vector<T>>& arCCoefs
   )
{
   if (arMcr.Size() != arCCoefs.size()) MIDASERROR("Num. mode combinations mismatch.");

   const auto size = Product(arNModals);
   const auto exci_dims_full = ShiftVals(arNModals, -1);

   GeneralMidasVector<T> v(size, T(0));
   auto it_mc = arMcr.begin();

   for(auto it_coefs = arCCoefs.begin(), end = arCCoefs.end(); it_coefs != end; ++it_coefs, ++it_mc)
   {
      const auto modes = SetFromVec(it_mc->MCVec());
      const auto exci_dims = SubsetDims(modes, exci_dims_full);
      const auto n_exci = Product(exci_dims);
      if (it_coefs->size() != n_exci) MIDASERROR("Size mismatch, number of amps.");

      //const std::vector<Uin> index_anni(modes.size(), 0);
      //const auto free_modes = ComplementaryModes(arNModals.size(), modes);
      const std::vector<Uin> free_indices(arNModals.size() - modes.size());
      std::vector<Uin> index_crea(arNModals.size());
      for(Uin i_crea = 0; i_crea < n_exci; ++i_crea)
      {
         index_crea = MatRepVibOper<T>::CombineIndices(modes, ShiftVals(MultiIndex(i_crea, exci_dims), +1), free_indices);
         v[AbsIndex(index_crea, arNModals)] = it_coefs->at(i_crea);
      }
   }

   return v;
}

/***************************************************************************//**
 * Computes \f$ \exp(T) \vert \Phi \rangle \f$ (i.e. exp(T) on the reference
 * state converted to VCI coefficients).
 * Uses that for each ModeCombi in the result, the VCI coefficients are the sum
 * of direct products of the amplitude tensors for the corresponding
 * sub-ModeCombis.
 * E.g. for a three ModeCombi:
 * ```
 *     c{0,1,2} = t{0}t{1}t{2}                           // if VCC[1]
 *              + t{0}t{1,2} + t{1}t{0,2} + t{2}t{0,1}   // if VCC[2]
 *              + t{0,1,2}                               // if VCC[3]
 * ```
 * where the sum is truncated based on which ModeCombis are in the
 * ModeCombiOpRange for the amplitudes (arMcr).
 *
 * @param[in] arNModals
 *    Determines number of modes and modals per mode of direct-product space.
 * @param[in] arMcr
 *    The ModeCombiOpRange of the T amplitudes.
 *    The ModeCombiRange addresses _must_ have been set correctly prior to
 *    calling this function, otherwise it's a hard error.
 * @param[in] arTAmps
 *    The T amplitudes; vectorized ModeCombi parameters in order defined by
 *    arMcr, and sizes of its ModeCombi%s, based on the arNModals.
 * @param[in] aMaxExciOut
 *    Max. mode combination level of result.
 * @return
 *    The result of \f$ \exp(T) \vert \Phi \rangle \f$, i.e. VCC to VCI
 *    conversion, within the ModeCombiOpRange space given by number of modes
 *    and aMaxExciOut.
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasVector<T> ExpTRef
   (  const std::vector<Uin>& arNModals
   ,  const ModeCombiOpRange& arMcr
   ,  const GeneralMidasVector<T>& arTAmps
   ,  const Uin aMaxExciOut
   )
{
   // Never trust, that the client has already set the ModeCombi addresses, so
   // check them.
   Uin check_addr = 0;
   for(const auto& mc: arMcr)
   {
      if (check_addr != mc.Address())
      {
         std::stringstream ss;
         ss << "Bad ModeCombi address:\n"
            << "   mc           = " << mc << '\n'
            << "   mc.Address() = " << mc.Address() << '\n'
            << "   check_addr   = " << check_addr << '\n'
            ;
         MIDASERROR(ss.str());
      }
      check_addr += NumParams(mc, arNModals);
   }

   // We don't expect a non-zero reference element in arTAmps, although it can
   // easily be accomodated by scaling the end result with exp(t_ref) if needed.
   ModeCombiOpRange::const_iterator it_ref;
   if (arMcr.Find(std::vector<In>{}, it_ref))
   {
      if (arTAmps.Size() > 0)
      {
         auto addr = it_ref->Address();
         // If addr is in [0,size), we assume it's been set validly, otherwise
         // set it to 0 (where the ref. elem. is usually at).
         if (addr < 0 || addr >= arTAmps.Size())
         {
            addr = 0;
         }
         if (arTAmps[addr] != T(0))
         {
            std::stringstream ss;
            ss << "t ampl. for ref. (address = " << it_ref->Address()
               << ") (which is " << arTAmps[it_ref->Address()]
               << ") != 0, unexpectedly.";
            MIDASERROR(ss.str());
         }
      }
   }

   // Set up ModeCombiOpRange and vector for result (might be different from arMcr).
   ModeCombiOpRange mcr_result(aMaxExciOut, arNModals.size());
   const Uin tot_size = SetAddresses(mcr_result, arNModals);
   GeneralMidasVector<T> mv_expT(tot_size, T(0));

   // Take care of reference, which is always 1 from the exponential.
   if (mv_expT.Size() > 0)
   {
      mv_expT[0] = 1;
   }

   // Loop through ModeCombis of result, find constituting sum-ModeCombis, form
   // sum of tensor direct products.
   std::vector<std::vector<ModeCombi>> vec_vec_sub_mcs;
   GeneralMidasVector<T> mc_res;
   std::pair<GeneralMidasVector<T>,std::set<Uin>> p_tens_dir_prod;
   std::vector<Uin> tensor_dims = ShiftVals(arNModals, -1);
   std::vector<std::set<Uin>> v_sub_mcs;
   std::vector<GeneralMidasVector<T>> v_sub_tens;
   for(auto it = mcr_result.Begin(1), end = mcr_result.end(); it != end; ++it)
   {
      SubSetSets(*it, vec_vec_sub_mcs, arMcr, 1);
      
      mc_res.SetNewSize(NumParams(*it, arNModals));
      mc_res.Zero();
      p_tens_dir_prod.first.SetNewSize(mc_res.Size());
      for(const auto& vec_sub_mcs: vec_vec_sub_mcs)
      {
         v_sub_mcs.resize(vec_sub_mcs.size());
         v_sub_tens.resize(vec_sub_mcs.size());
         for(Uin i = 0; i < v_sub_mcs.size(); ++i)
         {
            v_sub_mcs[i] = SetFromVec(vec_sub_mcs[i].MCVec());
            v_sub_tens[i].SetNewSize(NumParams(vec_sub_mcs[i],tensor_dims,0));
            arTAmps.PieceIo(IO_GET, v_sub_tens[i], v_sub_tens[i].Size(), vec_sub_mcs[i].Address());
         }
         p_tens_dir_prod = TensorDirectProduct(tensor_dims, v_sub_mcs, v_sub_tens);
         if (p_tens_dir_prod.second != SetFromVec(it->MCVec()))
         {
            std::stringstream ss;
            ss << "p_tens_dir_prod.second (which is " << p_tens_dir_prod.second
               << ") != it->MCVec() (which is " << SetFromVec(it->MCVec())
               << ")."
               ;
            MIDASERROR(ss.str());
         }
         mc_res += p_tens_dir_prod.first;
      }
      mv_expT.PieceIo(IO_PUT, mc_res, mc_res.Size(), it->Address());
   }

   return mv_expT;
}

/***************************************************************************//**
 * @note
 *    See other ExpTRef() for explanation.
 *    This one just extracts the T-amplitudes to a GeneralMidasVector, then
 *    calls the other method.
 *    The return type is still a GeneralMidasVector, which you can easily
 *    convert to GeneralDataCont if so desired.
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasVector<T> ExpTRef
   (  const std::vector<Uin>& arNModals
   ,  const ModeCombiOpRange& arMcr
   ,  const GeneralDataCont<T>& arTAmps
   ,  const Uin aMaxExciOut
   )
{
   if (arTAmps.InMem())
   {
      return ExpTRef(arNModals, arMcr, *arTAmps.GetVector(), aMaxExciOut);
   }
   else
   {
      GeneralMidasVector<T> tmp(arTAmps.Size());
      arTAmps.DataIo(IO_GET, tmp, tmp.Size());
      return ExpTRef(arNModals, arMcr, tmp, aMaxExciOut);
   }
}

/***************************************************************************//**
 * Performs the inverse mapping of ExpTRef, i.e. given a vector of C coefs.
 * (orderede according to given ModeCombiOpRange and num.modals), finds the T
 * ampls. so that `exp(T)|ref> = (1 + C)|ref>`.
 *
 * Method:
 * The `exp(T)|ref>` map is given by (with some terms omitted according which
 * ModeCombi%s are actually in the ModeCombiOpRange):
 * ```
 *     c{0}     = t{0}
 *     ...
 *     c{0,1}   = t{0}t{1}
 *              + t{0,1}
 *     ...
 *     c{0,1,2} = t{0}t{1}t{2}
 *              + t{0}t{1,2} + t{1}t{0,2} + t{2}t{0,1}
 *              + t{0,1,2}
 *     ...
 * ```
 * As is seen, with the c coefs. given, the t ampls. can be found by
 * back-substitution, starting at the 1-mode coefs./ampls.;
 * ```
 *     t{0}     = c{0}
 *     ...
 *     t{0,1}   = c{0,1}
 *              - t{0}t{1}
 *     ...
 *     t{0,1,2} = c{0,1,2}
 *              - t{0}t{1}t{2}
 *              - t{0}t{1,2} - t{1}t{0,2} - t{2}t{0,1}
 *     ...
 * ```
 *
 * @note
 *    Only set up to work with C_ref = 1 (checked), and thus T_ref = 0.
 *
 * @param[in] arNModals
 *    Determines number of modes and modals per mode of direct-product space.
 * @param[in] arMcr
 *    The ModeCombiOpRange of the C coefs./T amplitudes.
 *    The ModeCombiRange addresses _must_ have been set correctly prior to
 *    calling this function, otherwise it's a hard error.
 * @param[in] arCCoefs
 *    The VCI-like coefficients `1+C`.
 * @return
 *    The T ampls. so that `exp(T)|ref> = (1+C)|ref>` within the arMcr-space.
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasVector<T> InvExpTRef
   (  const std::vector<Uin>& arNModals
   ,  const ModeCombiOpRange& arMcr
   ,  const GeneralMidasVector<T>& arCCoefs
   )
{
   // Never trust, that the client has already set the ModeCombi addresses, so
   // check them.
   Uin check_addr = 0;
   for(const auto& mc: arMcr)
   {
      if (check_addr != mc.Address())
      {
         std::stringstream ss;
         ss << "Bad ModeCombi address:\n"
            << "   mc           = " << mc << '\n'
            << "   mc.Address() = " << mc.Address() << '\n'
            << "   check_addr   = " << check_addr << '\n'
            ;
         MIDASERROR(ss.str());
      }
      check_addr += NumParams(mc, arNModals);
   }

   // Check correct size of arCCoefs.
   if (check_addr != arCCoefs.Size())
   {
      std::stringstream ss;
      ss << "Size mismatch:\n"
         << "   check_addr      = " << check_addr << '\n'
         << "   arCCoefs.Size() = " << arCCoefs.Size() << '\n'
         ;
      MIDASERROR(ss.str());
   }

   // We don't expect a non-unit reference element in arCCoefs, although it can
   // easily be accomodated in the future by scaling.
   ModeCombiOpRange::const_iterator it_ref;
   const bool has_ref_mc = arMcr.Find(std::vector<In>{}, it_ref);
   if (  has_ref_mc
      && arCCoefs[it_ref->Address()] != T(1)
      )
   {
      std::stringstream ss;
      ss << "c coef. for ref. (address = " << it_ref->Address()
         << ") (which is " << arCCoefs[it_ref->Address()]
         << ") != 1, unexpectedly.";
      MIDASERROR(ss.str());
   }

   GeneralMidasVector<T> t_ampls = arCCoefs;
   
   // Set ref. ampl. to zero, if present.
   if (has_ref_mc)
   {
      t_ampls[it_ref->Address()] = T(0);
   }

   // Loop through ModeCombis of result, find constituting sum-ModeCombis, form
   // sum of tensor direct products for ModeCombi%s that are strict subsets of
   // target. Then subtract to get what target ampl. should be.
   std::vector<std::vector<ModeCombi>> vec_vec_sub_mcs;
   GeneralMidasVector<T> mc_res;
   std::pair<GeneralMidasVector<T>,std::set<Uin>> p_tens_dir_prod;
   std::vector<Uin> tensor_dims = ShiftVals(arNModals, -1);
   std::vector<std::set<Uin>> v_sub_mcs;
   std::vector<GeneralMidasVector<T>> v_sub_tens;
   for(auto it = arMcr.Begin(1), end = arMcr.end(); it != end; ++it)
   {
      SubSetSets(*it, vec_vec_sub_mcs, arMcr, 1);
      
      mc_res.SetNewSize(NumParams(*it, arNModals));
      mc_res.Zero();
      p_tens_dir_prod.first.SetNewSize(mc_res.Size());
      for(const auto& vec_sub_mcs: vec_vec_sub_mcs)
      {
         if (vec_sub_mcs.size() == 1)
         {
            // Then we're at the ModeCombi itself which we don't want to add to
            // the sum.
            continue;
         }

         v_sub_mcs.resize(vec_sub_mcs.size());
         v_sub_tens.resize(vec_sub_mcs.size());
         for(Uin i = 0; i < v_sub_mcs.size(); ++i)
         {
            v_sub_mcs[i] = SetFromVec(vec_sub_mcs[i].MCVec());
            v_sub_tens[i].SetNewSize(NumParams(vec_sub_mcs[i],tensor_dims,0));
            t_ampls.PieceIo(IO_GET, v_sub_tens[i], v_sub_tens[i].Size(), vec_sub_mcs[i].Address());
         }
         p_tens_dir_prod = TensorDirectProduct(tensor_dims, v_sub_mcs, v_sub_tens);
         if (p_tens_dir_prod.second != SetFromVec(it->MCVec()))
         {
            std::stringstream ss;
            ss << "p_tens_dir_prod.second (which is " << p_tens_dir_prod.second
               << ") != it->MCVec() (which is " << SetFromVec(it->MCVec())
               << ")."
               ;
            MIDASERROR(ss.str());
         }
         mc_res += p_tens_dir_prod.first;
      }
      // Subtract result from the stored C-coefs. to get the T-ampls. for that ModeCombi.
      t_ampls.PieceIo(IO_PUT, mc_res, mc_res.Size(), it->Address(), 1, 0, 1, true, T(-1));
   }

   return t_ampls;
}

/***************************************************************************//**
 * @note
 *    See other InvExpTRef() for explanation.
 *    This one just extracts the C-coefs. to a GeneralMidasVector, then
 *    calls the other method.
 *    The return type is still a GeneralMidasVector, which you can easily
 *    convert to GeneralDataCont if so desired.
 ******************************************************************************/
template
   <  typename T
   >
GeneralMidasVector<T> InvExpTRef
   (  const std::vector<Uin>& arNModals
   ,  const ModeCombiOpRange& arMcr
   ,  const GeneralDataCont<T>& arCCoefs
   )
{
   if (arCCoefs.InMem())
   {
      return InvExpTRef(arNModals, arMcr, *arCCoefs.GetVector());
   }
   else
   {
      GeneralMidasVector<T> tmp(arCCoefs.Size());
      arCCoefs.DataIo(IO_GET, tmp, tmp.Size());
      return InvExpTRef(arNModals, arMcr, tmp);
   }
}

/***************************************************************************//**
 * @note
 *    Inefficient implementation that construct full direct-product space
 *    matrices on the way. (As opposed to ExpTRef() which tries to be a little
 *    smarter if not going to full excitation level.)
 *
 * @param[in] arNModals
 *    Determines number of modes and modals per mode of direct-product space.
 * @param[in] arMcr
 *    The ModeCombiOpRange of the T and L parameters.
 * @param[in] arTAmps
 *    The T amplitudes; vectorized ModeCombi parameters in order defined by
 *    arMcr and its ModeCombi::Address()%s.
 * @param[in] arLCoefs
 *    The L coefficients; vectorized ModeCombi parameters in order defined by
 *    arMcr and its ModeCombi::Address()%s.
 * @param[in] aMaxExciOut
 *    Max. mode combination level of result.
 * @return
 *    The result of \f$ \langle\Phi\vert (1+L)\exp(-T) \f$, i.e. VCC->VCI for
 *    Lambda state, within the ModeCombiOpRange space given by number of modes
 *    and aMaxExciOut.
 ******************************************************************************/
template<typename T, template<typename> class CONT_TMPL>
GeneralMidasVector<T> Ref1pLExpmT
   (  const std::vector<Uin>& arNModals
   ,  const ModeCombiOpRange& arMcr
   ,  const CONT_TMPL<T>& arTAmps
   ,  const CONT_TMPL<T>& arLCoefs
   ,  const Uin aMaxExciOut
   )
{
   using vec_t = typename MatRepVibOper<T>::vec_t;

   const vec_t t_ampls(GetElements(arTAmps, 0, arTAmps.Size()));
   const vec_t l_coefs(GetElements(arLCoefs, 0, arLCoefs.Size()));
   SparseClusterOper clust_oper(arNModals, arMcr);

   ModeCombiOpRange mcr_result(aMaxExciOut, arNModals.size());
   SetAddresses(mcr_result, arNModals);

   // <res| = <ref|(1+L)exp(-T)
   vec_t res = clust_oper.RefToFullSpace<false>(l_coefs);
   res[0] += 1;
   res = TrfExpClusterOper<true,false>(clust_oper, t_ampls, std::move(res), T(-1));

   return MatRepVibOper<T>::ExtractToMcrSpace(res, mcr_result, arNModals);
}

/***************************************************************************//**
 * @note
 *    Inefficient implementation that construct full direct-product space
 *    matrices on the way. (As opposed to ExpTRef() which tries to be a little
 *    smarter if not going to full excitation level.)
 *
 * @param[in] arNModals
 *    Determines number of modes and modals per mode of direct-product space.
 * @param[in] arMcr
 *    The ModeCombiOpRange of the T and S parameters.
 * @param[in] arTAmps
 *    The T amplitudes; vectorized ModeCombi parameters in order defined by
 *    arMcr and its ModeCombi::Address()%s.
 * @param[in] arSAmps
 *    The S amplitudes; vectorized ModeCombi parameters in order defined by
 *    arMcr and its ModeCombi::Address()%s.
 * @param[in] aMaxExciOut
 *    Max. mode combination level of result.
 * @return
 *    The result of \f$ \langle\Phi\vert \exp(S)\exp(-T) \f$, i.e. VCC->VCI for
 *    bra in EVCC (extended VCC), within the ModeCombiOpRange space given by
 *    number of modes and aMaxExciOut.
 ******************************************************************************/
template<typename T, template<typename> class CONT_TMPL>
GeneralMidasVector<T> RefExpSExpmT
   (  const std::vector<Uin>& arNModals
   ,  const ModeCombiOpRange& arMcr
   ,  const CONT_TMPL<T>& arTAmps
   ,  const CONT_TMPL<T>& arSAmps
   ,  const Uin aMaxExciOut
   )
{
   using vec_t = typename MatRepVibOper<T>::vec_t;

   const vec_t t_ampls(GetElements(arTAmps, 0, arTAmps.Size()));
   const vec_t s_ampls(GetElements(arSAmps, 0, arSAmps.Size()));
   SparseClusterOper clust_oper(arNModals, arMcr);

   ModeCombiOpRange mcr_result(aMaxExciOut, arNModals.size());
   SetAddresses(mcr_result, arNModals);

   // <res| = <ref|exp(S)exp(-T)
   vec_t res(clust_oper.FullDim(), T(0));
   res[0] = 1;
   res = TrfExpClusterOper<false,false>(clust_oper, s_ampls, std::move(res), T(+1));
   res = TrfExpClusterOper<true,false>(clust_oper, t_ampls, std::move(res), T(-1));

   return MatRepVibOper<T>::ExtractToMcrSpace(res, mcr_result, arNModals);
}

} /* namespace midas::util::matrep */

#endif/*MATREPTRANSFORMERS_IMPL_H_INCLUDED*/
