/**
 *******************************************************************************
 * 
 * @file    SparseClusterOper_Decl.h
 * @date    23-01-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef SPARSECLUSTEROPER_DECL_H_INCLUDED
#define SPARSECLUSTEROPER_DECL_H_INCLUDED

// Standard headers.
#include <array>
#include <utility>
#include <vector>

// Midas headers.
#include "inc_gen/TypeDefs.h"
#include "input/ModeCombiOpRange.h"

// Forward declarations.
template<typename> class GeneralMidasVector;

namespace midas::util::matrep
{
   /************************************************************************//**
    * 
    ***************************************************************************/
   class SparseClusterOper
   {
      public:
         template<typename U> using vec_tmpl = GeneralMidasVector<U>;
         using index_t = std::vector<std::vector<std::pair<Uin,Uin>>>;

         //@{
         //! Default constructors. Assignments deleted.
         SparseClusterOper(const SparseClusterOper&) = default;
         SparseClusterOper(SparseClusterOper&&) = default;
         SparseClusterOper& operator=(const SparseClusterOper&) = delete;
         SparseClusterOper& operator=(SparseClusterOper&&) = delete;
         ~SparseClusterOper() = default;
         //@}

         //! Constructor sets up everything needed.
         SparseClusterOper(std::vector<Uin>, ModeCombiOpRange);

         //!
         template<bool DEEXC = false, bool CONJ = false, typename T>
         vec_tmpl<T> Transform
            (  const vec_tmpl<T>& arAmpls
            ,  const vec_tmpl<T>& arVec
            )  const;

         //!
         template<bool CONJ = false, typename T>
         vec_tmpl<T> RefToFullSpace
            (  const vec_tmpl<T>& arAmpls
            )  const;

         /******************************************************************//**
          * @name Queries
          *********************************************************************/
         //!@{
         //!
         const std::vector<Uin>& Dims() const {return mDims;}

         //!
         const ModeCombiOpRange& Mcr() const {return mMcr;}

         //!
         Uin NumModes() const {return mDims.size();}

         //! The dimension of the full space, i.e. product of dimensions for each mode.
         Uin FullDim() const {return mFullDim;}

         //!
         Uin NumAmpls() const {return !mAmplMcAddresses.empty()? mAmplMcAddresses.back(): 0;}

         //!
         const std::vector<Uin>& AmplMcAddresses() const {return mAmplMcAddresses;}

         //!
         Uin NumNonZeroElems() const {return mNumNonZeroElems;}

         //!
         bool ContainsEmptyMC() const {return Mcr().NumEmptyMCs() > 0;}

         //!
         const index_t& IndicesExc() const {return Indices<false>();}
         const index_t& IndicesDeExc() const {return Indices<true>();}

         Nb Density() const;
         Nb Sparsity() const;

         //! Example of division of labour (ranges in result vector) for given num. ranks.
         std::vector<Uin> ExampleMpiOmpRangesExc(Uin aNumMpiRanks) const;
         std::vector<Uin> ExampleMpiOmpRangesDeExc(Uin aNumMpiRanks) const;

         //! Actual ranges, given num. MPI ranks.
         const std::vector<Uin>& MpiOmpRangesExc() const {return MpiOmpRanges<false>();}
         const std::vector<Uin>& MpiOmpRangesDeExc() const {return MpiOmpRanges<true>();}

         //!@}

      private:
         //!
         const std::vector<Uin> mDims;

         //!
         const ModeCombiOpRange mMcr;

         //!
         const Uin mFullDim = 0;

         //! Number of amplitudes per ModeCombi. (For assertions.)
         const std::vector<Uin> mAmplMcAddresses;

         //! result_index[sum[{input_index,ampl_index}]]
         const std::array<index_t,2> mIndices;

         //!
         const Uin mNumNonZeroElems = 0;

         //!
         const std::array<std::vector<Uin>,2> mMpiOmpRanges;

         template<bool DEEXC>
         const index_t& Indices() const;

         template<bool DEEXC>
         const std::vector<Uin>& MpiOmpRanges() const;

         static std::vector<Uin> CalcAmplMcAddresses
            (  const std::vector<Uin>& arDims
            ,  const ModeCombiOpRange& arMcr
            );

         static std::array<index_t,2> CalcIndices
            (  const std::vector<Uin>& arDims
            ,  const ModeCombiOpRange& arMcr
            );

         static Uin CalcNumNonZeroElems
            (  const index_t& arIndices
            );

         static std::vector<Uin> CalcMpiOmpRanges
            (  const index_t& arIndices
            ,  const Uin aNumMpiRanks
            );
   };

   void OptimizationAnalysis
      (  const SparseClusterOper& arSco
      ,  std::ostream& arOs
      ,  Uin aIndent = 3
      );

} /* namespace midas::util::matrep */

#endif/*SPARSECLUSTEROPER_DECL_H_INCLUDED*/
