/**
 *******************************************************************************
 * 
 * @file    ShiftOperBraketLooper.h
 * @date    02-03-2020
 * @author  Mads Boettger Hansen (mb.hansen@chem.au.dk)
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef SHIFTOPERBRAKETLOOPER_H_INCLUDED
#define SHIFTOPERBRAKETLOOPER_H_INCLUDED

#include "util/matrep/ShiftOperBraketLooper_Decl.h"

#ifdef DISABLE_PRECOMPILED_TEMPLATES
#include "util/matrep/ShiftOperBraketLooper_Impl.h"
#endif /*DISABLE_PRECOMPILED_TEMPLATES*/

#endif/*SHIFTOPERBRAKETLOOPER_H_INCLUDED*/
