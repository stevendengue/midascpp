#ifndef RUN_ASYNC_H_INCLUDED
#define RUN_ASYNC_H_INCLUDED

#include <vector>
#include <string>
#include <iostream>
#include <future>
#include <utility>

#include "system_command_t.h"
#include "threaded_system_call.h"

/**
 * run system command asynchronously
 **/
auto run_async(const system_command_t& cmd, mutex_ostream& os)
   -> decltype(std::async(std::launch::async,threaded_system_call,cmd,std::ref(os)));

/**
 * check if thread is done
 **/
template<class T>
bool thread_done(std::future<T>& f)
{
   return (f.wait_for(std::chrono::milliseconds(0)) == std::future_status::ready);
}

#endif /* RUN_ASYNC_H_INCLUDED */
