#include "spawn_process.h"

#include<sys/wait.h>
#include<unistd.h>
#include<errno.h>
#include<iostream>
#include<algorithm> // for std::transform
#include<cstring>
#include <thread>

#include"util/stream/HeaderInserter.h"

#ifdef VAR_MPI
#include <mpi.h>
#include <mutex>
#include <string.h>
#include <chrono>
#include "util/paral/catenate_command.h"
#include "mpi/Impi.h"
#endif /* VAR_MPI */


/**
 * class to make sure that child process always terminates
 **/
class exit_guard
{
   private:
      int code = 1;

   public:
      ~exit_guard()
      {
         //abort();
         _Exit(code);
      }
};

/**
 *
 **/
int nargs_in_argv(char* const argv[])
{
   int size = 0;
   while(argv[size])
   {
      ++size;
   }
   return size;
}

/**
 * Down cast const char* to char*.
 **/
char* downcast_char(char const* c)
{
   return const_cast<char*>(c);
}

/**
 * Create a pseudo pointer to character array, from a system command.
 *
 * @param cmd   The system command to transform.
 *
 * @return      Return a pointer to character array to be used for e.g. execvp().
 **/
std::vector<char*> make_command
   (  const system_command_t& cmd
   )
{
   std::vector<char*> cmd_char( cmd.size() + 1 );
   std::transform( cmd.begin(), cmd.end(), cmd_char.begin(),
                         []( std::string const& arg ) { return downcast_char(arg.c_str()); } );
   cmd_char[cmd.size()] = nullptr;
   return cmd_char;
}

int read_from_fd(int fd, std::stringstream& sstr)
{
   char buffer[1024];
   int count;

   while( (count = read(fd, buffer, sizeof(buffer)-1)) )
   {
      if (count >= 0) 
      {
         buffer[count] = 0;
         //printf("%s", buffer);
         sstr << buffer;
      }
      else 
      {
         printf("IO Error\n");
         return 1;
         //abort();
      }
   }
   return 0;
}

/**
 * Wait for a child process to finish.
 *
 * @param pid      The pid of the child process.
 * @param status   (OUTOUT) On exit will contain the status of the wait function.
 * @param options  Optional extra options for waitpid().
 **/
void wait_for_child
   (  const pid_t pid
   ,  int& status
   ,  int options
   )
{
   int waitpid_status = 0;

   // Loop over waitpid to wait for child
   while(waitpid(pid, &waitpid_status, options) < 0)
   {
      if(errno != EINTR)
      {
         status = -1;
         break;
      }
      
      std::this_thread::yield();
   }

   // After process
   if (  WIFEXITED(waitpid_status) )
   {
      status = WEXITSTATUS(waitpid_status);
   }
   else if  (  WIFSIGNALED(waitpid_status) )
   {
      std::cout << " Child stopped abnormally from recieving signal: " << WTERMSIG(waitpid_status) 
#ifdef WCOREDUMP
                << (WCOREDUMP(waitpid_status) ? " (core file generated) " : "")
#endif /* WCOREDUMP */
                << "." << std::endl;
      status = -1;
   }
   else if  (  WIFSTOPPED(waitpid_status) )
   {
      std::cout << " Child stopped from recieving stop signal: " << WSTOPSIG(status) << "." << std::endl;
      status = -1;
   }
   else if  (  WIFCONTINUED(waitpid_status) )
   {
      std::cout << " Child continued after job control stop. " << std::endl;
      status = -1;
   }
   else
   {
      std::cout << " Unknown signal from waitpid. " << std::endl;
      status = -1;
   }
}

/*
 * since pipes are unidirectional, we need two pipes.
 * one for data to flow from parent's stdout to child's
 * stdin and the other for child's stdout to flow to
 * parent's stdin 
 */
#define NUM_PIPES          3

#define PARENT_WRITE_PIPE  0
#define PARENT_READ_PIPE   1
#define PARENT_ERR_PIPE    2
 
/* always in a pipe[], pipe[0] is for read and
 * pipe[1] is for write */
#define READ_FD  0
#define WRITE_FD 1
 
#define PARENT_READ_FD  ( pipes[PARENT_READ_PIPE][READ_FD]   )
#define PARENT_WRITE_FD ( pipes[PARENT_WRITE_PIPE][WRITE_FD] )
#define PARENT_ERR_FD   ( pipes[PARENT_ERR_PIPE][READ_FD] )
 
#define CHILD_READ_FD   ( pipes[PARENT_WRITE_PIPE][READ_FD]  )
#define CHILD_WRITE_FD  ( pipes[PARENT_READ_PIPE][WRITE_FD]  )
#define CHILD_ERR_FD    ( pipes[PARENT_ERR_PIPE][WRITE_FD]  )

#define FD_ERR -1

/**
 * Spawn a process from a system command, 
 * wait for it to finish and return standard- out and error for logging.
 * This is the "serial" version (read NON-MPI).
 *
 * @param cmd     The command to run.
 * @param dir     The directory to run the command in.
 * @param out     (OUTPUT) On exit will hold contents of stdout from command.
 * @param err     (OUTPUT) On exit will hold contents of stderr from command.
 * @param pid     (OUTPUT) On exit will hold the pid of the spawned process.
 *
 * @return  Return status.
 **/
int spawn_process_serial
   (  const system_command_t& cmd
   ,  const std::string& dir
   ,  std::stringstream& out
   ,  std::stringstream& err
   ,  pid_t& pid
   )
{
   auto argv_vec = make_command(cmd);
   auto argv = &argv_vec[0];

   //pid_t pid;
   int status = 0; // Init. to 0 to quench "uninit." warning, hope it's okay. -MBH, Feb 2017.
   int pipes[NUM_PIPES][2];
                   
   // pipes for parent to write and read
   int pip0 = pipe(pipes[PARENT_READ_PIPE]);
   int pip1 = pipe(pipes[PARENT_WRITE_PIPE]);
   int pip2 = pipe(pipes[PARENT_ERR_PIPE]);
   
   if( (pid = fork()) < 0 )
   {
      status = -1;
   }
   else if(!pid) 
   {
      // child process
      exit_guard guard;

      // setup pipes
      if(dup2(CHILD_READ_FD, fileno(stdin)) == FD_ERR)
      {
         std::cout << " CHILD_READ_FD DUP ERROR " << std::endl;
         abort();
      }
      if(dup2(CHILD_WRITE_FD, fileno(stdout)) == FD_ERR)
      {
         std::cout << " CHILD_WRITE_FD DUP ERROR " << std::endl;
         abort();
      }
      if(dup2(CHILD_ERR_FD, fileno(stderr)) == FD_ERR)
      {
         std::cout << " CHILD_ERR_FD DUP ERROR " << std::endl;
         abort();
      }

      /* Close fds not required by child. Also, we don't
      want the exec'ed program to know these existed */
      close(CHILD_READ_FD);
      close(CHILD_WRITE_FD);
      close(CHILD_ERR_FD);
      close(PARENT_READ_FD);
      close(PARENT_WRITE_FD);
      close(PARENT_ERR_FD);
      
      // change directory if needed
      if(!dir.empty())
      {
         if(chdir(dir.c_str()) == -1)
         {
            midas::stream::HeaderInserter err_buf(std::cerr.rdbuf(),"*** ",true);
            std::ostream err(&err_buf);
            err << "COULD NOT CHDIR\n"
                << dir 
                << std::flush;
         }
      }

      // make call
      if(execvp(argv[0], argv) == -1)
      {
         // error handling
         midas::stream::HeaderInserter err_buf(std::cerr.rdbuf(),"*** ",true);
         std::ostream err(&err_buf);
         err << "COULD NOT RUN : " << std::strerror(errno) << "\n" << std::endl;
         int i=0;
         while(argv[i] != 0)
         {
            err << argv[i] << " ";
            ++i;
         }
         err << std::flush;
      }
   } 
   else 
   {
      // parent
      /* close fds not required by parent */       
      close(CHILD_READ_FD);
      close(CHILD_WRITE_FD);
      close(CHILD_ERR_FD);
      
      // Write to child’s stdin
      //write(PARENT_WRITE_FD, "2^32\n", 5);
      
      // Read from child’s stdout
      if(read_from_fd(PARENT_READ_FD,out))
         std::cout << " STD OUT ERROR " << std::endl;
      if(read_from_fd(PARENT_ERR_FD,err))
         std::cout << " STD ERR ERROR " << std::endl;
      
      // Wait for child to finish up
      wait_for_child(pid, status, 0);
      
      close(PARENT_READ_FD);
      close(PARENT_WRITE_FD);
      close(PARENT_ERR_FD);
   }

   return status;
}

// undef
#undef NUM_PIPES

#undef PARENT_WRITE_PIPE
#undef PARENT_READ_PIPE
#undef PARENT_ERR_PIPE
 
#undef READ_FD
#undef WRITE_FD
 
#undef PARENT_READ_FD
#undef PARENT_WRITE_FD
#undef PARENT_ERR_FD
 
#undef CHILD_READ_FD
#undef CHILD_WRITE_FD
#undef CHILD_ERR_FD

#undef FD_ERR

/**
 * Wrapper to spawn a process from a system command.
 * Will call either the serial version, or MPI version if VAR_MPI is set.
 *
 * @param cmd    The command to run.
 * @param dir     The directory to run the command in.
 * @param out     (OUTPUT) On exit will hold contents of stdout from command.
 * @param err     (OUTPUT) On exit will hold contents of stderr from command.
 * @param pid     (OUTPUT) On exit will hold the pid of the spawned process.
 *
 * @return  Return status.
 **/
int spawn_process
   (  const system_command_t& cmd
   ,  const std::string& dir
   ,  std::stringstream& out
   ,  std::stringstream& err
   ,  pid_t& pid
   )
{
#ifdef VAR_MPI
   // Always call serial version, as MPI version is buggy.
   // CAVEAT: Serial version does not work with MPI and infiniband, as it calls fork().
   //         Fix is currently to run on ethernet network instead, when using MPI, as this should work OK!
   return spawn_process_serial(cmd, dir, out, err, pid);
#else
   return spawn_process_serial(cmd, dir, out, err, pid);
#endif /* VAR_MPI */
}
