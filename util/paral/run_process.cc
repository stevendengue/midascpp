#include "util/paral/run_process.h"

#include "util/paral/spawn_process.h"
#include "util/paral/system_command_t.h"

/**
 *
 **/
threaded_system_return run_process
   (  const system_command_t& cmd
   ,  mutex_ostream& os
   )
{
   return threaded_system_call(cmd, os);
}

/**
 * Run a command/process through the OS.
 *
 * @param cmd  The command to run.
 * @param out  On output contains the contents of standard output from the command.
 * @param err  On output contains the contents of standard error from the command.
 *
 * @return   Returns the status of the cmd.
 **/
int run_process
   (  const system_command_t& cmd
   ,  std::stringstream& out
   ,  std::stringstream& err
   )
{
   pid_t pid;
   return spawn_process(cmd, std::string(""), out, err, pid);
}

/**
 * Run a command/process through the OS.
 *
 * @param cmd  The command to run.
 * @param dir  The specific dir in which to run.
 * @param out  On output contains the contents of standard output from the command.
 * @param err  On output contains the contents of standard error from the command.
 *
 * @return   Returns the status of the cmd.
 **/
int run_process_dir
   (  const system_command_t& cmd
   ,  const std::string& dir
   ,  std::stringstream& out
   ,  std::stringstream& err
   )
{
   pid_t pid;
   return spawn_process(cmd, dir, out, err, pid);
}
