#ifndef THREADED_SYSTEM_CALL_H_INCLUDED
#define THREADED_SYSTEM_CALL_H_INCLUDED

#include <string>
#include <vector>
#include <iostream>
#include <future>
#include <chrono>

#include "system_command_t.h"

//! Return type of threaded system calls
struct threaded_system_return
{
   //! Return status of system call.
   int status = 0;
   std::chrono::duration<double> duration= std::chrono::duration<double>(0.0);
};

//! Return current time and date as string
std::string date_now();

//! Make a threaded system call
threaded_system_return threaded_system_call(const system_command_t& cmd
                                          , mutex_ostream& os
                                          );

//! Make a threaded system in dir
threaded_system_return threaded_system_call_dir(const system_command_t& cmd
                                              , std::string dir
                                              , mutex_ostream& os
                                              );

#endif /*THREADED_SYSTEM_CALL_H_INCLUDED */
