#ifndef SPAWN_PROCESS_H_INCLUDED
#define SPAWN_PROCESS_H_INCLUDED

#include <vector>
#include <string>
#include <sstream>

#include "system_command_t.h"


/**
 * Spawn child process, and wait for it to run.
 **/
//int spawn_process(char* const argv[], const char* dir, std::stringstream& out, std::stringstream& err, pid_t& pid);
int spawn_process(const system_command_t& cmd, const std::string& dir, std::stringstream& out, std::stringstream& err, pid_t& pid);

#endif /* SPAWN_PROCESS_H_INCLUDED */
