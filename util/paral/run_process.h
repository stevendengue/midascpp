#ifndef RUN_PROCESS_H_INCLUDED
#define RUN_PROCESS_H_INCLUDED

#include<vector>
#include<string>
#include<sstream>

#include "threaded_system_call.h"

threaded_system_return run_process(const std::vector<std::string>& cmd, mutex_ostream& os);

int run_process(const std::vector<std::string>& cmd, std::stringstream& out, std::stringstream& err);

int run_process_dir(const std::vector<std::string>& cmd, const std::string& dir, std::stringstream& out, std::stringstream& err);

#endif /* RUN_PROCESS_H_INCLUDED */
