/**
 *******************************************************************************
 * 
 * @file    Complex_Impl.h
 * @date    24-04-2018
 * @author  Niels Kristian Madsen (nielskm@chem.au.dk)
 *
 * @brief
 *    Implementation details.
 *
 * @copyright
 *    Ove Christiansen, Aarhus University.
 *    The code may only be used and/or copied with the written permission of
 *    the author or in accordance with the terms and conditions under which the
 *    program was supplied.  The code is provided "as is" without any expressed
 *    or implied warranty.
 * 
 *******************************************************************************
 **/
#ifndef MIDAS_TYPE_TRAITS_COMPLEX_IMPL_H_INCLUDED
#define MIDAS_TYPE_TRAITS_COMPLEX_IMPL_H_INCLUDED

#include <type_traits>
#include <complex>

//! Implementation details, not to be used externally.
namespace midas::type_traits::detail
{
   /************************************************************************//**
    * @name IsComplex
    *
    * For determining whether the template type is complex or not.
    * 
    * Defines
    * - general template with value = false.
    * - std::complex<T> specialization with value = true.
    *
    * @note
    *    The template type must be floating point. You can argue whether this
    *    restriction is necessary, but is introduced because std::complex<T> is
    *    only well-defined (specialized) for T being a floating-point type.
    ***************************************************************************/
   //!@{
   template<class T>
   struct IsComplex
      :  public std::false_type
   {
   };
   template<class T>
   struct IsComplex<std::complex<T>>
      :  public std::true_type
   {
      static_assert(std::is_floating_point_v<T>, "T must be floating-point type.");
   };
   //!@}

   /************************************************************************//**
    * @name RealType
    *
    * For determining the underlying real type of the template type T.
    * E.g.
    * - for a real type T, we want the type itself T.
    * - for a complex type std::complex<T>, we want the real type T that
    *   std::complex<T> is an extension of.
    * 
    * Defines
    * - general template with type = T.
    * - std::complex<T> specialization also with type = T.
    ***************************************************************************/
   //!@{
   template<class T>
   struct RealType
   {
      using type = T;
   };
   template<class T>
   struct RealType<std::complex<T>>
   {
      static_assert(std::is_floating_point_v<T>, "T must be floating-point type.");
      using type = T;
   };
   //!@}

} /* namespace midas::type_traits::detail */

#endif /* MIDAS_TYPE_TRAITS_COMPLEX_IMPL_H_INCLUDED */
