/**
************************************************************************
* 
* @file                FileSystem.h
*
* Created:             07/06-2017
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   Functions for manipulating files.
* 
* Last modified: See git...
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDAS_FILESYSTEM_H_INCLUDED
#define MIDAS_FILESYSTEM_H_INCLUDED

#include<sys/stat.h>
#include<string>
#include<regex>

namespace midas
{
namespace filesystem
{
/**
 * Query.
 **/
//! Check for existance of path?
bool Exists(const std::string& aPath);

//! Is path a regular file?
bool IsFile(const std::string& aPath);

//! Is path a directory?
bool IsDir(const std::string& aPath);

//! Is path a symbolic link?
bool IsSymlink(const std::string& aPath);

//! Determines if a file is executable.
bool IsExecutable(const std::string& aPath);

//! Get the permissions associated to a file.
mode_t GetFilePermissions(const std::string& aPath);

//! Size in bytes of file. For symlink the length of the pathname w/o term. null byte.
off_t Size(const std::string& aPath);

/**
 * Manipulate files.
 **/
//! Copy a file.
int Copy(const std::string& aSourceFile, const std::string& aTargetFile);

//! Rename/move a file.
int Rename(const std::string& aOldPath, const std::string& aNewPath);

//! Remove a file/directory/link
int Remove(const std::string& aPathName);

//! Touch a file
int Touch(const std::string& aPathName);

/**
 * Manipulate directories.
 **/
//! Create a directory
int Mkdir(const std::string& aPathName, bool aRecursive = false, mode_t aMode = S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IXOTH);

//! Delete a directory.
int Rmdir(const std::string& aPathName, bool aRecursive = false);

/**
 * Manipulate meta of files
 **/
//! Chmod a file
int Chmod(const std::string& aPathName, mode_t aMode);

//! Synchronize linux filesystem and buffers
void Sync();

/**
 * Manipulate file links.
 **/
//! Create a HARD link to a file (for most cases this is not what you want, see instead Symlink() function).
int Link(const std::string& aOldPath, const std::string& aNewPath);

//! Create a symbolic link to a file.
int Symlink(const std::string& aFilePath, const std::string& aLinkPath);

//! Unlink HARD or symbolic link.
int Unlink(const std::string& aPath);

/**
 * Manipulate content of files.
 **/
//! Grep in file.
std::vector<std::string> Grep(const std::string& aFilePath, const std::regex& aRegex);

//! Search directory for files matching regex.
std::vector<std::string> SearchDir(const std::string& aDirPath, const std::regex& aRegex);

/**
 * Check for errors, and report.
 **/
//! Error checking. Check errno, and if set print a message.
int ErrorCheck(const std::string& aMessage, bool aHardError = false);

} /* namespace filesystem */
} /* namespace midas */

#endif /* MIDAS_FILESYSTEM_H_INCLUDED */
