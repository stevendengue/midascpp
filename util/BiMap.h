#ifndef MIDAS_UTIL_BIMAP_H_INCLUDED
#define MIDAS_UTIL_BIMAP_H_INCLUDED

#include <memory>
#include <exception>

#include "util/Io.h"

namespace midas
{
namespace util
{

/**
 *
 **/
template
   <  typename T
   ,  typename K1
   ,  typename K2
   >
class BiMap
{
   public:
      //! define pointer type
      using ptr_type = std::shared_ptr<T>;
      
      //! Insert map entry
      void insert(const ptr_type& value, const K1& key1, const K2& key2)
      {
         mMap1.insert(std::make_pair(key1, value));
         mMap2.insert(std::make_pair(key2, value));
         assert_sizes();
      }
      
      //! Find from first key type (K1)
      auto find1(const K1& key) const
      {
         return mMap1.find(key);
      }
      
      //! Get reference to first map using first key type (K2)
      const std::map<K1, ptr_type>& map1() const
      {
         return mMap1;
      }

      //! Find from second key type (K2) 
      auto find2(const K2& key) const
      {
         return mMap2.find(key);
      }
      
      //! Get reference to second map using second key type (K2)
      const std::map<K2, ptr_type>& map2() const
      {
         return mMap2;
      }

      //! Clear the bi-map
      void clear()
      {
         mMap1.clear();
         mMap2.clear();
      }

      //! Get size of bi-map
      std::size_t size() const
      {
         assert_sizes();
         return mMap1.size();
      }
   
   private:
      //! Assert that sizes of both maps are the same
      void assert_sizes() const
      {
         if(mMap1.size() != mMap2.size())
         {
            MIDASERROR("sizes not the same! mMap1.size() = " + std::to_string(mMap1.size()) + " mMap2.size() " + std::to_string(mMap2.size()));
         }
      }

      //! Map for first key type (K1)
      std::map<K1, ptr_type > mMap1;
      //! Map for second key type (K2)
      std::map<K2, ptr_type > mMap2;
};

} /* namespace util */
} /* namespace midas */

#endif /* MIDAS_UTIL_BIMAP_H_INCLUDED */
