/**
************************************************************************
* 
* @file                Io.h 
*
* Created:             18-08-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Control io.
* 
* Last modified: Wed Nov 18, 2009  01:32PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef IO_H
#define IO_H 

#ifdef VAR_MPI
#include<mpi.h>
#endif  //VAR_MPI

#include<iostream>
using std::ostream;
using std::cout;
using std::endl;

#include<iomanip>
using std::setw;
using std::left;
using std::right;
using std::setfill;
using std::internal;
using std::showpoint;
using std::noshowpoint;
using std::showbase;
using std::noshowbase;
using std::fixed;
using std::scientific;
using std::setprecision;
using std::showpos;
using std::noshowpos;
using std::uppercase;
using std::nouppercase;

#include<fstream>
using std::ofstream;
using std::ifstream;
using std::fstream;
using std::ios_base;
using std::ios;

#include <sstream>
using std::istringstream;
using std::ostringstream;

#include<string>
using std::string;
using std::getline;
using std::copy;

#include<cstring>
using std::strcpy;
using std::strcat;

#include<stdio.h>

#include<set>
using std::set;
#include<vector>
using std::vector;
#include <map>

#include<algorithm>
#include<iterator>
using std::istream_iterator;
using std::ostream_iterator;

#include<cctype>

#include<stdexcept>

#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "inc_gen/MaxFiles.h"
#include "inc_gen/Warnings.h"
#include "util/Property.h"
#include "util/stream/HeaderInserter.h"
#include "util/stream/ScopedManipulators.h"
#include "util/Error.h"
#include "util/Io_fwd.h"

#include "libmda/util/output_call_addr.h"
#include "libmda/util/type_info.h"

template<class T> class GeneralMidasVector;
typedef GeneralMidasVector<Nb> MidasVector;
class MidasStream;

extern MidasStream Mout; 

void Out72Char(std::ostream&,char,char,char);
void OneArgOut72(std::ostream&, string, char='|');
void TwoArgOut72(std::ostream&, string, string, char='|');
void TwoArgOut72(std::ostream&, string, bool, char='|');
void TwoArgOut72(std::ostream&, string, In, char='|');
void TwoArgOut72(std::ostream&, string, size_t, char='|');
void TwoArgOut72(std::ostream&, string, Nb, char='|');
void OneArgBox72(std::ostream&, string);
void TwoArgOut(std::ostream&, string, In, In, In, char='|');
void TwoArgOut(std::ostream&, string, In, Nb, In, char='|');

//void Sleeper(In);
void CopyFile(std::string, std::string);
void CopyAllFiles(string, string);
void CopyAllFilesFullPath(string aFile1,string aFile2);
bool InquireFile(const std::string&);
//bool InquireDir(const string&);
//bool FileLocked(string);
//void LockFile(string);
//void UnLockFile(string);
//bool WaitTillUnLocked(string);
//void Pwd(string&);
//void MkDir(string);
void MvFile(string, string);
void RmFile(string);
void TouchFile(string);

inline string StringForBool(bool aB) {if (aB) return "true"; return "false";}
string StringForNb(Nb aNb,In aSize=10);
string StringAfterLastSlash(const string& arS);

inline Nb NbFromString(string aStr) {istringstream tmp(aStr); Nb a_nb; tmp>> a_nb; return a_nb;}
inline In InFromString(string aStr) {istringstream tmp(aStr); In a_in; tmp>> a_in; return a_in;}
inline void uppercase(string& arStr) {for (size_t j=I_0; j<arStr.length(); ++j) arStr[j]=toupper(arStr[j]); return;}

template <typename T>
In ValFromString(string& aStr, T& aVal)
{
   // Convert to type T.
   T result;
   istringstream stream(aStr);
   if (stream >> result)
      aVal = result;
   else
      return -3;            // Conversion failed.
   return 0;
}

template <>
In ValFromString(string& aStr, bool& aVal);

bool CIEqual(char aCh1, char aCh2);
///< Case-insensitive character comparison.
///< Utility used by GetValueFromKey.

// Get a value of type T given as "key=val" in string aStr.
// The key search is case insensitive!
template <typename T>
In GetValueFromKey (const string& aStr, const string& aKey, T& aVal)
{
   string::iterator pos;

   // Find key.
   string s = aStr;
   pos = search(s.begin(), s.end(), aKey.begin(), aKey.end(), CIEqual);
   if (pos == s.end())
      return -1;
   s.erase(s.begin(), pos+aKey.length());
   
   // Search for and remove spaces and a single "=" before value.
   In i = 0;
   while (' ' == s[i]) i++;
   if ('=' != s[i])
      return -2;            // Unknown character between key and value.
   i++;
   while (' ' == s[i]) i++;
   s.erase(0, i);

   // Extract string form of value.
   s = s.substr(0, s.find(" "));

   return ValFromString(s, aVal);
}

/*
void FileOpen(In& aUnit, string& aName);
void FileClose(In aUnit);
*/
bool StringNoCaseCompare(string s1,string s2, In aLcompare=0);
void CheckUnit(string& arS,Nb& arConv);
void OutPutSetOfStrings(MidasStream&, const std::set<string>&, const std::string&);
void OutPutVectorOfStrings(MidasStream&, const std::vector<string>&, const std::string&);

bool SearchInFile(string,ifstream&,string,string);
bool SearchInFile(string,std::istringstream&,string,string);
bool SearchInFile2(string, ifstream&, string, string&, Nb&, In);
//void GetPropInfo(string&,Property&,Nb&);
void ReadFormCheck(string,string);
void WriteDaltonProp(Nb,In,In,string,string,string = "SCF/DFT");
void WriteDaltonDer(In dim, MidasVector& aGrdHess,string& aOutPut);
vector<string> SplitString(const string& aS, const string& aSplit);
string SplitString(string& aS, const string& aSplit,In& aIndex);
string ReverseString(string aS);

//std::string GetHost();
//void RemoteCopyFromNode(string,string,string);
//void RemoteCopyToNode(string,string,string);

/**
* << NucMapInit
* */
void InitNucMaps();

namespace midas::util::detail
{
/***************************************************************************//**
 * @brief
 *    Implementation detail for outputting containers with iterator interface.
 *
 * @note
 *    Only supports 1-character delimiters at the moment, e.g. `{a,b,c}`.
 *    Template the delimiter type if multi-character delimiters become
 *    necessary. -MBH, Mar 2019.
 *
 * @param[in,out] arOut
 *    Outstream.
 * @param[in] arCont
 *    Container to be outputted. Must support begin(), end() iterators.
 *    Contained class must have operator<< ostream overload.
 * @param[in] arDelims
 *    Tuple containing delimiters to be used; {opening, separator, closing}.
 ******************************************************************************/
template<typename CONTAINER_T>
void OutputContainerImpl
   (  std::ostream& arOut
   ,  const CONTAINER_T& arCont
   ,  const std::tuple<char, char, char>& arDelims
   )
{
   const auto& opn = std::get<0>(arDelims);
   const auto& sep = std::get<1>(arDelims);
   const auto& cls = std::get<2>(arDelims);
   arOut << opn;
   for(auto it = arCont.begin(), end = arCont.end(); it != end; )
   {
      arOut << *it;
      if (++it != end)
      {
         arOut << sep;
      }
   }
   arOut << cls;
}

} /* namespace midas::util::detail */

/***************************************************************************//**
 * @brief
 *    operator<< overload for std::set
 * 
 * Example:
 * ~~~
 *    std::cout << std::set<int>{2, 4, 5}; // Prints "{2,4,5}"
 * ~~~
 ******************************************************************************/
template<typename T>
std::ostream& operator<<(std::ostream& arOut, const std::set<T>& arCont)
{
   midas::util::detail::OutputContainerImpl(arOut, arCont, std::make_tuple('{',',','}'));
   return arOut;
}

/***************************************************************************//**
 * @brief
 *    operator<< overload for std::vector
 * 
 * Example:
 * ~~~
 *    std::cout << std::vector<int>{2, 4, 5}; // Prints "(2,4,5)"
 * ~~~
 ******************************************************************************/
template<typename T> 
std::ostream& operator<<(std::ostream& arOut, const std::vector<T>& arCont)
{
   midas::util::detail::OutputContainerImpl(arOut, arCont, std::make_tuple('(',',',')'));
   return arOut;
}


/**
 * operator<< overload for std::pair
 **/
template<typename T, typename U> 
ostream& operator<<(std::ostream& arOut, const std::pair<T,U>& ar1)
{
   arOut << "[" << ar1.first << "," << ar1.second << "]";
   return arOut;
}

/**
 * operator<< overload std::map
 **/
template<class T, class U>
std::ostream& operator<<(std::ostream& arOut, const std::map<T,U>& ar1)
{
   arOut << " map<" << libmda::util::type_of<T>() << ", " << libmda::util::type_of<U>() << "> :\n"
         << " {\n";
   for(auto& elem : ar1)
   {
      arOut << "   " << elem.first << " -> " << elem.second << "\n"; 
   }
   arOut << " }" << std::endl;
   return arOut;
}

namespace midas::util::detail
{
   //! Implementation. Only to be used through call to FileIo().
   template<class T, int N>
   void FileIoImpl(const string&, T&, const In, const IoType<N>&, In&);

   //! Implementation. Only to be used through call to FileIo().
   template
      <  class MV
      ,  int N
      ,  std::enable_if_t
         <  std::is_same_v
            <  std::remove_const_t<MV>
            ,  GeneralMidasVector<typename MV::value_type>
            >
         ,  int
         >  = 0
      >
   void FileIoImpl(const string&, MV&, const In, const In,
               const IoType<N>&, In&, In aVecOff);
} /* namespace midas::util::detail */

/***************************************************************************//**
 * @name FileIo
 *
 * These four are tag dispatch overloads (one for IO_GET, one for IO_PUT,
 * single element and vectorized versions), introduced (July 2018) in order to
 * achieve const correctness of the GeneralMidasVector. See like methods in
 * implementation of GeneralMidasVector and GeneralDataCont. -MBH, July 2018.
 ******************************************************************************/
//!@{

//@{
/**
 * @brief
 *    Get/put a scalar from/to file.
 *
 * @param aFileName
 *    Name of the file.
 * @param aData
 *    Reference to data to read into (IO_GET) or write from (IO_PUT).
 * @param aAddr
 *    Address of destination/source data in the file. NB! The first element on
 *    file has index, i.e. NOT standard C++ counting.
 * @param aPutGet
 *    IO_GET or IO_PUT.
 * @param aIerr
 *    When calling: if aIerr == -1, the program will continue even if
 *    encountering an I/O error. On return: aIerr != 0 signals an I/O error
 *    occured.
 **/
template<class T, int N, std::enable_if_t<bool(N & IO_GET), int> = 0>
void FileIo
   (  const string& aFileName
   ,  T& aData
   ,  const In aAddr
   ,  const IoType<N>& aPutGet
   ,  In& aIerr
   )
{
   midas::util::detail::FileIoImpl(aFileName, aData, aAddr, aPutGet, aIerr);
}

template<class T, int N, std::enable_if_t<bool(N & IO_PUT), int> = 0>
void FileIo
   (  const string& aFileName
   ,  const T& aData
   ,  const In aAddr
   ,  const IoType<N>& aPutGet
   ,  In& aIerr
   )
{
   midas::util::detail::FileIoImpl(aFileName, aData, aAddr, aPutGet, aIerr);
}
//@}

//@{
/**
 * @brief
 *    Get/put an array of scalars (a vector) from/to file.
 *
 * @param aFileName
 *    Name of the file.
 * @param aData
 *    Reference to vector to read into (IO_GET) or write from (IO_PUT).
 * @param aAddr
 *    Address of destination/source data in the file. NB! The first element on
 *    file has index, i.e. NOT standard C++ counting.
 * @param aLength
 *    The number of elements to read/write.
 * @param aPutGet
 *    IO_GET or IO_PUT.
 * @param aIerr
 *    When calling: if aIerr == -1, the program will continue even if
 *    encountering an I/O error. On return: aIerr != 0 signals an I/O error
 *    occured.
 * @param aVecOff
 *    The offset in aData, i.e. the position of the first element to assign
 *    from/to. (Usual 0-based counting.)
 **/
template<class T, int N, std::enable_if_t<bool(N & IO_GET), int> = 0>
void FileIo
   (  const string& aFileName
   ,  GeneralMidasVector<T>& aData
   ,  const In aAddr
   ,  const In aLength
   ,  const IoType<N>& aPutGet
   ,  In& aIerr
   ,  In aVecOff=I_0
   )
{
   midas::util::detail::FileIoImpl(aFileName, aData, aAddr, aLength, aPutGet, aIerr, aVecOff);
}

template<class T, int N, std::enable_if_t<bool(N & IO_PUT), int> = 0>
void FileIo
   (  const string& aFileName
   ,  const GeneralMidasVector<T>& aData
   ,  const In aAddr
   ,  const In aLength
   ,  const IoType<N>& aPutGet
   ,  In& aIerr
   ,  In aVecOff=I_0
   )
{
   midas::util::detail::FileIoImpl(aFileName, aData, aAddr, aLength, aPutGet, aIerr, aVecOff);
}
//@}
//!@}

#include "LinkFileIo_Impl.h"

// mbh: end new template definitions...

#endif /* IO_H */

