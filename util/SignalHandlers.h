#ifndef MIDAS_UTIL_SIGNALHANDLERS_H_INCLUDED
#define MIDAS_UTIL_SIGNALHANDLERS_H_INCLUDED

namespace midas
{
namespace util
{

void SetupSignalHandlers();

} /* namespace util */
} /* namespace midas */

#endif /* MIDAS_UTIL_SIGNALHANDLERS_H_INCLUDED */
