/**
************************************************************************
* 
* @file                MidasSystemCaller.h 
*
* Created:             07-02-2012
*
* Author:              Ian H. Godtliebsen (ian@chem.au.dk)
*
* Short Description:   A system caller that logs stdout and stderr in a file.
* 
* Last modified: Tue. Feb 07 2012
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef MIDASSYSTEMCALLER_H
#define MIDASSYSTEMCALLER_H

#include <stdio.h>
#include <stdlib.h>
#include <cstring>

#include "util/Timer.h"
#include "util/paral/system_command_t.h"

/**
 *
 **/
class MidasSystemCaller
{
   private:
      //! Delete default constructor.
      MidasSystemCaller() = delete;
      
      //! Delete copy constructor.
      MidasSystemCaller(const MidasSystemCaller&) = delete;
      
      //! Destructor
      ~MidasSystemCaller();

   public:
      //!
      static unsigned mN;
      
      //!
      static void Init();
      
      //!
      static void Log
         (  const system_command_t& aS
         ,  const string& dir
         ,  const std::stringstream& std_out
         ,  const std::stringstream& std_err
         ,  int status
         ,  const char* file
         ,  const unsigned line
         ,  const std::string& date
         );

      //!
      static int SystemWithLog(const system_command_t& aS, const char*, const unsigned);
      
      //!
      static int SystemDirWithLog(const system_command_t& aS, const std::string& dir, const char*, const unsigned);
      
      //!
      static int SystemToStreamWithLog(const system_command_t& aS, std::ostream& os, const char* file, const unsigned line);
      
      //!
      static int SystemDirToStreamWithLog(const system_command_t& aS, const std::string& dir, std::ostream& os, const char* file, const unsigned line);
      
      //!
      static int SystemNoLog(const system_command_t& aS);
      
      //!
      static int SystemToStreamNoLog(const system_command_t& aS, std::ostream& os);
      
      //!
      template<class F, class... Ts>
      static void SystemWrap(F&& f, Ts&&... ts)
      {
         int status = f(std::forward<Ts>(ts)...);
      }
};


//
// macros for doing system calls... only define one of them!
// one will log calling/debug info the other will not
//
// macro to do system call with log
#define MIDASSYSTEM(a) MidasSystemCaller::SystemWrap(MidasSystemCaller::SystemWithLog,(a),__FILE__,__LINE__)
// macro to do system call without log
//#define MIDASSYSTEM(a) MidasSystemCaller::SystemWrap(MidasSystemCaller::SystemNoLog,(a))

// macro to do system call with log
#define MIDASSYSTEMTOSTREAM(a,b) MidasSystemCaller::SystemWrap(MidasSystemCaller::SystemToStreamWithLog,(a),(b),__FILE__,__LINE__)
// macro to do system call without log
//#define MIDASSYSTEMTOSTREAM(a,b) MidasSystemCaller::SystemWrap(MidasSystemCaller::SystemToStreamNoLog,(a),(b))

// macro to do system call with log
#define MIDASSYSTEMSTATUS(a) MidasSystemCaller::SystemWithLog((a),__FILE__,__LINE__)
// macro to do system call without log
//#define MIDASSYSTEMSTATUS(a) MidasSystemCaller::SystemNoLog((a))

// macro to do system call with log
#define MIDASSYSTEM_DIR_STATUS(a,b) MidasSystemCaller::SystemDirWithLog((a),(b),__FILE__,__LINE__)

#define MIDASSYSTEM_DIR(a,b) MidasSystemCaller::SystemWrap(MidasSystemCaller::SystemDirWithLog,(a),(b),__FILE__,__LINE__)

#define MIDASSYSTEM_DIR_STREAM_STATUS(a,b,c) MidasSystemCaller::SystemDirToStreamWithLog((a),(b),(c),__FILE__,__LINE__)

/**
 * access to logging file
 **/
extern mutex_ostream Mlog;

#endif /* MIDASSYSTEMCALLER_H */
