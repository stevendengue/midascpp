/**
************************************************************************
* 
* @file                Plot.cc
*
* Created:             4-10-2002
*
* Author:              Peter Seidler (seidler@chem.au.dk)
*
* Short Description:   Implementing Plot class methods.
* 
*        Last modified: Mon Jan 18, 2010  01:58PM
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

// std headers
#include <string>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <sstream>

// midas headers
#include "inc_gen/TypeDefs.h"
#include "inc_gen/Const.h"
#include "util/MidasSystemCaller.h"
#include "util/Io.h"
#include "input/Input.h"
#include "util/MidasStream.h"
#include "util/Plot.h"

Plot::Plot(const In aSize)
{
   mXvals.SetNewSize(aSize, false);
   mYvals.SetNewSize(aSize, false);

   mPlotRangeXl = C_0;
   mPlotRangeXr = C_0;
   mPlotRangeYb = C_0;
   mPlotRangeYt = C_0;

   mLogScaleX = false;
   mLogScaleY = false;

   mLineStyle = "lines";
   mSmooth    = false;
   mSmooth    = false;
   mSmoothMethod = "NOSMOOTHING";
   mKey = true;
}

Nb Plot::GetMinX()
{
   return mXvals.FindMinValue();
}

Nb Plot::GetMaxX() 
{
   return mXvals.FindMaxValue();
}
Nb Plot::GetMinY()
{
   return mYvals.FindMinValue();
}
Nb Plot::GetMaxY() 
{
   return mYvals.FindMaxValue();
}
                 
void Plot::ScaleX(Nb aScale) 
{
   mXvals.Scale(aScale);
}

void Plot::ScaleY(Nb aScale) 
{
   mYvals.Scale(aScale);
}

void Plot::SortDataPoints()
{
   vector<NbPair> points;
   points.reserve(mXvals.Size());

   for (In i=0; i<mXvals.Size(); i++)
   {
      NbPair xy(mXvals[i], mYvals[i]);
      points[i] = xy;
   }
   
   sort(points.begin(), points.end());
   
   for (In i=0; i<mXvals.Size(); i++)
   {
      NbPair xy = points[i];
      mXvals[i] = xy.first;
      mYvals[i] = xy.second;
   }
}

/**
* Return index i of mXvals, such that mXvals[i] <= aX < mXvals[i+1].
* -1 is returned if aX is out of range.
*/
In Plot::Locate(const Nb aX)
{
   In jl, jm, ju;

   // These lower/upper limits are robust
   // (result in no access outside allocated space).
   jl = 0;
   ju = mXvals.Size() - 1;

   while (ju-jl > 1)
   {
      jm = (ju+jl)/2  ;     // Midpoint.
      if (aX >= mXvals[jm])
         jl = jm;
      else
         ju = jm;
   }

   if (aX == mXvals[0])
      return 0;
   else if (aX == mXvals[mXvals.Size()-2])
      return mXvals.Size()-2;      // Return point to the left according
   else                            // to mXvals[i]<= aX < mXvals[i+1].
      return jl;
}

void Plot::SetRange(const Nb aXmin, const Nb aXmax)
{
   Nb diff = aXmax-aXmin;
   In dim = mXvals.Size();

   for (In i=0; i<dim; i++)
      mXvals[i] = aXmin + i*( diff/(dim-I_1) );

   mYvals.Zero();
}

In Plot::SetXYvals(const MidasVector& aX, const MidasVector& aY)
{
   if (aX.Size() != mXvals.Size())
      return -1;
   if (aY.Size() != mYvals.Size())
      return -2;

   for (In i=0; i<mXvals.Size(); i++)
   {
      mXvals[i] = aX[i];
      mYvals[i] = aY[i];
   }
   
   SortDataPoints();
   return 0;
}

void Plot::SetPlotRange(Nb axl, Nb axr, Nb ayb, Nb ayt)
{
   mPlotRangeXl = axl;
   mPlotRangeXr = axr;
   mPlotRangeYb = ayb;
   mPlotRangeYt = ayt;
//   Mout << "sne Plot range has been set" << endl;
}

void Plot::SetPlotRangeRel(Nb axl, Nb axr, Nb ayb, Nb ayt)
{
   Nb xmin = mXvals.FindMinValue();
   mPlotRangeXl = xmin - axl*fabs(xmin);
   Nb xmax = mXvals.FindMaxValue();
   mPlotRangeXr = xmax + axr*fabs(xmax);
   Nb ymin = mYvals.FindMinValue();
   mPlotRangeYb = ymin - ayb*fabs(ymin);
   Nb ymax = mYvals.FindMaxValue();
   mPlotRangeYt = ymax + ayt*fabs(ymax);
}

void Plot::AddGaussian(const Nb aXpos, const Nb aHeight, const Nb aWidth, const string& aLabel)
{
   // Add gaussian.
   for (In i=0; i<mXvals.Size(); i++)
   {
      Nb x = mXvals[i];
      mYvals[i] += aHeight * exp( -I_4*log(C_2) * (x-aXpos)*(x-aXpos) / (aWidth*aWidth) );
   }

   AddLabel(aXpos, aHeight, aLabel);
   
   // This is an attempt to take care of already existing peaks in
   // deciding where to put label. It looks bad on log scale.
   //AddLabel(aXpos, mYvals[Locate(aXpos)], aLabel);
}

void Plot::AddLorentzian(const Nb aXpos, const Nb aHeight, const Nb aWidth, const string& aLabel)
{
   // Add Lorentizan.
   for (In i=0; i<mXvals.Size(); i++)
   {
      Nb x = mXvals[i];
      mYvals[i] += aHeight * aWidth / C_2 / ( pow(aXpos-x,C_2) + aWidth*aWidth/C_4);
   }

   //AddLabel(aXpos, aHeight, aLabel);
}
void Plot::AddBar(const Nb aXpos, const Nb aHeight, const Nb aWidth, const string& aLabel)
{
   In il = Locate(aXpos - aWidth/C_2);      // First point to the left of bar.
   In ir = Locate(aXpos + aWidth/C_2);      // First point to the right of bar.
   
   for (In i=il; i<=ir; i++)
      mYvals[i] += aHeight;

   AddLabel(aXpos, mYvals[Locate(aXpos)], aLabel);
}

void Plot::AddLabel(const Nb aXpos, const Nb aYpos, const string& aLabel)
{
   Label lbl;
   lbl.x = aXpos;
   lbl.y = aYpos;
   lbl.lbl = aLabel;
   mLabels.push_back(lbl);
}

void Plot::AddAnaNormDist(const Nb aMu, const Nb aSigma, const string& aTitle)
{
   ostringstream expr;
   expr << "1.0/(" << aSigma << "*sqrt(2*pi)) * exp(-(x-" << aMu << ")**2"
        << "/(2*" << aSigma << "**2))";

   mAnaFuncs.push_back(expr.str());

   ostringstream title;
   if ("auto" == aTitle)
      title << "Norm. dist. ({/Symbol m}=" << aMu << ", {/Symbol s}=" << aSigma << ")";
   else
      title << aTitle;
      
   mAnaFuncTitles.push_back(title.str());
}

void Plot::GenerateDataFile(const string& aFilename) const
{
   string datafile = aFilename + ".data";
   
   ofstream odata_stream(datafile.c_str());
   MidasStreamBuf odata_buf(odata_stream);
   MidasStream odata(odata_buf);
   midas::stream::ScopedPrecision(22, odata);
   odata.setf(ios::scientific);
   odata.setf(ios::uppercase);
   for (In i=0; i<mXvals.Size(); i++)
   {
      odata << mXvals[i] << "\t" << mYvals[i] << endl;
   }
   odata_stream.close();
}
 
/**
* Create gnuplot command file and gnuplot data file.
* Filename should be specified without extension, as these are added by the function.
*/
void Plot::MakeGnuPlot(const string& aFilename)
{
   string cmdfile = aFilename + ".plot";
   
   GenerateDataFile(aFilename);

   // If range has not been set, set default.
   if ((mPlotRangeXl == mPlotRangeXr) &&
       (mPlotRangeYb == mPlotRangeYt))
      SetPlotRangeRel(C_0, C_0, C_0, C_0);

   // Start generating command file.
   ofstream ocmd_stream(cmdfile.c_str());
   MidasStreamBuf ocmd_buf(ocmd_stream);
   MidasStream ocmd(ocmd_buf);
   ocmd << "set term postscript enhanced" << endl
        << "set output \"" << aFilename << ".ps\""  << endl
        << "set xlabel \"" << mXlabel << "\"" << endl
        << "set ylabel \"" << mYlabel << "\"" << endl;

   ocmd << "set xrange [" << mPlotRangeXl << ":" << mPlotRangeXr << "]" << endl
        << "set yrange [" << mPlotRangeYb << ":" << mPlotRangeYt << "]" << endl;
   
   // Scaling of axes.
   if (mLogScaleX)
      ocmd << "set logscale x" << endl;
   if (mLogScaleY)
      ocmd << "set logscale y" << endl;
   if(mKey)
      ocmd << "set key" << endl;
   else
      ocmd << "unset key" << endl;
   
   // Add labels.
   for (vector<Label>::const_iterator it = mLabels.begin(); it != mLabels.end(); it++)
      ocmd << "set label \"" << it->lbl << "\" at first " << it->x
           << ", first " << it->y << " center" << endl;

   // Plot data and analytical functions.
   ocmd << "set style data " << mLineStyle << endl;

   ocmd << "plot \"" << aFilename << ".data\""
        << " title \"" << mTitle <<"\""; 
   if (mSmooth&&mXvals.Size()>I_2) ocmd << " smooth " << mSmoothMethod;

   if(mAnaFuncs.size() != 0)
      for (In i=0; i<mAnaFuncs.size(); i++)
            ocmd << ",\\" << endl
                 << mAnaFuncs[i] << " t \"" << mAnaFuncTitles[i];
   
   ocmd << endl;
}

void Plot::MakeGnuPlot(const string& aFilename,
                       Plot** aAddPlots, const In anAddPlots)
{
   string cmdfile = aFilename + ".plot";
   
   GenerateDataFile(aFilename);
   for (In i=0; i<anAddPlots; i++)
      aAddPlots[i]->GenerateDataFile(aFilename + "_" + std::to_string(i+1));

   // If range has not been set, set default.
   if ((mPlotRangeXl == mPlotRangeXr) && (mPlotRangeYb == mPlotRangeYt)) 
   {
     SetPlotRangeRel(C_0, C_0, C_0, C_0);
   }
   // Start generating command file.
   ofstream ocmd_stream(cmdfile.c_str());
   MidasStreamBuf ocmd_buf(ocmd_stream);
   MidasStream ocmd(ocmd_buf);
   ocmd << "set term postscript enhanced" << endl
        << "set output \"" << aFilename << ".ps\""  << endl
        << "set xlabel \"" << mXlabel << "\"" << endl
        << "set ylabel \"" << mYlabel << "\"" << endl;

   ocmd << "set xrange [" << mPlotRangeXl << ":" << mPlotRangeXr << "]" << endl
        << "set yrange [" << mPlotRangeYb << ":" << mPlotRangeYt << "]" << endl;
   
   // Scaling of axes.
   if (mLogScaleX)
      ocmd << "set logscale x" << endl;
   if (mLogScaleY)
      ocmd << "set logscale y" << endl;
   
   // Add labels.
   for (vector<Label>::const_iterator it = mLabels.begin(); it != mLabels.end(); it++)
      ocmd << "set label \"" << it->lbl << "\" at first " << it->x
           << ", first " << it->y << " center" << endl;
           
   for (In i=0; i<anAddPlots; i++)
      for (vector<Label>::const_iterator it = aAddPlots[i]->mLabels.begin();
           it != mLabels.end(); it++)
         ocmd << "set label \"" << it->lbl << "\" at first " << it->x
              << ", first " << it->y << " center" << endl;
       
   // Plot data and analytical functions.
   ocmd << "plot \"" << aFilename << ".data\"" 
        << " with " << mLineStyle
        << " title \"" << mTitle<<"\"";
   if (mSmooth&&mXvals.Size()>I_2) ocmd << " smooth " << mSmoothMethod;

   for (In i=0; i<anAddPlots; i++)
   {
//      Mout << " sne writes to plot file for i= " << i+1 << endl;
      ocmd << ",\\" << endl
//           << "plot \"" << aFilename << "_" << std::to_string(i) << ".data\""
//sne
           << "\"" << aFilename << "_" << std::to_string(i+1) << ".data\""
           << " with " << mLineStyle
           << " title \"" << aAddPlots[i]->mTitle << "\"";
      if (i==anAddPlots-1) ocmd << endl;
      if (mSmooth&&mXvals.Size()>I_2) ocmd << " smooth " << mSmoothMethod;
   }

   for (In i=0; i<mAnaFuncs.size(); i++)
         ocmd << ",\\" << endl
              << mAnaFuncs[i] << " t \"" << mAnaFuncTitles[i];

   for (In i=0; i<anAddPlots; i++)
      for (In j=0; j<aAddPlots[i]->mAnaFuncs.size(); j++)
            ocmd << ",\\" << endl
                 << aAddPlots[i]->mAnaFuncs[j]
                 << " t \"" << aAddPlots[i]->mAnaFuncTitles[j];
}

void Plot::MakePsFile(const string& aFilename)
{
   MakeGnuPlot(aFilename);
   system_command_t cmd = {"gnuplot", gAnalysisDir + "/" + aFilename + ".plot"};
   MIDASSYSTEM(cmd);
}

void Plot::MakePsFile(const string& aFilename, Plot** aAddPlots,
      const In anAddPlots)
{
   MakeGnuPlot(aFilename, aAddPlots, anAddPlots);
   system_command_t cmd = {"gnuplot", gAnalysisDir + "/" + aFilename + ".plot"};
   MIDASSYSTEM(cmd);
}

/***********************************
 *
 * Class implementation is over.
 * Next are some utility functions.
 *
 ***********************************/

/** Utility function for plotting a set of data in the same figure. */
void MakeMultiPlotPs(const string& aFilename, const string& aXlabel, const string& aYlabel,
                     string* aTitles,
                     MidasVector* aXvals, MidasVector* aYvals,
                     In anSets, bool aXlog, bool aYlog,
                     Nb aScaleX, Nb aScaleY)
{
   Plot mainplot(aXvals[0].Size());
   // Just in case someone tries to plot negative values on a logarithmic scale.
   if (aXlog) for(int j=0; j<aXvals[0].Size(); ++j) if(libmda::numeric::float_neg(aXvals[0][j])) aXvals[0] = -aXvals[0];
   if (aYlog) for(int j=0; j<aYvals[0].Size(); ++j) if(libmda::numeric::float_neg(aYvals[0][j])) aYvals[0] = -aYvals[0];
    
   mainplot.SetXYvals(aXvals[0], aYvals[0]);
   mainplot.SetXlabel(aXlabel);
   mainplot.SetYlabel(aYlabel);
   mainplot.SetTitle(aTitles[0]);
   mainplot.SetLogScaleX(aXlog);
   mainplot.SetLogScaleY(aYlog);
   mainplot.ScaleX(aScaleX);
   mainplot.ScaleY(aScaleY);

   // For determining plotting range.
   Nb xmin = aXvals[0].FindMinValue();
   Nb xmax = aXvals[0].FindMaxValue(); 
   Nb ymin = aYvals[0].FindMinValue();
   Nb ymax = aYvals[0].FindMaxValue();

   Plot** addplots = new Plot*[anSets-1];
   for (In i=1; i<anSets; i++)// sne change to <=
   {
      if (aXlog) for(int j=0; j<aXvals[i].Size(); ++j) if(libmda::numeric::float_neg(aXvals[i][j])) aXvals[0] = -aXvals[0];
      if (aYlog) for(int j=0; j<aYvals[i].Size(); ++j) if(libmda::numeric::float_neg(aYvals[i][j])) aYvals[0] = -aYvals[0];
         
      addplots[i-1] = new Plot(aXvals[i].Size());
      addplots[i-1]->SetXYvals(aXvals[i], aYvals[i]);
      addplots[i-1]->SetTitle(aTitles[i]);
      addplots[i-1]->ScaleX(aScaleX);
      addplots[i-1]->ScaleY(aScaleY);

      if (aXvals[i].FindMinValue() < xmin) xmin=aXvals[i].FindMinValue();
      if (aXvals[i].FindMaxValue() > xmax) xmax=aXvals[i].FindMaxValue();
      if (aYvals[i].FindMinValue() < ymin) ymin=aYvals[i].FindMinValue();
      if (aYvals[i].FindMaxValue() > ymax) ymax=aYvals[i].FindMaxValue();
   }
   ymin = ymin - fabs(ymin)/C_10;
   ymax = ymax + fabs(ymax)/C_10;
   if (fabs(ymax-ymin)<C_NB_EPSILON*C_10)
      ymax = ymin + C_NB_EPSILON*C_10_2;
   
   mainplot.SetPlotRange(aScaleX*xmin, aScaleX*xmax, aScaleY*ymin, aScaleY*ymax);
   mainplot.MakePsFile(aFilename, addplots, anSets-1);

   for (In i=1; i<anSets; i++) // sne change to <=
      delete addplots[i-1];
   delete [] addplots;
}

/** Utility function for plotting a set of data in the same figure.
 *  xValues are automatically generated as aFirstX, aFirstX+1, aFirstX+2, ...
 */
void MakeMultiPlotPsAutoX(const string& aFilename,
                          const string& aXlabel, const string& aYlabel,
                          string* aTitles,
                          In aFirstX, MidasVector* aYvals,
                          In aNsets, bool aXlog, bool aYlog,
                          Nb aScaleX, Nb aScaleY)
{
   // Simply generate the x values and call the overloaded function which takes
   // the x values as an argument.
   MidasVector* xvals = new MidasVector[aNsets];
   for (In i=0; i<aNsets; i++)
   {
      xvals[i].SetNewSize(aYvals[i].Size());
         for (In n=0; n<aYvals[i].Size(); n++)
            xvals[i][n] = aFirstX+n;
   }
   
   MakeMultiPlotPs(aFilename, aXlabel, aYlabel, aTitles, xvals, aYvals, aNsets,
                   aXlog, aYlog, aScaleX, aScaleY);

   delete[] xvals;
}
