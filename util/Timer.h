/**
************************************************************************
* 
* @file                util/Timer.h
*
* Created:             05-11-2001
*
* Author:              Ove Christiansen (ove@chem.au.dk)
*
* Short Description:   Timing utility class and declarations.
* 
* Last modified: man mar 21, 2005  11:40
* 
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef TIMER_H
#define TIMER_H

// mbh: support for PG compilers
#ifdef VAR_PGF77
#include <time.h>
#endif

// std headers
#include <fstream>
#include <string>

// midas headers.
#include "inc_gen/TypeDefs.h"
#include "util/MidasStream.h"

// using declarations
using std::ofstream;
using std::string;

/**
* declaration of outputdate functions. 
* */
void    OutputDate(std::ostream&,const string& s);


class Timer
{
   private:
      time_t  mTime0;
      clock_t mClock0;
      time_t  mTimeLast;
      clock_t mClockLast;
      bool    mClockWorks;
      bool    mTimeWorks;

   public:
      Timer();
      void CpuOut (std::ostream&,const std::string&,bool=false);
      void WallOut(std::ostream&,const std::string&,bool=false);
      Nb CpuTime (bool=false);
      Nb WallTime(bool=false);
      void SetTimer();
      void Reset();
      
      bool ClockWorks() const {return mClockWorks;}
      bool TimeWorks()  const {return mTimeWorks;}
};

#endif


