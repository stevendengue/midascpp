/**
************************************************************************
* 
* @file                FileHandler.h
*
* Created:             10-10-2012
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*                      Emil Lund Klinting (klint@chem.au.dk)
*
* Short Description:   Handler class for .mbar files
* 
* Last modified:       20-04-2018
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef BARINTERFACE_H
#define BARINTERFACE_H

// std headers
#include <map>
#include <list>
#include <vector>
#include <string>
#include <algorithm>
#include <fstream>
#include <stdarg.h>

// midas headers
#include "pes/PesInfo.h"
#include "input/PesCalcDef.h"
#include "input/ModeCombi.h"
#include "util/Io.h"
#include "util/conversions/TupleFromString.h"
#include "util/conversions/VectorFromString.h"
#include "inc_gen/TypeDefs.h"

namespace BarHandles
{
   using std::vector;
   using std::map;
   
   /**
    * The following struct and function allows us to set the fstream which we wish to direct our output to...
   **/
   struct FileNr
   {
      In mToPass;
   };

   inline FileNr
   set_file(In aIn)
   {
      FileNr theone;
      theone.mToPass = aIn;
      return theone;
   }

   struct StartOfBlock
   {
      In mToPass;
   };

   inline StartOfBlock
   set_readfrom(In aIn)
   {
      StartOfBlock theone;
      theone.mToPass = aIn;
      return theone;
   }

   struct EndOfBlock
   {
      In mToPass;
   };

   inline EndOfBlock
   set_readto(In aIn)
   {
      EndOfBlock theone;
      theone.mToPass = aIn;
      return theone;
   }
   
   /**
    *
   **/
   class BarFileInterface
   {
      private:
        
         //! PesCalcDef reference 
         const PesCalcDef&             mPesCalcDef;
      
         //! PesInfo reference
         const PesInfo&                mPesInfo;
         
         //! The current save directory
         const std::string             mSaveDir;

         //! The bar file storage type in use
         const std::string             mBarFileStorageType;

         //! A collection of all bar-file offsets
         std::vector<std::tuple<std::string, In, In>> mBarFilesOffsets;

         //! Bar-file data for all properties in use
         std::vector<std::vector<std::vector<Nb>>> mBarFileData;

      public:

         //! Constructor
         BarFileInterface(const PesCalcDef& aPesCalcDef, const PesInfo& aPesInfo);

         //! Destructor
         ~BarFileInterface();
 
         //! Determine if any special action is needed to be taken
         void Initialize();

         //! Get the offsets which interface to the reading of bar files 
         void GetOffsetsForInterface();

         //! Get the bar-file offsets for a particular mode combination
         void GetOffsetsForMc(const ModeCombi& aModeCombi, In& aReadFromLine, In& aReadToLine) const;

         //! Get single point data pertaining to a specific mode combination 
         void GetSinglePointBarDataForMc(const ModeCombi& aModeCombi, const In& aBarNumber, In& aInputPoints, std::vector<MidasVector>& aBarFileCoords, std::vector<MidasVector>& aBarFileCoordsUncompressed, MidasVector& aBarFileProperty, std::vector<MidasVector>& aBarFileDerivatives, const bool& aAvailableDerivatives, const bool& aDoMsi) const;
         
         //! Get single point property data pertaining to a specific mode combination only
         void GetSinglePointBarPropertyForMc(const ModeCombi& aModeCombi, const In& aBarNumber, MidasVector& aBarFileProperty) const;

         //! Read all bar-file information available into memory and store as member
         void ReadBarFileDataIntoMem();
   };
}
#endif //define BARINTERFACE_H
