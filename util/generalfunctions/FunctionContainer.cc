#ifndef DISABLE_PRECOMPILED_TEMPLATES

#include "FunctionContainer.h"
#include "FunctionContainer_Decl.h"
#include "FunctionContainer_Impl.h"

#include "mathlib/Taylor/taylor.hpp"

#define INSTANTIATE_FUNCTIONCONTAINER(T) \
template class FunctionContainer<T>;

// concrete instantiations
INSTANTIATE_FUNCTIONCONTAINER(float)
INSTANTIATE_FUNCTIONCONTAINER(double)

using taylor_float = taylor<float, 1, 1>;
INSTANTIATE_FUNCTIONCONTAINER(taylor_float)

using taylor_double = taylor<double, 1, 1>;
INSTANTIATE_FUNCTIONCONTAINER(taylor_double)

#undef INSTANTIATE_FUNCTIONCONTAINER

#endif /* DISABLE_PRECOMPILED_TEMPLATES */
