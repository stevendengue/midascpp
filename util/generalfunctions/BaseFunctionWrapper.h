/**
************************************************************************
* 
* @file                BaseFunctionWrapper.h
*
* Created:             06-06-2013
*
* Author:              Bo Thomsen (bothomsen@chem.au.dk)
*
* Short Description: Class for wrapping a function of a single variable,
                     complete with memory dealocation and all
* 
* Last modified: Thu Jun 6, 2013  01:13PM
*
* Copyright:
*
* Ove Christiansen, Aarhus University.
* The code may only be used and/or copied with the written permission 
* of the author or in accordance with the terms and conditions under 
* which the program was supplied.  The code is provided "as is" 
* without any expressed or implied warranty.
* 
************************************************************************
*/

#ifndef BASEFUNCTIONWRAPPER_H_INCLUDED
#define BASEFUNCTIONWRAPPER_H_INCLUDED

#include <vector>
#include <string>
#include <iostream>

#include "inc_gen/TypeDefs.h"
#include "util/generalfunctions/FunctionContainer.h"
#include "util/generalfunctions/GenericFunctions.h"
#include "util/Io.h"

using std::vector;
using std::string;

template<class T>
class BaseFunctionWrapper
{
   private:
      BaseFunctionWrapper();
   protected:
      vector<string> mVariableNames;
      vector<T>      mVariables;
      PrimitiveExpressions::AbstractExpr<T>* mPtr;
      In mAritOperCount;
      string mFunction;
   public:
      BaseFunctionWrapper(const string& aFunction, const vector<string>& aSetOfVars = vector<string>(1,"Q")) 
         : 
           mVariableNames(aSetOfVars)
         , mVariables(aSetOfVars.size(),C_0)
         , mFunction(aFunction)
         , mAritOperCount(I_0) 
      { 
      }

      BaseFunctionWrapper(const BaseFunctionWrapper<T>& aFW) 
         : 
           mVariableNames(aFW.mVariableNames)
         , mVariables(aFW.mVariables)
         , mAritOperCount(aFW.mAritOperCount)
         , mFunction(aFW.mFunction)
      {
         if(aFW.mPtr)
            mPtr = aFW.mPtr->Copy();
         else
            mPtr = nullptr;
      }

      bool CompareFuncStr(const string& aStr) const {return aStr == mFunction;}
      virtual string GetFunctionStr() const {return mFunction;}
      In GetAritOperCount() const {return mAritOperCount;}
      
      void SetVariable(const T& aValue, In aIn = 0) 
      {
         //Mout << "Evaluating function for : " << aIn << " aka " << mVariableNames[aIn] << " with val : " << aValue << endl;
         mVariables[aIn] = aValue;
      }

      void SetVariables(const vector<T>& aValues) 
      {
         if (aValues.size() != mVariables.size())
         {
            MIDASERROR("Something wrong in setting variables for function " + mFunction);
         }

         for (In i = I_0; i < mVariables.size(); ++i)
         {
            mVariables[i] = aValues[i];
         }
      }      

      virtual BaseFunctionWrapper<T>* Copy() = 0;
      virtual ~BaseFunctionWrapper() {if(mPtr != NULL){delete mPtr;}}
      T EvaluateFunc() {return mPtr->eval(mVariables);}
      T EvaluateFuncForVar(const T& aValue, In aIn = 0)
      {
         mVariables[aIn] = aValue;
         return mPtr->eval(mVariables);
      }
      
      T EvaluateFunctionForVars(const vector<T>& aValues)
      {
         return mPtr->eval(aValues);
      }     


      friend ostream& operator<<(ostream& as, const BaseFunctionWrapper<T>& ar)
      {
         as << "Operator(" << ar.mFunction << ") where pointer is ";
         if(ar.mPtr)
            as << "ready" << endl;
         else
            as << "not ready" << endl;
         return as;
      }
};
#endif //BASEFUNCTIONWRAPPER
